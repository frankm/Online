//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <RPC/RpcClientHandle.inl.h>
#include <HTTP/HttpClient.h>
#include <RPC/JSONRPC.h>
#include <RPC/XMLRPC.h>

// C/C++ include files

namespace rpc {
  template class RpcClient<http::HttpClient>;
  template class ClientHandle<xmlrpc::traits>;
  template class ClientHandle<jsonrpc::traits>;
}
