//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_RPC_REDIRECTIONPARSER_H
#define ONLINE_RPC_REDIRECTIONPARSER_H

// C/C++ include files
#include <string>
#include <vector>
#include <map>

/// Namespace for the http server and client apps
namespace rpc  {

  class RedirectionParser    {
  public:
    using Redirections = std::vector<std::map<std::string, std::string> >;
    RedirectionParser() = default;
    Redirections parse(const std::string& uri);

  };
}
#endif /* ONLINE_RPC_REDIRECTIONPARSER_H  */
