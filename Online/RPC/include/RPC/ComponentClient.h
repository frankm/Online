//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_COMPONENTCLIENT_H
#define RPC_COMPONENTCLIENT_H

// Framework include files
#include <RPC/RpcClientHandle.h>
#include <CPP/ObjectProperty.h>

// C/C++ include files
#include <memory>

/// Namespace for the dimrpc based implementation
namespace rpc  {

  class DimClient;

  ///  XMLRPC Client class based on DIM
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class ComponentClient : public XmlRpcClientH  {
  public:
    /// Definition of the property collection type
    typedef std::vector<ObjectProperty> Properties;
    /// Definition of the clients collection type
    typedef std::vector<std::string>    Clients;

  public:
    using XmlRpcClientH::XmlRpcClientH;

    /// Access the hosted client services
    Clients    clients()  const;
    /// Access all properties of an object
    Properties allProperties()  const;
    /// Access all properties with the same name from all clients
    Properties namedProperties(const std::string& name)  const;
    /// Access all properties of one remote client (service, ect.)
    Properties clientProperties(const std::string& client)  const;
    /// Access a single property of an object
    ObjectProperty property(const std::string& client,
			    const std::string& name)  const;
    /// Modify a property
    void setProperty(const std::string& client,
		     const std::string& name,
		     const std::string& value)  const;
    /// Modify a property
    void setPropertyObject(const ObjectProperty& property) const;
    /// Modify a whole bunch of properties. Call returns the number of changes
    int  setProperties(const Properties& properties)  const;
    /// Execute an interpreter request to the ROOT istance (if present)
    int interpreteCommand(const std::string& command)  const;
    /// Execute an interpreter request to the ROOT istance (if present)
    int interpreteCommands(const std::vector<std::string>& commands)  const;
    /// Exit the remote client
    int forceExit()  const;
  };
}

#endif  // RPC_COMPONENTCLIENT_H

