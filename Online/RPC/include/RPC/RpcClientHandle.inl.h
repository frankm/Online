//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_RPCCLIENTHANDLE_INL_H
#define RPC_RPCCLIENTHANDLE_INL_H

// Framework include files
#include <RPC/RpcClientHandle.h>

// C/C++ include files
#include <vector>
#include <sstream>
#include <stdexcept>

/// Namespace for the http based implementation
namespace rpc  {

  template <typename PROTOCOL> inline
    int RpcClient<PROTOCOL>::debug()  const   {
    return imp.debug();
  }

  template <typename PROTOCOL> inline
    int RpcClient<PROTOCOL>::setDebug(int value)   {
    return imp.setDebug(value);
  }

  template <typename PROTOCOL> inline
    std::string RpcClient<PROTOCOL>::name()  const   {
    return imp.name();
  }

  /// Connect client to given URI and execute RPC call
  template <typename PROTOCOL> inline
    std::vector<unsigned char> RpcClient<PROTOCOL>::request(const void* request_data,
							    size_t len)  const
    {
      return imp.request(request_data, len);
    }

  /// Initializing constructor
  template <typename PROTOCOL> inline
    ClientHandle<PROTOCOL>::ClientHandle(handler_t&& cl)
    : handler(std::move(cl))   {
  }

  /// Standard destructor
  template <typename PROTOCOL> inline
    ClientHandle<PROTOCOL>::~ClientHandle()
  {
    this->handler.reset();
  }

  /// Connect client to given URI
  template <typename PROTOCOL> inline
    typename PROTOCOL::Response 
    rpc::ClientHandle<PROTOCOL>::call(const typename PROTOCOL::Call& call)   const  {
    std::stringstream str;
    try  {
      auto resp = handler->request(call.str());
      typename PROTOCOL::Response mr = PROTOCOL::Response::decode(&resp[0], resp.size()-1);
      if ( mr.isFault() )   {
	str.str("");
	str << "RPC fault [" << mr.faultCode() << "]: " << mr.faultString();
	throw std::runtime_error(str.str());
      }
      return mr;
    }
    catch(const std::exception& e)   {
      str.str("");
      str << e.what();
    }
    catch( ... )   {
      std::error_code errcode(errno,std::system_category());
      str << "RPC fault [" << errcode.value() << "] (UNKOWN Exception): " << errcode.message();
    }
    throw std::runtime_error(str.str());
  }
}
#endif  // RPC_RPCCLIENTHANDLE_INL_H
