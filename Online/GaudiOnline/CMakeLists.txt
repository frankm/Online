#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/GaudiOnline
------------------
#]=======================================================================]

find_package(TBB REQUIRED)

online_library(GaudiOnline
        src/Configuration.cpp
        src/OnlineApplication.cpp
        src/QueuedFlowManager.cpp)
target_link_libraries(GaudiOnline 
 PUBLIC     Gaudi::GaudiKernel
            Online::EventHandling
            Online::OnlineBase
            TBB::tbb
 PRIVATE    Online::dim )
target_compile_options(GaudiOnline     PRIVATE -Wno-address-of-packed-member) 
target_compile_definitions(GaudiOnline PRIVATE MessageSvc=OnlineMessageSvc) 

online_gaudi_module(GaudiOnlineComp
        components/AlgFlowManager.cpp
        components/CFNodePropertiesParse.cpp
        components/Components.cpp
        components/Configuration.cpp
        components/ControlFlowNode.cpp
        components/EventProcessor.cpp
        components/FlowManager.cpp
        components/IOService.cpp
        components/InputAlg.cpp
        components/InteractiveAlg.cpp
        components/NumaControlSvc.cpp
        components/OnlineApp.cpp
        components/OnlineEventApp.cpp
        components/OnlineUISvc.cpp
        components/OutputAlg.cpp
        components/Passthrough.cpp
        components/Tell1BankDump.cpp
        components/Tell1HeaderDump.cpp
        components/FillingSchemeAlg.cpp)
target_link_libraries(GaudiOnlineComp PRIVATE
        LHCb::HltInterfaces
        cppgsl::cppgsl
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        Gaudi::GaudiPluginService
	Online::dim
        Online::EventData
        Online::EventHandling
        Online::GaudiOnline
        Online::OnlineBase
	Online::GauchoLib
        PkgConfig::numa
        ROOT::Core
        TBB::tbb
        ${CMAKE_DL_LIBS} -lrt)

online_apply_numa_defs(GaudiOnlineComp)
target_compile_options(GaudiOnlineComp     PRIVATE -Wno-address-of-packed-member) 
#target_compile_definitions(GaudiOnlineComp PRIVATE MessageSvc=OnlineMessageSvc)

online_install_includes(include)
online_install_python(python)

online_executable(GaudiOnlineExe main/main.cpp)
target_link_libraries(GaudiOnlineExe PRIVATE Online::dim Gaudi::GaudiKernel Online::GaudiOnline)

online_executable(GaudiCheckpoint main/checkpoint.cpp)
target_link_libraries(GaudiCheckpoint PRIVATE Online::dim Gaudi::GaudiKernel Online::GaudiOnline)

if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    find_library(tcmalloc_LIBRARY NAMES tcmalloc)
    find_library(unwind_LIBRARY NAMES unwind)
    target_link_libraries(GaudiCheckpoint PRIVATE ${tcmalloc_LIBRARY} ${unwind_LIBRARY})
    target_link_libraries(GaudiOnlineExe PRIVATE  ${tcmalloc_LIBRARY} ${unwind_LIBRARY})
endif()
