"""
     Online Passthrough application configuration

     @author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

from .OnlineApplication import *

class ReadDatafile(Application):
  def __init__(self, outputLevel, partitionName='OFFLINE', partitionID=0xFFFF):
    Application.__init__(self, 
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID,
                         classType=Class1)

  def setup_algorithms(self):
    import Gaudi.Configuration as Gaudi
    import Configurables
    self.config.numEventThreads  = 1
    self.config.numStatusThreads = 1
    self.config.events_HighMark  = 0
    self.config.events_LowMark   = 0
    self.config.logDeviceType    = 'RTL::Logger::LogDevice'
    self.config.logDeviceFormat  = '%TIME%LEVEL %-16SOURCE'

    input                        = Configurables.Online__InputAlg('EventInput')
    input.DeclareData            = True
    input.DeclareEvent           = True
    input.ExpandTAE              = True
    input.MakeRawEvent           = True
    explorer                     = Configurables.StoreExplorerAlg('Explorer')
    explorer.Load                = 1
    explorer.PrintFreq           = 1.0
    explorer.OutputLevel         = 1
    ask                          = Configurables.Online__InteractiveAlg('DumpHandler')
    ask.Prompt                   = "Press <ENTER> to dump banks, q or Q to quit :"
    dump                         = Configurables.Online__Tell1BankDump('Dump')
    dump.RawData                 = 'Banks/RawData'
    dump.CheckData               = 0
    dump.DumpData                = 1
    dump.FullDump                = 1
    dump.OutputLevel             = 1
    ctrl                         = Configurables.Online__InteractiveAlg('CommandHandler')
    ctrl.Prompt                  = "Press <ENTER> to continue, q or Q to quit :"
    self.app.TopAlg              = [input, explorer,ask,dump,ctrl]
    #self.app.TopAlg              = [input]
    self.broker.DataProducers    = self.app.TopAlg
    return self

def readDatafile(file):
  app = ReadDatafile(outputLevel=MSG_VERBOSE)
  if isinstance(file,list):
    app.setup_file_access(file)
  else:
    app.setup_file_access([file])
  app.config.autoStart = True
  app.config.autoStop  = True
  app.setup_hive(FlowManager("EventLoop"), 1)
  app.setup_algorithms()
  return app

