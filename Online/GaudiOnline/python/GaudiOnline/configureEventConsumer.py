from OnlineApplication import *

def configureConsumer(appMgr, broker, raw, output=False):
  from SetupHelper import setupAlgorithm
  application = Application(MSG_INFO, 'SF', 0xFFFF)
  #writer      = application.setup_mbm_output('EventOutput')
  #writer.MBM_maxConsumerWait = 10
  #input = application.setup_event_input(TAE=False)
  input = setupAlgorithm('Online__InputAlg',
                         appMgr, broker, 
                         instanceName='EventInput',
                         RawData='/Event/DAQ/RawData', 
                         IOSvc='Online::IOService')
  application.setup_monitoring()
  application.setup_mbm_access('Input', True)
  return input

