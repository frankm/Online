import os, sys, socket, GaudiOnline
import Configurables

print_level = 3
application = GaudiOnline.Passthrough(outputLevel=print_level,
                                      partitionName='FEST',
                                      partitionID=0xFFFF,
                                      classType=GaudiOnline.Class1)
#
event_source = application.partitionName+'_MON0101_EventSrv'
application.setup_net_access(event_source)

explorer                     = Configurables.StoreExplorerAlg('Explorer')
explorer.Load                = 1
explorer.PrintFreq           = 1
explorer.OutputLevel         = 2

application.setup_hive(GaudiOnline.FlowManager("EventLoop"), 40)
application.setup_algorithms([explorer], 0.01)
#application.passThrough.MicroDelayTime = 5e5 # 0.5 seconds
application.config.NET_cancelDeath = False
application.config.numEventThreads = 1
application.config.execMode        = 1
application.config.autoStart       = True
print('Setup complete....')
