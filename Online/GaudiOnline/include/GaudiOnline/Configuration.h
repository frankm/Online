//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef GAUDIONLINE_CONFIGURATION_H
#define GAUDIONLINE_CONFIGURATION_H

/// Framework include files
#include <EventHandling/MBMEventAccess.h>
#include <EventHandling/NetEventAccess.h>
#include <EventHandling/FileEventAccess.h>
#include <GaudiKernel/Service.h>


/// C/C++ include files

/// Forward declarations

/// Online namespace declaration
namespace Online  {

  /// Helper service to pass options to non-service clients
  /** @class Configuration Configuration.h
   *
   *
   * \author  M.Frank
   * \version 1.0
   * \date    25/04/2019
   */
  class Configuration : public Service  {
  public:
    /// Retrieve interface ID
    static const InterfaceID& interfaceID() {
      // Declaration of the interface ID.
      static const InterfaceID iid("Online::Configuration", 0, 0);
      return iid;
    }
  public:
    /// Property: Debug flag
    bool                           debug;
    /// Property: Flag to dump options in the application constructor
    bool                           dump_options;
    /// Property: Automatic startup flag
    bool                           autoStart;
    /// Property: Automatic stop flag
    bool                           autoStop;
    /// Property: Processing class (Default 1)
    int                            clazz;

    /// Property: Enable/disable PAUSE transition using incident    (RUNNING -> PAUSED)
    int                            enablePause;
    /// Property: Enable/disable CONTINUE transition using incident (READY -> RUNNING)
    int                            enableContinue;
    /// Property: Enable/disable STOP transition using incident     (RUNNING -> READY)
    int                            enableStop;
    /// Property: Enable/disable ERROR transition using incident    (* -> ERROR)
    int                            enableError;

    /// Property: Execution mode definition
    int                            execMode;
    /// Property: Numa node binding (Default: no binding)
    int                            numaNode;
    /// Property: Event IO Logger level
    int                            IO_output_level;
    /// Property: Maximum number of consecutive event processing errors
    long                           maxConsecutiveErrors;
    /// Property: Maximum number of parallel event threads
    size_t                         numEventThreads;
    /// Property: Maximum number of processing result threads
    size_t                         numStatusThreads;
    /// Property: Burst information printed every ... events
    size_t                         burstPrintCount;
    /// Property: Expand TAE frames, otherwise handle as normal bx (default true)
    bool                           expandTAE;
    /// Property: Enable data verification for Tell1 banks
    bool                           verifyBanks;

    /// Property: Enable monitoring: monitor service name/type or disabled if empty
    std::string                    monitorType;
    /// Property: Event data input type (MBM. NET, FILE)
    std::string                    inputType;
    /// Property: Call ApplicationMgr::run() (Default: false / 0)
    std::string                    runable;
    /// Property: Logger device type
    std::string                    logDeviceType;
    /// Property: Logger device output format
    std::string                    logDeviceFormat;
    /// Property: Logger device (if empty ==> environment ${LOGFIFO})
    std::string                    logDevice;
    /// Property: Type/Name of the event loop/flow manager
    std::string                    flowManager = "Online::OnlineFlowMgr/EventLoop";
    /// Property: Name of service providing max number of threads
    std::string                    numThreadSvcName {""};

    /// Properties for the MBM input data handling:
    MBMEventAccess::mbm_config_t   mbm;

    /// Properties for the MBM input data handling:
    NetEventAccess::net_config_t   net;

    /// Properties for the FILE input data handling:
    FileEventAccess::file_config_t file;

    struct generic_config_t : public EventAccess::config_t  {
      std::string  factory;
      /// Default constructor
      generic_config_t() = default;
      /// Move constructor
      generic_config_t(generic_config_t&& copy) = default;
      /// Copy constructor
      generic_config_t(const generic_config_t& copy) = default;
      /// Default destructor
      ~generic_config_t() = default;
      /// Assignment operator
      generic_config_t& operator=(generic_config_t&& copy) = default;
      /// Move assignment operator
      generic_config_t& operator=(const generic_config_t& copy) = default;
    } generic;

    /** Properties for the event queue handling:      */
    /// Property: Low water mark of pre-cached events
    size_t                         lowMark;
    /// Property: High water mark of pre-cached events
    size_t                         highMark;

    int outputLevel()  const  {  return this->Service::outputLevel();  }

  public:
    /// Specialized constructor
    Configuration(std::string name, ISvcLocator* svcloc);
    /// Default destructor
    virtual ~Configuration();
    /// Query interfaces of Interface
    virtual StatusCode queryInterface(const InterfaceID& riid, void** ppvInterface) override;
    /// Wait for debugger being attached
    void waitDebugger();
  };   // class Configuration
}      // namespace Online
#endif // GAUDIONLINE_CONFIGURATION_H
