//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//==========================================================================
#ifndef ONLINE_GAUDIONLINE_QUEUEDFLOWMANAGER_H
#define ONLINE_GAUDIONLINE_QUEUEDFLOWMANAGER_H

/// Framework include files
#include <GaudiKernel/SmartIF.h>
#include <GaudiKernel/EventContext.h>
#include <GaudiKernel/IHiveWhiteBoard.h>
#include <GaudiKernel/ThreadLocalContext.h>
#include <Gaudi/Interfaces/IQueueingEventProcessor.h>

#include <mutex>
#include <optional>
#include <condition_variable>
#include <tbb/concurrent_queue.h>

/// Online namespace declaration
namespace Online   {

  class QueuedFlowManager : virtual public Gaudi::Interfaces::IQueueingEventProcessor    {

  public:
    /// Standard Constructor
    QueuedFlowManager() = default;
    virtual ~QueuedFlowManager() = default;

    /// Pass reference to white-board
    StatusCode start(SmartIF<IHiveWhiteBoard> wb);

    ///=================================================================
    /// Process one single event synchronously (1 per thread)
    virtual std::tuple<StatusCode, EventContext> processEvent(EventContext&& evtContext) = 0;

    /// IQueueingEventProcessor override: Schedule the processing of an event.
    virtual void push( EventContext&& ctx )  override;
    /// IQueueingEventProcessor override: Tell if the processor has events in the queues.
    virtual bool empty() const  override;
    /// IQueueingEventProcessor override: Get the next available result.
    virtual std::optional<ResultType> pop()  override;

  public:

    /// Reference to the Whiteboard
    SmartIF<IHiveWhiteBoard>        m_whiteboard = nullptr;
    /// condition variable to wake up main thread when we need to create a new event
    mutable std::condition_variable m_createEventCond;
    /// mutex assoiciated with m_createEventCond condition variable
    std::mutex m_createEventMutex;

    /// TBB output queue
    tbb::concurrent_bounded_queue<std::tuple<StatusCode, EventContext>> m_done;
    /// Counter for the number of events in flight
    std::atomic<std::size_t>  m_inFlight{0};
    /// Event counter
    size_t  m_nextevt = 0;
  };
}
#endif // ONLINE_GAUDIONLINE_QUEUEDFLOWMANAGER_H
