//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : N.Nolte
//  Adapted for online use by M.Frank
//==========================================================================

// The following MUST be included before GaudiKernel/Parsers.h,
// which means very early on in the compilation unit.
#include "CFNodePropertiesParse.h"
// FW includes
#include "GaudiAlg/FunctionalDetails.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/DataSvc.h"
#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IAlgExecStateSvc.h"
#include "GaudiKernel/IAlgorithm.h"
#include "GaudiKernel/IDataBroker.h"
#include "GaudiKernel/IEventProcessor.h"
#include "GaudiKernel/IHiveWhiteBoard.h"
#include "GaudiKernel/Memory.h"
#include "GaudiKernel/ThreadLocalContext.h"
#include <Gaudi/Interfaces/IQueueingEventProcessor.h>

#include <algorithm>
#include <chrono>
#include <condition_variable>
#include <deque>
#include <fstream>
#include <iomanip>
#include <map>
#include <memory>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

// tbb
#include <tbb/concurrent_queue.h>

// locals
#include "ControlFlowNode.h"
#include "ISchedulerConfiguration.h"

namespace Online {

  class AlgFlowManager final
      : public extends<Service, Gaudi::Interfaces::IQueueingEventProcessor, LHCb::Interfaces::ISchedulerConfiguration> {

  public:
    /// Standard Constructor
    using extends::extends;

    /// IEventProcessor override: Implementation of IService::initialize
    StatusCode initialize() override;
    /// IEventProcessor override: Implementation of IService::reinitialize
    StatusCode reinitialize() override { return StatusCode::FAILURE; }
    /// IEventProcessor override: Implementation of IService::finalize
    StatusCode finalize() override;
    /// IEventProcessor override: Implementation of IService::start
    StatusCode start() override;

    /// IEventProcessor override: Implementation of IEventProcessor::nextEvent
    StatusCode nextEvent( int ) override; //  { return StatusCode::FAILURE; }
    /// IEventProcessor override: Implementation of IEventProcessor::executeEvent(void* par)
    StatusCode executeEvent( EventContext&& evtContext ) override;
    /// IEventProcessor override: Implementation of IEventProcessor::executeRun()
    StatusCode executeRun( int ) override { return StatusCode::FAILURE; }
    /// IEventProcessor override: Implementation of IEventProcessor::stopRun()
    StatusCode stopRun() override;
    /// IEventProcessor override: Implementation of IEventProcessor::createEventContext
    EventContext createEventContext() override;

    ///=================================================================
    /// Process one single event synchronously (1 per thread)
    int processEvent( EventContext& evtContext );

    /// IQueueingEventProcessor override: Schedule the processing of an event.
    virtual void push( EventContext&& ctx ) override;

    /// IQueueingEventProcessor override: Tell if the processor has events in the queues.
    virtual bool empty() const override;

    /// IQueueingEventProcessor override: Get the next available result.
    virtual std::optional<ResultType> pop() override;

    tbb::concurrent_bounded_queue<std::tuple<StatusCode, EventContext>> m_done;
    std::mutex                                                          m_resultLock;
    std::list<ResultType>                                               m_result;
    std::atomic<std::size_t>                                            m_inFlight{0};

  private:
    void buildLines();
    // configuring the execution order
    void configureScheduling();
    // build per-thread state-vector
    void buildNodeStates();

  public:
    size_t                                       m_nextevt = 0;
    Gaudi::Property<std::vector<NodeDefinition>> m_compositeCFProperties{
        this, "CompositeCFNodes", {}, "Specification of composite CF nodes"};
    Gaudi::Property<std::vector<std::string>> m_BarrierAlgNames{
        this, "BarrierAlgNames", {}, "Names of Barrier (Gather) Algorithms"};
    Gaudi::Property<std::vector<std::vector<std::string>>> m_userDefinedEdges{
        this, "AdditionalCFEdges", {}, "Additional Control Flow Edges defined by the User \
                                      (format: [ [before, after], [before2, after2] ])"};
    Gaudi::Property<std::set<std::string>> m_definitlyRunThese{
        this, "AdditionalAlgs", {}, "Add algs that do not participate in the control flow but should\
                                   definitly run, like e.g. a callgrindprofile"};

    /// Reference to the Event Data Service's IDataManagerSvc interface
    SmartIF<IDataManagerSvc> m_evtDataMgrSvc = nullptr;
    /// Reference to the Whiteboard
    SmartIF<IHiveWhiteBoard> m_whiteboard = nullptr;
    /// Reference to the AlgExecStateSvc
    SmartIF<IAlgExecStateSvc> m_algExecStateSvc = nullptr;
    // the used databroker
    SmartIF<IDataBroker> m_databroker = nullptr;

    /// atomic count of the number of finished events
    mutable std::atomic<uint64_t> m_finishedEvt{0};
    /// condition variable to wake up main thread when we need to create a new event
    mutable std::condition_variable m_createEventCond;
    /// mutex assoiciated with m_createEventCond condition variable
    std::mutex m_createEventMutex;

    std::deque<Gaudi::Accumulators::Counter<>>                 m_AlgExecCounters;
    std::deque<Gaudi::Accumulators::BinomialCounter<uint64_t>> m_NodeStateCounters;

  public:
    // state vectors for each event, once filled, then copied per event
    std::vector<NodeState> m_NodeStates;
    std::vector<AlgState>  m_AlgStates;
    using SchedulerStates = std::pair<std::vector<NodeState>, std::vector<AlgState>>;

  public:
    // all controlflownodes
    std::map<std::string, VNode> m_allVNodes;
    // all nodes to execute in ordered manner
    std::vector<gsl::not_null<BasicNode*>> m_orderedNodesVec;
    // highest node
    VNode* m_motherOfAllNodes = nullptr;

    std::vector<AlgWrapper> m_definitelyRunTheseAlgs;

    // for printing
    // printable dependency tree (will be built during initialize
    std::vector<std::string> m_printableDependencyTree;
    std::vector<std::string> m_AlgNames;
    // map order of print to order of m_NodeStates and m_allVNodes
    std::vector<int> m_mapPrintToNodeStateOrder;
    // maximum width of the dependencytree
    int m_maxTreeWidth;

    // functions to create m_printableDependencyTree
    void registerStructuredTree();
    void registerTreePrintWidth();
    // runtime adding of states to print tree and states
    template <typename Printable>
    std::stringstream buildPrintableStateTree( LHCb::span<Printable const> states ) const;

  public:
    // to be able to check which states belong to which node (from the outside)
    std::map<std::string, int> getNodeNamesWithIndices() const override {
      std::map<std::string, int> names_indices;
      std::transform( m_allVNodes.begin(), m_allVNodes.end(), std::inserter( names_indices, names_indices.end() ),
                      []( const auto& p ) {
                        return std::visit( []( const auto& n ) { return std::pair{n.m_name, n.m_NodeID}; }, p.second );
                      } );
      return names_indices;
    }

    std::stringstream
    buildPrintableStateTree( LHCb::Interfaces::ISchedulerConfiguration::State const& state ) const override {
      return buildPrintableStateTree( state.nodes() );
    }

    std::stringstream buildAlgsWithStates( LHCb::Interfaces::ISchedulerConfiguration::State const& ) const override;

    // template <typename printable>
    //   std::stringstream buildPrintableStateTree( std::vector<printable> const& states ) const;
    // std::stringstream buildAlgsWithStates( std::vector<AlgState> const& states ) const;
  };
} // namespace Online
