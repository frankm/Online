//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

// Include files from Gaudi
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <Tell1Data/Tell1Decoder.h>

// C/C++ include files
#include <atomic>

/// Online namespace declaration
namespace Online  {

/** @class Tell1BankDump Tell1BankDump.cpp
  *  Creates and fills dummy RawEvent
  *
  *  @author Markus Frank
  *  @date   2005-10-13
  */
  class Tell1BankDump : public Gaudi::Algorithm {

    typedef Tell1Bank BankHeader;
    typedef std::vector<std::pair<const BankHeader*, const void*> > evt_data_t;

    Gaudi::Property<bool>   m_full{  this, "FullDump",  false, "FullDump:  If true, full bank contents are dumped"};
    Gaudi::Property<bool>   m_dump{  this, "DumpData",  false, "DumpData:  If true, full bank contents are dumped"};
    Gaudi::Property<bool>   m_check{ this, "CheckData", false, "CheckData: If true, full bank contents are checked"};
    Gaudi::Property<int>    m_debug{ this, "Debug",     0,     "Number of events where all dump flags should be considered true"};
    DataObjectReadHandle<evt_data_t>  m_rawData{this, "RawData", "DAQ/RawData"};
    mutable std::atomic<long> m_numEvent;             ///< Event counter

  public:
    using Algorithm::Algorithm;

    /// Algorithm initialization
    StatusCode start() override  {
      m_numEvent = 0;
      return StatusCode::SUCCESS;
    }

    /// Main execution callback
    StatusCode execute(EventContext const& /* ctxt */) const override  {
      MsgStream info(msgSvc(),name());
      const auto* raw = m_rawData.get();
      std::map<int, std::vector<const Tell1Bank*> > banks;
      bool dmp = m_numEvent<m_debug || m_dump;
      bool chk = m_numEvent<m_debug || m_check;
      bool ful = m_numEvent<m_debug || m_full;
      static int evt = 0;
      ++evt;
      info << MSG::INFO;
      for ( const auto& dsc : *raw )   {
	banks[dsc.first->type()].push_back(dsc.first);
      }
      for ( const auto& dsc : banks )   {
        const auto& b = dsc.second;
	int btyp = dsc.first;
        int cnt, inc = (btyp == Tell1Bank::Rich) ? 64 : 32;
        const int *p;
        if ( b.size() > 0 )  {
          if ( dmp )  {
            info << "Evt No:" << std::left << std::setw(6) << evt
                 << " has " << b.size() << " bank(s) of type " << btyp
                 << " (" << Tell1Printout::bankType(btyp) << ") " << endmsg;
          }
          int k = 0;
          for(const auto* itB : b)  {
            const Tell1Bank* r = (Tell1Bank*)itB;
            if ( dmp )   {
              info << "Bank:  [" << Tell1Printout::bankHeader(r) << "] " << endmsg;
            }
            if( ful ) {
              cnt = 0;
              std::stringstream s;
              for(p=r->begin<int>(); p != r->end<int>(); ++p)  {
                s << std::hex << std::setw(8) << std::hex << *p << " ";
                if ( ++cnt == 10 ) {
                  info << "  Data:" << s.str() << endmsg;
                  s.str("");
                  cnt = 0;
                }
              }
              if ( cnt > 0 ) info << "  Data:" << s.str() << endmsg;
            }
            if( chk ) { // Check the patterns put in by RawEventCreator
              int kc = k;
              int ks = k+1;
              if ( r->type() != Tell1Bank::DAQ )  {
                if ( r->size() != inc*ks )  {
                  info << "Bad bank size:" << r->size() << " expected:" << ks*inc << endmsg;
                }
                if ( r->sourceID() != kc )  {
                  info << "Bad source ID:" << r->sourceID() << " expected:" << kc << endmsg;
                }
                for(p=r->begin<int>(), cnt=0; p != r->end<int>(); ++p, ++cnt)  {
                  if ( *p != cnt )  {
                    info << "Bad BANK DATA:" << *p << endmsg;
                  }
                }
                if ( cnt != (inc*ks)/int(sizeof(int)) )  {
                  info << "Bad amount of data in bank:" << cnt << " word" << endmsg;
                }
              }
            }
          }
        }
      }
      m_numEvent++;
      return StatusCode::SUCCESS;
    }
  };
}

DECLARE_COMPONENT( Online::Tell1BankDump )
