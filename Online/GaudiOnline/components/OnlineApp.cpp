//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <GaudiOnline/OnlineApplication.h>

/// Easier command line: only supply --application=OnlineEvents
class OnlineApp : public Online::OnlineApplication  {
public:
  using OnlineApplication::OnlineApplication;
  virtual ~OnlineApp() = default;
};

using namespace Online;

DECLARE_COMPONENT( OnlineApp )
DECLARE_COMPONENT( OnlineApplication )
