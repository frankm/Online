//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

// Include files from Gaudi
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <EventHandling/EventAccess.h>
#include <Tell1Data/Tell1Decoder.h>

// C/C++ include files
#include <atomic>

/// Online namespace declaration
namespace Online  {

/** @class Tell1HeaderDump Tell1HeaderDump.cpp
  *  Creates and fills dummy RawEvent
  *
  *  @author Markus Frank
  *  @date   2005-10-13
  */
  class Tell1HeaderDump : public Gaudi::Algorithm {

    typedef Tell1Bank BankHeader;
    typedef std::pair<size_t, EventAccess::shared_guard_t> evt_desc_t;

    Gaudi::Property<bool>   m_full{  this, "FullDump",  false, "FullDump:  If true, full bank contents are dumped"};
    Gaudi::Property<bool>   m_dump{  this, "DumpData",  false, "DumpData:  If true, full bank contents are dumped"};
    Gaudi::Property<bool>   m_check{ this, "CheckData", false, "CheckData: If true, full bank contents are checked"};
    Gaudi::Property<int>    m_debug{ this, "Debug",     0,     "Number of events where all dump flags should be considered true"};
    DataObjectReadHandle<evt_desc_t>  m_raw{this, "RawGuard", "Banks/RawDataGuard"};
    mutable std::atomic<long> m_numEvent;             ///< Event counter

  public:
    using Algorithm::Algorithm;

    /// Algorithm initialization
    StatusCode start() override  {
      m_numEvent = 0;
      return StatusCode::SUCCESS;
    }

    /// Main execution callback
    StatusCode execute(EventContext const& /* ctxt */) const override  {
      MsgStream info(msgSvc(),name());
      const auto* e = m_raw.get();
      if ( e->second )    {
	char text[256];
	info << MSG::INFO;
	if ( e->second->type() == event_traits::tell40::data_type )   {
	  const auto& evt = e->second->at(e->first).tell40_event;
	  info << "+-- PCIE40 event: " << e->first << " @ " << (void*)&evt << endmsg;
	}
	else if ( e->second->type() == event_traits::tell1::data_type )   {
	  const auto& evt = e->second->at(e->first).tell1_event;
	  info << "+-- TELL1  event: " << e->first << " @ " << (void*)&evt << endmsg;
	  ::snprintf(text, sizeof(text),"| Size: %5d %5d %5d  Checksum: 0x%08X compression: %3d Vsn: %3d",
		     evt->size0(), evt->size1(), evt->size2(), evt->checkSum(), evt->compression(), evt->headerVersion());
	  info << text << endmsg;
	  if ( evt->headerVersion() == 3 )   {
	    const auto* h = evt->subHeader().H1;
	    const auto& m = h->triggerMask();
	    ::snprintf(text, sizeof(text),"| Run: %6d  Orbit: 0x%08X Bunch: 0x%08X Mask: %08X %08X %08X %08X",
		       h->runNumber(), h->orbitNumber(), h->bunchID(), m[0], m[1], m[2], m[3]);
	    info << text << endmsg;
	  }
	}
	else  {
	  info << "Unknwon Event type: " << e->second->type() << endmsg;
	}
      }
      m_numEvent++;
      return StatusCode::SUCCESS;
    }
  };
}

DECLARE_COMPONENT( Online::Tell1HeaderDump )
