//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "IOService.h"
#include <GaudiKernel/MsgStream.h>

using namespace Online;

/// Factory instantiation
DECLARE_COMPONENT( IOService )

/// Check if IO was cancelled (should be unlikely)
bool IOService::isCancelled()  const   {
  return access ? access->isCancelled() : true;
}

/// Query interfaces of Interface
StatusCode IOService::queryInterface(const InterfaceID& riid, void** ppvInterface)
{
  if ( interfaceID().versionMatch(riid) )   {
    *ppvInterface = this;
    addRef();
    return StatusCode::SUCCESS;
  }
  return Service::queryInterface(riid,ppvInterface);
}

/// Push rawevent
size_t IOService::push(EventAccess::shared_guard_t&& burst)   {
  if ( burst )   {
    size_t len = burst->num_frame();
    std::lock_guard<std::mutex> lock(event_queue_lock);
    burst_queue.emplace_back(move(burst));
    return len;
  }
  return 0;
}

/// Pop event data
EventAccess::event_t IOService::pop()   {
  std::lock_guard<std::mutex> lock(event_queue_lock);
  bool empty = burst_queue.empty();
  if ( !empty )  {
    auto e (access->dequeueEvent(burst_queue.front()));
    if ( !burst_queue.front() )  {
      burst_queue.pop_front();
    }
    return e;
  }
  MsgStream err(msgSvc(), name());
  err << MSG::ERROR << "+++ Dequeue event FAILURE. Burst queue empty: "
      << (empty ? "YES" : "NO")
      << endmsg;
  return EventAccess::event_t(0, EventAccess::shared_guard_t());
}
