//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

#include "FlowManager.h"
#include <GaudiKernel/AppReturnCode.h>

#include <thread>
#include <gsl/pointers>
#include <tbb/task_arena.h>

using namespace std;
using namespace Online;

DECLARE_COMPONENT( FlowManager )

namespace {

  /// Helper class to set the application return code in case of early exit
  /// (e.g. exception). Copied from GaudiCoreSvc
  class RetCodeGuard {
  public:
    inline RetCodeGuard( SmartIF<IProperty> appmgr, int retcode )
        : m_appmgr( std::move( appmgr ) ), m_retcode( retcode ) {}
    inline void ignore() { m_retcode = Gaudi::ReturnCode::Success; }
    inline ~RetCodeGuard() {
      if ( Gaudi::ReturnCode::Success != m_retcode ) { Gaudi::setAppReturnCode( m_appmgr, m_retcode ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); }
    }

  private:
    SmartIF<IProperty> m_appmgr;
    int                m_retcode;
  };

  template <typename fun>
  class Wrapper final : public tbb::task {
    fun m_f;

  public:
    Wrapper( fun f ) : m_f( std::move( f ) ) {}
    tbb::task* execute() override {
      m_f();
      return nullptr;
    }
  };

  template <typename fun> void enqueue( fun&& f ) {
    tbb::task* task = new ( tbb::task::allocate_root() ) Wrapper{std::forward<fun>( f )};
    tbb::task::enqueue( *task );
  }
} // namespace

/// MinimalEventLoopMgr override: Implementation of IService::initialize
StatusCode FlowManager::initialize() {
  StatusCode sc = MinimalEventLoopMgr::initialize();
  if ( !sc.isSuccess() ) {
    error() << "Failed to initialize Service Base class." << endmsg;
    return StatusCode::FAILURE;
  }
  /// Setup access to event data services
  m_evtDataMgrSvc = service<IDataManagerSvc>( "EventDataSvc" );
  if ( !m_evtDataMgrSvc ) {
    fatal() << "Error retrieving EventDataSvc interface IDataManagerSvc." << endmsg;
    return StatusCode::FAILURE;
  }
  return sc;
}

/// MinimalEventLoopMgr override: Implementation of IService::finalize
StatusCode FlowManager::finalize() {
  m_evtDataMgrSvc.reset();
  return MinimalEventLoopMgr::finalize();
}

/// MinimalEventLoopMgr override: Implementation of IService::start
StatusCode FlowManager::start() {
  StatusCode sc = MinimalEventLoopMgr::start();
  QueuedFlowManager::start(m_WB).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  return sc;
}

/// MinimalEventLoopMgr override: Implementation of IService::stop
StatusCode FlowManager::stop() {
  StatusCode sc = MinimalEventLoopMgr::stop();
  return sc;
}

/// MinimalEventLoopMgr override: Implementation of IEventProcessor::createEventContext
EventContext FlowManager::createEventContext()   {
  return {m_nextevt++,0};
}

/// MinimalEventLoopMgr override: Implementation of IEventProcessor::executeEvent(...)
StatusCode FlowManager::executeEvent( EventContext&& ctx )   {
  auto ret = processEvent(move(ctx));
  return get<0>(move(ret));
}

std::tuple<StatusCode, EventContext> FlowManager::processEvent( EventContext&& context ) {
  bool eventfailed = false;
  int  slot = context.slot();
  m_aess->reset( context );
  Gaudi::Hive::setCurrentContext( context );

  // select the appropriate store
  if ( m_WB.isValid() ) m_WB->selectStore( slot ).ignore();

  // set event root
  StatusCode sc = m_evtDataMgrSvc->setRoot( "/Event", new DataObject() );
  if ( !sc.isSuccess() ) error() << "Error declaring event root DataObject" << endmsg;

  // Get the IProperty interface of the ApplicationMgr to pass it to RetCodeGuard
  const auto appmgr = serviceLocator()->as<IProperty>();
  // Call the execute() method of all top algorithms
  for ( auto& ita : m_topAlgList ) {
    sc = StatusCode::FAILURE;
    try {
      if ( m_abortEventListener.abortEvent ) {
        debug() << "AbortEvent incident fired by " << m_abortEventListener.abortEventSource << endmsg;
        m_abortEventListener.abortEvent = false;
        sc.ignore();
        break;
      }
      RetCodeGuard rcg( appmgr, Gaudi::ReturnCode::UnhandledException );
      sc = ita->sysExecute( context );
      rcg.ignore(); // disarm the guard
    } catch ( const GaudiException& Exception ) {
      fatal() << ".executeEvent(): Exception with tag=" << Exception.tag() << " thrown by " << ita->name() << endmsg;
      error() << Exception << endmsg;
    } catch ( const std::exception& Exception ) {
      fatal() << ".executeEvent(): Standard std::exception thrown by " << ita->name() << endmsg;
      error() << Exception.what() << endmsg;
    } catch ( ... ) { fatal() << ".executeEvent(): UNKNOWN Exception thrown by " << ita->name() << endmsg; }

    if ( !sc.isSuccess() ) {
      warning() << "Execution of algorithm " << ita->name() << " failed" << endmsg;
      eventfailed = true;
    }
  }

  m_aess->updateEventStatus( eventfailed, context );

  // ensure that the abortEvent flag is cleared before the next event
  if ( m_abortEventListener.abortEvent ) {
    debug() << "AbortEvent incident fired by " << m_abortEventListener.abortEventSource << endmsg;
    m_abortEventListener.abortEvent = false;
  }

  // Call the execute() method of all output streams
  for ( auto& ito : m_outStreamList ) {
    AlgExecState& state = m_aess->algExecState( ito, context );
    state.setFilterPassed( true );
    StatusCode sc = ito->sysExecute( context );
    if ( !sc.isSuccess() ) {
      warning() << "Execution of output stream " << ito->name() << " failed" << endmsg;
      eventfailed = true;
    }
  }

  StatusCode outcome{StatusCode::SUCCESS};
  // Check if there was an error processing current event
  if ( eventfailed ) {
    error() << "Error processing event loop." << endmsg;
    std::ostringstream ost;
    m_aess->dump( ost, context );
    debug() << "Dumping AlgExecStateSvc status:\n" << ost.str() << endmsg;
    outcome = StatusCode{StatusCode::FAILURE};
  }
  sc = m_WB->clearStore( slot );
  if ( !sc.isSuccess() ) warning() << "Clear of Event data store failed" << endmsg;
  sc = m_WB->freeStore( slot );
  if ( !sc.isSuccess() ) error() << "Whiteboard slot " << slot << " could not be properly cleared" << endmsg;
  return {std::move( outcome ), std::move( context )};
}
