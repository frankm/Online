//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "InputAlg.h"
#include <PCIE40Data/sodin.h>
#include <Tell1Data/RunInfo.h>
#include <Gaucho/IGauchoMonitorSvc.h>

// C/C++ include files
#include <stdexcept>

/// Factory instantiation
DECLARE_COMPONENT( Online::InputAlg )

class Online::InputAlg::Counters  {
public:
  /// Counters to detect and publish error conditions in Tell40 banks
  std::atomic<long> daqErrorTOTAL;
  std::atomic<long> daqErrorFragmentThrottled;
  std::atomic<long> daqErrorBXIDCorrupted;
  std::atomic<long> daqErrorSyncBXIDCurrupted;
  std::atomic<long> daqErrorFragmentMissing;
  std::atomic<long> daqErrorFragmentTruncated;
  std::atomic<long> daqErrorIdleBXIDCorrupted;
  std::atomic<long> daqErrorFragmentMalformed; // 95
  std::atomic<long> daqErrorEVIDJumped;        // 96
  std::atomic<long> daqCounters_NZS_mode;
  std::atomic<long> daqCounters_have_ODIN;
  std::atomic<long> daqCounters_no_ODIN;
  std::atomic<long> daqCounters_TAE;
  std::atomic<long> daqCounters_TAE_CENTRAL;
  Counters() = default;
  ~Counters() = default;
  void zero()   {
    this->daqErrorFragmentThrottled = 0;
    this->daqErrorBXIDCorrupted = 0;
    this->daqErrorSyncBXIDCurrupted = 0;
    this->daqErrorFragmentMissing = 0;
    this->daqErrorFragmentTruncated = 0;
    this->daqErrorIdleBXIDCorrupted = 0;
    this->daqErrorFragmentMalformed = 0; // 95
    this->daqErrorEVIDJumped = 0;        // 96

    this->daqCounters_NZS_mode = 0;
    this->daqCounters_have_ODIN = 0;
    this->daqCounters_no_ODIN = 0;
    this->daqCounters_TAE = 0;
    this->daqCounters_TAE_CENTRAL = 0;
  }
  void declare(SmartIF<IMonitorSvc>& monitor, InputAlg* owner)   {
    SmartIF<IGauchoMonitorSvc> m(monitor);
    if ( m )   {
      m->declareInfo("daqErrorTOTAL",             daqErrorTOTAL,             "DAQ Errors: daqErrorTOTAL", owner);
      m->declareInfo("daqErrorBXIDCorrupted",     daqErrorBXIDCorrupted,     "DAQ Errors: daqErrorBXIDCorrupted", owner);
      m->declareInfo("daqErrorFragmentThrottled", daqErrorFragmentThrottled, "DAQ Errors: daqErrorFragmentThrottled", owner);
      m->declareInfo("daqErrorSyncBXIDCurrupted", daqErrorSyncBXIDCurrupted, "DAQ Errors: daqErrorSyncBXIDCurrupted", owner);
      m->declareInfo("daqErrorFragmentMissing",   daqErrorFragmentMissing,   "DAQ Errors: daqErrorFragmentMissing", owner);
      m->declareInfo("daqErrorFragmentTruncated", daqErrorFragmentTruncated, "DAQ Errors: daqErrorFragmentTruncated", owner);
      m->declareInfo("daqErrorBXIDCorrupted",     daqErrorBXIDCorrupted,     "DAQ Errors: daqErrorBXIDCorrupted", owner);
      m->declareInfo("daqErrorIdleBXIDCorrupted", daqErrorIdleBXIDCorrupted, "DAQ Errors: daqErrorIdleBXIDCorrupted", owner);
      m->declareInfo("daqErrorFragmentMalformed", daqErrorFragmentMalformed, "DAQ Errors: daqErrorFragmentMalformed", owner);
      m->declareInfo("daqErrorEVIDJumped",        daqErrorEVIDJumped,        "DAQ Errors: daqErrorEVIDJumped", owner);

      m->declareInfo("daqCounters_NZS_mode",      daqCounters_NZS_mode,      "DAQ Counter: daqCounters_NZS_mode", owner);
      m->declareInfo("daqCounters_have_ODIN",     daqCounters_have_ODIN,     "DAQ Counter: daqCounters_have_ODIN", owner);
      m->declareInfo("daqCounters_no_ODIN",       daqCounters_no_ODIN,       "DAQ Counter: daqCounters_no_ODIN", owner);
      m->declareInfo("daqCounters_TAE",           daqCounters_TAE,           "DAQ Counter: daqCounters_TAE", owner);
      m->declareInfo("daqCounters_TAE_CENTRAL",   daqCounters_TAE_CENTRAL,   "DAQ Counter: daqCounters_TAE_CENTRAL", owner);
    }
  }
};

/// Halt the algorithm and allow for debugging
void Online::InputAlg::halt()   const    {
  if ( m_enableHalt )   {
    m_halt = 1;
    m_logger->warning("+++ Algorithm halted by input data handling problem.");
    while ( m_halt ) ::lib_rtl_sleep(100);
  }
}

StatusCode Online::InputAlg::expand_tae_tell1(const EventHeader* hdr)  const   {
  if ( hdr && hdr->is_mdf() )   {
    const auto* tae = (const Tell1Bank*)hdr->data();
    if ( tae->magic() == Tell1Bank::MagicPattern &&
	 tae->type()  == Tell1Bank::TAEHeader )   {
      const int* data  = tae->begin<int>();
      int    count_bx  = tae->size()/sizeof(int)/3;
      int    half_win  = (count_bx-1)/2;
      std::string pref_prev = "/Event/Prev", pref_next = "/Event/Next";
      const uint8_t* start = ((const uint8_t*)tae)+tae->totalSize();

      for(int i=-half_win; i<=half_win; ++i)   {
	int idx = i + half_win;
	int bx  = data[3*idx];
	int off = data[3*idx+1];
	int len = data[3*idx+2];
	if ( bx < 0 )
	  m_evtTool->put(pref_prev + _bx_offset(bx) + "/RawEvent", start+off, start+off+len).ignore();
	else if ( bx > 0 )
	  m_evtTool->put(pref_next + _bx_offset(bx) + "/RawEvent", start+off, start+off+len).ignore();
      }
      return StatusCode::SUCCESS;
    }
    m_logger->except("expand_tae: Invalid TAE bank [Invalid magic pattern] %04X", tae->magic());
    return StatusCode::FAILURE;
  }
  m_logger->except("expand_tae: Invalid Tell1 Eventheader found.");
  return StatusCode::FAILURE;
}

/// Initialize the algorithm
StatusCode Online::InputAlg::initialize()   {
  StatusCode sc = EventProcessor::initialize();
  if ( sc.isSuccess() )   {
    m_io = service<IOService>(m_ioService);
    if ( !m_io )   {
      return StatusCode::FAILURE;
    }
    IDataProviderSvc* svc = eventSvc();
    std::string tool_type = "Online__RawEventRegister";
    m_evtTool = IRawEventCreator::Factory::create(tool_type, svc);
    if ( !m_evtTool.get() )   {
      m_logger->except("Failed to access RawEvent creation tool!");
    }
    m_counters = std::make_unique<Counters>();
    /// Handle to monitor service
    m_monitor = monitorSvc();
    m_counters->zero();
    m_counters->declare(m_monitor, this);
  }
  return sc;
}

/// Finalize the algorithm
StatusCode Online::InputAlg::finalize()   {
  m_io.reset();
  m_evtTool.reset();
  if ( m_monitor )   {
    m_monitor->undeclareAll(this);
    m_monitor.reset();
  }
  m_counters.reset();
  return EventProcessor::finalize();
}
#include <Gaucho/IGauchoMonitorSvc.h>

/// Start the algorithm
StatusCode Online::InputAlg::start()   {
  return this->EventProcessor::start();
}

/// Stop the algorithm
StatusCode Online::InputAlg::stop()   {
  m_halt = 0;
  return this->EventProcessor::stop();
}

/// Execute single event
StatusCode Online::InputAlg::process(EventContext const& /* ctxt */)  const   {
  using namespace event_traits;
  typedef std::vector<void*> lb_banks_t;

  evt_desc_t  e(m_io->pop());
  if ( e.second )   {
    try  {
      bool declare_banks = m_declareEvt.value();
      bool make_raw_evt  = m_makeRawEvt.value();
      bool expand_tae    = m_io->expandTAE;
      bool save_guard    = declare_banks || make_raw_evt || expand_tae;

      if ( e.second->type() == tell40::data_type )   {
	const auto& evt = e.second->at(e.first).tell40_event;
	if ( evt->nzs_mode() )        	++m_counters->daqCounters_NZS_mode;
	if ( evt->have_odin() )     	++m_counters->daqCounters_have_ODIN;
	else                      	++m_counters->daqCounters_no_ODIN;
	if ( evt->is_tae() )            ++m_counters->daqCounters_TAE;
	if ( evt->is_tae_central() )	++m_counters->daqCounters_TAE_CENTRAL;
      }

      /// Now declare the event data if requested:
      if ( declare_banks || make_raw_evt )   {
	auto event = this->create_event_data(e, 0);
	if ( event.first != NO_MDF_ERROR )   {
	  return StatusCode::FAILURE;
	}
	m_rawData.put(reinterpret_cast<RawBanks&&>(std::move(event.second)));

	/// Invoke here the conversion to raw events
	if ( make_raw_evt )   {
	  const auto* pev = &event.second;
	  StatusCode sc = m_evtTool->put("/Event/DAQ/RawEvent", *(const lb_banks_t*)pev);
	  if ( !sc.isSuccess() )   {
	    m_logger->error("Failed to register RawEvent of main crossing at %s.","/Event/DAQ/RawEvent");
	    return StatusCode::FAILURE;
	  }
	}
	/// Expand TAE data if requested and present
	if ( expand_tae )   {
	  auto [half_window, central] = this->check_tae(e);
	  if ( central && half_window > 0 )   {
	    if ( make_raw_evt )   {
	      std::size_t eid = e.first;
	      const auto* hdr = e.second->at(eid).tell1_event;
	      StatusCode sc = this->expand_tae_tell1(hdr);
	      if ( !sc.isSuccess() )   {
	      }
	    }
	    /// Declare the TAE banks to the TES
	    if ( declare_banks )   {
	      this->expand_tae(m_bankDir, e);
	    }
	  }
	}
      }
      /// Declare error banks if required
      if ( e.second->type() == tell40::data_type && m_declareErrs.value() )   {
	auto errors = get_errors(e);
	for(const auto& err : errors )   {
	  ++m_counters->daqErrorTOTAL;
	  switch(err.first->type())   {
	  case RawBank40::DaqErrorFragmentThrottled:
	    ++m_counters->daqErrorFragmentThrottled;
	    break;
	  case RawBank40::DaqErrorBXIDCorrupted:
	    ++m_counters->daqErrorBXIDCorrupted;
	    break;
	  case RawBank40::DaqErrorSyncBXIDCurrupted:
	    ++m_counters->daqErrorSyncBXIDCurrupted;
	    break;
	  case RawBank40::DaqErrorFragmentMissing:
	    ++m_counters->daqErrorFragmentMissing;
	    break;
	  case RawBank40::DaqErrorFragmentTruncated:
	    ++m_counters->daqErrorFragmentTruncated;
	    break;
	  case RawBank40::DaqErrorIdleBXIDCorrupted:
	    ++m_counters->daqErrorIdleBXIDCorrupted;
	    break;
	  case RawBank40::DaqErrorFragmentMalformed:
	    ++m_counters->daqErrorFragmentMalformed;
	    break;
	  case RawBank40::DaqErrorEVIDJumped:
	    ++m_counters->daqErrorEVIDJumped;
	    break;
	  default:
	    break;
	  }
	}
	m_daqError.put(std::move(errors));
      }
      if ( save_guard || m_declareData.value() || m_declareErrs.value() )   {
	m_rawGuard.put(move(e));
      }
    }
    catch(const std::exception& e)    {
      m_logger->error("+++ Algorithm failed [%s].",e.what());
      return StatusCode::FAILURE;
    }
    catch(...)    {
      m_logger->error("+++ Algorithm failed [Unknown exception].");
      return StatusCode::FAILURE;
    }
    return StatusCode::SUCCESS;
  }
  else if ( m_io->isCancelled() )  {
    m_logger->error("+++ Algorithm failed [Invalid event after CANCEL].");
    return StatusCode::FAILURE;
  }
  m_logger->error("+++ Algorithm failed [Invalid data].");
  return StatusCode::FAILURE;
}
