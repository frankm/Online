//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  RawFile.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <Tell1Data/Tell1Decoder.h>
#include <Tell1Data/RawFile.h>
#include <RTL/Logger.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <system_error>
#include <filesystem>
#include <iostream>
#include <iomanip>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <cstring>
#include <climits>
#include <cerrno>
#ifdef _WIN32
#include <io.h>
#else
#include <unistd.h>
#define O_BINARY 0
#endif

#define HEAP_MEMORY_MAPPED (-100)

using namespace Online;
using namespace std;

/// Standard constructor
RawFile::RawFile(const string& fname) : m_name(fname)  {
}

/// Standard constructor
RawFile::RawFile(int descriptor) : m_name()  {
  char path[128], filePath[PATH_MAX];
  ::snprintf(path, sizeof(path),"/proc/self/fd/%d",descriptor);
  if ( ::readlink(path, filePath, sizeof(filePath)) == -1 )   {
    return;
  }
  m_fd = descriptor;
  m_name = filePath;
}

/// Standard constructor
RawFile::RawFile(int descriptor, bool) : m_name()  {
  m_fd = descriptor;
}

/// Return file position
long RawFile::seek(long offset, int whence)    {
  if ( isOpen() && !isMapped() )  {
    off64_t file_position = ::lseek64(m_fd, offset, whence);
    return file_position;
  }
  else if ( m_begin )  {
    unsigned char* ptr = m_ptr;
    switch(whence)  {
    case SEEK_SET:
      ptr = m_begin;
      break;
    case SEEK_END:
      ptr = m_end;
      break;
    case SEEK_CUR:
      break;
    default:
      errno = EINVAL;
      return -1;
    }
    ptr += offset;
    if ( ptr < m_begin || ptr > m_end )   {
      errno = EINVAL;
      return -1;
    }
    off64_t file_position = (m_ptr=ptr) - m_begin;
    return file_position;
  }
  return -1;
}

/// Return file size: either real file size of mapped size
std::size_t RawFile::data_size()   const   {
  if ( isOpen() && !isMapped() )  {
    struct stat sb;
    ::fstat(m_fd, &sb); /* To obtain file size */
    return sb.st_size;
  }
  else if ( m_begin )  {
    return m_end - m_begin;
  }
  return 0;
}

/// Return file position
long RawFile::position()  const    {
  if ( isOpen() && !isMapped() )  {
    off64_t file_position = ::lseek(m_fd, 0, SEEK_CUR);
    return file_position;
  }
  else if ( m_begin )  {
    off64_t file_position = m_ptr - m_begin;
    return file_position;
  }
  return -1;
}

/// Return file position
long RawFile::position(long offset, bool absolute)   {
  if ( isOpen() && !isMapped() )  {
    return this->seek(offset, absolute ? SEEK_SET : SEEK_CUR);
  }
  else if ( m_begin && offset > 0 )  {
    if ( absolute && offset >= 0 )  {
      m_ptr = m_begin + offset;
    }
    else if ( absolute )  {
      errno = EINVAL;
      return -1;
    }
    else   {
      m_ptr += offset;
      if ( m_ptr > m_end ) m_ptr = m_end;
    }
    return m_ptr - m_begin;
  }
  errno = EBADF;
  return -1;
}

// Close file descriptor if open
int RawFile::close()    {
  if ( m_reuse )   {
    this->seek(0, SEEK_SET);    
    m_ptr = m_begin;
    return 0;
  }
  else if ( m_fd == HEAP_MEMORY_MAPPED )   {
    m_begin = m_end = m_ptr = 0;
  }
  else if ( m_begin )   {
    ::munmap(m_begin, m_end-m_begin);
    m_begin = m_end = m_ptr = 0;
  }
  else if ( m_fd > 0 )  {
    ::close(m_fd);
  }
  m_fd = -1;
  return 0;
}

/// Reset information (close and reset name)
int RawFile::reset(bool close_file)   {
  if ( close_file )   {
    m_name = "";
    return this->close();
  }
  else if ( m_begin )   {
    m_ptr = m_begin;
  }
  else if ( isOpen() )  {
    this->seek(0, SEEK_SET);
  }
  return 0;
}

/// Open file in read mode
int RawFile::open(bool silent)  {
  int fd = ::open(cname(), O_RDONLY | O_BINARY | O_LARGEFILE, S_IREAD);
  if ( -1 == fd )    {
    std::string err = std::make_error_code(std::errc(errno)).message();
    ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: CANNOT OPEN %s: [%s]", cname(), err.c_str());
    return m_fd = fd;
  }
  ::lib_rtl_output(silent ? LIB_RTL_DEBUG : LIB_RTL_INFO,"RawFile: Opened %s for processing.",cname());
  return m_fd = fd;
}

/// Open file in write mode
int RawFile::openWrite(bool silent)  {
  int fd = ::open(cname(), O_CREAT|O_WRONLY|O_BINARY|O_LARGEFILE, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH);
  if ( -1 == fd )    {
    ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: CANNOT OPEN %s: [%s]", cname(), RTL::errorString(errno).c_str());
    return m_fd = fd;
  }
  ::lib_rtl_output(silent ? LIB_RTL_DEBUG : LIB_RTL_INFO,"RawFile: Opened %s for writing.",cname());
  return m_fd = fd;
}

/// Open file in read mode with mapping
int RawFile::openMapped(bool silent)  {
  int fd = open(silent);
  if ( -1 != fd )    {
    struct stat sb;
    ::fstat(fd, &sb); /* To obtain file size */
    void* ptr = ::mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if ( ptr == MAP_FAILED )   {
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: CANNOT MMAP %s: [%s]", cname(), RTL::errorString(errno).c_str());
      close();
      return -1;
    }
    m_ptr = m_begin = (unsigned char*)ptr;
    m_end = m_begin + sb.st_size;
  }
  return fd;
}

/// Map memory of the file
int RawFile::map_memory(const void* ptr, std::size_t len)  {
  m_begin = m_ptr = m_end = nullptr;
  this->close();
  m_fd  = HEAP_MEMORY_MAPPED;
  m_ptr = m_begin = (unsigned char*)ptr;
  m_end = m_begin + len;
  return 1;
}

/// Map memory of the file
int RawFile::unmap_memory()  {
  m_begin = m_ptr = m_end = nullptr;
  this->close();
  m_fd = -1;
  return 1;
}

/// Write chunk of data
long RawFile::write(const void* data, int len)   {
  if ( isOpen() )    {
    const unsigned char* p = (const unsigned char*) data;
    if ( len < 0 )   {
      errno = EINVAL;
      return -1;
    }
    long tmp = len;
    while (tmp > 0)  {
      long sc = ::write(m_fd, p + len - tmp, tmp);
      if (sc > 0)
	tmp -= sc;
      else return 0;
    }
    return long(len);
  }
  errno = EBADF;
  return -1;
}

/// Write chunk of data
long RawFile::write(const void* data, long len)   {
  if ( isOpen() )    {
    const unsigned char* p = (const unsigned char*) data;
    if ( len < 0 )   {
      errno = EINVAL;
      return -1;
    }
    long tmp = len;
    while (tmp > 0)  {
      long sc = ::write(m_fd, p + len - tmp, tmp);
      if (sc > 0)
	tmp -= sc;
      else return 0;
    }
    return len;
  }
  errno = EBADF;
  return -1;
}

/// Write chunk of data
long RawFile::write(const void* data, std::size_t len)   {
  if ( isOpen() )    {
    const unsigned char* p = (const unsigned char*) data;
    long tmp = (long)len;
    while (tmp > 0)  {
      int sc = ::write(m_fd, p + len - tmp, tmp);
      if (sc > 0)
	tmp -= sc;
      else return 0;
    }
    return len;
  }
  errno = EBADF;
  return -1;
}

/// Read chunk of data
long RawFile::read(void* pointer, long len) {
  unsigned char* p = (unsigned char*)pointer;
  if ( len < 0 )   {
    errno = EINVAL;
    return -1;
  }
  if ( isMapped() )   {
    if ( m_ptr + len <= m_end )  {
      ::memcpy(p, m_ptr, len);
      m_ptr += len;
      return len;
    }
    else if ( m_ptr < m_end )  {
      len = m_end - m_ptr;
      ::memcpy(p, m_ptr, len);
      m_ptr = m_end;
      return len;
    }
    return -1;
  }
  if ( isOpen() )    {
    long tmp = 0;
    while ( tmp < len )   {
      long sc = ::read(m_fd, p + tmp, len - tmp);
      if (sc > 0)
	tmp += sc;
      else if (sc == 0)
	return 0;
      else
	return -1;
    }
    return len;
  }
  errno = EBADF;
  return -1;
}

/// Unlink file
int RawFile::unlink()   {
  int sc = ::unlink(cname());
  if (sc != 0)        {
    int err_num = errno;
    auto err = RTL::errorString();
    ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: CANNOT UNLINK file: %s: [%s]",
		     cname(), !err.empty() ? err.c_str() : "????????");
    switch(err_num)  {
    case ENOENT:
    case ENOTDIR:
    case EPERM:
    case EACCES:
    case EIO:
    case EROFS:
      return err_num;  // Bad disk ?
    case EFAULT:
    case EBUSY:
    case EISDIR:
    case ELOOP:
    case ENAMETOOLONG:
    case ENOMEM:
    default:
      // Standard stuff: for one or another reason, this file cannot be used.
      // Close it, skip and continue.
      ::lib_rtl_output(LIB_RTL_INFO,"RawFile: File:%s SKIPPED for processing.", cname());
      ::close(m_fd);
      m_fd = -1;
      return 1;
    }
  }
  return sc;
}

int  RawFile::mkdir(const char* dir_name, int protection, bool silent)    {
  namespace fs = filesystem;
  error_code ec, ecexist;
  fs::path p = fs::path(dir_name);
  fs::create_directories(p, ec);
  if ( fs::exists(p, ecexist) )   {
    fs::perms prot = fs::perms::none;
    if ( protection&S_IRUSR ) prot |= fs::perms::owner_read;
    if ( protection&S_IWUSR ) prot |= fs::perms::owner_write;
    if ( protection&S_IXUSR ) prot |= fs::perms::owner_exec;
    if ( protection&S_IRGRP ) prot |= fs::perms::group_read;
    if ( protection&S_IWGRP ) prot |= fs::perms::group_write;
    if ( protection&S_IXGRP ) prot |= fs::perms::group_exec;
    if ( protection&S_IROTH ) prot |= fs::perms::others_read;
    if ( protection&S_IWOTH ) prot |= fs::perms::others_write;
    if ( protection&S_IXOTH ) prot |= fs::perms::others_exec;
    fs::permissions(p, prot);
    return 0;
  }
  if ( !silent )   {
    ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: CANNOT create directory %s: %d [%s]",
		     dir_name, ec.value(), ec.message().c_str());
  }
  return ec.value();
}

int RawFile::rmdir(const char* dir_name, bool recursive, bool silent)    {
  namespace fs = filesystem;
  error_code ec;
  fs::path p = fs::path(dir_name);

  if ( !fs::exists(p, ec) )   {
    ec = make_error_code(errc(ENOENT));
    if ( !silent )   {
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: CANNOT remove directory %s: %d [%s]",
		       dir_name, ec.value(), ec.message().c_str());
    }
    return ec.value();
  }
  else if ( !fs::is_directory(p,ec) )   {
    if ( !silent )   {
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: Entry %s is no directory: %d [%s]",
		       dir_name, ec.value(), ec.message().c_str());
    }
    return ec.value();
  }
  else if ( recursive )   {
    fs::remove_all(p, ec);
    if ( ec )   {
      if ( !silent )   {
	::lib_rtl_output(LIB_RTL_ERROR,"RawFile: CANNOT remove directory %s: %d [%s]",
			 dir_name, ec.value(), ec.message().c_str());
      }
    }
    else if ( !silent )   {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"RawFile: Remove directory %s", dir_name);
    }
    return ec.value();
  }
  auto ret = fs::remove(p, ec);
  if ( !ret || ec )   {
    if ( !silent )   {
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: CANNOT remove directory %s: %d [%s]",
		       dir_name, ec.value(), ec.message().c_str());
    }
  }
  else if ( !silent )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"RawFile: Remove directory %s", dir_name);
  }
  return ec.value();
}

/// Read single event using dynamic allocation
long RawFile::read_event(EventType expected,
			 EventType& found_type,
			 Allocator& allocator,
			 unsigned char** data)   {
  int size_buf[3];
  off64_t file_position = this->seek(0, SEEK_CUR);
  int status = this->read((unsigned char*)size_buf, sizeof(size_buf));
  if ( data ) *data = 0;
  if ( status <= 0 )   {
    this->close();
    return -1;
  }
  int  evt_size = size_buf[0];
  bool is_mdf   = evt_size > 0 && size_buf[0] == size_buf[1] && size_buf[0] == size_buf[2];

  found_type = expected;
  if ( expected == AUTO_INPUT_TYPE )  {
    if ( is_mdf )  {
      ::lib_rtl_output(LIB_RTL_INFO,"RawFile: Input type AUTO: MDF record found. Switch to input mode MDF.");
      found_type = MDF_INPUT_TYPE;
    }
  }
  else if ( expected == MDF_INPUT_TYPE && !is_mdf )   {
    // Corrupted file: Try to fix it!
    long skip = this->scanToNextMDF();
    if ( skip > 0 )  { // Just move on to next record!
      ::lib_rtl_output(LIB_RTL_WARNING,"RawFile: Corrupted file found: %s at position %ld. "
		       "Skip %ld bytes after sweep to next MDF record.",
		       cname(), file_position, skip);
      return this->read_event(expected, found_type, allocator, data);
    }
    ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: Corrupted file found: %s at position %ld. "
		     "Skip rest of file and delete it!",
		     cname(), file_position);
    this->close();
    found_type = MEP_INPUT_TYPE;
    return -1;
  }
  else if ( expected == MEP_INPUT_TYPE && is_mdf )   {
    ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: Corrupted MEP file found: %s at position %ld. "
		     "looks like an MDF file. Event size:%d bytes.",
		     cname(), file_position, evt_size);
    this->seek(file_position, SEEK_SET);
    this->saveRestOfFile();
    m_fd = 0;
    found_type = MDF_INPUT_TYPE;
    return -1;	  // Error? Should I stop?
  }
  int  buf_size = evt_size;
  unsigned char* data_ptr = allocator(buf_size);
  unsigned char* read_ptr = 0;
  std::size_t read_len = 0;
  if ( 0 == data_ptr )   {
    this->seek(file_position, SEEK_SET);
    return -1;
  }
  if ( is_mdf )   {
    read_ptr = data_ptr;
    ::memcpy(read_ptr, size_buf, sizeof(size_buf));
    read_ptr += sizeof(size_buf);
    read_len  = evt_size-sizeof(size_buf);
  }
  else {
    this->seek(file_position, SEEK_SET);
    return -1;
  }
  status = this->read(read_ptr, read_len);
  if ( status <= 0 )   {  // End-of file, continue with next file
    reset();
    return -1;
  }
  if ( data ) *data = data_ptr;
  return buf_size;
}

/// Read single event into fixed buffer
long RawFile::read_event(EventType      expected,
			 EventType&     found_type,
			 unsigned char* data_ptr,
			 std::size_t    len)
{
  unsigned char* data;
  InPlaceAllocator alloc(data_ptr, len);
  return this->read_event(expected, found_type, alloc, &data);
}

/// Read multiple events into fixed buffer (burst mode)
std::pair<long,long>
RawFile::read_multiple_events(std::size_t    num_events,
			      EventType      expected,
			      EventType&     found_type,
			      unsigned char* p,
			      std::size_t    len)
{
  if ( p && len > 0 )   {
    std::size_t len_left  = len;
    std::size_t len_frame = 0;
    long        num_evts  = 0;
    unsigned char*  ptr = p;
    EventType found;
    off64_t file_position = this->seek(0, SEEK_CUR);
    for (std::size_t i=0; i<num_events && ptr < p+len; ++i)   {
      // Type is tricky here: auto-switch to concrete mode if AUTO is enabled
      long evt_len = this->read_event(expected, found, ptr, len_left);
      if ( expected != AUTO_INPUT_TYPE && found != expected )  {
	if ( m_fd ) this->seek(file_position, SEEK_SET);
	found_type = found;
	break;
      }
      else if ( expected != AUTO_INPUT_TYPE && found != expected )  {
	this->seek(file_position, SEEK_SET);
	found_type = found;
	break;
      }
      else if ( evt_len > 0 )   {
	file_position = this->seek(0, SEEK_CUR);
	found_type = found;
	expected   = found;
	len_left  -= evt_len;
	ptr       += evt_len;
	len_frame += evt_len;
	++num_evts;
	continue;
      }
      this->seek(file_position, SEEK_SET);
      break;
    }
    return make_pair(num_evts,long(len_frame));
  }
  return make_pair(-1,-1);
}

/// Search bad file and try to seek the next event record. Returns offset to next event
long RawFile::scanToNextMDF()    {
  unsigned char   buff[128], *bend = buff+sizeof(buff);
  off64_t         file_position = this->seek(0, SEEK_CUR);
  off64_t         hdr_len = EventHeader::sizeOf(3); // Header version 3!
  std::size_t     num_file_byte = 0, read_len = sizeof(buff)-hdr_len;
  unsigned char*  qstart = buff+hdr_len;
  int status = 1, count = 0, _debug = 0;

  ::memset(buff,0,sizeof(buff));
  while ( status>0 )  {
    status = this->read(qstart, read_len);
    if ( status > 0 )  {
      num_file_byte += read_len;
      for( unsigned char* p = qstart; p<bend; ++p, ++count )  {
	Tell1Bank* bank = (Tell1Bank*)p;
	if ( bank->magic() == Tell1Bank::MagicPattern )   {
	  // Possible start of a new bank. See if MDF header preceeds it.
	  EventHeader* hdr  = (EventHeader*)(p-hdr_len);
	  if ( _debug )  {
	    //cout << "Bank[" << count << "]: " << EventPrintout::bankHeader(bank) << endl;
	  }
	  if ( hdr->headerVersion()>0 && hdr->headerVersion()<5 )  {
	    std::size_t siz = hdr->size0();
	    if ( _debug )  {
	      cout << "EventHeader:[" << count << ", " << long(p-qstart) << "] Size:"
		   << siz << " " << hdr->size1() << " " << hdr->size2()
		   << " Vsn:" << hdr->headerVersion() << endl;
	    }
	    if ( hdr->subheaderLength() < 64 )  {
	      if ( siz>0 && siz == hdr->size1() && siz == hdr->size2() )  {
		int off = (((unsigned char*)hdr)-qstart)-read_len;
		int skip = num_file_byte+off;
		// Set the file descriptor back to the beginning of the MDF header. Job done.
		seek(file_position+skip, SEEK_SET);
		cout << "[WARNING] Got it: EventHeader:[" << count << ", " << skip << "] Size:"
		     << hdr->size0() << " " << hdr->size1() << " " << hdr->size2()
		     << " Vsn:" << hdr->headerVersion() << endl << flush;
		return skip;
	      }
	    }
	  }
	  // Otherwise continue sweeping over the record to the next bank!
	  // At some point there MUST be a bank preceeded by a EventHeader
	  // or the file is at end!
	}
      }
      // Copy the last EventHeader::sizeOf(3) bytes to the beginning of the buffer
      // since it might contain the next MDF header data. Then restart the scan
      ::memcpy(buff,bend-hdr_len,hdr_len);
    }
    else  {  // Nothing can be read anymore: EOF
      this->seek(0, SEEK_END); // Position to end of file
      // This will make the next read fail!
      // +1: Just in case already the first read failed!
      //     The returned number is anyhow for diagnostics only
      return num_file_byte > 0 ? num_file_byte : 1;
    }
  }
  return 0;
}

/// Save remainder of currently read file
void RawFile::saveRestOfFile()   {
  RawFile out(m_name);
  ::lib_rtl_output(LIB_RTL_INFO,"RawFile: Saving rest of file[%d]: %s", m_fd, cname());
  if ( out.openWrite() == -1 )    {
    auto err = RTL::errorString();
    ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: CANNOT Create file: %s: [%s]",
		     cname(), !err.empty() ? err.c_str() : "????????");
    return;
  }
  std::size_t cnt = 0;
  unsigned char buffer[10 * 1024];
  for (int ret; (ret = this->read(buffer, sizeof(buffer))) > 0; )  {
    if ( !out.write(buffer, ret) )      {
      auto err = RTL::errorString();
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: CANNOT write file: %s: [%s]",
		       cname(), !err.empty() ? err.c_str() : "????????");
    }
    cnt += ret;
  }
  ::lib_rtl_output(LIB_RTL_INFO,"RawFile: Wrote %ld bytes to file:%s fd:%d",
		   cnt, out.cname(), out.fileno());
  out.close();
  ::close(m_fd);
  m_fd = -1;
}

/// Scan single record and check for consistency
int RawFile::checkRecord(std::size_t offset, const void* data, std::size_t length)  const    {
  if ( data && length )   {
    const char* ptr  = (const char*)data;
    bool bad_record  = this->checkEventHeader(offset, data, length) != 1;
    const EventHeader* hdr = (EventHeader*)ptr;
    const char* start = ptr + EventHeader::sizeOf(hdr->headerVersion());
    const char* end   = ptr + hdr->size();
    for( ; start < end; )  {
      const Tell1Bank* b = (const Tell1Bank*)start;
      bad_record = bad_record || this->checkBank(start-ptr,b)==1;
      if ( b->size() > 0 )   {
	start += b->size();
	continue;
      }
      break;
    }
    return bad_record ? 0 : 1;
  }
  return 0;
}

/// Scan single record and check for consistency
int RawFile::checkEventHeader(std::size_t offset, const void* data, std::size_t length)  const    {
  if ( data && length )   {
    const char* ptr  = (const char*)data;
    const EventHeader* hdr = (const EventHeader*)ptr;
    bool bad_record = this->checkEventHeader(offset, data, length) != 1;

    if ( hdr->size0() != length )   {    /// Check the MDF header length(0)
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: File:%s Offset:%ld Event has inconsistent size(0): [%d %d %d]",
	       cname(), offset, hdr->size0(), hdr->size1(), hdr->size2());
      bad_record = true;
    }
    if ( hdr->size1() != length )   {    /// Check the MDF header length(1)
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: File:%s Offset:%ld Event has inconsistent size(1): [%d %d %d]",
	       cname(), offset, hdr->size0(), hdr->size1(), hdr->size2());
      bad_record = true;
    }
    if ( hdr->size1() != length )   {    /// Check the MDF header length(2)
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: File:%s Offset:%ld Event has inconsistent size(1): [%d %d %d]",
	       cname(), offset, hdr->size0(), hdr->size1(), hdr->size2());
      bad_record = true;
    }
    if ( hdr->headerVersion() != 3 )   {
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: File:%s Offset:%ld Event header has bad header version:%d [!=3]",
	       cname(), offset, hdr->headerVersion());
      bad_record = true;
    }
    return bad_record ? 0 : 1;
  }
  return 0;
}

/// Check content of bank
int RawFile::checkBank(std::size_t offset, const void* data)  const  {
  const Tell1Bank* b = (const Tell1Bank*)data;
  typedef Tell1Printout _P;
  // Check bank's magic word
  if ( b->magic() == Tell1Bank::MagicPattern )  {
    // Crude check on bank type
    if ( b->type() < Tell1Bank::LastType )  {
      // Crude check on bank length
      if ( b->size() >= 0 )  {
	// Now check source ID range:
	//// TBD !!
	return 1;
      }
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: %s Offset:%ld Bad bank length found in Tell1 bank %p: %s",
	       cname(), offset, data, _P::bankHeader(b).c_str());
      return 0;
    }
    ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: %s Offset:%ld Unknown Bank type in Tell1 bank %p: %s",
	     cname(), offset, data, _P::bankHeader(b).c_str());
    return 0;
  }
  ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: %s Offset:%ld Bad magic pattern in Tell1 bank %p: %s",
	   cname(), offset, data, _P::bankHeader(b).c_str());
  return 0;
}

/// Dump a set of raw banks
void RawFile::dump(const unsigned char* data, std::size_t len)   const  {
  const unsigned char* ptr = data;
  const unsigned char* end = data + len;
  while( ptr < end )   {
    const Tell1Bank* bank = (Tell1Bank*)ptr;
    if ( checkBank(ptr-data, ptr) )    {
      dump(bank);
      ptr += bank->totalSize();
      continue;
    }
    break;
  }
}

/// Dump the content of a single raw bank to screen
void RawFile::dump (const Tell1Bank* bank)   const  {
  const Tell1Bank* r = bank;
  int cnt, flags = 3;
  bool ful = flags&1;
  bool dmp = flags&1;
  stringstream info;

  if ( dmp ) {
    info << "Bank:  [" << Tell1Printout::bankHeader(r) << "] " << endl;
  }
  if( ful ) {
    cnt = 0;
    const int* p;
    std::stringstream s;
    for(p=r->begin<int>(); p != r->end<int>(); ++p)  {
      s << std::hex << std::setw(8) << std::hex << *p << " ";
      if ( ++cnt == 10 ) {
	info << "  Data:" << s.str() << endl;
	s.str("");
	cnt = 0;
      }
    }
    if ( cnt > 0 ) info << "  Data:" << s.str() << endl;
  }
  cout << info.str();
}

namespace {
  void help()  {
  }
}

extern "C" int mdf_file_read(int argc, char** argv)   {
  struct malloc_allocator final : public RawFile::Allocator {
    unsigned char* operator()(std::size_t memsize) override {
      return (unsigned char*)::malloc(memsize);
    }
  } allocator;
  int packing = 1;
  int memsize = 0;
  string input;
  RTL::CLI cli(argc,argv,help);
  cli.getopt("input", 3, input);
  cli.getopt("packing", 3, packing);
  cli.getopt("allocate", 3, memsize);
  RTL::Logger::install_rtl_printer(LIB_RTL_DEBUG);
  unsigned char* memptr = 0;
  if ( memsize > 0 ) memptr = (unsigned char*)::operator new(memsize);
  RawFile mdf(input);
  if ( -1 == mdf.open() )   {
    return -1;
  }
  for(long num_evt = 0, num_read = 0;;)   {
    const unsigned char* data = 0;
    RawFile::EventType   found_type;
    pair<long,long>      ret;
    ret = mdf.read_multiple_events(packing,
				   RawFile::MDF_INPUT_TYPE,
				   found_type,
				   memptr,
				   memsize);
    data = memptr;
    ++num_read;
    ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ =========================================================== ++");
    ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ Read %4ld events with %7ld bytes total.", ret.first, ret.second);
    const unsigned char* start = data;
    for(int i=0; i < ret.first; ++i)   {
      const EventHeader* header = (const EventHeader*)start;
      std::size_t        length = header->size0();
      std::size_t        hdrlen = header->sizeOf(header->headerVersion());
      ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ =========================================================== ++");
      ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ Bank dump of subevent %3d Run:%d Orbit:%d bunchID:%d", i,
	       header->subHeader().H1->runNumber(),
	       header->subHeader().H1->orbitNumber(),
	       header->subHeader().H1->bunchID());
      ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ =========================================================== ++");
      mdf.dump(start+hdrlen, length-hdrlen);
      start += length;
      ++num_evt;
    }
    ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ =========================================================== ++");
    if ( ret.first < packing )   {
      ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ Finished input source after %ld events and %ld read actions.",
	       num_evt, num_read);
      ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ =========================================================== ++");
      break;
    }
  }
  ::operator delete(memptr);
  return 0;
}
