/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Tell1Data/Tell1Decoder.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <netinet/in.h>

using namespace std;
using Online::Tell1Bank;
using Online::Tell1Printout;
static const char* s_checkLabel = "BankCheck    ERROR  ";


/// one-at-time hash function
unsigned int Online::hash32Checksum(const void* ptr, size_t len) {
  unsigned int hash = 0;
  const char* k = (const char*)ptr;
  for (size_t i=0; i<len; ++i, ++k) {
    hash += *k;
    hash += (hash << 10);
    hash ^= (hash >> 6);
  }
  hash += (hash << 3);
  hash ^= (hash >> 11);
  hash += (hash << 15);
  return hash;
}

/* ========================================================================= */
unsigned int Online::adler32Checksum(unsigned int adler,
				     const char* buf,
				     size_t len)
{
#define DO1(buf,i)  {s1 +=(unsigned char)buf[i]; s2 += s1;}
#define DO2(buf,i)  DO1(buf,i); DO1(buf,i+1);
#define DO4(buf,i)  DO2(buf,i); DO2(buf,i+2);
#define DO8(buf,i)  DO4(buf,i); DO4(buf,i+4);
#define DO16(buf)   DO8(buf,0); DO8(buf,8);

  static const unsigned int BASE = 65521;    /* largest prime smaller than 65536 */
  /* NMAX is the largest n such that 255n(n+1)/2 + (n+1)(BASE-1) <= 2^32-1 */
  static const unsigned int NMAX = 5550;
  unsigned int s1 = adler & 0xffff;
  unsigned int s2 = (adler >> 16) & 0xffff;
  int k;

  if (buf == NULL) return 1;

  while (len > 0) {
    k = len < NMAX ? (int)len : NMAX;
    len -= k;
    while (k >= 16) {
      DO16(buf);
      buf += 16;
      k -= 16;
    }
    if (k != 0) do {
	s1 += (unsigned char)*buf++;
	s2 += s1;
      } while (--k);
    s1 %= BASE;
    s2 %= BASE;
  }
  unsigned int result = (s2 << 16) | s1;
  return result;
}
/* ========================================================================= */

static unsigned int xorChecksum(const int* ptr, size_t len)  {
  unsigned int checksum = 0;
  len = len/sizeof(int) + (len%sizeof(int) ? 1 : 0);
  for(const int *p=ptr, *end=p+len; p<end; ++p)  {
    checksum ^= *p;
  }
  return checksum;
}

#define QUOTIENT  0x04c11db7
class CRC32Table  {
public:
  unsigned int m_data[256];
  CRC32Table()  {
    unsigned int crc;
    for (int i = 0; i < 256; i++)    {
      crc = i << 24;
      for (int j = 0; j < 8; j++)   {
        if (crc & 0x80000000)
          crc = (crc << 1) ^ QUOTIENT;
        else
          crc = crc << 1;
      }
      m_data[i] = htonl(crc);
    }
  }
  const unsigned int* data() const { return m_data; }
};

// Only works for word aligned data and assumes that the data is an exact number of words
// Copyright  1993 Richard Black. All rights are reserved.
static unsigned int crc32Checksum(const char *data, size_t len)    {
  static CRC32Table table;
  const unsigned int *crctab = table.data();
  const unsigned int *p = (const unsigned int *)data;
  const unsigned int *e = (const unsigned int *)(data + len);
  if ( len < 4 || (size_t(data)%sizeof(unsigned int)) != 0 ) return ~0x0;
  unsigned int result = ~*p++;
  while( p < e )  {
#if defined(LITTLE_ENDIAN)
    result = crctab[result & 0xff] ^ result >> 8;
    result = crctab[result & 0xff] ^ result >> 8;
    result = crctab[result & 0xff] ^ result >> 8;
    result = crctab[result & 0xff] ^ result >> 8;
    result ^= *p++;
#else
    result = crctab[result >> 24] ^ result << 8;
    result = crctab[result >> 24] ^ result << 8;
    result = crctab[result >> 24] ^ result << 8;
    result = crctab[result >> 24] ^ result << 8;
    result ^= *p++;
#endif
  }

  return ~result;
}

static unsigned short crc16Checksum (const char *data, size_t len) {
  static const unsigned short wCRCTable[] =
    { 0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
      0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
      0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
      0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
      0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
      0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
      0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
      0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
      0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
      0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
      0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
      0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
      0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
      0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
      0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
      0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
      0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
      0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
      0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
      0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
      0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
      0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
      0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
      0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
      0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
      0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
      0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
      0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
      0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
      0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
      0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
      0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040 };
  unsigned int nTemp;
  unsigned short wCRCWord = 0xFFFF;
  while (len--)  {
    nTemp = *data++ ^ wCRCWord;
    wCRCWord >>= 8;
    wCRCWord = (unsigned short)(wCRCWord^wCRCTable[nTemp]);
  }
  return wCRCWord;
}

static char crc8Checksum(const char *data, int len) {
  static unsigned char crc8_table[] =
    { 0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
      157,195, 33,127,252,162, 64, 30, 95,  1,227,189, 62, 96,130,220,
      35,125,159,193, 66, 28,254,160,225,191, 93,  3,128,222, 60, 98,
      190,224,  2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
      70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89,  7,
      219,133,103, 57,186,228,  6, 88, 25, 71,165,251,120, 38,196,154,
      101, 59,217,135,  4, 90,184,230,167,249, 27, 69,198,152,122, 36,
      248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91,  5,231,185,
      140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
      17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
      175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
      50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
      202,148,118, 40,171,245, 23, 73,  8, 86,180,234,105, 55,213,139,
      87,  9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
      233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
      116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53
    };
  const char *s = data;
  char c = 0;
  while (len--) c = crc8_table[c ^ *s++];
  return c;
}

/// Generate  Checksum
unsigned int Online::genChecksum(int flag,const void* ptr, size_t len)  {
  switch(flag)  {
  case 0:
    return xorChecksum((const int*)ptr, len);
  case 1:
    return hash32Checksum(ptr, len);
  case 2:
    len = (len/sizeof(int))*sizeof(int);
    return crc32Checksum((const char*)ptr, len);
  case 3:
    len = (len/sizeof(short))*sizeof(short);
    return crc16Checksum((const char*)ptr, len);
  case 4:
    return crc8Checksum((const char*)ptr, len);
  case 5:
    len = (len/sizeof(int))*sizeof(int);
    return adler32Checksum(1, (const char*)ptr, len);
  case 22:  // Old CRC32 (fixed by now)
    return crc32Checksum((const char*)ptr, len);
  default:
    return ~0x0;
  }
}

static void print_previous_bank(const Tell1Bank* prev) {
  char txt[255];
  if ( prev == 0 )
    ::snprintf(txt,sizeof(txt),"%s Bad bank is the first bank in the MEP fragment.",s_checkLabel);
  else
    ::snprintf(txt,sizeof(txt),"%s Previous (good) bank [%p]: %s",s_checkLabel,
	       (void*)prev,Tell1Printout::bankHeader(prev).c_str());
  cout << txt << endl;
}

/// Check sanity of raw bank structure
bool Online::checkRawBank(const Tell1Bank* b, bool throw_exc, bool print_cout)  {
  typedef Tell1Printout _P;
  // Check bank's magic word: Either Tell1 magic word or PCIE40 magic word
  if ( b->magic() == Tell1Bank::MagicPattern || b->magic() == 0xFACE )  {
    // Crude check on the bank type
    if ( b->type() < Tell1Bank::LastType )  {
      // Crude check on the bank length
      if ( b->size() >= 0 )  // Zero bank length is apparently legal....
	{
	  // Now check source ID range:
	  //// TBD !!
	  return true;
	}
      char txt2[255];
      ::snprintf(txt2, sizeof(txt2), "%s Bad bank length found in Tell1 bank %p: %s", 
		 s_checkLabel, reinterpret_cast<const void*>(b), _P::bankHeader(b).c_str());
      if ( print_cout ) cout << txt2 << endl;
      if ( throw_exc  ) throw runtime_error(txt2);
      return false;
    }
    char txt1[255];
    ::snprintf(txt1, sizeof(txt1), "%s Unknown Bank type in Tell1 bank %p: %s", 
	       s_checkLabel, reinterpret_cast<const void*>(b), _P::bankHeader(b).c_str());
    if ( print_cout ) cout << txt1 << endl;
    if ( throw_exc  ) throw runtime_error(txt1);
    return false;
  }
  // Error: Bad magic pattern; needs handling
  char txt0[255];
  ::snprintf(txt0, sizeof(txt0), "%s Bad magic pattern in Tell1 bank %p: %s",
	     s_checkLabel, reinterpret_cast<const void*>(b), _P::bankHeader(b).c_str());
  if ( print_cout ) cout << txt0 << endl;
  if ( throw_exc  ) throw runtime_error(txt0);
  return false;
}

/// Check consistency of MEP fragment using magic bank patterns.
bool Online::checkRawBanks(const void* start, const void* end, bool exc,bool prt)  {
  char txt[255];
  Tell1Bank* prev = 0;
  if ( end >= start )  {
    for(Tell1Bank* b=(Tell1Bank*)start, *e=(Tell1Bank*)end; b < e; )  {
      if ( !checkRawBank(b,false,true) ) goto Error;  // Check bank sanity
      b = (Tell1Bank*)(((const char*)b) + b->totalSize());
      prev = b;
    }
    return true;
  }
 Error:  // Anyhow only end up here if no exception was thrown...
  ::snprintf(txt, sizeof(txt), "%s Error in multi raw bank buffer start:%p end:%p",
	     s_checkLabel,reinterpret_cast<const void*>(start),reinterpret_cast<const void*>(end));
  if ( prt ) {
    cout << txt << endl;
    print_previous_bank(prev);
  }
  if ( exc ) throw runtime_error(txt);
  return false;
}

#if 0
/// Check consistency of MEP fragment
bool Online::checkFragment(const Tell1Fragment* f, bool exc, bool prt)  {
  const char* s = f->start();
  const char* e = f->end();
  if ( s+f->size()-1 == e )  {
    if ( checkRawBanks(s, e, exc, prt) ) return true;
  }
  char txt[255];
  ::sprintf(txt,"%s MEP fragment error at %p: EID_l:%d Size:%ld %ld",
            s_checkLabel,reinterpret_cast<const void*>(f),f->eventID(),long(f->size()),long(e-s));
  if ( prt ) cout << txt << endl;
  if ( exc ) throw runtime_error(txt);
  return false;
}

/// Check consistency of MEP multi fragment
bool Online::checkMultiFragment(const Tell1MultiFragment* mf, bool exc, bool prt)  {
  char txt[255];
  const Tell1Fragment* s = mf->first();
  const Tell1Fragment* e = mf->last();
  size_t siz = mf->size();
  if ( e >= s && mf->start()+siz == mf->end() )  {
    for( ; s < e; s = mf->next(s) )  {
      if ( !checkFragment(s,exc,prt) )  goto Error;
    }
    return true;
  }
 Error:  // Anyhow only end up here if no exception was thrown...
  ::sprintf(txt,"%s MEP multi fragment error at %p: EID_l:%u Size:%ld %ld",
            s_checkLabel,reinterpret_cast<const void*>(mf),mf->eventID(),long(siz),long(s-e));
  if ( prt ) cout << txt << endl;
  if ( exc ) throw runtime_error(txt);
  return false;
}

/// Check consistency of MEP multi event
bool Online::checkMEPEvent (const Tell1MEP* me, bool exc, bool prt)  {
  char txt[255];
  const Tell1MultiFragment* s = me->first();
  const Tell1MultiFragment* e = me->last();
  if ( e >= s && me->start()+me->size()-1 == me->end() )  {
    for( ; s < e; s = me->next(s) )  {
      if ( !checkMultiFragment(s,exc,prt) ) goto Error;
    }
    return true;
  }
 Error:  // Anyhow only end up here if no exception was thrown...
  ::sprintf(txt,"%s MEP event error at %p: Size:%ld",s_checkLabel,reinterpret_cast<const void*>(me),long(me->size()));
  if ( prt ) cout << txt << endl;
  if ( exc ) throw runtime_error(txt);
  return false;
}
#endif

/// Check consistency of MEP multi event fragment
bool Online::checkRecord(const EventHeader* h, int opt_len, bool exc,bool prt)    {
  if ( h )  {
    int  compress;
    char txt[255];
    const char *start, *end;
    if ( h->size0() != h->size1() || h->size0() != h->size2() )  {
      ::snprintf(txt,sizeof(txt),"%s Inconsistent MDF header size: %u <-> %u <-> %u at %p",
		 s_checkLabel,h->size0(),h->size1(),h->size2(),reinterpret_cast<const void*>(h));
      goto Error;
    }
    if ( opt_len != ~0x0 && size_t(opt_len) != h->size0() )  {
      ::snprintf(txt,sizeof(txt),"%s Wrong MDF header size: %u <-> %d at %p",
		 s_checkLabel,h->size0(),opt_len,reinterpret_cast<const void*>(h));
      goto Error;
    }
    compress  = h->compression()&0xF;
    if ( compress )  {
      // No uncompressing here! Assume everything is OK.
      return true;
    }
    start = ((char*)h) + sizeof(EventHeader) + h->subheaderLength();
    end   = ((char*)h) + h->size0();
    if ( !checkRawBanks(start,end,exc,prt) )  {
      ::snprintf(txt,sizeof(txt),"%s Error in multi raw bank buffer start:%p end:%p",
		 s_checkLabel,(const void*)start, (const void*)end);
      goto Error;
    }
    return true;

  Error:  // Anyhow only end up here if no exception was thrown...
    if ( prt ) cout << txt << endl;
    if ( exc ) throw runtime_error(txt);
  }
  return false;
}

/// Conditional decoding of raw buffer from MDF to vector of raw banks
int Online::decodeRawBanks(const void* start, const void* end, vector<Tell1Bank*>& banks) {
  const char* s = (const char*)start;
  Tell1Bank *prev = 0, *bank = (Tell1Bank*)s;
  try {
    while (s < end)  {
      bank = (Tell1Bank*)s;
      checkRawBank(bank,true,true);  // Check bank sanity
      banks.push_back(bank);
      s += bank->totalSize();
      prev = bank;
    }
    return 1;
  }
  catch(const exception& e) {
    print_previous_bank(prev);
  }
  catch(...) {
    print_previous_bank(prev);
  }
  throw runtime_error("Error decoding raw banks!");
}
#if 0

/// Conditional decoding of raw buffer from MDF to vector of bank pointers
int Online::decodeFragment(const Tell1Fragment* f, vector<Tell1Bank*>& raw)  {
  return decodeRawBanks(f->start(), f->end(), raw);
}

/// Decode single fragment into a list of pairs (event id,bank)
int Online::decodeFragment2Banks(const Tell1Fragment* f,
				 unsigned int event_id_high,
				 vector<pair<unsigned int,Tell1Bank*> >& banks)
{
  if ( f )  {
    const Tell1Bank *prev = 0;
    try {
      unsigned int evID = (event_id_high&0xFFFF0000) + (f->eventID()&0xFFFF);
      const Tell1Bank* l = f->last();
      for(Tell1Bank* b=f->first(); b<l; b=f->next(b)) {
	checkRawBank(b,true,true);
	banks.emplace_back(evID,b);
      }
      return 1;
    }
    catch(const exception& e) {
      print_previous_bank(prev);
      throw e;
    }
    catch(...) {
      print_previous_bank(prev);
      throw runtime_error("Unknown error while checking banks.");
    }
  }
  return 0;
}

/// Decode multi fragment into a list of pairs (event id,bank)
int Online::decodeMultiFragment2Banks(const Tell1MultiFragment* mf,
				      unsigned int&   partitionID,
				      vector<pair<unsigned int,Tell1Bank*> >& banks)
{
  unsigned int eid_h = 0;
  if ( mf )  {
    partitionID = mf->partitionID();
    eid_h = mf->eventID();
    for (Tell1Fragment* f = mf->first(); f<mf->last(); f=mf->next(f)) {
      if (f != mf->first() && 0 == f->eventID() ) ++eid_h;
      if ( !decodeFragment2Banks(f,eid_h,banks) )   {
        char txt[132];
        sprintf(txt,"%s Failed to decode MEP fragment at %p",s_checkLabel,reinterpret_cast<const void*>(mf));
        throw runtime_error(txt);
      }
    }
    return 1;
  }
  return 0;
}

// Decode MEP into bank collections
int Online::decodeMEP( const Tell1MEP* me,
		       unsigned int&   partitionID,
		       map<unsigned int,vector<Tell1Bank*> >& events)
{
  typedef vector<Tell1Bank*> _Banks;
  unsigned int evID, eid_h = 0, eid_l = 0;
  const Tell1Bank *prev = 0;

  try {
    for (Tell1MultiFragment* mf = me->first(); mf<me->last(); mf=me->next(mf)) {
      partitionID = mf->partitionID();
      eid_h = mf->eventID()&0xFFFF0000;
      prev = 0;
      for (Tell1Fragment* f = mf->first(); f<mf->last(); f=mf->next(f)) {
	eid_l = f->eventID();
	if ( f != mf->first() && eid_l==0 ) ++eid_h;
	evID = eid_h + (eid_l&0xFFFF);
	_Banks& banks = events[evID];
	for(Tell1Bank *b=f->first(), *l=f->last(); b<l; b=f->next(b)) {
	  checkRawBank(b,true,true);
	  banks.push_back(b);
	  prev = b;
	}
      }
    }
  }
  catch(const exception& e) {
    print_previous_bank(prev);
    throw e;
  }
  catch(...) {
    print_previous_bank(prev);
    throw runtime_error("Unknown error while checking banks.");
  }
  return 1;
}

/// Decode MEP into Tell1 fragment collections
int Online::decodeMEP( const Tell1MEP* me, 
		       unsigned int&   partitionID, 
		       map<unsigned int,vector<Tell1Fragment*> >& events)   {
  typedef vector<Tell1Fragment*> _Frags;
  unsigned int evID, eid_h = 0, eid_l = 0;
  try {
    for (Tell1MultiFragment* mf = me->first(); mf<me->last(); mf=me->next(mf)) {
      //const Tell1Fragment* prev = 0;
      partitionID = mf->partitionID();
      eid_h = mf->eventID()&0xFFFF0000;
      for (Tell1Fragment* f = mf->first(); f<mf->last(); f=mf->next(f)) {
	eid_l = f->eventID();
	if ( f != mf->first() && eid_l==0 ) ++eid_h;
	evID = eid_h + (eid_l&0xFFFF);
	_Frags& frg = events[evID];
	frg.push_back(f);
	//prev = f;
      }
    }
  }
  catch(const exception& e) {
    throw e;
  }
  catch(...) {
    throw runtime_error("Unknown error while checking banks.");
  }
  return 1;
}

// Decode MEP into fragments event by event
int Online::decodeMEP2EventFragments( const Tell1MEP* me,
				      unsigned int&   partitionID,
				      map<unsigned int, vector<Tell1Fragment*> >& events)
{
  unsigned int evID, eid_h = 0, eid_l = 0;
  for (Tell1MultiFragment* mf = me->first(); mf<me->last(); mf=me->next(mf)) {
    bool first = true;
    partitionID = mf->partitionID();
    eid_h = mf->eventID()&0xFFFF0000;
    for (Tell1Fragment* f = mf->first(); f<mf->last(); f=mf->next(f)) {
      if ( !first && eid_l==0 ) {
	eid_h = (mf->eventID()+1)&0xFFFF0000;
      }
      first = false;
      eid_l = f->eventID();
      evID = eid_h + (eid_l&0xFFFF);
      events[evID].push_back(f);
    }
  }
  return 1;
}

/// Decode MEP into a list of banks
int Online::decodeMEP2Banks( const Tell1MEP* me,
			     unsigned int&   partitionID,
			     vector<pair<unsigned int,Tell1Bank*> >& banks)
{
  for (Tell1MultiFragment* mf = me->first(); mf<me->last(); mf=me->next(mf)) {
    if ( !decodeMultiFragment2Banks(mf,partitionID,banks) )  {
      char txt[132];
      sprintf(txt,"%s Failed to decode MEP multi fragment at %p",s_checkLabel,reinterpret_cast<void*>(mf));
      throw runtime_error(txt);
    }
  }
  return 1;
}
#endif

/// Copy RawEvent data from bank vectors to sequential buffer
int Online::encodeRawBank(const Tell1Bank* b, char* const data, size_t size, size_t* length)   {
  size_t s = b->totalSize();
  if ( size >= s )  {
    ::memcpy(data, b, s);
    if ( length ) *length = s;
    return 1;
  }
  return 0;
}
#if 0
/// Copy MEP fragment into opaque data buffer
int Online::encodeFragment(const Tell1Fragment* f, char* const data, size_t len)  {
  char* ptr = data;
  for(Tell1Bank* b=f->first(); b < f->last(); b=f->next(b))  {
    size_t s = b->totalSize();
    if ( ptr+s > data+len )  {
      return 0;
    }
    ::memcpy(ptr, b, s);
    ptr += s;
  }
  return 1;
}

/// Copy array of bank pointers into opaque data buffer
int Online::encodeFragment(const vector<Tell1Bank*>& banks, Tell1Fragment* f)    {
  Tell1Bank* b = f->first();
  for(const Tell1Bank* bank : banks)   {
    size_t s = bank->totalSize();
    void* bptr = (void*)b;
    ::memcpy(bptr, bank, s);
    f->setSize(s+f->size());
    b = f->next(b);
  }
  return 1;
}
#endif

/// Returns the prefix on TES according to bx number, - is previous, + is next
string Online::rootFromBxOffset(int bxOffset) {
  if ( 0 == bxOffset )
    return "/Event";
  if ( 0 < bxOffset )
    return string("/Event/Next") + char('0'+bxOffset);
  return string("/Event/Prev") + char('0'-bxOffset);
}

/// Access to the TAE bank (if present)
Tell1Bank* Online::getTAEBank(const char* start) {
  Tell1Bank* b = (Tell1Bank*)start;            // Get the first bank in the buffer
  if ( b->type() == Tell1Bank::TAEHeader ) {   // Is it the TAE bank?
    return b;
  }
  if ( b->type() == Tell1Bank::DAQ ) {         // Is it the TAE bank?
    start += b->totalSize();                   //
    b = (Tell1Bank*)start;                     // If the first bank is a MDF (DAQ) bank,
  }                                            // then the second bank must be the TAE header
  if ( b->type() == Tell1Bank::TAEHeader ) {   // Is it the TAE bank?
    return b;
  }
  return nullptr;
}

/// Returns the offset of the TAE with respect to the central bx
int Online::bxOffsetTAE(const string& root) {
  size_t idx = string::npos;
  if ( (idx=root.find("/Prev")) != string::npos )
    return -(root[idx+5]-'0');
  if ( (idx=root.find("/Next")) != string::npos )
    return root[idx+5]-'0';
  return 0;
}
#if 0
/// Return vector of MEP sub-event names
vector<string> Online::buffersMEP(const char* start) {
  vector<string> result;
  Tell1MEP* me = (Tell1MEP*)start;
  Tell1MEP::Fragment* f = me->first();
  int num_subevt = f->packing();
  for(int i=-((num_subevt-1)/2), n=0; n<num_subevt; ++n,++i)
    result.push_back(rootFromBxOffset(i));
  return result;
}
#endif

/// Return vector of TAE event names
vector<string> Online::buffersTAE(const char* start) {
  vector<string> result;
  Tell1Bank* b = getTAEBank(start);
  if ( b && b->type() == Tell1Bank::TAEHeader ) {   // Is it the TAE bank?
    int nBlocks = b->size()/sizeof(int)/3;          // The TAE bank is a vector of triplets
    const int* block  = b->begin<int>();
    for(int nbl = 0; nBlocks > nbl; ++nbl, block +=3)
      result.push_back(rootFromBxOffset(*block));
  }
  return result;
}

std::string Online::Tell1Printout::bankHeader(const Tell1Bank* r)  {
  std::stringstream s; 
  s << "Size:"      << std::setw(4) << int(r->size()) 
    << " Type:"     << std::setw(2) << int(r->type())
    << " / "        << std::setw(5) << bankType(r->type())
    << " Source:"   << std::setw(3) << int(r->sourceID())
    << " Vsn:"      << std::setw(2) << int(r->version()) 
    << " Magic: 0x" << std::setw(4) << std::hex << r->magic();
  return s.str();
}

std::string Online::Tell1Printout::bankType(const Tell1Bank* r)  {
  if ( r ) return bankType(r->type());
  return "BAD_BANK";
}

std::string Online::Tell1Printout::bankType(int i)  {
#define PRINT(x)  case Tell1Bank::x : return #x;
  switch(i)  {
    PRINT(L0Calo);              //  0
    PRINT(L0DU);                //  1
    PRINT(PrsE);                //  2
    PRINT(EcalE);               //  3
    PRINT(HcalE);               //  4
    PRINT(PrsTrig);             //  5
    PRINT(EcalTrig);            //  6
    PRINT(HcalTrig);            //  7
    PRINT(Velo);                //  8
    PRINT(Rich);                //  9
    PRINT(TT);                  // 10
    PRINT(IT);                  // 11
    PRINT(OT);                  // 12
    PRINT(Muon);                // 13
    PRINT(L0PU);                // 14
    PRINT(DAQ);                 // 15
    PRINT(ODIN);                // 16
    PRINT(HltDecReports);       // 17
    PRINT(VeloFull);            // 18
    PRINT(TTFull);              // 19
    PRINT(ITFull);              // 20
    PRINT(EcalPacked);          // 21
    PRINT(HcalPacked);          // 22
    PRINT(PrsPacked);           // 23
    PRINT(L0Muon);              // 24
    PRINT(ITError);             // 25
    PRINT(TTError);             // 26
    PRINT(ITPedestal);          // 27
    PRINT(TTPedestal);          // 28
    PRINT(VeloError);           // 29
    PRINT(VeloPedestal);        // 30
    PRINT(VeloProcFull);        // 31
    PRINT(OTRaw);               // 32
    PRINT(OTError);             // 33
    PRINT(EcalPackedError);     // 34
    PRINT(HcalPackedError);     // 35  
    PRINT(PrsPackedError);      // 36
    PRINT(L0CaloFull);          // 37
    PRINT(L0CaloError);         // 38
    PRINT(L0MuonCtrlAll);       // 39
    PRINT(L0MuonProcCand);      // 40
    PRINT(L0MuonProcData);      // 41
    PRINT(L0MuonRaw);           // 42
    PRINT(L0MuonError);         // 43
    PRINT(GaudiSerialize);      // 44
    PRINT(GaudiHeader);         // 45
    PRINT(TTProcFull);          // 46
    PRINT(ITProcFull);          // 47
    PRINT(TAEHeader);           // 48
    PRINT(MuonFull);            // 49
    PRINT(MuonError);           // 50
    PRINT(TestDet);             // 51
    PRINT(L0DUError);           // 52
    PRINT(HltRoutingBits);      // 53
    PRINT(HltSelReports);       // 54
    PRINT(HltVertexReports);    // 55
    PRINT(HltLumiSummary);      // 56
    PRINT(L0PUFull);            // 57
    PRINT(L0PUError);           // 58
    PRINT(DstBank);             // 59
    PRINT(DstData);             // 60
    PRINT(DstAddress);          // 61
    PRINT(FileID);              // 62
    PRINT(VP);                  // 63   
    PRINT(FTCluster);           // 64
    PRINT(VL);                  // 65
    PRINT(UT);                  // 66
    PRINT(UTFull);              // 67
    PRINT(UTError);             // 68
    PRINT(UTPedestal);          // 69
    PRINT(HC);                  // 70
    PRINT(HltTrackReports);     // 71
    PRINT(HCError);             // 72
    PRINT(VPRetinaCluster);     // 73
    PRINT(FTGeneric);           // 74
    PRINT(FTCalibration);       // 75
    PRINT(FTNZS);               // 76
    PRINT(Calo);                // 77
    PRINT(CaloError);           // 78
    PRINT(MuonSpecial);         // 79
    PRINT(RichCommissioning);   // 80
    PRINT(RichError);           // 81
    PRINT(FTSpecial);           // 82
    PRINT(CaloSpecial);         // 83
    PRINT(Plume);               // 84
    PRINT(PlumeSpecial);        // 85
    PRINT(PlumeError);          // 86
    PRINT(VeloThresholdScan);   // 87
    PRINT(FTError);             // 88
    /// DAQ errors:
    PRINT(DaqErrorFragmentThrottled);   // 89
    PRINT(DaqErrorBXIDCorrupted);       // 90 
    PRINT(DaqErrorSyncBXIDCorrupted);   // 91
    PRINT(DaqErrorFragmentMissing);     // 92
    PRINT(DaqErrorFragmentTruncated);   // 93
    PRINT(DaqErrorIdleBXIDCorrupted);   // 94
    PRINT(DaqErrorFragmentMalformed);   // 95
    PRINT(DaqErrorEVIDJumped);          // 96

  default:
    return "UNKNOWN";
#undef PRINT
  }
}
