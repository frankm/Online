//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//              B.Jost
//==========================================================================

// Framework include files
#include <PCIE40Data/pcie40encoder.h>
#include <PCIE40Data/pcie40_writer.h>
#include <PCIE40Data/RawBank40.h>
#include <PCIE40Data/sodin.h>
#include <Tell1Data/RunInfo.h>


// C/C++ include files
#include <map>
#include <cmath>
#include <vector>
#include <cstdio>
#include <cstring>
#include <cstdarg>

using namespace Online;
using namespace Online::pcie40;

#define __UNLIKELY( x ) __builtin_expect( ( x ), 0 )

/// Helper class to store MFP information from MDF file read
/*
 * \author  M.Frank
 * \version 1.0
 * \date    15/09/2019
 */
class encoder_t::build_multi_fragment_t  final  {
public:
  unsigned long               event_id = 0;
  unsigned short              source_id = 0;
  unsigned char               version = 0;
  std::vector<unsigned char>  types;
  std::vector<unsigned short> sizes;
  std::vector<unsigned char>  data;

public:
  /// Inhibit default constructor
  build_multi_fragment_t() = delete;
  /// Initializing constructor
  build_multi_fragment_t(unsigned long eid, unsigned short sid, unsigned char vsn);
  /// Move constructor
  build_multi_fragment_t(build_multi_fragment_t&& copy) = default;
  /// Copy constructor
  build_multi_fragment_t(const build_multi_fragment_t& copy) = default;
  /// Move assignment
  build_multi_fragment_t& operator=(build_multi_fragment_t&& copy) = default;
  /// Copy assignment
  build_multi_fragment_t& operator=(const build_multi_fragment_t& copy) = default;
  /// Default destructor
  ~build_multi_fragment_t() = default;
};

/// Helper class to store MFP information from MDF file read
/*
 * \author  M.Frank
 * \version 1.0
 * \date    15/09/2019
 */
class encoder_t::data_t  final  {
public:
  /// Map of source id to MFPs
  std::map<unsigned short,build_multi_fragment_t> data;

public:
  /// Default constructor
  data_t() = default;
  /// Move constructor
  data_t(data_t&& copy) = default;
  /// Copy constructor
  data_t(const data_t& copy) = default;
  /// Move assignment
  data_t& operator=(data_t&& copy) = default;
  /// Copy assignment
  data_t& operator=(const data_t& copy) = default;
  /// Default destructor
  ~data_t() = default;
  /// Patch ODIN banks to inject TAE sequences
  void patch_odin_banks(const tae_options& tae_opts)   {
    for ( auto& pf : data )    {
      build_multi_fragment_t& f = pf.second;
      if ( !f.types.empty() && f.types[0] == RawBank40::ODIN )   {
	const auto*   sizes = &f.sizes.at(0);
	unsigned char*  ptr = &f.data.at(0);
	std::size_t packing = f.sizes.size();
	int      window_len = 0;
	bool       have_tae = false;
	double  probability = tae_opts.fraction / (2*tae_opts.half_window + 1);
	for(std::size_t i=0; i < packing; ++i)    {
	  if ( !have_tae && (i + 2*tae_opts.half_window + 1) < packing )  {
	    have_tae = probability < (double(::rand()) / double(RAND_MAX));
	  }
	  if ( have_tae && window_len <= 2*tae_opts.half_window )    {
	    if ( f.version < 7 )    {
	      RunInfo* odin      = (RunInfo*)ptr;
	      odin->TAEWindow    = tae_opts.half_window;
	      odin->triggerType |= OdinData::TimingTrigger;
	    }
	    else  {
	      sodin_t* odin      = (sodin_t*)ptr;
	      odin->_tae_window  = tae_opts.half_window;
	      odin->_tae_central = (window_len == tae_opts.half_window) ? 1 : 0;
	    }
	    ptr += sizes[i];
	    ++window_len;
	    continue;
	  }
	  have_tae = false;
	  ptr += sizes[i];
	  window_len = 0;
	}
	return;
      }
    }
  }
};


/// Initializing constructor
encoder_t::build_multi_fragment_t::build_multi_fragment_t(unsigned long  eid,
							  unsigned short sid,
							  unsigned char  vsn)
  : event_id(eid), source_id(sid), version(vsn)
{
}

/// Default constructor
encoder_t::encoder_t()
{
  data = new data_t();
}

/// Default destructor
encoder_t::~encoder_t()
{
  delete data;
}

static const unsigned char* __bank_detector_map() {
  static unsigned char* detector_map = 0;
  if ( 0 == detector_map )  {
    detector_map = new unsigned char[256];
    ::memset(detector_map,0,256);
    detector_map[bank_t::L0Calo] = 25;        //  0
    detector_map[bank_t::L0DU] = 17;            //  1
    detector_map[bank_t::PrsE] = 18;            //  2
    detector_map[bank_t::EcalE]= params::detectorid_Ecal;           //  3
    detector_map[bank_t::HcalE]= params::detectorid_Hcal;           //  4
    detector_map[bank_t::PrsTrig] = 31;         //  5
    detector_map[bank_t::EcalTrig] = 16;        //  6
    detector_map[bank_t::HcalTrig] = 16;        //  7
    detector_map[bank_t::Velo] = 2;            //  8
    detector_map[bank_t::Rich] = params::detectorid_Rich1;            //  9
    detector_map[bank_t::TT] = 19;              // 10
    detector_map[bank_t::IT] = 20;              // 11
    detector_map[bank_t::OT] = 21;              // 12
    detector_map[bank_t::Muon] = params::detectorid_MuonA;            // 13
    detector_map[bank_t::L0PU] = 22;            // 14
    detector_map[bank_t::DAQ] = 23;             // 15
    detector_map[bank_t::ODIN] = params::detectorid_ODIN;            // 16
    detector_map[bank_t::HltDecReports] = 30;   // 17
    detector_map[bank_t::VeloFull] = params::detectorid_VPA;        // 18
    detector_map[bank_t::TTFull] = 19;          // 19
    detector_map[bank_t::ITFull] = 20;          // 20
    detector_map[bank_t::EcalPacked]= params::detectorid_Ecal;      // 21
    detector_map[bank_t::HcalPacked]= params::detectorid_Hcal;      // 22
    detector_map[bank_t::PrsPacked] = 18;       // 23
    detector_map[bank_t::L0Muon] = 24;          // 24
    detector_map[bank_t::ITError] = 20;         // 25
    detector_map[bank_t::TTError] = 19;         // 26
    detector_map[bank_t::ITPedestal] = 20;      // 27
    detector_map[bank_t::TTPedestal] = 19;      // 28
    detector_map[bank_t::VeloError] = params::detectorid_VPA;       // 29
    detector_map[bank_t::VeloPedestal] = params::detectorid_VPA;    // 30
    detector_map[bank_t::VeloProcFull] = params::detectorid_VPA;    // 31
    detector_map[bank_t::OTRaw] = 21;           // 32
    detector_map[bank_t::OTError] = 21;         // 33
    detector_map[bank_t::EcalPackedError]= params::detectorid_Ecal; // 34
    detector_map[bank_t::HcalPackedError]= params::detectorid_Hcal; // 35
    detector_map[bank_t::PrsPackedError] = 18;  // 36
    detector_map[bank_t::L0CaloFull] = 25;      // 37
    detector_map[bank_t::L0CaloError] = 25;     // 38
    detector_map[bank_t::L0MuonCtrlAll] = 29;   // 39
    detector_map[bank_t::L0MuonProcCand] = 28;  // 40
    detector_map[bank_t::L0MuonProcData] = 27;  // 41
    detector_map[bank_t::L0MuonRaw] = 29;       // 42
    detector_map[bank_t::L0MuonError] = 29;     // 43
    detector_map[bank_t::GaudiSerialize] = 30;  // 44
    detector_map[bank_t::GaudiHeader] = 30;     // 45
    detector_map[bank_t::TTProcFull] = 19;      // 46
    detector_map[bank_t::ITProcFull] = 20;      // 47
    detector_map[bank_t::TAEHeader]= params::detectorid_Ecal;       // 48
    detector_map[bank_t::MuonFull] = params::detectorid_MuonA;        // 49
    detector_map[bank_t::MuonError] = params::detectorid_MuonA;       // 50
    detector_map[bank_t::TestDet] = 15;         // 51
    detector_map[bank_t::L0DUError] = 21;       // 52
    detector_map[bank_t::HltRoutingBits] = 30;  // 53
    detector_map[bank_t::HltSelReports] = 30;   // 54
    detector_map[bank_t::HltVertexReports] = 30;   // 55
    detector_map[bank_t::HltLumiSummary] = 30;  // 56
    detector_map[bank_t::L0PUFull] = 20;        // 57
    detector_map[bank_t::L0PUError] = 20;       // 58
    detector_map[bank_t::DstBank]= params::detectorid_Ecal;         // 59
    detector_map[bank_t::DstData]= params::detectorid_Ecal;         // 60
    detector_map[bank_t::DstAddress]= params::detectorid_Ecal;      // 61
    detector_map[bank_t::FileID]= params::detectorid_Ecal;          // 62
    detector_map[bank_t::VP] = params::detectorid_VPA;              // 63
    detector_map[bank_t::FTCluster] = params::detectorid_FTA;       // 64
    detector_map[bank_t::VL] = params::detectorid_VPA;              // 65
    detector_map[bank_t::UT] = params::detectorid_UTA;              // 66
    detector_map[bank_t::UTFull] = params::detectorid_UTA;          // 67
    detector_map[bank_t::UTError] = params::detectorid_UTA;         // 68
    detector_map[bank_t::UTPedestal] = params::detectorid_UTA;      // 69
    detector_map[bank_t::HC] = 26;              // 70
    detector_map[bank_t::HltTrackReports] = 30; // 71
    detector_map[bank_t::HCError] = 26;         // 72
    detector_map[bank_t::VPRetinaCluster] =  params::detectorid_VPA;   // 73
    detector_map[bank_t::FTGeneric] =  params::detectorid_FTA;         // 74
    detector_map[bank_t::FTCalibration] =  params::detectorid_FTA;     // 75
    detector_map[bank_t::FTNZS] =  params::detectorid_FTA;             // 76
    detector_map[bank_t::Calo] =  params::detectorid_Ecal;              // 77
    detector_map[bank_t::CaloError] =  params::detectorid_Ecal;         // 78
    detector_map[bank_t::MuonSpecial] =  params::detectorid_MuonA;       // 79
    detector_map[bank_t::RichCommissioning] =  params::detectorid_Rich1; // 80
    detector_map[bank_t::RichError] =  params::detectorid_Rich1;         // 81
    detector_map[bank_t::FTSpecial] =  params::detectorid_FTA;         // 82
    detector_map[bank_t::CaloSpecial] =  params::detectorid_Ecal;       // 83
    detector_map[bank_t::Plume] =  params::detectorid_Plume;             // 84
    detector_map[bank_t::PlumeSpecial] =  params::detectorid_Plume;      // 85
    detector_map[bank_t::PlumeError] =  params::detectorid_Plume;        // 86
    detector_map[bank_t::VeloThresholdScan] =  params::detectorid_VPA; // 87 Hardware only ?
    detector_map[bank_t::FTError] =  params::detectorid_FTA;           // 88
    detector_map[bank_t::LastType] = 0; // LOOP Marker; add new bank types ONLY before!
  }
  return detector_map;
}

/// Access packing factor for the contained MFPs
std::size_t encoder_t::packingFactor()   const   {
  if ( data && !data->data.empty() )   {
    return data->data.begin()->second.sizes.size();
  }
  return 0;
}

/// Append a new MDF event (consecutive raw banks) to the MEP
void encoder_t::append(std::size_t eid, const void* buff, std::size_t len)  {
  static constexpr unsigned short not_top_5 = (unsigned short)((0xFFFF>>5)&0xFFFF);
  const unsigned char* detector_map = __bank_detector_map();

  const unsigned char* start = (const unsigned char*)buff;
  const unsigned char* end   = start + len;
  while( start < end )   {
    const  bank_t* b = (const bank_t*)start;
    std::size_t blen = b->totalSize();
    // HACK::: Manually fix ODIN abnk version to 7!
    if ( b->type() == bank_t::ODIN )   {
      const_cast<bank_t*>(b)->setVersion(7);
    }
    bool skip = false;
    for(uint8_t bt : skip_banks)   {
      if ( b->type() == bt )  {
	skip = true;
	break;
      }
    }
    if ( skip )   {
      // Skip!
    }
    else if ( b->type() == bank_t::DAQ )   {
      // Skip!
    }
    else   {
      const unsigned char* ptr = add_ptr<unsigned char>(b,b->hdrSize());
      unsigned short sid = b->sourceID();
      unsigned char  vsn = b->version();
      /// Patch source IDs (with APPROXIMATE detector id)
      if ( this->patch_source_ids )   {
	sid = ((detector_map[b->type()]<<11)&params::sourceid_TOP5) + (sid&not_top_5);
	if ( b->sourceID() != (b->sourceID()&not_top_5) )    {
	  const char* btype = "not interpreted";
	  ::printf("Inconsistent source ID: %04X > %04X  Type:%4d size:%4d [%s]\n",
		   b->sourceID(), (b->sourceID()&not_top_5), int(b->type()), int(b->size()), btype);
	}
      }

      auto i = data->data.find(sid);
      if ( i == data->data.end() )   {
	i = data->data.insert(std::make_pair(sid,build_multi_fragment_t(eid, sid, vsn))).first;
	i->second.types.reserve(3600);
	i->second.sizes.reserve(3600);
	i->second.data.reserve(100*1024);
      }
      build_multi_fragment_t& f = i->second;
      f.types.emplace_back(b->type());
      f.sizes.emplace_back(b->size());
      f.data.insert(f.data.end(), ptr, ptr+blen-b->hdrSize());
      // ::printf("Add Bank: eid:%4ld sid:%4d typ:%3d len:%4d\n",eid,int(sid),int(b->type()),int(b->size()));
    }
    start += blen;
  }
}

/// Fix fragment: extend sizes and types array of MFP if necessary
void encoder_t::fix_fragment()  const  {
  std::size_t num_frag = 0;
  for( const auto& pf : data->data )    {
    num_frag = std::max(num_frag, pf.second.sizes.size());
  }
  for( auto& pf : data->data )    {
    auto&   f = pf.second;
    while ( f.sizes.size() < num_frag ) {
      f.sizes.emplace_back(0);
      f.types.emplace_back(0);
    }
  }
}

/// Reset internals and reallocate data structures
void encoder_t::reset()   {
  delete data;
  data = new data_t();
}

namespace  {
  /// Total record size EXcluding the MEP header
  std::size_t multi_fragment_length(const encoder_t::build_multi_fragment_t& mfp,
			       std::size_t alignment)
  {
    std::size_t len = sizeof(multi_fragment_header_t);
    std::size_t nf  = mfp.sizes.size();
    len += round_up(sizeof(uint8_t)*nf,multi_fragment_header_t::types_alignment);
    len += round_up(sizeof(uint16_t)*nf,alignment);
    len += round_up(sizeof(uint8_t)*mfp.data.size(),alignment);
    return len;
  }
  std::size_t _get_padding(std::size_t data_len, std::size_t alignment)   {
    return round_up(data_len,alignment)-data_len;
  }
}

/// Total record size INcluding the MEP header
std::size_t encoder_t::record_length()  const  {
  std::size_t mfp_cnt = data->data.size();
  std::size_t rec_len = sizeof(mep_header_t);
  rec_len += round_up(sizeof(uint16_t)*mfp_cnt,multi_fragment_header_t::types_alignment);
  rec_len += sizeof(uint32_t)*mfp_cnt;
  for(const auto& pf : data->data)
    rec_len += multi_fragment_length(pf.second, 1<<fragment_alignment);
  return rec_len;
}

/// Output MEP record to writer object
std::pair<std::size_t,std::size_t> encoder_t::write_record(output_t& writer)  const  {
  mep_header_t            hdr;
  multi_fragment_header_t frg_hdr;
  std::vector<uint16_t>   source_ids;
  std::vector<uint32_t>   mfp_offsets;
  std::size_t len = 0, mfp_cnt = data->data.size(), padding;
  uint32_t the_offset = 0;
  char pad[1024];

  ::memset(pad, 0, sizeof(pad));

  /// Patch the bank types of empty banks
  for ( auto& pf : data->data )    {
    auto& f = pf.second;
    for ( auto t : f.types )   {
      if ( t != 0 )  {
	for(std::size_t i=0, n=f.types.size(); i<n; ++i)
	  if ( 0 == f.types[i] ) f.types[i] = t;
	break;
      }
    }
  }
  /// Add the array of source IDs
  for(const auto& pf : data->data)
    source_ids.emplace_back(pf.first);
  /// Add the array of offsets
  the_offset = sizeof(hdr) + 
    round_up(sizeof(uint16_t)*mfp_cnt,multi_fragment_header_t::types_alignment) +
    sizeof(uint32_t)*mfp_cnt;
  for(const auto& pf : data->data)  {
    mfp_offsets.emplace_back(the_offset/sizeof(uint32_t));
    the_offset += multi_fragment_length(pf.second, 1<<fragment_alignment);
  }

  std::size_t rec_len = record_length();
  std::size_t hdr_padding = _get_padding(rec_len,sizeof(uint32_t));
  if ( hdr_padding != 0 )   {
    ::printf("Invalid MEP header padding: %ld\n", hdr_padding);
  }
  hdr.size       = (rec_len+hdr_padding) / sizeof(uint32_t);
  hdr.num_source = data->data.size();
  len += writer.output(&hdr, sizeof(hdr));
  len += writer.output(&source_ids[0], sizeof(uint16_t)*mfp_cnt);
  padding = _get_padding(sizeof(uint16_t)*mfp_cnt,multi_fragment_header_t::types_alignment);
  if ( padding ) len += writer.output(pad, sizeof(uint8_t)*padding);
  len += writer.output(&mfp_offsets[0], sizeof(uint32_t)*mfp_cnt);

  for(const auto& pf : data->data)    {
    uint16_t       sid = pf.first;
    const auto&    f   = pf.second;
    std::size_t    nf  = f.sizes.size();

    frg_hdr.event_id   = f.event_id;
    frg_hdr.source_id  = sid;
    frg_hdr.packing    = nf;
    frg_hdr.size       = multi_fragment_length(f, 1<<fragment_alignment);
    frg_hdr.alignment  = fragment_alignment;
    frg_hdr.version    = f.version;

    len += writer.output(&frg_hdr, sizeof(frg_hdr));
    len += writer.output(&f.types[0], sizeof(uint8_t)*nf);
    padding = _get_padding(sizeof(uint8_t)*nf,multi_fragment_header_t::types_alignment);
    if ( padding ) len += writer.output(pad, sizeof(uint8_t)*padding);

    len += writer.output(&f.sizes[0], sizeof(uint16_t)*nf);
    padding = _get_padding(sizeof(uint16_t)*nf, 1<<fragment_alignment);
    if ( padding ) len += writer.output(pad, sizeof(uint8_t)*padding);

    len += writer.output(&f.data[0],  f.data.size());
    padding = _get_padding(sizeof(uint8_t)*f.data.size(), 1<<fragment_alignment);
    if ( padding ) len += writer.output(pad, sizeof(uint8_t)*padding);
  }
  if ( hdr_padding ) len += writer.output(pad, sizeof(uint8_t)*hdr_padding);
  return std::make_pair(data->data.size(), len);
}

/// Output MEP record to writer object
std::pair<std::size_t,std::size_t>
encoder_t::write_record(output_t& writer, const tae_options& tae_opts)  const  {
  /// Patch ODIN banks to inject TAE sequences
  if ( tae_opts.fraction > 0e0 )   {
    data->patch_odin_banks(tae_opts);
  }
  return this->write_record(writer);
}
