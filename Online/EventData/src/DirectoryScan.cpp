//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DirectoryScan.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "Tell1Data/DirectoryScan.h"
#include "RTL/readdir.h"
#include "RTL/rtl.h"

/// C/C++ include files
#include <sys/stat.h>
#include <fcntl.h>
#include <cerrno>

using namespace Online;
using namespace std;


/// Add single file to the file map
size_t DirectoryScan::addFile(const std::string& dir_name,
			      const std::string& name,
			      const std::string& prefix,
			      const std::vector<std::string>& allowedRuns,
			      bool take_all)
{
  if ( !prefix.empty() && 0 != ::strncmp(name.c_str(),prefix.c_str(),prefix.length()) ) {
    return 0;
  }
  else if ( !take_all )  {
    bool take_run = false;
    for(vector<string>::const_iterator i=allowedRuns.begin(); i!=allowedRuns.end(); ++i) {
      if ( name.find((*i)) == 0) {
	take_run = true;
	break;
      }
    }
    if ( !take_run )   {
      return 0;
    }
  }
  // Remove the identified bad ones from the list...
  string  fname = dir_name.empty() ? name : (dir_name + "/" + name);
  if ( badFiles.find(fname) != badFiles.end() )
    return 0;

  files.emplace(name,RawFile(fname));
  return 1;
}

/// Scan single directory for matching files
size_t DirectoryScan::scanDirectory(const std::string& name,
				    const std::string& filePrefix,
				    const std::vector<std::string>& allowedRuns)
{
  if ( !name.empty() )   {
    struct stat file;
    if ( 0 == ::stat(name.c_str(),&file) )   {
      bool take_all = (allowedRuns.size() > 0 && allowedRuns[0]=="*");
      if ( S_ISREG(file.st_mode) )   {
	/// Regulat file: Add it to the list
	return addFile("", name, "", std::vector<std::string>(), true);
      }
      else if ( S_ISLNK(file.st_mode) )   {
	/// Symbolic link. Read it and check if it is a file
	char path[PATH_MAX];
	if ( ::readlink(name.c_str(), path, sizeof(path)) > 0 )   {
	  if ( 0 == ::stat(path,&file) )   {
	    if ( S_ISREG(file.st_mode) )   {
	      return addFile("", path, "", std::vector<std::string>(), true);
	    }
	  }
	}
      }
      DIR* dir = opendir(name.c_str());
      if (dir)  {
	struct dirent *entry;
	while ((entry = ::readdir(dir)) != 0)    {
	  addFile(name, entry->d_name, filePrefix, allowedRuns, take_all);
	}
	::closedir(dir);
	m_logger->info("Scanned directory %s: %d files.",name.c_str(), int(files.size()));
	return files.size();
      }
    }
    auto err = RTL::errorString();
    m_logger->info("Failed to open directory: %s",!err.empty() ? err.c_str() : "????????");
  }
  return 0;
}

/// Scan directories for matching files
size_t DirectoryScan::scanFiles(const std::vector<std::string>& directories,
				const std::string& prefix,
				const std::vector<std::string>& allowedRuns)
{
  for(size_t i=0; i<directories.size(); ++i)  {
    scanDirectory(directories[i], prefix, allowedRuns);
  }
  return files.size();
}

/// Read broken hosts file
bool DirectoryScan::isBrokenHost(const std::string& fname)  const   {
  string broken_hosts = fname;
  if ( !broken_hosts.empty() ) {
    struct stat file;
    if ( 0 == ::stat(broken_hosts.c_str(),&file) ) {
      const string node = RTL::nodeNameShort();
      int fd = ::open(broken_hosts.c_str(),O_RDONLY);
      if ( -1 == fd )  {
        m_logger->error("Failed to access broken node file: %s [Error ignored]",broken_hosts.c_str());
	return false;
      }
      char* data = new char[file.st_size+1];
      int rc = ::read(fd,data,file.st_size);
      if ( 1 == rc ) {
        data[file.st_size] = 0;
        for(int i=0; i<file.st_size; ++i) 
          data[i] = ::toupper(data[i]);
        for(char* ptr=(char*)node.c_str(); *ptr; ++ptr)
          *ptr = ::toupper(*ptr);
        if ( ::strstr(data,node.c_str()) ) {
          m_logger->warning("Node is disabled and will not process any deferred files.");
          return true;
        }
      }
      delete [] data;
      ::close(fd);
    }
    else {
      m_logger->error("Failed to access broken node file:%s [Error ignored]",broken_hosts.c_str());
    }
  }
  return false;
}
