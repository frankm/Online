//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//              B.Jost
//==========================================================================
#ifndef ONLINE_PCIE40DATA_PCIE40ENCODER_H
#define ONLINE_PCIE40DATA_PCIE40ENCODER_H

// Framework include files
#include <PCIE40Data/pcie40.h>

// C/C++ include files
#include <memory>
#include <vector>

/// Online namespace declaration
namespace Online {

  /// pcie40 namespace declaration
  namespace pcie40  {

    /// Encoder class: Inputs MDF raw banks. Creates MEP structures according to EDMS 2100937
    /*
     * \author  M.Frank
     * \version 1.0
     * \date    15/09/2019
     */
    class encoder_t  final {
    private:
      /// Helper class to store MFP information from MDF file read
      class data_t;

      /// Pointer to data block
      data_t* data {0};

    public:

      /// Helper class to force injecting TAE sequences
      class tae_options  {
      public:
	uint8_t half_window = 0;
	double  fraction    = 0e0;
      };

      /// Helper class to store MFP information from MDF file read
      class build_multi_fragment_t;

      /// Default MFP alignment.
      /**  According to EDMS 2100937 the byte alignmant is then 
       *   2**fragment_alignment bytes.
       */
      uint8_t fragment_alignment {2};

      /// Patch source IDs (with APPROXIMATE detector id)
      uint8_t patch_source_ids   {false};

      /// Choose which banks should be skipped
      std::vector<uint8_t> skip_banks {};

      /// Class template to output the encoded result (e.g. to file)
      class output_t {
      public:
	/// Default destructor
	virtual ~output_t() = default;
	/// Output method signature
	virtual size_t output(const void* buffer, size_t len) = 0;
      };

    public:
      /// Default constructor
      encoder_t();
      /// INHIBIT Move constructor
      encoder_t(encoder_t&& copy) = delete;
      /// INHIBIT Copy constructor
      encoder_t(const encoder_t& copy) = delete;
      /// INHIBIT Move assignment
      encoder_t& operator=(encoder_t&& copy) = delete;
      /// INHIBIT Copy assignment
      encoder_t& operator=(const encoder_t& copy) = delete;
      /// Default destructor
      ~encoder_t();

      const unsigned char* bank_detector_map()  const;
      /// Access packing factor for the contained MFPs
      size_t packingFactor()   const;
      /// Append a new MDF event (consecutive raw banks) to the MEP
      void append(size_t eid, const void* buff, size_t len);
      /// Fix fragment: extend sizes and types array of MFP if necessary
      void fix_fragment()  const;
      /// Reset internals and reallocate data structures
      void reset();
      /// Total record size INcluding the MEP header
      size_t record_length()  const;
      /// Output MEP record to writer object
      std::pair<size_t,size_t> write_record(output_t& writer)  const;
      /// Output MEP record to writer object
      std::pair<size_t,size_t> write_record(output_t& writer,const tae_options& tae_opts)  const;
    };
  }  // namespace pcie40
}    // namepace Online
#endif // ONLINE_PCIE40DATA_PCIE40ENCODER_H
