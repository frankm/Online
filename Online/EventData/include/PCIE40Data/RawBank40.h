//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//==========================================================================
/*
 * RawBank.h
 *
 *  Created on: Apr 2, 2019
 *      Author: beat
 */
#ifndef ONLINE_PCIE40DATA_PCIE40DATA_RAWBANK40_H_
#define ONLINE_PCIE40DATA_PCIE40DATA_RAWBANK40_H_

#include <cstdio>
#include <iostream>
#include "PCIE40Data/Pack.h"

/// Online namespace declaration
namespace Online {

  /** @class LHCb::RawBank40 RawBank40.h
   *
   * Online Raw data bank created from MEPs sent by the PCIe40 or from MDF RawBanks read from file
   *
   * For a detailed description of the raw bank format,
   * see <a href="https://edms.cern.ch/document/565851/5">EDMS-565851/5</a>
   *
   * Note concerning the changes done 06/03/2006:
   * - The bank size is in BYTES
   * - The full size of a bank in memory is long word (32 bit) aligned
   *   ie. a bank of 17 Bytes length actually uses 20 Bytes in memory.
   * - The bank length accessors size() and setSize() do not contain
   *   the bank header, ie. size() = (total size) - (header size).
   * - The length passed to the RawEvent::createBank should NOT
   *   contain the size of the header !
   * - If the full padded bank size is required use the utility
   *   function RawBank40::totalSize = size + header size + padding size.
   *
   * @author Helder Lopes
   * @author Markus Frank
   * @author Beat Jost
   * created Tue Oct 04 14:45:20 2005
   *
   */
  class PACKED_DATA RawBank40
  {
  private:
    /// Magic word (by definition 0xFACE)
    unsigned short m_magic {MagicPattern};
    /// Bank length in bytes (must be >= 0)
    unsigned short m_length {0};
    /// Bank type (must be >= 0)
    unsigned char  m_type {0};
    /// Version identifier (must be >= 0)
    unsigned char  m_version {0};
    /// Source ID (valid source IDs are > 0; invalid ones 0)
    unsigned short m_sourceID {0};
    /// Opaque data block
    const unsigned int *m_data {0};

  public:
    /// Dummy Constructor
    RawBank40() = default;

    /// Define bank types for RawBank
    enum BankType {
      L0Calo = 0,        //  0
      L0DU,              //  1
      PrsE,              //  2
      EcalE,             //  3
      HcalE,             //  4
      PrsTrig,           //  5
      EcalTrig,          //  6
      HcalTrig,          //  7
      Velo,              //  8
      Rich,              //  9
      TT,                // 10
      IT,                // 11
      OT,                // 12
      Muon,              // 13
      L0PU,              // 14
      DAQ,               // 15
      ODIN,              // 16
      HltDecReports,     // 17
      VeloFull,          // 18
      TTFull,            // 19
      ITFull,            // 20
      EcalPacked,        // 21
      HcalPacked,        // 22
      PrsPacked,         // 23
      L0Muon,            // 24
      ITError,           // 25
      TTError,           // 26
      ITPedestal,        // 27
      TTPedestal,        // 28
      VeloError,         // 29
      VeloPedestal,      // 30
      VeloProcFull,      // 31
      OTRaw,             // 32
      OTError,           // 33
      EcalPackedError,   // 34
      HcalPackedError,   // 35
      PrsPackedError,    // 36
      L0CaloFull,        // 37
      L0CaloError,       // 38
      L0MuonCtrlAll,     // 39
      L0MuonProcCand,    // 40
      L0MuonProcData,    // 41
      L0MuonRaw,         // 42
      L0MuonError,       // 43
      GaudiSerialize,    // 44
      GaudiHeader,       // 45
      TTProcFull,        // 46
      ITProcFull,        // 47
      TAEHeader,         // 48
      MuonFull,          // 49
      MuonError,         // 50
      TestDet,           // 51
      L0DUError,         // 52
      HltRoutingBits,    // 53
      HltSelReports,     // 54
      HltVertexReports,  // 55
      HltLumiSummary,    // 56
      L0PUFull,          // 57
      L0PUError,         // 58
      DstBank,           // 59
      DstData,           // 60
      DstAddress,        // 61
      FileID,            // 62
      VP,                // 63  Does not exist in hardware. Only SIM
      FTCluster,         // 64
      VL,                // 65
      UT,                // 66
      UTFull,            // 67
      UTError,           // 68
      UTPedestal,        // 69
      HC,                // 70
      HltTrackReports,   // 71
      HCError,           // 72
      VPRetinaCluster,   // 73
      FTGeneric,         // 74
      FTCalibration,     // 75
      FTNZS,             // 76
      Calo,              // 77
      CaloError,         // 78
      MuonSpecial,       // 79
      RichCommissioning, // 80
      RichError,         // 81
      FTSpecial,         // 82
      CaloSpecial,       // 83
      Plume,             // 84
      PlumeSpecial,      // 85
      PlumeError,        // 86
      VeloThresholdScan, // 87 Hardware only ?
      FTError,           // 88
      // Banks above are reserved for DAQ, add only generic DaqError types below.
      DaqErrorFragmentThrottled, // 89
      DaqErrorBXIDCorrupted,     // 90
      DaqErrorSyncBXIDCurrupted, // 91
      DaqErrorFragmentMissing,   // 92
      DaqErrorFragmentTruncated, // 93
      DaqErrorIdleBXIDCorrupted, // 94
      DaqErrorFragmentMalformed, // 95
      DaqErrorEVIDJumped,        // 96
      LastType, // LOOP Marker; add new bank types ONLY before!
      Other                     = 1,
    };

    /// Magic pattern for Raw bank headers
    enum RawPattern    {
      MagicPattern = 0xFACE
    };

    /// Access to magic word for integrity check
    int magic() const               {  return m_magic;                              }

    /// Set magic word
    void setMagic()                 {  m_magic = MagicPattern;                      }

    /// Header size
    static int hdrSize()  {  return sizeof(RawBank40) - sizeof(RawBank40::m_data);   }

    /// Return size of the data body part of the bank
    int size() const                {  return m_length - hdrSize();                  }

    /// Set data size of the bank in bytes
    void setSize(size_t val)        {  m_length = (val&0xFFFF) + hdrSize();          }

    /// Access the full (padded) size of the bank
    int totalSize() const           {
      typedef unsigned int T;
      return m_length % sizeof(T) == 0 ? m_length : (m_length / sizeof(T) + 1) * sizeof(T);
    }
    /// Return bankType of this bank
    BankType type() const           {  return BankType(int(m_type));                 }

    /// Set the bank type
    void setType(BankType val)      {  m_type = (unsigned char) (char(val) & 0xFF);  }

    /// Return version of this bank
    int version() const             {  return m_version;                             }

    /// Set the version information of this bank
    void setVersion(int val)        {  m_version = (unsigned char) (val & 0xFF);     }

    /// Return SourceID of this bank  (TELL1 board ID)
    unsigned short sourceID() const {  return m_sourceID;                            }

    /// Set the source ID of this bank (TELL1 board ID)
    void setSourceID(int val)       {  m_sourceID = (unsigned short) (val & 0xFFFF); }

    /// Return pointer to beginning of data body of the bank (const data access)
    const unsigned int* data() const{  return m_data;                                }

    void setData(const void *val)   {  m_data = (const unsigned int*) val;           }

    /// Begin iterator
    template<typename T> T* begin() {  return (T*) m_data;                           }

    /// End iterator
    template<typename T> T* end()   {  return ((T*) m_data) + size() / sizeof(T);    }

    /// Begin iterator over const iteration
    template<typename T> const T* begin() const { return (const T*) m_data;          }

    /// End iterator of const iteration
    template<typename T> const T* end() const  {
      return ((const T*) m_data) + size() / sizeof(T);
    }
  };

  /// Output streamer
  inline std::ostream& operator <<(std::ostream& os, const RawBank40& b)  {
    char text[256];
    ::snprintf(text,sizeof(text),
	       "RawBank40: magic: %0x Length: %d Type: %d Version: %d SourceID: %0x",
	       int(b.magic()), int(b.totalSize()), int(b.type()), int(b.version()), int(b.sourceID()));
    os << text;
    return os;
  }
}    // namepace Online

#include "PCIE40Data/Unpack.h"
#endif /* ONLINE_PCIE40DATA_PCIE40DATA_RAWBANK40_H_ */
