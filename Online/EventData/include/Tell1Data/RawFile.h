//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  RawFile.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef TELL1DATA_RAWFILE_H
#define TELL1DATA_RAWFILE_H

/// Framework include files
#include "Tell1Data/Tell1Decoder.h"

/// C/C++ include files
#include <map>
#include <vector>

namespace Online  {

  class RawFile   {
  protected:
    /// File name
    std::string    m_name;
    /// File handle
    int            m_fd = -1;
    /// Reuse-flag
    int            m_reuse = 0;
    /// Mapping address (if file is memory mapped)
    unsigned char* m_begin = 0;
    /// End of mapped memory region (if file is memory mapped)
    unsigned char* m_end = 0;
    /// Current memory pointer (if file is memory mapped)
    unsigned char* m_ptr = 0;

  public:
    enum EventType {
      NO_INPUT_TYPE   = 0,
      AUTO_INPUT_TYPE = 1,
      MDF_INPUT_TYPE  = 2,
      MEP_INPUT_TYPE  = 3
    };
    /// Input buffer allocater signature
    struct Allocator {
      virtual ~Allocator() = default;
      virtual unsigned char* operator()(std::size_t memsize) = 0;
    };
    /// In-place allocator using pre-defined buffer
    struct InPlaceAllocator : public Allocator {
      unsigned char* ptr = 0;
      std::size_t len = 0;
      InPlaceAllocator(unsigned char* p, std::size_t l);
      InPlaceAllocator() = delete;
      InPlaceAllocator( InPlaceAllocator&& copy ) = delete;
      InPlaceAllocator( const InPlaceAllocator& copy ) = delete;
      virtual ~InPlaceAllocator() = default;
      InPlaceAllocator& operator=( const InPlaceAllocator& copy ) = delete;
      virtual unsigned char* operator()(std::size_t length)   override  final {
	return (unsigned char*)((length<=len) ? ptr : 0);
      }
    };

  public:
    RawFile() = default;
    /// Copy constructor
    RawFile(RawFile&& copy) = default;
    /// Copy constructor
    RawFile(const RawFile& copy) = default;
    /// Standard constructor
    RawFile(const std::string& nam);
    /// Standard constructor
    RawFile(int descriptor);
    /// Standard constructor
    RawFile(int descriptor, bool);
    /// Default destructor
    virtual ~RawFile() = default;
    /// Assignment
    RawFile& operator=(RawFile&& copy) = default;
    /// Assignment
    RawFile& operator=(const RawFile& copy) = default;

    /// Access file name
    const std::string& name() const          {  return m_name;            }
    /// Access file name
    const char* cname()  const               {  return m_name.c_str();    }
    /// Set file name
    void setName(const std::string& value)   {  m_name = value;           }
    /// Access file descriptor
    int fileno()  const                      {  return m_fd;              }
    /// Check if file is connected
    bool isOpen()  const                     {  return m_fd > 0;          }
    /// Check if the file is mapped
    bool isMapped()  const                   {  return m_begin != m_end;  }
    /// Acces the mapped file size
    std::size_t mapped_size()  const         {  return m_end - m_begin;   }
    /// Access the data size: file size of mapped size
    std::size_t data_size()  const;

    /// Start of the mapped data size
    const unsigned char* begin()  const      {  return m_begin;           }
    /// End of the mapped data size
    const unsigned char* end()  const        {  return m_end;             }
    /// Current pointer in the mapped data space
    const unsigned char* pointer()  const    {  return m_ptr;             }
    /// Advance the pointer in the mapped size
    const unsigned char* advance(long bytes) {  return (m_ptr += bytes);  }
    /// Set re-use flag
    void setReuse(bool value)                {  m_reuse = value ? 1 : 0;  }
    /// Seek to required 
    long seek(long offset, int whence);
    /// Read chunk of data
    long read(void* buffer, long len);
    /// Write chunk of data
    long write(const void* data, int len);
    /// Write chunk of data
    long write(const void* data, long len);
    /// Write chunk of data
    long write(const void* data, std::size_t len);
    /// Open file in read mode
    int  open(bool silent=true);
    /// Map memory of the file
    int  map_memory(const void* data, std::size_t len);
    /// Unmap memory of the file
    int  unmap_memory();
    /// Open file in read mode with mapping
    int  openMapped(bool silent=true);
    /// Open file in write mode
    int  openWrite(bool silent=true);
    /// Unlink file
    int  unlink();
    /// Return file position
    long position()  const;
    /// Set absolute file position
    long position(long offset, bool absolute=true);
    // Close file descriptor if open
    int  close();
    /// Reset information (close and reset name)
    int  reset(bool close=true);
    /// Create a directory structure
    static int  mkdir(const char* dir_name, int protection, bool silent=true);
    /// Delete a directory structure
    static int  rmdir(const char* dir_name, bool recursive, bool silent=true);

    /// Read single event using dynamic allocation
    long read_event(EventType expected, EventType& found_type,
		    Allocator& allocator, unsigned char** data);
    /// Read single event into fixed buffer
    long read_event(EventType expected, EventType& found_type,
		    unsigned char* data, std::size_t len);

    /// Read multiple events into fixed buffer (burst mode)
    std::pair<long,long>
      read_multiple_events(std::size_t num_events,
			   EventType expected, EventType& found_type,
			   unsigned char* p, std::size_t len);

    /// Search bad file and try to seek the next event record. Returns offset to next event
    long scanToNextMDF();
    /// Save remainder of currently read file
    void saveRestOfFile();
    /// Scan single record and check for consistency
    int checkRecord(std::size_t offset, const void* data, std::size_t length)   const;
    /// Check if the event header is correct
    int checkEventHeader(std::size_t offset, const void* data, std::size_t length)   const;
    /// Check content of bank
    int checkBank(std::size_t offset, const void* data)   const;
    /// Dump a set of raw banks
    void dump(const unsigned char* data, std::size_t len)  const;
    /// Dump the content of a single raw bank to screen
    void dump (const Tell1Bank* bank)  const;
  };

  inline RawFile::InPlaceAllocator::InPlaceAllocator(unsigned char* p, std::size_t l)
    : ptr(p), len(l)
  {
  }

}
#endif /* TELL1DATA_RAWFILE_H  */
