//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef GAUDIONLINE_TELL1DECODER_H
#define GAUDIONLINE_TELL1DECODER_H

#include "Tell1Data/Tell1Bank.h"
#include "Tell1Data/EventHeader.h"
#include <numeric>
#include <cstring>
#include <vector>
#include <map>

/*
 *    Online namespace
 */
namespace Online  {

  typedef std::pair<char*,int> DataDescriptor;

  /// one-at-time hash function
  unsigned int hash32Checksum(const void* ptr, size_t len);
  /// Adler32 checksum
  unsigned int adler32Checksum(unsigned int adler,const char* buf,size_t len);
  /// Generate  Checksum
  unsigned int genChecksum(int flag,const void* ptr, size_t len);

  /// Check sanity of raw bank structure
  bool checkRawBank(const Tell1Bank* b, bool throw_exc=true,bool print_cout = false);

  /// Check consistency of raw bank sequence using magic words
  bool checkRawBanks(const void* start, const void* end, bool throw_exc=true,bool print_cout = false);
#if 0
  /// Check consistency of MEP fragment
  bool checkFragment(const Tell1Fragment* f, bool exc=true, bool prt = false);

  /// Check consistency of MEP multi fragment
  bool checkMultiFragment(const Tell1MultiFragment* mf, bool exc=true, bool prt = false);

  /// Check consistency of MEP multi event
  bool checkMEPEvent (const Tell1MEP* me, bool exc=true, bool prt = false);
#endif
  /// Check consistency of MEP multi event fragment
  bool checkRecord(const EventHeader* h, int opt_len, bool exc,bool prt);

  /// Determine length of the sequential buffer from RawEvent object
  template <typename T> size_t rawEventLengthBanks(const T& banks)    {
    size_t len = 0;
    for(const auto* b : banks) len += b->totalSize();
    return len;
  }

  /// Conditional decoding of raw buffer from MDF to vector of raw banks
  int decodeRawBanks(const void* start, const void* end, std::vector<Tell1Bank*>& banks);
#if 0
  /// Decode MEP into bank collections
  int decodeMEP( const Tell1MEP* me, 
		  unsigned int&   partitionID, 
		  std::map<unsigned int,std::vector<Tell1Bank*> >& events);

  /// Decode MEP into Tell1 fragment collections
  int decodeMEP( const Tell1MEP* me, 
		 unsigned int&   partitionID, 
		 std::map<unsigned int,std::vector<Tell1Fragment*> >& events);
#endif
  /// Conditional decoding of raw buffer from MDF to bank offsets
  int decodeRawBanks(const void* start, const void* end, int* offsets, int* noffset);

  /// Conditional decoding of raw buffer from MDF to bank offsets. Data are passed as descriptor
  inline int decodeRawBanks(const DataDescriptor& data, int* offsets, int* noffset)    {
    const unsigned char* end = (const unsigned char*)data.first;
    return decodeRawBanks(data.first,end+data.second,offsets,noffset);
  }
#if 0
  /// Decode MEP into fragments event by event
  int decodeMEP2EventFragments(const Tell1MEP* me, 
			       unsigned int&   partitionID, 
			       std::map<unsigned int, std::vector<Tell1Fragment*> >& events);
  /// Decode MEP into banks event by event
  int decodeMEP2EventBanks(const Tell1MEP* me, 
			   unsigned int&   partitionID, 
			   std::map<unsigned int, std::vector<Tell1Bank*> >& events);

  /// Decode single fragment into a list of pairs (event id,bank)
  int decodeFragment2Banks(const Tell1Fragment* f, 
			   unsigned int event_id_high,
			   std::vector<std::pair<unsigned int,Tell1Bank*> >& banks);

  /// Decode multi fragment into a list of pairs (event id,bank)
  int decodeMultiFragment2Banks( const Tell1MultiFragment* mf, 
				 unsigned int&   partitionID, 
				 std::vector<std::pair<unsigned int,Tell1Bank*> >& banks);

  /// Decode MEP event into a plain list of pairs (event id,bank)
  int decodeMEP2Banks( const Tell1MEP* me, 
		       unsigned int&   partitionID, 
		       std::vector<std::pair<unsigned int,Tell1Bank*> >& banks);
#endif
  /// Copy RawEvent data from bank vectors to sequential buffer
  int encodeRawBank(const Tell1Bank* b, char* const data, size_t size, size_t* length);

  /// Copy RawEvent data from bank vectors to sequential buffer
  template <typename T>
    int encodeRawBanks(const T& banks, char* const data, size_t size, bool skip_hdr_bank, size_t* length)  {
    size_t len = 0, s;
    for(const Tell1Bank* b : banks)   {
      if ( skip_hdr_bank )  {
	if ( b->type() == Tell1Bank::DAQ )   {
	  if ( b->version() == DAQ_STATUS_BANK ) {
	    continue;
	  }
	}
      }
      s = b->totalSize();
      if ( size >= (len+s) )  {
	::memcpy(data+len, b, s);
	len += s;
	continue;
      }
      return 0;
    }
    if ( length ) *length = len;
    return 1;
  }
#if 0
  /// Copy array of bank pointers into opaque data buffery
  int encodeFragment(const std::vector<Tell1Bank*>& banks, Tell1Fragment* f);

  /// Copy MEP fragment into opaque sequential data buffer
  int encodeFragment(const Tell1Fragment* f, char* const data, size_t len);

  /// Conditional decoding of raw buffer from MDF to vector of bank pointers
  int decodeFragment(const Tell1Fragment* f, std::vector<Tell1Bank*>& raw);

  /// Return vector of MEP sub-event names
  std::vector<std::string> buffersMEP(const char* start);
#endif
  /// Returns the prefix on TES according to bx number, - is previous, + is next
  std::string rootFromBxOffset(int bxOffset);

  /// Access to the TAE bank (if present)
  Tell1Bank* getTAEBank(const char* start);

  /// Return vector of TAE event names
  std::vector<std::string> buffersTAE(const char* start);

  /// Returns the offset of the TAE with respect to the central bx
  int bxOffsetTAE(const std::string& root);

  class Tell1Printout {
  public:
    static std::string bankHeader( const Tell1Bank* r );
    static std::string bankType( const Tell1Bank* r );
    static std::string bankType( int i );
  }; 
}

#endif // GAUDIONLINE_TELL1DECODER_H
