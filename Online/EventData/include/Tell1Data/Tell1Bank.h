//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Tell1Bank.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_TELL1BANK_H
#define ONLINE_DATAFLOW_TELL1BANK_H 1

#include <Tell1Data/RunInfo.h>
#include <Tell1Data/Pack.h>
#include <cstdlib>
#include <iostream>

/// Online namespace declaration
namespace Online    {

  /// Tell1 data output from the LHCb online system
  /** @class Online::Tell1Bank Tell1Bank.h
   *
   * Raw data bank sent by the TELL1 boards of the LHCb DAQ.
   *
   * For a detailed description of the raw bank format,
   * see <a href="https://edms.cern.ch/document/565851/5">EDMS-565851/5</a>
   *
   * Note concerning the changes done 06/03/2006:
   * - The bank size is in BYTES
   * - The full size of a bank in memory is long word (32 bit) aligned
   *   ie. a bank of 17 Bytes length actually uses 20 Bytes in memory.
   * - The bank length accessors size() and setSize() do not contain
   *   the bank header, ie. size() = (total size) - (header size).
   * - The length passed to the RawEvent::createBank should NOT
   *   contain the size of the header !
   * - If the full padded bank size is required use the utility
   *   function Tell1Bank::totalSize = size + header size + padding size.
   *
   * @author Helder Lopes
   * @author Markus Frank
   * created Tue Oct 04 14:45:20 2005
   * 
   */
  class PACKED_DATA Tell1Bank  {
  protected:
    /// Default Constructor
    Tell1Bank() {}

    /// Default Destructor
    ~Tell1Bank() {}

  public:   

    /// Define bank types for RawBank
    enum BankType {
      L0Calo = 0,        //  0
      L0DU,              //  1
      PrsE,              //  2
      EcalE,             //  3
      HcalE,             //  4
      PrsTrig,           //  5
      EcalTrig,          //  6
      HcalTrig,          //  7
      Velo,              //  8
      Rich,              //  9
      TT = 10,                // 10
      IT,                // 11
      OT,                // 12
      Muon,              // 13
      L0PU,              // 14
      DAQ,               // 15
      ODIN,              // 16
      HltDecReports,     // 17
      VeloFull,          // 18
      TTFull,            // 19
      ITFull = 20,            // 20
      EcalPacked,        // 21
      HcalPacked,        // 22
      PrsPacked,         // 23
      L0Muon,            // 24
      ITError,           // 25
      TTError,           // 26
      ITPedestal,        // 27
      TTPedestal,        // 28
      VeloError,         // 29
      VeloPedestal = 30,      // 30
      VeloProcFull,      // 31
      OTRaw,             // 32
      OTError,           // 33
      EcalPackedError,   // 34
      HcalPackedError,   // 35
      PrsPackedError,    // 36
      L0CaloFull,        // 37
      L0CaloError,       // 38
      L0MuonCtrlAll,     // 39
      L0MuonProcCand = 40,    // 40
      L0MuonProcData,    // 41
      L0MuonRaw,         // 42
      L0MuonError,       // 43
      GaudiSerialize,    // 44
      GaudiHeader,       // 45
      TTProcFull,        // 46
      ITProcFull,        // 47
      TAEHeader,         // 48
      MuonFull,          // 49
      MuonError = 50,         // 50
      TestDet,           // 51
      L0DUError,         // 52
      HltRoutingBits,    // 53
      HltSelReports,     // 54
      HltVertexReports,  // 55
      HltLumiSummary,    // 56
      L0PUFull,          // 57
      L0PUError,         // 58
      DstBank,           // 59
      DstData = 60,           // 60
      DstAddress,        // 61
      FileID,            // 62
      VP,                // 63  Does not exist in hardware. Only SIM
      FTCluster,         // 64
      VL,                // 65
      UT,                // 66
      UTFull,            // 67
      UTError,           // 68
      UTPedestal,        // 69
      HC = 70,                // 70
      HltTrackReports,   // 71
      HCError,           // 72
      VPRetinaCluster,   // 73
      FTGeneric,         // 74
      FTCalibration,     // 75
      FTNZS,             // 76
      Calo,              // 77
      CaloError,         // 78
      MuonSpecial,       // 79
      RichCommissioning = 80, // 80
      RichError,         // 81
      FTSpecial,         // 82
      CaloSpecial,       // 83
      Plume,             // 84
      PlumeSpecial,      // 85
      PlumeError,        // 86
      VeloThresholdScan, // 87  Hardware only ?
      FTError,           // 88
      // Banks above are reserved for DAQ, add only generic DaqError types below.
      DaqErrorFragmentThrottled = 89, // 89
      DaqErrorBXIDCorrupted,     // 90
      DaqErrorSyncBXIDCorrupted, // 91
      DaqErrorFragmentMissing,   // 92
      DaqErrorFragmentTruncated, // 93
      DaqErrorIdleBXIDCorrupted, // 94
      DaqErrorFragmentMalformed, // 95
      DaqErrorEVIDJumped,        // 96

      // Add new types here. Don't forget to update also RawBank.cpp
      LastType, // LOOP Marker; add new bank types ONLY before!
    };

    /// Magic pattern for Raw bank headers
    enum RawPattern{ MagicPattern=0xCBCB };

    /// Access to magic word for integrity check
    int magic()  const               {  return m_magic;                            }

    /// Set magic word
    void setMagic()                  {  m_magic = MagicPattern;                    }

    /// Header size
    static int hdrSize()             {  return sizeof(Tell1Bank)-sizeof(Tell1Bank::m_data); }

    /// Return size of the data body part of the bank
    int size() const                 {  return m_length-hdrSize();                 }

    /// Set data size of the bank in bytes
    void setSize(std::size_t val)    {  m_length = (val&0xFFFF)+hdrSize();         }

    /// Access the full (padded) size of the bank
    int totalSize() const            { 
      typedef unsigned int T;
      return m_length%sizeof(T)==0 ? m_length : (m_length/sizeof(T)+1)*sizeof(T);
    }
    /// Return bankType of this bank 
    int type() const                 {  return int(m_type);                        }

    /// Set the bank type
    void setType(unsigned char val)  {  m_type = val&0xFF;                         }

    /// Return version of this bank 
    int version() const              {  return m_version;                          }

    /// Set the version information of this bank
    void setVersion(int val)         {  m_version = (unsigned char)(val&0xFF);     }

    /// Return SourceID of this bank  (TELL1 board ID)
    int sourceID() const             {  return m_sourceID;                         }

    /// Set the source ID of this bank (TELL1 board ID)
    void setSourceID(int val)        {  m_sourceID = (unsigned short)(val&0xFFFF); }

    /// Return pointer to begining of data body of the bank
    unsigned int* data()             {  return m_data;                             }

    /// Return pointer to begining of data body of the bank (const data access)
    const unsigned int* data() const {  return m_data;                             }

    /// Begin iterator 
    template <typename T> T* begin() {  return (T*)this->data();                   }

    /// End iterator 
    template <typename T> T* end()   {  return ((T*)this->data())+size()/sizeof(T);}

    /// Begin iterator over const iteration
    template <typename T> const T* begin() const  {  return (T*)this->data();      }

    /// End iterator of const iteration
    template <typename T>  const T* end() const
      {  return ((T*)this->data()) + size()/sizeof(T);                             }

    Tell1Bank* next()  {  return (Tell1Bank*)((char*)this + totalSize());          }
    const Tell1Bank* next()  const
    {        return (const Tell1Bank*)((const char*)this + totalSize());           }

  private:
    /// Magic word (by definition 0xCBCB)
    unsigned short m_magic;
    /// Bank length in bytes (must be >= 0)
    unsigned short m_length;
    /// Bank type (must be >= 0)
    unsigned char  m_type;
    /// Version identifier (must be >= 0)
    unsigned char  m_version;
    /// Source ID (valid source IDs are > 0; invalid ones 0)
    short          m_sourceID;
    /// Opaque data block
    unsigned int   m_data[1];
  }; // class Tell1Bank

  /// Output streamer
  inline std::ostream& operator <<(std::ostream& os, const Tell1Bank& b)  {
    char text[256];
    ::snprintf(text,sizeof(text),
	       "Tell1Bank: magic: %0x Length: %d Type: %d Version: %d SourceID: %0x",
	       int(b.magic()), int(b.totalSize()), int(b.type()), int(b.version()), int(b.sourceID()));
    os << text;
    return os;
  }
} // namespace Online

#include <Tell1Data/Unpack.h>
#endif ///ONLINE_DATAFLOW_TELL1BANK_H
