//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DirectoryScan.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef TELL1DATA_DIRECTORYSCAN_H
#define TELL1DATA_DIRECTORYSCAN_H

/// Framework include scans
#include "Tell1Data/RawFile.h"
#include "RTL/Logger.h"

/// C/C++ include scans
#include <map>
#include <set>
#include <memory>

namespace Online  {

  class DirectoryScan    {
  protected:
    std::unique_ptr<RTL::Logger>& m_logger;

  public:
    typedef std::map<std::string, RawFile> FileMap;

    /// List of files to process
    FileMap                  files;
    /// List of files, where something fishy happened while opening
    std::set<std::string>    badFiles;

    /// Add single file to the file map
    size_t addFile(const std::string& dir_name, 
		   const std::string& name, 
		   const std::string& prefix,
		   const std::vector<std::string>& allowedRuns,
		   bool take_all);

  public:
    /// Default constructor
    DirectoryScan(std::unique_ptr<RTL::Logger>& logger);
    /// Default destructor
    virtual ~DirectoryScan() = default;
    /// Read broken hosts file
    virtual bool isBrokenHost(const std::string& file)   const;
    /// Scan single directory for matching files
    virtual size_t scanDirectory(const std::string& dir_name,
				 const std::string& prefix,
				 const std::vector<std::string>& allowedRuns);
    /// Scan directories for matching files
    virtual size_t scanFiles(const std::vector<std::string>& directories,
			     const std::string& prefix,
			     const std::vector<std::string>& allowedRuns);
  };

  /// Default constructor
  inline DirectoryScan::DirectoryScan(std::unique_ptr<RTL::Logger>& logger) 
    : m_logger(logger) {
  }
}
#endif /* TELL1DATA_DIRECTORYSCAN_H  */
