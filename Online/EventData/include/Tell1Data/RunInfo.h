//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Niko Neufeld
//               Markus Frank
//==========================================================================
#ifndef GAUDIONLINE_RUNINFO_H
#define GAUDIONLINE_RUNINFO_H

// Framework include files
#include <Tell1Data/Pack.h>

/*
 *  Online namespace declaration
 */
namespace Online  {

  namespace OdinData  {
    /// Type of trigger broadcasted by ODIN
    enum TriggerType{ PhysicsTrigger     = 0,
		      BeamGasTrigger     = 1,
		      LumiTrigger        = 2,
		      TechnicalTrigger   = 3,
		      AuxiliaryTrigger   = 4,
		      NonZSupTrigger     = 5,
		      TimingTrigger      = 6,
		      CalibrationTrigger = 7
    };
  }

  /** @class OnlineRunInfo OnlineRunInfo.h MDF/OnlineRunInfo.h
    *
    * Basic run information from ODIN bank.
    */
  class PACKED_DATA RunInfo  {
  public:
    unsigned int   Run;
    unsigned short EventType;
    unsigned short CalibrationStep;
    unsigned int   Orbit;
    long long int  L0ID;
    long long int  GPSTime;
    unsigned       detStatus   : 24;
    unsigned       errors      :  8;
    unsigned       bunchID     : 12;
    unsigned       TAEWindow   :  3;
    unsigned       pad0        :  1;
    unsigned       triggerType :  3;
    unsigned       readoutType :  2;
    unsigned       forceBit    :  1;
    unsigned       bxType      :  2;
    unsigned       bunchCurrent:  8;
    unsigned int   TCK;

    /// Emulate SODIN access functions to ease interfacing
    unsigned int   run_number()   const  { return Run; }
    unsigned short event_type()   const  { return EventType; }
    unsigned short step_number()  const  { return CalibrationStep; }
    unsigned char  error_type()   const  { return errors; }
    unsigned int   tck()          const  { return TCK; }
    unsigned short bunch_id()     const  { return bunchID; }
    unsigned short orbit_id()     const  { return Orbit;   }
    unsigned char  bx_type()      const  { return bxType;  }
    unsigned long  event_id()     const  { return L0ID;    }    
    unsigned char  tae_window()   const  { return TAEWindow;                         }
    bool           tae_central()  const  { return 0 != (triggerType&OdinData::TimingTrigger); }
    bool           is_tae()       const  { return tae_central() || tae_window() > 0; }
  };

}
#include <Tell1Data/Unpack.h>
#endif // GAUDIONLINE_RUNINFO_H
