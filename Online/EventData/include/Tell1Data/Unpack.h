//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#undef PACK_DATA
#undef PACKED_DATA
#ifdef _WIN32
#pragma pack(pop, datamember_aligment)
#elif defined(__clang__) || defined(__CLING__)
#pragma clang diagnostic pop
#elif defined(__GNUC__) && __GNUC__ >= 5
#pragma GCC diagnostic pop
#endif
