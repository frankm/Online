//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework include files
#include "ROLogger/FileLogger.h"
#include "ROLogger/PartitionListener.h"
#include "ROLoggerDefs.h"

#include "RTL/rtl.h"
#include "CPP/Event.h"
#include "CPP/IocSensor.h"
#include "UPI/UpiSensor.h"
#include "UPI/UpiDisplay.h"
#include "UPI/upidef.h"
#include "dim/dis.h"

#include <sstream>
#include <cstring>
#include <climits>

#include <sys/types.h>
#include <sys/stat.h>
#ifdef _WIN32
#define vsnprintf _vsnprintf
#define stat64     __stat64
#define stat64Func _stat64
#define fopen64    fopen
#else
#include <unistd.h>
#define stat64Func stat64
#endif
static const char*  s_SevList[] = {"VERBOSE","DEBUG","INFO","WARNING","ERROR","FATAL"};

using namespace ROLogger;
using namespace std;

static void closeUPI(bool have_menu) {
  if ( have_menu )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"Close window.");
    ::upic_quit();
  }
  ::lib_rtl_sleep(200);
  ::exit(0);
}
static void rtl_help_fun() {
  cout 
    <<"Usage: romon_file_logger -option [-option]                            "<<endl
    <<"        -service=<string>     Service name to accept commands.        "<<endl
    <<endl;
}

/// Standard constructor
FileLogger::FileLogger(int argc, char** argv) 
  : MessageLogger(3,true,false)
{
  bool not_use_time = false, have_menu = false;
  string title, facility;
  RTL::CLI cli(argc, argv, rtl_help_fun);

  if ( cli.getopt("debug",  3) != 0 )  {
    bool run = true;
    while(run) ::lib_rtl_sleep(10);
  }

  m_fileHandler = "/group/online/dataflow/scripts/closePartitionLog.sh";
  cli.getopt("partition",4, m_partition);
  cli.getopt("facility", 4, facility);
  cli.getopt("logdir",   4, m_outdir);
  cli.getopt("severity", 4, m_severity);
  cli.getopt("size",     4, m_fileSize);
  cli.getopt("close",    4, m_fileHandler);
  have_menu = cli.getopt("menu", 3) != 0;

  m_timerfc3339 = cli.getopt("timeformatrfc3339",3) != 0; 
  not_use_time = cli.getopt("notime",3) != 0;
  ::memset(&m_date,0,sizeof(m_date));
  ::memset(&m_checkDate,0,sizeof(m_checkDate));

  if ( not_use_time ) m_useTimeForName = false;
  if ( m_partition.empty() ) {
    ::lib_rtl_output(LIB_RTL_ERROR,"You have to supply a partition name to display its data.");
    ::lib_rtl_sleep(3000);
    closeUPI(false);
  }
  if ( m_outdir.empty() ) {
    ::lib_rtl_output(LIB_RTL_ERROR,"You have to supply an output file name.");
    ::lib_rtl_sleep(3000);
    closeUPI(false);
  }
  m_colors  = false;
  m_display = true;
  m_output  = 0;
  openOutput();
  if ( 0 == m_output ) {
    ::lib_rtl_sleep(3000);
    closeUPI(false);
  }

  if ( facility.empty() && m_partition == "LHCb1" ) facility = "lhcb1";
  else if ( facility.empty() && m_partition == "LHCb2" ) facility = "lhcb2";
  else if ( facility.empty() && m_partition == "LHCbA" ) facility = "lhcba";
  else if ( facility.empty() ) facility = "gaudi";
  if ( have_menu )   {
    m_id = UpiSensor::instance().newID();
    title = "Error logger:"+m_partition;
    ::strncpy(m_msgSeverity,s_SevList[m_severity-1],sizeof(m_msgSeverity)-1);
    m_msgSeverity[sizeof(m_msgSeverity)-1]=0;
    ::upic_open_window();
    ::upic_open_menu(m_id,0,0,title.c_str(),RTL::processName().c_str(),RTL::nodeName().c_str());
    ::upic_add_comment(CMD_COM1,    "Current output file:","");
    ::upic_add_comment(CMD_COM2,    "","");
    ::upic_set_param(m_msgSeverity,1, "A7",m_msgSeverity, 0,0,s_SevList,sizeof(s_SevList)/sizeof(s_SevList[0]),1);
    ::upic_add_command(CMD_SEVERITY,"Log Severity level ^^^^^^^ ","");
    ::upic_add_command(CMD_SHOW,    "Show summary","");
    ::upic_add_command(CMD_START,   "Connect to messages","");
    ::upic_add_command(CMD_CONNECT, "Switch output file","");
    ::upic_add_command(CMD_CLOSE,   "Exit Logger","");
    ::upic_close_menu();
    UpiSensor::instance().add(this,m_id);
    m_quit = new UPI::ReallyClose(m_id,CMD_CLOSE);
    ::upic_set_cursor(m_id,CMD_SHOW,0);
  }

  m_listener = new PartitionListener(this,m_partition,facility);
  m_printRunNo = 1;
  time_t secs = ::time(NULL);
  struct tm *now = ::localtime(&secs);
  m_thisYear = now->tm_year;
}

/// Standard destructor
FileLogger::~FileLogger()  {
  if ( m_listener ) delete m_listener;
  if ( m_quit ) delete m_quit;
}

/// Open new output file
FILE* FileLogger::openOutput() {
  char txt[PATH_MAX], tmbuff[64];
  time_t tim = ::time(0);
  struct stat64 st;
  struct tm now;

  if ( m_output )   {
    ::fclose(m_output);
    ::strftime(tmbuff,sizeof(tmbuff),"%Y-%m-%d:%Hh",&m_date);
    ::snprintf(txt,sizeof(txt),"%s/%s.log.%s",m_outdir.c_str(),m_partition.c_str(),tmbuff);
    if ( !m_fileHandler.empty() && ::stat64Func(m_fileHandler.c_str(),&st) == 0 ) {
      //IocSensor::instance().send(this, CMD_HANDLE_FILE_CLOSE, new std::string(m_outputName));
      // Need to handle the old file here: zip, delete etc.
      string cmd = m_fileHandler + " -from " + m_outputName + " -to ";
      cmd += txt;
      ::system(cmd.c_str());
    }
    else   {
      int sc = ::rename(m_outputName.c_str(),txt);
      if ( 0 != sc )  {
	::lib_rtl_output(LIB_RTL_INFO,"The output file: %s  cannot be renamed to %s!",
			 m_outputName.c_str(), txt);
	::exit(EINVAL);
      }
    }
  }
  ::localtime_r(&tim, &now);
  ::strftime(tmbuff,sizeof(tmbuff),"%Y-%m-%d.log",&now);
  if ( m_useTimeForName )  {
    ::snprintf(txt,sizeof(txt),"%s/%s-%s",m_outdir.c_str(),m_partition.c_str(),tmbuff);
  }
  else  {
    ::snprintf(txt,sizeof(txt),"%s/%s.log",m_outdir.c_str(),m_partition.c_str());
  }

  if ( ::stat64Func(txt,&st) == 0 ) {
    ::lib_rtl_output(LIB_RTL_INFO,"The output file: %s  exists already. Reconnecting.....",txt);
  }
  m_outputName = "";
  m_output = ::fopen64(txt,"a+");
  if ( 0 == m_output ) {
    ::lib_rtl_output(LIB_RTL_INFO,"Cannot open output file: %s [%s]",txt,RTL::errorString().c_str());
  }
  else {
    ::lib_rtl_output(LIB_RTL_INFO,"Opened output file:%s",txt);
    if ( m_quit ) ::upic_replace_comment(m_id,CMD_COM2,txt,"");
    m_date = now;
    m_checkDate = now;
    m_outputName = txt;
  }
  return m_output;
}

/// Print multi-line header information with source information
void FileLogger::printSources(const std::vector<std::string>& sources, bool /* with_guards */)    {
  char time_buff[128];
  struct tm now;
  time_t tim = time(0);
  ::localtime_r(&tim, &now);
  ::strftime(time_buff, sizeof(time_buff), "%Y-%b-%d %H:%M:%S[INFO]", &now);
  for(vector<string>::const_iterator i=sources.begin();i!=sources.end();++i)   {
    ::fprintf(m_output,"%s %s: %s(Partition_Change): %s\n",
	      time_buff, RTL::nodeNameShort().c_str(), m_partition.c_str(), (*i).c_str());
  }
}

/// Print single message retrieved from error logger
void FileLogger::printMessage(const char* msg, bool crlf) {
  struct tm now;
  time_t tim = ::time(0);
  ::localtime_r(&tim, &now);
  if ( m_fileSize == 0 && (m_date.tm_yday != now.tm_yday) ) {
    m_output = openOutput();
  }
  else if ( m_fileSize>0 && (m_checkDate.tm_min != now.tm_min) ) {
    struct stat64 st;
    m_checkDate = now;
    if ( ::fstat64(fileno(m_output),&st) == 0 ) {
      if ( st.st_size > m_fileSize )  {
	m_output = openOutput();
      }
    }
  }
  if ( m_timerfc3339 ) {
    char *refmsg = formatTimeRFC3339(msg);
    MessageLogger::printMessage(refmsg, crlf);
    ::free(refmsg);
  } else {
    MessageLogger::printMessage(msg,crlf);
  }
  if ( m_connected && m_quit ) ::upic_write_message(msg+12,"");  
}

char * FileLogger::formatTimeRFC3339(const char * msg) {
  char *refmsg = ::strdup(msg);
  if (strlen(msg) < 12) return refmsg;
  struct tm logtime;
  if (!::strptime(msg, "%B%d-%H%M%S", &logtime)) return refmsg;
  logtime.tm_year = m_thisYear;
  refmsg = (char*) ::realloc(refmsg, strlen(refmsg) + 999999999);
  ::strftime(refmsg, 21, "%Y-%b-%d %H:%M:%S", &logtime);
  ::strcpy(refmsg + 20, msg + 12);
  return refmsg;
}

/// Display callback handler
void FileLogger::handle(const Event& ev) {
  typedef vector<string> _SV;
  IocSensor& ioc = IocSensor::instance();
  ioc_data data(ev.data);
  switch(ev.eventtype) {
  case UpiEvent:
    m_msgSeverity[sizeof(m_msgSeverity)-1] = 0;
    if ( ::strchr(m_msgSeverity,' ')  ) *::strchr(m_msgSeverity,' ') = 0;
    switch(ev.command_id)  {
    case UPI::UpiDisplay::CMD_CLOSE:
      if ( haveUPI() )   {
	UpiSensor::instance().remove(this,m_id);
	::upic_delete_menu(m_id);
	closeUPI(true);
      }
      ::exit(0);
      return;
    case CMD_SEVERITY:
      ioc.send(this,ev.command_id,new string(m_msgSeverity));
      return;
    case CMD_CLOSE:
      //UpiSensor::instance().remove(this,m_id);
      //::upic_delete_menu(m_id);
      //closeUPI();
      m_quit->invoke();
      return;
    case CMD_START:
      if ( haveUPI() )   {
	if ( m_connected )
	  ::upic_replace_command(m_id,CMD_START,"Connect to messages","");
	else
	  ::upic_replace_command(m_id,CMD_START,"Disconnect from messages","");
	m_connected = !m_connected;
      }
      return;
    case CMD_CONNECT:
      openOutput();
      return;
    case CMD_SHOW:
      if ( haveUPI() ) ::upic_write_message(getSummary().c_str(),"");
      return;
    }
    break;
  case IocEvent:
    switch(ev.type) {
    case CMD_UPDATE: {
      _SV v = m_farms;      
      string tmp;
      stringstream s;
      if ( !m_storage.empty() ) v.push_back(m_storage);
      if ( !m_monitoring.empty() ) v.push_back(m_monitoring);
      if ( !m_reconstruction.empty() ) v.push_back(m_reconstruction);
      tmp = "Farm content:";
      for(_SV::const_iterator i=v.begin();i!=v.end();++i) {
	const string& n = *i;
	s << n << ends;
	tmp += n;
	tmp += " ";
      }
      s << ends;
      if ( m_quit ) ::upic_write_message(tmp.c_str(),"");
      tmp = s.str();
      removeAllServices();
      handleMessages(tmp.c_str(),tmp.c_str()+tmp.length());
      return;
    }
    case CMD_SEVERITY:
      setMessageSeverity(m_msgSeverity);
      delete data.str;
      return;
    case CMD_UPDATE_NODES:
      delete data.vec;
      return;
    case CMD_CONNECT_STORAGE:
      m_storage = *data.str;
      delete data.str;
      ioc.send(this,CMD_UPDATE,this);
      return;
    case CMD_CONNECT_MONITORING:
      m_monitoring = *data.str;
      delete data.str;
      ioc.send(this,CMD_UPDATE,this);
      return;
    case CMD_CONNECT_RECONSTRUCTION:
      m_reconstruction = *data.str;
      delete data.str;
      ioc.send(this,CMD_UPDATE,this);
      return;
    case CMD_DISCONNECT_STORAGE:
      m_storage = "";
      ioc.send(this,CMD_UPDATE,this);
      return;
    case CMD_DISCONNECT_MONITORING:
      m_monitoring = "";
      ioc.send(this,CMD_UPDATE,this);
      return;
    case CMD_DISCONNECT_RECONSTRUCTION:
      m_reconstruction = "";
      ioc.send(this,CMD_UPDATE,this);
      return;
    case CMD_UPDATE_RUNNUMBER: {
      char text[32];
      ::snprintf(text,sizeof(text),"%ld",ev.iocData<long>());
      newRunNumber(text);
    }
      return;
    case CMD_UPDATE_FARMS:
      m_farms = *data.vec;
      delete data.vec;
      ioc.send(this,CMD_UPDATE,this);
      return;
    default:
      break;
    }
    break;
  default:
    MessageLogger::handle(ev);
    return;
  }
  ::lib_rtl_output(LIB_RTL_INFO,"Received unknown input.....%d",ev.eventtype);
}


static size_t do_output(void* par,int,const char* fmt, va_list& args) {
  FileLogger* l = (FileLogger*)par;
  if ( l->haveUPI() )  {
    char buffer[1024];
    size_t len = ::vsnprintf(buffer, sizeof(buffer), fmt, args);
    ::upic_write_message(buffer,"");
    return len;
  }
  string f = fmt;
  f += "\n";
  return ::vfprintf(stdout, f.c_str(), args);
}

extern "C" int romon_file_logger(int argc, char** argv) {
  UpiSensor& s = UpiSensor::instance();
  FileLogger mon(argc, argv);
  ::lib_rtl_install_printer(do_output,&mon);
  ::dis_start_serving((char*)RTL::processName().c_str());
  s.run();
  return 1;
}
