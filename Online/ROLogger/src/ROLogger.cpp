//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework include files
#include "ROLoggerDefs.h"
#include "CPP/Interactor.h"
#include "CPP/Event.h"
#include "RTL/strdef.h"
#include <cstdio>

using namespace std;

string ROLogger::fmcLogger(const string& host,const string& facility) {
  static const char* prefix = ::getenv("ROLOGGER_PREFIX")
    ? ::getenv("ROLOGGER_PREFIX") : "/FMC";
  char txt[128];
  string h = RTL::str_upper(host);
  ::snprintf(txt,sizeof(txt),"%s/%s/logger/%s/log",prefix,h.c_str(),facility.c_str());
  return txt;
}

void ROLogger::backSpaceCallBack (int menu,int /* cmd */,int par,const void* param)  {
  Event ev;
  ev.target     = (Interactor*)param;
  ev.eventtype  = UpiEvent;
  ev.menu_id    = menu;
  ev.command_id = CMD_BACKSPACE;
  ev.param_id   = par;
  ev.index_id   = 0;
  ev.target->handle(ev);
}
