//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework include files
#include <ROLogger/NodeListener.h>
#include <UPI/UpiSensor.h>
#include <CPP/IocSensor.h>
#include <UPI/upidef.h>
#include <RTL/strdef.h>
#include <dim/dic.h>
#include "ROLoggerDefs.h"

#include <cstring>
#include <vector>
#include <memory>

using namespace ROLogger;
using namespace std;
typedef vector<string> _SV;

bool NodeListener::check_match(const string& node)   const {
  for ( const auto& m : m_matches )   {
    if ( std::regex_match(node, m) )
      return true;
  }
  return false;
}

/// Standard constructor with object setup through parameters
NodeListener::NodeListener(Interactor* parent, const string& n, const vector<string>& matches) 
  : m_parent(parent), m_name(n)
{
  string svc = n+"/SERVICE_LIST";
  for( const auto& m : matches )
    m_matches.emplace_back(regex(m));
  m_loggerDP = ::dic_info_service(svc.c_str(),MONITORED,0,0,0,svcHandler,(long)this,0,0);
}

/// Standard destructor
NodeListener::~NodeListener() {
  ::dic_release_service(m_loggerDP);
}

/// DIM command service callback
void NodeListener::svcHandler(void* tag, void* address, int* size) {
  const char* nil = nullptr;
  auto f = std::make_unique<_SV>();
  NodeListener* h = *(NodeListener**)tag;
  for(const char* data = (char*)address, *end=data+*size;data > nil+1 && data<end;data = ::strchr(data,'\n')+1)   {
    string svc = data;
    string log_service = svc.substr(0,svc.find("|"));
    if ( h->check_match(log_service) )   {
      f->push_back(log_service);
    }
  }
  IocSensor::instance().send(h->m_parent,CMD_UPDATE_FARMS,f.release());
}

