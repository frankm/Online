//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework include files
#include "ROLogger/RecoListener.h"
#include "UPI/UpiSensor.h"
#include "CPP/IocSensor.h"
#include "UPI/upidef.h"
#include "ROLoggerDefs.h"

#include <cstring>
#include <vector>
#include <memory>

#include "dim/dic.h"

using namespace ROLogger;
using namespace std;
typedef vector<string> _SV;

/// Standard constructor with object setup through parameters
RecoListener::RecoListener(Interactor* parent,const string& n, const string& fac) 
  : m_parent(parent), m_name(n), m_facility(fac)
{
  char txt[64];
  auto f = std::make_unique<_SV>();
  for(int i=0; i<5; ++i) {
    for(int j=0; j<10; ++j) {
      snprintf(txt,sizeof(txt),"HLT%c%02d",char(i+'A'),int(j<4 ? j+1 : j+2));
      f->push_back(fmcLogger(txt,m_facility));
    }
  }
  f->push_back(fmcLogger("STOREIO01",m_facility));
  f->push_back(fmcLogger("STOREIO01","recomgr"));
  f->push_back(fmcLogger("STORECTL01",m_facility));
  IocSensor::instance().send(m_parent,CMD_UPDATE_FARMS,f.release());
}

/// Standard destructor
RecoListener::~RecoListener() {
  //::dic_release_service(m_subFarmDP);
}

/// DIM command service callback
void RecoListener::subFarmHandler(void* tag, void* address, int* size) {
  auto f = std::make_unique<_SV>();
  RecoListener* h = *(RecoListener**)tag;
  for(const char* data = (char*)address, *end=data+*size;data<end;data += strlen(data)+1)
    f->push_back(fmcLogger(data,h->m_facility));
  f->push_back(fmcLogger("STOREIO01",h->m_facility));
  f->push_back(fmcLogger("STOREIO01","recomgr"));
  f->push_back(fmcLogger("STORECTL01",h->m_facility));
  f->push_back(fmcLogger("MONA08",h->m_facility));
  f->push_back(fmcLogger("MONA09",h->m_facility));
  f->push_back(fmcLogger("MONA07",h->m_facility));
  if ( h->name() == "LHCb" ) f->push_back(fmcLogger("CALD07",h->m_facility));
  IocSensor::instance().send(h->m_parent,CMD_UPDATE_FARMS,f.release());
}

