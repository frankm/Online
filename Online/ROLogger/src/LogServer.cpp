//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//  ROLogger
//-------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//=========================================================================

// Framework include files
#include "ROLogger/LogServer.h"
#include "CPP/IocSensor.h"
#include "CPP/Event.h"
#include "RTL/rtl.h"
#include "dim/dis.h"
#include "dim/dic.h"

// C/C++ include files
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <cstdio>
#include <cerrno>
#include <memory>
#include <thread>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#ifdef _WIN32
#include <io.h>
#include <direct.h>
#define O_NONBLOCK 0x0
#define W_OK       0x6
inline bool S_ISFIFO(int) { return false; }
#endif

using namespace ROLogger;
using namespace std;

enum {
  CMD_DEL = 1,
  CMD_ADD,
  CMD_CONNECT,
  CMD_PUBLISH
};

namespace  {
  /// Extract node/service name from DNS info
  void getServiceNode(char* s, string& svc, string& node) {
    char* at = strchr(s,'@');
    *at = 0;
    svc = s;
    node = at+1;
  }
}

/// Initializing constructor
LogServer::LogServer()
  : m_out_fifo_fd(-1)
{
}

/// Default destructor
LogServer::~LogServer()   {
  if ( m_out_fifo_fd != -1 ) {
    ::close(m_out_fifo_fd);
    m_out_fifo_fd = -1;
  }
}

/// Create new poll pool
int LogServer::_poll_create(int max_count)   {
  m_poll_fd = ::epoll_create(max_count);
  if ( m_poll_fd < 0 ) {
    ::lib_rtl_output(LIB_RTL_OS,"Failed to create client epollset.\n");
    return errno;
  }
  return 0;
}

/// Add connection to poll pool
int LogServer::_poll_add(int new_fd)   {
  if ( new_fd >= 0 )  {
    struct epoll_event epoll;
    epoll.events  = EPOLLERR | EPOLLHUP;
    epoll.data.fd = new_fd;
    if ( 0 > ::epoll_ctl(m_poll_fd, EPOLL_CTL_ADD, new_fd, &epoll) ) {
      ::lib_rtl_output(LIB_RTL_OS,"Failed to add FIFO connection to epoll descriptor.\n");
      return errno;
    }
    return 0;
  }
  return errno = EINVAL;
}

/// Remove connection from poll pool
int LogServer::_poll_del(int fd)   {
  if ( fd >= 0 )  {
    struct epoll_event epoll;
    epoll.data.fd = fd;
    if ( 0 > ::epoll_ctl(m_poll_fd, EPOLL_CTL_DEL, fd, &epoll) ) {
      ::lib_rtl_output(LIB_RTL_OS,"Failed to remove client connection %d.\n",fd);
      return errno;
    }
    return 0;
  }
  return errno = EINVAL;
}

/// Poll on connections delivering data
int LogServer::_poll(int& events, int tmo)  {
  struct epoll_event epoll;
  int nclients = ::epoll_wait(m_poll_fd, &epoll, 1, tmo);
  events = epoll.events;
  if ( nclients > 0 )
    return epoll.data.fd;
  return nclients;
}

/// Connect input fifo
int LogServer::connectInputFifo(const string& fifo) {
  struct stat statBuf;
  // Create poll set
  if ( m_poll_fd == -1 )    {
    if ( 0 != _poll_create(10) )   {
      ::printf("Failed to create input poll-set: %s\n",::strerror(errno));
      return errno;
    }
  }
  // get fifo information
  if( ::stat(fifo.c_str(),&statBuf )==-1)  {
    if ( 0 != ::mkfifo(fifo.c_str(),0666) )  {
      ::printf("mkfifo(\"%s\",0666): %s\n",fifo.c_str(),::strerror(errno));
      return errno;
    }
    ::stat(fifo.c_str(),&statBuf );
  }
  // check if fifo is a FIFO */
  if( !S_ISFIFO(statBuf.st_mode) )  {
    ::printf("I-node: \"%s\" is not a FIFO!\n",fifo.c_str());
    return EINVAL;
  }
  if( ::access(fifo.c_str(),W_OK) == -1 )    { // access denied
    ::printf("access(\"%s\"): %s!\n",fifo.c_str(),::strerror(errno));
    return EACCES;
  }
  // open error log 
  m_in_fifo_fd = ::open(fifo.c_str(),O_RDONLY|O_NONBLOCK|O_NOATIME);
  if ( m_in_fifo_fd == -1 )  {
    ::printf("open(\"%s\"): %s!\n",fifo.c_str(),::strerror(errno));
    return errno;
  }
  if ( _poll_add(m_in_fifo_fd) )   {
    return errno;
  }
  return 0;
}

/// Connect output fifo
int LogServer::connectOutputFifo(const string& fifo) {
  struct stat statBuf;
  // get fifo information
  if( ::stat(fifo.c_str(),&statBuf )==-1)  {
    if ( 0 != ::mkfifo(fifo.c_str(),0666) )  {
      ::printf("mkfifo(\"%s\",0666): %s\n",fifo.c_str(),::strerror(errno));
      return errno;
    }
    ::stat(fifo.c_str(),&statBuf );
  }
  // check if fifo is a FIFO */
  if( !S_ISFIFO(statBuf.st_mode) )  {
    ::printf("I-node: \"%s\" is not a FIFO!\n",fifo.c_str());
    return EINVAL;
  }
  if( ::access(fifo.c_str(),W_OK) == -1 )    { // access denied
    ::printf("access(\"%s\"): %s!\n",fifo.c_str(),::strerror(errno));
    return EACCES;
  }
  // open error log
  m_out_fifo_fd = ::open(fifo.c_str(),O_WRONLY|O_NONBLOCK|O_NOATIME);
  if ( m_out_fifo_fd == -1 )  {
    ::printf("open(\"%s\"): %s!\n",fifo.c_str(),::strerror(errno));
    return errno;
  }
  return 0;
}

/// Close all log files
int LogServer::close() {
  if ( m_out_fifo_fd != -1 ) ::close(m_out_fifo_fd);
  m_out_fifo_fd = -1;
  return 1;
}

void LogServer::listen_in_dns()   {
  char in_dns[512];
  ::dic_get_dns_node(in_dns);
  if ( 0 != m_dns_dp )    {
    ::dic_release_service(m_dns_dp);
  }
  m_dns_dp = ::dic_info_service("DIS_DNS/SERVER_LIST",MONITORED,0,0,0,dnsDataHandler,(long)this,(void*)"DEAD",5);
}

/// Write message to output device
int LogServer::write_out_fifo(const string& msg) {
  const char* p = msg.c_str();
  for(size_t wr=msg.length(), written=0; written < wr; ) {
    int w = ::write(m_out_fifo_fd, p+written, wr-written);
    if ( w > 0 )  {
      written += w;
    }
    else if ( errno != EAGAIN )  {
      return errno;
    }
    else  {
      ::lib_rtl_usleep(1);
    }
  }
  return 1;
}

/// Print content of log files as they come....
void LogServer::read_in_fifos() {
  char c;
  string value;
  while ( 1 )   {
    int events = 0;
    int fd = _poll(events, m_poll_tmo);
    if ( fd > 0 )   {
      /// Nead the next line and publish the result
      int nb = ::read(fd, &c, 1);
      if ( nb > 0 )   {
	if ( (c == '\r' || c == '\n') && value.length() == 0 )   {
	  continue;
	}
	else if ( c == '\n' || c == '\r' )    {
	  publish(value);
	  value = "";
	}
	else   {
	  value += c;
	}
      }
    }
  }
}

int LogServer::publish(const string& value)    {
  int ret = 0;
  if ( !value.empty() )  {
    if ( m_out_dim_service > 0 )  {
      lock_guard<mutex> lock(m_output_guard);
      m_out_dim_data = value;
      ret += ::dis_update_service( m_out_dim_service );
    }
    if ( m_out_fifo_fd > 0 )   {
      lock_guard<mutex> lock(m_output_guard);
      write_out_fifo(value);
      ++ret;
    }
  }
  return ret;
}

/// DIM command service callback on DNS for newly created services
void LogServer::dnsDataHandler(void* tag, void* address, int* size) {
  if ( address && tag && *size > 0 ) {
    LogServer* listener = *(LogServer**)tag;
    char *msg = (char*)address;
    string svc, node;
    switch(msg[0]) {
    case '+':  {
      smatch match;
      getServiceNode(++msg,svc,node);
      if ( regex_search(svc, match, listener->m_in_service_match) )   {
	IocSensor::instance().send(listener,CMD_ADD,new string(svc));
      }
      break;
    }
    case '-':
      break;
    case '!':
      //getServiceNode(++msg,svc,node);
      //log() << "Service " << msg << " in ERROR." << endl;
      break;
    default:
      if ( *(int*)msg != *(int*)"DEAD" )  {
        char *at, *p = msg, *last = msg;
        auto loggers = std::make_unique<vector<string>>();
        while ( last != 0 && (at=strchr(p,'@')) != 0 )  {
	  smatch match;
          last = strchr(at,'|');
          if ( last ) *last = 0;
          getServiceNode(p,svc,node);
	  if ( regex_search(svc, match, listener->m_in_service_match) )   {
	    loggers->push_back(svc);
          }
          p = last+1;
        }
        if ( !loggers->empty() )   {
          IocSensor::instance().send(listener,CMD_CONNECT,loggers.release());
	}
      }
      break;
    }
  }
}

/// DIM command service callback to handle log data
void LogServer::logDataHandler(void* tag, void* address, int* size)   {
  if ( address && tag && *size > 0 ) {
    LogServer* listener = *(LogServer**)tag;
    char *msg = (char*)address;
    if ( *(int*)msg != *(int*)"DEAD" )  {
      IocSensor::instance().send(listener,CMD_PUBLISH,new string(msg));      
    }
  }
}

/// Interactor overload: event interrupt handler
void LogServer::handle(const CPP::Event& ev)    {
  switch(ev.eventtype) {
  case TimeEvent:
    return;
  case IocEvent:
    switch(ev.type) {
      break;
    case CMD_PUBLISH:   {
      std::unique_ptr<string> msg(ev.iocPtr<string>());
      publish(*msg);
      break;
    }
    case CMD_DEL:  {
      break;
    }
    case CMD_ADD:   {
      std::unique_ptr<string> svc(ev.iocPtr<string>());
      lock_guard<mutex> lock(m_input_guard);
      if ( m_in_dim_services.find(*svc) == m_in_dim_services.end() )  {
	int svc_id = ::dic_info_service(svc->c_str(),MONITORED,0,0,0,logDataHandler,(long)this,(void*)"DEAD",5);
	m_in_dim_services.insert(make_pair(svc->c_str(), svc_id));
      }
      break;
    }
    case CMD_CONNECT:   {
      std::unique_ptr<vector<string> > v(ev.iocPtr<vector<string> >());
      const vector<string>& vec = *v;
      lock_guard<mutex> lock(m_input_guard);
      for( const auto& s : vec )   {
	if ( m_in_dim_services.find(s) == m_in_dim_services.end() )  {
	  int svc_id = ::dic_info_service(s.c_str(),MONITORED,0,0,0,logDataHandler,(long)this,(void*)"DEAD",5);
	  m_in_dim_services.insert(make_pair(s, svc_id));
	}
      }
      break;
    }
    default:
      break;
    }
    break;
  default:
    break;
  }
}

namespace {
  void help_fun() {
  }
}

extern "C" int fifo_log_server(int argc, char** argv) {
  string in_fifo;
  string out_fifo;
  string in_dns, out_dns;
  string in_DIM, out_DIM;
  string proc = RTL::processName();
  RTL::CLI cli(argc, argv, help_fun);
  cli.getopt("infifo",3,in_fifo);
  cli.getopt("outfifo",3,out_fifo);
  cli.getopt("indns",3,in_dns);
  cli.getopt("outdns",3,out_dns);
  
  LogServer* server = new LogServer();
  if ( !in_fifo.empty() && server->connectInputFifo(in_fifo) ) {
    ::printf("Failed to connect the input fifo: %s [%s]\n",in_fifo.c_str(),::strerror(errno));
  }
  if ( !out_fifo.empty() && server->connectOutputFifo(out_fifo) ) {
    ::printf("Failed to connect the output fifo: %s [%s]\n",out_fifo.c_str(),::strerror(errno));
  }
  if ( !in_dns.empty() )   {
    ::dic_set_dns_node(in_dns.c_str());
    server->listen_in_dns();
  }
  if ( !out_dns.empty() )   {
    ::dis_set_dns_node(out_dns.c_str());
  }
  ::dis_start_serving(proc.c_str());

  std::unique_ptr<thread> fifo_thread;
  if ( !in_fifo.empty() )    {
    fifo_thread.reset(new thread([server]() { server->read_in_fifos(); }));
  }
  fifo_thread->join();
  return 0x1;
}
