//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "DimPublish.h"
#include <RTL/strdef.h>
#include <dim/dis.h>

/// C/C++ include files
#include <vector>
#include <cstring>
#include <unistd.h>

using namespace std;
using namespace kafka;

/// DIM callback to supply the payload
void DimPublish::feed_message(void* tag, void** buff, int* size, int* /* first */)   {
  static const char* data = "";
  DimPublish* pub = *(DimPublish**)tag;
  if ( pub && pub->payload_length )   {
    *buff = pub->payload;
    *size = pub->payload_length;
    return;
  }
  *buff = (void*)data;
  *size = 0;
}

/// Default constructor
DimPublish::DimPublish(long dns_id, const string& h, const string& tag)
  : host(RTL::str_upper(h))
{
  if ( !host.empty() )   {
    string svc;
    if ( tag.empty() )
      svc = "/"+host+"/log";
    else
      svc = "/"+host+"/"+tag+"/log";
    serviceID = ::dis_add_service_dns(dns_id, svc.c_str(), "C", 0, 0, feed_message, (long)this);
    // ::printf("Added service %s\n",svc.c_str());
  }
  payload = (char*)::malloc(payload_max=1024);
  payload_length = 0;
}

/// Default destructor
DimPublish::~DimPublish()   {
  if ( serviceID != 0 )   {
    ::dis_remove_service(serviceID);
    serviceID = 0;
  }
}

void DimPublish::set(const void* pay, size_t len)   {
  if ( len < 10*1024 )   {
    if ( payload_max <= len+1 )  {
      payload = (char*)::realloc(payload, payload_max=len+256);
    }
    ::memcpy(payload, pay, len);
    payload[len]   = 0;
    if      ( len >= 1 && payload[len-1] == '\n' ) payload[len-1] = 0, --len;
    else if ( len >= 2 && payload[len-2] == '\n' ) payload[len-2] = 0, len -= 2;
    else if ( len >= 3 && payload[len-3] == '\n' ) payload[len-3] = 0, len -= 3;
    payload_length = len+1;
    if ( payload_length > 1 )   {
      ::dis_update_service(serviceID);
    }
  }
}
