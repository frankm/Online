//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_KAFKAPRODUCER_H
#define ROLOGGER_KAFKAPRODUCER_H

/// Framework include files
#include "LogServer.h"

/// C/C++ include files
#include <vector>
#include <regex>

/// kafka namespace declaration
namespace kafka  {

  /// Terminal logger class
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class KafkaProducer : public Listener  {
  public:
    class Implementation;
    typedef std::vector<std::regex> Matches;
    Matches         node_match;
    Matches         message_veto;
    bool            print_stats = false;
    std::unique_ptr<Implementation> imp;

  public:
    /// Initializing constructor
    KafkaProducer(std::vector<char*>& args);
    /// Default destructor
    virtual ~KafkaProducer();
    /// Initialize the producer
    virtual int initialize()  override;
    /// Handler for all messages
    virtual void handle_payload(const char* topic,
				char* payload, size_t plen,
				char* key, size_t klen)     override;
    /// Handle monitoring 
    virtual void handle_monitoring(const Monitor& monitor)  override;
  };
}
#endif // ROLOGGER_KAFKAPRODUCER_H
