//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "PublishListener.h"
#include <RTL/strdef.h>
#include <dim/dis.h>

/// C/C++ include files
#include <vector>
#include <cstring>
#include <unistd.h>

using namespace std;
using namespace kafka;

#define HOST_TAG     "\"host\":\""
#define MESSAGE_TAG  "\"message\":\""
#define PHYSICAL_TAG "\"physical_host\":\""

/// Default constructor
PublishListener::PublishListener(const string& dns_name, const string& n, const string& t)
  : name(RTL::str_upper(n)), tag(RTL::str_upper(t))
{
  //  publish_monitor(dns_name, name, tag);
  string dns = dns_name;
  string svc = name+"/"+tag+"/monitor";
  if ( dns.empty() )   {
    char text[132];
    ::dis_get_dns_node(text);
    dns = text;
  }
  if ( 0 == this->dns_ID )   {
    int port        = ::dis_get_dns_port();
    this->dns_ID    = ::dis_add_dns(dns.c_str(), port);
  }
}

/// Default destructor
PublishListener::~PublishListener()   {
}

/// Handler for all messages
void PublishListener::handle_payload(const char* /* topic */,
				    char* payload, size_t plen,
				    char* /* key */, size_t /* klen */)
{
  static int m_len = ::strlen(MESSAGE_TAG);
  static int p_len = ::strlen(PHYSICAL_TAG);
  static int h_len = ::strlen(HOST_TAG);
  char* message = ::strstr(payload, MESSAGE_TAG);
  char* host_physical = 0;
  char* host_start = 0;
  char* host_end = 0;
  char c_host_physical, c_host_end;
  if ( message )   {
    message += m_len;
    host_physical = ::strstr(message, PHYSICAL_TAG);
    if ( host_physical )  {
      c_host_physical = *(host_physical-2);
      *(host_physical-2) = 0;
      host_start = ::strstr(host_physical+p_len, HOST_TAG);
      if ( host_start )   {
	host_start += h_len;
	host_end = ::strstr(host_start, "\"}");
	if ( host_end )  {
	  c_host_end = *host_end;
	  *host_end = 0;
	}
      }
      if ( host_start )   {
	uint32_t hash = DimPublish::hash32(host_start);
	Services::iterator i = this->services_list.find(hash);
	if ( i == this->services_list.end() )   {
	  auto ent = make_pair(hash, make_unique<DimPublish>(dns_ID, host_start, tag));
	  auto ret = this->services_list.insert(move(ent));
	  if ( ret.second )   {
	    i = ret.first;
	    //::printf("++ Added messages from: '%s'  %p\n",host_start, (*i).second.get());
	    ::dis_start_serving_dns(dns_ID, (name+"/"+tag).c_str());
	  }
	}
	/// restore the payload
	if ( host_end ) *host_end = c_host_end;
	if ( host_physical ) *(host_physical-2) = c_host_physical;
	if ( i != services_list.end() )   {
	  DimPublish* p = (*i).second.get();
	  p->set(payload, plen);
	}
	else {
	  /// Error!
	}
	return;
      }
      /// restore the payload
      if ( host_end ) *host_end = c_host_end;
      if ( host_physical ) *(host_physical-2) = c_host_physical;
      if ( message )
	printf("Host-ERROR: %s\n", message);
      else
	printf("Payload-ERROR: %s\n", payload);
    }
  }
}

/// Handle monitoring 
void PublishListener::handle_monitoring(const Monitor& mon_info)  {
  if ( print_stats )   {
    print_statistics(mon_info);
  }
}
