//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_PUBLISHLISTENER_H
#define ROLOGGER_PUBLISHLISTENER_H

/// Framework include files
#include "LogServer.h"
#include "DimPublish.h"

/// C/C++ include files
#include <map>
#include <memory>

/// kafka namespace declaration
namespace kafka  {

  /// Kafka listener to publish node payload
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class DimListener : public Listener  {
  public:
    std::unique_ptr<DimPublish> service;
    std::string dns_name;
    std::string name;
    std::string tag;
    std::string real_host;
    std::string physical_host;
    std::string output_trailer;
    long        dns_ID    = 0;
    bool        print_stats = false;

  public:
    /// Default constructor
    DimListener(const std::string& dns, const std::string& name, const std::string& tag);
    /// Default destructor
    virtual ~DimListener();
    /// Handler for all messages
    virtual void handle_payload(const char* topic,
				char* payload, size_t plen,
				char* key,     size_t klen)  override;  
    /// Handle monitoring 
    virtual void handle_monitoring(const Monitor& monitor)  override;
  };
}       // End namespace kafka
#endif  // ROLOGGER_PUBLISHLISTENER_H
