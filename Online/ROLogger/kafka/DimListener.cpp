//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "DimListener.h"
#include "DimPublish.h"
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dis.h>

/// C/C++ include files
#include <vector>
#include <cstring>
#include <unistd.h>

using namespace std;
using namespace kafka;

/// Default constructor
DimListener::DimListener(const string& dns, const string& n, const string& t)
  : dns_name(RTL::str_upper(dns)), name(RTL::str_upper(n)), tag(RTL::str_upper(t))
{
  //  publish_monitor(dns_name, name, tag);
  string svc = name+"/"+tag+"/monitor";
  if ( this->dns_name.empty() )   {
    char text[132];
    ::dis_get_dns_node(text);
    this->dns_name = text;
  }
  if ( 0 == this->dns_ID )   {
    int port        = ::dis_get_dns_port();
    this->dns_ID    = ::dis_add_dns(this->dns_name.c_str(), port);
  }
  char host[128], pid_str[32];
  ::snprintf(pid_str, sizeof(pid_str), "%d", ::lib_rtl_pid());
  ::gethostname(host,sizeof(host));
  const char* dim_host  = ::getenv("DIM_HOST_NODE");
  dim_host = dim_host ? dim_host : host;
  this->output_trailer += "\",\"physical_host\":\"";
  this->output_trailer += RTL::str_lower(host);
  this->output_trailer += "\",\"host\":\"";
  this->output_trailer += RTL::str_lower(dim_host);
  this->output_trailer += "\",\"pid\":\"";
  this->output_trailer += pid_str;
  this->output_trailer += "\",\"utgid\":\""+RTL::processName();
  this->output_trailer += "\",\"systag\":\"SYSTEM\"}\n";
  service = make_unique<DimPublish>(this->dns_ID, dim_host, tag);
  ::dis_start_serving_dns(this->dns_ID, (name+"/"+tag).c_str());
  ::lib_rtl_output(LIB_RTL_VERBOSE,"+++ DNS: '%s'  [%ld]   Server: '%s'",
		   dns.c_str(), this->dns_ID, (name+"/"+tag).c_str());
}

/// Default destructor
DimListener::~DimListener()   {
}

/// Handler for all messages
void DimListener::handle_payload(const char* /* topic */,
				 char* payload,   size_t plen,
				 char* /* key */, size_t /* klen */)
{
  static constexpr long PAYLOAD_MATCH = 0x73656D697440227BL;  // *(long*)"{\"@timest";
  int match = *(long*)payload == PAYLOAD_MATCH;
  if ( match )   {
    service->set(payload, plen);
  }
  else   {
    struct tm tim;
    char   time_str[128];
    time_t now = ::time(0);
    string output;
    ::localtime_r(&now, &tim);
    ::strftime(time_str,sizeof(time_str),"{\"@timestamp\":\"%Y-%m-%dT%H:%M:%S.000000%z\",\"message\":\"",&tim);
    if ( payload[0] && payload[0] == ' ' ) ++payload;
    output.reserve(512);
    output  = time_str;
    output += payload;
    output += this->output_trailer;
    service->set(output.c_str(), output.length());
  }
}

/// Handle monitoring 
void DimListener::handle_monitoring(const Monitor& mon_info)  {
  if ( print_stats )   {
    print_statistics(mon_info);
  }
}
