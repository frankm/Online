//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "OutputListener.h"
#include "RTL/graphics.h"

/// C/C++ include files
#include <vector>
#include <cstring>
#include <unistd.h>

using namespace kafka;

#define END_TAG      "\",\""
#define HOST_TAG     "\"host\":\""
#define UTGID_TAG    "\"utgid\":\""
#define SYSTAG_TAG   "\"systag\":\""
#define MESSAGE_TAG  "\"message\":\""
#define PHYSICAL_TAG "\"physical_host\":\""
#define COUNTER_TAG  "\"count\":\""

/// Handle monitoring 
void OutputListener::handle_monitoring(const Monitor& mon_info)  {
  if ( print_stats )   {
    print_statistics(mon_info);
  }
}

static char* mark_end(char* ptr)    {
  static constexpr size_t e_tag_len   = sizeof(END_TAG) - 2;
  if ( ptr )   {
    char* end = ::strstr(ptr, END_TAG);
    if ( end )   {
      *end = 0;
      return end+e_tag_len; // 
    }
    end = ::strstr(ptr+1,"\"}");
    if ( end )   {
      *end = 0;
    }
  }
  return nullptr;
}

static char* mark_start(char* ptr, const char* tag, size_t tag_len)    {
  if ( ptr )   {
    char* end = ::strstr(ptr, tag);
    if ( end )   {
      *(end+tag_len-1) = 0;
      return end+tag_len;
    }
  }
  return nullptr;
}

/// Second level object initialization
int OutputListener::initialize()     {
  if ( have_colors )   {
    graphics::bg_black();
    graphics::white();
  }
  return this->Listener::initialize();
}

void OutputListener::print_msg(const char* time_stamp,
			       const char* message,
			       const char* systag,
			       const char* utgid,
			       const char* host,
			       const char* /* physical */,
			       const char* counter)
{
  if ( message && ::strlen(message)>0 )   {
    const char* tag = systag ? systag : "SYSTEM";

    if ( have_colors )   {
      int severity = message[0];
      if ( 0 == strncmp(tag,"SYSTEM",3) ) severity = 'D';
      switch(severity)    {
      case 'V':
	graphics::nobold(); graphics::blue();
	break;
      case 'D':
	graphics::nobold(); graphics::green();
	break;
      case 'I':
	graphics::nobold(); graphics::white();
	break;
      case 'W':
	graphics::bold(); graphics::yellow();
	break;
      case 'E':
	graphics::bold(); graphics::red();
	break;
      case 'F':
	graphics::bold(); graphics::magenta();
	break;
      case 'A':
	graphics::bold(); graphics::white();
	break;
      default:
	graphics::nobold(); graphics::white();
	break;
      }
    }
    size_t len = 0;
    char text[1024];
    text[0] = 0;
    if ( !simple_msg )   {
      len += ::snprintf(text+len,sizeof(text)-len,"%s %-11s",
			time_stamp               ? time_stamp : "",
			host                     ? host       : "");
      if ( counter )
	len += ::snprintf(text+len,sizeof(text)-len,"%-9s%-7s",counter,tag);
      else
	len += ::snprintf(text+len,sizeof(text)-len,"%-7s",tag);
    }
    len += ::snprintf(text+len,sizeof(text)-len,"%-34s%s\n",
		      utgid ? utgid : "", message ? message : "");
    ::fputs(text,stdout);
    ::fflush(stdout);
    if ( have_colors )   {
      graphics::white();
      graphics::plain();
      graphics::bg_black();
    }
  }
}
#include <unistd.h>

/// Handler for all messages
void OutputListener::handle_payload(const char* /* topic */,
				    char* payload, size_t plen,
				    char* /* key */, size_t /* klen */)
{
  static constexpr long PAYLOAD_MATCH = 0x73656D697440227BL;  // *(long*)"{\"@timest";
  static constexpr size_t m_tag_len   = sizeof(MESSAGE_TAG)  - 1;
  static constexpr size_t p_tag_len   = sizeof(PHYSICAL_TAG) - 1;
  static constexpr size_t h_tag_len   = sizeof(HOST_TAG)     - 1;
  static constexpr size_t u_tag_len   = sizeof(UTGID_TAG)    - 1;
  static constexpr size_t s_tag_len   = sizeof(SYSTAG_TAG)   - 1;
  static constexpr size_t s_cnt_len   = sizeof(COUNTER_TAG)  - 1;

  //printf("@%16lX: %.*s: %.*s\n", m->offset, (int)m->key_len, (char*)m->key, (int)m->len, (char*)m->payload);
  char* time_stamp = payload + 15;
  int   pay_match      = *(long*)payload == PAYLOAD_MATCH;

  if ( pay_match )   {
    std::string raw_msg;
    *(time_stamp+10) = ' ';
    *(time_stamp+19) = 0;
    if ( print_raw )   {
      raw_msg.clear();
      raw_msg.reserve(plen+1);
      raw_msg.insert(raw_msg.end(), payload, payload+plen);
      raw_msg.push_back('\n');
    }
    char* message  = mark_start(time_stamp+20, MESSAGE_TAG, m_tag_len);
    char* msg_end  = mark_end(message);
    size_t len     = msg_end-message-3;
    if ( message && len > plen )   {
      len = strlen(message);
    }
    char* physical  = mark_start(msg_end, PHYSICAL_TAG, p_tag_len);
    char* phys_end  = mark_end(physical);
    char* host      = mark_start(phys_end, HOST_TAG,     h_tag_len);
    char* host_end  = mark_end(host);
    char* utgid     = mark_start(host_end, UTGID_TAG,    u_tag_len);
    char* utgid_end = mark_end(utgid);
    char* systag    = mark_start(utgid_end, SYSTAG_TAG,   s_tag_len);
    char* sys_end   = mark_end(systag);
    char* counter   = mark_start(sys_end, COUNTER_TAG,  s_cnt_len);
    char* cnt_end   = mark_end(counter);

#if 0
    if ( message && message[0] == '{' )  {
      bool wait = true;
      ::printf("Havoc!!!!\n\n");
      while (wait) ::sleep(1);
    }
    if ( utgid && utgid[0] == '{' )  {
      bool wait = true;
      ::printf("Havoc!!!!\n\n");
      while (wait) ::sleep(1);
    }
    if ( systag && systag[0] == '{' )  {
      bool wait = true;
      ::printf("Havoc!!!!\n\n");
      while (wait) ::sleep(1);
    }
#endif
    if ( !message )   {
      return;
    }
    /// Check if tag matches:
    if ( !this->select_tag.empty() )   {
      if ( this->select_tag != systag )   {
	return;
      }
    }
    if ( message[len] == '\n' )   {
      message[len] = 0;
    }
    if ( message[0] && message[0] == ' ' )  {
      ++message;
    }
    if ( !message_veto.empty() )   {
      for ( const auto& m : message_veto )  {
	if ( std::regex_match(message, m) )  {
	  return;
	}
      }
    }
    if ( !message_match.empty() )   {
      bool match_msg = false;
      for ( const auto& m : message_match )  {
	if ( std::regex_match(message, m) )  {
	  match_msg = true;
	  break;
	}
      }
      if ( !match_msg ) return;
    }
    /// If match criteria are empty, the message is accepted.
    /// If not, we want to have an AND logic.
    bool match_host  = node_match.empty();
    bool match_utgid = utgid_match.empty();
    if ( host && !node_match.empty() )    {
      for ( const auto& m : node_match )  {
	if ( std::regex_match(host, m) )  {
	  match_host = true;
	  break;
	}
      }
    }
    if ( utgid && !utgid_match.empty() )   {
      for ( const auto& m : utgid_match )  {
	if ( std::regex_match(utgid, m) )  {
	  match_utgid = true;
	  break;
	}
      }
    }
    if ( match_utgid && match_host )   {
      if ( print_raw )   {
	::fwrite(raw_msg.c_str(), 1, raw_msg.length(), stdout);
	::fflush(stdout);
      }
      else   {
	print_msg(time_stamp, message, systag, utgid, host, physical, counter);
      }
    }
    if ( msg_end   ) *msg_end   = '\"';
    if ( phys_end  ) *phys_end  = '\"';
    if ( host_end  ) *host_end  = '\"';
    if ( utgid_end ) *utgid_end = '\"';
    if ( sys_end   ) *sys_end   = '\"';
    if ( cnt_end   ) *cnt_end   = '\"';
    if ( time_stamp ) *(time_stamp+10)  = 'T';
    if ( time_stamp ) *(time_stamp+19)  = ' ';
  }
  else   {
    ::printf("%s %-8s '%s'\n", "PAYLOAD", "ERROR: Actual value is ", payload);
  }
}
