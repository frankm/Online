//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_DIMPUBLISH_H
#define ROLOGGER_DIMPUBLISH_H

/// Framework include files

/// C/C++ include files
#include <string>

/// kafka namespace declaration
namespace kafka  {
  
  /// Single node publisher
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class DimPublish  {
  public:
    std::string  host;
    unsigned int serviceID      = -1;
    size_t       payload_length = 0;
    size_t       payload_max    = 0;
    char*        payload        = nullptr;
    long         dns_ID         = 0;
    /// DIM callback to supply the payload
    static void feed_message(void* tag, void** buf, int* size, int* first);

  public:
    /// Default constructor
    DimPublish(long dns_id, const std::string& host, const std::string& tag);
    /// Move constructor
    DimPublish(DimPublish&& copy) = default;
    /// Copy constructor
    DimPublish(const DimPublish& copy) = default;
    /// Move assignment
    DimPublish& operator=(DimPublish&& copy) = default;
    /// Copy assignment
    DimPublish& operator=(const DimPublish& copy) = default;
    /// Default destructor
    virtual ~DimPublish();

    /// We need it so often: one-at-time 32 bit hash function
    static unsigned int hash32(const char* key) {
      unsigned int hash = 0;
      const char* k = key;
      for (; *k; k++) {
	hash += *k;
	hash += (hash << 10);
	hash ^= (hash >> 6);
      }
      hash += (hash << 3);
      hash ^= (hash >> 11); hash += (hash << 15);
      return hash;
    }
    static unsigned int hash32(const std::string& key) {
      return hash32(key.c_str());
    }
    void set(const void* pay, size_t len);
  };
}       // End namespace kafka
#endif  // ROLOGGER_DIMPUBLISH_H
