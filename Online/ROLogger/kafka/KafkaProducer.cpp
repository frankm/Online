//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "KafkaProducer.h"
#include <RTL/rtl.h>

/// Kafka include file
#include "rdkafka.h"  /* for Kafka driver */

/// C/C++ include files
#include <vector>
#include <cstring>
#include <csignal>
#include <sys/time.h>
#include <unistd.h>

/// kafka namespace declaration
namespace kafka  {

  /// Terminal logger class
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class KafkaProducer::Implementation   {
  public:
    /// Reference to kafka instance
    rd_kafka_t*                      rk = nullptr;
    /// Reference to kafka topic handle
    rd_kafka_topic_t*                rkt = nullptr;
    /// Reference to kafka configuration instance
    rd_kafka_conf_t*                 config = nullptr;
    /// Reference to kafka topic configuration instance
    rd_kafka_topic_conf_t*           topic_config = nullptr;
    /// Reference to kafka topic partition list
    rd_kafka_topic_partition_list_t* topics = nullptr;
    /// Optional Kafka headers
    rd_kafka_headers_t*              user_headers = nullptr;

    /// Brokers identification
    std::string     brokers;
    /// Topic name
    std::string     topic;
    /// Monitoring instance
    Monitor         monitor;
    /// Message counter
    long            msg_counter = 0;
    /// Flag to add counter to message payload
    bool            add_counter = false;

    /// Signal handler to stop
    static void sig_stop (int sig);
    /// Signal handler for monitoring
    static void sig_usr1 (int sig);
    /// Kafka error callback
    static void err_call (rd_kafka_t* rk, int err, const char* reason, void* opaque);
    /// Kafka statistics callback
    static int  stats_call(rd_kafka_t *rk, char *json, size_t json_len, void* /* opaque */);
    /// Kafka throttle callback
    static void throttle_call(rd_kafka_t *rk, const char *broker, int32_t broker_id, int msec, void* opaque);
    /// Kafka offset callback
    static void offset_commit_call(rd_kafka_t *rk, rd_kafka_resp_err_t err,
				   rd_kafka_topic_partition_list_t *offsets, void* opaque);
    /// Kafka message delivery callback
    static void msg_delivered_call (rd_kafka_t *rk, const rd_kafka_message_t *msg, void *opaque);

  public:
    /// Default constructor
    Implementation() = default;
    /// Default destructor
    virtual ~Implementation() = default;
    /// Perform basic initialization
    void init();
    /// Start the kafka producerrrr
    int start();
    /// Send message payload to kafka server
    rd_kafka_resp_err_t send_message(int         partition, 
				     const char* payload,
				     size_t      plen,
				     const char* key,
				     size_t      klen);
  };
}

using namespace kafka;
static int verbosity = 1;
static volatile sig_atomic_t s_running = 1;
static KafkaProducer::Implementation* s_global_producer = nullptr;

void KafkaProducer::Implementation::sig_stop (int /* sig */) {
  if ( !s_running )
    exit(0);
  s_running = 0;
}

void KafkaProducer::Implementation::sig_usr1 (int /* sig */) {
  ::rd_kafka_dump(stdout, s_global_producer->rk);
}

/// Perform basic initialization
void KafkaProducer::Implementation::init()    {
  this->config = ::rd_kafka_conf_new();
  this->topics = ::rd_kafka_topic_partition_list_new(1);
  this->topic_config = ::rd_kafka_topic_conf_new();
  /// Instrument kafka with the appropriate callbacks
  //::rd_kafka_conf_set_error_cb(this->config, err_call);
  //::rd_kafka_conf_set_stats_cb(this->config, stats_call);
  //::rd_kafka_conf_set_throttle_cb(this->config, throttle_call);
  //::rd_kafka_conf_set_offset_commit_cb(this->config, offset_commit_call);
  /// Callback on message delivery
  ::rd_kafka_conf_set_dr_msg_cb(this->config, msg_delivered_call);
  /// Kafka topic configuration
  ::rd_kafka_topic_conf_set(this->topic_config, "auto.offset.reset", "earliest", NULL, 0);
}

/// Start the kafka producerrrr
int KafkaProducer::Implementation::start()    {
  char errstr[512];

  /// Create Kafka handle
  if ( !(this->rk = ::rd_kafka_new(RD_KAFKA_PRODUCER, this->config, errstr, sizeof(errstr))) )  {
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ Failed to create Kafka consumer: %s", errstr);
    return 0;
  }
  ::signal(SIGIO, sig_stop);
#ifdef SIGUSR1
  ::signal(SIGUSR1, sig_usr1);
#endif
  /// Add broker(s) 
  if ( brokers.empty() || ::rd_kafka_brokers_add(this->rk, this->brokers.c_str()) < 1) {
    ::lib_rtl_output(LIB_RTL_ERROR, "+++ No valid Kafka brokers specified");
    return 0;
  }
  ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ Kafka brokers specified: %s topic: %s",
		   this->brokers.c_str(), this->topic.c_str());
  ::rd_kafka_topic_partition_list_add(this->topics, this->topic.c_str(), RD_KAFKA_PARTITION_UA);

  /// Explicitly create topic to avoid per-msg lookups
  this->rkt = ::rd_kafka_topic_new(this->rk, topic.c_str(), this->topic_config);
  //::rd_kafka_conf_set_default_topic_conf(this->config, this->topic_config);
  return 1;
}

/// Send message payload to kafka server
rd_kafka_resp_err_t KafkaProducer::Implementation::send_message(int         partition,
								const char* payload,
								size_t      plen,
								const char* key,
								size_t      klen)
{
  int ret;
  int msgflags    = RD_KAFKA_MSG_F_COPY;
#if 0
  if ( this->user_headers )    {
    rd_kafka_headers_t *headers_copy = ::rd_kafka_headers_copy(this->user_headers);
    rd_kafka_resp_err_t err = rd_kafka_producev(this->rk,
						RD_KAFKA_V_RKT(this->rkt),
						RD_KAFKA_V_PARTITION(partition),
						RD_KAFKA_V_MSGFLAGS(msgflags),
						RD_KAFKA_V_VALUE(load, plen),
						RD_KAFKA_V_KEY(key, klen),
						RD_KAFKA_V_HEADERS(headers_copy),
						RD_KAFKA_V_END);
    if (err)   {
      ::rd_kafka_headers_destroy(headers_copy);
    }
    return err;
  }
#endif
  if ( payload[plen-1] == 0 ) --plen;
  if ( add_counter )    {
    char text[32];
    char* load = (char*)payload;
    std::string data;
    ::snprintf(text, sizeof(text), ",\"count\":\"%ld\"}", msg_counter);
    load[plen-1] = 0;
    data  = load;
    data += text;
    plen  = data.length();
    ret = ::rd_kafka_produce(this->rkt,partition,msgflags,(void*)data.c_str(),plen,key,klen,NULL);
  }
  else   {
    ret = ::rd_kafka_produce(this->rkt,partition,msgflags,(void*)payload,plen,key,klen,NULL);
  }
  ::rd_kafka_poll(this->rk, 0);
  if ( ret == -1 )  {
    return ::rd_kafka_last_error();
  }
  ++msg_counter;
  return RD_KAFKA_RESP_ERR_NO_ERROR;
}

/// Kafka error callback
void KafkaProducer::Implementation::err_call(rd_kafka_t*  rk,
					     int      /*  err */,
					     const char*  reason,
					     void *   /*  opaque */)
{
  ::lib_rtl_output(LIB_RTL_INFO,"+++ Kafka ERROR CALLBACK: %s: %s", ::rd_kafka_name(rk), reason);
}

/// Kafka statistics callback
int KafkaProducer::Implementation::stats_call (rd_kafka_t* /* rk */,
					       char*       /* json */,
					       size_t      /* json_len */,
					       void*       /* opaque */)
{
  return 0;
}

/// Kafka throttle callback
void KafkaProducer::Implementation::throttle_call(rd_kafka_t* rk,
						  const char* broker,
						  int32_t     broker_id,
						  int         msec,
						  void*    /* opaque */)
{
  ::lib_rtl_output(LIB_RTL_INFO,"+++ Kafka THROTTLE %s: %dms by %s (%8X)",
		   ::rd_kafka_name(rk), msec, broker, broker_id);
}

void KafkaProducer::Implementation::offset_commit_call(rd_kafka_t *rk,
						       rd_kafka_resp_err_t err,
						       rd_kafka_topic_partition_list_t *offsets,
						       void* /* opaque */)
{
  if ( err || verbosity >= 2 )   {
    ::lib_rtl_output(LIB_RTL_INFO,"+++ Kafka offset call %s: Offset commit of %d partition(s)",
		     ::rd_kafka_name(rk), offsets->cnt);
  }
  for (int i = 0 ; i < offsets->cnt ; i++) {
    rd_kafka_topic_partition_t *par = &offsets->elems[i];
    if ( par->err || verbosity >= 2 )   {
      ::lib_rtl_output(LIB_RTL_INFO,"+++ Kafka %s: %s [%8X] @ %16lX",
		       ::rd_kafka_name(rk), par->topic, par->partition, par->offset);
    }
  }
}

void KafkaProducer::Implementation::msg_delivered_call (rd_kafka_t* /* rk */,
							const rd_kafka_message_t* msg,
							void* /* opaque */)
{
  Monitor& monitor = s_global_producer->monitor;
  rd_ts_t now = rd_clock();

  monitor.messages++;
  monitor.messages_dr_wait--;
  if ( msg->err )   {
    monitor.messages_dr_err++;
  }
  else {
    monitor.messages_dr_ok++;
    monitor.bytes_dr_ok += msg->len;
  }
  if ( (msg->err && (monitor.messages_dr_err < 50)) ) {
#if 0
    if ( verbosity >= 2 )   {
      ::lib_rtl_output(LIB_RTL_ERROR, "+++ Kafka message delivery failed: %s [%6d]: %s (%li remain)",
		       ::rd_kafka_topic_name(msg->rkt),
		       msg->partition,
		       ::rd_kafka_err2str(msg->err),
		       monitor.messages_dr_wait);
    }
    else if (verbosity > 2)    {
      ::lib_rtl_output(LIB_RTL_ERROR, "+++ Kafka message delivered (offset %ld): %li remain\n",
		       msg->offset, monitor.messages_dr_wait);
    }
#endif
    monitor.t_last = now;
  }
  monitor.offset = msg->offset;
}

/// Initializing constructor
KafkaProducer::KafkaProducer(std::vector<char*>& args)   {
  int    argc = args.size();
  char** argv = (char**)&args[0];
  bool   print_help = false;
  std::string value;

  imp = std::make_unique<Implementation>();
  for( int i=0; i<argc; ++i )   {
    char c = argv[i][0];
    if ( c == '-' )   {
      c = argv[i][1];
      switch(c)   {
      case 'c':
	imp->add_counter = true;
	break;
      case 'B':
	imp->brokers = argv[++i];
	break;
      case 't':
	imp->topic = argv[++i];
	break;
      case 'H':  {
	size_t name_sz = -1;
	char* nam = argv[++i];
	char* val = strchr(nam, '=');
	if ( val ) {
	  name_sz = (size_t)(val-nam);
	  val++; // past the '='
	}
	if ( nullptr == imp->user_headers )   {
	  imp->user_headers = ::rd_kafka_headers_new(8);
	}
	rd_kafka_resp_err_t err =
	  ::rd_kafka_header_add(imp->user_headers, nam, name_sz, val, -1);
	if ( err ) {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Failed to add Kafka header %s: %s\n",
			   nam, ::rd_kafka_err2str(err));
	  ::exit(1);
	}
	break;
      }
      case 'h':
	print_help = true;
	break;
      case 'i':
      case 'P':
	this->print_stats = ::atoi(argv[++i]);
	break;
      case 'V':
	value = argv[++i];
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Adding node match: %s", value.c_str());
	this->message_veto.emplace_back(std::regex(value));
	break;
      default:
	break;
      }
    }
  }
  if ( print_help )   {
    ::printf("KafkaProducer -opt [-opt]                                     \n"
	     "     -h             Print this help.                          \n"
	     "     -B <broker>    Set Kafka message broker.                 \n"
	     "     -H name=value  Add optional headers to kafka.            \n"
	     "     -P             Print statistics                          \n"
	     "     -V <reg-ex>    Veto messages matching regular expression.\n");
    return;
  }
  else if ( imp->topic.empty() )    {
    ::printf("+++ No kafka topic name specified. Check -t option!");
    ::exit(EINVAL);
  }
  else if ( imp->brokers.empty() )    {
    ::printf("+++ No kafka broker specified. Check -B option!");
    ::exit(EINVAL);
  }
}

/// Initialize the producer
int KafkaProducer::initialize()    {
  s_global_producer = imp.get();
  imp->init();
  return imp->start();
}

/// Default destructor
KafkaProducer::~KafkaProducer()    {
  imp.reset();
}

/// Handle monitoring 
void KafkaProducer::handle_monitoring(const Monitor& mon_info)  {
  if ( print_stats )   {
    print_statistics(mon_info);
  }
}

/// Handler for all messages
void KafkaProducer::handle_payload(const char* /* topic  */,
				   char*          payload,
				   size_t         plen,
				   char*       /* key    */,
				   size_t      /* klen   */)
{
  const char* key    = 0;
  int         keylen = 0;
  int         num_errors = 0;
  int         partition = RD_KAFKA_PARTITION_UA;
  rd_kafka_resp_err_t err = imp->send_message(partition, payload, plen, key, keylen);
  if ( err == 0 )   {    // No error!
    return;
  }
  if ( num_errors < 50 )   {
    if ( err == RD_KAFKA_RESP_ERR__UNKNOWN_PARTITION )   {
      ::lib_rtl_output(LIB_RTL_ERROR,"+++ Kafka producer error: No such partition: %6d", partition);
    }
    else if ( err == RD_KAFKA_RESP_ERR__QUEUE_FULL )   {
      //
      // ::lib_rtl_output(LIB_RTL_ERROR,"+++ Kafka producer error: %s (backpressure)", rd_kafka_err2str(err));
    }
    else   {
      ::lib_rtl_output(LIB_RTL_ERROR,"+++ Kafka producer error: %s", rd_kafka_err2str(err));
    }
  }
}
