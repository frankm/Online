//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_PUBLISHLISTENER_H
#define ROLOGGER_PUBLISHLISTENER_H

/// Framework include files
#include "LogServer.h"
#include "DimPublish.h"

/// C/C++ include files
#include <map>

/// kafka namespace declaration
namespace kafka  {
  
  /// Kafka listener to publish node payload
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class PublishListener : public Listener  {
  public:
    typedef std::map<uint32_t, std::unique_ptr<DimPublish> > Services;
    Services     services_list;
    std::string  name;
    std::string  tag;
    long         dns_ID    = 0;
    bool         print_stats = false;

  public:
    /// Default constructor
    PublishListener(const std::string& dns, const std::string& name, const std::string& tag);
    /// Default destructor
    virtual ~PublishListener();
    /// Handler for all messages
    virtual void handle_payload(const char* topic,
				char* payload, size_t plen,
				char* key, size_t klen)  override;
    /// Handle monitoring 
    virtual void handle_monitoring(const Monitor& monitor)  override;
  };
}       // End namespace kafka
#endif  // ROLOGGER_PUBLISHLISTENER_H
