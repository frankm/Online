//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_DIMCONSUMER_H
#define ROLOGGER_DIMCONSUMER_H

/// Framework include files
#include "LogServer.h"

/// C/C++ include files
#include <vector>
#include <regex>

/// kafka namespace declaration
namespace kafka  {

  /// Terminal logger class
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class DimConsumer : public Consumer  {
  public:
    typedef std::vector<std::regex> Matches;
    Matches        node_match;
    Matches        message_veto;
    std::string    broker;
    unsigned int   brokerServices = 0;

    bool           print_stats = false;

    /// DimInfo overload to process messages
    static void analyze_services(void* tag, void* address, int* size);

    /// Analyze the DIM service list of the broker
    void analyze_services(const char* services);

  public:
    /// Default constructor
    DimConsumer() = default;
    /// Default destructor
    virtual ~DimConsumer() = default;
    /// Initialize the logger and connect to services
    virtual int initialize();
  };
}
#endif // ROLOGGER_DIMCONSUMER_H
