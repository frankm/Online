//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#if 0
/// Framework include files
#include <CPP/Event.h>
#include <CPP/IocSensor.h>
#include <CPP/Interactor.h>

/// C/C++ include files
#include <map>
#include <set>
#include <mutex>
#include <string>
#include <thread>
#include <memory>
#include <vector>

#define FUSE_USE_VERSION 26
#include <fuse.h>

/// Forward declarations
class KafkaFS;

#define LOGFS_VERSION  "1.0.0"


/// Kafka logger object
/**
 *
 *   \author  M.Frank
 *   \version 1.0
 */
class KafkaFS : public CPP::Interactor   {
public:

  enum {
    DEBUG   = 1,
    INFO    = 2,
    WARNING = 3,
    ERROR   = 4
  };

  /// Kafka logger device
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class LogDevice  final  {
  public:
    typedef std::set<LogDevice*> Children;
    std::mutex  lock;
    std::string path;
    std::string data;
    struct stat info;
    Children    children;
    KafkaFS*    fs        = nullptr;
    LogDevice*  parent    = nullptr;
    long        ref_count = 0;
    int         file_fd   = -1;

  public:
    /// Initializing constructor
    LogDevice(KafkaFS* fs, const std::string& path, struct stat& stbuf);
    /// Default destructor
    virtual ~LogDevice() = default;
    LogDevice* add_ref();
    long release();
    /// Send message if still pending buffers exist
    int  flush_buffers();
    void set_parent(LogDevice* parent);
    void read_data();
  };

  /// Handle to Kafka logger device
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class DevHandle  final  {
    LogDevice* device = nullptr;
  public:
    DevHandle() : device(nullptr)  {
    }
    DevHandle(LogDevice* dev) : device(dev)  {
      if ( dev ) dev->add_ref();
    }
    DevHandle(DevHandle&& copy)   {
      device      = copy.device;
      copy.device = nullptr;
    }
    DevHandle(const DevHandle& copy)  {
      if ( copy.device ) device = copy.device->add_ref();
    }
    DevHandle& operator=(DevHandle&& copy)  {
      if ( copy.device != device )  {
	if ( device ) device->release();
	device      = copy.device;
	copy.device = nullptr;
      }
      return *this;
    }
    DevHandle& operator=(const DevHandle& copy)  {
      if ( copy.device != device )  {
	if ( copy.device ) copy.device->add_ref();
	if ( device ) device->release();
	device = copy.device;
      }
      return *this;
    }
    DevHandle& operator=(LogDevice* dev)  {
      if ( dev ) dev->add_ref();
      if ( device ) device->release();
      device = dev;
      return *this;
    }
    ~DevHandle()    {
      if ( device ) device->release();
    }
    LogDevice* operator->() const  {  return device; }
    operator LogDevice*() const  {  return device; }
    bool operator !()   const  {  return nullptr == device; }
    LogDevice* ptr()  const  {  return device; }
    LogDevice* release()   { LogDevice* dev = device; device = nullptr; return dev; }
  };

  typedef std::map<std::string, LogDevice*> FileMap;
  typedef std::unique_ptr<std::thread>      ThreadHandle;
  FileMap         m_logs;
  DevHandle       root          {0};
  ThreadHandle    listener      {};
  ThreadHandle    reader        {};
  std::mutex      file_system_lock;
  std::string     topic;
  int             root_gid     = -1;
  int             root_uid     = -1;
  int             print_level  = DEBUG;
  int             run_kafka_fs = 1;
  int             epollFD      = -1;
  int             epollTMO     = 100;

public:
  /// Listen to newly opened fifos and listen to the file descriptors
  void listen();
  /// Poll on the FIFOs and publish the data to the message system
  void read_fifos();
  /// Display callback handler
  virtual void handle(const CPP::Event& ev)  override;
  size_t print(int level, const char* format, ...);

public:
  /// Default constructor
  KafkaFS(int);
  /// Default destructor
  virtual ~KafkaFS() = default;
  /// Instantiator
  static KafkaFS& instance();
  /// Allocate a new INO number
  static int      new_ino();
  /// Access an existing file entry
  DevHandle       get(const std::string& path);
  /// Access the parent of an object identified by the child name
  DevHandle       get_parent(const std::string& path);

  /// Create a file node
  int mknod(const std::string& path, mode_t mode, dev_t dev);
  /// Create and open a file
  int create(const std::string& path, mode_t mode, struct fuse_file_info *fi);
  /// Open new message object
  int open(const std::string& path, struct fuse_file_info *fi);
  /// Check file access permissions
  int access(const std::string& path, unsigned int mode);
  /// Change the size of a file
  int truncate(const std::string& path, off_t size);
  /// Possibly flush cached data
  int flush(const std::string& path, struct fuse_file_info *fi);
  /// Release an open file
  int release(const std::string& path, struct fuse_file_info *fi);
  /// Get file attributes.
  int getattr(const std::string& path, struct stat *stbuf);
  /// Write data to an open file
  int write(const std::string& path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi);
  /// Remove a file
  int unlink(const std::string& path);
  /// Rename a file
  int rename(const std::string& from, const std::string& to);

  /// Create a directory 
  int mkdir(const std::string& path, mode_t mode);
  /// Remove a directory
  int rmdir(const std::string& path);
  /// Open directory
  int opendir(const std::string& path, struct fuse_file_info* fi);
  /// Read directory
  int readdir(const std::string& path, void *buf, fuse_fill_dir_t filler, off_t, struct fuse_file_info *fi);
  /// Close directory
  int releasedir(const std::string& path, struct fuse_file_info* fi);

};


#pragma GCC diagnostic ignored "-Wunused-parameter"

#define EPOLL_CTL_FREE  101

/// Framework include files
#include <WT/wtdef.h>

/// C/C++ include files
#include <iostream>
#include <cstdlib>
#include <cstdarg>
#include <cstring>
#include <climits>
#include <cstdio>
#include <cerrno>
#include <ctime>
#include <unistd.h>
#include <sys/xattr.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <libgen.h>

#define LOGS_MOUNT_POINT "/logs"

enum   {
  FIOC_NONE,
  FIOC_ROOT,
  FIOC_FILE,
};

typedef std::lock_guard<std::mutex> Lock;
using namespace std;

/// Get error string from errno
std::string error(int err)   {
  string e;
  char text[1024];
  e = ::strerror_r(err, text, sizeof(text));
  //e = text;
  return e; // return std::string(text);
}

/// Initializing constructor
KafkaFS::LogDevice::LogDevice(KafkaFS* sys, const std::string& p, struct stat& stbuf)
  : path(p), info(stbuf), fs(sys)
{
}

KafkaFS::LogDevice* KafkaFS::LogDevice::add_ref()    {
  Lock guard(this->lock);
  ++ref_count;
  fs->print(DEBUG,
	    "==> LogDevice::add_ref [%p]  %ld\n", (void*)this, ref_count);
  return this;
}

long KafkaFS::LogDevice::release()   {
  long ref;  {
    Lock guard(this->lock);
    ref = --ref_count;
  }
  fs->print(DEBUG,
	    "==> LogDevice::release [%p]  %ld\n", (void*)this, ref);
  if ( 0 == ref ) delete this;
  return ref;
}

/// Send message if still pending buffers exist
int KafkaFS::LogDevice::flush_buffers()   {
  return 0;
}

void KafkaFS::LogDevice::set_parent(LogDevice* dev)    {
  LogDevice* par = parent;
  parent = 0;
  if ( dev )   {
    parent = dev->add_ref();
    parent->lock.lock();
    ++parent->info.st_nlink;
    bool ret = parent->children.insert(add_ref()).second;
    if ( !ret )   {
      fs->print(ERROR,
		"Child node %s ALREADY PRESENT in %s\n",
		this->path.c_str(), parent->path.c_str());
    }
    parent->lock.unlock();
    return;
  }
  if ( par )   {
    par->lock.lock();
    Children::iterator i = par->children.find(this);
    if ( i != par->children.end() )   {
      par->children.erase(i);
      --par->info.st_nlink;
      par->lock.unlock();
      par->release();
      release();
      return;
    }
    par->lock.unlock();
    fs->print(ERROR,
	      "Node %s with invalid child node: %s\n",
	      par->path.c_str(), this->path.c_str());
  }
}

void KafkaFS::LogDevice::read_data()    {
  char c;
  while(1)   {
    int nb = ::read(file_fd, &c, 1);
    if ( nb <= 0 ) {
      return;
    }
    if ( (c == '\n' || c == '\0') && !this->data.empty() )   {
      this->data += '\n';
      ::write(STDOUT_FILENO, this->data.c_str(), this->data.length());
      this->data = "";
      continue;
    }
    this->data += c;
  }
}

/// Instantiator
KafkaFS& KafkaFS::instance()    {
  static KafkaFS fs(0);
  return fs;
}

/// Allocate a new INO number
int KafkaFS::new_ino()    {
  static int s_ino = 0;
  return ++s_ino;
}

/// Default constructor
KafkaFS::KafkaFS(int)   {
  struct stat st;
  ::stat("/",&st);
  root_uid = st.st_uid;
  root_gid = st.st_gid;
  ::memset(&st, 0, sizeof(st));
  st.st_dev = 0;
  st.st_ino = new_ino();
  st.st_uid = root_uid;
  st.st_gid = root_gid;
  st.st_atime = st.st_mtime = st.st_ctime = ::time(nullptr);
  st.st_mode  = S_IFDIR | 0777;
  st.st_size  = 4096;
  st.st_nlink = 1;
  root = new LogDevice(this, "/", st);
  m_logs.insert(make_pair(root->path, root->add_ref()));

  st.st_ino   = new_ino();
  st.st_mode  = S_IFIFO | 0666;
  st.st_size  = 4096;
  DevHandle child = new LogDevice(this, "/test", st);
  child->set_parent(root);
  m_logs.insert(make_pair(child->path, child->add_ref()));

  epollFD  = ::epoll_create(10);
  IocSensor::instance();
  listener = make_unique<std::thread>([this]{  this->listen();     });
  reader   = make_unique<std::thread>([this]{  this->read_fifos(); });
}

size_t KafkaFS::print(int level, const char* format, ...)   {
  if ( level >= print_level )  {
    va_list args;
    va_start( args, format );
    size_t result = ::vfprintf(stdout, format, args);
    va_end(args);
    return result;
  }
  return 0;
}

/// Display callback handler
void KafkaFS::handle(const CPP::Event& ev)    {
  switch(ev.eventtype) {
  case IocEvent:  {
    switch(ev.type)   {
    case EPOLL_CTL_ADD:  {
      LogDevice* log = ev.iocPtr<LogDevice>();
      if ( log->file_fd == -1 )   {
	struct epoll_event ev;
	string logpath = LOGS_MOUNT_POINT + log->path;
	log->add_ref();
	log->file_fd = ::open(logpath.c_str(), O_RDONLY|O_NONBLOCK);
	if ( log->file_fd != -1 )   {
	  this->print(INFO, "+++ Successfully opened path: %s refcnt: %ld\n",
		      logpath.c_str(), log->ref_count);
	  ev.events   = EPOLLIN | EPOLLRDHUP | EPOLLERR | EPOLLHUP | EPOLLET;
	  ev.data.ptr = log;
	  int ret = ::epoll_ctl(epollFD, EPOLL_CTL_ADD, log->file_fd, &ev);
	  if ( ret != 0 )    {
	    this->print(INFO, "+++ Failed to add FD %d to epoll structure. [%s]\n",
			log->file_fd, error(errno).c_str());
	  }
	}
      }
      break;
    }
    case EPOLL_CTL_DEL:   {
      LogDevice* log = ev.iocPtr<LogDevice>();
      if ( log->file_fd != -1 )   {
	struct epoll_event ev;
	ev.events   = EPOLLIN | EPOLLRDHUP | EPOLLERR | EPOLLHUP | EPOLLET;
	ev.data.ptr = log;
	::epoll_ctl(epollFD, EPOLL_CTL_DEL, log->file_fd, &ev);
	::close(log->file_fd);
	log->file_fd = -1;
	log->release();
      }
      break;
    }
    case EPOLL_CTL_FREE:   {
      LogDevice* log = ev.iocPtr<LogDevice>();
      log->release();
      break;
    }
    default:
      break;
    }
    break;
  }
  default:   {
    break;
  }
  }
}

/// Poll on the FIFOs and publish the data to the message system
void KafkaFS::read_fifos()    {
  while ( run_kafka_fs )    {
    struct epoll_event epoll[10];
    int nclients = ::epoll_wait(epollFD, epoll, sizeof(epoll)/sizeof(epoll[0]), epollTMO);
    if ( nclients > 0 )    {
      for( int i=0; i < nclients; ++i )   {
	if ( epoll[i].events&EPOLLIN )   {
	  LogDevice* log = (LogDevice*)epoll[i].data.ptr;
	  log->read_data();
	}
	else  {
	  this->print(INFO, ".");
	}
      }
    }
  }
}

/// Access an existing file entry
KafkaFS::DevHandle KafkaFS::get(const std::string& path)   {
  Lock lock(file_system_lock);
  FileMap::const_iterator i = m_logs.find(path);
  if ( i != m_logs.end() )   {
    return (*i).second;
  }
  return nullptr;
}

/// Access the parent of an object identified by the child name
KafkaFS::DevHandle KafkaFS::get_parent(const std::string& path)  {
  size_t idx = path.rfind('/');
  if ( idx != string::npos )  {
    if ( 0 == idx ) 
      return get("/");
    string p = path.substr(0,idx);
    return get(p);
  }
  return nullptr;
}

/// Listen to newly opened fifos and listen to the file descriptors
void KafkaFS::listen()    {
  int status, substatus, ok=1;
  unsigned int facility;
  void* param;
  while (1==ok)  {
    status = ::wtc_wait( &facility, &param, &substatus );  
    if ( facility == WT_FACILITY_EXIT )   {
      ::wtc_shutdown();
      return;
    }
    if ( status != WT_SUCCESS && status != WT_BADACTIONSTAT && status != WT_NOSUBSCRIBED) {
      this->print(INFO, "+++ exiting wt_wait status = %d param %p substat %d, fac %d.\n",
		  status, param, substatus, facility);
    }
  } 
}

/// Remove a file
int KafkaFS::unlink(const std::string& path)   {
  DevHandle dev; {
    Lock lock(file_system_lock);
    FileMap::iterator i = m_logs.find(path);
    if ( i == m_logs.end() )  {
      this->print(INFO, "+++ unlink %s FAILED [%s]\n", path.c_str(), error(EINVAL).c_str());
      return -EINVAL;
    }
    dev = (*i).second;
    m_logs.erase(i);
  }
  this->print(INFO, "+++ unlink %s refcount %ld\n", path.c_str(), dev->ref_count);
  dev->set_parent(0);
  IocSensor::instance().send(this, EPOLL_CTL_DEL,  dev.ptr());
  IocSensor::instance().send(this, EPOLL_CTL_FREE, dev.ptr());
  return 0;
}

/// Rename a file
int KafkaFS::rename(const std::string& from, const std::string& to)    {
  DevHandle dev_to = get(to);
  if ( dev_to )   {
    this->print(INFO, "+++ rename %s -> %s FAILED [%s]\n", from.c_str(), to.c_str(), error(EEXIST).c_str());
    return -EEXIST;
  }
  DevHandle par_to = get_parent(to);
  if ( !par_to )   {
    this->print(INFO, "+++ rename %s -> %s FAILED [%s]\n", from.c_str(), to.c_str(), error(ENOTDIR).c_str());
    return -ENOTDIR;
  }
  {
    Lock lock(file_system_lock);
    FileMap::const_iterator i = m_logs.find(from);
    if ( i == m_logs.end() )   {
      this->print(INFO, "+++ rename %s -> %s FAILED [%s]\n", from.c_str(), to.c_str(), error(EINVAL).c_str());
      return -EINVAL;
    }
    DevHandle log = (*i).second;
    this->print(INFO, "+++ rename %s -> %s\n", from.c_str(), to.c_str());
    log->path = to;
    log->set_parent(par_to);
    /// erase+insert is ref'count neutral!
    m_logs.erase(i);
    m_logs.insert(make_pair(log->path, log));
  }
  return 0;
}

/// Create and open a file
int KafkaFS::create(const std::string& path, mode_t mode, struct fuse_file_info *fi)   {
  auto dev = get(path);
  if ( dev && (mode&O_EXCL) == O_EXCL )   {
    this->print(INFO, "+++ create %s FAILED [%s]\n", path.c_str(), error(EEXIST).c_str());
    return -EEXIST;
  }
  else if ( dev )    {
    this->print(INFO, "Create: re-use existing file %s\n", path.c_str());
    dev->add_ref();
    return 0;
  }
  return open(path, fi);
}

/// Open new message object
int KafkaFS::open(const std::string& path, struct fuse_file_info *fi)    {
  auto dev = get(path);
  if ( !dev )   {
    auto par = get_parent(path);
    if ( par )   {
      struct stat st = root->info;
      this->print(INFO, "Creating file %s\n", path.c_str());
      st.st_ino   = new_ino();
      st.st_atim  = st.st_mtim = st.st_ctim = { ::time(nullptr), 0 };
      st.st_mode  = S_IFREG | 0666;
      st.st_nlink = 1;
      st.st_size  = 0;
      DevHandle log = new LogDevice(this, path, st);
      log->set_parent(par);
      {
	Lock lock(file_system_lock);
	m_logs.insert(make_pair(path, log->add_ref()));
      }
      log->add_ref();
      return 0;
    }
    this->print(INFO, "+++ open %s FAILED [%s]\n", path.c_str(), error(ENOTDIR).c_str());
    return -ENOTDIR;
  }
  this->print(INFO, "Reopen existing file %s\n", path.c_str());
  dev->add_ref();
  return 0;
}

int KafkaFS::mknod(const string& path, mode_t mode, dev_t device)  {
  auto dev = get(path);
  if ( dev )   {
    this->print(INFO, "+++ mknod: %s FAILED [%s]\n", path.c_str(), error(EEXIST).c_str());
    return -EEXIST;
  }
  auto par = get_parent(path);
  if ( par )   {
    struct stat st = root->info;
    this->print(INFO, "+++ mknod: %s mode: %o dev: %ld\n", path.c_str(), mode, device);
    st.st_dev   = device;
    st.st_ino   = new_ino();
    st.st_atime = st.st_mtime = st.st_ctime = ::time(nullptr);
    st.st_mode  = S_IFIFO | 0666;
    st.st_nlink = 1;
    DevHandle log = new LogDevice(this, path, st);
    log->set_parent(par);
    {
      Lock lock(file_system_lock);
      if ( !m_logs.insert(make_pair(path, log->add_ref())).second )    {
	this->print(INFO, "+++ mknod: %s FAILED [%s]\n", path.c_str(), error(EEXIST).c_str());
	return -EEXIST;
      }
    }
    IocSensor::instance().send(this, EPOLL_CTL_ADD, log.ptr());
    return 0;
  }
  this->print(INFO, "+++ mknod: %s FAILED [%s]\n", path.c_str(), error(ENOTDIR).c_str());
  return -ENOTDIR;
}

/// Check file access permissions
int KafkaFS::access(const string& path, unsigned int mode)   {
  auto dev = get(path);
  if ( dev )   {
    if ( (dev->info.st_mode&mode) == mode )   {
      this->print(INFO, "+++ access: %s mode: %04o \n", path.c_str(), mode);
      return 0;
    }
    this->print(INFO, "+++ access: %s mode: %04o FAILED [%s]\n", path.c_str(), mode, error(EPERM).c_str());
    return -EPERM;
  }
  this->print(INFO, "+++ access: %s FAILED [%s]\n", path.c_str(), error(EBADF).c_str());
  return -EBADF;
}

/// Change the size of a file
int KafkaFS::truncate(const string& path, off_t size)  {
  auto dev = get(path);
  if ( dev )   {
    if ( (dev->info.st_mode&S_IFREG) == S_IFREG )   {
      this->print(INFO, "+++ truncate: %s len:%ld [NOOP]\n", path.c_str(), size);
      dev->info.st_size = size;
      return 0;
    }
    else if ( (dev->info.st_mode&S_IFDIR) == S_IFDIR )   {
      this->print(INFO, "+++ truncate: %s FAILED [%s]\n", path.c_str(), error(EISDIR).c_str());
      return -EISDIR;
    }
    else if ( !(dev->info.st_mode&(S_IWUSR|S_IWGRP|S_IWOTH)) )   {
      this->print(INFO, "+++ truncate: %s FAILED [%s]\n", path.c_str(), error(EACCES).c_str());
      return -EACCES;
    }
  }
  this->print(INFO, "+++ truncate: %s FAILED [%s]\n", path.c_str(), error(ENOENT).c_str());
  return -ENOENT;
}

/// Possibly flush cached data
int KafkaFS::flush(const std::string& path, struct fuse_file_info *fi)   {
  auto dev = get(path);
  if ( dev )   {
    this->print(INFO, "+++ flush %s\n", path.c_str());
    dev->flush_buffers();
    return 0;
  }
  this->print(INFO, "+++ flush %s FAILED [%s]\n", path.c_str(), error(ENOENT).c_str());
  return -ENOENT;  
}

/// Release an open file
int KafkaFS::release(const std::string& path, struct fuse_file_info *fi)   {
  Lock lock(file_system_lock);
  FileMap::iterator i = m_logs.find(path);
  if ( i != m_logs.end() )   {
    DevHandle dev = (*i).second;
    this->print(INFO, "+++ release %s  refcount %ld\n", path.c_str(), dev->ref_count);
    dev->release();
    // If this is the last instance, we have to eject it
    if ( dev->ref_count < 4 )   {
      m_logs.erase(i);
      dev->set_parent(0);
    }
    return 0;
  }
  this->print(INFO, "+++ release %s FAILED [%s]\n", path.c_str(), error(ENOENT).c_str());
  return -ENOENT;  
}

/// Get file attributes.
int KafkaFS::getattr(const std::string& path, struct stat *stbuf)  {
  auto dev = get(path);
  if ( dev )   {
    const struct stat& st = dev->info;
    time_t now = ::time(nullptr);
    stbuf->st_uid = st.st_uid;
    stbuf->st_gid = st.st_gid;
    stbuf->st_atime = dev->info.st_atime = now;
    stbuf->st_mtime = dev->info.st_mtime = now;
    stbuf->st_mode  = st.st_mode;
    stbuf->st_nlink = st.st_nlink;
    stbuf->st_size  = st.st_size;
    this->print(INFO, "+++ getattr %s mode: %04o size:%ld nlink:%ld\n",
		path.c_str(), stbuf->st_mode, stbuf->st_size, stbuf->st_nlink);
    return 0;
  }
  this->print(INFO, "+++ getattr %s FAILED [%s]\n", path.c_str(), error(ENOENT).c_str());
  return -ENOENT;
}

/// Write data to an open file
int KafkaFS::write(const string& path,
		   const char*   buf,
		   size_t        size,
		   off_t         offset,
		   struct fuse_file_info *fi)
{
  auto dev = get(path);
  if ( dev )   {
    this->print(INFO, "+++ write %s OK\n", path.c_str());
    return ::write(STDOUT_FILENO, buf, size);
  }
  this->print(INFO, "+++ write %s FAILED [%s]\n", path.c_str(), error(EINVAL).c_str());
  return -EINVAL;
}

/// Open directory
int KafkaFS::opendir(const std::string& path, struct fuse_file_info* fi)   {
  return 0;
}

/// Read directory
int KafkaFS::readdir(const std::string& path,
		     void*              buf,
		     fuse_fill_dir_t    filler, 
		     off_t,
		     struct fuse_file_info *fi)
{
  auto dev = get(path);
  if ( dev )   {
    char text[PATH_MAX];
    this->print(INFO, "+++ readdir OK:    %-12s nlink:%ld\n", dev->path.c_str(), dev->info.st_nlink);
    filler(buf, ".", NULL, 0);
    filler(buf, "..", NULL, 0);
    for(const auto* c : dev->children)   {
      ::strncpy(text,c->path.c_str(),sizeof(text)-1);
      text[sizeof(text)-1] = 0;
      char* base = ::basename(text);
      this->print(INFO, "+++ readdir\t Entry: %-12s nlink:%ld\n",c->path.c_str(),c->info.st_nlink);
      filler(buf, base, &c->info, 0);
    }
    return 0;
  }
  this->print(INFO, "+++ readdir %s FAILED [%s]\n", path.c_str(), error(EINVAL).c_str());
  return -EINVAL;
}

/// Close directory
int KafkaFS::releasedir(const std::string& path, struct fuse_file_info* fi)   {
  return 0;
}

/// Create a directory 
int KafkaFS::mkdir(const std::string& path, mode_t mode)   {
  DevHandle par = get_parent(path);
  if ( !par || (par->info.st_mode&S_IFDIR) != S_IFDIR )   {
    this->print(INFO, "+++ mkdir %s FAILED [%s]\n", path.c_str(), error(ENOTDIR).c_str());
    return -ENOTDIR;
  }
  DevHandle dev = get(path);
  if ( !dev )   {
    struct stat st = root->info;
    this->print(INFO, "+++ mkdir %s\n", path.c_str());
    st.st_ino   = new_ino();
    st.st_atim  = st.st_mtim = st.st_ctim = {::time(nullptr), 0};
    st.st_mode  = S_IFDIR | 0777;
    st.st_nlink = 1;
    st.st_size  = 0;
    auto* log = new LogDevice(this, path, st);
    log->set_parent(par);
    {
      Lock lock(file_system_lock);
      if ( !m_logs.insert(make_pair(path, log->add_ref())).second )    {
	this->print(INFO, "+++ mkdir: %s FAILED [%s]\n", path.c_str(), error(EEXIST).c_str());
	return -EEXIST;
      }
    }
    return 0;
  }
  return 0;
}

/// Remove a directory
int KafkaFS::rmdir(const std::string& path)   {
  DevHandle dev(nullptr);
  {
    Lock lock(file_system_lock);
    FileMap::iterator i = m_logs.find(path);
    if ( i == m_logs.end() )  {
      this->print(INFO, "+++ rmdir %s FAILED [%s]\n", path.c_str(), error(ENOENT).c_str());
      return -ENOENT;
    }
    dev = (*i).second;
    if ( (dev->info.st_mode&S_IFDIR) != S_IFDIR )  {
      this->print(INFO, "+++ rmdir %s FAILED [%s]\n", path.c_str(), error(ENOTDIR).c_str());
      return -ENOTDIR;
    }
    else if ( !dev->children.empty() )   {
      this->print(INFO, "+++ rmdir %s FAILED [%s]\n", path.c_str(), error(ENOTEMPTY).c_str());
      return -ENOTEMPTY;
    }
    m_logs.erase(i);
  }
  this->print(INFO, "+++ rmdir %s refcount %ld \n", path.c_str(), dev->ref_count);
  dev->set_parent(0);
  dev->release();
  return 0;
}

/// Rename a file
static int fioc_rename(const char *from, const char *to)   {
  return KafkaFS::instance().rename(from, to);
}

/// Remove a file
static int fioc_unlink(const char *path)   {
  return KafkaFS::instance().unlink(path);
}

/**
 * Create and open a file
 *
 * If the file does not exist, first create it with the specified
 * mode, and then open it.
 *
 * If this method is not implemented or under Linux kernel
 * versions earlier than 2.6.15, the mknod() and open() methods
 * will be called instead.
 *
 * Introduced in version 2.5
 */
static int fioc_create(const char* path, mode_t mode, struct fuse_file_info *fi)   {
  return KafkaFS::instance().create(path, mode, fi);
}

/** File open operation
 *
 * No creation (O_CREAT, O_EXCL) and by default also no
 * truncation (O_TRUNC) flags will be passed to open(). If an
 * application specifies O_TRUNC, fuse first calls truncate()
 * and then open(). Only if 'atomic_o_trunc' has been
 * specified and kernel version is 2.6.24 or later, O_TRUNC is
 * passed on to open.
 *
 * Unless the 'default_permissions' mount option is given,
 * open should check if the operation is permitted for the
 * given flags. Optionally open may also return an arbitrary
 * filehandle in the fuse_file_info structure, which will be
 * passed to all file operations.
 *
 * Changed in version 2.2
 */
static int fioc_open(const char *path, struct fuse_file_info *fi)  {
  return KafkaFS::instance().open(path, fi);
}

/** Create a file node
 *
 * This is called for creation of all non-directory, non-symlink
 * nodes.  If the filesystem defines a create() method, then for
 * regular files that will be called instead.
 */
static int fioc_mknod(const char* path, mode_t mode, dev_t dev)  {
  return KafkaFS::instance().mknod(path, mode, dev);
}

/** Possibly flush cached data
 *
 * BIG NOTE: This is not equivalent to fsync().  It's not a
 * request to sync dirty data.
 *
 * Flush is called on each close() of a file descriptor.  So if a
 * filesystem wants to return write errors in close() and the file
 * has cached dirty data, this is a good place to write back data
 * and return any errors.  Since many applications ignore close()
 * errors this is not always useful.
 *
 * NOTE: The flush() method may be called more than once for each
 * open().	This happens if more than one file descriptor refers
 * to an opened file due to dup(), dup2() or fork() calls.	It is
 * not possible to determine if a flush is final, so each flush
 * should be treated equally.  Multiple write-flush sequences are
 * relatively rare, so this shouldn't be a problem.
 *
 * Filesystems shouldn't assume that flush will always be called
 * after some writes, or that if will be called at all.
 *
 * Changed in version 2.2
 */
static int fioc_flush(const char *path, struct fuse_file_info *fi)   {
  return KafkaFS::instance().flush(path, fi);
}

/** Release an open file
 *
 * Release is called when there are no more references to an open
 * file: all file descriptors are closed and all memory mappings
 * are unmapped.
 *
 * For every open() call there will be exactly one release() call
 * with the same flags and file descriptor.	 It is possible to
 * have a file opened more than once, in which case only the last
 * release will mean, that no more reads/writes will happen on the
 * file.  The return value of release is ignored.
 *
 * Changed in version 2.2
 */
static int fioc_release(const char *path, struct fuse_file_info *fi)   {
  return KafkaFS::instance().release(path, fi);
}

/** Get file attributes.
 *
 * Similar to stat().  The 'st_dev' and 'st_blksize' fields are
 * ignored.	 The 'st_ino' field is ignored except if the 'use_ino'
 * mount option is given.
 */
static int fioc_getattr(const char *path, struct stat *stbuf)  {
  return KafkaFS::instance().getattr(path, stbuf);
}

/** Read data from an open file
 *
 * Read should return exactly the number of bytes requested except
 * on EOF or error, otherwise the rest of the data will be
 * substituted with zeroes.	 An exception to this is when the
 * 'direct_io' mount option is specified, in which case the return
 * value of the read system call will reflect the return value of
 * this operation.
 *
 * Changed in version 2.2
 */
static int fioc_read(const char *path, char *buf, size_t size,
		     off_t offset, struct fuse_file_info *fi)
{
  return -EINVAL;
}

/** Write data to an open file
 *
 * Write should return exactly the number of bytes requested
 * except on error.	 An exception to this is when the 'direct_io'
 * mount option is specified (see read operation).
 *
 * Changed in version 2.2
 */
static int fioc_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)   {
  return KafkaFS::instance().write(path, buf, size, offset, fi);
}

/**
 * Check file access permissions
 *
 * This will be called for the access() system call.  If the
 * 'default_permissions' mount option is given, this method is not
 * called.
 *
 * This method is not called under Linux kernel versions 2.4.x
 *
 * Introduced in version 2.5
 */
static int fioc_access(const char* path, int mode)   {
  return KafkaFS::instance().access(path, mode);
}

/** Change the size of a file
 */
static int fioc_truncate(const char *path, off_t size)  {
  return KafkaFS::instance().truncate(path, size);
}

/** Open directory
 *
 * Unless the 'default_permissions' mount option is given,
 * this method should check if opendir is permitted for this
 * directory. Optionally opendir may also return an arbitrary
 * filehandle in the fuse_file_info structure, which will be
 * passed to readdir, closedir and fsyncdir.
 *
 * Introduced in version 2.3
 */
static int fioc_opendir(const char* path, struct fuse_file_info* fi)   {
  return KafkaFS::instance().opendir(path, fi);
}

/** Release directory
 *
 * Introduced in version 2.3
 */
static int fioc_releasedir(const char* path, struct fuse_file_info* fi)   {
  return KafkaFS::instance().releasedir(path, fi);
}

/** Read directory
 *
 * This supersedes the old getdir() interface.  New applications
 * should use this.
 *
 * The filesystem may choose between two modes of operation:
 *
 * 1) The readdir implementation ignores the offset parameter, and
 * passes zero to the filler function's offset.  The filler
 * function will not return '1' (unless an error happens), so the
 * whole directory is read in a single readdir operation.  This
 * works just like the old getdir() method.
 *
 * 2) The readdir implementation keeps track of the offsets of the
 * directory entries.  It uses the offset parameter and always
 * passes non-zero offset to the filler function.  When the buffer
 * is full (or an error happens) the filler function will return
 * '1'.
 *
 * Introduced in version 2.3
 */
static int fioc_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
  return KafkaFS::instance().readdir(path, buf, filler, offset, fi);
}

/** Create a directory 
 *
 * Note that the mode argument may not have the type specification
 * bits set, i.e. S_ISDIR(mode) can be false.  To obtain the
 * correct directory type bits use  mode|S_IFDIR
 * */
static int fioc_mkdir(const char *path, mode_t mode)   {
  return KafkaFS::instance().mkdir(path, mode);
}

/** Remove a directory
 * */
static int fioc_rmdir(const char *path)   {
  return KafkaFS::instance().rmdir(path);
}

static int fioc_ioctl(const char *path, int cmd, void *arg,
		      struct fuse_file_info *fi, unsigned int flags, void *data)
{
  return -EINVAL;
}

#if 0
/// xattr operations are optional and can safely be left unimplemented
static int fioc_setxattr(const char *path, const char *name, const char *value, size_t size, int flags)  {
  int res = lsetxattr(path, name, value, size, flags);
  if (res == -1)
    return -errno;
  return 0;
}

static int fioc_getxattr(const char *path, const char *name, char *value, size_t size)  {
  int res = lgetxattr(path, name, value, size);
  if (res == -1)
    return -errno;
  return res;
}

static int fioc_listxattr(const char *path, char *list, size_t size)  {
  int res = llistxattr(path, list, size);
  if (res == -1)
    return -errno;
  return res;
}

static int fioc_removexattr(const char *path, const char *name)    {
  int res = lremovexattr(path, name);
  if (res == -1)
    return -errno;
  return 0;
}
#endif

namespace   {
  struct options_t {
    std::string topic    { "partition_logs" };
    bool  stats_enabled  { false };
    int   print_level    { KafkaFS::DEBUG };
    int   doexit         { false };
    int   retval;
  };
  enum {
    KEY_STATS,
    KEY_HELP,
    KEY_VERSION,
    KEY_TOPIC,
    KEY_PRINT,
    KEY_COREDUMPS
  };
  void print_help(const char* /* progname */)   {
    ::printf("FUSE log file FS  version " LOGFS_VERSION "\n"
	     "On the fly generate output fifo devices for processes executing the in the\n"
	     "LHCb online system. Based on fioc example from fuse.                    \n\n"
	     "Specific log-FS options are:                                              \n"
	     "    -t <topic>             Kafka topic name to publish messages.          \n"
	     "    -P <level>             Debug printput level.                          \n"
	     "                           DEBUG: 1  INFO: 2 WARNING: 3  ERROR: 4         \n"
	     "    -S                     Collect and publish statistics.                \n");
  }
}

static int fioc_opt_proc(void* data, const char *arg, int key, struct fuse_args *outargs) {
  options_t* opts = (options_t*)data;

  switch (key) {
  case FUSE_OPT_KEY_NONOPT:
    return 1;

  case KEY_PRINT:
    opts->print_level = ::atol(arg+3);
    return 0;

  case KEY_TOPIC:
    opts->topic = arg+3;
    return 0;

  case KEY_STATS:
    opts->stats_enabled = 1;
    return 0;

  case KEY_HELP:
    print_help(outargs->argv[0]);
    fuse_opt_add_arg(outargs, "-ho");
    return 0;

  case KEY_VERSION:
    printf("Fuse logfs version: " LOGFS_VERSION "\n");
    opts->doexit = 1;
    return 1;

  default:
    opts->retval = 1;
    return 1;
  }
}

extern "C" int kafkalog_main(int argc, char **argv)  {
  struct fuse_args args = FUSE_ARGS_INIT(argc, argv);
  struct fuse_operations fioc_oper;
  struct fuse_opt        fioc_opts[] = {
    FUSE_OPT_KEY("--help",    KEY_HELP),
    FUSE_OPT_KEY("--version", KEY_VERSION),
    FUSE_OPT_KEY("-h",        KEY_HELP),
    FUSE_OPT_KEY("-V",        KEY_VERSION),
    FUSE_OPT_KEY("-t=",       KEY_TOPIC),
    FUSE_OPT_KEY("-S",        KEY_STATS),
    FUSE_OPT_KEY("-P=",       KEY_PRINT),
    FUSE_OPT_END
  };
  options_t options;
  if ( fuse_opt_parse(&args, &options, fioc_opts, fioc_opt_proc) == -1)
    return EINVAL;

  if ( options.doexit )
    return 1;

  ::memset(&fioc_oper, 0, sizeof(fioc_oper));
  fioc_oper.getattr	= fioc_getattr;
  fioc_oper.opendir     = fioc_opendir;
  fioc_oper.releasedir  = fioc_releasedir;
  fioc_oper.readdir	= fioc_readdir;
  fioc_oper.mkdir	= fioc_mkdir;
  fioc_oper.rmdir	= fioc_rmdir;
  fioc_oper.truncate	= fioc_truncate;
  fioc_oper.mknod	= fioc_mknod;
  fioc_oper.create	= fioc_create;
  fioc_oper.open	= fioc_open;
  fioc_oper.flush	= fioc_flush;
  fioc_oper.release	= fioc_release;
  fioc_oper.rename      = fioc_rename;
  fioc_oper.read	= fioc_read;
  fioc_oper.write	= fioc_write;
  fioc_oper.access	= fioc_access;
  fioc_oper.ioctl	= fioc_ioctl;
  fioc_oper.unlink      = fioc_unlink;
#if 0
  fioc_oper.setxattr    = fioc_setxattr;
  fioc_oper.getxattr    = fioc_getxattr;
  fioc_oper.listxattr   = fioc_listxattr;
  fioc_oper.removexattr = fioc_removexattr;
#endif
  KafkaFS::instance().topic       = options.topic;
  KafkaFS::instance().print_level = options.print_level;

  return fuse_main(argc, argv, &fioc_oper, NULL);
}
#endif
