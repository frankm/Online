//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "LogServer.h"

/// Kafka include file
#include "rdkafka.h"  /* for Kafka driver */

/// C/C++ include files
#include <map>
#include <memory>
#include <string>
#include <sstream>

/// kafka namespace declaration
namespace kafka  {

  namespace  {

    class ConsumerImp : public Consumer::Implementation  {
    public:
      enum  {
	PUBLISH_DATA  =   0x1, /* tabular format */
	PUBLISH_SUMMARY = 0x2  /* summary format */
      };

      /// Kafka error callback
      static void err_call (rd_kafka_t* rk, int err, const char* reason, void* opaque);
      /// Kafka statistics callback
      static int  stats_call (rd_kafka_t *rk, char *json, size_t json_len, void* /* opaque */);
      /// Kafka throttle callback
      static void throttle_call (rd_kafka_t *rk, const char *broker, int32_t broker_id, int msec, void* opaque);
      /// Kafka offset callback
      static void offset_commit_call (rd_kafka_t *rk, rd_kafka_resp_err_t err, rd_kafka_topic_partition_list_t *offsets, void* opaque);
      /// Kafka re-balancing callback
      static void rebalance_call (rd_kafka_t *rk, rd_kafka_resp_err_t err,
				  rd_kafka_topic_partition_list_t *partitions, void* /* opaque */);

      /// Signal handler to stop
      static void sig_stop (int sig);
      /// Signal handler for monitoring
      static void sig_usr1 (int sig);

      /// Reference to kafka instance
      rd_kafka_t*                      m_rk = nullptr;
      /// Reference to kafka configuration instance
      rd_kafka_conf_t*                 m_config = nullptr;
      /// Reference to kafka topic configuration instance
      rd_kafka_topic_conf_t*           m_topic_config = nullptr;
      /// Reference to kafka topic partition list
      rd_kafka_topic_partition_list_t* m_topics = nullptr;

      /// Set configuration value
      const ConsumerImp& set_string(const std::string& name, const std::string& value)  const;

    public:
      /// Default constructor
      ConsumerImp();
      /// Default destructor
      virtual ~ConsumerImp();

      rd_kafka_conf_t* config()  const  {  return m_config;  }
      /// Set configuration value
      template <typename T>
      const ConsumerImp& set(const std::string& name, const T& value)  const;
      /// Set configuration value
      const ConsumerImp& set(const std::string&& name_val)  const;

      /// Base consumer overload: Add new listening topic
      virtual void add_topic(const std::string& name)  override;
      /// Base consumer overload: Start listening to kafka topics
      virtual int  listen(const std::string& brokers)  override;
      /// Base consumer overload: Shutdown processing
      virtual void shut_down()  override;
      /// Run the Kafka instance forever
      virtual void run()  override;

      /// List the consumer configuration
      void list_configuration()  const;
      /// Dump the kafka configuration
      void dump_configuration()  const;
      /// Dump the consumer topics
      void dump_topic_configuration()  const;
      /// Handler for all messages
      void handle_message(rd_kafka_message_t *msg);
    };
  }
}
using namespace kafka;

/// Framework include files
#include <RTL/rtl.h>

/// C/C++ include files
#include <ctime>
#include <cctype>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <csignal>
#include <sys/time.h>
#include <unistd.h>

static int verbosity = 1;
static volatile sig_atomic_t s_running = 1;
static ConsumerImp* s_global_consumer = nullptr;

/// Set configuration value
template <typename T> inline 
const ConsumerImp& ConsumerImp::set(const std::string& name, const T& value)  const  {
  std::stringstream str;
  str << value;
  return this->set_string(name, str.str());
}

/// Default constructor
ConsumerImp::ConsumerImp()   
{
  s_global_consumer = this;
  m_config = ::rd_kafka_conf_new();
  m_topics = rd_kafka_topic_partition_list_new(1);
  m_topic_config = ::rd_kafka_topic_conf_new();
  ::rd_kafka_conf_set_error_cb(m_config, err_call);
  ::rd_kafka_conf_set_stats_cb(m_config, stats_call);
  ::rd_kafka_conf_set_throttle_cb(m_config, throttle_call);
  ::rd_kafka_conf_set_offset_commit_cb(m_config, offset_commit_call);
  /// High-level balanced Consumer
  ::rd_kafka_conf_set_rebalance_cb(m_config, rebalance_call);
  /// Kafka topic configuration
  ::rd_kafka_topic_conf_set(m_topic_config, "auto.offset.reset", "earliest", NULL, 0);
}

/// Default destructor
ConsumerImp::~ConsumerImp()
{
}

/// Start listening to kafka topics
int ConsumerImp::listen(const std::string& brokers)   {
  char errstr[512];
  /// Create Kafka handle
  if ( !(m_rk = ::rd_kafka_new(RD_KAFKA_CONSUMER, m_config, errstr, sizeof(errstr))) )  {
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ Failed to create Kafka consumer: %s",errstr);
    return 0;
  }
  s_global_consumer = this;
  /// Forward all events to consumer queue 
  ::rd_kafka_poll_set_consumer(m_rk);
  ::signal(SIGIO, ConsumerImp::sig_stop);
#ifdef SIGUSR1
  ::signal(SIGUSR1, ConsumerImp::sig_usr1);
#endif
  ::rd_kafka_conf_set_default_topic_conf(m_config, m_topic_config);
  /// Add broker(s) 
  ::lib_rtl_output(LIB_RTL_ERROR, "+++ brokers specified: %s",brokers.c_str());
  if ( brokers.empty() || ::rd_kafka_brokers_add(m_rk, brokers.c_str()) < 1) {
    ::lib_rtl_output(LIB_RTL_ERROR, "+++ No valid brokers specified");
    return 0;
  }
  rd_kafka_resp_err_t err = ::rd_kafka_subscribe(m_rk, m_topics);
  if ( err )  {
    ::lib_rtl_output(LIB_RTL_ERROR, "+++ Subscribe failed: %s", rd_kafka_err2str(err));
    return 0;
  }
  return 1;
}

/// Shutdown processing
void ConsumerImp::shut_down()   {
  rd_kafka_resp_err_t err;
  monitor.t_end = rd_clock();
  auto rk = m_rk;
  m_rk = nullptr;
  if ( m_rk )   {
    err = ::rd_kafka_consumer_close(rk);
    if (err)   {
      lib_rtl_output(LIB_RTL_ERROR, "+++ Failed to close consumer: %s",::rd_kafka_err2str(err));
    }
    for(Listeners::iterator i=listeners.begin(); i!=listeners.end(); ++i)
      (*i)->handle_monitoring(monitor);
    ::rd_kafka_destroy(rk);
    ::rd_kafka_topic_partition_list_destroy(m_topics);
    m_topics = nullptr;
  }
  /// Let background threads clean up and terminate cleanly
  ::rd_kafka_wait_destroyed(2000);
}

void ConsumerImp::run()    {
  while ( s_running )  {
    /// Consume messages.
    /// A message may either be a real message, or an event (if msg->err is set).
    rd_kafka_message_t *msg = ::rd_kafka_consumer_poll(m_rk, 1000);
    if ( msg )   {
      /// Start measuring from first message received
      if ( !monitor.t_start )   {
	monitor.t_start = monitor.t_last = rd_clock();
      }
      this->handle_message(msg);
      ::rd_kafka_message_destroy(msg);
    }
    auto now = rd_clock();
    if ( now - monitor.t_last > monitor_interval )   {
      for(Listeners::iterator i=listeners.begin(); i!=listeners.end(); ++i)
	(*i)->handle_monitoring(monitor);
      monitor.t_last        = now;
      monitor.messages_last = monitor.messages;
      monitor.bytes_last    = monitor.bytes;
    }
  }
}

void ConsumerImp::sig_stop (int /* sig */) {
  if ( !s_running )
    exit(0);
  s_running = 0;
}

void ConsumerImp::sig_usr1 (int /* sig */) {
  ::rd_kafka_dump(stdout, s_global_consumer->m_rk);
}

/// Kafka error callback
void ConsumerImp::err_call(rd_kafka_t*  rk,
			   int      /*  err */,
			   const char*  reason,
			   void *   /*  opaque */) {
  ::lib_rtl_output(LIB_RTL_INFO,"+++ ERROR CALLBACK: %s: %s", ::rd_kafka_name(rk), reason);
}

/// Kafka throttle callback
void ConsumerImp::throttle_call(rd_kafka_t* rk,
				const char* broker,
				int32_t     broker_id,
				int         msec,
				void*    /* opaque */)
{
  ::lib_rtl_output(LIB_RTL_INFO,"+++ THROTTLED %s: %dms by %s (%8X)", ::rd_kafka_name(rk), msec, broker, broker_id);
}

/// Kafka statistics callback
int ConsumerImp::stats_call (rd_kafka_t* /* rk */,
			     char*       /* json */,
			     size_t      /* json_len */,
			     void*       /* opaque */)
{
  return 0;
}

void ConsumerImp::offset_commit_call(rd_kafka_t *rk,
				     rd_kafka_resp_err_t err,
				     rd_kafka_topic_partition_list_t *offsets,
				     void* /* opaque */)
{
  if ( err || verbosity >= 2 )   {
    ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: Offset commit of %d partition(s)", ::rd_kafka_name(rk), offsets->cnt);
  }
  for (int i = 0 ; i < offsets->cnt ; i++) {
    rd_kafka_topic_partition_t *par = &offsets->elems[i];
    if ( par->err || verbosity >= 2 )   {
      ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: %s [%8X] @ %16lX", ::rd_kafka_name(rk), par->topic, par->partition, par->offset);
    }
  }
}

void ConsumerImp::rebalance_call(rd_kafka_t *rk, 
				 rd_kafka_resp_err_t err,
				 rd_kafka_topic_partition_list_t *partitions,
				 void* /* opaque */)
{
  switch (err)   {
  case RD_KAFKA_RESP_ERR__ASSIGN_PARTITIONS:
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ Group rebalanced: %d partition(s) assigned",partitions->cnt);
    s_global_consumer->monitor.partitions = partitions->cnt;
    ::rd_kafka_assign(rk, partitions);
    break;

  case RD_KAFKA_RESP_ERR__REVOKE_PARTITIONS:
    lib_rtl_output(LIB_RTL_ERROR,"+++ Group rebalanced: %d partition(s) revoked",partitions->cnt);
    s_global_consumer->monitor.partitions = 0;
    ::rd_kafka_assign(rk, NULL);
    break;
    
  default:
    break;
  }
}

/// Set configuration value
const ConsumerImp& ConsumerImp::set_string(const std::string& n, const std::string& v)  const {
  char err[512];
  if (rd_kafka_conf_set(m_config, n.c_str(), v.c_str(),err, sizeof(err)) != RD_KAFKA_CONF_OK)  {
    ::lib_rtl_output(LIB_RTL_ERROR, "%% %s", err);
    exit(1);
  }
  return *this;
}

/// Set configuration value
const ConsumerImp& ConsumerImp::set(const std::string&& name_val)  const  {
  char *name = (char*)name_val.c_str();
  char* val  = ::strchr(name, '=');
  if ( !val ) {
    lib_rtl_output(LIB_RTL_ERROR, "+++ Expected -X property=value, not %s", name);
    exit(EINVAL);
  }
  *val = 0;
  return this->set(name, ++val);
}

/// Add new listening topic
void ConsumerImp::add_topic(const std::string& name)   {
  ::lib_rtl_output(LIB_RTL_ERROR, "+++ Adding topic: %s", name.c_str());
  ::rd_kafka_topic_partition_list_add(m_topics, name.c_str(), RD_KAFKA_PARTITION_UA);
}

/// List the consumer configuration
void ConsumerImp::list_configuration()   const   {
  ::rd_kafka_conf_properties_show(stdout);
}

/// Dump the kafka configuration
void ConsumerImp::dump_configuration()   const   {
  size_t cnt = 0;
  const char **arr = ::rd_kafka_conf_dump(m_config, &cnt);
  ::fprintf(stderr,"# Global config\n");
  for( size_t i = 0 ; i < cnt ; i += 2 )
    ::fprintf(stderr,"%s = %s\n", arr[i], arr[i+1]);
  ::fprintf(stderr,"\n");
  ::rd_kafka_conf_dump_free(arr, cnt);
  dump_topic_configuration();
}

/// Dump the consumer topics
void ConsumerImp::dump_topic_configuration()   const   {
  size_t cnt = 0;
  const char **arr = ::rd_kafka_topic_conf_dump(m_topic_config, &cnt);
  ::fprintf(stderr,"# Topic config\n");
  for( size_t i = 0 ; i < cnt ; i += 2 )
    ::fprintf(stderr,"%s = %s\n", arr[i], arr[i+1]);
  ::fprintf(stderr,"\n");
  ::rd_kafka_conf_dump_free(arr, cnt);
}

/// Handler for all messages
void ConsumerImp::handle_message (rd_kafka_message_t *msg) {
  if ( msg )   {
    rd_kafka_message_t& m = *msg;
    if ( m.err )   {
      if ( m.err == RD_KAFKA_RESP_ERR__PARTITION_EOF )   {
	monitor.offset = m.offset;
      }
      else  {
	if( m.err == RD_KAFKA_RESP_ERR__UNKNOWN_PARTITION ||
	    m.err == RD_KAFKA_RESP_ERR__UNKNOWN_TOPIC )  {
	  s_running = 0;
	}
	monitor.messages_dr_err++;
      }
      for(Listeners::iterator i=listeners.begin(); i != listeners.end(); ++i)
	(*i)->handle_error(m);
      return;
    }
    monitor.messages++;
#if 0
    if ( (monitor.messages%10) == 0 )
      ::lib_rtl_output(LIB_RTL_INFO,"Processed %ld messages",monitor.messages);
#endif
    monitor.bytes += m.len;
    monitor.offset = m.offset; 
    for(Listeners::iterator i=listeners.begin(); i != listeners.end(); ++i)
      (*i)->handle_message(m);
  }
}

static void usage(int /* argc */, char** argv)   {
  ::printf(
	  "Usage: %s -t <topic> [-b <broker,broker..>] [options..]          \n"
	  "                                                                 \n"
	  "librdkafka version %s (0x%08x)                                   \n"
	  "                                                                 \n"
	  " Options:                                                        \n"
	  "  -g <groupid> High-level Kafka Consumer group identifier        \n"
	  "  -t <topic>   Topic to consume.....                             \n"
	  "  -q <length>  Length log message queue.                         \n"
	  "  -i <msecs>   Monitor time interval in milli seconds.           \n"
	  "  -s <msecs>   Session timeout in milli seconds.                 \n"
	  "  -d [facs..]  Enable debugging contexts:                        \n"
	  "               %s            \n"
	  "  -v <number>  Verbosity level                                   \n"
	  "  -X <prop=name> Set arbitrary librdkafka configuration property \n"
	  "  -L           Show full list of supported properties.           \n"
	  "  -C           Show configuration                                \n",
	  argv[0],
	  rd_kafka_version_str(), rd_kafka_version(),
	  RD_KAFKA_DEBUG_CONTEXTS);
  exit(1);
}

/// kafka namespace declaration
namespace kafka  {

  template <> std::unique_ptr<Consumer::Implementation>
  Consumer::create<KafkaConsumer>(int argc, char** argv)
  {
    auto consumer = std::make_unique<ConsumerImp>();
    std::string debug;
    //std::string broker        = "10.128.96.12:30400";
    std::string group_id      = "blabla";
    long        stat_interval = 5000;
    long        que_len       = 1000000;
    long        session_tmo   = 6000;
    bool        list_conf     = false;
    bool        dump_conf     = false;
    int         opt;

    /* Tell rdkafka to (try to) maintain 1M messages in its internal receive buffers.
     * This is to avoid application -> rdkafka -> broker  per-message ping-pong
     * latency. The larger the local queue, the higher the performance.
     */
    (*consumer)
      .set("enable.auto.commit",          "false")
      .set("enable.auto.offset.store",    "false")
      .set("fetch.error.backoff.ms",      1)
      .set("statistics.interval.ms",      0)
      .set("auto.offset.reset",           "latest")
      .set("enable.partition.eof",        "true")
      .set("internal.termination.signal", SIGIO);     // Quick termination

    while ((opt=getopt(argc, argv, "t:i:d:g:q:s:u:v:LCX:")) != -1) {
      switch (opt) {
      case 't':
	consumer->add_topic(optarg);
	break;
      case 'i':
	consumer->monitor_interval = 1000 * ::atoi(optarg);
	break;
      case 'd':
	debug       = optarg;
	break;
      case 'g':
	group_id    = optarg;
	break;
      case 'q':
	que_len     = ::atol(optarg);
	break;
      case 's':
	session_tmo = ::atol(optarg);
	break;
      case 'v':
	verbosity   = ::atol(optarg);
	break;
      case 'C':
	dump_conf   = true;
	break;
      case 'L':
	list_conf   = true;
	break;
      case 'X':
	consumer->set(optarg);
	break;
      default:
	::lib_rtl_output(LIB_RTL_ERROR,"Unknown option: %c", opt);
	usage(argc, argv);
      }
    }
    (*consumer)
      .set("queued.min.messages",         que_len)
      .set("session.timeout.ms",          session_tmo)
      //.set("bootstrap.servers",           broker)
      .set("statistics.interval.ms",      stat_interval)
      .set("group.id",                    group_id);

    if ( !debug.empty() )   {
      consumer->set("debug", debug);
    }
    if ( list_conf )   {
      consumer->list_configuration();
      exit(0);
    }
    if ( dump_conf )   {
      consumer->dump_configuration();
      exit(0);
    }
    return consumer;
  }
}
