//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_OUTPUTLISTENER_H
#define ROLOGGER_OUTPUTLISTENER_H

/// Framework include files
#include "LogServer.h"

/// C/C++ include files
#include <vector>
#include <regex>

/// kafka namespace declaration
namespace kafka  {

  /// Terminal logger class
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class OutputListener : public Listener  {
  public:
    typedef std::vector<std::regex> Matches;
    Matches node_match;
    Matches utgid_match;
    Matches message_veto;
    Matches message_match;
    std::string select_tag;
    bool    print_raw     = false;
    bool    print_stats   = false;
    bool    print_counter = false;
    bool    simple_msg    = false;
    bool    have_colors   = false;

    void print_msg(const char* time_stamp,
		   const char* message,
		   const char* systag,
		   const char* utgid,
		   const char* host,
		   const char* physical,
		   const char* counter);
  public:
    /// Default constructor
    OutputListener() = default;
    /// Default destructor
    virtual ~OutputListener() = default;
    /// Second level object initialization
    virtual int initialize()  override;
    /// Handler for all messages
    virtual void handle_payload(const char* topic,
				char* payload, size_t plen,
				char* key, size_t klen)     override;
    /// Handle monitoring 
    virtual void handle_monitoring(const Monitor& monitor)  override;
  };
}
#endif // ROLOGGER_OUTPUTLISTENER_H
