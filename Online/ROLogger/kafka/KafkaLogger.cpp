//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "KafkaProducer.h"
#include <RTL/Logger.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <vector>
#include <cerrno>
#include <cstring>


namespace  {

  int help()   {
    ::printf("run_kafka_logger -opt [-opt]                                  \n"
	     "   Run server on a HLT node and feed logs to kafka.           \n"
	     "                                                              \n"
	     "     -h             Print this help.                          \n"
	     "     -I             Connect to stdin as message source and    \n"
	     "                    forward the message to terminal.          \n"
	     "     -K             Connect kafka instance directly and       \n"
	     "                    display all matching messages to terminal.\n"
	     "     -L             Connect to all services on the main pub.  \n"
	     "                    instance on ecs03 (DEFAULT).              \n"
	     "     -NodeLogger    Run node logger (pick up messages from one\n"
	     "                    node directly. Do not connect to main     \n"
	     "                    publishing instance.                      \n"
	     "     -b <broker>    Set DIM message broker.                   \n"
	     "     -O <level>     Set RTL output level (1...4).             \n"
	     "     -P <number>    Print statistics every <number> messages. \n"
	     "     -V <reg-ex>    Veto messages matching regular expression.\n");
    return 0;
  }
}

extern "C" int run_kafka_logger (int argc, char **argv) {
  using namespace kafka;
  int                     output_level = LIB_RTL_INFO;
  std::string             broker, value;
  bool print_help =       false;
  bool print_stats =      false;
  int  consumer_type =    DIMLOG_CONSUMER;
  std::vector<char*>      args;
  std::vector<std::regex> message_veto;
  std::string dns, name, tag;

  if ( ::getenv("UTGID") )  {
    name = ::getenv("UTGID");
  }
  for( int i=0; i<argc; ++i )   {
    char c = argv[i][0];
    if ( c == '-' )   {
      c = argv[i][1];
      switch(c)   {
      case 'b':
	broker = argv[++i];
	break;
      case 'h':
	print_help = true;
	args.push_back(argv[i]);
	break;
      case 'n':
	name = argv[++i];
	break;
      case 'D':
	dns  = argv[++i];
	break;
      case 'O':
	output_level = ::atol(argv[++i]);
	break;
      case 'V':
	value = argv[++i];
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Adding message veto: %s",value.c_str());
	message_veto.emplace_back(std::regex(value));
	break;
      case 'L':
	consumer_type = DIMLOG_CONSUMER;
	break;
      case 'N': // NodeConsumer
	consumer_type = DIMNODE_CONSUMER;
	break;
      case 'K':
	consumer_type = KAFKA_CONSUMER;
	break;
      case 'I':
	consumer_type = STDIN_CONSUMER;
	break;
      case 'Q':
	tag = argv[++i];
	break;
      case 'i':
	[[fallthrough]];
      case 'P':
	print_stats = true;
	[[fallthrough]];
      default:
	args.push_back(argv[i]);
	break;
      }
      continue;
    }
    args.push_back(argv[i]);
  }
  RTL::Logger::install_log(RTL::Logger::log_args(output_level));
  RTL::Logger::print_startup("Kafka message producer");
  KafkaProducer listener(args);
  listener.message_veto = std::move(message_veto);
  listener.print_stats  = print_stats;
  listener.initialize();

  auto imp = Consumer::create(consumer_type, args);
  if ( !imp.get() || print_help )   {
    help();
    return EINVAL;
  }
  Consumer      consumer(std::move(imp));
  consumer.add_listener(&listener);
  if ( !consumer.listen(broker) )    {
    return EINVAL;
  }
  if ( !dns.empty() )   {
    consumer.publish_monitor(dns, name, tag);
  }
  consumer.run();
  return 0;
}
