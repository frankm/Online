//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_NODELISTENER_H
#define ROLOGGER_NODELISTENER_H

// C++ include files
#include <string>
#include <regex>
#include "CPP/Interactor.h"

/*
 *   ROLogger namespace declaration
 */
namespace ROLogger {

  /**@class NodeListener NodeListener.h ROLogger/NodeListener.h
   *
   *   DIM error logger
   *
   *   @author M.Frank
   */
  class NodeListener : public Interactor {
  protected:
    /// Reference to parent for communication
    Interactor* m_parent;
    /// Listener service name
    std::string m_name;
    /// Listener facility
    std::string m_facility;
    std::vector<std::regex> m_matches;
    /// Dim service ID for subfarms
    int         m_loggerDP;

    bool  check_match(const std::string& node)   const;
    static void svcHandler(void* tag, void* address, int* size);

  public:
    /// Standard constructor with object setup through parameters
    NodeListener(Interactor* parent,const std::string& name, const std::vector<std::string>& matches);
    /// Standard destructor
    virtual ~NodeListener();
    /// Access name
    const std::string& name() const {   return m_name; }
  };
}      /* End namespace ROLogger */
#endif /* ROLOGGER_NODELISTENER_H */
