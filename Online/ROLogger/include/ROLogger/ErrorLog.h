//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

#ifndef ROLOGGER_ERRORLOG_H
#define ROLOGGER_ERRORLOG_H

// Framework include files
#include "CPP/Interactor.h"

/*
 *   ROLogger namespace declaration
 */
namespace ROLogger {
  // Forward declarations

  class Logger;

  /**@class ErrorLog ErrorLog.h ROLogger/ErrorLog.h
   *
   *   DIM error logger
   *
   *   @author M.Frank
   */
  class ErrorLog : public Interactor {
  protected:
    /// Reference to message logger
    Interactor* m_messageLog;
    /// Reference to history logger
    Interactor* m_historyLog;
    /// Reference to partition display
    Interactor* m_partDisplay;
    /// Reference to partition listener
    Interactor* m_partListener;

  public:
    /// Standard constructor with object setup through parameters
    ErrorLog(int argc, char** argv);
    /// Standard destructor
    virtual ~ErrorLog();

    /// Shutdown client windows
    void shutdown();
    /// Display callback handler
    void handle(const CPP::Event& ev) override;
    /// Help printout for RTL CLI
    static void help_fun();
  };
}      /* End namespace ROLogger */
#endif /* ROLOGGER_ERRORLOG_H */
