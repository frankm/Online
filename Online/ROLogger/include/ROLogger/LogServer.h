//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_LOGSERVER_H
#define ROLOGGER_LOGSERVER_H

/// Framework include files
#include <CPP/Interactor.h>

// C++ include files
#include <string>
#include <mutex>
#include <regex>

/*
 *   ROLogger namespace declaration
 */
namespace ROLogger {

  /**@class LogServer LogServer.h ROLogger/LogServer.h
   *
   *   DIM error logger
   *
   *   @author M.Frank
   */
  class LogServer : public CPP::Interactor {
    typedef std::map<std::string, int> dim_inputs;
    int         m_poll_fd = -1;
    /// Poll timeout
    int         m_poll_tmo = 10;

    /// Dim service ID for DNS services
    int         m_dns_dp = 0;
    /// Input FIFO file descriptor
    int         m_in_fifo_fd = -1;
    /// Map of input dim services
    dim_inputs  m_in_dim_services;
    std::mutex  m_input_guard;

    /// Output FIFO file descriptor
    int         m_out_fifo_fd = -1;
    /// Output DIM service 
    int         m_out_dim_service = -1;
    std::string m_out_dim_data;
    std::mutex  m_output_guard;

    std::regex  m_in_service_match;

    /// Create new poll pool
    int _poll_create(int max_count);
    /// Add connection to poll pool
    int _poll_add(int new_fd);
    /// Remove connection from poll pool
    int _poll_del(int fd);
    /// Poll on connections delivering data
    int _poll(int& events, int tmo);

  public:
    /// Initializing constructor
    LogServer();
    /// Default destructor
    virtual ~LogServer(); 

    /// DIM command service callback on DNS for newly created services
    static void dnsDataHandler(void* tag, void* address, int* size);
    /// DIM command service callback to handle log data
    static void logDataHandler(void* tag, void* address, int* size);

    /// Connect input fifo
    int connectInputFifo(const std::string& fifo);
    /// Connect output fifo
    int connectOutputFifo(const std::string& fifo);
    /// Listen to input dns if set
    void listen_in_dns();

    /// Close all log files
    int close();
    /// Print content of log files as they come....
    int publish(const std::string& value);
    /// Read fifo input source
    void read_in_fifos();
    /// Write message to output device
    int write_out_fifo(const std::string& msg);

    /// Interactor overload: event interrupt handler
    virtual void handle(const CPP::Event& ev)  override;
  };
}      /* End namespace ROLogger */
#endif /* ROLOGGER_LOGSERVER_H */
