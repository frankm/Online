//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_FILELOGGER_H
#define ROLOGGER_FILELOGGER_H

// Framework include files
#include "ROLogger/MessageLogger.h"

// Forward declarations
namespace UPI { class ReallyClose; }

/*
 *   ROLogger namespace declaration
 */
namespace ROLogger {
  // Forward declarations
  class PartitionListener;

  /**@class FileLogger FileLogger.h ROLogger/FileLogger.h
   *
   *   DIM error logger
   *
   *   @author M.Frank
   */
  class FileLogger : public MessageLogger  {
    typedef std::vector<std::string> Farms;
    enum { kByte = 1024, MByte = 1024*1024, GByte = 1024*1024*1024 };
  protected:
    /// Menu id
    int                m_id = -1;
    long               m_fileSize = 0;
    /// Reference to partition listener
    PartitionListener* m_listener = 0;
    /// Property: Output directory
    std::string        m_outdir;
    std::string        m_partition;
    /// Property: Current output file name
    std::string        m_outputName;
    /// Property: Handler script called when file was closed.
    std::string        m_fileHandler;
    /// Property: Flag to use time-stamp for file name
    bool               m_useTimeForName = true;
    /// Property: Use RFC3339 https://www.ietf.org/rfc/rfc3339.txt for time-format
    bool               m_timerfc3339 = false;


    /// Parameter buffer
    char               m_msgSeverity[16];
    /// Buffer to store HT farm content
    Farms              m_farms;
    /// Prefix for date check of messages
    struct tm          m_date, m_checkDate;
    /// Menu to shutdown application
    UPI::ReallyClose*  m_quit = 0;
    /// Flag to dump messages to UPI
    bool               m_connected = false;

    /// Open new output file
    FILE* openOutput();
    /// Print multi-line header information with source information
    virtual void printSources(const std::vector<std::string>& titles, bool with_guards=true)  override;
    /// Print single message retrieved from error logger
    virtual void printMessage(const char* msg, bool crlf=true) override;
    // Format message according to RFC 3339
    // returns a pointer to a string with the re-formatted message
    // ownership is passed, this string must be free-ed by the caller
    char* formatTimeRFC3339(const char* msg);
  public:
    /// Standard constructor with object setup through parameters
    FileLogger(int argc, char** argv);
    /// Standard destructor
    virtual ~FileLogger();
    /// Interactor callback handler
    virtual void handle(const Event& ev) override;
    /// 
    bool haveUPI() const  { return m_quit != 0; }
    //
    int m_thisYear;
  };
}
#endif /* ROLOGGER_FILELOGGER_H */
