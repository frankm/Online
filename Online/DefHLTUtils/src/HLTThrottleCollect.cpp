/*
 * HLTDefBridge.cpp
 *
 *  Created on: Jun 27, 2012
 *      Author: Beat Jost
 */

#include "dim/dic.hxx"
#include "dim/dis.hxx"
#include <string>
#include "ROMon/Utilities.h"
#include "stdio.h"
#include "RTL/rtl.h"
#include "RTL/strdef.h"
#include <set>
#include <map>

using namespace std;
static char s_empty[]="";
class Throttle;

class NodeEnt
{
  public:
    string m_host;
    string m_hostent;
    Throttle *m_throttle;
    int m_tvalue;
    NodeEnt(string&h, Throttle* t)
    {
        m_host = h;
        m_hostent="";
        m_throttle = t;
        m_tvalue = -1;
    }
};
map<string,NodeEnt*> m_ServiceMap;
set<string> m_received;
set<string> m_MepHosts;
DimService *m_outService;
class Throttle:public DimInfo
{
  public:
    NodeEnt *m_NodeEnt;
    string m_node;
    int tval;
    Throttle(string &nam,string &h):DimInfo(nam.c_str(),10,s_empty)
    {
      m_node = h;
      m_NodeEnt = 0;
      tval =0;
    }
    void infoHandler() override
    {
      if (m_NodeEnt == 0)
      {
        m_NodeEnt = m_ServiceMap[m_node];
      }
      string inp=getString();
      if (inp == "")
      {
//        ::printf("Server on node %s died...\n",m_node.c_str());
        return;
      }
      m_received.insert(m_node);
      if (inp.find("disable") != string::npos)
      {
        tval =0;
        m_NodeEnt->m_hostent = m_node +" "+to_string(0);
      }
      else
      {
        string p = "Rate limiting active with sleeping time ";
        string v=inp.substr(p.size());
        v=v.substr(0,v.find(" "));
        m_NodeEnt->m_hostent = m_node +" "+v;
      }
//      ::printf("%s\n",m_NodeEnt->m_hostent.c_str() );
      if (m_received.size() == m_MepHosts.size())
      {
        string sumdata="";
        for (auto i = m_ServiceMap.begin();i!=m_ServiceMap.end();i++)
        {
          sumdata = sumdata+i->second->m_hostent+"|";
        }

        m_received.clear();
        sumdata = sumdata+"\0";
        m_outService->updateService((void*)sumdata.c_str(),sumdata.size()+1);
      }
    }
};
class DNSServers : public DimInfo
{
  public:
    DNSServers():DimInfo("DIS_DNS/SERVER_LIST",s_empty)
    {
      m_MepHosts.clear();
    }
    void infoHandler() override
    {
      char *inp=getString();
//      ::printf("%s: Got Input %s\n",RTL::processName().c_str(),inp);
      if (inp[0] == '+') //new server(s)
      {
        inp++;
//        ::printf("New Server: %s\n",inp);
        dyn_string *servs = Strsplit(inp,"|");
        for (size_t i=0;i<servs->size();i++)
        {
          if (servs->at(i).find("_MEPrx") != string::npos)
          {
            dyn_string *meprx=Strsplit(servs->at(i).c_str(),"_");
//            ::printf("Adding %s to node set\n",meprx->at(1).c_str());
            m_MepHosts.insert(meprx->at(1));
            string sname = servs->at(i).substr(0,servs->at(i).find("@"));
            sname = sname +"/OverflowStatus";
            Throttle *t = new Throttle(sname,meprx->at(1));
            m_ServiceMap[meprx->at(1)] = new NodeEnt(meprx->at(1),t);
            delete meprx;
          }
        }
        delete servs;
        return;
      }
      if (inp[0] == '-') //server(s) ended
      {
        inp++;
//        ::printf("Ended Server: %s\n",inp);
        dyn_string *servs = Strsplit(inp,"|");
        for (size_t i=0;i<servs->size();i++)
        {
          if (servs->at(i).find("_MEPrx") != string::npos)
          {
            dyn_string *meprx=Strsplit(servs->at(i).c_str(),"_");
//            ::printf("Removing %s from node set\n",meprx->at(1).c_str());
            m_MepHosts.erase(meprx->at(1));
            NodeEnt *n=m_ServiceMap[meprx->at(1)];
            m_ServiceMap.erase(meprx->at(1));
            delete n->m_throttle;
            delete n;
            delete meprx;
          }
        }
        delete servs;
        return;
      }
      dyn_string *servs = Strsplit(inp,"|");
      for (size_t i=0;i<servs->size();i++)
      {
        if (servs->at(i).find("_MEPrx") != string::npos)
        {
          dyn_string *meprx=Strsplit(servs->at(i).c_str(),"_");
//          ::printf("Adding %s to node set\n",meprx->at(1).c_str());
          m_MepHosts.insert(meprx->at(1));
          string sname = servs->at(i).substr(0,servs->at(i).find("@"));
          sname = sname +"/OverflowStatus";
          Throttle *t = new Throttle(sname,meprx->at(1));
          m_ServiceMap[meprx->at(1)] = new NodeEnt(meprx->at(1),t);
          delete meprx;
        }
      }
      delete servs;
      return;
    }
};
//class DefHltcommand : public DimCommand
//{
//public:
//  DefHltcommand(const char *name, char *format):  DimCommand(name, format)
//  {
//  }
//  virtual void commandHandler() override
//  {
//    char * input = this->getString();
//    ::printf("%s: Got Input %s\n",RTL::processName().c_str(),input);
//    dyn_string *MEPlist;
//    MEPlist = Strsplit(input,"|");
//
//    size_t i;
//    for (i=0;i<MEPlist->size();i++)
//    {
//      if (MEPlist->at(i).size() == 0) break;
//      dyn_string *MEP = Strsplit(MEPlist->at(i).c_str()," ");
//      int val;
//      if (MEP->size()>1)
//      {
//        ::sscanf(MEP->at(1).c_str(),"%d",&val);
//        DimClient::sendCommandNB(MEP->at(0).c_str(),val);
//      }
//      delete (MEP);
//    }
//    delete (MEPlist);
//  }
//};
int main(int , char **)
{
  std::string myHost = RTL::str_lower(RTL::nodeNameShort());
  DimClient::setDnsNode(myHost.c_str());
  DimServer::setDnsNode("ecs03");
  DimServer::autoStartOn();
  DimServer::start((myHost+"_HLTThrottleCollect").c_str());
  m_outService = new DimService((myHost+"/Throttle").c_str(),"C",(void*)s_empty,0);
//  DefHltcommand m_DefHltcommand((myHost+"_HLTDefBridge/EnDisCommand").c_str(),(char*)"C");
  DNSServers *m_Servers= new DNSServers();
  m_Servers->getName();
  while (1)
  {
    sleep (60);
  }
  return 0;
}
