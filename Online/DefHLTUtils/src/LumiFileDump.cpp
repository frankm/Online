/*
 * LumiAnalysis.cpp
 *
 *  Created on: Mar 28, 2017
 *      Author: beat
 */
#include <map>
#include <vector>
#include <list>
#include <ctime>
#include <string>
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"
#include <set>
#include "errno.h"
#include <dirent.h> // directory header

#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
//#include <filesystem>
typedef unsigned long        CounterType[4];
using namespace std;
//using namespace std::experimental::filesystem;
using namespace boost::filesystem;
namespace
{
  template<typename T> class RunListItem
  {
    public:
      int m_runno;
      T m_count;
      time_t m_LastCnt;
      RunListItem(int runno, T &Count, time_t LastCnt)
      {
        ::memset(this, 0, sizeof(*this));
        m_runno = runno;
        m_count = Count;
        m_LastCnt = LastCnt;
      }
      RunListItem(int runno)
      {
        ::memset(this, 0, sizeof(*this));
        m_runno = runno;
        m_LastCnt = 0;
      }
  };

  typedef RunListItem<CounterType> LumiRunItem;
  template<typename T> class RunMap: public map<unsigned int, void*>
  {
    public:
      FILE *m_f;
      string m_fn;
      int m_Fdesc;
      void *m_RunPtr;
      size_t m_secsiz;
      RunListItem<T> *m_lastPtr;
      RunListItem<T> *m_CurrpTR;
      RunMap()
      {
        m_RunPtr = 0;
        m_lastPtr = 0;
        m_CurrpTR = 0;
        m_Fdesc = 0;
        size_t page_size = (size_t) sysconf (_SC_PAGESIZE);
        m_secsiz = (10000*sizeof(RunListItem<T>)+page_size)& ~page_size;
      }
      void SetFile(string fn)
      {
        using namespace boost::filesystem;
        m_fn = fn;
        string dyr = path(m_fn).parent_path().string();
        try
        {
          create_directories(dyr);
        } catch (...)
        {
          ::printf( "Cannot Create Directory %s\n",
              dyr.c_str());
        }
      }
      void OpenBackingStore()
      {
        size_t page_size = (size_t) sysconf (_SC_PAGESIZE);
        m_secsiz = (10000*sizeof(RunListItem<T>)+page_size)& ~page_size;
        m_Fdesc = open(m_fn.c_str(),O_RDWR+O_CREAT+O_EXCL,S_IRWXU+S_IRWXG+S_IRWXO);
        if (m_Fdesc >=0)  // File didn't exist, was created..
        {
          close(m_Fdesc);
          int status = truncate(m_fn.c_str(),m_secsiz);
          if (status != 0)
          {
            int ierr = errno;
            ::printf( "Cannot extend file %s Errno %d\n",m_fn.c_str(),ierr);
          }
          m_Fdesc = open(m_fn.c_str(),O_RDWR+O_CREAT,S_IRWXU+S_IRWXG+S_IRWXO);
          if (m_Fdesc < 0)
          {
            int ierr = errno;
            ::printf( "Cannot Open file %s Errno %d\n",m_fn.c_str(),ierr);
          }
          m_RunPtr = mmap(0,m_secsiz,PROT_READ+PROT_WRITE,MAP_SHARED,m_Fdesc,0);
          if (m_RunPtr==MAP_FAILED)
          {
            int ierr = errno;
            ::printf( "Cannot map file %s Errno %d\n",m_fn.c_str(),ierr);
          }
          memset(m_RunPtr,0,m_secsiz);
          this->clear();
          m_lastPtr = (RunListItem<T> *)m_RunPtr;
        }
        else
        {
          this->clear();
          m_Fdesc = open(m_fn.c_str(),O_RDWR+O_CREAT,S_IRWXU+S_IRWXG+S_IRWXO);
          if (m_Fdesc <0)
          {
            ::printf( "Cannot Open file %s\n",m_fn.c_str());
            m_RunPtr = 0;
          }
          else
          {
            m_RunPtr = mmap(0,m_secsiz,PROT_READ+PROT_WRITE,MAP_SHARED,m_Fdesc,0);
            RunListItem<T> *rptr = (RunListItem<T> *)m_RunPtr;
            m_lastPtr = rptr;
            while (rptr->m_runno >0)
            {
              insert(make_pair(rptr->m_runno,rptr));
              rptr++;
              m_lastPtr = rptr;
            }
          }
        }
      }
      void closeBackingStore()
      {
        munmap(0,m_secsiz);
        close(m_Fdesc);
      }
    RunListItem<T> *NewEntry(int rn)
    {
      if (m_Fdesc == 0 && m_lastPtr == 0)
      {
        m_lastPtr = (RunListItem<T> *)malloc(m_secsiz);
      }
      RunListItem<T> *p;
      p = m_lastPtr;
      m_lastPtr = p+1;
      memset(p,0,sizeof(*p));
      p->m_runno = rn;
      return p;
    }
    void Sync()
    {
      msync((void*)m_RunPtr,m_secsiz,MS_ASYNC);
    }
  };
}


int main(int argc, char *argv[])
{
  vector<string> ffils;
  vector<string> sfils;
  RunMap<CounterType> farmSumm_RunMap;
  RunMap<CounterType> storeSumm_RunMap;
  set<unsigned int> PhysRuns;
  string dyr;
  string fnam;
  if (argc >1)
  {
    fnam = argv[1];
  }
  else
  {
   return 0;
  }
  RunMap<CounterType> *fRMap = new RunMap<CounterType>();
  fRMap->SetFile(fnam);
  fRMap->OpenBackingStore();
  for (auto j=fRMap->begin();j != fRMap->end();j++)
  {
    CounterType *cnts;
    cnts=(CounterType*)j->second;
    printf("Run %d Counters: %ld %ld %ld %ld %ld\n",j->first,*cnts[0],*cnts[1],*cnts[2],*cnts[3],*cnts[4]);
  }
  return 0;
}
