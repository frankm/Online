#include "HLTFileEqualizer.h"
#include <math.h>
#include <time.h>
//#include "ROMon/Utilities.h"
#include "RTL/strdef.h"

static FILE *outf;
//static float Spread = 0.25 * 0.01; //allowed spread in %
using namespace std;
extern "C"
{
  void toLowerCase(std::string &s)
  {
    for (unsigned int i = 0; i < s.size(); i++)
    {
      s[i] = tolower(s[i]);
    }
  }
}
template <typename T> string ROUND(T &val, int dig)
{
  T tmp = val;

  double mult = 1.0;
  switch (dig)
  {
    case 0:
      break;
    case 1:
    {
      mult = 10.0;
      break;
    }
    case 2:
    {
      mult = 100.0;
      break;
    }
    case 3:
    {
      mult = 1000.0;
      break;
    }
    case 4:
    {
      mult = 10000.0;
      break;
    }
    case 5:
    {
      mult = 100000.0;
      break;
    }
    default:
    {
      mult = pow(double(10.0),double(dig));
      break;
    }
  }
  tmp = int(mult*tmp+0.5)/mult;
  string ret=to_string(tmp);
  ret = ret.substr(0,ret.find(".")+dig);
  return ret;
}
dyn_string *Strsplit(char *s, char *del)
{
  char *pch;
  pch = strtok(s, del);
  dyn_string *v = new dyn_string();
  int nel = 0;
  while (pch != 0)
  {
    v->push_back(std::string(pch));
    if (pch != s)
      *(pch - 1) = *del;
    nel++;
    pch = strtok(0, del);
  }
  return v;
}
dyn_string *Strsplit(const char *s, const char *del)
{
  return Strsplit((char*) s, (char*) del);
}
dyn_string *Strsplit(const char *s, char *del)
{
  return Strsplit((char*) s, del);
}
dyn_string *Strsplit(char *s, const char *del)
{
  return Strsplit((char*) s, (char*) del);
}

HLTFileEqualizer::HLTFileEqualizer()
{
  m_disMap.clear();
//  static int NoLink=-1;
  char sf[128];
//  char row;
  m_nnodes = 0;
  m_nfiles = 0;
  m_nfiles2 = 0;
  m_low = 10;
  m_high = 20;
  m_holdofftime = 0;
  m_enabledFarm.clear();
  m_thrDelay=1500;
  m_Freeze = 0;
  this->m_TargetSpread=0.0010;
  fprintf(outf, "%s\n", sf);
  this->m_RMSDecrement=0.1;
  m_averageUsage_prev = 0;
  m_averageUsage = 0;
  m_ThrottleMap.clear();
  m_ThrottleMap[CLASS_SLOW] = new THRCLSS(CLASS_SLOW,&this->m_SlowNodeSet);
  m_ThrottleMap[CLASS_MEDIUM] = new THRCLSS(CLASS_MEDIUM,&this->m_MediumNodeSet);
  m_ThrottleMap[CLASS_FAST] = new THRCLSS(CLASS_FAST,&this->m_FastNodeSet);
  m_ThrottleMap[CLASS_FASTER] = new THRCLSS(CLASS_FASTER,&this->m_FasterNodeSet);
  m_ThrottleMap[CLASS_GLOBAL] = new THRCLSS(CLASS_GLOBAL,&this->m_AllNodes);
  m_currentClass = 0;
  m_SafeClass = new THRCLSS(CLASS_SAFE,0);
  m_TotLimit = 0;
//  m_statusService = new DimService(const char *name, char *format, void *structure, int size)
  m_configService = new DimService("HLTFileEqualizer/Config", "C", (void *)"\0", 1);
  m_IncrementParams = new DimService("HLTFileEqualizer/IncParams", "C", (void *)"\0", 1);
  mSetService = new DimService("HLTFileEqualizer/CurrSet","C",(void*)"\0",1);
  m_AveragesService = new DimService("HLTFileEqualizer/Averages","C",(void*)"\0",1);
  m_Current_Settings = new DimService("HLTFileEqualizer/CurrentSettings","C",(void*)"\0",1);
  m_ThrottleStatistics = new DimService("HLTFileEqualizer/ThrottleStatistics","C",(void*)"\0",1);
  m_EnStat = new DimService("HLTFileEqualizer/EnableStatus",m_Externals.m_inhibit_act);
  m_Acting = new DimService("HLTFileEqualizer/Acting",m_Externals.m_act);
  m_VeloState = new DimService("HLTFileEqualizer/VeloState",m_Externals.m_VeloPosition);
  memset((void*)&m_Externals,0,sizeof(m_Externals));
  m_ExternalsSvc = new DimService("HLTFileEqualizer/Externals","I",&m_Externals,sizeof(m_Externals));
//  m_enabledFarm.insert(std::string("hltb01"));
//  m_enabledFarm.insert(std::string("hltb02"));
  m_lastgoodThrLimit = 0;
  m_Externals.m_freeze = false;
  m_increments.push_back(make_pair(10000,100000));
  m_increments.push_back(make_pair(8000,100000));
  m_increments.push_back(make_pair(4500,50000));
  m_increments.push_back(make_pair(3000,25000));

}
namespace
{
//  inline void toLowerCase(std::string &s)
//  {
//    for (unsigned int i=0;i<s.size();i++)
//    {
//      s[i] = tolower(s[i]);
//    }
//  }
  inline void toUpperCase(std::string &s)
  {
    for (unsigned int i = 0; i < s.size(); i++)
    {
      s[i] = toupper(s[i]);
    }
  }
}
char* Ctim()
{
  time_t t = time(0);
  char *Ctim = ctime(&t);
  *strchr(Ctim,'\n')=0;
  return Ctim;

}
void FarmDtime::infoHandler()
{
  FDTime *dt =(FDTime*)getData();
  if (dt->m_credits ==0)
  {
    fprintf(outf,"%s: Credits == 0. Not really possible. Ignoring\n",Ctim());
    return;
  }
  if (dt->m_credits == 0xffff)
  {
    fprintf(outf,"%s: Credits == -1. TFC not configured. Ignoring\n",Ctim());
    return;
  }
  if (time(0) <m_equalizer->m_holdofftime)
  {
    m_equalizer->m_lastgoodThrLimit = m_equalizer->m_Freeze;
    m_equalizer->m_FDtime.m_FDtime=dt->m_FDtime;
    m_equalizer->m_FDtime.m_credits=dt->m_credits;
    m_equalizer->m_FDtime.m_Limit=dt->m_Limit;
    m_nlow = 0;
    return;
  }
  m_equalizer->m_Externals.m_Holdoff=false;
  if ((dt->m_credits > 3500) && m_equalizer->m_Externals.m_act)
  {
    if (m_equalizer->m_TotLimit < m_equalizer->m_currentClass->m_totthrlimit)
    {
      m_equalizer->m_lastgoodThrLimit = m_equalizer->m_currentClass->m_totthrlimit*0.9;
    }
    else
    {
      m_equalizer->m_lastgoodThrLimit = m_equalizer->m_TotLimit;
    }
    m_equalizer->m_FDtime.m_FDtime=dt->m_FDtime;
    m_equalizer->m_FDtime.m_credits=dt->m_credits;
    m_equalizer->m_FDtime.m_Limit=dt->m_Limit;
    m_nlow = 0;
  }
  if (dt->m_credits<4*dt->m_Limit)
  {
    fprintf(outf,"%s: Danger of dead Time generation. Analyzing and Acting...\n",Ctim());
    m_nlow ++;
    if (m_nlow >0)
    {
      m_equalizer->m_FDtime.m_FDtime=dt->m_FDtime;
      m_equalizer->m_FDtime.m_credits=dt->m_credits;
      m_equalizer->m_FDtime.m_Limit=dt->m_Limit;
      m_equalizer->Analyze();
      m_equalizer->Act();
    }
    else
    {
      fprintf(outf,"%s: Ignoring for the time being...\n",Ctim());
    }
  }
  else
  {
    m_equalizer->m_FDtime.m_FDtime=dt->m_FDtime;
    m_equalizer->m_FDtime.m_credits=dt->m_credits;
    m_equalizer->m_FDtime.m_Limit=dt->m_Limit;
    m_nlow=0;
  }
  return;
}
void LostMEPs::infoHandler()
{
  m_equalizer->m_LostMEP = getFloat();
  if (m_equalizer->m_LostMEP >1.0)
  {
    fprintf(outf,"Lost MEP rate %f. Feezing throttling\n",m_equalizer->m_LostMEP);
    m_equalizer->Freeze();
    m_myfreeze = true;
  }
  else
  {
    if (m_myfreeze)
    {
      fprintf(outf,"Lost MEP rate %f. Thawing throttle\n",m_equalizer->m_LostMEP);
      m_equalizer->Thaw(30);
      m_myfreeze = false;
    }
  }
  m_equalizer->m_Externals.m_FreezeMEPs = m_myfreeze;
}
void L0TriggerRate::infoHandler()
{
  m_equalizer->m_L0Rate = getFloat();
}
void L0DeadTime::infoHandler()
{
  m_equalizer->m_L0DeadTime = getFloat();
  if (m_equalizer->m_L0DeadTime >15.0)
  {
    fprintf(outf,"L0 dead time %f. Feezing throttling\n",m_equalizer->m_L0DeadTime);
    m_equalizer->Freeze();
    m_myfreeze = true;
  }
  else
  {
    if (m_myfreeze)
    {
      fprintf(outf,"L0 dead time %f. Thawing throttling\n",m_equalizer->m_L0DeadTime);
      m_equalizer->Thaw(10);
      m_myfreeze = false;
    }
  }
  m_equalizer->m_Externals.m_FreezeDeadt = m_myfreeze;
}
void VeloPosInfo::infoHandler()
{
  m_VeloPos = getInt();
  m_equalizer->m_Externals.m_VeloPosition = m_VeloPos;
  m_equalizer->setActing();
  m_equalizer->m_ExternalsSvc->updateService();
}
//#define NumRms 2.5
void HLTFileEqualizer::Analyze()
{
//  float NumRms=2.5;
  dim_lock();
//  if (m_RunStatus != 1)
//  {
//    fprintf(outf,"%s: LHCb Not Running. Ignoring...\n",Ctim());
//    dim_unlock();
//    return;
//  }
  m_Actions.clear();
  map<string, myNode> Nodes;
  dyn_string *nlist = Strsplit(m_StorageValue.c_str(), "|");
  double av_usage = 0.0;
//  double sum_usage=0.0;
  double tot_space = 0.0;
  double used_space = 0.0;
  float avail_sp, free_sp;
  int nnodes = 0;
  set<string> AboveNodes;
  set<string> BelowNodes;
  AboveNodes.clear();
  BelowNodes.clear();
  for (size_t i = 0; i < nlist->size(); i++)
  {
    dyn_string *nodent = Strsplit(nlist->at(i).c_str(), " ");
    std::string nam = nodent->at(0);
    std::string subfarm = nam.substr(0, 6);
    auto nit = m_NodeSet.find(nam);
    if (nit == m_NodeSet.end())
    {
      delete nodent;
      continue;
    }
    nit = this->m_exclNodes.find(nam);
    if (nit != m_exclNodes.end())
    {
      delete nodent;
      continue;
    }
    nit = m_LHCbFarms.find(subfarm);
    if (nit == m_LHCbFarms.end())
    {
      delete nodent;
      continue;
    }
//    delete nlist;
    dyn_string *storquants = Strsplit(nodent->at(1).c_str(), "/");
    avail_sp = std::stof(storquants->at(0));
    tot_space += avail_sp;
    free_sp = std::stof(storquants->at(1));
    used_space += (avail_sp - free_sp);
    delete storquants;
    myNode nod(nam);
    nod.m_DskCapacity = avail_sp;
    nod.m_DskUsed = avail_sp - free_sp;
    nod.m_DiskUsage = nod.DiskUsage();
    nnodes++;
    Nodes[nam] = nod;
    delete nodent;
  }
  delete nlist;
  fprintf(outf, "%s: Farm Dead Time Status: DT %f, credits %d, limit %d\n",Ctim(),
      m_FDtime.m_FDtime, m_FDtime.m_credits, m_FDtime.m_Limit);
  if (m_FDtime.m_credits <4*m_FDtime.m_Limit)
  {
    int credsover = m_FDtime.m_credits-m_FDtime.m_Limit;
    float diff;
    diff = 1.0-float(credsover)/3/m_FDtime.m_Limit;
    diff *= 0.5;
    diff = diff*m_TotLimit;
//    if(m_FDtime.m_credits-m_FDtime.m_Limit >0)
//    {
//      float dif=(1000+m_FDtime.m_FDtime*5000.0+3000/(float(m_FDtime.m_credits-m_FDtime.m_Limit)/m_FDtime.m_Limit));
//      fprintf(outf,"Reducing throttle by %f from %d\n",dif,m_TotLimit);
//      m_TotLimit -=dif;
//    }
//    else
//    {
//      float dif=(1000+m_FDtime.m_FDtime*5000.0);
      fprintf(outf,"Reducing throttle by %f from %d\n",diff,m_TotLimit);
      m_TotLimit -=diff;
//    }
      m_lastgoodThrLimit = 0.9*m_lastgoodThrLimit;
      m_TotLimit = m_lastgoodThrLimit;
      fprintf(outf,"Setting throttle Limit to last known good %d\n",m_TotLimit);
    if (m_TotLimit <0)m_TotLimit=0;
//    m_RMSDecrement/=2.0;
  }
  else if (m_FDtime.m_credits >10000)
  {
    m_TotLimit = this->m_lastgoodThrLimit;
  }
  else
  {
    if (!m_Externals.m_inhibit_act && (this->m_Externals.m_deferState == 1))
    {
    }
  }
  av_usage = used_space / tot_space;
  double sum2=0.0;
  for (auto &i : Nodes)
  {
    sum2 += (i.second.m_DiskUsage*i.second.m_DiskUsage);
  }
  double rms;
  float av_slow=0.0,av_medium=0.0,av_fast=0.0,av_faster=0.0;
  int n_slow=0,n_medium=0,n_fast=0,n_faster=0;
  for (auto &i : Nodes)
  {
    if (m_SlowNodeSet.contains( i.second.m_name))
    {
      av_slow+=i.second.m_DiskUsage;
      n_slow++;
    }
    else if (this->m_MediumNodeSet.contains( i.second.m_name))
    {
      av_medium+=i.second.m_DiskUsage;
      n_medium++;
    }
    else if (this->m_FastNodeSet.contains( i.second.m_name))
    {
      av_fast+=i.second.m_DiskUsage;
      n_fast++;
    }
    else if (this->m_FasterNodeSet.contains( i.second.m_name))
    {
      av_faster+=i.second.m_DiskUsage;
      n_faster++;
    }
    if (i.second.m_DiskUsage > av_usage)
    {
      AboveNodes.insert(i.second.m_name);
    }
    else if (i.second.m_DiskUsage <= av_usage )
    {
      BelowNodes.insert(i.second.m_name);
    }
  }

  av_slow /= n_slow;
  av_medium /= n_medium;
  av_fast /= n_fast;
  av_faster /= n_faster;
  m_ThrottleMap[CLASS_SLOW]->m_average_prev =  m_ThrottleMap[CLASS_SLOW]->m_average;
  m_ThrottleMap[CLASS_MEDIUM]->m_average_prev =  m_ThrottleMap[CLASS_MEDIUM]->m_average;
  m_ThrottleMap[CLASS_FAST]->m_average_prev =  m_ThrottleMap[CLASS_FAST]->m_average;
  m_ThrottleMap[CLASS_FASTER]->m_average_prev =  m_ThrottleMap[CLASS_FASTER]->m_average;
  m_ThrottleMap[CLASS_GLOBAL]->m_average_prev =  m_ThrottleMap[CLASS_GLOBAL]->m_average;
  m_ThrottleMap[CLASS_SLOW]->m_average = av_slow;
  m_ThrottleMap[CLASS_MEDIUM]->m_average = av_medium;
  m_ThrottleMap[CLASS_FAST]->m_average = av_fast;
  m_ThrottleMap[CLASS_FASTER]->m_average = av_faster;
  m_ThrottleMap[CLASS_GLOBAL]->m_average = av_usage;
  for (auto &i : m_ThrottleMap)
  {
    i.second->Clear();
  }
  THRCLSS *oldclass= m_currentClass;
  THRCLSS *maxclass;
  float mx = av_slow;
  {
    maxclass = m_ThrottleMap[CLASS_SLOW];
    if (av_medium > mx)
    {
      mx = av_medium;
      maxclass = m_ThrottleMap[CLASS_MEDIUM];
    }
    if (av_fast > mx)
    {
      mx = av_fast;
      maxclass = m_ThrottleMap[CLASS_FAST];
    }
    if (av_faster > mx)
    {
      mx = av_faster;
      maxclass = m_ThrottleMap[CLASS_FASTER];
    }
  }
  maxclass = m_ThrottleMap[CLASS_GLOBAL];
  if (m_lastgoodThrLimit == 0)
  {
    m_lastgoodThrLimit = maxclass->m_totthrottle;
  }
  if (oldclass!=0)
  {
//    if (oldclass->m_average < mx)
    if (oldclass->m_average < av_usage)
    {
      m_currentClass = maxclass;
    }
  }
  else
  {
    m_currentClass = maxclass;
  }
  if (oldclass != m_currentClass)
  {
    fprintf(outf,"Change of throttle class... \n");
    m_TotLimit = m_currentClass->m_totthrlimit;
    this->m_thrDelay = m_currentClass->m_NodethrLimit;
    m_RMSDecrement=0.1;
  }
  this->m_thrDelay = m_currentClass->m_NodethrLimit;
  fprintf(outf,"Current Throttle Class type %d\n",m_currentClass->m_type);
  m_averageUsage_prev = m_averageUsage;
  m_averageUsage = av_usage;
  if (m_Externals.m_inhibit_act || (m_Externals.m_deferState != 1) ||(m_Externals.m_RunStatus != 1) || (m_Externals.m_VeloPosition == 0))
  {
    fprintf(outf, "not acting...\n");
    fflush(outf);
    updateServices();
    dim_unlock();
    return;
  }
  if (time(0) <this->m_holdofftime)
  {
    fprintf(outf,"%s: Holdoff period... Ignoring...\n",Ctim());
    fflush(outf);
    m_totThrottle = 0;
    updateServices();
    dim_unlock();
    return;
  }
  m_Externals.m_Holdoff=false;

  double rms1=sum2/Nodes.size()-av_usage*av_usage;
  if (rms1 < 0 ) rms1=-rms1;
  rms1 = ::sqrt(rms1);
  rms = rms1>0.001?rms1:0.001;
  m_TargetSpread = rms;
  fprintf(outf,"%s: Average usage %f RMS %f (%f) Target Spread %f\n",Ctim(),av_usage,rms,rms1,m_TargetSpread);
  NodeSet ThrNodes;
  size_t Ndis = m_disMap.size();
  for (auto &it : BelowNodes)
  {
    myNode nod = Nodes[it];
    auto nit = m_disMap.find(nod.m_name);
    if (nit != m_disMap.end())
    {
      m_Actions[nod.m_subfarm].push_back(make_pair(nod.m_name, 0));
      Ndis--;
    }
  }
  bool frozen = m_Externals.m_freeze;
  if (m_Externals.m_freeze)
  {
    m_lastgoodThrLimit = m_Freeze;
    m_TotLimit= 0;
    updateServices();
    dim_unlock();
    return;
  }
  if (m_thawing)
  {
   m_thawing = false;
   m_TotLimit = m_Freeze;
  }
  else
  {
    if (!frozen)
    {
      for (size_t iii=0;iii<m_increments.size();iii++)
      {
        if (m_FDtime.m_credits > m_increments[iii].first)
        {
          m_TotLimit += m_increments[iii].second;
          break;
        }
      }
    }
//    if (m_FDtime.m_credits >10000)
//    {
//      m_TotLimit +=100000;
//    }
//    else if (m_FDtime.m_credits >8000)
//    {
//      if (!frozen)
//      {
//        m_TotLimit +=100000;
//      }
//    }
//    else if (m_FDtime.m_credits > 4000)
//    {
//      if (!frozen)
//      {
//        m_TotLimit +=75000;
//      }
//    }
//    else if (m_FDtime.m_credits>3000)
//    {
//      if (!frozen)
//      {
//        m_TotLimit +=10000;
//      }
//    }
  }
//  m_TotLimit = m_TotLimit>m_currentClass->m_totthrlimit?m_currentClass->m_totthrlimit:m_TotLimit;
  int totThrottle = 0;
//  int CLoss=0;
  float scale = 1.0;
  m_currentClass->m_minThr = 0;
  m_currentClass->m_maxThr = 0;
  for (auto jj=m_ThrottleMap.begin();jj!=m_ThrottleMap.end();jj++)
  {
    jj->second->m_Nnodes = 0;
    jj->second->m_totthrottle = 0;
  }
  for (auto &it : AboveNodes)
  {
    string Nnam=it;
    myNode nod = Nodes[it];
//    double dev = (nod.m_DiskUsage-av_usage)/this->m_TargetSpread;
//    int del=m_thrDelay*dev+100;
    int del = (m_thrDelay)*(1.0+100*(nod.m_DiskUsage-av_usage));///this->m_TargetSpread);

    nod.m_thrDelay= del;//<m_thrDelay?del:m_thrDelay;
    if (!m_currentClass->m_nodeSet->contains(Nnam))
    {
        continue;
    }
    float fact=1.0;
    THRCLSS *cls=0;
    if (m_ThrottleMap[CLASS_SLOW]->m_nodeSet->contains(nod.m_name))
    {
      cls = m_ThrottleMap[CLASS_SLOW];
    }
    else if (m_ThrottleMap[CLASS_MEDIUM]->m_nodeSet->contains(nod.m_name))
    {
      cls = m_ThrottleMap[CLASS_MEDIUM];
    }
    else if (m_ThrottleMap[CLASS_FAST]->m_nodeSet->contains(nod.m_name))
    {
      cls = m_ThrottleMap[CLASS_FAST];
    }
    else if (m_ThrottleMap[CLASS_FASTER]->m_nodeSet->contains(nod.m_name))
    {
      cls = m_ThrottleMap[CLASS_FASTER];
    }
    fact = cls->m_ThrFactor;
    if (fact > 0.0)
    {
      nod.m_thrDelay /= fact;
      totThrottle+= nod.m_thrDelay;
    }
    else
    {
      nod.m_thrDelay = 0;
    }
    Nodes[it] = nod;
  }
//  if (totThrottle >m_TotLimit)
  {
    scale = float(m_TotLimit)/float(totThrottle);
  }
  fprintf (outf,"%s: Scale Factor %f %d / %d\n",Ctim(),scale,m_TotLimit, totThrottle);
  totThrottle=0;
  long totspac=0;
  long usedspac=0;
  for (auto &it : AboveNodes)
  {
    string Nnam=it;
    myNode nod = Nodes[it];
    totspac += nod.m_DskCapacity;
    usedspac += nod.m_DskUsed;
//    double dev = (nod.m_DiskUsage-av_usage)/this->m_TargetSpread;
//    int del=scale*m_thrDelay*dev;
    int del = (m_thrDelay)*(1.0+100*(nod.m_DiskUsage-av_usage));///this->m_TargetSpread);
    del=scale*del;
    nod.m_thrDelay= scale*nod.m_thrDelay;
    nod.m_thrDelay= nod.m_thrDelay<m_thrDelay?nod.m_thrDelay:m_thrDelay;
    if (!m_currentClass->m_nodeSet->contains(Nnam))
    {
      nod.m_thrDelay = 0;
    }
//    float fact=1.0;
    THRCLSS *cls=0;
    if (m_ThrottleMap[CLASS_SLOW]->m_nodeSet->contains(nod.m_name))
    {
      cls = m_ThrottleMap[CLASS_SLOW];
    }
    else if (m_ThrottleMap[CLASS_MEDIUM]->m_nodeSet->contains(nod.m_name))
    {
      cls = m_ThrottleMap[CLASS_MEDIUM];
    }
    else if (m_ThrottleMap[CLASS_FAST]->m_nodeSet->contains(nod.m_name))
    {
      cls = m_ThrottleMap[CLASS_FAST];
    }
    else if (m_ThrottleMap[CLASS_FASTER]->m_nodeSet->contains(nod.m_name))
    {
      cls = m_ThrottleMap[CLASS_FASTER];
    }
//    fact = cls->m_ThrFactor;
//    if (fact <= 0.0)
//    {
//      nod.m_thrDelay = 0;
//    }
//    else
//    {
//      nod.m_thrDelay /= fact;
//    }
    cls->m_Nnodes++;
    cls->m_totthrottle += nod.m_thrDelay;
    cls->m_minThr = nod.m_thrDelay<cls->m_minThr?nod.m_thrDelay:cls->m_minThr;
    cls->m_maxThr = nod.m_thrDelay>cls->m_maxThr?nod.m_thrDelay:cls->m_maxThr;
    m_ThrottleMap[CLASS_GLOBAL]->m_Nnodes++;
    m_ThrottleMap[CLASS_GLOBAL]->m_totthrottle+=nod.m_thrDelay;
    auto nit = m_disMap.find(nod.m_name);
    if (nit == m_disMap.end())
    {
      m_Actions[nod.m_subfarm].push_back(make_pair(nod.m_name, nod.m_thrDelay));
      Ndis++;
    }
    else
    {
      m_Actions[nod.m_subfarm].push_back(make_pair(nod.m_name, nod.m_thrDelay));
    }
    Nodes[it] = nod;
    totThrottle+= nod.m_thrDelay;
    m_currentClass->m_minThr = nod.m_thrDelay<m_currentClass->m_minThr?nod.m_thrDelay:m_currentClass->m_minThr;
    m_currentClass->m_maxThr = nod.m_thrDelay>m_currentClass->m_maxThr?nod.m_thrDelay:m_currentClass->m_maxThr;
  }
  m_ThrottleMap[CLASS_GLOBAL]->m_average = float(usedspac)/float(totspac);
  m_totThrottle = totThrottle;
  fprintf (outf,"%s: Average Disk usage Slow nodes %f\n",Ctim(),av_slow);
  fprintf (outf,"%s: Average Disk usage Medium nodes %f\n",Ctim(),av_medium);
  fprintf (outf,"%s: Average Disk usage Fast nodes %f\n",Ctim(),av_fast);
  fprintf (outf,"%s: Average Disk usage Faster nodes %f\n",Ctim(),av_faster);
  fprintf(outf, "%s: Number of nodes below average (%f): %ld\n", Ctim(),av_usage,
      BelowNodes.size());
  fprintf(outf, "%s: Number of nodes above average (%f): %ld\n", Ctim(),av_usage,
      AboveNodes.size());
  fprintf(outf, "%s: Number of throttled nodes %ld Total Throttle Delay %d (per Node: %f)\n", Ctim(),
      m_currentClass->m_nodeSet->size(),totThrottle,float(totThrottle)/m_currentClass->m_nodeSet->size());
  m_ThrottleMap[CLASS_SLOW]->fPrint(outf);
  m_ThrottleMap[CLASS_MEDIUM]->fPrint(outf);
  m_ThrottleMap[CLASS_FAST]->fPrint(outf);
  m_ThrottleMap[CLASS_FASTER]->fPrint(outf);
  m_ThrottleMap[CLASS_GLOBAL]->fPrint(outf);
  fprintf(outf,"Last good Limit %d\n",this->m_lastgoodThrLimit);
//  string tnods;
//  for (auto &it : *m_currentClass->m_nodeSet)
//  {
//    tnods += it+" "+std::to_string(Nodes[it].m_thrDelay)+"|";
//  }
//  fprintf(outf,"%s: Throttled Nodes:\n%s\n",Ctim(),tnods.c_str());
  fprintf(outf, "==================\n");
  fflush(outf);
  updateServices();
  dim_unlock();
}
void HLTFileEqualizer::updateServices()
{
  std::string val;
  val = "TotalThrottleLimit "+std::to_string(m_currentClass->m_totthrlimit)+"|NodeThrottleLimit "+std::to_string(m_currentClass->m_NodethrLimit)+"\0";
  m_configService->updateService((void*)val.c_str(),val.size()+1);
  val = m_currentClass->m_Name+"\0";
  mSetService->updateService((void*)val.c_str(),val.size()+1);
  val.clear();
  float delta;
  float a;
  for (auto &i : m_ThrottleMap)
  {
    delta = i.second->m_average-i.second->m_average_prev;
    delta *= 100.0;
//    val += std::to_string(100.0*i.second->m_average)+"     "+to_string(delta)+"|";
    val += ROUND(a =100.0*i.second->m_average,5)+"       "+ROUND(delta,5)+"|";
  }
  delta = m_averageUsage-m_averageUsage_prev;
  delta *= 100.0;
//  val += std::to_string(100.0*m_averageUsage)+" "+to_string(delta)+"|";//+"\0";
//  val += std::to_string(m_totThrottle) +"|";//"\0";
//  val += std::to_string(float(m_totThrottle)/m_currentClass->m_nodeSet->size())+"|" ;
//  val += std::to_string(m_currentClass->m_minThr)+"|"+std::to_string(m_currentClass->m_maxThr)+"\0";
  val += ROUND(a=100.0*m_averageUsage,5)+"       "+ROUND(delta,5)+"|";//+"\0";
  val += to_string(m_totThrottle) +"|";//"\0";
  val += ROUND(a=float(m_totThrottle)/m_currentClass->m_nodeSet->size(),2)+"|" ;
  val += to_string(m_currentClass->m_minThr)+"|"+std::to_string(m_currentClass->m_maxThr)+"\0";
  m_AveragesService->updateService((void*)val.c_str(),val.size()+1);
  val.clear();
  for (auto &i : m_ThrottleMap)
  {
    val += i.second->m_Name+"/totthrlimit "+to_string(i.second->m_totthrlimit)+
        "/NodethrLimit "+to_string(i.second->m_NodethrLimit)+"/ThrottleFactor "+to_string(i.second->m_ThrFactor)+"|";
  }
  val +="\0";
  m_Current_Settings->updateService((void*)val.c_str(),val.size()+1);
  val.clear();
  val ="";
  for (auto jj=m_ThrottleMap.begin();jj!=m_ThrottleMap.end();jj++)
  {
    val += jj->second->m_Name+"/";
    val += to_string(jj->second->m_Nnodes)+" ";
    val += to_string(jj->second->m_totthrottle)+" ";
    val += to_string(jj->second->m_minThr)+" ";
    val += to_string(jj->second->m_maxThr)+"|";
  }
  val += "\0";
  m_ThrottleStatistics->updateService((void*)val.c_str(),val.size()+1);
  val.clear();
  for (size_t jj=0;jj<m_increments.size();jj++)
  {
    val = val+to_string(m_increments[jj].first)+" "+to_string(m_increments[jj].second)+"|";
  }
  val += "\0";
  m_IncrementParams->updateService((void*)val.c_str(),val.size()+1);
  m_Acting->updateService();
  m_VeloState->updateService();
  m_ExternalsSvc->updateService();
}
void HLTFileEqualizer::setActing()
{
  m_Externals.m_act = (m_Externals.m_RunStatus == 1) && !m_Externals.m_inhibit_act && (m_Externals.m_deferState == 1) && (m_Externals.m_VeloPosition == 1)?1:0;
}
void HLTFileEqualizer::Act()
{
  if (time(0) <this->m_holdofftime)
  {
    fprintf(outf,"%s: Holdoff period... Ignoring...\n",Ctim());
    return;
  }
  m_Externals.m_Holdoff=false;
  dim_lock();
  setActing();
//  m_Externals.m_act = (m_Externals.m_RunStatus == 1) && !m_Externals.m_inhibit_act && (m_Externals.m_deferState == 1) && (m_Externals.m_VeloPosition == 1)?1:0;
  if (m_Externals.m_RunStatus != 1)
  {
    fprintf(outf,"%s: LHCb Not Running. Ignoring...\n",Ctim());
    dim_unlock();
    return;
  }
  if (!m_Externals.m_inhibit_act && (this->m_Externals.m_deferState == 1) && (m_Externals.m_VeloPosition == 1))
  {
    for (auto fit = m_Actions.begin(); fit != m_Actions.end(); fit++)
    {
      if (!m_enabledFarm.empty()
          && (m_enabledFarm.find((*fit).first) == m_enabledFarm.end()))
      {
        continue;
      }
      //    fprintf(outf,"On Farm %s:\n",(*fit).first.c_str());
      std::list<std::pair<std::string, int> >::iterator i;
      std::string sf_mesg = "";
      std::string endisSvc;
      endisSvc = (*fit).first + "_HLTDefBridge/EnDisCommand";
      for (i = (*fit).second.begin(); i != (*fit).second.end(); i++)
      {
        std::string svcname;
        std::string node = (*i).first;
        toUpperCase(node);
        svcname = "LHCb_" + node + "_MEPrx_0/setOverflow";
        char cmd[1024];
        sprintf(cmd, "%s %d|", svcname.c_str(), (*i).second);
        sf_mesg.append(cmd);

      }
//      fprintf(outf, "%s: Sending command %s to bridge on farm %s\n", Ctim(),
//          sf_mesg.c_str(), endisSvc.c_str());
      DimClient::sendCommandNB(endisSvc.c_str(), (char*) sf_mesg.c_str());
    }
  }
  else
  {
    fprintf(outf, "Action inhibited or deferal off. Not acting...\n");
  }
  dim_unlock();
}
void HLTFileEqualizer::BufferDump()
{
}
void HLTFileEqualizer::Dump()
{
}
void HLTFileEqualizer::ableAll(int StateValue)
{
  myActionMap Actions;
  for (auto nit = m_AllNodes.begin(); nit != m_AllNodes.end(); nit++)
  {
    Actions[nit->substr(0,6)].push_back(std::make_pair(*nit,StateValue));
  }
  for (auto fit = Actions.begin(); fit != Actions.end(); fit++)
  {
    std::list<std::pair<std::string, int> >::iterator i;
    std::string sf_mesg = "";
    std::string endisSvc;
    endisSvc = (*fit).first + "_HLTDefBridge/EnDisCommand";
    for (i = (*fit).second.begin(); i != (*fit).second.end(); i++)
    {
      std::string svcname;
      std::string node = (*i).first;
      toUpperCase(node);
      svcname = "LHCb_" + node + "_MEPrx_0/setOverflow";
      //      DimClient::sendCommand(svcname.c_str(), (*i).second);
      //      sprintf(cmd,"dim_send_command.exe %s %d -dns %s -s -i&",svcname.c_str(),(*i).second,(*fit).first.c_str());
      char cmd[1024];
      sprintf(cmd, "%s %d|", svcname.c_str(), StateValue);
      sf_mesg.append(cmd);

      //      ::system(cmd);
      //      fprintf(outf,"\tMEPRX on Node %s (%s) value %d\n",(*i).first.c_str(),svcname.c_str(),(*i).second);
    }
    sf_mesg.append("\0");
    DimClient::sendCommandNB(endisSvc.c_str(), (void*) (sf_mesg.c_str()),
        sf_mesg.size()+1);
//    fprintf(outf,"message to Subfarm %s:\n%s\n",(*fit).first.c_str(),sf_mesg.c_str());
  }
}

namespace
{
  class EnActCommand: public DimCommand
  {
    public:
      HLTFileEqualizer *m_equl;
      EnActCommand(HLTFileEqualizer *e,const char *name, char * format) :
          DimCommand(name, format)
      {
        m_equl=e;
        return;
      }
      ;
      virtual void commandHandler() override
      {
        int command = getInt();
        m_equl->m_Externals.m_inhibit_act = !command;
        m_equl->m_lastgoodThrLimit =100000;
        m_equl->setActing();
        m_equl->m_ExternalsSvc->updateService();
        m_equl->m_EnStat->updateService();
      }
      ;
  };
  class ExitCommand: public DimCommand
  {
    public:
      NodeSet *m_nodemap;
      HLTFileEqualizer *m_equl;
      ExitCommand(const char *name, char *format, NodeSet *nodm,
          HLTFileEqualizer *elz) :
          DimCommand(name, format)
      {
        m_nodemap = nodm;
        m_equl = elz;
      }
//      void ableAll(int StateValue)
//      {
//        myActionMap Actions;
//        for (auto nit = m_equl->m_AllNodes.begin(); nit != m_equl->m_AllNodes.end(); nit++)
//        {
//          Actions[nit->substr(0,6)].push_back(std::make_pair(*nit,StateValue));
//        }
//        for (auto fit = Actions.begin(); fit != Actions.end(); fit++)
//        {
//          std::list<std::pair<std::string, int> >::iterator i;
//          std::string sf_mesg = "";
//          std::string endisSvc;
//          endisSvc = (*fit).first + "_HLTDefBridge/EnDisCommand";
//          for (i = (*fit).second.begin(); i != (*fit).second.end(); i++)
//          {
//            std::string svcname;
//            std::string node = (*i).first;
//            toUpperCase(node);
//            svcname = "LHCb_" + node + "_MEPrx_0/setOverflow";
//            //      DimClient::sendCommand(svcname.c_str(), (*i).second);
//            //      sprintf(cmd,"dim_send_command.exe %s %d -dns %s -s -i&",svcname.c_str(),(*i).second,(*fit).first.c_str());
//            char cmd[1024];
//            sprintf(cmd, "%s %d|", svcname.c_str(), StateValue);
//            sf_mesg.append(cmd);
//
//            //      ::system(cmd);
//            //      fprintf(outf,"\tMEPRX on Node %s (%s) value %d\n",(*i).first.c_str(),svcname.c_str(),(*i).second);
//          }
//          DimClient::sendCommandNB(endisSvc.c_str(), (void*) (sf_mesg.c_str()),
//              sf_mesg.size());
//          fprintf(outf,"message to Subfarm %s:\n%s\n",(*fit).first.c_str(),sf_mesg.c_str());
//        }
//      }
//
//      void enableAll()
//      {
//        ableAll(0);
//      }
//      void disableAll()
//      {
//        ableAll(m_equl->m_thrDelay);
//      }
      virtual void commandHandler() override
      {
        int command = getInt();
        switch (command)
        {
          case 1:
          {
            m_equl->enableAll();
            ::sleep(5);
            ::exit(0);
            break;
          }
          case 0:
          {
            ::exit(0);
          }
        }
      }
  };
  class ReleaseThrottleCommand : public ExitCommand
  {
    public:
      ReleaseThrottleCommand(const char *name, char *format, NodeSet *nodm,
          HLTFileEqualizer *elz) :ExitCommand(name,format,nodm,elz)
      {

      }
      virtual void commandHandler() override
      {
        getInt();
//        int command = getInt();
        m_equl->m_totThrottle = 0;
        m_equl->m_Externals.m_inhibit_act = true;
        m_equl->setActing();
        m_equl->m_EnStat->updateService();
        m_equl->enableAll();
      }

  };
}
HLTFileEqualizer *m_equalizer;
int m_nolink;
int m_state;
LHCbThrottleInfo::LHCbThrottleInfo(char *name, HLTFileEqualizer *e) :
    DimInfo(name, (char*) "")
{
  m_equalizer = e;
  string m_input;
}
void LHCbThrottleInfo::infoHandler()
{
  char *input = this->getString();
  dyn_string *inp = Strsplit(input, "|");
  for (size_t i = 0; i < inp->size(); i++)
  {
    dyn_string *ent = Strsplit(inp->at(i).c_str(), " ");
    string nod = ent->at(0);
    int val = std::stoi(ent->at(1));
    if (val == 0)
    {
      auto it = m_equalizer->m_disMap.find(nod);
      if (it != m_equalizer->m_disMap.end())
      {
        m_equalizer->m_disMap.erase(it);
        delete it->second;
      }
    }
    else
    {
      auto it = m_equalizer->m_disMap.find(nod);
      if (it == m_equalizer->m_disMap.end())
      {
        time_t t = time(&t);
        DisNode *e = new DisNode(nod, t);
        m_equalizer->m_disMap[nod] = e;
      }
    }
    delete ent;
  }
  delete inp;
}

LHCb1RunStatus::LHCb1RunStatus(char *name, int nolink, HLTFileEqualizer *e) :
    DimInfo(name, nolink)
{
  m_nolink = nolink;
  m_equalizer = e;
  m_state = 0;
  m_prevstate =0;
}
void HLTFileEqualizer::Freeze()
{
  m_Freeze  = m_totThrottle;
  enableAll();
  m_totThrottle = 0;
  m_Externals.m_freeze = true;
}
void HLTFileEqualizer::Thaw(int holdoff)
{
  m_holdofftime = time(0)+holdoff;
  m_totThrottle = m_Freeze;
  m_TotLimit = m_Freeze;
  m_Externals.m_freeze = false;
  m_thawing = true;
  m_Externals.m_Holdoff = true;
}
void LHCb1RunStatus::infoHandler()
{
#define READY 1
  int data;
  data = getInt();
  m_prevstate = m_state;
  m_state = data;
  if ((m_prevstate ==0) && (m_state == 1))
  {
    fprintf(outf,"Running State %d. thawing throttling\n",m_state);
    m_equalizer->Thaw(40);
//    m_equalizer->m_holdofftime = time(0)+40;
//    m_equalizer->m_totThrottle =m_equalizer->m_Freeze;
  }
  if (m_state == 0)
  {
    fprintf(outf,"Running State %d. Feezing throttling\n",m_state);
    m_equalizer->Freeze();
//    m_equalizer->m_Freeze =m_equalizer->m_totThrottle;
//    m_equalizer->enableAll();
//    m_equalizer->m_totThrottle = 0;
  }
  m_equalizer->m_Externals.m_RunStatus = data;
  m_equalizer->setActing();
  m_equalizer->m_ExternalsSvc->updateService();
//  m_equalizer->updateServices();
}

ExclInfo::ExclInfo(char *name, NodeSet *nodeset) :
    DimInfo(name, (char*) "\0")
{
  m_exclNodes = nodeset;
}
FarmStorageStatus::FarmStorageStatus(char *name, char *nolink,
    HLTFileEqualizer *e) :
    DimInfo(name, nolink)
{
  m_equalizer = e;
  m_state = 0;
  m_nolink = nolink;
}
void FarmStorageStatus::infoHandler()
{
  m_equalizer->m_StorageValue = (char*) this->getData();
}

void ExclInfo::infoHandler()
{
  char *input;
  input = getString();
  int ilen = this->getSize();
  char *pos;
  char *epos = input + ilen-1;
  pos = input;
  while (pos < epos)
  {
    pos = strchr(input, '\0');
    if (pos == 0)
      break;
    *pos = '|';
  }
  *epos='\0';
  fprintf(outf, "New Exclude Nodes List: %s\n", input);
  dyn_string *nlist = Strsplit(input, "|");

  m_exclNodes->clear();
  for (size_t i = 0; i < nlist->size(); i++)
  {
    if (nlist->at(i).size() > 0)
    {
      string nam = nlist->at(i);
      nlist->at(i) = RTL::str_lower(nlist->at(i));
      m_exclNodes->insert(nlist->at(i));
    }
  }
  delete nlist;
}
void LHCbFarms::infoHandler()
{
  char *input;
  input = getString();
  int ilen = this->getSize();
  char *pos;
  char *epos = input + ilen-1;
  pos = input;
  while (pos < epos)
  {
    pos = strchr(input, '\0');
    if (pos == 0)
      break;
    *pos = '|';
  }
  *epos='\0';
  fprintf(outf, "New LHCb Farm List: %s\n", input);
  dyn_string *nlist = Strsplit(input, "|");

  m_equalizer->m_LHCbFarms.clear();
  for (size_t i = 0; i < nlist->size(); i++)
  {
    if (nlist->at(i).size() > 0)
    {
      string nam = nlist->at(i);
      nlist->at(i) = RTL::str_lower(nlist->at(i));
      m_equalizer->m_LHCbFarms.insert(nlist->at(i));
    }
  }
  delete nlist;
}
void SettingsCommand::commandHandler()
{
  char *s = getString();
  dyn_string *ds= Strsplit(s,"/");
  string nset = ds->at(0);
  THRCLSS *cl=0;
  fprintf (outf,"Received settings command %s\n",s);
  for (auto &it : m_equalizer->m_ThrottleMap)
  {
    if (it.second->m_Name == nset)
    {
      cl = it.second;
      break;
    }
  }
  if (cl == 0)
  {
    fprintf (outf,"Unknown throttle class %s",nset.c_str());
    return;
  }
  for (size_t i=1;i<ds->size();i++)
  {
        string set = ds->at(i);
        dyn_string *itms = Strsplit(set.c_str()," ");
        if (itms->at(0) == "totthrlimit")
        {
          cl->m_totthrlimit = stoi(itms->at(1));
        }
        else if (itms->at(0) == "NodethrLimit")
        {
          cl->m_NodethrLimit = stoi(itms->at(1));
        }
        else if (itms->at(0) == "NodethrLimit")
        {
          cl->m_NodethrLimit = stoi(itms->at(1));
        }
        else if (itms->at(0) == "ThrottleFactor")
        {
          cl->m_ThrFactor = stof(itms->at(1));
        }
        delete itms;
  }
  fflush(outf);
  delete ds;
//  m_equalizer->m_TotLimit=m_equalizer->m_currentClass->m_totthrlimit;
//  m_equalizer->m_thrDelay = m_equalizer->m_currentClass->m_NodethrLimit;
}
void IncParamComm::commandHandler()
{
  int sts = this->getSize();
  char *s = getString();
  s[sts]=0;
  dyn_string *ds=Strsplit(s,"|");
  m_equalizer->m_increments.clear();
  for (size_t i = 0;i<ds->size();i++)
  {
    string itm=ds->at(i);
    size_t idx;
    int ilim = std::stoi(itm,&idx);
    int iinc = std::stoi(itm.substr(idx));
    m_equalizer->m_increments.push_back(make_pair(ilim,iinc));
  }
  delete ds;
}
void DoCommand::commandHandler()
{
//  int what = getInt();
  this->m_equalizer->Analyze();
  m_equalizer->Act();
}
int main(int argc, char **argv)
{
  char *ofile = getenv("HLTEQ_LOGF");
  if (ofile == 0)
  {
    outf = fopen("/group/online/HLTFileEqualizer.log", "a+");
  }
  else
  {
    outf = fopen(ofile, "a+");

  }
  fprintf(outf, "HLTFileEqualizer starting at...");
  {
    time_t rawtime;
    time(&rawtime);
    fprintf(outf, "%s", asctime(localtime(&rawtime)));
  }
  string AllNodes = string(getenv("HLT_AllNodes"));
  string FastNodes = string(getenv("HLT_FastNodes"));
  string FasterNodes = string(getenv("HLT_FasterNodes"));
  string SlowNodes = string(getenv("HLT_SlowNodes"));
  string MediumNodes = string(getenv("HLT_MediumNodes"));
  toLowerCase(AllNodes);
  toLowerCase(FastNodes);
  toLowerCase(FasterNodes);
  toLowerCase(SlowNodes);
  toLowerCase(MediumNodes);

  DimClient::setDnsNode("ecs03");
  char *dns = getenv("HLTEQ_DNSNODE");
  if (dns == 0)
  {
    DimServer::setDnsNode("ecs03");
  }
  else
  {
    DimServer::setDnsNode(dns);
  }
  HLTFileEqualizer elz;
//  nodemap::iterator nit;
  char *disact = getenv("HLTEQ_INHIBIT_ACT");
  elz.m_Externals.m_inhibit_act = (disact != 0);
  dyn_string *ds = Strsplit(AllNodes.c_str(), " ");
  for (size_t i = 0; i < ds->size(); i++)
  {
    elz.m_NodeSet.insert(ds->at(i));
  }
  delete ds;
  ds = Strsplit(SlowNodes.c_str(), " ");
  for (size_t i = 0; i < ds->size(); i++)
  {
    elz.m_SlowNodeSet.insert(ds->at(i));
  }
  delete ds;
  ds = Strsplit(MediumNodes.c_str(), " ");
  for (size_t i = 0; i < ds->size(); i++)
  {
    elz.m_MediumNodeSet.insert(ds->at(i));
//    elz.m_SlowNodeSet.insert(ds->at(i));
  }
  delete ds;
  ds = Strsplit(FastNodes.c_str(), " ");
  for (size_t i = 0; i < ds->size(); i++)
  {
    elz.m_FastNodeSet.insert(ds->at(i));
  }
  delete ds;
  ds = Strsplit(FasterNodes.c_str(), " ");
  for (size_t i = 0; i < ds->size(); i++)
  {
    elz.m_FasterNodeSet.insert(ds->at(i));
  }
  delete ds;
  elz.setActing();

  elz.m_AllNodes = elz.m_NodeSet;
//  int m_DefState = -1;
  DimServer::start("HLTFileEqualizer");
  DimServer::autoStartOn();
  int low, high;
  if (argc > 1)
  {
    sscanf(argv[1], "%d", &low);
    elz.m_low = low;
  }
  if (argc > 2)
  {
    sscanf(argv[2], "%d", &high);
    elz.m_high = high;
  }
  int slp=30;
//  fprintf(outf, "Low/high options %d %d\n", elz.m_low, elz.m_high);
  ExclInfo exclInfo((char*) "HLT/ExcludedNodes", &elz.m_exclNodes);
  DeferState DeferState((char*)"RunInfo/LHCb/DeferHLT", 0, &elz);
  ExitCommand EnableandExit("HLTFileEqualizer/Exit", (char*) "I", &elz.m_AllNodes,
      &elz);
  ReleaseThrottleCommand ReleaseThrottleCommand("HLTFileEqualizer/ReleaseThrottles", (char*) "I", &elz.m_AllNodes,
      &elz);
  EnActCommand EnActCommand(&elz,"HLTFileEqualizer/EnableActions", (char*) "I");
  LHCb1RunStatus LHCb1runstatus((char*) "RunInfo/LHCb/Running", 0, &elz);
  /*FarmStorageStatus *m_FarmStorageStatus = */new FarmStorageStatus(
      (char*) "FarmStatus/StorageStatus", (char*) "", &elz);
  /*LHCbThrottleInfo *thr_Info = */new LHCbThrottleInfo(
      (char*) "LHCb_MepthrottleCnt", &elz);
  FarmDtime FDTime((char*) "RunInfo/LHCb/FarmDeadTime", &elz);
  LHCbFarms lhcbfarms((char*) "RunInfo/LHCb/HLTsubFarms", &elz);
  new IntCommand("HLTFileEqualizer/Delay (ms)",&elz.m_thrDelay);
  new FloatCommand("HLTFileEqualizer/TargetSpread",&elz.m_TargetSpread);
  new FloatCommand("HLTFileEqualizer/RMSDecrement",&elz.m_RMSDecrement);
  new IntCommand("HLTFileEqualizer/SleepIntv",&slp);
  new IntCommand("HLTFileEqualizer/MaxTotThrottle",&elz.m_TotLimit);
  new DoCommand("HLTFileEqualizer/DoIt",&elz);
  new SettingsCommand((char*)"HLTFileEqualizer/Settings",&elz);
  new VeloPosInfo((char*)"LHCbStatus/VELO",&elz);
  new L0TriggerRate((char*)"LHCb/L0TriggerRate",0.0,&elz);
  new L0DeadTime((char*)"LHCb/L0DeadTime",0.0,&elz);
  new LostMEPs((char*)"LHCb/LostMEP",0.0,&elz);
  new IncParamComm((char*)"HLTFileEqualizer/SetIncParams",&elz);
  fflush(outf);
  sleep(30);
  elz.Analyze();
  while (1)
  {
    sleep(slp);
    elz.Analyze();
    elz.Act();
    elz.m_lastgoodThrLimit = elz.m_TotLimit;
    fflush(outf);
  }
  return 0;
}

