/*
 * FileStatistics.h
 *
 *  Created on: Jun 8, 2012
 *      Author: Beat Jost
 */

#ifndef FILESTATISTICS_H_
#define FILESTATISTICS_H_
#include <map>
#include <string>
#include <list>
#include <set>
#include "dim/dic.hxx"
#include "dim/dis.hxx"
#define MBM_IMPLEMENTATION
#include "ROMon/ROMon.h"
#include "ROMon/CPUMon.h"
#include "stdio.h"
#include "time.h"
#include "HLTPerfFormatter.h"
using namespace ROMon;
using namespace std;
typedef std::map<int,int> RunMap;
class MBMStat
{
  public:
    std::string name{"undefined"};
    int produced = 0;
    int seen = 0;
    int evtot = 0;
    int sptot = 0;
    int evused = 0;
    int spused = 0;
    float p_rate = 0.0;
    float s_rate = 0.0;
    float spocc = 0.0;
    float evocc = 0.0;
    MBMStat() = default;
    MBMStat(const MBMStat & c) = default;
    MBMStat& operator=(const MBMStat & c) = default;
    void calcRate(MBMStat &p,long dtime)
    {
      double delta = dtime;
      if (produced >= p.produced)
      {
        p_rate = 1000.0*float(produced-p.produced)/delta;
      }
      else
      {
        p_rate = 0.0;
      }
      if (seen >=p.seen)
      {
        s_rate = 1000.0*float(seen-p.seen)/delta;
      }
      else
      {
        s_rate = 0.0;
      }
      evocc = 0.0;
      if (evtot >0)
      {
        evocc = float(evused)/float(evtot);
      }
      spocc = 0.0;
      if (sptot >0)
      {
        evocc = float(spused)/float(sptot);
      }
    }
};
class PartMBMs
{
public:
  MBMStat Events;
  MBMStat Overflow;
  MBMStat Send;
  MBMStat ProcPerf;
  MBMStat Events_prev;
  MBMStat Overflow_prev;
  MBMStat Send_prev;
  MBMStat ProcPerf_prev;
  PartMBMs() = default;
  PartMBMs(const PartMBMs& copy) = default;
  PartMBMs& operator=(const PartMBMs& copy) = default;
};

struct NodeQuantities
{
  int m_nofiles = 0;
  int m_state = 0;
  char m_ROC_state = 0;
  RunMap m_runmap{};
  bool m_excl{false};
  long ReadTime = 0;
  long ReadTime_prev = 0;
  float m_nodePerformance = 0.0;
  bool m_active{false};
  float m_cfiles = 0.0;
};

class myNode
{
  public:
    std::string m_name;
    std::string m_subfarm;
    NodeQuantities m_DefQ;
    NodeQuantities m_HLT1Q;
    PartMBMs AnyPart;
    PartMBMs LHCb2;
//    MBMStat Events;
//    MBMStat Overflow;
//    MBMStat Send;
//    MBMStat ProcPerf;
//    MBMStat Events_prev;
//    MBMStat Overflow_prev;
//    MBMStat Send_prev;
//    MBMStat ProcPerf_prev;
    myNode(std::string n)
    {
      m_name = n;
      m_DefQ.m_state = 1;
      m_DefQ.m_nofiles = 0;
      m_subfarm = m_name.substr(0,6);
      m_DefQ.m_ROC_state = '?';
      m_DefQ.m_runmap.clear();
      m_DefQ.m_excl = false;
      m_DefQ.ReadTime_prev = 0;
      m_DefQ.ReadTime = 0;
      m_DefQ.m_nodePerformance = 0.0;
      m_DefQ.m_active = false;
      m_DefQ.m_cfiles = 0.0;
      m_HLT1Q.m_state = 1;
      m_HLT1Q.m_nofiles = 0;
      m_HLT1Q.m_ROC_state = '?';
      m_HLT1Q.m_runmap.clear();
      m_HLT1Q.m_excl = false;
      m_HLT1Q.ReadTime_prev = 0;
      m_HLT1Q.ReadTime = 0;
      m_HLT1Q.m_nodePerformance = 0.0;
      m_HLT1Q.m_active = false;
      m_HLT1Q.m_cfiles = 0.0;
    };
    myNode() = default;
    myNode(const myNode& copy) = default;
    myNode& operator=(const myNode& copy) = default;
};
class SFarm
{
  public:
    std::string m_svcnam;

};
class DefHltInfoHandler;
class HLT1InfoHandler;
class MBMInfoHandler;
typedef std::map<std::string,myNode*> myNodeMap;
typedef std::map<std::string,std::list<std::pair<std::string,int> > > myActionMap; //list of nodes per subfarm to execute an action on.
typedef std::set<std::string> NodeSet;
typedef std::map<std::string,float> NodePerfMap;
class FileStatistics
{
  public:
    std::map<std::string,SFarm *> m_Farms;
    myNodeMap m_Nodes;
    myNodeMap m_AllNodes;
    nodemap M_PMap;
    std::map<std::string,DimUpdatedInfo*> m_infoMap;
    DefHltInfoHandler *m_DefHandler;
    HLT1InfoHandler *m_HLT1Handler;
    MBMInfoHandler *m_MBMInfoHandler;
    int m_nnodes;
    int m_nfiles;
    long m_nfiles2;
    int m_low;
    int m_high;
    DimInfo *m_DefStateInfo;
    DimService *m_DefNodeList;
    DimService *m_DefNodeListDiff;
    DimService *m_DefNodesRunsFiles;
    DimService *m_DefStatServ;
    DimService *m_NodesBuffersEvents;
    DimService *m_NodesBuffersEvents_LHCb2;
    DimService *m_NodesBuffersOcc;
    DimService *m_NodesBuffersOcc_LHCb2;
    DimService *m_HLT1NodeList;
    DimService *m_HLT1NodeListDiff;
    DimService *m_HLT1NodesRunsFiles;
    std::string m_Defservdat;
    std::string m_DefservdatDiff;
    std::string m_DefservdatNodesRunsFiles;
    std::string m_HLT1servdat;
    std::string m_HLT1servdatDiff;
    std::string m_HLT1servdatNodesRunsFiles;
    std::string m_DefservdatNodesBuffersEvents;
    std::string m_DefservdatNodesBuffersEvents_LHCb2;
    std::string m_DefservdatNodesBuffersOcc;
    std::string m_DefservdatNodesBuffersOcc_LHCb2;
    NodeSet m_enabledFarm;
    NodeSet m_recvNodes;
    NodeSet m_BufferrecvNodes;
    NodeSet m_exclNodes;
    std::set<std::string> m_AllpFarms;
    std::set<std::string> m_AllpNodes;
    NodePerfMap m_nodePerf;
    FileStatistics();
    void Analyze();
    void Dump();
    void BufferDump();
    void FillMBMBufferRates(char *,std::string &,PartMBMs &);
    void FillMBMBufferOccs(char *,std::string &,PartMBMs &);
};
typedef ROMon::DeferredHLTSubfarmStats _DHLTSF;
typedef ROMon::Nodeset _MBMSF;

class DefHltInfoHandler : public DimInfoHandler
{
  public:
    SFarm *m_subfarm;
    _DHLTSF *m_sfstatus;
    int m_bufsiz;
    FileStatistics *m_Equalizer;
    DefHltInfoHandler(FileStatistics *e);
    void infoHandler() override;
};

class HLT1InfoHandler : public DimInfoHandler
{
  public:
    SFarm *m_subfarm;
    _DHLTSF *m_sfstatus;
    int m_bufsiz;
    FileStatistics *m_Equalizer;
    HLT1InfoHandler(FileStatistics *e);
    void infoHandler() override;
};

class LHCb1RunStatus : public DimInfo
{
  public:
    FileStatistics *m_equalizer;
    int m_nolink;
    int m_state;
    LHCb1RunStatus(char *name, int nolink,FileStatistics *e);
    void infoHandler() override;
};

class ExclInfo : public DimInfo
{
  public:
    NodeSet *m_exclNodes;
    ExclInfo(char *name, NodeSet *nodeset);
    void infoHandler() override;
};

class MBMInfoHandler : public DimInfoHandler
{
  public:
//    SFarm *m_subfarm;
    _MBMSF *m_sfstatus;
    int m_bufsiz;
    FileStatistics *m_Equalizer;
    MBMInfoHandler(FileStatistics *e);
    void infoHandler() override;
};


#endif /* FILESTATISTICS_H_ */
