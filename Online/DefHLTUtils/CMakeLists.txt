#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/DefHLTUtils
------------------
#]=======================================================================]

find_package(TBB REQUIRED)

online_library(DefUtil src/HLTPerfFormatter.cpp)
target_link_libraries(DefUtil
 PUBLIC     Online::OnlineBase
            Online::ROMon
            Online::dim
            ROOT::Core
            ROOT::Gpad
            ROOT::Hist
            ROOT::Rint
	    TBB::tbb
)
if(CMAKE_CXX_COMPILER_ID MATCHES "^(Apple)?Clang$")
  target_compile_options(DefUtil PRIVATE -Wno-address-of-packed-member)
endif()
# ---------------------------------------------------------------------------------------
macro(add_exe name)
  online_executable(${name} src/${name}.cpp)
  target_compile_options(${name} PRIVATE -Wno-pedantic -Wno-shadow)
  if(CMAKE_CXX_COMPILER_ID MATCHES "^(Apple)?Clang$")
    target_compile_options(${name} PRIVATE -Wno-address-of-packed-member)
  endif()
  target_link_libraries(${name}  PRIVATE Online::DefUtil Boost::filesystem)
endmacro()

foreach(app IN ITEMS DiracMon FarmStatus FileStatistics HLTFileEqualizer HLTDefBridge DefHltPlot FileDeleter LumiAnalysis LumiFileDump HLTThrottleCollect)
  add_exe(${app})
endforeach()
target_link_libraries(DefHltPlot PRIVATE Online::GauchoBase)
