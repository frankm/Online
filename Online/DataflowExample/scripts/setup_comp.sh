#
#
#export UPGRADE=/home/rkrawczy/CERN_20/MARKUS_FLAVIO_MBM_INT/Online;
#export INSTALLATION=/home/rkrawczy/CERN_20/MARKUS_FLAVIO_MBM_INT;

COMP=clang8;
COMP=gcc8;
if test -n "${1}"; then
  COMP="${1}";
  echo "Compiling for: $COMP";
fi;
#
export CMTDEB=x86_64-centos7-${COMP}-do0;
export CMTOPT=x86_64-centos7-${COMP}-opt;
export CMTCONFIG=$CMTOPT;
export BINARY_TAG=$CMTOPT;
export ALL_CMT_PLATFORMS="${CMTDEB} ${CMTOPT}";
. /cvmfs/lhcb.cern.ch/lib/LbEnv.sh  -c ${CMTOPT}
#
export CMTPROJECTPATH=${INSTALLATION}:/cvmfs/lhcb.cern.ch/lib/lcg/releases:/cvmfs/lhcb.cern.ch/lib/lcg/app/releases:/cvmfs/lhcb.cern.ch/lib/lcg/external
export CMAKE_MODULE_PATH=${CMTPROJECTPATH};
export CMAKE_PREFIX_PATH=${INSTALLATION}:${CMAKE_PREFIX_PATH};
#
#
mmbmmon()   {
    xterm -ls -132 -geometry 132x65 -title ${HOST}/MBMMon \
	-e "export UTGID=${HOST}_MBMMon_\$$;\
            . ${INSTALLATION}/Online/setup.${BINARY_TAG}.vars; \
            exec -a ${HOST}/MBMMon ${INSTALLATION}/Online/InstallArea/${BINARY_TAG}/bin/gentest.exe libOnlineKernel.so mbm_mon_scr" &
}
