#!/bin/bash
#
#
. ./setup.x86_64-centos7-gcc9-do0.vars;
#
export INFO_OPTIONS=${DATAFLOWEXAMPLEROOT}/options/OnlineEnv.opts;
export MBM_SETUP_OPTIONS=${DATAFLOWEXAMPLEROOT}/options/MBMSetup.opts;
export DATAFLOW_task="gentest libDataflow.so dataflow_run_task -msg=Dataflow_OutputLogger -mon=Dataflow_DIMMonitoring";
#
xterm -ls -132 -geometry 132x28 -hold -e "UTGID=MEPInit_0 ${DATAFLOW_task} -class=Class0 -opts=${DATAFLOWEXAMPLEROOT}/options/MEPInit.opts -auto" &
/usr/bin/sleep 4;
xterm -ls -132 -geometry 165x45 -hold -e "UTGID=mbmmon_$$ gentest libOnlineBase.so mbm_mon" &
/usr/bin/sleep 4;
xterm -ls -132 -geometry 132x28 -hold -e "UTGID=Prod_0 ${DATAFLOW_task} -class=Class2 -opts=${DATAFLOWEXAMPLEROOT}/options/Prod.opts -auto" &
