#include "Options/sys.h"
#include <cstdlib>

namespace {
  std::string i_resolve(std::string& source, int recursions)  {
    if ( recursions > 0 )  {
      int lvl = 0, mx_lvl = 0;
      for(const char* c=source.c_str(), *beg=nullptr; *c != 0; ++c)  {
        switch ( *c )  {
	case '$':
	  if ( *(c+1) == '{' )  {
	    lvl++;
	    if ( lvl > mx_lvl )  {
	      mx_lvl = lvl;
	      beg = c;
	    }
	    c += 2;
	  }
	  break;
	case '}':
	  if ( lvl == mx_lvl )  {
	    std::string env(beg+2,c-beg-2);
	    std::string rep;
	    if ( ! System::getEnv(env.c_str(), rep) )
	      rep = i_resolve(env, --recursions);
	    if ( rep.length() )  {
	      std::string e(beg,c-beg+1);
	      size_t idx=std::string::npos;
	      while((idx=source.find(e)) != std::string::npos)  {
		source.replace(idx, e.length(), rep);
	      }
	      return i_resolve(source, --recursions);
	    }
	    else  {
	      // error: environment cannot be resolved....
	      // Try to continue, but there is not too much hope.
	    }
	  }
	  lvl--;
	  break;
	default:
	  break;
        }
      }
    }
    return source;
  }
}

/// get a particular env var, storing the value in the passed string (if set)
bool System::getEnv(const char* var, std::string &value) {
  char* env;
  if  ( (env = getenv(var)) != nullptr ) {
    value = env;
    return true;
  } else {
    return false;
  }
}

int System::resolveEnv(const std::string& var, std::string& res, int recursions)  {
  std::string source = var;
  res = i_resolve(source, recursions);
  return (res.find("${") == std::string::npos);
}
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/filesystem.hpp>
namespace bf = boost::filesystem;
using namespace std;

#ifdef _WIN32
static const char* path_separator = ",;";
#else
static const char* path_separator = ",:";
#endif

typedef enum
  {
    LocalSearch,
    RecursiveSearch
  } SearchType;

typedef enum {
  PR_regular_file,
  PR_directory
} PR_file_type;

typedef enum {
  PR_local,
  PR_recursive
} PR_search_type;


static bool
PR_find( const bf::path& file, const string& search_list,
         PR_file_type file_type, SearchType search_type,
         string& result ) {

  bool found(false);

  // look for file as specified first

  try {
    if ( ( file_type == PR_regular_file && is_regular_file( file ) ) ||
         ( file_type == PR_directory && is_directory( file ) ) ) {
      result = bf::system_complete(file).string();
      return true;
    }
  } catch (const bf::filesystem_error& /*err*/) {
  }

  // assume that "." is always part of the search path, so check locally first

  try {
    bf::path local = bf::initial_path() / file;
    if ( ( file_type == PR_regular_file && is_regular_file( local ) ) ||
         ( file_type == PR_directory && is_directory( local ) ) ) {
      result = bf::system_complete(file).string();
      return true;
    }
  } catch (const bf::filesystem_error& /*err*/) {
  }


  // iterate through search list
  vector<string> spv;
  split(spv, search_list, boost::is_any_of( path_separator), boost::token_compress_on);
  for (const auto& itr : spv ) {

    bf::path fp = itr / file;

    try {
      if ( ( file_type == PR_regular_file && is_regular_file( fp ) ) ||
           ( file_type == PR_directory && is_directory( fp ) ) ) {
        result = bf::system_complete(fp).string();
        return true;
      }
    } catch (const bf::filesystem_error& /*err*/) {
    }


    // if recursive searching requested, drill down
    if (search_type == RecursiveSearch &&
        is_directory( bf::path(itr) ) ) {

      bf::recursive_directory_iterator end_itr;
      try {
        for ( bf::recursive_directory_iterator ritr( itr );
              ritr != end_itr; ++ritr) {

          // skip if not a directory
          if (! is_directory( bf::path(*ritr) ) ) { continue; }

          bf::path fp2 = bf::path(*ritr) / file;
          if ( ( file_type == PR_regular_file && is_regular_file( fp2 ) ) ||
               ( file_type == PR_directory && is_directory( fp2 ) ) ) {
            result = bf::system_complete( fp2 ).string();
            return true;
          }
        }
      } catch (const bf::filesystem_error& /*err*/) {
      }
    }

  }

  return found;
}

std::string 
System::PathResolver::find_file_from_list (const std::string& logical_file_name,
					   const std::string& search_list)
{
  std::string result("");
  bf::path lfn( logical_file_name );

  /* bool found = */
  PR_find (lfn, search_list, PR_regular_file, LocalSearch, result);

  // The following functionality was in the original PathResolver, but I believe
  // that it's WRONG. It extracts the filename of the requested item, and searches
  // for that if the preceding search fails. i.e., if you're looking for "B/a.txt",
  // and that fails, it will look for just "a.txt" in the search list.

  // if (! found && lfn.filename() != lfn ) {
  //   result = "";
  //   PR_find (lfn.filename(), search_list, PR_regular_file, search_type, result);
  // }

  return (result);
}
