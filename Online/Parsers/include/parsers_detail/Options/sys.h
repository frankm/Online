#include <string>

namespace System {
  /// get a particular env var, storing the value in the passed string (if set)
  bool getEnv(const char* var, std::string &value);
  inline bool getEnv(const std::string &var, std::string &value) {
    return getEnv(var.c_str(), value);
  }
  /// get a particular env var, return "UNKNOWN" if not defined
  inline std::string getEnv(const char* var)   {
    std::string val;
    if ( getEnv(var,val) ) return val;
    return "UNKNOWN";
  }

  int resolveEnv(const std::string& var, std::string& res, int recursions=124);
  namespace PathResolver {
    std::string find_file_from_list (const std::string& logical_file_name,
				     const std::string& search_list);
  }
}
