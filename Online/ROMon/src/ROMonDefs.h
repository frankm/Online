//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROMON_ROMONDEF_H
#define ROMON_ROMONDEF_H

// C++ include files
#include <iostream>
#include <string>
#include <cstdarg>

/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  std::pair<int, unsigned int> ro_gettime();
  bool ro_match_end(const std::string& pattern, const char* data);
  bool ro_match_start(const std::string& pattern, const char* data);
  void ro_move_string(char* target, const char* source, size_t buff_len);

  /// Extract node/service name from DNS info
  void getServiceNode(char* s, std::string& svc, std::string& node);

  class DimLock {
  public:
    DimLock();
    ~DimLock();
  };

  class DimReverseLock {
  public:
    DimReverseLock();
    ~DimReverseLock();
  };
  template <typename A, typename B> inline A ro_min(const A& a, const B& b) {
    return a < b ? a : b;
  }
}
#endif // ROMON_ROMONDEF_H
