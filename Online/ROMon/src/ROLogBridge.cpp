//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROMON_ROLOGBRIDGE_H
#define ROMON_ROLOGBRIDGE_H 1

// Framework include files
#include <CPP/Interactor.h>
#include <ROMon/FMCMonListener.h>

// C++ include files
#include <vector>

/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  class PartitionListener;

  namespace {

    class ClusterLogListener : public FMCMonListener, public ROUpdateHandler {
    protected:
      /// Service container type
      typedef std::map<Item*,int>  Services;
      /// Map of services to be bridged
      Services    m_services;
      /// Prefix for resulting services
      std::string m_prefix;
    public:
      /// Standard constructor with initialization
      ClusterLogListener(bool verbose, const std::string& name, const std::string& prefix);
      /// Default destructor
      virtual ~ClusterLogListener();
      /// Add service to be bridged
      void addService(const std::string& svc);
      /// Update handler
      void update(void* param) override;
      /// Feed data to DIS when updating data
      static void feed(void* tag, void** buf, int* size, int* first);
    };
  }

  /**@class ROLogBridge ROLogBridge.h GaudiOnline/ROLogBridge.h
   *
   * Readout monitor DIM server for a single node
   *
   * @author M.Frank
   */
  class ROLogBridge : public CPP::Interactor {
  protected:
    /// Cluster container type definition
    typedef std::vector<RODimListener*> Servers;
    /// Cluster container
    Servers            m_servers;
    /// Process name
    std::string        m_name;
    /// Prefix for resulting service names
    std::string        m_prefix;
    /// Reference to partition listener
    PartitionListener* m_partListener = 0;
    /// Printout level
    long               m_print;

    struct ClusterLogAdder {
      ROLogBridge* bridge;
      ClusterLogAdder(ROLogBridge* b) : bridge(b) {}
      void operator()(const std::string& n) const { bridge->addCluster(n); }
    };
  public:
    /// Standard constructor with initialization
    ROLogBridge(int argc, char** argv);
    /// Default destructor
    virtual ~ROLogBridge();
    /// Add cluster data points to bridge
    void addCluster(const std::string& name);
    /// Help printout in case of -h /? or wrong arguments
    static void help();
    /// Interactor override ....
    void handle(const CPP::Event& ev) override;
  };
}      // End namespace ROMon
#endif /* ROMON_ROLOGBRIDGE_H */

//====================================================================
//  ROMon
//--------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//====================================================================

// C++ include files
#include <iostream>
#include <stdexcept>

// Framework includes
#include <dim/dic.h>
#include <dim/dis.h>
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include <RTL/Logger.h>
#include <CPP/Event.h>
#include <CPP/IocSensor.h>
#include <ROMon/Constants.h>
#include <ROMon/PartitionListener.h>
#include "ROMonDefs.h"

#include <algorithm>
#include <memory>

using namespace ROMon;
using namespace std;
typedef FMCMonListener::Descriptor DSC;
typedef RODimListener::Clients     Clients;

/// Standard constructor with initialization
ClusterLogListener::ClusterLogListener(bool verbose, const string& sub_farm, const string& pref)
  : FMCMonListener(verbose), m_prefix(pref)
{
  string sf  = RTL::str_upper(sub_farm);
  string sf3 = sf.substr(0,3);
  m_infoTMO = 0;
  setItem("");
  setMatch("*");
  setUpdateHandler(this);
  string svc0 = "/" + sf + "/logger";
  addService(svc0+"/fmc/log");
  addService(svc0+"/gaudi/log");
  addService(svc0+"/pvss/log");
  if ( sf == "HLT01" || sf.substr(0,4) == "MONA" || sf.substr(0,5) == "STORE" )  {
    addService(svc0+"/pvssconfig/log");
  }
  if ( sf.substr(0,3) == "HLT" || sf.substr(0,6) == "MONA10" )  {
    addService(svc0+"/lhcba/log");
    addService(svc0+"/lhcb1/log");
    addService(svc0+"/lhcb2/log");
  }
  else if ( sf.substr(0,5) == "STORE" || sf.substr(0,4) == "MONA" )  {
    for(size_t i=0; i<0xF; ++i)  {
      char text[132];
      if ( sf.substr(0,5) == "STORE" )
	::snprintf(text,sizeof(text),"/Storage_Slice%02lX/log",i);
      else if ( sf.substr(0,6) == "MONA08" )
	::snprintf(text,sizeof(text),"/Monitoring_Slice%02lX/log",i);
      else if ( sf.substr(0,6) == "MONA09" )  {
	::snprintf(text,sizeof(text),"/Reconstruction_Slice%02lX/log",i);
	if ( i > 3 ) break;
      }
      else  {	// ignore....
	continue;
      }
      addService(svc0+text);      
    }
  }
  ::lib_rtl_output(LIB_RTL_DEBUG,"[ROLogBridge] Added services for subfarm:%s",sf.c_str());
}

/// Default destructor
ClusterLogListener::~ClusterLogListener()  {
  dim_lock();
  for(const auto& i : m_services )
    ::dis_remove_service(i.second);
  m_services.clear();
  dim_unlock();
}

/// Add service to be bridged
void ClusterLogListener::addService(const string& svc) {
  string svc_name = m_prefix+svc;
  addHandler("/FMC" + svc,"/FMC" + svc);
  auto i = m_clients.find("/FMC" + svc);
  if ( i != m_clients.end() )  {
    Item* it = (*i).second;
    int   id = ::dis_add_service(svc_name.c_str(),"C",0,0,feed,(long)it);
    m_services.insert(make_pair(it,id));
    ::lib_rtl_output(LIB_RTL_DEBUG,"[ROLogBridge] Added service:%s",svc_name.c_str());
  }
  else  {
    ::lib_rtl_output(LIB_RTL_DEBUG,
		     "[ROLogBridge] Service:%s ALREADY PRESENT! [Internal Error]",
		     svc_name.c_str());
  }
}

/// Update handler
void ClusterLogListener::update(void* param) {
  Item* it = (Item*)param;
  auto j = m_services.find(it);
  if ( j != m_services.end() ) {
    int id = (*j).second;
    ::lib_rtl_output(LIB_RTL_VERBOSE,"[ROLogBridge] Update Service:%d",id);
    ::dis_update_service(id);
  }
}

/// Feed data to DIS when updating data
void ClusterLogListener::feed(void* tag, void** buff, int* size, int* ) {
  static const char* data = "";
  Item* it = *(Item**)tag;
  if ( it ) {
    Descriptor* d = it->data<Descriptor>();
    if ( d  && d->data ) {
      *buff = (void*)(d->data ? d->data : data);
      *size = d->actual;
      return;
    }
  }
  *buff = (void*)data;
  *size = 0;
}

/// Standard constructor
ROLogBridge::ROLogBridge(int argc, char** argv) : m_print(LIB_RTL_WARNING)  {
  string PUBLISHING_NODE = "ECS03", from=PUBLISHING_NODE, to=PUBLISHING_NODE;
  RTL::CLI cli(argc, argv, ROLogBridge::help);
  m_name = "/"+RTL::str_upper(RTL::nodeNameShort())+"/"+RTL::processName();
  cli.getopt("publish",2, m_prefix="/Farm");
  cli.getopt("print",2,m_print);
  cli.getopt("from",2, from);
  cli.getopt("to",2, to);
  if ( cli.getopt("debug", 5) )    {
    bool wait = 1;
    while ( wait > 0 )   {
      ::lib_rtl_sleep(1000);
    }
  }
  RTL::Logger::install_log(m_print);
  ::dic_set_dns_node(from.c_str());
  ::dis_set_dns_node(to.c_str());
  m_partListener = new PartitionListener(this,"Subfarms","*",true);
  ::dis_start_serving(m_name.c_str());
}

/// Default destructor
ROLogBridge::~ROLogBridge() {
  delete m_partListener;
}

/// Add cluster data points to bridge
void ROLogBridge::addCluster(const string& sf) {
  m_servers.push_back(new ClusterLogListener(m_print<LIB_RTL_INFO, sf, m_prefix));
  ::lib_rtl_output(LIB_RTL_VERBOSE,"[ROLogBridge] Added subfarm:%s",sf.c_str());
}

/// Interactor override ....
void ROLogBridge::handle(const CPP::Event& ev) {
  typedef vector<string> StringV;
  switch(ev.eventtype) {
  case TimeEvent:
    //if (ev.timer_data == ??? ) {}
    return;
  case IocEvent:
    switch(ev.type) {
    case CMD_CONNECT: {
      std::unique_ptr<StringV> farms{(StringV*)ev.data};
      for_each(farms->begin()+1,farms->end(),ClusterLogAdder(this));
      ::dis_start_serving(m_name.c_str());
      return;
    }
    default:
      break;
    }
    break;
  default:
    break;
  }
}

/// Help printout in case of -h /? or wrong arguments
void ROLogBridge::help() {
  ::lib_rtl_output(LIB_RTL_ALWAYS,"romon_bridge -opt [-opt]\n"
                   "     -from=<string>     Node name from which the datapoints should be consumed.\n"
                   "     -to=<string>       Node to which these data points should be published.\n"
                   "     -print=<integer>   Printout value and verbosity.\n"
                   "     -publish=<string>  Prefix for published services.\n\n");
}

/// Main entry point to start the application
extern "C" int romon_loggerbridge(int argc, char** argv) {
  ROLogBridge mon(argc,argv);
  RTL::Logger::print_startup("Process information COLLECTOR");
  IocSensor::instance().run();
  return 1;
}

