//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework include files
#include "ROMon/SysInfo.h"
#include "ROMon/CPUMonOstream.h"
#include "ROMon/Sys.h"
#include "ROMonDefs.h"
#include "RTL/rtl.h"
#include "RTL/strdef.h"
#include "RTL/readdir.h"
#include "dim/dis.h"

// C/C++ include files
#include <iostream>
#include <stdexcept>
#include <cstring>
#include <cstdio>
#include <cerrno>
#include <cmath>
#include <fcntl.h>
#include <sys/stat.h>

using namespace std;
using namespace ROMon;
#define CPUINFO_SIZE  6*1024
#define PROCINFO_SIZE 300*1024

/// Default constructor
SysInfo::SysInfo(NodeStats* buff, size_t len, int nbuffs)
  : m_buffer(buff), m_buffLen(len), m_idx(1), m_readings(0), m_nBuffs(nbuffs)
{
  int j;
  //string match, n = RTL::str_upper(RTL::nodeNameShort());
  //match = "/FMC/"+n+"/task_manager";
  //::lib_rtl_output(LIB_RTL_INFO,"SysInfo: Listening to service: %s.",match.c_str());
  m_cpuInfo = new char*[m_nBuffs];
  for(j=0; j<m_nBuffs; ++j) m_cpuInfo[j] = new char[CPUINFO_SIZE];
  m_procInfo = new char*[m_nBuffs];
  for(j=0; j<m_nBuffs; ++j) m_procInfo[j] = new char[PROCINFO_SIZE];
  // Setup the object
  RTL::read(m_mem);
  statistics()->reset();
}

SysInfo::~SysInfo()  {
  int i;
  for(i=0; i<m_nBuffs; ++i) delete [] m_procInfo[i];
  for(i=0; i<m_nBuffs; ++i) delete [] m_cpuInfo[i];
  delete [] m_cpuInfo;
  delete [] m_procInfo;
}

int SysInfo::combineCPUInfo() {
  CPUset&       cr = *(statistics()->cpu());
  const CPUset& cn = *cpuNow();
  const CPUset& cl = *cpuLast();
  CPUset::Cores::iterator m = cr.cores.begin();
  CPUset::Cores::const_iterator i=cn.cores.begin(), j=cl.cores.begin();
  float d_user, d_system, d_nice, d_idle, d_iowait, d_IRQ, d_softIRQ, d_tot, d_percent;

  ::memcpy(&cr,&cn,cn.length());
  d_user    = cn.averages.user    - cl.averages.user;
  d_system  = cn.averages.system  - cl.averages.system;
  d_nice    = cn.averages.nice    - cl.averages.nice;
  d_idle    = cn.averages.idle    - cl.averages.idle;
  d_iowait  = cn.averages.iowait  - cl.averages.iowait;
  d_IRQ     = cn.averages.IRQ     - cl.averages.IRQ;
  d_softIRQ = cn.averages.softIRQ - cl.averages.softIRQ;
  d_tot     = d_user+d_system+d_nice+d_idle+d_IRQ+d_softIRQ+d_iowait;
  d_percent = (float)(d_tot/1e2);
  //cout << d_user << " " << d_system << " " << d_nice << " " << d_idle << " " << d_iowait << " " << d_IRQ << " " << d_softIRQ << " " << d_tot << " " << d_percent << endl;
  cr.averages.user     = d_user/d_percent;
  cr.averages.system   = d_system/d_percent;
  cr.averages.nice     = d_nice/d_percent;
  cr.averages.idle     = d_idle/d_percent;
  cr.averages.iowait   = d_iowait/d_percent;
  cr.averages.IRQ      = d_IRQ/d_percent;
  cr.averages.softIRQ  = d_softIRQ/d_percent;
  cr.ctxtRate          = (float)((cn.ctxtRate-cl.ctxtRate)*1e3/d_tot);
  cr.memory            = m_mem.memTotal;
  cr.memfree           = m_mem.memFree;
  //cout << m_mem << endl;
  for( ; i != cn.cores.end(); i=cn.cores.next(i), j=cl.cores.next(j), m=cr.cores.next(m) ) {
    d_user    = (*i).stats.user    - (*j).stats.user;
    d_system  = (*i).stats.system  - (*j).stats.system;
    d_nice    = (*i).stats.nice    - (*j).stats.nice;
    d_idle    = (*i).stats.idle    - (*j).stats.idle;
    d_iowait  = (*i).stats.iowait  - (*j).stats.iowait;
    d_IRQ     = (*i).stats.IRQ     - (*j).stats.IRQ;
    d_softIRQ = (*i).stats.softIRQ - (*j).stats.softIRQ;
    d_percent = (float)((d_user+d_system+d_nice+d_idle+d_IRQ+d_softIRQ+d_iowait)/1e2);
    (*m).stats.user     = d_user    / d_percent;
    (*m).stats.system   = d_system  / d_percent;
    (*m).stats.nice     = d_nice    / d_percent;
    (*m).stats.idle     = d_idle    / d_percent;
    (*m).stats.iowait   = d_iowait  / d_percent;
    (*m).stats.IRQ      = d_IRQ     / d_percent;
    (*m).stats.softIRQ  = d_softIRQ / d_percent;
  }
  return 1;
}

int SysInfo::combineProcessInfo() {
  const Procset& pn = *procsNow();
  const Procset& pl = *procsLast();
  Procset& pr = *statistics()->procs();
  const Procset::Processes& now = pn.processes;
  const Procset::Processes& last = pl.processes;
  
  Procset::Processes& res = pr.reset()->processes;
  Procset::Processes::const_iterator i, j, k;
  Procset::Processes::iterator m = res.reset();
  double diff = ((1e3*double(pn.time-pl.time))+double(pn.millitm)-double(pl.millitm))/1e3; // seconds!
  pr.time = pn.time;
  pr.millitm = pn.millitm;
  ::strcpy(pr.name,pn.name);
  for(i=now.begin(), j=last.begin(); i != now.end(); i=now.next(i) ) {
    const Process& q = *i;
    int pid = q.pid;
    int found = 0;
    for(k=j; j != last.end(); j=last.next(j) )
      if ( (*j).pid == pid ) {found=1; break;}
    if ( 0==found ) {
      for(j=last.begin(); j != k; j=last.next(j))
        if ( (*j).pid == pid ) {found=1; break;}
    }
    if ( found==1 && (*j).pid == pid ) {
      Process& p = *m;
      ::strncpy(p.utgid,q.utgid,sizeof(p.utgid));
      p.utgid[sizeof(p.utgid)-1] = 0;
      ::strncpy(p.owner,q.owner,sizeof(p.owner));
      p.owner[sizeof(p.owner)-1] = 0;
      ::strncpy(p.cmd,q.cmd,sizeof(p.cmd));
      p.cmd[sizeof(p.cmd)-1] = 0;
      p.cpu     = (q.cpu-(*j).cpu)*1e2/diff;
      p.mem     = (float)(q.vsize * 1e2 / float(m_mem.memTotal));
      p.vsize   = q.vsize;
      p.rss     = q.rss;
      p.stack   = q.stack;
      p.pid     = q.pid;
      p.ppid    = q.ppid;
      p.threads = q.threads;
      p.start   = q.start;
      p.state   = q.state;
      m = res.add(m);
      if ( ((char*)m)-((char*)m_buffer) > (int)m_buffLen ) {
	::lib_rtl_output(LIB_RTL_ERROR,"Global section memory too small.....exiting");
        return 0;
      }
    }
  }
  return 1;
}

/// Initialize the object with all static information
int SysInfo::init() {
  ::lib_rtl_output(LIB_RTL_DEBUG,"SysInfo: Initializing statistics....");
  for(int i=0; i<m_nBuffs-1; ++i) {
    newReading();
    read(*cpuNow()->reset(),CPUINFO_SIZE);
    read(*procsNow()->reset(),PROCINFO_SIZE);
    ::lib_rtl_sleep(1000);
  }
  return 1;
}

/// Update changing object data items
int SysInfo::update(const std::vector<std::string>& dir_names) {
  unsigned long long blk_size = 0, total_blk = 0, availible_blk = 0;
  int ret_code = 1;
  newReading();
  read(m_mem);
  NodeStats* ns = statistics();
  ns->localdisk.blockSize = 0;
  ns->localdisk.numBlocks = 0;
  ns->localdisk.freeBlocks = 0;
  ns->localdisk.tempBlocks = 0;
  ns->localdisk.goodDevices = 0;
  ns->localdisk.numDevices  = (short)dir_names.size();
  for( const auto& dir_name : dir_names )  {
    int ret = ::lib_rtl_diskspace_access(dir_name.c_str(),&blk_size,&total_blk,&availible_blk,W_OK);
    if ( lib_rtl_is_success(ret) )   {
      ns->localdisk.blockSize   = blk_size;
      ns->localdisk.numBlocks  += total_blk;
      ns->localdisk.freeBlocks += availible_blk;
      ++ns->localdisk.goodDevices;
	/// Remove the blocks allocated in the temporary directory used for auxiliary stuff
	string dnam = dir_name+"/../.tmp", fnam;
	DIR* dir_tmp = ::opendir(dnam.c_str());
	if ( dir_tmp ) {
	  struct dirent *entry;
	  while ( (entry=::readdir(dir_tmp)) != 0 ) {
	    struct stat sb;
	    fnam = dnam + "/" + entry->d_name;
	    if ( 0 == ::stat(fnam.c_str(),&sb) )  {
	      size_t nblk = std::ceil(double(sb.st_size)/double(ns->localdisk.blockSize));
	      ns->localdisk.freeBlocks += nblk;
	      ns->localdisk.tempBlocks += nblk;
	    }
	  }
	  ::closedir(dir_tmp);
	}
    }
  }
  ::memcpy(&ns->memory,&m_mem,sizeof(Memory));
  read(*cpuNow()->reset(),CPUINFO_SIZE);
  //readStat(*cpuNow(), CPUINFO_SIZE, m_numCores);
  if ( 0 == read(*procsNow()->reset(), PROCINFO_SIZE) )    {
    ret_code = 0;
  }
  combineCPUInfo();
  combineProcessInfo();
  statistics()->fixup();
  return ret_code;
}

extern "C" int romon_sysinfo(int,char**)  {
  bool do_run = true;
  size_t cnt=0, len = CPUINFO_SIZE+PROCINFO_SIZE;
  char* buff = new char[len];
  std::vector<std::string> hlt1_dirs = {"/localdisk1/hlt1","/localdisk2/hlt1"};
  SysInfo sys((NodeStats*)buff,len,2);
  sys.init();
  while(do_run) {
    stringstream str;
    sys.update(hlt1_dirs);
    ::lib_rtl_output(LIB_RTL_INFO,"SysInfo: %s %ld %s",
		     "============================",++cnt,"===========================");
    str << *sys.statistics();
    ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
    ::lib_rtl_output(LIB_RTL_INFO,"===== DATA SIZE: %d",sys.statistics()->length());
    ::lib_rtl_sleep(5000);
  }
  return 1;
}
