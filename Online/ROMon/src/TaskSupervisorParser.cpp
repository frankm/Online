//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework include files
#include "ROMon/TaskSupervisorParser.h"
#include "XML/Printout.h"

// C++ include files
#include <iostream>
#include <iomanip>
#include <cstring>
#include <ctime>

using namespace std;
using namespace Online;

namespace {
  string str_empty;
  struct Constants  {
    xml_tag_t BOOT              ; // ( "Boot");
    xml_tag_t SYSTEM            ; // ( "System");
    xml_tag_t TASKS             ; // ( "Tasks");
    xml_tag_t TASKLIST          ; // ( "TaskList");
    xml_tag_t TASK              ; // ( "Task");
    xml_tag_t LOCALDISK         ; // ( "Localdisk");
    xml_tag_t NAME              ; // ( "Name");
    xml_tag_t NODE              ; // ( "Node");
    xml_tag_t NODELIST          ; // ( "NodeList");
    xml_tag_t NODETYPE          ; // ( "NodeType");
    xml_tag_t CLUSTER           ; // ( "Cluster");
    xml_tag_t CONNECTIONS       ; // ( "Connections");
    xml_tag_t CONNECTIONLIST    ; // ( "ConnectionList");
    xml_tag_t CONNECTION        ; // ( "Connection");
    xml_tag_t PROJECTS          ; // ( "Projects");
    xml_tag_t PROJECT           ; // ( "Project");
    xml_tag_t PATH              ; // ( "Path");
    xml_tag_t USERNAME          ; // ( "UserName");
    xml_tag_t DIMDNSNODE        ; // ( "DimDnsNode");
    xml_tag_t DIMSERVICENAME    ; // ( "DimServiceName");
    xml_tag_t RESPONSIBLE       ; // ( "Responsible");

    xml_tag_t Attr_name         ; // ( "name");
    xml_tag_t Attr_status       ; // ( "status");
    xml_tag_t Attr_count        ; // ( "count");
    xml_tag_t Attr_time         ; // ( "time");
    xml_tag_t Attr_type         ; // ( "type");
    xml_tag_t Attr_missing      ; // ( "missing");
    xml_tag_t Attr_ok           ; // ( "ok");
    xml_tag_t Attr_eventMgr     ; // ( "event");
    xml_tag_t Attr_dataMgr      ; // ( "data");
    xml_tag_t Attr_distMgr      ; // ( "dist");
    xml_tag_t Attr_fsmSrv       ; // ( "fsmSrv");
    xml_tag_t Attr_devHdlr      ; // ( "devHandler");

    xml_tag_t Attr_rss          ; // ( "rss");
    xml_tag_t Attr_data         ; // ( "data");
    xml_tag_t Attr_stack        ; // ( "stack");
    xml_tag_t Attr_vsize        ; // ( "vsize");
    xml_tag_t Attr_perc_mem     ; // ( "perc_mem");
    xml_tag_t Attr_perc_cpu     ; // ( "perc_cpu");
    xml_tag_t Attr_blk_size     ; // ( "blk_size");
    xml_tag_t Attr_blk_temp     ; // ( "temp");
    xml_tag_t Attr_blk_total    ; // ( "total");
    xml_tag_t Attr_blk_availible; // ( "availible");
    xml_tag_t Attr_dev_num      ; // ( "dev_num");
    xml_tag_t Attr_dev_good     ; // ( "dev_good");
    Constants();
  };

  Constants::Constants() :
    BOOT              ( "Boot"),
    SYSTEM            ( "System"),
    TASKS             ( "Tasks"),
    TASKLIST          ( "TaskList"),
    TASK              ( "Task"),
    LOCALDISK         ( "Localdisk"),
    NAME              ( "Name"),
    NODE              ( "Node"),
    NODELIST          ( "NodeList"),
    NODETYPE          ( "NodeType"),
    CLUSTER           ( "Cluster"),
    CONNECTIONS       ( "Connections"),
    CONNECTIONLIST    ( "ConnectionList"),
    CONNECTION        ( "Connection"),
    PROJECTS          ( "Projects"),
    PROJECT           ( "Project"),
    PATH              ( "Path"),
    USERNAME          ( "UserName"),
    DIMDNSNODE        ( "DimDnsNode"),
    DIMSERVICENAME    ( "DimServiceName"),
    RESPONSIBLE       ( "Responsible"),

    Attr_name         ( "name"),
    Attr_status       ( "status"),
    Attr_count        ( "count"),
    Attr_time         ( "time"),
    Attr_type         ( "type"),
    Attr_missing      ( "missing"),
    Attr_ok           ( "ok"),
    Attr_eventMgr     ( "event"),
    Attr_dataMgr      ( "data"),
    Attr_distMgr      ( "dist"),
    Attr_fsmSrv       ( "fsmSrv"),
    Attr_devHdlr      ( "devHandler"),

    Attr_rss          ( "rss"),
    Attr_data         ( "data"),
    Attr_stack        ( "stack"),
    Attr_vsize        ( "vsize"),
    Attr_perc_mem     ( "perc_mem"),
    Attr_perc_cpu     ( "perc_cpu"),
    Attr_blk_size     ( "blk_size"),
    Attr_blk_temp     ( "temp"),
    Attr_blk_total    ( "total"),
    Attr_blk_availible( "availible"),
    Attr_dev_num      ( "dev_num"),
    Attr_dev_good     ( "dev_good")
  {}
}

static const string Status_OK("OK");
static Constants* gc = 0;
using namespace ROMon;

// ----------------------------------------------------------------------------
XML::TaskSupervisorParser::TaskSupervisorParser()  {
  xml::DocumentHandler::setMinimumPrintLevel(WARNING);
  if ( 0 == gc ) gc = new Constants;
}
// ----------------------------------------------------------------------------
XML::TaskSupervisorParser::~TaskSupervisorParser()   {
}

namespace Online  {
namespace xml  {
  struct Decode   {
    std::string decoded;
    Decode(const std::string& s);
    operator std::string ()  const   {  return decoded; }
    std::string operator()() const   {  return decoded; }
  };
}
}

Online::xml::Decode::Decode(const std::string& s)    {
  decoded.reserve(s.length());
  for(const char* c = s.c_str(); *c; ++c)   {
    switch(*c)   {
    case '&':   {
      switch( *(++c) )   {
      case 'l':  // &lt;
	decoded += '<';
	c += 2;
	break;
      case 'g':  // &gt;
	decoded += '>';
	c += 2;
	break;
      case 'q':  // &quot;
	decoded += '"';
	c += 4;
	break;
      case 'a':  // &apos;
	decoded += '\'';
	c += 4;
	break;
      default:
	break;
      }
      break;
    }
    default:
      decoded += *c;
      break;
    }
  }
}

// ----------------------------------------------------------------------------
void XML::TaskSupervisorParser::getNodes(xml_h fde, Cluster& cluster) const {
  xml_elt_t elt(fde);
  cluster.name   = elt.attr<string>(gc->Attr_name);
  cluster.time   = elt.attr<string>(gc->Attr_time,str_empty);
  cluster.status = elt.attr<string>(gc->Attr_status,str_empty);
  cluster.nodes.clear();
  for(xml_coll_t c(fde,gc->NODE); c; ++c) {
    xml_elt_t b(c.child(gc->BOOT, false));
    xml_elt_t s(c.child(gc->SYSTEM, false));
    xml_elt_t l(c.child(gc->LOCALDISK, false));
    xml_elt_t e(c.child(gc->TASKS, false));
    xml_elt_t p(c.child(gc->PROJECTS, false));
    xml_elt_t g(c.child(gc->CONNECTIONS, false));
    string nam = c.attr<string>(gc->Attr_name);
    cluster.nodes.insert(make_pair(nam,Node(nam,c.attr<string>(gc->Attr_status,str_empty))));
    Node& node = cluster.nodes[nam];

    cluster.time = c.attr<string>(gc->Attr_time,str_empty);
    node.time = c.attr<string>(gc->Attr_time,str_empty);
    node.tasks.clear();
    node.conns.clear();
    node.projects.clear();
    node.blk_size  = 0;
    node.blk_total = 0;
    node.blk_temp  = 0;
    node.blk_availible = 0;
    node.dev_good  = 0;
    node.dev_num   = 0;
    node.totalTaskCount = node.missTaskCount = node.taskCount = 0;
    node.totalConnCount = node.missConnCount = node.connCount = 0;
    node.rss = node.vsize = node.data = node.stack = 0;
    node.perc_cpu = node.perc_mem = 0.;
    if ( b ) {
      char buff[64];
      ::memset(buff,0,sizeof(buff));
      time_t boot_time = ::atol(b.attr<string>(gc->Attr_time,"0").c_str());
      ::strftime(buff,sizeof(buff),"%Y-%m-%d %H:%M:%S",::localtime(&boot_time));
      node.boot = buff;
    }
    if ( l ) {
      node.dev_num   = ::atoi(l.attr<string>(gc->Attr_dev_num,"0").c_str());
      node.dev_good  = ::atoi(l.attr<string>(gc->Attr_dev_good,"0").c_str());
      node.blk_size  = ::atol(l.attr<string>(gc->Attr_blk_size,"0").c_str());
      node.blk_temp  = ::atol(l.attr<string>(gc->Attr_blk_temp,"0").c_str());
      node.blk_total = ::atol(l.attr<string>(gc->Attr_blk_total,"0").c_str());
      node.blk_availible = ::atol(l.attr<string>(gc->Attr_blk_availible,"0").c_str());
    }
    if ( s ) {
      node.rss = ::atof(s.attr<string>(gc->Attr_rss,"0").c_str());
      if ( node.rss < 0 ) node.rss     = -1;
      node.data = ::atof(s.attr<string>(gc->Attr_data,"0").c_str());
      if ( node.data < 0 ) node.data   = -1;
      node.stack = ::atof(s.attr<string>(gc->Attr_stack,"0").c_str());
      if ( node.stack < 0 ) node.stack = -1;
      node.vsize = ::atof(s.attr<string>(gc->Attr_vsize,"0").c_str());
      if ( node.data < 0 ) node.vsize  = -1;
      node.perc_cpu = ::atof(s.attr<string>(gc->Attr_perc_cpu,"0").c_str());
      node.perc_mem = ::atof(s.attr<string>(gc->Attr_perc_mem,"0").c_str());
    }
    if ( e ) {
      node.taskCount      = ::atoi(e.attr<string>(gc->Attr_ok,"0").c_str());
      node.totalTaskCount = ::atoi(e.attr<string>(gc->Attr_count,"0").c_str());
      node.missTaskCount  = ::atoi(e.attr<string>(gc->Attr_missing,"0").c_str());
      for(xml_coll_t t(e,gc->TASK); t; ++t)
        node.tasks.push_back(make_pair(xml::Decode(t.attr<string>(gc->Attr_name)),
				       t.attr<string>(gc->Attr_status,str_empty)==Status_OK));
    }
    if ( g ) {
      node.connCount      = ::atoi(g.attr<string>(gc->Attr_ok,"0").c_str());
      node.totalConnCount = ::atoi(g.attr<string>(gc->Attr_count,"0").c_str());
      node.missConnCount  = ::atoi(g.attr<string>(gc->Attr_missing,"0").c_str());
      for(xml_coll_t t(g,gc->CONNECTION); t; ++t)
        node.conns.push_back(make_pair(xml::Decode(t.attr<string>(gc->Attr_name)),
				       t.attr<string>(gc->Attr_status,str_empty)==Status_OK));
    }
    if ( p ) {
      for(xml_coll_t t(p,gc->PROJECT); t; ++t) {
        node.projects.push_back(Cluster::PVSSProject());
        Cluster::PVSSProject& pr = node.projects.back();
        pr.name     = xml::Decode(t.attr<string>(gc->Attr_name));
        pr.eventMgr = t.attr<string>(gc->Attr_eventMgr,str_empty)=="RUNNING";
        pr.dataMgr  = t.attr<string>(gc->Attr_dataMgr,str_empty)=="RUNNING";
        pr.distMgr  = t.attr<string>(gc->Attr_distMgr,str_empty)=="RUNNING";
        pr.devHdlr  = t.attr<string>(gc->Attr_devHdlr,str_empty)=="RUNNING";
        pr.fsmSrv   = t.attr<string>(gc->Attr_fsmSrv,str_empty)=="RUNNING";
      }
    }
  }
}

// ----------------------------------------------------------------------------
bool XML::TaskSupervisorParser::parseFile(const std::string& file)   {
  this->assign(xml::DocumentHandler().load(file));
  return this->root().ptr() != 0;
}

// ----------------------------------------------------------------------------
bool XML::TaskSupervisorParser::parseBuffer(const std::string& sys, const void* data, size_t len)   {
  this->assign(xml::DocumentHandler().parse((const char*)data, len, sys.c_str(), 0));
  return this->root().ptr() != 0;
}

// ----------------------------------------------------------------------------
void XML::TaskSupervisorParser::getClusterNodes(Cluster& cluster) const {
  getNodes(this->root(),cluster);
}

// ----------------------------------------------------------------------------
void XML::TaskSupervisorParser::getClusters(Clusters& clusters) const {
  for(xml_coll_t c(this->root(), gc->CLUSTER); c; ++c) {
    clusters.push_back(Cluster(c.attr<string>(gc->Attr_name),c.attr<string>(gc->Attr_status,str_empty)));
    getNodes(c,clusters.back());
  }
}

// ----------------------------------------------------------------------------
void XML::TaskSupervisorParser::getInventory(Inventory& inv) const {
  typedef Inventory::Task              _T;
  typedef Inventory::TaskList          _TL;
  typedef Inventory::ConnectionList    _CL;
  typedef Inventory::NodeType          _NT;
  typedef Inventory::NodeCollection    _NC;
  xml_h fde = this->root();

  for(xml_coll_t c(fde, gc->TASK); c; ++c) {
    string typ = xml::Decode(c.hasAttr(gc->Attr_type) ? c.attr<string>(gc->Attr_type) : string());
    string nam = xml::Decode(c.attr<string>(gc->Attr_name));
    inv.tasks[nam] = _T(nam,typ);
    _T& t = inv.tasks[nam];
    if ( c.hasChild(gc->PATH) ) t.path = c.child(gc->PATH).value();
    if ( c.hasChild(gc->USERNAME) ) t.user = c.child(gc->USERNAME).value();
    if ( c.hasChild(gc->DIMDNSNODE) ) t.dimDns = c.child(gc->DIMDNSNODE).value();
    if ( c.hasChild(gc->DIMSERVICENAME) ) t.dimSvc = c.child(gc->DIMSERVICENAME).value();
    if ( c.hasChild(gc->RESPONSIBLE)    ) t.responsible = c.child(gc->RESPONSIBLE).value();
  }
  for(xml_coll_t c(fde, gc->TASKLIST); c; ++c) {
    string nam = xml::Decode(c.attr<string>(gc->Attr_name));
    _TL& tl = inv.tasklists[nam];
    for(xml_coll_t t(c, gc->TASK); t; ++t)
      tl.push_back(xml::Decode(t.attr<string>(gc->Attr_name)));
  }
  for(xml_coll_t c(fde, gc->CONNECTIONLIST); c; ++c) {
    _CL& cl = inv.connlists[xml::Decode(c.attr<string>(gc->Attr_name))];
    for(xml_coll_t t(c, gc->CONNECTION); t; ++t)
      cl.push_back(xml::Decode(t.attr<string>(gc->Attr_name)));
  }
  // Build whole Node type
  for(xml_coll_t c(fde, gc->NODETYPE); c; ++c) {
    _NT& nt = inv.nodetypes[xml::Decode(c.attr<string>(gc->Attr_name))];
    nt.name = xml::Decode(c.attr<string>(gc->Attr_name));
    for(xml_coll_t t(c, gc->TASKLIST); t; ++t) {
      const _TL& tl = inv.tasklists[xml::Decode(t.attr<string>(gc->Attr_name))];
      nt.tasks.insert(nt.tasks.end(),tl.begin(),tl.end());
    }
    for(xml_coll_t t(c, gc->CONNECTIONLIST); t; ++t) {
      const _CL& cl = inv.connlists[xml::Decode(t.attr<string>(gc->Attr_name))];
      nt.connections.insert(nt.connections.end(),cl.begin(),cl.end());
    }
    for(xml_coll_t t(c, gc->TASK); t; ++t)
      nt.tasks.push_back(xml::Decode(t.attr<string>(gc->Attr_name)));
    for(xml_coll_t t(c, gc->CONNECTION); t; ++t)
      nt.connections.push_back(xml::Decode(t.attr<string>(gc->Attr_name)));
    for(xml_coll_t t(c, gc->PROJECT); t; ++t)
      nt.projects.push_back(xml::Decode(t.attr<string>(gc->Attr_name)));
  }
  for(xml_coll_t c(fde, gc->NODELIST); c; ++c) {
    string nam = xml::Decode(c.attr<string>(gc->Attr_name));
    if ( nam.empty() ) {
      for(xml_coll_t n(c, gc->NAME); n; ++n)
        nam = xml::Decode(n.value());
      cout << "---->:" << nam << endl;
    }
    _NC& nc = inv.nodecollections[nam];
    nc.name = nam;
    for(xml_coll_t n(c, gc->NODE); n; ++n)
      nc.nodes[xml::Decode(n.attr<string>(gc->Attr_name))] = xml::Decode(n.attr<string>(gc->Attr_type,str_empty));
  }
}

#include "RTL/rtl.h"
#include <cstdarg>

static void help_cluster() {
  ::printf("You have to supply a file name\n");
  ::exit(0);
}

static size_t prt(void*,int,const char* fmt,va_list& args) {
  size_t result;
  string format = fmt;
  format += "\n";
  result = ::vfprintf(stdout, format.c_str(), args);
  ::fflush(stdout);
  return result;
}

static string getInput(int argc, char** argv) {
  RTL::CLI cli(argc,argv,help_cluster);
  string fname;
  cli.getopt("input",1,fname);
  if ( fname.empty() ) help_cluster();
  ::lib_rtl_install_printer(prt,0);
  return fname;
}

extern "C" int tsksup_inventory(int argc, char** argv) {
  string fname=getInput(argc,argv);
  XML::TaskSupervisorParser ts;
  ts.parseFile(fname);
  ROMon::Inventory inv;
  ts.getInventory(inv);
  cout << inv << endl;
  return 0;
}

extern "C" int tsksup_cluster(int argc, char** argv) {
  string fname=getInput(argc,argv);
  XML::TaskSupervisorParser ts;
  ts.parseFile(fname);
  Cluster cluster;
  ts.getClusterNodes(cluster);
  cout << cluster << endl;
  return 0;
}
extern "C" int tsksup_network(int argc, char** argv) {
  string fname=getInput(argc,argv);
  XML::TaskSupervisorParser ts;
  XML::TaskSupervisorParser::Clusters clusters;
  ts.parseFile(fname);
  ts.getClusters(clusters);
  cout << clusters << endl;
  return 0;
}
