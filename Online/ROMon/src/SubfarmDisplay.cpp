//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// C++ include files
#include <cstdlib>
#include <iostream>
#include <limits>

// Framework include files
#include "RTL/strdef.h"
#include "ROMonDefs.h"
#define MBM_IMPLEMENTATION
#include "ROMon/ROMon.h"
#include "ROMon/Constants.h"
#include "ROMon/SubfarmDisplay.h"
#include "dim/dic.hxx"

using namespace ROMon;
using namespace std;

typedef Nodeset::Nodes               Nodes;
typedef Node::Buffers                Buffers;
typedef MBMBuffer::Clients           Clients;
typedef Node::Tasks                  Tasks;

namespace {
  struct Info {
    long data[5];
    Info() { 
      data[0]=data[1]=data[2]=data[3]=data[4]=0;
    }
    Info(const Info& c) { 
      data[0]=c.data[0];
      data[1]=c.data[1];
      data[2]=c.data[2];
      data[3]=c.data[3];
      data[4]=c.data[4];
    }
    Info& operator=(const Info& c)  {
      data[0]=c.data[0];
      data[1]=c.data[1];
      data[2]=c.data[2];
      data[3]=c.data[3];
      data[4]=c.data[4];
      return *this;
    }
  };
}

static void help() {
  cout <<"  romon_subfarm -option [-option]" << endl
       <<"       -h[eaderheight]=<number>     Height of the header        display.                      " << endl
       <<"       -d[elay]=<number>            Time delay in millisecond between 2 updates.              " << endl
       <<"       -p[artition]=<name>          Partition name for buffer filtering.                      " << endl
       <<"       -s[ervicename]=<name>        Name of the DIM service  providing monitoring information." << endl
       << endl;
}

/// Static abstract object creator.
ClusterDisplay* ROMon::createSubfarmDisplay(int width, int height, int posx, int posy, int argc, char** argv) {
  return new SubfarmDisplay(width,height,posx,posy,argc,argv);
}

/// Standard constructor
SubfarmDisplay::SubfarmDisplay(int width, int height, int posx, int posy, int argc, char** argv)
  : ClusterDisplay(width, height), m_nodes(0)
{
  m_position = Position(posx,posy);
  init(argc, argv);
}

/// Standard constructor
SubfarmDisplay::SubfarmDisplay(int argc, char** argv) : ClusterDisplay(), m_nodes(0)   {
  init(argc, argv);
}

void SubfarmDisplay::init(int argc, char** argv)   {
  RTL::CLI cli(argc,argv,help);
  int hdr_height;

  cli.getopt("headerheight",  1, hdr_height    =    5);
  cli.getopt("delay",         1, m_delay       = 1000);
  cli.getopt("partition",     1, m_partition   = "*");
  cli.getopt("servicename",   1, m_svcName     = "/hlte07/ROpublish");
  if ( m_partition == "*" ) m_partition = "";
  else if ( m_partition == "ALL" ) m_partition = "";
  if ( cli.getopt("debug", 5) ) {
    printf("To start debugger type:  gdb --pid %d\n",::lib_rtl_pid());
    ::lib_rtl_sleep(30*1000);
  }
  setup_window();
  int posx     = m_position.x-2;
  int posy     = m_position.y-2;
  string nodes_title = "Node Information";
  if ( !m_partition.empty() )  {
    nodes_title += " Partition:"+m_partition;
  }
  m_nodes      = createSubDisplay(Position(posx,posy+hdr_height),
                                  Area(m_area.width,m_area.height - hdr_height),
                                  nodes_title);
  end_update();
  m_measure = 0;
}

/// Standard destructor
SubfarmDisplay::~SubfarmDisplay()  {
  begin_update();
  if ( m_nodes    ) delete m_nodes;
  end_update();
}

/// Display the node information
void SubfarmDisplay::showNodes(const Nodeset& ns)  {
  MonitorDisplay* disp = m_nodes;
  const char* fmt = " %-12s%5d %s";
  char buff_text[256], text1[256], text2[256];
  map<string,Info>   totals;
  map<size_t,string> buffers;
  long ntsk_tot = 0;
  bool partitioned = !m_partition.empty();

  ::snprintf(text1,sizeof(text1),"           - MBM -  ");
  ::snprintf(text2,sizeof(text2)," Node      Clients ");
  const char* pattern = "  -------------------------------------   ";

  for (Nodes::const_iterator n=ns.nodes.begin(); n!=ns.nodes.end(); n=ns.nodes.next(n))  {
    const Buffers& buffs = *(*n).buffers();
    bool doBreak = false;
    buff_text[0] = 0;
    for(Buffers::const_iterator ib=buffs.begin(); ib!=buffs.end(); ib=buffs.next(ib))  {
      const char* bn = (*ib).name;
      size_t bnlen = ::strlen(bn);
      if ( partitioned && ro_match_end(m_partition,bn) ) {
        size_t len = ::strlen(text1);
        ::snprintf(text1+len,sizeof(text1),"%s",pattern);
        ::snprintf(text1+len+12,sizeof(text1),"%s",(*ib).name);
        text1[len+11] = ' ';
        text1[len+12+bnlen] = ' ';
        len = ::strlen(text2);
        ::snprintf(text2+len,sizeof(text2),"%11s%11s%6s%12s%2s","Produced","Consumed","Slots","Space","");
        doBreak = true;
      }
    }
    if ( doBreak ) break;
  }
  if ( !partitioned )    {
    text1[0] = 0;
    ::snprintf(text2,sizeof(text2),"  %16s %16s %16s %16s",
	       "No.Tasks", "No.Buffers", "No.Consumers","No.Producers");
  }
  text1[sizeof(text1)-1] = 0;
  text2[sizeof(text2)-1] = 0;
  disp->draw_line(header_flags, text1);
  disp->draw_line(header_flags, text2);
  ++m_measure;
  for (Nodes::const_iterator n=ns.nodes.begin(); n!=ns.nodes.end(); n=ns.nodes.next(n))  {
    map<string,map<string,pair<long,long> > >::iterator in = m_minimal.find((*n).name);
    const Buffers& buffs = *(*n).buffers();
    size_t ntsk = 0;
    buff_text[0] = 0;
    if ( in == m_minimal.end() ) in = m_minimal.insert(make_pair((*n).name,map<string,pair<long,long> >())).first;
    size_t buffs_node = 0, cons_node = 0, prod_node = 0;
    for(Buffers::const_iterator ib=buffs.begin(); ib!=buffs.end(); ib=buffs.next(ib))  {
      Info info;
      const Clients& clients = (*ib).clients;
      ++buffs_node;
      if ( partitioned && ro_match_end(m_partition,(*ib).name) ) {
        map<string,pair<long,long> >::iterator ibuf = (*in).second.find((*ib).name);
        const MBMBuffer::Control&  ctrl = (*ib).ctrl;
        const char* bn = (*ib).name;
        if ( ibuf == (*in).second.end() ) ibuf = (*in).second.insert(make_pair(bn,make_pair(0,0))).first;
        info.data[0] = ctrl.tot_produced;
        info.data[1] = ctrl.p_emax-ctrl.i_events;
        info.data[2] = (ctrl.i_space*ctrl.bytes_p_Bit)/1024;
        for (Clients::const_iterator ic=clients.begin(); ic!=clients.end(); ic=clients.next(ic))  {
          ++ntsk;
          if ( (*ic).type == 'C' ) info.data[3] += (*ic).events;
          if ( (*ic).type == 'P' ) info.data[4] += (*ic).events;
        }
        if ( totals.find(bn) == totals.end() ) buffers[totals.size()] = bn;
        for(size_t k=0; k<5; ++k) totals[bn].data[k] += info.data[k];
        size_t len = ::strlen(buff_text);
        ::snprintf(buff_text+len,sizeof(buff_text)-len,"%11ld%11ld%6ld%12ld%s",
                   info.data[4],info.data[4],info.data[1],info.data[2],(*ibuf).second.second ? "/S" : "  ");
	buff_text[sizeof(buff_text)-1] = 0;
      }
      else if ( !partitioned )  {
        for (Clients::const_iterator ic=clients.begin(); ic!=clients.end(); ic=clients.next(ic))  {
	  ++ntsk;
          if ( (*ic).type == 'C' ) ++cons_node;
          if ( (*ic).type == 'P' ) ++prod_node;
	}
      }
    }
    ntsk_tot += ntsk;
    if ( !partitioned )  {
      ::snprintf(buff_text,sizeof(buff_text),"%16ld %16ld %16ld",
		 buffs_node, cons_node, prod_node);
    }
    disp->draw_line_normal(fmt, (*n).name, ntsk, buff_text);
  }
  disp->draw_line_normal("");
  if ( partitioned )  {
    buff_text[0] = 0;
    for(size_t i=0; i<totals.size(); ++i)  {
      size_t len  = ::strlen(buff_text);
      Info&  info = totals[buffers[i]];
      ::snprintf(buff_text+len,sizeof(buff_text)-len,"%11ld%11ld%6ld%12ld  ",
		 info.data[4],info.data[4],info.data[1],info.data[2]);
    }
    disp->draw_line(summary_flags, fmt, "Total:", ntsk_tot, buff_text);
  }
  disp->draw_line_normal("");
  disp->draw_line_normal("");
  disp->draw_line_normal("");
  disp->draw_line_normal("  c or C    ->  Show The CPU monitor");
  disp->draw_line_normal("  d or D    ->  Show the CPU monitor (bar-format)");
  disp->draw_line_normal("  l or L    ->  Show System info of the subfarm");
  disp->draw_line_normal("  b or B    ->  Show buffer manager of selected node");
  disp->draw_line_normal("  s or S    ->  Show CPU Usage summary for the whole subfarm");
  disp->draw_line_normal("  p or P    ->  Show tasks with a UTGID");
  disp->draw_line_normal("  w or W    ->  Show WINCC tasks (Controls PCs only) ");
  disp->draw_line_normal("  v or V    ->  Show SYSTEM processes ");
  disp->draw_line_normal("  a or A    ->  Show NON system, NON WINCC, NON utgid processes (The rest)");
  disp->draw_line_normal("  CTRL-W    ->  Redraw window");
  disp->draw_line_normal("  CTRL-e    ->  Exit");
  disp->draw_line_normal("  h/H       ->  Help");
}

/// Update header information
void SubfarmDisplay::showHeader(const Nodeset& ns)   {
  char b1[64], b2[64];
  TimeStamp frst=ns.firstUpdate(), last=ns.lastUpdate();
  time_t t1 = frst.first, t2 = last.first;
  ::strftime(b1,sizeof(b1),"%H:%M:%S",::localtime(&t1));
  ::strftime(b2,sizeof(b1),"%H:%M:%S",::localtime(&t2));
  draw_line_normal ("");
  
  draw_line_reverse("         Subfarm monitoring on %s   [%s]   %s %s", ns.name, ::lib_rtl_timestr(),
                    m_partition.empty() ? "" : "Partition:",
                    m_partition.empty() ? "" : m_partition.c_str());
  draw_line_bold   ("         Information updates date between: %s.%03d and %s.%03d",b1,frst.second,b2,last.second);
}

/// Number of nodes in the dataset
size_t SubfarmDisplay::numNodes()  {
  size_t n = 0;
  const Nodeset* ns = data().data<const Nodeset>();
  if ( ns ) {
    switch(ns->type)  {
    case Nodeset::TYPE:
      n = ns->nodes.size();
    default:
      break;
    }
  }
  return n;
}

/// Retrieve cluster name from cluster display
std::string SubfarmDisplay::clusterName() const {
  const Nodeset* ns = (const Nodeset*)data().pointer;
  return ns ? ns->name : "";
}

/// Retrieve node name from cluster display by offset
string SubfarmDisplay::nodeName(size_t offset) {
  const Nodeset* ns = (const Nodeset*)data().pointer;
  if ( ns ) {
    size_t cnt;
    Nodes::const_iterator n;
    const Nodes& nodes = ns->nodes;
    for (n=nodes.begin(), cnt=0; n!=nodes.end(); n=nodes.next(n), ++cnt)  {
      if ( cnt == offset ) {
        return (*n).name;
      }
    }
  }
  return "";
}

/// Update all displays
void SubfarmDisplay::updateDisplay(const Nodeset& ns)   {
  begin_update();
  m_nodes->begin_update();
  showHeader(ns);
  showNodes(ns);
  m_nodes->end_update();
  end_update();
}

extern "C" int romon_subfarm(int argc,char** argv) {
  SubfarmDisplay disp(argc,argv);
  disp.initialize();
  disp.run();
  return 1;
}
