//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// C++ include files
#include <cstdlib>

// Framework include files
#include "CPP/IocSensor.h"

#define MBM_IMPLEMENTATION
#include "ROMonDefs.h"
#include "ROMon/ROMon.h"
#include "ROMon/ROMonDisplay.h"
#include "dim/dic.hxx"

using namespace ROMon;

char* ROMonDisplay::Descriptor::reserve(size_t siz) {
  if ( siz > length ) {
    release();
    pointer = (char*)::malloc(length=siz);
  }
  return pointer;
}

/// Initializing constructor
ROMonDisplay::ROMonDisplay(int width, int height)
  : ScrDisplay(width,height)
{
}

/// Initialize the data access
void ROMonDisplay::initialize()   {
  m_svcID = ::dic_info_service(m_svcName.c_str(),MONITORED,0,0,0,infoHandler,(long)this,0,0);
}

/// Finalize data access
void ROMonDisplay::finalize()   {
  try {
    if ( m_svcID2 != 0 ) {
      ::dic_release_service(m_svcID2);
      m_svcID2 = 0;
    }
    if ( m_svcID != 0 ) {
      ::dic_release_service(m_svcID);
      m_svcID = 0;
    }
  }
  catch(...) {
  }
}

/// Start the update cycle
void ROMonDisplay::update()   {
  const Nodeset* ns = m_data.data<const Nodeset>();
  if ( ns && m_data.actual>0 ) {
    if ( ns->type == Nodeset::TYPE ) {
      DimLock dim_lock;
      std::lock_guard<std::mutex> lock(m_lock);
      updateDisplay(*ns);
    }
    else if ( ns->type == Node::TYPE ) {
      DimLock dim_lock;
      std::lock_guard<std::mutex> lock(m_lock);
      updateDisplay(*m_data.data<const Node>());
    }
  }
}

/// Update all displays
void ROMonDisplay::updateDisplay(const Nodeset& /* ns */)   {
}

/// Update all displays
void ROMonDisplay::updateDisplay(const Node& /* ns */)   {
}

/// Run the interrupt loop
void ROMonDisplay::run()   {
  while(1) {
    //IocSensor::instance().send(this,CMD_UPDATEDISPLAY,this);
    update();
    ::lib_rtl_sleep(m_delay);
  }
}

/// Interactor overload: Display callback handler
void ROMonDisplay::handle(const CPP::Event& ev) {
  switch(ev.eventtype) {
  case TimeEvent:
    if ( ev.timer_data == (void*)CMD_UPDATEDISPLAY ) 
      update();
    break;
  case IocEvent:
    if ( ev.type == CMD_UPDATEDISPLAY ) 
      update();
  default:
    break;
  }
}

/// DimInfoHandler overload
void ROMonDisplay::infoHandler(void* tag, void* address, int* size) {
  if ( address && tag && size && *size>0) {
    size_t len = *size;
    ROMonData data(address);
    ROMonDisplay* display = *(ROMonDisplay**)tag;
    Descriptor& d = display->m_data;
    switch(data.type())  {
    case Node::TYPE:
    case Nodeset::TYPE:
      break;
    default:
      if ( !display->m_readAlways ) return;
    }
    std::lock_guard<std::mutex> lock(display->lock());
    if ( d.length < len+1 ) {
      d.length = len+1;
      d.reserve(size_t(1.2*len));
    }
    d.actual = len;
    ::memcpy(d.data<char>(),address,d.actual);
    d.data<char>()[len]=0;
  }
}
