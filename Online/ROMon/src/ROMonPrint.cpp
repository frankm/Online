//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMonPrint.cpp
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <ROMon/ROMonPrint.h>
#include <ROMon/TaskSupervisor.h>
#include <ROMon/TaskSupervisorParser.h>
#include <MBM/bmstruct.h>
#include <ROMon/CPUMon.h>
#include <ROMon/ROMon.h>
#include "sstat.h"

// C/C++ include files
#include <stdexcept>
#include <iostream>
#include <cstdio>

using namespace std;

#define s_line_seperator "+=================================================================================================================\n"

namespace ROMon  {

  template <> void Printer<Process>::print(const Process& p)  {
    time_t tim = p.start;
    char state, text[64], tmb[32];
    ::strftime(tmb,sizeof(tmb),"%b %d %H:%M",::localtime(&tim));
    state = p.state == 0 ? '?' : p.state;
    if ( ::strncmp(p.utgid,"N/A",3)==0 ) {
      ::strncpy(text,p.cmd,sizeof(text));
      text[27] = 0;
      text[26]=text[25]=text[24]='.';
      ::printf("|  %3s:%-28s %-10s %c %5d %5d %6.3f %6.1f %7.1f %7.0f %6.1f %3d %s  %s\n",
	       p.utgid,text,p.owner,state,p.pid,p.ppid,p.mem,p.vsize/1024.,p.rss/1024.,
	       p.stack,p.cpu,p.threads,tmb,"** No UTGID");
    }
    else {
      ::printf("|  %-32s %-10s %c %5d %5d %6.3f %6.1f %7.1f %7.0f %6.1f %3d %s\n",
	       p.utgid,p.owner,state,p.pid,p.ppid,p.mem,p.vsize/1024.,p.rss/1024.,p.stack,p.cpu,p.threads,tmb);
    }
  }

  template <> void Printer<FSMTask>::print(const FSMTask& p)  {
    char tmb[32];
    time_t tim = p.doneCmd;
    ::strftime(tmb,sizeof(tmb),"%b %d %H:%M",::localtime(&tim));
    ::printf("|  %-32s pid:%10d part ID:%06X State:%c Target:%c Meta:%c   %s\n",
	     p.name,p.processID, p.partitionID, p.state, 
	     p.targetState, p.metaState, tmb);
  }


  template <> void Printer<MBMBuffer>::print(const MBMBuffer& b)  {
    const MBMBuffer::Clients& clients   = b.clients;
    const MBMBuffer::Control& c = b.ctrl;
    ::printf("|  Buffer:%-18s  Produced:%lld Actual:%lld Seen:%lld Pending:%-3ld Max:%-3d "
	     "Space(kB):[Tot:%-6ld Free:%-6ld] Users:[Tot:%-3ld Max:%-3d]\n",
	     b.name, c.tot_produced, c.tot_actual, c.tot_seen, c.i_events, c.p_emax,
	     long((c.bm_size*c.bytes_p_Bit)/1024), long((c.i_space*c.bytes_p_Bit)/1024), 
	     c.i_users, c.p_umax);
    if ( clients.size() > 0 )  {
      for (auto ic=clients.begin(); ic!=clients.end(); ic=clients.next(ic))  {
	MBMBuffer::Clients::const_reference c = (*ic);
	const char* cnam = c.name;
	if ( cnam ) {
	  if ( c.type == 'C' )
	    ::printf("|  %-40sPart:%04X pid:%6d C%4s%12ld %c%c%c%c %s\n",cnam,c.partitionID,c.processID,
		     sstat[(size_t)c.state],c.events,c.reqs[0],c.reqs[1],c.reqs[2],c.reqs[3],b.name);
	  else if ( c.type == 'P' )
	    ::printf("|  %-40sPart:%04X pid:%6d P%4s%12ld %4s %s\n",cnam,c.partitionID,c.processID,
		     sstat[(size_t)c.state],c.events,"",b.name);
	  else
	    ::printf("|  %-40sPart:%04X pid:%6d ?%4s%12s %4s %s\n",cnam,c.partitionID,c.processID,"","","",b.name);
	}
      }
    }
  }

  template <> void Printer<Node>::print(const Node& n)   {
    const Node::Buffers* buffs = n.buffers();
    const Node::Tasks* procs = n.tasks();
    time_t tim = n.time;
    char text[128];
    ::strftime(text,sizeof(text),"%H:%M:%S",::localtime(&tim));
    ::printf(s_line_seperator);
    ::printf("|  Node:%s last update:%s%03d [%d processes]\n",n.name,text,n.millitm,procs->size());
    for(Node::Tasks::const_iterator ip=procs->begin(); ip!=procs->end(); ip=procs->next(ip))
      printer(*ip)();
    for(Node::Buffers::const_iterator ib=buffs->begin(); ib!=buffs->end(); ib=buffs->next(ib))  {
      ::printf(s_line_seperator);
      printer(*ib)();
    }
  }

  template <> void Printer<Procset>::print(const Procset& procset)  {
    typedef Procset::Processes _P;
    const _P& ps = procset.processes;
    ::printf(s_line_seperator);
    ::printf("|  Node \"%s\" has %d processes\n",procset.name,ps.size());
    ::printf("|  %-32s %-6s %-5s %5s %5s %6s %6s %7s %7s %6s %3s %s\n|\n",
	     "UTGID","Owner","State","PID","PPID","Mem[%]",
	     "VM[MB]","RSS[MB]","Stk[kB]","CPU[%]","Thr","Started");
    for(_P::const_iterator ip=ps.begin(); ip!=ps.end(); ip=ps.next(ip))
      printer(*ip)();
    ::printf(s_line_seperator);
  }

  template <> void Printer<CPU>::print(const CPU& c)  {
    ::printf("|  %5.0f %5d %9.0f %9.3f %9.3f %9.3f %9.3f %10.3f %8.3f %8.3f\n",
	     c.clock,c.cache,c.bogomips,c.stats.user,c.stats.system,c.stats.nice,
	     c.stats.idle,c.stats.iowait,c.stats.IRQ,c.stats.softIRQ);
  }

  template <> void Printer<CPUset>::print(const CPUset& cs)  {
    typedef CPUset::Cores _C;
    const _C& c = cs.cores;
    const CPU::Stat& avg = cs.averages;
    time_t t1 = cs.time;
    char tim[128];
    ::printf(s_line_seperator);
    ::strftime(tim,sizeof(tim),"%H:%M:%S",::localtime(&t1));
    ::printf("|  Node \"%s\" has %d cores. Last update:%s\n",cs.name,c.size(),tim);
    ::printf("|  %-15s %5s %5s %5s %5s %8s %9s %8s %8s %10s %8s %8s\n",
	     "Familiy","Cores","Mtot","Mfree","Ctxt","User[%]",
	     "System[%]","Nice[%]","Idle[%]","IO wait[%]","IRQ","SoftIRQ");
    ::printf("|  %-15s %5d %5.0f %5.0f %5.0f %8.3f %9.3f %8.3f %8.3f %10.2f %8.3f %8.3f\n",
	     cs.family,cs.cores.size(),double(cs.memory/1024),double(cs.memfree/1024),
	     cs.ctxtRate,avg.user,avg.system,avg.nice,avg.idle,avg.iowait,avg.IRQ,avg.softIRQ);
    ::printf("+= %5s %5s %9s %9s %9s %9s %9s %10s %8s %8s\n",
	     "Clock","Cache","Bogomips","User[%]","Sys[%]","Nice[%]",
	     "Idle[%]","IO wait[%]","IRQ","SoftIRQ");
    for(_C::const_iterator ic=c.begin(); ic!=c.end(); ic=c.next(ic))
      printer(*ic)();
  }

  template <> void Printer<DeferredHLTSubfarmStats>::print(const DeferredHLTSubfarmStats& hs)   {
    typedef DeferredHLTSubfarmStats _S;
    ::printf(s_line_seperator);
    ::printf("|  Subfarm: %s  has %d runs.\n",hs.name,hs.runs.size());
    for(_S::Runs::const_iterator i=hs.runs.begin(); i!=hs.runs.end(); i=hs.runs.next(i)) {
      const _S::RunItem& r = (*i);
      ::printf("|  %-10d :  %-10d files.\n",r.first, r.second);
    }
    ::printf(s_line_seperator);
  }
}
