//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMonPrint.h
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_ROMON_ROMONPRINT_H
#define ONLINE_ROMON_ROMONPRINT_H

/*
 *   ROMon namespace declaration
 */
namespace ROMon {
  /// Helper class to dump ROMon objects to stdout
  /**
   *  @author  M.Frank
   *  @version 1.0
   */
  template<typename T> class Printer {
    typedef T object_t;
    const object_t& object;
  public:
    /// No default constructor
    Printer() = delete;
    /// Initializing constructor
    Printer(const object_t& o) : object(o) {}
    /// Copy constructor
    Printer(const Printer& p) = default;
    /// Assignment operator
    Printer& operator=(const Printer& c) = default;
    /// Static print method
    static void print(const object_t& o);
    /// Operator() invoking the printout
    void operator()()  const  {  print(object); }
  };
  /// Helper function to create a new printer
  template <typename T> Printer<T> printer(const T& o) {  return Printer<T>(o); }

}      // End namespace ROMon

#endif // ONLINE_ROMON_ROMONPRINT_H
