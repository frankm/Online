//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROMON_SUBFARMDISPLAY_H
#define ROMON_SUBFARMDISPLAY_H 1

// Framework includes
#include "ROMon/ClusterDisplay.h"

/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  /**@class HltSubfarmDisplay ROMon.h GaudiOnline/HltSubfarmDisplay.h
   *
   *   Monitoring display for the LHCb storage system.
   *
   *   @author M.Frank
   */
  class HltSubfarmDisplay : public ClusterDisplay  {
  protected:

    /// reference to the node display
    MonitorDisplay* m_nodes;

    /// Initialize window
    void init(int argc, char** arv);

  public:
    /// Initializing constructor
    HltSubfarmDisplay(int width, int height, int posx, int posy, int argc, char** argv);

    /// Standard constructor
    HltSubfarmDisplay(int argc, char** argv);

    /// Standard destructor
    virtual ~HltSubfarmDisplay();

    /// Number of nodes in the dataset
    size_t numNodes() override;

    /// Retrieve cluster name from cluster display
    std::string clusterName() const override;

    /// Retrieve node name from cluster display by offset
    std::string nodeName(size_t offset) override;

    /// Access Node display
    MonitorDisplay* nodeDisplay() const override {    return m_nodes; }

    /// Show the display header information (title, time, ...)
    void showHeader();

    /// Display the node information
    void showNodes();

    /// Update all displays
    void update() override;
  };

  /// Static abstract object creator.
  ClusterDisplay*  createHltSubfarmDisplay(int width, int height, int posx, int posy, int argc, char** argv);
}      // End namespace ROMon
#endif /* ROMON_SUBFARMDISPLAY_H */

