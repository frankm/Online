#include <smiuirtl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include "smixx_parstring_util.h"

/*=========================================================================================*/
void print_help()
{

        printf("Usage : \n");
	printf("        smiChangeOption <domain> <option-id> <value>\n");
	return;
}
/*=========================================================================================*/
int main(argc,argv)
int argc;
char **argv;
{
	char *domain = 0, *option = 0, *value = 0;

	if ( argc != 4 ) 
	{
		print_help(); exit(1);
	}

	domain = argv[1]; option = argv[2]; value = argv[3];
// printf( " domain = %s  option = %s  value = %s \n",domain,option,value);

	smiui_change_option(domain,option,value);
	
/*	char optString[516];
	smiui_get_options(domain,optString);
	printf( " options: %s\n",optString);*/

	usleep(500000);

	exit(0);
}

