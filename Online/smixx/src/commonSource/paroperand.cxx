// paroperand.cxx: implementation of the ParOperand class.
//
//                                                B. Franek
//                                              January 2012
// Copyright Information:
//      Copyright (C) 1999/2008 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <cstring>
#include "utilities.hxx"
#include "paroperand.hxx"
#include "errorwarning.hxx"
#include "smlline.hxx"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ParOperand::ParOperand()
{
	_operandType = "";
	_parmValueType = "";
	_cValue ="";
	_parName ="";
	_ownerObjName ="";
	_userRequestedType ="";
	
	_initNotFinished = -1;
	_operValueType = "";
	return;
}

int ParOperand::initFromSMLcode(SMLlineVector* pSMLcode
	                   , int& inext, int& jnext) 
{
	_initNotFinished = 0;

	SMLline lineBeingTranslated = (*pSMLcode)[inext];
// -- First make sure that all the leading spaces and tabs are skipped --
	int ist,jst,inbl,jnbl,dum1,dum2,dum3,dum4;
	ist = inext; jst = jnext;
	char firstNBLchr = firstNonBlank(pSMLcode,ist,jst,
	                                    inbl,jnbl,dum1,dum2,dum3,dum4);
	if ( firstNBLchr == '\0' )
	{  // end of the code before NBL char found
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"parsing Operand");
		
		cout << endl << " ParOperand::initFromSMLcode ..." << endl;
		cout << " Failed getting operand indiValue" << endl;
		throw FATAL;	 
	}
	inext = inbl; jnext = jnbl;
//--------------------------------------------------------------------------
	
	int idel,jdel; int ierr;
	Name form; 
	 char del; Name indiValue;
//cout << " Entering getIndiValue " << " inext jnext " 
//  << inext << " " << jnext << endl;
	lineBeingTranslated = (*pSMLcode)[inext];

// Here will come check for cast. The cast must be on the same line
// as the following name and immediately preceed it

	checkForCast(lineBeingTranslated,jnext,jnext);
	
//	lineBeingTranslated.indicateCharacter
//   (" First character for ParOperand::initialise..., before getIndiValue",
//               jnext); //debug
	del = getIndiValue(pSMLcode,inext,jnext," )="
	                  ,indiValue,form,ierr
		          ,idel,jdel,inext,jnext);
//cout << " delimiter : |" << del << "|  idel jdel inext jnext "
//<< idel << " " << jdel << " " << inext << " " << jnext << endl; 
	if ( ierr != 0 )
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"parsing Operand");
		
		cout << endl << " ParOperand::initFromSMLcode ..." << endl;
		cout << " Failed getting operand indiValue" << endl;
		throw FATAL;
	}

	if ( del == ')' ) 
	{
		inext = idel; jnext = jdel; // closing bracket has to be found
	}
	if ( del == '=' ) 
	{
		inext = idel; jnext = jdel; // same about the equal sign
	}

//cout << indiValue << form << endl;
	
	Name operandType = form;
	if ( operandType == "UNKNOWN" )
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"parsing Operand");
		
		cout << endl << " ParOperand::initFromSMLcode ..." << endl;
		cout << " Unrecognised operand" << endl;
		throw FATAL;
	}
	
	if ( operandType == "NAME" )
	{
		_operandType = operandType;
		_parName = indiValue;
		_ownerObjName = "THIS";
		
	}
		
	else if ( operandType == "COMPNAME" )
	{
		_operandType = operandType;
		char *pvalue = indiValue.getString();
		char *pdot, *pless, *pgt;
		if ( ( pdot = strrchr(pvalue,'.') ) )
		{
			*pdot = '\0';
			_ownerObjName = pvalue;
			*pdot ='.';
			_parName = (pdot+1);
		}
		else if ( ( pless = strrchr(pvalue,'<') ) )
		{
			*pless = '\0';
			_ownerObjName = pvalue;
			*pless = '<';
			pgt = strrchr(pvalue,'>');
			*pgt = '\0';
			_parName = (pless+1);
			*pgt = '>';
		}
	}

	else
	{  // this is constant value
		_operandType = "VALUE";
		_operValueType = operandType;
		_cValue = indiValue;
	}
	
	
	return 0;
}
//-----------------------------------------------------------------------------
void ParOperand::checkForCast(const SMLline& line, const int& jst, int& jnext)
{
// In the following we assume, that all the leading blanks were skipped

//	line.indicateCharacter(" First character for ParOperand::checkForCast",jst); //debug

	char* pointerToLineBody = line.getString();
	char* pointerTojstIn =&pointerToLineBody[jst];

	Name temp = pointerTojstIn;
	temp.upCase();  //convert it to uppercase
	char* pointerTojst = temp.getString();

     
	if (strncmp("(S)",pointerTojst,3) == 0) {_userRequestedType = "STRING"; jnext = jst+3; return;}
	if (strncmp("(STRING)",pointerTojst,8) == 0) {_userRequestedType = "STRING"; jnext = jst+8; return;} 

	if (strncmp("(I)",pointerTojst,3) == 0) {_userRequestedType = "INT"; jnext = jst+3; return;} 
	if (strncmp("(INT)",pointerTojst,5) == 0) {_userRequestedType = "INT"; jnext = jst+5; return;}
 
	if (strncmp("(F)",pointerTojst,3) == 0) {_userRequestedType = "FLOAT"; jnext = jst+3; return;} 
	if (strncmp("(FLOAT)",pointerTojst,7) == 0) {_userRequestedType = "FLOAT"; jnext = jst+7; return;}


	_userRequestedType = "\0";
	jnext = jst;
	return; 
}
//--------------------------------------------------------------------------
int ParOperand::initFromSobj_firstLine(char line[])
{
  if ( _initNotFinished == 1 )
    // The object is already being initialised, so this method should not be called
    {
		cout << " ***Error : Initialising from sobj ; Internal error " << endl;
		throw FATAL;					
    } 
// This is the first line of the SOBJ section containing the information about the operand
// This first line should be the operand type
      
	_initNotFinished = 1;
	
	_operandType = line;
	
	if ( _operandType == "VALUE" ||
	     _operandType == "NAME"  ||
	     _operandType == "COMPNAME" ) { return 1; }
	
	cout << " ***Error : Initialising from sobj ; Internal error " << endl;
	cout << "  Illegal operand type  " << _operandType << endl;
	throw FATAL;
	
}
//--------------------------------------------------------------------------
int ParOperand::initFromSobj_nextLine(char line[])
{
  if ( _initNotFinished != 1 )
    //    initFromSobj_firstLine   has to be called first. 
    {
		cout << " ***Error : Initialising from sobj ; Internal error " << endl;
		throw FATAL;					
    }       

	if ( _operandType == "VALUE" )
	{
		if ( _operValueType == "" ) { _operValueType = line; return 1;}
		if ( _cValue == "" )
		{	_cValue = line;
			_cValue_gen.init(_cValue.getString());
			_initNotFinished = 0;
			return 0;
		}
		cout << " ***Error : Initialising from sobj ; Internal error " << endl;
		throw FATAL;					
	}

	if ( _operandType == "NAME" )
	{
		if ( _operValueType == "" ) { _operValueType = line; return 1;}
		if ( _parName == "" )
		{	_parName = line;
			_initNotFinished = 0;
			return 0;
		}
		cout << " ***Error : Initialising from sobj ; Internal error " << endl;
		throw FATAL;					
	}

	if ( _operandType == "COMPNAME" )
	  {
	  	if ( _operValueType == "" ) { _operValueType = line; return 1;}
		if ( _ownerObjName == "" )
		{ 
			_ownerObjName = line;
			_ownerObjName_gen.init(line);
			 return 1;
		}
		if ( _parName == "" )
		{	_parName = line;
			_initNotFinished = 0;
			return 0;
		}
		cout << " ***Error : Initialising from sobj ; Internal error " << endl;
		throw FATAL;					
	}
	cout << " ***Error : Initialising from sobj ; Internal error " << endl;
	throw FATAL;
}
//---------------------  Destructor  BF Mar 2020  ----------------------------
ParOperand::~ParOperand()
{
	return;
}
//----------------------------------------------------------------------------------
void ParOperand::out(const Name offset) const
{
	char* poff = offset.getString();
	
	cout << endl << poff << " Operand type          : " << _operandType << endl;
	cout << poff         << " Operand requested type: " << _operValueType << endl;	
        cout << poff         << " Constant Value        : " << _cValue << endl;
		
        cout << poff         << " Parm Value Type       : " << _parmValueType << endl;
        cout << poff         << " Par Name              : " << _parName << endl;
        cout << poff         << " Owner Object          : " << _ownerObjName << endl;
        cout << poff         << " User cast request     : " << _userRequestedType << endl;


 
        return;
}
//----------------------------------------------------------------------
void ParOperand::outShort() const
{
	cout <<
	printingName() << " " << _operandType << " " << _operValueType << endl;
	return;
}
//======================================================================
void ParOperand::outSobj( ofstream& sobj ) const
{
	sobj << _operandType.getString()  << endl;
	
	if ( _operandType == "VALUE" )
	{
		sobj << _operValueType.getString() << endl;
		sobj << _cValue.getString() << endl;
		return;
	}
		
	Name operValueType;
	if ( _operValueType == "" )
	{
	// nobody managed to assign value to it, I am seting it to STRING
	// most likely FOR instruction
		operValueType = "STRING";
	}	
	else { operValueType = _operValueType; }
	
	if ( _operandType == "NAME" )
	{
		sobj << operValueType.getString() << endl;
		sobj << _parName.getString() << endl;
		return;
	}
	
	if ( _operandType == "COMPNAME" )
	{
		sobj << operValueType.getString() << endl;
		sobj << _ownerObjName.getString() << endl;
		sobj << _parName.getString() << endl;
		return;
	}

	return;
}
//======================================================================
Name& ParOperand::operandType() 
{
	return _operandType;
}
//======================================================================
Name& ParOperand::parmValueType() 
{
	return _parmValueType;
}
//======================================================================
Name& ParOperand::cValue() 
{
	return _cValue;
}
//======================================================================
Name& ParOperand::parName() 
{
	return _parName;
}
//======================================================================
Name& ParOperand::ownerObjName() 
{
	return _ownerObjName;
}
//======================================================================
Name& ParOperand::userRequestedType() 
{
	return _userRequestedType;
}
//=====================================================================
void ParOperand::eraseUserRequestedType() 
{
	_userRequestedType = "";
	return;
}
//======================================================================
void ParOperand::thisObjPar() 
{
	_operandType = "COMPNAME";
	_ownerObjName = "THIS";
	return ;
}
//======================================================================
void ParOperand::setParmValueType(Name& valueType)
{
	_parmValueType = valueType;
	return;
}
//======================================================================
Name ParOperand::operValueType()
{
	return _operValueType;
}
//=======================================================================
Name ParOperand::printingName(bool showThis) const
{
	Name temp;
		
	if ( _operandType == "VALUE" )
	{
		temp = _cValue;
	}
	else if ( _operandType == "NAME" )
	{
		temp = _parName;
	}
	else
	{ 
		if (  !(_ownerObjName == "THIS") || 
		       (_ownerObjName == "THIS" && showThis)  ) 		
		{
			temp = _ownerObjName;
			temp += ".";
		}
		temp += _parName;
	}
	
	return temp;
}
//=======================================================================
int ParOperand::setOperValueType(Name cast)
{
	if ( _operandType == "VALUE" )
	{
		cout <<
		 " ***Error : 'ParOperand::setOperValueType' Internal error " 
		<< endl;
		throw FATAL;	
	}
	
	if ( cast == "" )
	{
		_operValueType = _parmValueType;
		return 1;
	}
	
	if ( cast == "FLOAT" && _parmValueType == "STRING" )
	{
		return 0;  // casting from STRING to FLOAT not allowed
	}
	
	_operValueType = cast;
	return 1;
}
//=======================================================================
void ParOperand::replaceArgs(const NameVector& args)
{
	
//	cout << " ParOperand::replaceArgs called " << endl;
	int noArgs = args.length();
	
	Name arg("");
	char repStr[10];
	
	if ( _operandType == "COMPNAME" ) 
	{
		for ( int i=0; i<noArgs; i++ )
		{
			arg = args[i];
			if ( _ownerObjName == arg )
			{
				sprintf(repStr,"$(arg%d)",i+1);
				_ownerObjName = repStr;
				break;
			}
		}
		return;	
	}
	
	if ( _operandType == "NAME") {}
	else { return; }

	
	for ( int i=0; i<noArgs; i++ )
	{
		arg = args[i];
		if ( _parName == arg )
		{
			sprintf(repStr,"$(arg%d)",i+1);
			_operandType = "VALUE";
			_operValueType = "STRING";
			 _cValue = repStr;
			 _parName ="";
			break;
		}
	}
	return;
}
//==========================================================================
bool ParOperand::hasArgs()
{
	return _cValue_gen.argument() || _ownerObjName_gen.argument();
}
//===========================================================================
void ParOperand::setCurrentArgs(const NameVector& currArgs)
{
	ArgNameStatus_t err;

	if ( !hasArgs() ) return;   // does not have argument
	
	if (_ownerObjName_gen.argument())
	{
		Name objName = _ownerObjName_gen.name(currArgs,err);
		_ownerObjName = objName;
		return;
	}
		
	Name temp = _cValue_gen.name(currArgs,err);
	if ( temp[0] == '\"' ) { _cValue = temp; }
	else
	{
		_cValue = "\"";
		_cValue += temp;
		_cValue += "\"";
	}
	
	// *******check the error
	return;
}
