#include <string.h>
#include "varelementvector.hxx"

//--------------------------------------------------------------------
//                                                        July 2021
//                                                        B. Franek
//--------------------------------------------------------------------
VarElementVector::VarElementVector()
	: _vector()
{}
//--------------------------------
VarElementVector::VarElementVector(const VarElementVector& vec)
{
	_vector = vec._vector;
	return;
}
//-------------------------------------------------------
VarElementVector::~VarElementVector() {}
//-------------------------------------------------------
VarElement VarElementVector::operator [] (const int elem) const
{
	VarElement tempve = _vector[elem];
	return tempve;
}
//--------------------------------------------------------
VarElementVector& VarElementVector::operator += (const VarElement& velem)
{
	_vector.push_back(velem);
	return *this;
}
//-------------------------------------------------------
int VarElementVector::length() const
{
	return _vector.size();
}
//--------------------------------------------------------
bool VarElementVector::isPresent(const VarElement& velem) const
{
	int ln = _vector.size();
	
	for (int i=0; i<ln; i++)
	{
		if ( _vector[i] == velem ) { return true; }
	}
	
	return false;
}
//---------------------------------------------------------
void VarElementVector::out(const Name offset) const
{
	int ln = _vector.size();
	cout << endl;
	for (int i=0; i<ln; i++)
	{	
		cout << offset << _vector[i].outString() << endl;
	}
	
	return;
}
//------------------------------------------------------------
bool VarElementVector::isNamePresent(const Name& name, Parms* pPar) const
{
	int ln = _vector.size();
	
	for (int i=0; i<ln; i++)
	{
		if (_vector[i].actualName(pPar) == name) { return true; }
	}
	
	return false;
}
