#include <string.h>
#include "varelement.hxx"
#include "utilities.hxx"
#include "parms.hxx"

//--------------------------------------------------------------------
//                                                        March 2021
//                                                        B. Franek
//--------------------------------------------------------------------
VarElement::VarElement()
	: _name(""), _parName("")
{}
//--------------------------------
VarElement::VarElement(const VarElement &velement)
{
	_name = velement._name;
	_parName = velement._parName;
	return;
}
//--------------------------------
VarElement::VarElement(const Name &elemId)
	: _name(""), _parName("")
{
// elemId is either   name or $(parName) or &VAL_OF_parName
	if ( elemId == "" ) return;

	Name tempId(elemId);
	tempId.upCase(); tempId.trim();
	
	if ( tempId[0] == '$' )
	{
		if ( tempId[1] == '(' && tempId.lastChar() == ')' )
		{
			tempId.removeLastChar();
			_name = "";
			_parName = tempId.subString(2,-1);

		}
		else {
			_name = tempId;
			_parName = "";
		}
	}
	else if ( tempId.subString(0,8) == "&VAL_OF_" )
	{
		_name = "";
		_parName = tempId.subString(8,-1);
	
	}
	else 
	{
		_name = tempId;
		_parName = "";
	}

	if ( (_name == ""  && !check_name(_parName)) ||
	     (_parName == "" && !check_name(_name) ))
	{
		cout << " ****** warning  VarElement  "
		 << elemId << "  is not name " << endl;
	}
	return;
}
//--------------------------------------------------------
VarElement::VarElement(const char id[] )
	: _name(""), _parName("")
{
	Name temp(id);
	*this = temp;
	return;
}

//---------------------------------
VarElement::~VarElement() {}
//--------------------------------------------------------------
VarElement& VarElement::operator=(const VarElement& velem)
{
	_name = velem._name;
	_parName = velem._parName;

	return *this ;
}
//--------------------------------------------------------------
VarElement& VarElement::operator=(const Name& id)
{
	VarElement temp(id);
	*this = temp;
	return *this ;
}
//--------------------------------------------------------------
VarElement& VarElement::operator=(const char id[] )
{
	Name temp = id;
	*this = temp;
	return *this;
}
//---------------------------------------------------------------
bool VarElement::operator ==(const VarElement& velem) const
{
	if ( _name == velem._name &&
	     _parName == velem._parName ) return true;
		 
	return false;
}
//---------------------------------------------------------------
bool VarElement::operator !=(const VarElement& velem) const
{
	if ( _name == velem._name &&
	     _parName == velem._parName ) return false;
		 
	return true;
}
//---------------------------------------------------------------
bool VarElement::operator ==(const char elemId[] ) const
{
	VarElement temp(elemId);
	if ( *this == temp ) return true;
	return false;
}
//---------------------------------------------------------------
Name VarElement::name() const
{
	return _name;
}
//---------------------------------------------------------------
Name VarElement::parName() const
{
	return _parName;
}
//------------------------------------------------------------
Name VarElement::actualName(Parms* pActPars) const
{
	if ( !(_name == "" ) ) return _name;
	
	Name parVal("");
	
	if (pActPars) { parVal = pActPars->getParCurrValue(_parName); }
	else { parVal = paramcons::notfound; }
	if( parVal == paramcons::notfound ) {
		cout << " *** VarElement::actualName(...) "
		<< "No value for parameter " << _parName << endl;
		return parVal;
	}
	
	parVal.upCase(); parVal.trim();
	
// string parameters have double quotes around them, we have to get rid of them
	if ( parVal[0] != '\"' || parVal.lastChar() != '\"' )
	{
		cout << " *** VarElement::actualName(...) "
		<< " value for parameter " << _parName << " is not string " << endl;
		return parVal;
	}
		
	parVal = parVal.subString(1,-1); //removes the first char
	parVal.removeLastChar();
		
	return parVal;
}
//------------------------------------------------------------
bool VarElement::actualNameAccessible(Parms* pActPars) const
{
	if ( !(_name == "" ) ) return true;

	Name parVal("");
	
	if (pActPars) { parVal = pActPars->getParCurrValue(_parName); }
	else { parVal = paramcons::notfound; }
	if( parVal == paramcons::notfound ) {
		cout << " *** VarElement::actualName(...) "
		<< "No value for parameter " << _parName << endl;
		return false;
	}
	return true;
}
//------------------------------------------------------------
bool VarElement::paramAccessible(Parms* pActPars) const
{
	if ( !(_name == "" ) ) return false;
	
	Name parVal("");
	
	if (pActPars) { parVal = pActPars->getParCurrValue(_parName); }
	else { parVal = paramcons::notfound; }
	if( parVal == paramcons::notfound ) {
		cout << " *** VarElement::paramAccessible(...) "
		<< " parameter " << _parName << " not found among action parameters"
		<< endl;
		return false;
	}
	return true;
}
//---------------------------------------------------------------
Name VarElement::outString() const
{
	Name outStr("");

	if ( _name == "" )
	{
		outStr = "$(";
		outStr += _parName;
		outStr += ")";
	}
	else
	{
		outStr = _name;
	}
	return outStr;
}
//-------------------------------------------------------------
void VarElement::out(const char indent[]) const
{
	cout << indent << outString() << endl;
	return;
}
//-------------------------------------------------------------
Name VarElement::stringForSobj() const
{
	Name outStr("");
	if ( _name == "" )
	{
		outStr = "&VAL_OF_";
		outStr += _parName;
	}
	else { outStr = _name; }
	
	return outStr;
}
