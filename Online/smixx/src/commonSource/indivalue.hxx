//--------------------------  IndiValue  class  -----------------------
#ifndef INDIVALUE_HH
#define INDIVALUE_HH


/** @class IndiValue
  This class handles indirect values in SML constructs
  Indirect value is either
      constant i.e. string, int or float  (e.g. "ABC", 7, 78.9)
   or  name (e.g. TYPE) in which case the value is meant to be taken from
                        the value of action or object parameter
   or  name1.name2  ( LHC.STATUS ) name1 is name of an object and name2
                     is name of one of its parameters. 
   
  @auther Boda Franek
  @date August 2021
*/
//--------------------------------------------------------------------
//                                                        August 2021
//                                                        B. Franek
//--------------------------------------------------------------------

#include "name.hxx"
#include "varelement.hxx"

class ObjectRegistrar;
class SMIObject;
class State;
class Action;
//-----------------------

class IndiValue
{
public :
	IndiValue();

	IndiValue(const IndiValue& ival);

	IndiValue(const Name& val);

	IndiValue(const char val[]);

	~IndiValue();

	IndiValue& operator=(const IndiValue& ival);	
	
	IndiValue& operator=(const Name& val);
	
	IndiValue& operator=(const char val[] );

	
	bool operator == (const IndiValue& ival) const;
	
	bool operator == (const char val[] ) const;
	
	bool operator == (const Name& val) const;
	
	bool operator != (const IndiValue& ival) const;
			
	Name form() const;
	
	Name constant() const;

	Name parName() const;

	Name objName() const;

	Name objParName() const;
	
	bool isConstant() const;	
	
/**
   true when it is COMPNAME and object name is $(par) type
*/
	bool hasParametricObjName() const;
			
	Name outString() const;
	
	void out(const char indent[] = " ") const;
	
	Name stringForSobj() const;	
/**
 Used only by State Manager
*/	
	Name actualValue( ObjectRegistrar& objects,
	                  SMIObject* pParentObject,
                      State* pParentState,
                      Action* pParentAction,
                      Name& actualType, int& err) const;
/**
  Used only by Translator. When actualType = "", it means it is unavailable
*/	
	bool actualValueAccessible( ObjectRegistrar& objects,
                                SMIObject* pParentObject,
                                State* pParentState,
                                Action* pParentAction,
                                Name& actualType) const;
private :
/**
   does the same as 'actualValue' when _form is "NAME"
*/	
	Name actualValue_NAME( ObjectRegistrar& objects,
	                  SMIObject* pParentObject,
                      State* pParentState,
                      Action* pParentAction,
                      Name& actualType, int& err) const;
/**
   does the same as 'actualValue' when _form is "COMPNAME"
   at stage when the actual object name is known
*/	
	Name actualValue_COMPNAME_ObjKnown( ObjectRegistrar& objects,
	                  SMIObject* pParentObject,
                      State* pParentState,
                      Action* pParentAction,
                      Name othObjName,
                      Name& actualType, int& err) const;
//-------  private data ------------------------------------

	Name _form;  // STRING, INT, FLOAT, NAME or COMPNAME

	Name _constant; 

	Name _parName;

//	Name _objName;
	VarElement _objId;
	
	Name _objParName;

};

#endif
