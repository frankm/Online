//--------------------------------------------------------------------
//                                                        August 2021
//                                                        B. Franek
//--------------------------------------------------------------------
#include "indivalue.hxx"
#include "name.hxx"
#include "utilities.hxx"

#include "param.hxx"
#include "objectregistrar.hxx"
#include "smiobject.hxx"
#include "state.hxx"
#include "action.hxx"
#include "utilities.hxx"
#include "errorwarning.hxx"
#include "reservednames.hxx"


IndiValue::IndiValue()
: _form(""), _constant(""), _parName(""), _objId(""), _objParName("")
{}
//----------------------
IndiValue::IndiValue(const IndiValue& ival)
: _form(ival._form),
  _constant(ival._constant),
  _parName(ival._parName),
  _objId(ival._objId),
  _objParName(ival._objParName)
{}
//---------------------------------------------
IndiValue::IndiValue(const Name& valin)
: _form(""), _constant(""), _parName(""), _objId(""), _objParName("")
{
	Name val("");
	val = valin;
	Name objId("");
	
	val.trim();
	if (check_string(val))			
		{_form="STRING"; _constant = val;}
	else if (check_int(val))		
		{_form="INT"; _constant = val;}
	else if (check_float(val))		
		{_form="FLOAT";  _constant = val;}
	else if (check_name(val))		
		{_form="NAME";  _parName = val;}
	else if (check_compname(val))	
		{
			_form="COMPNAME";
			check_compname(val,objId,_objParName);
			objId.upCase(); _objParName.upCase();
			_objId = objId;
		}
	else
		{_form="UNKNOWN";}
		
	return;

}
//----------------------------------
IndiValue::IndiValue(const char val[])
{
	Name tempNm(""); tempNm = val;
	IndiValue tempiv(tempNm);
	this->_form = tempiv._form;
	this->_constant = tempiv._constant;
	this->_parName = tempiv._parName;
	this->_objId = tempiv._objId;
	this->_objParName = tempiv._objParName;
}
//-----------------------------------
IndiValue::~IndiValue() {}
//-----------------------------------------------------------------------
IndiValue& IndiValue::operator=(const IndiValue& ival)
{
	this->_form = ival._form;
	this->_constant = ival._constant;
	this->_parName = ival._parName;
	this->_objId = ival._objId;
	this->_objParName = ival._objParName;

	return *this;
}
//-------------------------------------------------------------------
IndiValue& IndiValue::operator=(const Name& val)
{
	IndiValue tempiv(val);
	this->_form = tempiv._form;
	this->_constant = tempiv._constant;
	this->_parName = tempiv._parName;
	this->_objId = tempiv._objId;
	this->_objParName = tempiv._objParName;
	
	return *this;	
}
//-------------------------------------------------------------------
IndiValue& IndiValue::operator=(const char val[])
{
	IndiValue tempiv(val);
	this->_form = tempiv._form;
	this->_constant = tempiv._constant;
	this->_parName = tempiv._parName;
	this->_objId = tempiv._objId;
	this->_objParName = tempiv._objParName;
	
	return *this;	
}
//----------------------------------------------------------------
bool IndiValue::operator == (const IndiValue& ival) const
{
	if (this->_form != ival._form) return false;
	if (this->_constant != ival._constant) return false;
	if (this->_parName != ival._parName) return false;
	if( this->_objId != ival._objId) return false;
	if (this->_objParName != ival._objParName) return false;
	return true;
}
//----------------------------------------------------------------
bool IndiValue::operator == (const char val[]) const
{
	IndiValue ival(val);
	if (this->_form != ival._form) return false;
	if (this->_constant != ival._constant) return false;
	if (this->_parName != ival._parName) return false;
	if( this->_objId != ival._objId) return false;
	if (this->_objParName != ival._objParName) return false;
	return true;
}	
//----------------------------------------------------------------
bool IndiValue::operator == (const Name& val) const
{
	IndiValue ival(val);
	if ( *this == ival ) return true;
	return false;
}
//----------------------------------------------------------------
bool IndiValue::operator != (const IndiValue& ival) const
{
	if ( *this == ival ) return false;
	return true;
}	
//-------------------------------------------------------------
Name IndiValue::form() const {return _form;}
//-------------------------------------------------------------
Name IndiValue::constant() const {return _constant;}
//-------------------------------------------------------------
Name IndiValue::parName() const {return _parName;}
//-------------------------------------------------------------
Name IndiValue::objName() const {return _objId.outString();}
//-------------------------------------------------------------
Name IndiValue::objParName() const {return _objParName;}
//------------------------------------------------------------
bool IndiValue::isConstant() const
{
	if ( _constant != "" ) return true;
	return false;
}
//----------------------------------------------------------
bool IndiValue::hasParametricObjName() const
{
	if ( _form != "COMPNAME" ) return false;
	if ( _objId.name() == "" ) return true;
	return false;
}
//-----------------------------------------------------------
Name IndiValue::outString() const
{
	if (_constant != "" ) return _constant;
	if (_parName != "" ) return _parName;
	if (_objId != "" )
	{
		Name temp("");
		temp = _objId.outString();
		temp += ".";
		temp += _objParName;
		return temp;
	}
	return "";
}
//---------------------------------------------------------
void IndiValue::out(const char indent[]) const
{
	cout << indent << outString() << endl;
	return;
}
//---------------------------------------------------------
Name IndiValue::stringForSobj() const
{
	return outString();
}
//--------------------------------------------------------
Name IndiValue::actualValue( ObjectRegistrar& objects,
	                  SMIObject* pParentObject,
                      State* pParentState,
                      Action* pParentAction,
					  Name& actualType, int& err) const
{
	err = 0;
	if (_constant != "") { actualType = _form; return _constant;}
	
	Name value("");
	
	if ( _form == "NAME" )  
	{ 
		value = actualValue_NAME(objects,
	                  pParentObject,
                      pParentState,
                      pParentAction,
                      actualType, err);
		return value;
	}
	//---------------------------------
	else if ( _form == "COMPNAME" )
	{
		Parms* pPars = pParentAction->pActionParameters();
		Name actualObjectName = _objId.actualName(pPars);

		value =  actualValue_COMPNAME_ObjKnown( objects,
	                  pParentObject,
                      pParentState,
                      pParentAction,
                      actualObjectName,
                      actualType, err);
	}
	else
	{
		cout << " !!!! internal error  "
		<< "actualIndiValue  " << endl
		<< "   indiValue : " << outString() << endl;
		throw FATAL;
	}		

	return value;
}
//-------------------------------------------------------------------------
bool IndiValue::actualValueAccessible( ObjectRegistrar& objects,
                            SMIObject* pParentObject,
                            State* pParentState,
                            Action* pParentAction,
                            Name& actualType) const
{
	if (_constant != "") { actualType = _form; return true;}
	
	if ( _form == "NAME" )  
	{ 
		int err = 0;
		Name value = actualValue_NAME( objects,
	                  pParentObject,
                      pParentState,
                      pParentAction,
                      actualType, err);
		if ( err == 0 ) return true;
		return false;
	}
	else if ( _form == "COMPNAME" )
	{
		Parms* pPars = pParentAction->pActionParameters();
		if ( !_objId.actualNameAccessible(pPars) ) { return false; }
		if ( _objId.name() == "" ) {actualType = ""; return true;} 
		
		Name actualObjectName = _objId.name();

		int err = 0;
		Name value = actualValue_COMPNAME_ObjKnown( objects,
	                  pParentObject,
                      pParentState,
                      pParentAction,
                      actualObjectName,
                      actualType, err);
		if ( err == 0 ) return true;
		return false;
	}
	else
	{
		cout << " !!!! internal error  "
		<< "actualIndiValue  " << endl
		<< "   indiValue : " << outString() << endl;
		throw FATAL;
	}
	return false;		
}
//-------------------------------------------------------------------------
Name IndiValue::actualValue_NAME( ObjectRegistrar& objects,
	                  SMIObject* pParentObject,
                      State* pParentState,
                      Action* pParentAction,
                      Name& actualType, int& err) const
{

	if ( _form == "" )
	       {objects.gimePointer("NONSENCE");}  // removed compiler warning

	err = 0;
	Name value("");
	
	if ( _form != "NAME" )
	{ cout << " ***** Internal Error: The method called with form = " << _form
		<< endl;  
		err = 1;
		return "&INTERNALERROR";
	} 
		// first check if it is reserved name
	Name resName = _parName;
	if (ReservedNames::isReserved(resName))
	{
		Name resType;
		if (ReservedNames::getType(resName,
		                           pParentObject,
		                           pParentState,
		                           pParentAction,
		                           resType)
		    )
		{
			actualType = resType;
			Name currVal("");
			if (ReservedNames::getValue(resName,
			                            pParentObject,
			                            pParentState,
			                            pParentAction,
			                            currVal)
			) {}
				
			if ( resType == "STRING" )
			{				
				Name tmp = "\""; tmp += currVal; tmp += "\"";
				currVal = tmp; 
			}
			value = currVal;
			return value;
		}
		else 
		{
			err = 1;
			return "&RESNAMENOACCESS";
		}
	}
		// not reserved name and so the parameter takes value either from local action
		// or object parameter
		// Let's check that it exists
	Name locParName = _parName; Name actParType(""), objParType("");
	actParType = paramcons::notfound; objParType = paramcons::notfound;
		
	if ( pParentAction != NULL )
	{
		actParType = 
		(pParentAction->pActionParameters())->getParDeclType(locParName);
	}
	if ( pParentObject != NULL )
	{
		objParType = 
		(pParentObject->pObjectParameters())->getParDeclType(locParName);
	}
	if      (!(actParType == paramcons::notfound)) 
	{
		actualType = actParType;
		Name actParValue = 
		(pParentAction->pActionParameters())->getParCurrValue(locParName);
		value = actParValue;			
		return value;
	}
	else if (!(objParType == paramcons::notfound))
	{
		actualType = objParType;
		Name objParValue = 
		(pParentObject->pObjectParameters())->getParCurrValue(locParName);
		value = objParValue;			
		return value;
	}
	else { err = 1; return "&NOTDECLARED"; }
	

}
//--------------------------------------------------------------------------
Name IndiValue::actualValue_COMPNAME_ObjKnown( ObjectRegistrar& objects,
	                  SMIObject* pParentObject,
                      State* pParentState,
                      Action* pParentAction,
                      Name objectName,
                      Name& actualType, int& err) const
{
	if (pParentState == 0) {}    // removing compiler warning
	if (pParentAction == 0) {}   // removing compiler warning
	
	
	err = 0;

	Name value("");

    Name othObjName = objectName;       // the name of some other object
	SMIObject* pOthObj;                 // pointer to it
	Name othParName = _objParName;      //  the name of one of its parameters
	
	if ( othObjName == "THIS" ) { pOthObj = pParentObject; }
	else 			{ pOthObj = objects.gimePointer(othObjName); }
	if ( pOthObj == 0 ) { err = 1; return "&REMOBJNOTDECLARED"; }
	
	// the other object is declared
	// first check if its parameter name is reserved name
	if (ReservedNames::isReserved(othParName))
	{
		// in this case it can be only _STATE_ or _ACTION_
		
		if (othParName == "_STATE_" || othParName == "_ACTION_" ) {}
		else { err = 1; return "&RESNAMENOACCESS"; }
			 
		Name resType = "STRING";
		actualType = resType;
		Name currVal;
			
		if ( othParName == "_STATE_" ) 
		{currVal = pOthObj->objCurrState();}
		else
		{currVal = pOthObj->objCurrAction();}
			
		Name tmp = "\""; tmp += currVal; tmp += "\"";
		currVal = tmp; 
			
		value = currVal;
		return value;
	}
	
	// It is not reserved name so it has to be a name of one of the other
	// object parameters
	
	Name othObjParType =
	                (pOthObj->pObjectParameters())->getParDeclType(othParName);
	
	if ( othObjParType == paramcons::notfound ) 
	{ 
		err = 1; return "&REMPARNOTDECLARED";
	}
	actualType = othObjParType;
	
	Name othObjParValue =
	               (pOthObj->pObjectParameters())->getParCurrValue(othParName);
	
	value = othObjParValue;
	return value;
}
