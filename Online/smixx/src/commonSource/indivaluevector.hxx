//--------------------------  IndiValueVector  class  -----------------------
#ifndef INDIVALUEVECTOR_HH
#define INDIVALUEVECTOR_HH

/** @class IndiValueVector
  @author Boda Franek
  @date August 2021
*/
//--------------------------------------------------------------------
//                                                        August 2021
//                                                        B. Franek
//--------------------------------------------------------------------
#include <vector>
#include "name.hxx"
#include "indivalue.hxx"


class IndiValueVector
{
public :
	IndiValueVector();
	
	IndiValueVector(const IndiValueVector& vec);
	
	~IndiValueVector();
	
	IndiValue operator [] (const int elem) const;
	
	IndiValueVector& operator += (const IndiValue& velem);
	
	int length() const;
	
	void out(const Name offset="") const;
	
private :	
	
	std::vector<IndiValue> _vector;
	
}; 
#endif
