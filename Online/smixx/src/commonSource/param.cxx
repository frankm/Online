//----------------------  Class  Param  -----------------------------------
// Container class holding data for one parameter
//
//                                                      Author: Boda Franek
//                                                      Date : October 2020
//----------------------------------------------------------------------------
#include "param.hxx"
#include "utilities.hxx"

//--------------------------------------------------------------------------
Param::Param()
: _name(""), _indiValue(""), _type(""), _currValue("")
{ return; }
//--------------------------------------------------------------------
Param::~Param() {return;}
//--------------------------------------------------------------------------
Param::Param(const Name& name, const Name& indivalue, const Name& type )
: _name(name), _indiValue(indivalue), _type(type)
{
	_currValue = paramcons::noval;
	checkData();
	return;
}
//---------------------------------------------------------------------
void Param::init(const Name& name, const Name& indiValue, const Name& type )
{
	_name = name;
	_indiValue = indiValue;
	_type = type;
	_currValue = paramcons::noval;
	checkData();
	return;
}
//---------------------------------------------------------------------
Name Param::paramName() const { return _name; }
Name Param::paramDeclType() const { return _type; }
Name Param::paramIndiValue() const { return _indiValue.outString(); }
Name Param::paramCurrValue() const 
{ 
	if ( _type == paramcons::undef )
	{
		cout << " Error : paramCurrValue() method not intended for" <<
		" DO/CALL type parameters." << endl <<
		" Use paramValueAndType(...) instead" << endl; 
	}
	return _currValue;
}
//--------------------------------------------------------------------
Name Param::paramValueAndType(ObjectRegistrar& objects,
                         SMIObject* pParentObject,
                            State*  pParentState,
                            Action* pParentAction,
                            Name& actualType, int& err) const
{
	err = 0;
	
	if ( _type != paramcons::undef )
	{ // this is declaration parameter
		actualType = _type;
		return _currValue;
	}
	
	// do/call type of parameter 
	if ( _indiValue.isConstant() )
	{
		actualType = _indiValue.form();
		return _indiValue.outString();
	}
	
	actualType ="";	
	Name actualValue = _indiValue.actualValue(objects,
                                            pParentObject,
                                            pParentState,
                                            pParentAction,
                                            actualType,err);
	return actualValue;
}
//--------------------------------------------------------------------
bool Param::paramValueAccessible(ObjectRegistrar& objects,
                         SMIObject* pParentObject,
                            State*  pParentState,
                            Action* pParentAction,
                            Name& actualType) const
{
	if ( _type != paramcons::undef )
	{ // this is declaration parameter
		actualType = _type;
		return true;
	}
	
	// do/call type of parameter 
	if ( _indiValue.isConstant() )
	{
		actualType = _indiValue.form();
		return true;
	}
	
	actualType ="";	
	return _indiValue.actualValueAccessible(objects,
                                  pParentObject,
                                  pParentState,
                                  pParentAction,
                                  actualType);
}
//------------------------------------------------------------------------
void Param::setParamCurrValue(const Name& value)
{
	if ( _type == paramcons::undef )
	{
		cout << " Error : setParamCurrValue(..) is meaningless for DO/CALL" <<
		" type parameters" << endl;
		return;
	}
	
	Name newValueType = getGenValueType(value);
	
	if ( newValueType != _type )
	{
		cout << "  Error: setParamCurrValue(..) is supllying value " <<
		value << " incompatible with the parameter type " << _type << endl;
		return;
	}
	_currValue = value;
	return;
}
//----------------------------------------------------------------
Name Param::outString(int format) const
{
	Name temp("");
	
	if ( format == 1 )
	{
		temp += _name;
		if ( _indiValue != paramcons::noval )
		{	
			temp += " = "; temp += _indiValue.outString();
		}
		return temp;		
	}
	
	if ( _type == "INT" || _type == "FLOAT"  ) { temp += _type; temp += " ";}
	temp += _name;
	
	if ( format == 2 )
	{
		if ( _indiValue != paramcons::noval )
		{	
			temp += " = "; temp += _indiValue.outString();
		}
		return temp;	
	}
	
	if ( format == 3)
	{	
		temp += " ("; temp += _indiValue.outString(); temp += ")";
		
		if ( _currValue != paramcons::noval )
		{
			temp += " = "; temp += _currValue;
			return temp;
		}
	}
 
	return temp;
}
//---------------------------------------------------------------
Name Param::indiValueForm() const
{
	return getGenValueType(_indiValue.outString());
}
//-----------------------------------------------------------------
void Param::checkData()
{
	if (!check_name(_name)) 
	{
		cout << " Internal error : Parameter : |" << _name <<
		"| is not a name" << endl; // it will be thrown out other places
		return;
	} 
	
	if (_type != paramcons::undef)
//--------------------------------------
	{ // This is declarative parameter.
	  // it has to have a simple type present and defaultvalue (indiValue)
	  // if present has to have the same type.
		if ( _type == "STRING" || _type == "FLOAT" || _type == "INT" )
		{
			if ( _indiValue != paramcons::noval )
			{
				if ( _indiValue.form() == _type ) 
				{
					_currValue = _indiValue.outString();
					return;
				}
				cout << " Internal error : Parameter " << _name 
				<< " default value " << _indiValue.outString() 
				<< "  has incorrect type " << endl;
				return;
			}
		}
		else
		{
			cout << " Internal error : Parameter " << _name <<
			" Incorrect type |" << _type << "| specified" << endl;
			return;		
		}
	}
	else
//-------------------------------------------------
	{ // This is do/call type parameter 
	  // _indiValue must be present and of known form.

		if ( _indiValue.form() == "UNKNOWN" || _indiValue == paramcons::noval )
		{
			cout << " Internal error : indivalue : |" <<
			     _indiValue.outString() <<
			"| has incorect format" << endl; // it will be thrown out other places
			return;		
		}	  	
	}

	return;
}
