//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  March 2017
// Copyright Information:
//      Copyright (C) 1996-2017 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
#ifndef RESERVEDNAMES_HH
#define RESERVEDNAMES_HH

#include "name.hxx"

class SMIObject;
class State;
class Action;

/** @class ReservedNames
This class encapsulates functions that have todo with reserved names such
as _DOMAIN_
*/
class ReservedNames
{
public:
	static bool isReserved(Name&);
	static int getType(const Name&,
	                   const SMIObject* pParentObject,
			   const State* pParentState,
			   const Action* pParentAction,
			   Name& type);
	static int getValue(const Name&,
	                   SMIObject* pParentObject,
			   State* pParentState,
			   Action* pParentAction,
			   Name& value);


private:
	static const int num = 4;
	static const char* rnames[num];
};

#endif
