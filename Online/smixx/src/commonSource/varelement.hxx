//--------------------------  VarElement  class  -----------------------
#ifndef VARELEMENT_HH
#define VARELEMENT_HH

#include "name.hxx"
class Parms;

/** @class VarElement
  This class handles variable elements in SML constructs
  @auther Boda Franek
  @date March 2021
*/
//--------------------------------------------------------------------
//                                                        June 2018
//                                                        B. Franek
//--------------------------------------------------------------------

class VarElement
{
public :
	VarElement();
	
	VarElement(const VarElement& vel);
	
	VarElement(const Name& id);
	
	VarElement(const char id[] );
	
	~VarElement();
	
	VarElement& operator=(const VarElement& vel);	
	
	VarElement& operator=(const Name& id);
	
	VarElement& operator=(const char id[] );
	
	bool operator == (const VarElement& velem) const;
	
	bool operator != (const VarElement& velem) const;
		
	bool operator == (const char id[] ) const;

	Name name() const;

	Name parName() const;
/**
   if _name is "" then the name is picked up
              from the value of _parName action parameter
			  otherwise _name is returned.
*/	
	Name actualName(Parms* pActionParameters) const;
	
/**
	returns 'false' when _name is "" 
	and the relevent parameter is not found
*/
	bool actualNameAccessible(Parms* pActionParameters) const;
/**
	returns 'false' when _name is ""
	and the relevent parameter is not found. 
	Also returns false when _parName is ""
*/
	bool paramAccessible(Parms* pActionParameters) const;
		
	Name outString() const;
	
	void out(const char indent[] = " ") const;
	
	Name stringForSobj() const;
	
private :
	

	Name _name; // direct name
	
	Name _parName; // name of the action parameter whose value is the name
	
};


#endif
