#include <string.h>
#include "indivaluevector.hxx"

//--------------------------------------------------------------------
//                                                        August 2021
//                                                        B. Franek
//--------------------------------------------------------------------
IndiValueVector::IndiValueVector()
	: _vector()
{}
//--------------------------------
IndiValueVector::IndiValueVector(const IndiValueVector& vec)
{
	_vector = vec._vector;
	return;
}
//-------------------------------------------------------
IndiValueVector::~IndiValueVector() {}
//-------------------------------------------------------
IndiValue IndiValueVector::operator [] (const int elem) const
{
	IndiValue tempve = _vector[elem];
	return tempve;
}
//--------------------------------------------------------
IndiValueVector& IndiValueVector::operator += (const IndiValue& velem)
{
	_vector.push_back(velem);
	return *this;
}
//-------------------------------------------------------
int IndiValueVector::length() const
{
	return _vector.size();
}
//---------------------------------------------------------
void IndiValueVector::out(const Name offset) const
{
	int ln = _vector.size();
	cout << endl;
	for (int i=0; i<ln; i++)
	{	
		cout << offset << _vector[i].outString() << endl;
	}
	
	return;
}

