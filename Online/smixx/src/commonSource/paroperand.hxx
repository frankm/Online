
// paroperand.hxx: interface for the ParOperand class.
//
//                                                  B. Franek
//                                                January 2012
//
// Copyright Information:
//      Copyright (C) 1999-2012 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#ifndef PAROPERAND_HH
#define PAROPERAND_HH

#include "name.hxx"
#include "smllinevector.hxx"
#include "namevector.hxx"
#include "argname.hxx"

/** @class ParOperand
    Instantiations of this class keep information about :
    \verbatim
      a) SET instruction operands such as 'RUNNUMBER', 'N' and '1' in the
            SET instruction   SET RUNNUMBER = N + 1
	and
      b) Simple condition type 4 operands such as 'NAME' and '5' in the
             simple condition (NAME == 5) 
	     
   Operand  can be one of the 3 types:
       1) VALUE     - a simple value such as 3 (integer),
                     or 5.7 (floating point) or "COSMICS" (character string)
                     (NB:name CONSTANT instead VALUE would have been better choice)
       2) NAME      - name of a parameter (object or action) in the current object
       3) COMPNAME  - composite name of a parameter in another object expressed as
                                  'another-object-name.parameter-name'
    \endverbatim
    		  
    Class 'ParOperand' is used by both Translator and State Manager each using
    different initiliation method (see below)

    Translator, during the examination stage may change some of the private data.
    This is to do with casting. For this purpose Translator employs 'OepManager'
    class. It then exports a subset (see below) of private data via .sobj file
    to be used by State Manager

    After the initialisation State Manager can change _ownerObjName.
    
    NB: Given the fact that State Manager is using only subset of the private
         data it would be better to use inheritance and make the part used in SM
	 base class.
*/
class ParOperand
{
public:
	ParOperand();
/**
     used in Translator for initialisation from SML code
     @return status code
     @param pSMLcode pointer to SML code block of code
     @param inext the initialisation proceeds until it is 'satisfied'. It then
                  returns the next 'usable' position in the SML block
		  in inext (line number) and jnext (col) pair.
     @param jnext see inext
*/	
	int initFromSMLcode(SMLlineVector* pSMLcode
	                   , int& inext, int& jnext) ;

/**
    Used in State Manager for initialisation from sobj input .
    This method is calld for the first line of the SOBJ section
    containing the info about the operand and should be the operand type
    (VALUE, NAME ot COMPNAME)
    @return status code. Can only be 1.
    @param line[] points to the first line of the SOBJ section
*/			   
	int initFromSobj_firstLine(char line[]);

/**
    Used in State Manager for initialisation from sobj input.
    This method is called for the 2nd, 3rd etc lines of the SOBJ section
    containing the info about the operand.
    @retval <1> {means expecting another line to follow.}
    @retval <0> {means that the line presented is the end line
                 of the SOBJ section.}
    @param line[]  points to 2nd, 3rd... line of the SOBJ section
*/			   
	int initFromSobj_nextLine(char line[]);
			   
	virtual ~ParOperand();
	
/**  prints private data
*/
	void out(const Name offset = "") const;
	
/**
  Will print one line with 3 items.
  The first item is either constant value or the parameter reference.
  The second item is the operand type and the third is the value type
  requested from State Manager (_operValueType)\n
\verbatim
  E.G.    3            VALUE       INT
  or     RUNTYPE       NAME        STRING
  or     OBJ.RUNTYPE   COMPNAME    STRING
\endverbatim
*/
	void outShort() const;
	
/**
   Outputs into SOBJ file:
\verbatim
   VALUE
   INT
   3
or
   NAME
   STRING
   RUNTYPE
or
   COMPNAME
   STRING
   OBJ
   RUNTYPE
\endverbatim
*/
	void outSobj( ofstream& sobj ) const;


//    methods for retrieval of various private data

	Name& operandType();
	Name& parmValueType();
	Name& cValue();
	Name& parName();
	Name& ownerObjName();
	Name& userRequestedType();
	Name operValueType();
/**
    During examination stage, when it is found that user is attemting
    to cast constant value, the method is used to erase user's request.
*/	
	void eraseUserRequestedType();
/**
    During the examination stage when the types of parameter values
    are already known, it is used to set '_parmValueType'   
*/
	void setParmValueType(Name& valueType);
/**
     Will set '_operValueType' depending on the value of the arg 'cast'
     if 'cast' is "" then it will simply set it to the value of '_parmValueType'
     Otherwise it will set it to 'cast' if such a cast is permited.
     @retval <1> { success}
     @retval <0> {casting STRING to FLOAT not allowed, casting ignored}
*/
	int setOperValueType(Name cast);
/**
   Returns a string with either the value of the operand (VALUE), or
   name of the referenced parameter.
   This is usefull for printing for example error messages.
*/
	Name printingName(bool showThis = true) const;
	
/**
    this method will set owner object to be THIS. Used at the examination
    stage of Translator to limit the left side of SET instruction to 
    the current object. I.E. to prevent the object name as being interpreted
    as action parameter.
    It is also used in State Manager to speed things up.
*/
	void thisObjPar();
/**
   If _operandType is NAME, then
   the first name in the 'args' array that matches _parName will cause _parName 
   to be replaces by $(argi) where i is the position of the matching element in 
   'args' array.
*/
	void replaceArgs(const NameVector& args);

        bool hasArgs();  // used only by State Manager
	
	void setCurrentArgs(const NameVector& currArgs);  //used only by State Manager
			
protected :
/**
     Called from Translator during initialisation and determines
     if user is requesting cast in SML. It sets '_userRequestedType'
     accordingly.  
*/
	void checkForCast(const SMLline& line, const int& jst , int& jnext);


// -------------------   Private Data decribing the operand --------  
	  

	Name _operandType;  /**< Type of the operand.
	                   It can be either VALUE or NAME or COMPNAME.
             Depending on this type, its actual value is either saved
	     in _cValue  (when operand type is VALUE)
	  or is accessed through the parameter reference _parmName and
	       _ownerObjectName.*/
	
	Name _cValue;  /**<when operand type is VALUE it is set to the 
	               constant value of the operand. Otherwise set to "".
                       */

	    
	ArgName _cValue_gen;    // generalised constant value
                       
	Name _parName;  /**<
	                When the operand type is VALUE then:\n
	                _parName and _ownerObjName are set to ""
			
	                When the operand type is NAME then:\n
	    _parName is the name of a local parameter (either action or object)
	    and _ownerObjName is set to "".

	    
	                when the operand type is COMPNAME then:\n
	     _parName is the parameter name of the other object's parameter
	      and _ownerObjName is the other object's name */	


	Name _ownerObjName; /**< see _parName description*/
	
	ArgName _ownerObjName_gen;	

	Name _operValueType;  /**<For operand type VALUE it is set to
	                      the _cValue type at initialisation in
			      Translator.\n
                 For NAME and COMPNAME it is calculated at the examination
		 stage based on user requested casting and the value type
		 of the other operands in the SET instruction or simple
		 condition.\n
                 This is then the operand value type into which State
		 Manager will be requested to cast the parameter value. */


//========  Data below this line are not passed on to the State Manager =======

	Name _userRequestedType;  /**<
	     Value type requested by user in SML (user casting) */

	Name _parmValueType; /**<
    Value type of refered parameter found from SML at the examination stage
     of Translator */

	int _initNotFinished; /**<
        Used during initialisation in State Manager purely for internal purposes.
	The value 1 indicates that the initialisation that was started is still
	in progress */

};

#endif 
