//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  January 2018
// Copyright Information:
//      Copyright (C) 1996-2018 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------


#include "name.hxx"
#include "errorwarning.hxx"
#include "examination_stage.hxx"

bool ExaminationStage::_inProgress = false;
Name ExaminationStage::_status = "SUCCESS";


void ExaminationStage::start()
{
	_inProgress = true;
	return;
}

void ExaminationStage::stop()
{
	_inProgress = false;
	return;
}

int ExaminationStage::updExamStatus(const Name& currStat)
{
	if ( currStat == "SUCCESS" ) {}
	else if ( currStat == "WARNING" ) 
	{
		if (_status == "SUCCESS" ) { _status = currStat;}
	}
	else if ( currStat == "SEVERE WARNING" )
	{
		if (_status == "SUCCESS" ||
		    _status == "WARNING" ) { _status = currStat; } 
	}
	else if (currStat == "ERROR")
	{
		if (_status == "SUCCESS" ||
		    _status == "WARNING" ||
		    _status == "SEVERE WARNING") { _status = currStat; } 
	}
	else if (currStat == "FATAL")
	{
		if (_status == "SUCCESS" ||
		    _status == "WARNING" ||
		    _status == "SEVERE WARNING" ||
		    _status == "ERROR" ) { _status = currStat; } 
	}
	else
	{
		cout << "****************** unknown exam status : " << currStat
		<< endl;
		return 0;
	}

	return 1;
}

bool ExaminationStage::inProgress()
{
	return _inProgress;
}

Name ExaminationStage::examStatus()
{
	return _status;
}

ExitStatus ExaminationStage::convStatusToExitStatus()
{
	if ( _status == "SUCCESS" ) {return SUCCESS;}
	else if ( _status == "WARNING" ) {return WARNING;}
	else if ( _status == "SEVERE WARNING" ) {return SEVERE_WARNING;}
	else if (_status == "ERROR") {return ERROR;}
	else if (_status == "FATAL") {return FATAL;}
        cout << " ***** Illegal _status at Examination Stage " << _status <<             endl;
	return FATAL;
}
