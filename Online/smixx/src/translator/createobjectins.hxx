// createobjectins.hxx: interface for the CreateObjectIns class.
//
//                                                  B. Franek
//                                                 April 2010
//
//////////////////////////////////////////////////////////////////////
#ifndef CREATEOBJECTINS_HH
#define CREATEOBJECTINS_HH

#include "instruction.hxx"
#include "varelement.hxx"

class CreateObjectIns  : public Instruction 
{
public:
	CreateObjectIns();

	virtual ~CreateObjectIns();

	virtual void translate() ;

	void out(const Name offset) const;

	virtual void outSobj(ofstream& sobj) const;


protected :

	VarElement _objectId;
  
/**
	The class name to which created object belongs
*/
	Name _className;
  
};

#endif 
