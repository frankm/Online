// smlunit.cxx: implementation of the SMLUnit class.
//
//                                                B. Franek
//                                               2 July 1999
//////////////////////////////////////////////////////////////////////

#include "smixx_common.hxx"
#include "smlunit.hxx"
#include "assert.h"
#include "string.h"
#include "smlline.hxx"
#include "utilities.hxx"
#include <iomanip>
#include "smiobject.hxx"
#include "ut_tr.hxx"
#include "stdlib.h"
#include "errorwarning.hxx"

using std::cout;
using std::endl;
using std::setw;
using std::left;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SMLUnit::SMLUnit( const char* id, const int buf, const Name name, const Name type)
	: _id(id), _name(name), _type(type),
	  _firstColOfUnit(0), _internalSMLUnits(),
	  _pParentUnit(NULL)
{
    _pSMLcode = new SMLlineVector(buf);
     assert(_pSMLcode != 0); 
   
//	 cout << "SMLUnit constructor called" << endl;
}

SMLUnit::~SMLUnit()
{
    delete _pSMLcode;
}

void SMLUnit::setFirstCol(const int col)
{
	_firstColOfUnit = col;
	return;
}
int SMLUnit::acceptLine( const SMLline& line )
{
    *_pSMLcode += line;
//	cout << "acceptLine _id :" << _id << " _name : " << _name << endl;
//	cout << line << endl;
	return 0;
}
//----------------------------------------------------------------------------
int SMLUnit::acceptLines( const SMLlineVector& lines, const int ibeg, const int iend )
{
	int nend;
	int nelem = lines.length();
	if ( iend == -1 ) { nend = nelem -1; }
	else { nend = iend; }
	
	if ( ibeg < 0 || nend < ibeg || nend >= nelem )
	{
		cout << " **** Internal error SMLUnit::acceptLines " << endl;
		cout << " Vector length: " << nelem << " indexes: " << ibeg << " " << iend << endl;
		throw FATAL;
	}
	
	for ( int i=ibeg; i<= nend; i++ )
	{
		*_pSMLcode += lines[i];
	}  
	
	return 0;
}
//----------------------------  BF  August 2020  --------------------------
int SMLUnit::acceptOtherUnitCode( const SMLUnit& other) 
{
	*_pSMLcode = *(other._pSMLcode);
	return 0;
}
//----------------------------------------------------------------------------
void SMLUnit::out(const Name offSet) const
{
	Name nextOffset;
	nextOffset = offSet;

	nextOffset += "  |";


    char* ptn = offSet.getString();

    void* ptnvoid; SMLUnit* pUnit;
	int numOfUnits = _internalSMLUnits.length();
	cout << ptn 
		<< "----------------------------------------------"<< endl 
		<< ptn << 
		" SMLUnit : " << _id << " " << _name << "  No of internal units : " << numOfUnits << endl;
	
	ptn = offSet.getString();
	if ( numOfUnits == 0 ) {
    	_pSMLcode->out(ptn);
	}
	else {
		for ( int i=0; i<numOfUnits; i++ ) {
			cout << ptn << "  " << i+1 << endl;
			ptnvoid = _internalSMLUnits.gimePointer(i);
            pUnit = (SMLUnit*)ptnvoid;
	        pUnit->out(nextOffset);
		}
	}
}
//------------------------------------------------- BF April 2000 -----------------
//void SMLUnit::outSobj(ofstream& sobj) const
//{
//	return;
//}
//------------------------------------------------- BF April 1999 -------------
void SMLUnit::translateUnits() {
	
     int numOfUnits = _internalSMLUnits.length();

     void* ptnvoid; SMLUnit* pUnit;

     for ( int iu=0; iu<numOfUnits; iu++ ) {  //Translation loop
        ptnvoid = _internalSMLUnits.gimePointer(iu);
        pUnit = (SMLUnit*)ptnvoid;
//       cout << " Going to translate :" << endl;
//		pUnit->outCodeOnly(" ");
//cout << " Translating " << pUnit->unitId() << pUnit->unitName() << endl;
	    pUnit->translate();

	 }   // end of translation loop
}

//----------------------------------------------------------------

Name SMLUnit::unitId() const
{
	return _id;
}

//----------------------------------------------------------------

Name SMLUnit::unitName() const
{
	return _name;
}
//-----------------------------------------------------------------
Name SMLUnit::unitType() const
{
       return _type;
}
//-----------------------------------------------------------------

void SMLUnit::outCodeOnly(const Name offSet) const
{

	Name nextOffset;
	nextOffset = offSet;

	nextOffset += "  |";


    char* ptn = offSet.getString();

	int numOfUnits = _internalSMLUnits.length();
	cout << ptn 
		<< "----------------------------------------------"<< endl 
		<< ptn << 
		" SMLUnit : " << _id << " " << _name << "  No of internal units : " << numOfUnits << endl;
	
	ptn = offSet.getString();
	_pSMLcode->out(ptn);
}
//----------------------------------------------------------------
void SMLUnit::outInternalUnitsCodeOnly(const Name offset) const
{
	char* ptn = offset.getString();
	SMLUnit *pUnit;
	int noUnits = _internalSMLUnits.length();
	cout << endl
	     << ptn <<"   No. of units : " << noUnits
	     << endl;
	for (int iu=0; iu<noUnits; iu++ )
	{
		void* ptnvoid = _internalSMLUnits.gimePointer(iu);
		pUnit = (SMLUnit*)ptnvoid;
		cout << ptn << " == Unit : " << iu << endl;
		pUnit->outCodeOnly(offset);
	}
	return;
}
//----------------------------------------------------------------
SMLline SMLUnit::firstLineOfUnit() const
{
	return (*_pSMLcode)[0];
}
//---------------------------------------------------------------
int SMLUnit::nextChar(const int iline, const int ch,
				int& nextLine, int& nextCh) const
{
	int numOfLines = _pSMLcode->length();
	if ( iline <0 || iline >= numOfLines ) return 0;

	SMLline line = (*_pSMLcode)[iline];
	char* pLine = line.getString();
	int lineLength = strlen(pLine);

	if ( ch < 0 || ch >= lineLength ) {return 0;}

	nextCh = ch +1;

	if ( nextCh < lineLength ) {nextLine = iline; return 1;}

	nextCh = 0;

	for ( int i=iline+1; i < numOfLines; i++) {
		line = (*_pSMLcode)[i];
		pLine = line.getString();
		lineLength = strlen(pLine);
		if ( lineLength > 0 ) {nextLine = i; return 1;}
	}
	return 0;
}
//-----------------------------------------------------------------
void SMLUnit::setParentUnit(SMLUnit* pUn)
{
	_pParentUnit = pUn;
	return;
}
//--------------------------------------------------------------------------
void SMLUnit::ancestry( NameVector& ids, NameVector& names)
{
	SMLUnit* pUn;
	int i;
	
	pUn = _pParentUnit;
	
	for (pUn = _pParentUnit, i=0; pUn != NULL; pUn = pUn->_pParentUnit, i++)
	{
		ids += pUn->_id;
		names += pUn->_name;
	}
			
	return;
}
//------------------------------------------------- BF April 2013 -------------
int SMLUnit::examineUnits() {
//cout << endl << " SMLUnit::examineUnits()";	
	int numOfUnits = _internalSMLUnits.length();
//cout << " no. of units " << numOfUnits  << endl;
	void* ptnvoid; SMLUnit* pUnit;

	int anyErrors = 0;
	int iflg;
	
	for ( int iu=0; iu<numOfUnits; iu++ )
	{  //Examination loop
		ptnvoid = _internalSMLUnits.gimePointer(iu);
		pUnit = (SMLUnit*)ptnvoid;
//cout << " pUnit " << pUnit << endl;
//cout << " Examining internal unit " << pUnit->unitId() << pUnit->unitName() << endl;
		iflg = pUnit->examine();
		if (iflg != 0) anyErrors = 1;

	}   // end of examination loop
	return anyErrors;
}
//------------------------------------------------- BF April 2013 -------------
int SMLUnit::examine()
{
	int iflg;
	
//cout << left;
//cout << "SMLUnit::examine()  Unit : "
//<< setw(12) << unitId().getString()
//<< " Name : " << setw(20) << unitName().getString() << endl;
	
	iflg = examineUnits();	
	return iflg;
}

/*int SMLUnit::prevChar(const int line, const int ch, 
				int prevLine, int prevCh) const
{
	return 0;
}*/
//----------------------------------------------------------------------------
void SMLUnit::parents(Name& object, Name& state, Name& action)
{
	object = ""; state = ""; action = "";

	SMLUnit* pUn;
	Name id, name;
	
	for (pUn = this; pUn != NULL; pUn = pUn->_pParentUnit)
	{
		id = pUn->_id;
		name = pUn->_name;
		
		if ( id == "Object" )
		{
			object = name;
		}
		
		if ( id == "State" )
		{
			state = name;
		}
		
		if ( id == "Action" )
		{
			action = name;
		}

	}
	
	
	return;
}
//-------------------------------------------------------------------------
SMLUnit* SMLUnit::parentPointer(const Name& id)
{
	SMLUnit* pUn;
	
	for (pUn = this; pUn != NULL; pUn = pUn->_pParentUnit)
	{
		if( id == pUn->_id ) { return pUn; }
	}
	
	return NULL;
}
//-------------------------------------------------------------------------
void SMLUnit::printParents()
{
	Name object, state, action;
	
	char objectorclass[10]; Name temp;
	SMIObject* parentObject;
	
	parents(object,state,action);
	
	parentObject = (SMIObject*) _pParentUnit->parentPointer("Object"); 
	
	if ( parentObject->isClass() ) { temp = " Class : "; }
	else { temp = " Object : "; }
	
	strcpy (objectorclass,temp.getString());
	cout << objectorclass << object << "  State : " << state;
	if ( action == "" ) { cout << endl; }
	else { cout << "  Action : " << action << endl; }
	
	return;
}
//--------------------------------------------------------------------
void SMLUnit::printCode() const
{
	char temp[] = " ";
	_pSMLcode->out(temp);
}
//------------------------------------------------------------------------------
void SMLUnit::collectUnit(SMLUnit* pUnit, int istart,
                          std::vector<SMLlineType_t>& termTypes,   
                          int& nextUnitStart, SMLlineType_t& nextUnit)
{
// the method collects a unit of code from _pSMLcode vector and
// stores it in pUnit instantiation of SMLUnit class.

// it assumes that the unit of collected code begins with 
// line istart of _pSMLcode.
// the end of the unit Block is signalled by collecting a line which is of
// any of the types in 'termTypes' vector. 

// after having collected the unit, nextUnit is set to the line type
// that terminated it.
//                      nextUnitStart is set to the line number in _pSMLcode
//                      where that unit starts.
// if end of _pSMLcode was reached, nextUnit is set to END_LINE


	SMLline line; Name name;
	SMLlineType_t lineType;
	                                                              	
	int numOfLines = _pSMLcode->length();

//cout << " collectUnit numOfLines  istart " << numOfLines << " " 
//     << istart << endl;
	int nTerm = termTypes.size();

	nextUnitStart = -1;
	
	for (int i = istart; i<numOfLines; i++)
	{
		line = (*_pSMLcode)[i];	
		if ( i == istart )
		{
			pUnit->acceptLine(line);
			continue;
		}
			
		lineType = line.lineType(name);

		for ( int j = 0; j<nTerm; j++ )
		{
			if ( lineType == termTypes[j] )
			{
				nextUnitStart = i;
				nextUnit = lineType;
				return;
			}
		}
		pUnit->acceptLine(line);
	}

	nextUnit = END_LINE;

	return;
}
//-----------------------------------------------------------------------
void SMLUnit::printCodeAllIntUnits() const
{
	int numUnits = _internalSMLUnits.length();
	void* ptnvoid; SMLUnit* pU;

	cout << endl << " No. of internal units" << numUnits << endl;
	
	for ( int iu=0; iu<numUnits; iu++)
	{
		ptnvoid = _internalSMLUnits.gimePointer(iu);
		pU = (SMLUnit* )ptnvoid;
		cout << endl << "  Unit " << iu << endl;
		pU->printCode();
	}
	
	return;
}
