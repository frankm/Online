// ifUnit.cxx: implementation of the IfUnit class.
//
//                                                B. Franek
//                                             4 October 1999
//////////////////////////////////////////////////////////////////////
#include <assert.h>
#include <stdio.h>
#include "smlline.hxx"
#include "ifunit.hxx"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IfUnit::IfUnit( Action* pParentAction, InsList* pParent) : SMLUnit("ifunit",10) 
{
   _name = "ifunit";
    _pParentAction = pParentAction;
   _pParentList = pParent;
   return;
}
IfUnit::~IfUnit()
{
    delete _pSMLcode;
}
//----------------------------------  BF October 2019  ---------------------
void IfUnit::translate() {

//	cout << " Translating " << _id << _name << endl;
//    _pSMLcode->out();

// IfUnit has two internal blocks: IfUnitHeadBlock and InsBlock.
// we shall instantiate and register them first
	
	SMLUnit *pUnit; Name unitType;
	
	_pHeadBlock = new IfUnitHeadBlock();
	pUnit = _pHeadBlock;
	assert (pUnit != 0);				
	unitType = "Head block";
	_internalSMLUnits.addRegardless(unitType,pUnit);
	pUnit->setParentUnit(this);
	
	_pInsList = new InsList(_pParentAction, _pParentList);
	pUnit = _pInsList;
	assert (pUnit != 0);				
	unitType = "InsList";
	_internalSMLUnits.addRegardless(unitType,pUnit);
	pUnit->setParentUnit(this);
	
// Now we shall collect the Head first. The head is everything from the begining
// untill the first instruction

	Name instype;
	SMLline line;

	int numOfLines = _pSMLcode->length(); int begInsList = numOfLines;
	
	pUnit = _pHeadBlock;
	for (int i = 0; i<numOfLines; i++) {   //***** Beg of collection loop
		line = (*_pSMLcode)[i];

		if (i > 0)
		{
			bool insLine = line.instructionLine(instype);
			if (insLine )
			{
				begInsList = i; break;
			}
		}

		pUnit->acceptLine(line);
	}                              //****** end of collection loop

// Now collect InsList
	pUnit = _pInsList;
	for (int i = begInsList; i<numOfLines; i++)
	{
		line = (*_pSMLcode)[i];
		pUnit->acceptLine(line);
	}

	translateUnits();
}
//---------------------------------  BF  April 2018  -------------------
void IfUnit::outSobj( ofstream& sobj) const
{

	if (!_pHeadBlock->_else) {
		_pHeadBlock->_condition.outSobj(sobj);
	}
	else {
		sobj << "else" << endl;
		sobj << "    0" << "    0" << endl;
	}

	int id = _pInsList->id();
	int level = _pInsList->level();

	char tempch[80];

	sprintf(tempch,"%3d%3d",id,level);
	sobj << tempch << endl;

	return;
}
//-----------------------------------  BF  October 2019  ------------------
bool IfUnit::noInstructionsPresent() const
{
	if ( _pInsList->numOfInstructions() == 0 ) return true;
	return false;
}
