// swstay_in_state.cxx: implementation of the SWStay_in_State class.
//
//                                                B. Franek
//                                               July 2015
// Copyright Information:
//      Copyright (C) 1999/2015 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include "utilities.hxx"
#include "errorwarning.hxx"
#include "swstay_in_state.hxx"

SWStay_in_State::SWStay_in_State() : SMLUnit("SWStay_inState",1)
{
	return;
}
//----------------------------------------------------------------------
void SWStay_in_State::translate()
{

	Name token; int idel,jdel; int inext,jnext;
	
	SMLline lineBeingTranslated;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	int ist,jst;
	
	ist = 0; jst = _firstColOfUnit;
//cout << " Starting SWstay_in_state_translate() " << ist << " " << jst << endl;
//char temp[] = " "; _pSMLcode->out(temp);

	getNextToken(_pSMLcode,ist,jst," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();

	if ( token == "STAY_IN_STATE"){}
	else {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"expected STAY_IN_STATE");
		cout << " Found instead " << token << endl;
		throw FATAL;
	}
	
//	_actionNm = "&NULL"; _objectNm = "&THIS_OBJECT";

        if (inext>0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"Some crap follows STAY_IN_STATE");
		throw FATAL;
	}
	return;

}
//----------------------------------------------------------------------
int SWStay_in_State::examine()
{
//cout << " There is nothing to examine for STAY_IN_STATE" << endl;
	return 0;
}
//----------------------------------------------------------------------
void SWStay_in_State::outSobj(ofstream& sobj) const
{
// For reasons of backward compatibility, this response is
// simulated like if it was instruction
//     DO &NULL &THIS_OBJECT
//  should realy be changed sometime in the future.

	sobj << "do" << endl;

	sobj << "&NULL" << endl;

	int numpar = 0;

	sobj << "    " << numpar << endl;

	sobj << "&THIS_OBJECT" << endl;
}

