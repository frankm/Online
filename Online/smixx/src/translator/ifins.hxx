// ifins.hxx: interface for the IfIns class.
//
//                                                  B. Franek
//                                                30 September 1999
//
//////////////////////////////////////////////////////////////////////
#ifndef IFINS_HH
#define IFINS_HH

#include "instruction.hxx"
#include "attributes.hxx"
#include "inslist.hxx"
#include "action.hxx"

class IfIns  : public Instruction 
{
public:
	
	IfIns( Action* pParent, InsList* pParentList );

	virtual ~IfIns();

	virtual void translate() ;

	virtual void outSobj(ofstream& sobj) const;
/**
  will call 'replaceArgs' for all If Units
*/
	void replaceArgs(const NameVector& args);
	
/**
  If Instruction without any associated instructions
*/
	bool sterileInstruction();
	
protected :

	Action* _pParentAction;

	InsList* _pParentList;
};

#endif 
