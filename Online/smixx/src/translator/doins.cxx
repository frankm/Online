// doins.cxx: implementation of the DoIns class.
//
//                                                B. Franek
//                                           30 September 1999
// Copyright Information:
//      Copyright (C) 1999/2001 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "smlunit.hxx"
#include "smlline.hxx"
#include "name.hxx"
#include "utilities.hxx"
#include "doins.hxx"
#include "smiobject.hxx"
#include "smiobjectset.hxx"
#include "ut_tr.hxx"
#include "errorwarning.hxx"
#include "action.hxx"
#include "nmdptnr.hxx"

#include "registrar.hxx"

extern Registrar allObjects, allObjectSets, allClasses;

#include "objectregistrar.hxx"
extern ObjectRegistrar allSMIObjects;

#include "reservednames.hxx"
#include "param.hxx"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DoIns::DoIns():
	Instruction(),
	WhenResponse(),
	_actionNm(""),
	_parameters(),
	_objectId(""),
	_setId("")
{
   _name = "do";
//   cout << "Do constructor called" << endl;
   return;
}

DoIns::DoIns(const Name& objectName) :
	Instruction(),
	WhenResponse(),
	_actionNm(""),
	_parameters(),
	_objectId(objectName),
	_setId("")  
{
   _name = "do";
//   cout << "Do constructor called" << endl;
   return;
}

DoIns::~DoIns()
{
    delete _pSMLcode; // this should not be necessary, SMLUnit destructor will do it.
}
//-----------------------------------------------------------------------
void DoIns::translate() {

	Name token; int idel,jdel; int inext,jnext;
	
	SMLline lineBeingTranslated;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	int ist,jst;
	
	ist = 0; jst = _firstColOfUnit;
//cout << " Starting translating DO " << ist << " " << jst << endl;
//char temp[] = " "; _pSMLcode->out(temp);

	char del = getNextToken(_pSMLcode,ist,jst," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();

	if ( token == "DO"){}
	else {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"expected DO instruction not found");
		cout << " Found instead " << token << endl;
		throw FATAL;
	}

	ist = inext; jst = jnext;

	if (ist < 0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"  Nothing follows the keyword DO");
		throw FATAL;
	}
	
	lineBeingTranslated = (*_pSMLcode)[ist];
	del = getNextToken(_pSMLcode,ist,jst," (",_actionNm,idel,jdel,inext,jnext);
	_actionNm.upCase(); 
	if ( !check_name(_actionNm) ) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << " Action: " << _actionNm << " is not a name" << endl;
		throw FATAL;
	}

//cout << _actionNm << endl;

	if ( del == '(' ) {
		ist = idel, jst = jdel;
		_parameters.initFromSMLcode( 0, _pSMLcode, ist,jst,inext,jnext);
// If something goes wrong, Translator is terminated inside the method
	}

	if ( _objectId == "" ) {}
	else
	{  // object name has been supplied, this is the end of the instruction
	   // have to make sure that nothing else follows
		if ( inext < 0 )
		{  // This means that either 'getNextToken' or 'initFromSML'
		   //  determined that we are at the end of the code as we
		   // should be so everything is fine and can return 
		return;
		}
//  The following is necessary because the parameter method does not at the
//  find the next non-blank character so inext could be positive and so
// we have to do it on its behalf
		ist = inext; jst = jnext;
		int dum1,dum2,dum3,dum4;
		firstNonBlank(_pSMLcode,ist,jst,dum1,dum2,inext,jnext,dum3,dum4);

        	if (inext>=0) {
			ErrorWarning::printHead("ERROR",lineBeingTranslated
			,"Some crap follows DO instruction");
			throw FATAL;
		}
		return;
	}
		
// object name has not been supplied in the constructor, so it should follow
	ist = inext; jst = jnext;
	if (ist < 0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"DO instruction is incomplete");
		throw FATAL;
	}
	
	Name nextToken("");
	lineBeingTranslated = (*_pSMLcode)[ist];
	del = getNextToken(_pSMLcode,ist,jst," ",nextToken,idel,jdel,inext,jnext);
	nextToken.upCase();
	nextToken.trim();

	if (nextToken == "ALL_IN") {
		if (inext < 0) {
			ErrorWarning::printHead("ERROR",lineBeingTranslated
			,"DO instruction is incomplete");
			throw FATAL;
		}
		
		Name setid("");
		lineBeingTranslated = (*_pSMLcode)[inext];
		del = getNextToken(_pSMLcode,inext,jnext," ",
					setid,idel,jdel,inext,jnext);
		_setId = setid;
	}
	else
	{
		_objectId = nextToken;
	}
	
	if (inext>0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"Some crap follows DO instruction");
		throw FATAL;
	}
	return;
}
//---------------------------------------------------------------------------------
void DoIns::out(const Name offset = "") const
{
	SMLUnit::out(offset); 
	char* ptn=offset.getString(); cout << ptn ;
	if ( _setId == "" )
	{
		cout << "Action : "<< _actionNm << 
		"  Object : " << _objectId.outString() << endl;
	}
	else
	{
		cout << "Action : " << _actionNm << " ALL_IN :" <<
		 _setId.outString() << endl;
	}
	_parameters.outParms(offset);

  return;
}
//----------------------------------  BF April 2000  ------------------
void DoIns::outSobj(ofstream& sobj) const
{
	sobj << "do" << endl;

	sobj << _actionNm.getString() << endl;

	int numpar = _parameters.numOfEntries();

	sobj << "    " << numpar << endl;

	for (int ip=0; ip<numpar; ip++) {
		Name name, indiValue;
		name = _parameters.getParName(ip);
		indiValue = _parameters.getParIndiValue(ip);		
		sobj << name.getString() << endl;
		sobj << indiValue.getString() << endl;
	}

	if ( _setId == "" )
	{
		sobj << _objectId.stringForSobj() << endl;
	}
	else
	{
		sobj << "&ALL_IN_" << _setId.stringForSobj() << endl;
	}
	return;
}
//---------------------------------------------------------------------------
int DoIns::examine()
{
	int retcode = 0;
	int examineActionRetCode = 0; // return code returned from 'examineAction' 
	
	SMLline firstLine = (*_pSMLcode)[0];
	
/*	
//beg debug
  cout << endl 
  << " ====================== DoIns::examine() ============= " << endl;
	
  cout << "  Parent : " << _pParentUnit->unitId() 
  << "  " << _pParentUnit->unitName() << endl;
		
  cout << "     Complete Ancestry " << endl;
	
	int num;
	NameVector ids,names;
	
	_pParentUnit->ancestry(ids,names);
	
	num = ids.length();

	for (int i=0; i<num; i++)
	{
		cout << "     " << ids[i] << "  " << names[i] << endl;
	}
	
	cout << "Parent Unit Code " << endl;	

	char temp[] = " "; _pSMLcode->out(temp);	
//end debug
*/
//cout << " examining DO " << endl << firstLine << endl
//<< " Object " << _objectId.outString() << " Action " << _actionNm << endl;

	SMIObject* pParentObject = (SMIObject*)(_pParentUnit->parentPointer("Object")); 
	State* pParentState = (State*)(_pParentUnit->parentPointer("State")); 
	Action* pParentAction = (Action*)(_pParentUnit->parentPointer("Action")); 

	Name setname = _setId.name();
	Name setparname =_setId.parName();
	Name objname = _objectId.name();
	Name parname = _objectId.parName();
		
	if ( _setId == "" )
	{  // ordinary object. Check, that it is declared
		SMIObject* pObj;
		
		if (parname == "")
		{  // simple name, we can examine 
			
			pObj = gimeObjectPointer(objname); 
			if ( objname == "&THIS_OBJECT" ){pObj = pParentObject;}
		
			if ( pObj == 0 )
			{
				//	retcode = 1; review
				ErrorWarning::printHead("SEVERE WARNING",firstLine);
				cout << " Object " << objname <<
				" is not declared " << endl;
				cout << endl; _pParentUnit->printParents(); cout << endl;
			}
			else
			{
			// the object is declared. Check the action 
				examineActionRetCode = examineAction
				  	( pParentObject,pParentState,pParentAction,pObj);		
			}
		}
	}
	else
	{  // object set. Check, that it is declared
		if (setparname == "")
		{ // simple name so we can examine
		
			void* pvoid = allObjectSets.gimePointer(setname);
			if ( pvoid == 0 )
			{
			//	retcode = 1; review
				ErrorWarning::printHead("SEVERE WARNING",firstLine);
				cout << " Object Set " << setname <<
				" is not declared " << endl;
				cout << endl; _pParentUnit->printParents(); cout << endl;

			}
			else
			{
			// Set is declared. If it is associated with a class, we can
			// check the action.
				SMIObjectSet* pObjSet = (SMIObjectSet*)pvoid;
				Name setClass = pObjSet->setClass();
				if (setClass == "") {}
				else
				{
				// it is associated with class
					void* pvoid = allClasses.gimePointer(setClass);
					if ( pvoid == 0 ) {} // the class is not declared, will be dealt with elsewhere
					else
					{
						SMIObject* pObj = (SMIObject*)pvoid;
						examineActionRetCode = examineAction
				  	     (pParentObject,pParentState,pParentAction,pObj);
					
					}
				}
			}
		}
	}
	
		
	
	int iflg = examineUnits();
	
	return retcode + examineActionRetCode + iflg;
}
//-------------------------------------------------------------------------------------------------------
Name DoIns::outString() 
{
	Name temp;
	
	temp = "do ";
	temp += _actionNm;
	
	int numPars = _parameters.numOfEntries();
	
	if (numPars > 0) {temp += " (...) ";}
	else {temp += " ";}
	
	if ( _setId == "" ) { temp += _objectId.outString();}
	else { temp += "all_in "; temp += _setId.outString();}
	
	return temp;
}
//-------------------------------------------------------------------------
int DoIns::examineParameters
	   ( SMIObject* pParentObject, State* pParentState, Action* pParentAction,
	    SMIObject* pTargetObject)
{
	if (pParentState==NULL) {} // unused argument  pacifying compiler
// examines DO parameters from several different points of views. If everything is OK, it returns zero

	SMLline firstLine=firstLineOfUnit();
	int retCode = 0;
//cout << endl << firstLine << "   looking for candidate actions in " << pTargetObject->unitName() << endl;

	int noPars = _parameters.numOfEntries();
	if ( noPars == 0 ) return retCode; // no parameters - nothing to do
	 
//	Name temp = " "; _parameters.out(temp);

//-----------------------------------------------------------------------------------------	
// For DO parameters that take their values from local action or objects parameters, 
// check for those parameters existence is made. Also the value type of the DO parameter
// is taken to be that of the local action/object parameter

	Param param;
//	cout << endl << endl << firstLine << endl;
//	cout << " Parms before: " << endl; _parameters.out(" ");
		
	for ( int ip=0; ip<noPars; ip++ )   // DO parameters loop
	{
		param = _parameters.getParam(ip);
				
		Name value, valueType;

		if ( !param.paramValueAccessible
		            (allSMIObjects,
                      pParentObject,
                      pParentState,
                      pParentAction,
                      valueType) )
		{
			retCode = 1;
			ErrorWarning::printHead("SEVERE WARNING",firstLine);
			cout << " Parameter " << param.paramName() 
			 << " not accessible" << endl;
			cout << endl; _pParentUnit->printParents(); cout << endl;		
		}
	}
//	cout << " Parms after: " << endl; _parameters.out(" "); 
	if (retCode != 0) return retCode;
// ------------------------------------------------------------------------------------------	
// now the compatibility of DO parameters will be checked with those of the target action
//  First check what type of DO is it (WHEN or belonging to an ACTION)

	Name doParName, doParType, doParValue;
	Name actParType, objParType;
	
	if ( pParentAction == NULL )
	{   //  the DO belongs to WHEN. In this case the action is on the parent object 
	    //  in the parent state
	    	Name parentStateName = pParentState->unitName();
	    	Action* pAction = pParentState->gimeActionPointer(_actionNm);
		if ( pAction == NULL)
		{
			retCode = 1;
			ErrorWarning::printHead("SEVERE WARNING",firstLine);
			cout << " Undeclared action" << endl;
			
			cout << endl << " Action " << _actionNm 
			<< " is not declared in state " << parentStateName << endl; 
			cout << endl; _pParentUnit->printParents(); cout << endl;
		}
		int retComp = checkCompatibility(parentStateName,pAction);
		retCode = retCode + retComp;
		retCode = 0; // review
		return retCode;
	}

//---------------------------------------------------------------------------------------------------
//   DO belongs to an action. This is much more complicated because we do not know 
//  in what state the target object will be when the action reaches it.

// These checks are switched off when the target object is associated. The freedom
// has been allowed far too long and users got used to that.

	if ( pTargetObject->isAssociated() )
	{
	  retCode = 0; // review
	  return retCode;
	}
	
//  So firstly we will find if there are any candidate actions in any of the states
//  of the target object. At this stage we consider as the candidate any action
//  of the correct name that has at least the same number of parameters as our DO.
	
	NmdPtnrVector candidates; NmdPtnr item; Name trgStateNm;
	
	int numCandidates = pTargetObject->candidateActions(_actionNm,noPars,candidates);
	
// the candidate actions are returned in the Vector of named pointers.
// in every item the pointer is the pointer to the action and the name is the name of
// the state containing this action
	
//	cout << " Candidates " << numCandidates << endl;
//	candidates.out();

	if ( numCandidates == 0 )
	{
		retCode = 1;
		ErrorWarning::printHead("SEVERE WARNING",firstLine);
		cout << " No suitable action in the target object " << endl;
		cout << " There is no action in the target object having at least" << endl
		     << " the same number of parameters as the DO instruction" << endl;	
		cout << endl; _pParentUnit->printParents(); cout << endl;
	}

//-------------------------------------------------------------------------------------------------	
	Action* pTrgAct; Name offset(" "); 

// For every candidate target action, more thorough compatibility che ck with DO parameters
//  is made	
	for ( int ic=0; ic<numCandidates; ic++ )
	{
		item = candidates[ic];
		pTrgAct = (Action*)(item.pointer());
		trgStateNm = (Name)(item.name());
//		cout << endl << " Target action " << _actionNm << " in state " << trgStateNm << endl;

		retCode = retCode + checkCompatibility(trgStateNm, pTrgAct);
	}

	retCode = 0;  // review	
	return retCode;
}
//------------------------------------------------------------------------------------
int DoIns::checkCompatibility(const Name& trgObjState, Action* pTrgAction)
{
// Trg is short for Target

	SMLline firstLine = firstLineOfUnit();
	
	SMLline trgActionFirstLine = pTrgAction->firstLineOfUnit();
	
	Parms* pTrgParms = pTrgAction->pActionParameters();
	
	Name offset(" ");
	
//	cout << " Do parameters" << endl; _parameters.out(offset);
//	cout << endl << " Target Object Action parameters" << endl; pTrgParms->out(offset);
	
// will loop through DO action parameters and for every one it will check that
// there exists parameter in the target object action of the same name and of the correct type.

	Name doParName, doParType, doParValue;
	int idopar, iTrgPar;
	Name trgParType;

	
	SMIObject* parentObject = (SMIObject*) _pParentUnit->parentPointer("Object");
	State* parentState = (State*) _pParentUnit->parentPointer("State");  
	Action* parentAction = (Action*) _pParentUnit->parentPointer("Action");      

	int retcode = 0;

	for ( idopar = 0; idopar<_parameters.numOfEntries(); idopar++ )
	{
		Param doPar = _parameters.getParam(idopar);
		doParName = doPar.paramName();
		
		if ( !doPar.paramValueAccessible
		            (allSMIObjects,
                      parentObject,
                      parentState,
                      parentAction,
                      doParType) )
		{
		//	retcode = 1; review
			ErrorWarning::printHead("SEVERE WARNING",firstLine);
			cout << " Parameter " << doParName << "  error: " << doParValue
            << endl;
			cout << endl; _pParentUnit->printParents(); cout << endl;
			continue;		
		}

		iTrgPar = pTrgParms->getParIndex(doParName);
		if ( iTrgPar < 0 )
		{
		//	retcode = 1; review
			ErrorWarning::printHead("SEVERE WARNING",firstLine);
			cout << " Parameter " << doParName 
			    << "  not found among action parameters of target object" << endl;
			cout << " action on target object in state " << trgObjState 
			     << " does not have parameter of that name " << endl;
			trgActionFirstLine.out();
			cout << endl; _pParentUnit->printParents(); cout << endl;
			continue;		
		}
		else
		{
			if ( doParType == "" ) {}
			else
			{
				trgParType = pTrgParms->getParDeclType(iTrgPar);
				if ( trgParType == doParType ) {}
				else
				{
				//	retcode = 1; review
					ErrorWarning::printHead("SEVERE WARNING",firstLine);
					cout << " DO parameter has incompatible type" << endl;
					cout << " action on target object in state " << trgObjState 
			     		  << " has parameter " << doParName << " but of different type " << endl;
					cout << " Action par type : " << trgParType << endl
					     << " DO par type : " << doParType << endl << endl;
					trgActionFirstLine.out();
					cout << endl; _pParentUnit->printParents(); cout << endl;		
					continue;
				}
			}
		}
	}
	
//----------------------------
//  will loop through the parameters of the target object action and for those that do not have
// default value will check that DO supplies it.

	Name trgParName, trgParVal;
	
	for ( iTrgPar = 0; iTrgPar<pTrgParms->numOfEntries(); iTrgPar++ )	
	{
		trgParName = pTrgParms->getParName(iTrgPar);
		trgParVal = pTrgParms->getParIndiValue(iTrgPar);
		
		if ( trgParVal == paramcons::noval )
		{
			idopar = _parameters.getParIndex(trgParName);
			if ( idopar < 0 )
			{
			//	retcode = 1; review
				ErrorWarning::printHead("SEVERE WARNING",firstLine);
				cout << " Parameter missing" << endl;
				cout << " action on target object in state " << trgObjState 
		     		  << " needs parameter " << trgParName 
				  << " but DO does not supply it " << endl;
				trgActionFirstLine.out();
				cout << endl; _pParentUnit->printParents(); cout << endl;		
			}
			
		}
	} 
	
	return retcode;
}
//-------------------------------------------------------------------------------------------
int DoIns::examineAction( SMIObject* pParentObject, State* pParentState, Action* pParentAction,
	                 SMIObject* pTargetObject)
{
	SMLline firstLine = firstLineOfUnit();
	
	int retCode = 0;
	int examineParametersRetCode = 0;
	
	if ( pParentAction == NULL )
	{  // The DO belongs to WHEN. In this case, the action must be declared in parent state
		if (pParentState->hasAction(_actionNm))
		{
			examineParametersRetCode = examineParameters
				  (pParentObject,pParentState,pParentAction,pTargetObject);
		}
		else
		{
			retCode = 1;
			ErrorWarning::printHead("SEVERE WARNING",firstLine);
			cout << " Action " << _actionNm <<
			" is not declared in state " << pParentState->unitName()
			<< " of object " << pParentObject->unitName() << endl;
			cout << endl; _pParentUnit->printParents(); cout << endl;
		}
		
		retCode = retCode + examineParametersRetCode;
		retCode = 0;		
	
		return retCode;
	}

//    This DO is part of an action

		if (pTargetObject->hasAction(_actionNm))
		{
			examineParametersRetCode = examineParameters
				(pParentObject,pParentState,pParentAction,pTargetObject);
		}
		else
		{
			retCode = 1;
			ErrorWarning::printHead("SEVERE WARNING",firstLine);
			cout << " Action " << _actionNm <<
				" is not declared in object " << pTargetObject->unitName() << endl;
				cout << endl; _pParentUnit->printParents(); cout << endl;
		}

	retCode = retCode + examineParametersRetCode;
	retCode = 0;		
	
	return retCode;

}
