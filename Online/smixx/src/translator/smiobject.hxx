//-----------------------------------------------------------------------
//                         SMIObject  Class
//                                                 B. Franek
//                                                 21 March 1999
//-----------------------------------------------------------------------
//
#ifndef SMIOBJECT_HH
#define SMIOBJECT_HH
#include "smixx_common.hxx"
#include "smlunit.hxx"
#include "name.hxx"
#include "smllinevector.hxx"
#include "namevector.hxx"
#include "registrar.hxx"
#include "attributeblock.hxx"
#include "parameterblock.hxx"
#include "state.hxx"
#include "namevector.hxx"
#include "nmdptnrvector.hxx"


class  SMIObject : public SMLUnit {

	public :

		SMIObject 
			( const Name&, int cl, int assoc);

		virtual ~SMIObject();

		Name name() const;
		
		virtual void translate();

		virtual void outSobj(ofstream& sobj) const;
				       
		bool isClass() const;

		bool isAssociated() const;
						
/**    Returns the names of the Object's states and their number. If any of the
     states is undeclared, undeclared is 1, otherwise zero.
*/
		int getStates( NameVector& states, int& undeclared) const;
		
/**  Checks if object has state 'stateNm' declared. 
      If such state is declared ... returns true, otherwise false
      undeclared is set to 1 if objects has undeclared states, otherwise 0
*/
		bool hasState(const Name& stateNm, int& undeclared) const;
		
		bool hasAction(const Name& actionNm ) const;
		
		bool hasFunction(const Name& functionNm ) const;

		Action* gimePointerToFunction(const Name& funNm) const;
				
		int examine();
		
  		Name outString();
		
/** Will collect pointers to actions that satisfy the following criteria:
      - have name actionNm
      - have number of parameters greater or equal to noPars
      returns number of such actions
*/	
		int candidateActions
		      (const Name& actionNm, const int& noPars,
		       NmdPtnrVector& pActions);
		       
		Parms* pObjectParameters();
		
// This method is not used by Translator and is there only to enable some software
// to run from commonSource
		Name objCurrState() const;
// Same as above
		Name objCurrAction() const;
	
	protected :
	
		int _class, _associated;
		Name _isOfClass;

		AttributeBlock* _pAttributeBlock;

		ParameterBlock* _pParameterBlock;
		
		Registrar _functions;

		Registrar _states;
		
	private :
		
	void processAttributeBlock(int& istart, int& next_istart, sectionType_t& nextSection);
	
	void processParameters(int& istart, int& next_istart, sectionType_t& nextSection);
	
	void createCollectTranslateFunction
		        (int& istart, int& next_istart, SMLlineType_t& nextUnit);

	
	void processFunctions(int& istart, int& next_istart, sectionType_t& nextSection);
	
	void createCollectTranslateState
		        (int& istart, int& next_istart, SMLlineType_t& nextUnit);

	
	void processStates(int& istart, int& next_istart, sectionType_t& nextSection);


};
#endif
