// DestroyObjectins.cxx: implementation of the DestroyObjectIns class.
//
//                                                B. Franek
//                                               January 2020
//////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "smlunit.hxx"
#include "smlline.hxx"
#include "name.hxx"
#include "utilities.hxx"
#include "destroyobjectins.hxx"
#include "registrar.hxx"
#include "errorwarning.hxx"

	extern Registrar allUnits;
	extern Registrar allClasses;
	
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DestroyObjectIns::DestroyObjectIns()
: _objectId("")  
{
	_name = "DestroyObject";   // in SMLUnit class
   return;
}

DestroyObjectIns::~DestroyObjectIns()
{
    delete _pSMLcode;
}

void DestroyObjectIns::translate() {

	Name token; int idel,jdel; int inext,jnext; 
	Name ofClass;
	Name tempObjId;
	
	SMLline lineBeingTranslated;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	
	getNextToken(_pSMLcode,0,0," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();

	if ( token == "DESTROY_OBJECT" ) {
		lineBeingTranslated = (*_pSMLcode)[inext];
		getNextToken(_pSMLcode,inext,jnext," ",tempObjId,idel,jdel,inext,jnext);
	}
	else  {
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "Expected DESTROY_OBJECT instruction" << endl;
		throw FATAL;
	}
	
	_objectId = tempObjId;

	if (inext>0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"Some crap is following DESTROY_OBJECT instruction");
		throw FATAL;
	}
	return;
	
}

//--------------------------------------------------------------------------
void DestroyObjectIns::out(const Name offset) const
{
	SMLUnit::out(offset); 
	char* ptn=offset.getString(); cout << ptn ;
	
	cout << "DestroyObject " << _objectId.outString() << endl;
	return;
}
//------------------------------------------  BF January 2020  -----------
void DestroyObjectIns::outSobj(ofstream& sobj) const
{

	sobj << "destroy_object" << endl;

        sobj << _objectId.stringForSobj() << endl;
	
	return;
}
