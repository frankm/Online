// waitforins.cxx: implementation of the WaitForIns class.
//
//                                                B. Franek
//                                           11 December 2014
// Copyright Information:
//      Copyright (C) 1999/2008 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <cstring>
#include "utilities.hxx"
#include "waitforins.hxx"
#include "smiobject.hxx"
#include "smiobjectset.hxx"
#include "registrar.hxx"
#include "errorwarning.hxx"
#include "when.hxx"
#include "action.hxx"
#include "assert.h"

extern Registrar allObjects, allObjectSets;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

WaitForIns::WaitForIns()  
{
   _name = "waitfor";
//   cout << "waitfor constructor called" << endl;
   return;
}

WaitForIns::~WaitForIns()
{
    delete _pSMLcode;
}
//--------------------------------  translate  -------------------------
void WaitForIns::translate()
{
//cout << endl << "  translating Wait_For " << endl; char offset[5] = " ";
//_pSMLcode->out(offset);

	
	SMLline lineBeingTranslated;
	
	Name token; int idel,jdel; int inext,jnext;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	char del = getNextToken(_pSMLcode,0,0," ",token,idel,jdel,inext,jnext);
	if ( del == ' ' ) {}    // this is to make compiler happy
	token.upCase(); token.trim();

	if ( token == "WAIT_FOR"){}
	else
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << " Keyword WAIT_FOR not found" << endl;
		throw FATAL;
	}

	SMLline line; Name name;
	int numOfLines = _pSMLcode->length();
	
	int firstUnitFound =0;
	int newUnit = 0;
	SMLUnit* pUnit;
	int whenNo = 0;
	Name unitType;
	Registrar* pTypeRegistrar = 0;
	Name registeredName;
	
	for ( int i=1; i<numOfLines; i++)
	{
		line = (*_pSMLcode)[i];
		if (line.commentLine() && firstUnitFound == 0) {continue;}
		int waitForKeyword = line.waitForKeyword();
		if ( waitForKeyword == 200) { break; } // END_WAIT_FOR
		else if ( waitForKeyword == 100)  // WHEN
		{
			firstUnitFound = 1;
			newUnit = 1;
			pUnit = new When(whenNo, true);
			whenNo++;
			assert (pUnit != 0 );
			unitType = "When";
			pTypeRegistrar = &_whens;
			registeredName = "-";
//cout << " WaitForIns::translate()   start of when found " << endl
// << line << endl; 
			
		}
		else {}
		
		if (newUnit)
		{
			newUnit = 0;
			_internalSMLUnits.addRegardless(unitType,pUnit);
			pUnit->setParentUnit(this);
			pTypeRegistrar->addRegardless(registeredName,pUnit);
		}
		
		pUnit->acceptLine(line);
	}
	
	translateUnits();
	
	return;
}
//-----------------------------------------------------------------------
void WaitForIns::out(const Name offset) const
{
cout << " WaitForIns::out   not implemented yet" << endl; return;
	SMLUnit::out(offset);
	char* ptn=offset.getString();
	cout << ptn ; cout << "Objects to wait for:" << endl;
	_refObjects.out(offset);
	
	cout << ptn ; cout << "Objects in object sets to wait for:" << endl;
	_refObjectSets.out(offset);
	
	return;
}

//----------------------------------  BF January 2009  ------------------
void WaitForIns::outSobj(ofstream& sobj) const
{
//cout << " WaitForIns::outSobj  started" << endl;
	sobj << "waitfor" << endl;

	int nw = _whens.length();

	for (int iw=0; iw < nw; iw++) {
		void* ptnvoid = _whens.gimePointer(iw) ;
		When* pWhen;
		pWhen = (When*)ptnvoid;
		pWhen->outSobj(sobj);
	}
	sobj << "endwaitfor" << endl;
	
	return;
}

//---------------------------------------------------------------------------
int WaitForIns::examine()
{
//cout << endl << " WaitForIns::examine" << endl;

	int retcode = 0;
	SMLline firstLine = (*_pSMLcode)[0];

/*  cout << endl 
  << " ====================== WaitIns::examine() ============= " << endl;
	
  cout << "  Parent : " << _pParentUnit->unitId() 
  << "  " << _pParentUnit->unitName() << endl;
		
  cout << "     Complete Ancestry " << endl;
	
	int num;
	NameVector ids,names;
	
	_pParentUnit->ancestry(ids,names);
	
	num = ids.length();

	for (int i=0; i<num; i++)
	{
		cout << "     " << ids[i] << "  " << names[i] << endl;
	}
*/
	int iflg = examineUnits();
	retcode = retcode + iflg;
	
//cout << " return from WaitForIns::examine() " << retcode << endl;
	return retcode;
}

//-------------------------------------------------------------------------------------------------------
Name WaitForIns::outString() 
{
	Name temp;
	
	temp = " WaitForIns::outString   not implemented yet"; return temp;
	
	temp = "WAIT ( ";
	
	int first = 0;
	int numObjects = _refObjects.length();
	Name objname;
	
	for (int i=0; i<numObjects; i++)
	{
		objname = _refObjects[i];
		if ( first == 0 )
		{
			first = 1;
			temp += objname;
		}
		else
		{
			temp += ", ";
			temp += objname;
		}
	}
	
	int numObjectSets = _refObjectSets.length();
	Name objsetname;
	
	for (int i=0; i<numObjectSets; i++)
	{
		objsetname = _refObjectSets[i];
		if ( first == 0 ) 
		{ 
			first = 1;
			temp += " all_in ";
			temp += objsetname;
		}
		else
		{
			temp += ", all_in ";
			temp += objsetname;
		} 
		
	}
	
	temp += " )";
	
	return temp;
}

/*int main()
{
	return 1;
}*/
