// wfmcontinue.hxx: interface for the WFMcontinue class.
//
//                                                  B. Franek
//                                                 July 2015
//
//////////////////////////////////////////////////////////////////////
#ifndef WFMCONTINUE_HH
#define WFMCONTINUE_HH

#include "whenresponse.hxx"
#include "smlunit.hxx"

class WFWcontinue : public SMLUnit, public WhenResponse
{
public:
	WFWcontinue();

	~WFWcontinue() {};

	void translate();
	
	int examine();

	void outSobj(ofstream& sobj) const;

	void out(const Name offset) const 
	             { cout << offset.getString() 
		      << "CONTINUE" << endl; return; };

protected :

};

#endif 
