// reportins.cxx: implementation of the ReportIns class.
//
//                                                B. Franek
//                                                 July 2017
//////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "smlunit.hxx"
#include "smlline.hxx"
#include "name.hxx"
#include "utilities.hxx"
#include "registrar.hxx"
#include "smiobject.hxx"
#include "state.hxx"
#include "action.hxx"
#include "reportins.hxx"
#include "errorwarning.hxx"
#include "reservednames.hxx"
#include "parms.hxx"

extern Registrar allObjects;	

#include "objectregistrar.hxx"
extern ObjectRegistrar allSMIObjects;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ReportIns::ReportIns() :
	Instruction(),
	_severity(""),
	_msgElements()
{
	_name = "report";
	return;
}

ReportIns::~ReportIns()
{
}

void ReportIns::translate() {

//cout << endl << " ReportIns::translate() " << endl;

	Name token; int ist,jst,idel,jdel; int inext,jnext; 
	
	SMLline lineBeingTranslated;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	
	char del = getNextToken(_pSMLcode,0,0,"(",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();
	
	if ( token == "REPORT" ) {}
	else
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << " Keyword REPORT not found" << endl;
		throw FATAL;
	}

	ist = inext; jst = jnext;

	if (ist < 0 || del != '(')
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "Nothing sensible follows the keyword REPORT"
		     << endl;
		throw FATAL;
	}
	
//lineBeingTranslated.indicateCharacter(" jst ",jst);

	del = getNextToken(_pSMLcode,ist,jst,",",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();
	lineBeingTranslated = (*_pSMLcode)[ist];
	
	if ( token == "INFO" || token == "WARNING" || token == "ERROR" || token == "FATAL") {}
	else
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "Severity has illegal value " << token 
		     << endl;
		throw FATAL;
	
	}
	
	_severity = token;
	
	ist = inext; jst = jnext;
	
	if (ist < 0 || del != ',')
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "Nothing sensible follows Severity"
		     << endl;
		throw FATAL;
	}
//lineBeingTranslated.indicateCharacter(" jst ",jst);	

	extractReportElements(_pSMLcode,ist,jst,inext,jnext);
	
//((*_pSMLcode)[inext]).indicateCharacter(" jnext ",jnext);

	if ( inext < 0 ) return;
	ist = inext; jst = jnext;
	
	getNextToken(_pSMLcode,ist,jst," ",token,idel,jdel,inext,jnext);
	if ( inext>= 0 )
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "Some crap follows REPORT instruction"
		     << endl;
		throw FATAL;
	
	}
	
//	out(" ");	 	
	return;
}
//------------------------------------------------------------------------
void ReportIns::extractReportElements(SMLlineVector* pSMLcode,
				const int istin, const int jstin,  
				int& inext, int& jnext)
{
// cout << endl << "ReportIns::extractReportElemnts"
//      << endl;
 
// istin, jstin points to the first character of the first element
	int ist, jst,  idel,jdel;
	ist = istin;
	jst = jstin;
	char del;
	SMLline lineBeingTranslated;
	
	for (;;) 
	{
		lineBeingTranslated = (*_pSMLcode)[ist];
//lineBeingTranslated.indicateCharacter(" jst ",jst);
    
		getNextElement(pSMLcode, ist,jst,
			idel,jdel,del,inext,jnext);
			
//((*_pSMLcode)[inext]).indicateCharacter(" jnext ",jnext);			
//cout << "delimiter |" <<del<<"|"<< " inext jnext " << inext << " " << jnext << endl;

		if (del==')') { return; }
		if (inext<0) {
			ErrorWarning::printHead("ERROR",lineBeingTranslated,
				"Brackets do not balance while parsing REPORT");
				throw FATAL;
		}
		ist = inext; jst = jnext;
	}     

      
	return;
}
//--------------------------------------------------------------------------------
void ReportIns::getNextElement(SMLlineVector* pSMLcode,
			const int ist, const int jst,
			int& idel, int& jdel, char& del,
			int& inext, int& jnext)
{
	
	SMLline lineBeingTranslated;
	
	
	lineBeingTranslated = (*_pSMLcode)[ist];
//	lineBeingTranslated.indicateCharacter(" jst ",jst);

	Name indiValue;
	Name form;
	int ierr;
	
	del = getIndiValue(pSMLcode,ist,jst,"+)",indiValue,form,ierr,
				idel,jdel,inext,jnext);
	
//	cout << form << "  " << indiValue << " " << del << endl;

//	lineBeingTranslated = (*_pSMLcode)[inext];
//	lineBeingTranslated.indicateCharacter(" jnext ",jnext);	 
	
	if (ierr != 0 || form == "UNKNOWN" )
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << " Error parsing REPORT element" << endl;
		throw FATAL;
	}

	_msgElements += indiValue;	
	return;
}

//--------------------------------------------------------------------------
void ReportIns::out(const Name offset) const
{
	SMLUnit::out(offset); 
	char* ptn=offset.getString(); cout << ptn ;
	
	cout << " report  Severity : " << _severity << endl;
	cout << ptn; _msgElements.out(offset);

	return;
}
//------------------------------------------  BF July  2017 -----------
void ReportIns::outSobj(ofstream& sobj) const
{

	sobj << "report" << endl;
	sobj << _severity.getString() << endl;
	
	int numElm = _msgElements.length();
	Name element;
	
	sobj << "    " << numElm << endl;
	
	for ( int i=0; i<numElm; i++ )
	{
		sobj << _msgElements[i].stringForSobj() << endl;
	}
	
	return;
}
//-------------------------------------------------------------------------
int ReportIns::examine()
{
	int retCode = 0;
	
	SMLline firstLine = (*_pSMLcode)[0];
	
	
//beg debug
/*
  cout << endl 
  << " ====================== ReportIns::examine() ============= " << endl;
	
  cout << "  Parent : " << _pParentUnit->unitId() 
  << "  " << _pParentUnit->unitName() << endl;
		
  cout << "     Complete Ancestry " << endl;
	
	int num;
	NameVector ids,names;
	
	_pParentUnit->ancestry(ids,names);
	
	num = ids.length();

	for (int i=0; i<num; i++)
	{
		cout << "     " << ids[i] << "  " << names[i] << endl;
	}
	
	cout << "Parent Unit Code " << endl;	

	char temp[] = " "; _pSMLcode->out(temp);	
*/
//end debug

	SMIObject* parentObject;
	State* parentState;
	Action* parentAction;

	
	parentObject = (SMIObject*) _pParentUnit->parentPointer("Object");
	parentState = (State*) _pParentUnit->parentPointer("State");  
	parentAction = (Action*) _pParentUnit->parentPointer("Action");      


	int numElm = _msgElements.length();
	IndiValue element;
	Name value,valueType;
	Name actualType(""); 
	
	for ( int ie=0; ie<numElm; ie++ )
	{
		element = _msgElements[ie];
		if ( element.isConstant() ) { continue; } // nothing to check for constant
	// This is an indirect value
		
		if ( !element.actualValueAccessible
				(allSMIObjects,
				 parentObject,
				 parentState,
				 parentAction,
				 actualType) )
		{
			retCode = 1;
			ErrorWarning::printHead("SEVERE WARNING",firstLine);		
			cout << " The value of "
			     << element.outString() << " is not accessible"  << endl;
			return retCode;
		}
			
	}
	
	return retCode;
}
