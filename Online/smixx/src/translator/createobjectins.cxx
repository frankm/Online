// CreateObjectins.cxx: implementation of the CreateObjectIns class.
//
//                                                B. Franek
//                                               April 2010
//////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "smlunit.hxx"
#include "smlline.hxx"
#include "name.hxx"
#include "utilities.hxx"
#include "createobjectins.hxx"
#include "registrar.hxx"
#include "errorwarning.hxx"

	extern Registrar allUnits;
	extern Registrar allClasses;
	
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CreateObjectIns::CreateObjectIns()  
{
   _name = "CreateObject";
   return;
}

CreateObjectIns::~CreateObjectIns()
{
    delete _pSMLcode;
}

void CreateObjectIns::translate() {

	Name token; int idel,jdel; int inext,jnext; 
	Name ofClass;
	Name tempObjId;
	
	SMLline lineBeingTranslated;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	
	getNextToken(_pSMLcode,0,0," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();

	if ( token == "CREATE_OBJECT" ) {
		lineBeingTranslated = (*_pSMLcode)[inext];
		getNextToken(_pSMLcode,inext,jnext," ",tempObjId,idel,jdel,inext,jnext);
	}
	else  {
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "Expected CREATE_OBJECT instruction" << endl;
		throw FATAL;
	}
	
	_objectId = tempObjId;
	
	lineBeingTranslated = (*_pSMLcode)[inext];
	getNextToken(_pSMLcode,inext,jnext," ",ofClass,idel,jdel,inext,jnext);
	
	ofClass.upCase();
	if ( ofClass == "OF_CLASS" ) {}
	else {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"OF_CLASS keyword not found");
		throw FATAL;
	}
	
	lineBeingTranslated = (*_pSMLcode)[inext];
	getNextToken(_pSMLcode,inext,jnext," ",_className,idel,jdel,inext,jnext);
	
	_className.upCase(); _className.trim();
	if (!check_name(_className)) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << " Class name : " << _className << "  is not a name" << endl;
		throw FATAL;
	}
	
	void *ptn = allClasses.gimePointer(_className);
	if (!ptn) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << " Class : " << _className << "  has not been declared" << endl;
		throw FATAL;
	}
	
        if (inext>0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"Some crap is following CREATE_OBJECT instruction");
		throw FATAL;
	}
	return;
	
}

//--------------------------------------------------------------------------
void CreateObjectIns::out(const Name offset) const
{
	SMLUnit::out(offset); 
	char* ptn=offset.getString(); cout << ptn ;
	
	cout << "createObject " << _objectId.outString() << " of_class " << _className << endl;
	return;
}
//------------------------------------------  BF April 2010  -----------
void CreateObjectIns::outSobj(ofstream& sobj) const
{

	sobj << "create_object" << endl;

        sobj << _objectId.stringForSobj() << endl;
	
// The following line makes it look like declaration of an object belonging
// to class and also having no attributes of its own... see isofclassobject.cxx	

	int n1,n2,n3; char line[80];
	n1 = 1; n2 = 0; n3 = 0;
	sprintf(line,"%5d%5d%5d",n1,n2,n3);
	sobj << line << endl;
//----------------------------------------------------------------
	
	sobj << _className.getString() << endl;
	
	return;
}
