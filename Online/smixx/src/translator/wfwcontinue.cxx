// WFWcontinue.cxx: implementation of the WFWcontinue class.
//
//                                                B. Franek
//                                               July 2015
// Copyright Information:
//      Copyright (C) 1999/2015 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include "utilities.hxx"
#include "errorwarning.hxx"
#include "wfwcontinue.hxx"

WFWcontinue::WFWcontinue() : SMLUnit("WFWcontinue",1)
{
}
//----------------------------------------------------------------------
void WFWcontinue::translate()
{

	Name token; int idel,jdel; int inext,jnext;
	
	SMLline lineBeingTranslated;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	int ist,jst;
	
	ist = 0; jst = _firstColOfUnit;
//cout << " Starting WFWcontinue-translate() " << ist << " " << jst << endl;
//char temp[] = " "; _pSMLcode->out(temp);

	getNextToken(_pSMLcode,ist,jst," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();

	if ( token == "CONTINUE"){}
	else {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"expected CONTINUE");
		cout << " Found instead " << token << endl;
		throw FATAL;
	}
	
//	_actionNm = "&CONTINUE"; _objectNm = "&THIS_OBJECT";

        if (inext>0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"Some crap follows CONTINUE");
		throw FATAL;
	}
	return;

}
//----------------------------------------------------------------------
int WFWcontinue::examine()
{
//cout << " There is nothing to examine for CONTINUE " << endl;
	return 0;
}
//----------------------------------------------------------------------
void WFWcontinue::outSobj(ofstream& sobj) const
{
// For reasons of backward compatibility, this response is
// simulated like if it was instruction 
//      DO &CONTINUE &THIS_OBJECT
//  should realy be changed sometime in the future.

	sobj << "do" << endl;

	sobj << "&CONTINUE" << endl;

	int numpar = 0;

	sobj << "    " << numpar << endl;

	sobj << "&THIS_OBJECT" << endl;
}

