//-----------------------------------------------------------------------
//                         ActionHeadBlock  Class
//                                                 B. Franek
//                                                 23 November 1999
//-----------------------------------------------------------------------
//
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "actionheadblock.hxx"
#include "smlunit.hxx"
#include "smlline.hxx"
#include "name.hxx"
#include "utilities.hxx"
#include "errorwarning.hxx"
#include "reservednames.hxx"

//--------------------------- Constructors -------------------------------

ActionHeadBlock::ActionHeadBlock ( ) 
	: SMLUnit("action head block",2),
	  _parameters() {
	return;
}

ActionHeadBlock::~ActionHeadBlock() {
    delete _pSMLcode;
}

  
void ActionHeadBlock::translate() {

/*	_pSMLcode->out();
	SMLline line; char* pLine;*/

	SMLline firstLine = (*_pSMLcode)[0];
	
    Name token; int idel,jdel; int inext,jnext;
	int ist,jst;
	char del = getNextToken(_pSMLcode,0,0,":",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();

// the first two tokens have already been processed and all we have to check
// is if there are any parameters
	
    ist = inext; jst = jnext;

//	line = (*_pSMLcode)[ist];pLine = line.getString();

	del = getNextToken(_pSMLcode,ist,jst," (",token,idel,jdel,inext,jnext);

/*	cout << " idel,jdel " << idel << ", " << jdel << "  del |" << del << "|" << endl;
	cout << " next,jnext " << inext << ", " << jnext << endl;*/

	if ( del != '(' ) {
		if ( inext >= 0 )
		{
			ErrorWarning::printHead("ERROR",firstLine,
			"Unrecognised code follows action declaration");
			throw FATAL;
		}
		else {
			return;
		}
	}

	ist = idel; jst = jdel;
	_parameters.initFromSMLcode( 1, _pSMLcode, ist,jst,inext,jnext);
// If something goes wrong, Translator is terminated inside the method
	return;
}

void ActionHeadBlock::out(const Name offset) const
{

	SMLUnit::out(offset); 
	char* ptn=offset.getString(); cout << ptn << endl;
	_parameters.outParms(offset);

}
//---------------------------------------------------------------------------
int ActionHeadBlock::examine()
{
	int retcode = 0;
	
/*
  cout << endl 
  << " ====================== ActionHeadBlock::examine() ============= " << endl;
	
  cout << "  Parent : " << _pParentUnit->unitId() 
  << "  " << _pParentUnit->unitName() << endl;
		
  cout << "     Complete Ancestry " << endl;
	
	int num;
	NameVector ids,names;
	
	_pParentUnit->ancestry(ids,names);
	
	num = ids.length();

	for (int i=0; i<num; i++)
	{
		cout << "     " << ids[i] << "  " << names[i] << endl;
	}
*/

	int numPar = _parameters.numOfEntries();
	
	for ( int ip=0; ip<numPar; ip++ )
	{
		Name parName = _parameters.getParName(ip);
		if ( ReservedNames::isReserved(parName) )
		{
			ErrorWarning::printHead("ERROR",(*_pSMLcode)[0]
			,"reserved name not allowed to be declared");
			cout << endl;
			retcode = 1; break;
		}
	}

	
	int iflg = examineUnits();
	
	return retcode+iflg;
}

