//------------------------  Class   D o I n s  -----------------------------
#include "smixx_common.hxx"
#include <stdio.h>
#include <assert.h>
#include "utilities.hxx"
#include "ut_sm.hxx"
//-------------------- Externals -----------------
#include "objectregistrar.hxx"
   extern ObjectRegistrar allSMIObjects;
   extern  Name smiDomain;
#include "registrar.hxx"
   extern Registrar allSMIObjectSets;
//-------------------------------------------------
#include "doins.hxx"
#include "smiobjectset.hxx"
#include "smiobject.hxx"
#include "parms.hxx"
#include "param.hxx"
#include "alarm.hxx"
#include "options.hxx"
#include "reservednames.hxx"
#include "getvarelem.hxx"
//                                                      Date : 5-May-1996
//                                                     Author: Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//---------------------------- DoIns -------------------------------------
//
DoIns::DoIns
( char lines[][MAXRECL], int lev, int *no_lines, 
  SMIObject *pobj, State *pstat, Action* pact){
   char tempstr[MAXRECL];
   int lin;
   
	if ( pstat == NULL ) {}  // will remove compiler warning
	
   _level = lev;

   _pParentObject = pobj;
   
   _pParentState = pstat;

   _pParentAction = pact;
//    cout << " do init _pParentAction : " << _pParentAction << "\n";

   sscanf(lines[1],"%s",tempstr);
   _actionNm = tempstr;

   sscanf(lines[2],"%d",&_numOfPara);

   lin = 3;

   Name paraNm, paraGenVal, paraGenType;

   if ( _numOfPara > 0) {
      // For every parameter Translator sends just its name and its generalized
      // value
      for(int ipar=0; ipar<_numOfPara; ipar++) {
         strcpy(tempstr,lines[lin]);
         paraNm = tempstr;
         lin++;
	 strcpy(tempstr,lines[lin]);
         paraGenVal = tempstr;

        if ( !strcmp(tempstr,"&SMIDOMAIN") ) {
            paraGenVal = "\"";
            paraGenVal += smiDomain;
            paraGenVal += "\"";
         }
        
        lin++;
		Param tempPar(paraNm,paraGenVal,paramcons::undef);
		_doInsParameters.addParam(tempPar);
      }
   }

   sscanf(lines[lin],"%s",tempstr);
   
	if (strncmp(tempstr,"&ALL_IN_",8) == 0) 
	{ // this is ALL_IN type
		_objectId = "";
		_setId = &tempstr[8];
	}
	else
	{
		_objectId = tempstr;
		_setId = "";
	}
	   
   *no_lines = lin + 1;
   return;
}
//--------------------- Destructor  BF March 2020 -----------------
DoIns::~DoIns()
{
//  No objects created in constructor on the heap so nothing to destroy. 
    return;
}


//---------------------------- whatAreYou ---------------------------------

void DoIns::whatAreYou(){
   	Name temp = nBlanks(_level*4+10);
	char* ident = temp.getString();
	
	cout << ident << outShort().getString() << endl;
	return;
}

//----------------------------  execute  ---------------------------------------
//
//
int DoIns::execute( Name& endState ){

	int dbg; Options::iValue("d",dbg);
	int imainIndent = 3*_level+5;
	Name mainIndent = nBlanks(imainIndent);

//debug beg
if ( dbg > 3 )
{
	cout << mainIndent << "executing: " << outShort() << endl;
}
//debug end

   Name actionString = _actionNm;  

	if (_numOfPara > 0) 
	{  // build parameter string for dispatch and append it to action name
		Name tempParString;
		createOutgoingParameters(tempParString);
		
		actionString += tempParString;

//    cout << " Parameter string : " << tempParString << endl;
	}
//---------------------------------------------------------------------------

//   cout << " Action string : " << actionString << endl;
//   cout.flush();

	SMIObject* pSMIObj;
	Name objectName;
	
	Name actSetNm("");
	void* ptnv;

	if ( _objectId == "" )
	{  //  all_in _setId type
		int flg = GetVarElem::actualSetName( _setId,
		                 _pParentAction,
						 "do",
						 actSetNm,ptnv);
		if (!flg) {endState = "not changed"; return 0;}		

		SMIObjectSet* ptnSet = (SMIObjectSet*) ptnv;

		ptnSet->reset();

		while (ptnSet->nextObject(objectName)) {
			pSMIObj = allSMIObjects.gimePointer(objectName);
			if ( pSMIObj == 0 ) {
			cout << " Object " << objectName 
			<< " in set " << actSetNm << " does not exists" << endl;
			Alarm::message("FATAL",_pParentObject->name(),
			" Processing DO ... an object missing in SET");
			}
			pSMIObj->queueAction(actionString);
		}
	}

	else {
		Name actObjNm("");
		int flg = GetVarElem::actualObjectName( _objectId,
		                 _pParentAction,
						 "do",
						 actObjNm);
		if (!flg) {endState = "not changed"; return 0;}
						 
		pSMIObj = allSMIObjects.gimePointer(actObjNm);		
		pSMIObj->queueAction(actionString);
	}

   endState = "not changed";
   return 0;         // do instruction allways finishes
}


//-------------------------  executeHp  ---------------------------------------
//
//
void DoIns::executeHp(){

	int dbg; Options::iValue("d",dbg);
    
	Name actionString = _actionNm;
	
	if ( _numOfPara > 0 )
	{  // build parameter string for dispatch and append it to action name
		Name tempParString;
		createOutgoingParameters(tempParString);
		
		actionString += tempParString;
	}
	
if ( dbg > 7 )
{
  cout << " Action string : " << actionString << endl;
}
    	_pParentObject->queueHpAction(actionString);

	return ;
}
//------------------------- actionName --------------------------------------
Name DoIns::actionName() const
{
	return _actionNm;
}
//--------------------------------------------------------------------------
void DoIns::createOutgoingParameters(Name& parmString)
{
//         do OTHER_OBJECT_ACTION(OTHER_OBJECT_ACTION_PAR_1 = 75,...
//                                OTHER_OBJECT_ACTION_PAR_N = LOCAL_PAR,...)
//                                OTHER_OBJECT
//
//    otObject  ...  other object 
//    otObjAct ... other object action
//    otObjActPar ... other object action parameter
//
//cout << endl << endl << " Entering createOutgoingParametersNew" << endl
//<<  " Do parameters :" << endl;    _doInsParameters.out();

	int dbg; Options::iValue("d",dbg);

	Parms tmpPars; // temporary DO parameters

if ( dbg > 7 )
{
    cout << " Do parameters :" << endl;    _doInsParameters.outParms();
}
	Name otObjActParNm,           // other object action parameter name
	            indiVal;           // its indirect value 
 
	
	Name parSpecVal,         // specific value dispatched in DO
	     parSpecValType;     // specific value type:   INT, FLOAT or STRING
	
	int numPar = _doInsParameters.numOfEntries();
	
	// objective of this do is to replace before the dispatch indirect values by
	// specific values and also supply the correct value type
	
	
	for ( int ipar=0; ipar<numPar; ipar++ )
	{
	//   ......otObjActParNm=indiVal....
		Param doInsPar = _doInsParameters.getParam(ipar);
		otObjActParNm = doInsPar.paramName();
		
		int err;
		parSpecVal = doInsPar.paramValueAndType
				(allSMIObjects,
				 _pParentObject,
				_pParentState,
				_pParentAction,
				 parSpecValType,err);

		if ( err != 0 )
		{	 
			Alarm::message("FATAL",_pParentObject->name(),
				" Processing DO ... problem with parameters");
		}		


		if ( parSpecVal == paramcons::noval )
		{
			cout << " *** Error : parameter " << doInsPar.paramName() 
			<< " does not have value" << endl;
			Alarm::message("FATAL",_pParentObject->name(),
				" Processing DO ... parameter has no value");
		}
		Param tempPar(otObjActParNm,parSpecVal,parSpecValType.getString());
		tmpPars.addParam(tempPar);
		
	}

	tmpPars.buildParmString(parmString);
	return; 
}
//---------------------------------------------------------------------------
void DoIns::executeResponse()
{
	executeHp();
	return;
}
//----------------------------------------------------------------------------
Name DoIns::outShort()
{
	Name temp;
	
	temp = "do ";
	temp += _actionNm;
	temp += " ";
	
	temp += _doInsParameters.outParmsString(1);
	temp += " ";
	
	if ( _setId == "" )
	{
		temp += _objectId.outString(); temp.replace("&THIS_OBJECT","");
	}
	else
	{
		temp += "all_in "; temp += _setId.outString();
	}


// Format: do RO_SETUP ("/RO_FILE="SMI_LES_RO_FILE) LES

	return temp;
}

