// wfwmove_to.cxx: implementation of the WFWMove_To class.
//
//                                                B. Franek
//                                               October 2015
// Copyright Information:
//      Copyright (C) 1999/2015 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////

#include "wfwmove_to.hxx"

WFWMove_To::WFWMove_To( char lines[][MAXRECL], int &noLines)
{
// do
// &MOVE_TO_'state-name'
// 0
// &THIS_OBJECT

	Name actionNm = lines[1];
	
	_stateNm = actionNm.subString(9,-1);
	noLines = 4;
	return;
} 

//-----------------  Destructor  BF Mar 2020  ------------------------
WFWMove_To::~WFWMove_To() { return; }

//----------------------------------------------------------------------
Name WFWMove_To::nextMove() { return _stateNm;}

//-----------------------------------------------------------------------
Name WFWMove_To::outShort()
{
	Name temp;
	
	temp = "move_to ";
	
	temp += _stateNm;
	
	return temp;
}



