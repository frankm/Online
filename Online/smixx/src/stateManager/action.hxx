//-----------------------------  A c t i o n  Class  -------------------------
#ifndef ACTION_HH
#define ACTION_HH

#include "parameters.hxx"
#include "inslist.hxx"
#include "name.hxx"
#include "parms.hxx"
#include "ptrvector.hxx"
#include "action_return_status.hxx"
class SMIObject;
class State;
class InsList;
//
//                                                   23-Sep-1996
//                                                   Boda Franek
//---------------------------------------------------------------------------
class Action {
public :
  Action
    ( char lines[][MAXRECL],Name& action_nm,int& no_lines,
      SMIObject *pobj, State* pstat);
      
  ~Action();

  void whatAreYou();

	Name outShort() const;

  void actionString(Name& actionstr);

	ActionReturnStatus_t execute(Name& endState, const Name& actionstr) ;

  Parms* pCurrentParameters() ; // returns the pointer to the current parms
  
  Parms* pActionParameters() ; // same as pCurrentParameters
	
	Name& actionName();

	void setDiagPrintOffset(int offset);

	int actionDiagPrintOffset() const;
	
private :
	
  Name _actionName;

  InsList *_pblock;      // Pointer to the instruction list(block) (block zero)
  
  PtrVector _allBlocks;  // array of pointers to all the blocks of the action
  
  Name _objName;    // Name of the object to which the action belongs

  SMIObject *_pParentObject;

  State* _pParentState;

//------------  Dynamic data

  Parms _currentParameters;  

	int _diagPrintOffset;   // for diagnostic printing
	
	int _suspend_flag;
};

#endif
