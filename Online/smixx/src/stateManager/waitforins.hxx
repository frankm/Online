//----------------------------- WaitForIns  Class ---------------------------------
#ifndef WAITFORINS_HH
#define WAITFORINS_HH

#include "parameters.hxx"
#include "instruction.hxx"

class State;
class Action;
class PtrVector;
#include "namevector.hxx"
#include "namelist.hxx"
//
//                                                               February 2015
//                                                               B. Franek
// Copyright Information:
//      Copyright (C) 1996-2015 CCLRC. All Rights Reserved.
//-----------------------------------------------------------------------------

class WaitForIns : public Instruction
{
public :
	WaitForIns( char lines[][MAXRECL],int lev
		,int& no_lines, SMIObject *pobj, State* pstat, Action*);


	~WaitForIns();

	void whatAreYou();

	int execute( Name& endState );   // 0-normal,  2-suspended

/** 
it establishes which objects were added and which were removed by conmparing
Previous Referenced Objects List with Current Referenced Objects List.
It unregisters its object as being client of removed objects.
It registers its object as being client of added objects.
Finaly it checks if it can resume execution and if yes, it returns 1.
*/	
	int reportingChangedSets ();
	            
/** returns TRUE if at least one of the when conditions evaluates to TRUE
    else FALSE
*/
	bool areYouReadyToResume(); 
	
private:
//-------------- functions --------------------------------------
/** will execute the whens one by one. The first one, whose condition gives
   TRUE will terminate this sequence and the corresp instruction is returned
   as either "&CONTINUE" or "'state name where to move'". If the sequence 
   finishes without a success, "" is returned.
*/
	Name executeWhens(); 
	
/** finds all the objects referenced by the WAIT instruction. I.E.
    the directly referenced objects plust the objects contained in the
    referenced sets
*/
	void getCurrentRefObjects();
	
/** will inform all the objects participating in WAIT_FOR instruction
    that the object has been suspended. Every time any of them changes 
    state, WAIT_FOR has to be re-evaluated
*/
	void informServersOfSuspension(); 

/** will inform all the objects participating in WAIT_FOR instruction
    that the object has been released.
*/
	void informServersOfSuspensionEnd(); 

	void updateRefObjectandSets();
	
//-------------- data -------------------------------------------

	PtrVector _whens;
		
	int _level;

	State* _pParentState;
	
	Action* _pParentAction;

	int _suspend_flag;  // 0 - 'fresh' WAIT
			   //  1 - suspended because all when's conditions are false


	Name _objName;    // Name of the parent object 

/** Pointer to the parent object
*/
	SMIObject* _pParentObject; 
	
/** List of objects that are listed in the WAIT
*/
	NameVector _refObjects;
	
/** List of object sets that are listed in the WAIT
*/
	NameVector _refObjectSets;  
	
	NameList _refObjectsCurrent;
	
	Name _resultOfLastExecuteWhens;            
};

#endif
