//
//  option.cxx
//  smiSM-Options
//
//  Created by Bohumil Franek on 28/03/2016.
//  Copyright © 2016 Bohumil Franek. All rights reserved.
//
#include <climits>
#include <cfloat>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utilities.hxx"
#include "option.hxx"

//---------------------------------------------------------------------------------
Option::Option(const char Id[],
               const char type[],
               const char defvalue[],
               const char name[],
               const char comment[] )
: _Id(Id), _type(type), _defvalue(defvalue), _name(name), _comment(comment)
{
    _value = _defvalue;
    _ivalue = INT_MAX;
    _fvalue = FLT_MAX;
    
    if (!convertValue()) exit(2);
    
    return;
}
//---------------------  Destructor  BF Mar 2020  -----------------------
Option::~Option() { return; }
//---------------------------------------------------------------------------------
char* Option::outString() 
{
    Name tempId = "-"; tempId += _Id;

//    sprintf(_outputStr,"%33s: %7s %30s",_name.getString(),_value.getString(),tempId.getString());
    sprintf(_outputStr,"%s(%s): %s",_name.getString(),tempId.getString(),_value.getString());
    return _outputStr;
}
//---------------------------------------------------------------------------------
int Option::newValue(const char value[])
{
	Option oldOpt = *this;
	
	_value = value;
    
	if (convertValue()) return 1;
	
	*this = oldOpt;  //keep the old values

	return 0;
}
//---------------------------------------------------------------------------------
void* Option::gimeConvValue()
{
    void* ptr = NULL;
    
    if ( _type == "INT" || _type == "BOOL" ) ptr = (void*)&_ivalue;
    if ( _type == "FLOAT") ptr = (void*)&_fvalue;
    if (_type == "STRING") ptr =(void*)_value.getString();
    
    return ptr;
}
//---------------------------------------------------------------------------------
Name& Option::gimeValue()
{
    return _value;
}
//---------------------------------------------------------------------------------
char* Option::cValue()
{
    if ( _type == "STRING" ) {}
    else
    {
        cout << " error   " << _Id << " is not STRING" << endl;
        return NULL;
    }
    
    return _value.getString();
}
//---------------------------------------------------------------------------------
int& Option::iValue()
{
    if ( _type == "INT" || _type == "BOOL" ) {}
    else
    {
        cout << " error   " << _Id << " is not INT nor BOOL"
	     << " but " << _type << endl;
    }
    
    return _ivalue;
}
//---------------------------------------------------------------------------------
float& Option::fValue()
{
    if ( _type == "FLOAT" ) {}
    else
    {
        cout << " error   " << _Id << " is not FLOAT" << endl;
    }
    
    return _fvalue;
}
//---------------------------------------------------------------------------------
Name& Option::gimeId()
{
    return _Id;
}
//---------------------------------------------------------------------------------
Name& Option::gimeType()
{
    return _type;
}
//---------------------------------------------------------------------------------
int Option::convertValue()
{
    
    if ( _type == "INT" || _type == "BOOL" )
    {
    	if ( !check_int(_value) )
	{
            cout << "error  value of " << _Id << " is not an integer" <<  endl;
            return 0;	
	}
	sscanf(&_value[0],"%d",&_ivalue);
	if (_type == "BOOL" && _ivalue != 0 && _ivalue != 1 )
	{
            cout << "error  value of " << _Id << " is not bool" <<  endl;
            return 0;
	}

    }
    
    else if ( _type == "FLOAT" )
    {
    	if ( !check_float(_value) )
	{
            cout << "error  value of " << _Id << " is not a float" <<  endl;
            return 0;	
	}
        sscanf(&_value[0],"%f",&_fvalue);
    }
    
    else if ( _type == "STRING") {}
    
    else
    {
        cout << "error  " << _Id << "  unknown type " << _type << endl;
        return 0;
    }
    
    return 1;
}
//---------------------------------------------------------------------------------
Name Option::gimeExportString() 
{
    Name exportString = _Id; exportString += "/";
    
    exportString += _type; exportString += "/";
    
    exportString += _value; exportString += "/";
    
    exportString += _name; 

    
    return exportString;
}
