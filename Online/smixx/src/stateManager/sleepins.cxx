//-----------------------------  SleepIns  Class ------- SLEEP instruction -------
#include "smixx_common.hxx"
#include <stdio.h>
#include <assert.h>

#include "parameters.hxx"
#include "ut_sm.hxx"
//---------------------- Externals ----------------------
#include "objectregistrar.hxx"
#include "registrar.hxx"
   extern ObjectRegistrar allSMIObjects;
   extern Registrar allSMIObjectSets;
#include "resumehandler.hxx"
   extern ResumeHandler resumeHandler;
//-------------------------------------------------------
#include "sleepins.hxx"
#include "smiobject.hxx"
#include "options.hxx"
//
//                                                        B. Franek
//                                                        July 2011
// Copyright Information:
//      Copyright (C) 1996-2011 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------
//
//------------------------------ SleepIns -------------------------------------

SleepIns::SleepIns
	( char lines[][MAXRECL], int lev, int& no_lines
	, SMIObject *pobj, State *pstat, Action* pact)
{
//-------------------------------------------------------------------------
// Input :
// lines[0] ....... the first line after the 'sleep'
// lev    ....... the block level

// Output :
// no_lines ....... number of lines in 'sleep' (not counting the first 'sleep')
//----------------------------------------------------------------------------

	if ( pstat == NULL ) {}  // will remove compiler warning

	_objName = pobj->name();
	_pParentObject = pobj;
   
	_pParentAction = pact;

	_pParentState = pstat;
		
	_suspend_flag = 0;
	
	_pTimer = NULL;  // This will get created when needed at least once

//   cout << " SLEEP belongs to object : " << _objName << "\n";

	_level = lev;

	int il;

	il = 0;

	_timeToSleep = lines[il];
	
	no_lines = 1;

	return ;
}
//-------------------  Destructor  BF  Mar 2020  -----------------
SleepIns::~SleepIns()
{
	if ( _pTimer != NULL ) delete _pTimer;
	return;
}
//==================================================================
void SleepIns::whatAreYou()
{
	Name temp = nBlanks(_level*4+10);
	char* ident = temp.getString();

	cout << ident;	
	cout << outShort() << endl;
	
	return;
}
//----------------------------  outShort()  -------------------
Name SleepIns::outShort() const
{
	Name temp;
	temp = "sleep ";
	temp += _timeToSleep.outString();
	return temp;
}
//========================================================================
int SleepIns::execute(Name& endState)
{
	int dbg; Options::iValue("d",dbg);
	int imainIndent = 3*_level+5;
	Name mainIndent = nBlanks(imainIndent);

	endState = "not changed";

//debug beg
if ( dbg>3 )
{
	cout << mainIndent;
	if ( _suspend_flag == 0 ) { cout << "executing: "; }
	else { cout << "resuming: "; }
	cout << outShort();
	cout << endl; 
}
//end debug	

//debug beg
if ( dbg > 5 )
{
	cout << endl << " start================= SleepIns::execute =========== " << endl;
	cout  << "   SLEEP instruction(" << this << " of " << _objName << endl;
}
//debug end

	
	if ( _suspend_flag == 0 )
	{ // This is a fresh unsuspended SLEEP
		// will start time
		Name actualType(""); int err;
		Name actualSleep = _timeToSleep.actualValue(allSMIObjects,
                                                    _pParentObject,
                                                    _pParentState,
                                                    _pParentAction,
                                                    actualType,err);

		sscanf(actualSleep.getString()," %d",&_actualTimeToSleep);
		
		if ( _pTimer == NULL )
		{
			_pTimer = new SleepInsTimer(_pParentObject->name());
		}
		_pTimer->startTimer(_actualTimeToSleep);
		cout << mainIndent;
		cout << "starting timer  time = " << _actualTimeToSleep <<endl;		
		_pParentObject->setPointerToSuspendedSLEEP(this);
		//***** need to tell ResumeHandler about this
		resumeHandler.registerSuspendedObject(_pParentObject);
		_suspend_flag = 1;
//debug beg
if ( dbg>3 )
{
	cout << mainIndent << "suspending: sleep" << endl;
}
//debug end
//debug beg
if ( dbg > 5 )
{
	cout << " SLEEP suspended" << endl;
	cout << " return================= SleepIns::execute =========== "
	 << endl << endl;
}
//debug end
		return 2;
			
	}
	else
	{  // this is suspended SLEEP that was now released
		_suspend_flag = 0;
//debug beg
if ( dbg > 3)
{
	cout << mainIndent << "sleep terminated" << endl;
}
//debug end
//debug beg
if ( dbg > 5 )
{
	cout << "  terminated" << endl;
	cout << " return================= SleepIns::execute =========== "
	 << endl << endl;
}
//debug end
		return 0;
	}	
}

