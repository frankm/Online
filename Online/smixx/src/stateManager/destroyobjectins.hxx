//-------------------------  Class   DestroyObjectIns  -------------------------------
#ifndef DESTROYOBJECTINS_HH
#define DESTROYOBJECTINS_HH

#include "name.hxx"
#include "namevector.hxx"
#include "parameters.hxx"
#include "instruction.hxx"
#include "varelement.hxx"
class SMIObject;
class State;
class Action;
//                                                  Date :    Jan 2020
//                                                  Author : Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2020 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------


class DestroyObjectIns : public Instruction{
public:
	DestroyObjectIns
           ( char lines[][MAXRECL], const int lev, int& no_lines,
              SMIObject* pobj, State *pstat, Action* pact);

	~DestroyObjectIns();
	
	void whatAreYou() ;
	
	Name outShort() const ;

	int execute( Name& endState );   // 0-normal,  1-terminated action, 2-suspended
private:
	int _level;


	VarElement _objectId;

	SMIObject *_pParentObject;

	Action *_pParentAction;

};

#endif
