// wfwcontinue.hxx: interface for the WFWContinue class.
//
//                                                  B. Franek
//                                                 October 2015
//
//////////////////////////////////////////////////////////////////////
#ifndef WFWCONTINUE_HH
#define WFWCONTINUE_HH

#include "parameters.hxx"
#include "whenresponse.hxx"

class WFWContinue : public WhenResponse
{
public:
	WFWContinue( char lines[][MAXRECL], int &noLines );

	~WFWContinue();
	
	Name nextMove();
	
	Name outShort();
	
protected :

};

#endif 
