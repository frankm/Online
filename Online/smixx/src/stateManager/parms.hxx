//----------------------  Class   Parms  -----------------------------------
//
//
//                                                      Author: Boda Franek
//                                                      Date : 10 June 1998
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//
//----------------------------------------------------------------------------
#ifndef PARMS_HH
#define PARMS_HH
#include "parmsbase.hxx"

class Name;
//----------------------------------------------------------------------------

class Parms : public ParmsBase
{
	public :
		Parms();
	  
		Parms(const Parms& ); 

		~Parms();
	  
		Parms& operator=(const Parms& );

//----    State Manager methods   ------------------

		void buildParmString(Name& str) const;

		void initFromParmString(const Name& str);

		int setFromParmString(const Name& str);

};

#endif
