//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  April 2021
//----------------------------------------------------------------------
#ifndef GETVARELEM_HH
#define GETVARELEM_HH

class Name;
class Action;
class VarElement;

class GetVarElem {
public:

	static int actualObjectName( const VarElement& objectId,
								       Action* pAct,
								 const Name& insname,
								 Name& actualName);

	static int actualSetName( const VarElement& setId,
								       Action* pAct,
								 const Name& insname,
								 Name& actualName, void* &ptnvSet);

	
private:

};

#endif
