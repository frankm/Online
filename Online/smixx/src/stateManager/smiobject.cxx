//--------------------------  SMIObject Class-----------------------------------
// $Id
//#define FAST_SMI
#include <stdlib.h>
#include "smixx_common.hxx"
#include <stdio.h>
#include <assert.h>
#include "ut_sm.hxx"
//----------- Externals ------------------
#include "queue_name.hxx"
   extern Queue_Name executableObjectQ;
#include "objectregistrar.hxx"
   extern ObjectRegistrar allSMIObjects;
#include "ifhandler.hxx"
   extern IfHandler ifHandler;
#include "resumehandler.hxx"
   extern ResumeHandler resumeHandler;
#include "registrar.hxx"
   extern Registrar allSMIClasses;
   extern Registrar allSMIObjectSets;
//---------------------------------------
#include "smiclass.hxx"
#include "smiobject.hxx"
#include "smiobjectset.hxx"
#include "commhandler.hxx"
#include "msg.hxx"

#include "waitins.hxx"
#include "waitforins.hxx"
#include "ifins.hxx"
#include "alarm.hxx"
#include "options.hxx"
#include <math.h>
#include "timehandling.hxx"
#include "param.hxx"
//                                                         19-August-1996
//                                                          B. Franek
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------
//
typedef char OBJLINE[MAXRECL];

SMIObject::SMIObject
 ( char lines[][MAXRECL]){

	int dbg; Options::iValue("d",dbg);

// Infinite loop detection initialisation  --------
	_loopStarted = false;
	_seriousLoopStarted = false;	
	_fstateTime = TimeHandling::floatTime() - 1.0;
	// 1 second is subtracted to prevent false loop reporting at the
	// begining
//                                         --------
	
	
    _lock = 0;

//  0th line is:*OBJECT

   char objnm_temp[MAXRECL];
   strcpy(objnm_temp,lines[1]);
   _objectName = objnm_temp;

    if ( dbg > 4 ) {
	cout << "         Constructing Object  " << _objectName << endl;
    }


   allSMIObjects.registerObject(this);

    int numOfAttributes;
    sscanf(lines[2],"%d %d %d",&_classFlag,&numOfAttributes,&_associated);

   int lin;
   lin = 3;

   if ( _classFlag != 0 ) {
       _associated = 0; // for objects belonging to a class, this is taken 
                        // from the class and anything comming from the translator
			// output is ignored
       _className = lines[3];
      lin = 4;
   }

    Name attribNm;
    if ( numOfAttributes > 0 ) {
       for ( int ia=0; ia<numOfAttributes; ia++) {
          attribNm = lines[lin];     // Read in the atributes
          _attributes += attribNm;
          lin++;
       }
    }


   if ( _classFlag != 0 ) {   //---- The object belongs to a class
      SMIClass *ptnr;

      ptnr = static_cast<SMIClass*>(allSMIClasses.gimePointer(_className));

//      cout << "Object belongs to class : " << _className << "\n";

      int nl;
      nl = ptnr->numOfLines();
      char* pClassCode = new char[nl*MAXRECL];
      OBJLINE* clines = (OBJLINE*) pClassCode;

      for (int il=0; il<nl; il++) {  //---- Get the lines from the class
         ptnr->gimeLine(il,clines[il]);
//         cout << clines[il] << "\n";
      }
//  line 0 is *CLASS;  line 1 is the 'class name'; line 2 is num of attributes;
      int numOfClassAtributes;
      sscanf(clines[2],"%d",&numOfClassAtributes);

//      cout << "The class has " << numOfClassAtributes << " atributes \n";

      int icl;
      icl = 3;
      int ipom = 0;
      if ( numOfClassAtributes != 0 ) {  //--- Add the class atributes to object
         for ( int ica=0; ica<numOfClassAtributes; ica++) {
            attribNm = clines[icl]; _attributes += attribNm;
            if( attribNm == "ASSOCIATED" ){ipom=1;}
            icl++;
//            cout << attribNm << "\n";
         }
         numOfAttributes = numOfAttributes + numOfClassAtributes;
      }            
      if ( ipom == 1 ) { _associated = 1;}    // class atribute is stronger
      bodyLog(&clines[icl]);
      delete [] pClassCode;
   }
   else {   //---- The object does not belong to a class

      bodyLog(&lines[lin]);
   }

//   print_obj(_objectName);

    if ( dbg > 1 ) {
        if ( _objectParameters.numOfEntries() != 0 ) {
            cout << " Params : " << _objectParameters.numOfEntries() 
                << endl;
        }
    }

   return;
}
//---------------------- destructor  BF Feb 2020 --------------------
SMIObject::~SMIObject()
{
	State* pState;
	
	int numOfStates = _states.length();
	for (int i=0; i<numOfStates; i++)
	{
		pState = static_cast<State*>(_states.gimePointer(i));
		delete pState;
	}
	
	Action* pFunction;
	
	int numOfFunctions = _functions.length();
	for (int i=0; i<numOfFunctions; i++)
	{
		pFunction = static_cast<Action*>(_functions.gimePointer(i));
		delete pFunction;
	}

	return;
}
//------------------------------------------------------------------------------
void SMIObject::bodyLog( char lines[][MAXRECL]) {
//---
// This processes the object following the atributes
//---

	int lin = 0;

// If the object has any parameters, they should be here
	if ( !strcmp(lines[lin],"*PARAMETERS" ) ){
		lin++;
		
		Name parName,parVal;
		Name parType;

		for (;;) {
			if ( !strcmp(lines[lin],"*END_PARAMETERS") ) { lin++; break; }
			parType = lines[lin];
			lin++;
			parName = lines[lin];
			lin++;
			parVal = lines[lin];
			lin++;
			 
// the following line is to make it compatible with older versions of SOBJ		
			 if (parVal == "&nodefault") parVal = paramcons::noval;			  
			
			if ( parVal == paramcons::noval )
			{
				Param tempPar(parName,paramcons::noval,parType);
				_objectParameters.addParam(tempPar);
			}
			else
			{
				Param tempPar(parName,parVal,parType);
				_objectParameters.addParam(tempPar);
			}
		}
	}

// If the object has any functions declared, they should be here
	if ( !strcmp(lines[lin],"*ACTION" ) ){
		
		for (;;) 
		{
			if (  !strcmp(lines[lin],"*STATE") ||
			      !strcmp(lines[lin],"*END_OBJECT") ) { break; }
			if (  !strcmp(lines[lin],"*ACTION") )
			{
				Name functionNm = lines[lin+1];
				int fun_lines;  //  number of function lines
				Action* pFunction = new Action
				               (&lines[lin], functionNm, fun_lines,this,0);
				assert ( pFunction != 0 );
				int flg = _functions.add(functionNm,pFunction);
				if ( flg != 1 ) {
					cout << " *** Error: Object :" << _objectName <<
					" has function declared twice" << endl;
					Alarm::message("FATAL",_objectName,"Initialisation error");
				}

//        cout << " No of function lines : " << fun_lines << "\n";
			lin = lin + fun_lines - 1;	
			}
			else { 
				cout << "Strange sequence " << lines[lin] << " encountered\n";
				Alarm::message("FATAL",_objectName,"Initialisation error");
			} 
			lin++;
		}
	}


//-----   establish the begining and end of the State section
	int ibegStSec = lin;
	int iendStSec;
	getLimitsOfStateSection(lines, ibegStSec, iendStSec);
//-----
    State* pState;
	int dead_state_declared = 0;
	int initial_state_declared = 0;
	int undeclared_state_declared = 0;

	for ( int ln=ibegStSec; ln<=iendStSec; ln++) {

		if (!strcmp(lines[ln],"*STATE")) {
			int st_lines;  //  and the number of state lines
			pState = new State(&lines[ln],this, &st_lines);
			assert ( pState != 0 );
			int flg = _states.add(pState->stateName(),pState);
			if ( flg != 1 ) {
				cout << " *** Error: Object :" << _objectName <<
				" has state " << pState->stateName() << "declared twice" <<
				 endl;
				Alarm::message("FATAL",_objectName,"Initialisation error");
			}

			if ( pState->deadState() ) {dead_state_declared = 1;}
			if ( pState->initialState() ) {initial_state_declared = 1;}

//        cout << " No of state lines : " << st_lines << "\n";
			ln = ln + st_lines - 1;
		}
		else { 
			cout << "Strange sequence " << lines[ln] << " encountered\n";
			Alarm::message("FATAL",_objectName,"Initialisation error");
		}

	}

   
// For associated objects we insist that they have all 3 special states.
// if they have not been declared, we create them.
    if ( _associated &&  ( dead_state_declared == 0 ||
                            initial_state_declared == 0 ||
                            undeclared_state_declared == 0 ) )
    {
		createAdditionalStates(dead_state_declared,
                            initial_state_declared,
                            undeclared_state_declared);
	}


  return;
}
//---------------------------------------------------------------------------
void SMIObject::getLimitsOfStateSection
                     ( char lines[][MAXRECL], int& ibegStSec, int& iendStSec )
{

//  ibegStSec  on input it is the line inx following 
//	           the parameter section
//  iendStSec  end of the state section
/**
  After parameter section there is State section. this consists of individual
  states each starting with *STATE and ending with *END_STATE
  After the state section there is just one line i.e. *END_OBJECT or *END_CLASS
  We used to have subobjects. For the legacy reasons Translator, for associated
  objects, still outputs some remnants of this i.e. : at the beginning of the 
  section it puts *SUBOBJECT followed by &DUMMY and at the end *END_SUBOBJECT
  see fig
  (in future, Translator will have removed. However we must be able to accept
  the old Translator output as well)
  This method finds the proper beginning and the end of the state section so
  that the 'bodyLog' method can ignore this nonsence.
  fig:
  *SUBOBJECT
  &DUMMY
  *STATE
  ...
  *END_STATE
  ...
  *STATE
   ...
  *END_STATE
  *END_SUBOBJECT
  *END_OBJECT (or *END_CLASS)
*/    
    int iobjEnd; // end of the object section

    Name line;

    for (int i=ibegStSec; ; i++)
    {
        line = lines[i];
        if  ( line == "*END_OBJECT" || line == "*END_CLASS")
        { iobjEnd = i; break; }
    }
    iendStSec = iobjEnd-1;

    Name lineibegStSec, lineiendStSec;
    lineibegStSec = lines[ibegStSec];
    lineiendStSec = lines[iendStSec];

//    cout << endl << " Object: " << _objectName << " Beg State Section: "
//                                       << ibegStSec << " " << lineibegStSec
//                                       << " End State Section: "
//                                       << iendStSec << " " << lineiendStSec
//                                       << endl;
    
    if ( lineibegStSec == "*SUBOBJECT")
    {
        Name nextLine = lines[ibegStSec+1];
        if ( !(nextLine == "&DUMMY") )
        {
            cout << " *** Error: Object :" << _objectName << endl
            << " after $SUBOBJECT there has to follow &DUMMY" << endl;
            return;
            Alarm::message("FATAL",_objectName,"Initialisation error");
        }
        if ( !(lineiendStSec == "*END_SUBOBJECT") )
        {
            cout << " *** Error: Object :" << _objectName << endl
            << " Subobject section has to finish with *END_SUBOBJECT" << endl;
            return;
            Alarm::message("FATAL",_objectName,"Initialisation error");
        }
        ibegStSec = ibegStSec + 2;
        iendStSec = iendStSec - 1;
        
        lineibegStSec = lines[ibegStSec];
        lineiendStSec = lines[iendStSec];
// cout << endl << " Section redefined :   "  << " Beg State Section: "
//                                      << ibegStSec << " " << lineibegStSec
//                                      << " End State Section: "
//                                      << iendStSec << " " << lineiendStSec
//                                     << endl;
    }

    if ( !(lineibegStSec == "*STATE") )
    {
        cout << " *** Error: Object :" << _objectName << endl
        << " State section has to start with *STATE" << endl;
        return;
        Alarm::message("FATAL",_objectName,"Initialisation error");
    }

    if ( !(lineiendStSec == "*END_STATE") )
    {
        cout << " *** Error: Object :" << _objectName << endl
        << " State section has to end with *END_STATE" << endl;
        return;
        Alarm::message("FATAL",_objectName,"Initialisation error");
    }
    return;
}
//---------------------------------------------------------------------------
void SMIObject::createAdditionalStates(int dead_state_declared,
					int initial_state_declared,
					int undeclared_state_declared)
{
	State* pState;
	
    if ( dead_state_declared == 0 ) {
        pState = new State("DEAD",this);
        assert ( pState != 0 );
        int flg = _states.add("DEAD",pState);
        if ( flg != 1 ) {
            cout << " *** Error: Object :" << _objectName <<
                " has state DEAD not declared as dead_state" << endl;
                Alarm::message("FATAL",_objectName,"Initialisation error");
        }
    }

    if ( initial_state_declared == 0 ) {
        pState = new State("&INITIAL",this);
        assert ( pState != 0 );
        int flg = _states.add("&INITIAL",pState);
        if ( flg != 1 ) {
            cout << " *** Error: Object :" << _objectName <<
                " has state &INITIAL not declared as initial_state" << endl;
                Alarm::message("FATAL",_objectName,"Initialisation error");
        }
    }

    if ( undeclared_state_declared == 0 &&
        !_objectName.exists("::&ALLOC") )
    {   // will create default 'undeclared_state', unless it is a remote ALLOC obj.
        pState = new State("&UNDECLARED_STATE",this);
        assert ( pState != 0 );
        int flg = _states.add("&UNDECLARED_STATE",pState);
        if ( flg != 1 ) {
            cout << " *** Error: Object :" << _objectName <<
                " has state &UNDECLARED_STATE not declared as undeclared_state" << endl;
            Alarm::message("FATAL",_objectName,"Initialisation error");
        }
    }
    return;
}
//-----------------------------------------------------------------------------
void SMIObject::whatAreYou() {

	cout << endl << " ------------------------------------" << endl
	<< " Object : " << _objectName << " ";
	int noAttr = _attributes.length();
	
	for ( int i=0; i<noAttr; i++ )
	{
		cout << "/" << _attributes[i];
	}
	cout << endl;
	
	int noPar = _objectParameters.numOfEntries();
	if ( noPar > 0)
	{
		Name parString = _objectParameters.outParmsString(2);
		parString.removeLastChar();
		char* ptnr = parString.getString();
		
		cout << " Parameters: " 
		     << ptnr+1 << endl;
	}
	
	int noFun = _functions.length();
	for ( int i=0; i<noFun; i++ )
	{
		Action* pFunction = static_cast<Action*>(_functions.gimePointer(i));
		pFunction->whatAreYou();
	}
	
	int noStates = _states.length();
	for ( int i=0; i<noStates; i++ )
	{
		State* pState = static_cast<State*>(_states.gimePointer(i));
		pState->whatAreYou();
	}
}
//------------------------------  startUP  -------------------------------------
// Logical objects will determine their initial state. That is either the state
// that is marked /initial or the first state. They will then inject this state
// on the stateQ and will wait for it to come back.
//
// Associated objects will also startup in initial state which is either
// declared /initial_state or it will be called &INITIAL

void SMIObject::startUp( Queue_TwoNames* pStateQ, CommHandler *pCommHandler){

	int dbg; Options::iValue("d",dbg);

	_pStateQ = pStateQ;
	_pCommHandler = pCommHandler;
   
	Name curr_state;
//
//---  First determine what is the object's initial state
//
	if ( _associated == 0 ) {                          //---  Logical object
//------ Determine if there is the initial state declared
//       if not, choose the first state

		_currStateInx = stateInxInitialState();
		if (_currStateInx < 0 ) { _currStateInx = 0; }
    	curr_state = currentState();  

		if ( dbg > 0 || Msg::dllmsgRxPtr != 0 ) {
			char timeStr[30];
			Name message;
			message = "<";
			message += _objectName;
			message += "> starting up in state <";
			message += curr_state;
			message += ">";
			if ( Msg::dllmsgRxPtr != 0 ) (*Msg::dllmsgRxPtr)(message.getString());
			if (dbg > 0)
			{
				gime_date_time(timeStr);
		  		cout << timeStr << " - " << message.getString() << endl;
			} 
		}
      
//---  add to the State Q
		TwoNames  stateInfo;
		stateInfo.set(_objectName,curr_state);
		pStateQ->add(stateInfo);
	}
	else {                          //---  Associated object
		_currStateInx =stateInxInitialState();
		if ( _currStateInx < 0 ) 
		{
			print_obj(_objectName);
			cout << " Initial state does not exists " << endl; cout.flush();
			Alarm::message("FATAL",_objectName,"Initialisation error");
		}

		curr_state = currentState();

		if ( dbg > 0 ) { 
			print_obj(_objectName);
			cout << " starting up in state :" << curr_state << endl;
		}
	}
   
	strcpy(_internalState,"WaitSetState");
	return;
}

//=========================  setStateDiagMessageNormal ======================
void SMIObject::setStateDiagMessageNormal(const Name& inStateString) const
{

	int dbg; Options::iValue("d",dbg);

	if ( dbg > 0 || Msg::dllmsgRxPtr != 0 ) {
		char timeStr[30];
		Name message;
		message = "<";
		message += _objectName;
		message += "> in state <";
		message += inStateString;
		message += ">";
		if ( Msg::dllmsgRxPtr != 0 ) (*Msg::dllmsgRxPtr)(message.getString());
		if (dbg > 0)
		{
			gime_date_time(timeStr);
		  	cout << timeStr << " - " << message.getString() << endl;
		} 
	}
	return;
}
void SMIObject::setStateDiagMessageConverted(const Name& inStateString) const
{

	int dbg; Options::iValue("d",dbg);

	if ( dbg > 0 || Msg::dllmsgRxPtr != 0 )
	{
		char timeStr[30];
		Name message;
		message = "<";
		message += _objectName;
		message += "> in state <";
		message += currentState();
		message += ">";
		message += " converted from <";
		message += inStateString;
		message += ">";
		if ( Msg::dllmsgRxPtr != 0 ) (*Msg::dllmsgRxPtr)(message.getString());
		if (dbg > 0)
		{
			gime_date_time(timeStr);
	  		cout << timeStr << " - " << message.getString() << endl;
		} 
	}

	return;
}
//*****************************  setState  *********************************

void SMIObject::setState( const Name& statestr) { 

//cout << " SMIObject::setState called  object : " << _objectName << endl
//     << " state string: " << statestr 
//     << " Internal state : " << _internalState << endl;
	
	int dbg; Options::iValue("d",dbg);

	checkForLoop();
	
	State* pState;    

	if ( _objectName.exists("::&ALLOC") )
	{
		setStateDiagMessageNormal(statestr);
		setStateAlloc(statestr);
		return;
	}

//----- if there are parameters, process them
	Name stateNm,paramStr;
	split(statestr,stateNm,paramStr); 

	if ( stateNm == "$DEAD" ) {}
	else {
		if ( _associated != 0 ) { // Only external objects
//                                 carry param string
			if( _objectParameters.setFromParmString(paramStr) != 1 ) {
				cout << " Parameter string " << paramStr << endl
				<< " incompatible with the object parameters " << endl;
				cout.flush();
				Alarm::message("FATAL",_objectName,
				"method setState() error");
			}
		}
	}
//-------

	if (_associated == 0) {  //---------------------------- logical object
//                            For logical object, the set State call is 
//                            caused by the object previously injecting
//                            its state onto the state queue and setting its
//                            internal state to "WaitSetState"          
		if ( strcmp(_internalState,"WaitSetState") ) {
			cout << " Object not in the correct internal state";
			cout.flush();
			Alarm::message("FATAL",_objectName,"method setState() error");
		}
		
		strcpy(_internalState,"Idle");
		pState = static_cast<State*>(_states.gimePointer(_currStateInx));
		
		if ( pState->stateName() == stateNm) {}
		else {
			cout << " Logic failure...the current state is supposed to be :"
			<< pState->stateName() << "/n";
			cout.flush();
			Alarm::message("FATAL",_objectName,"method setState() error");
		}
		
		setStateDiagMessageNormal(statestr);
	}                      //--------------------

	else {                 //---------------------- associated object
//                          for these objects setState is the conseq.
//                          of either an action having been sent to
//                          the remote proxy or it is a spontaneous
//                          change of state.
	
		if ( stateNm == "$DEAD" ) {  //--- $DEAD state indicating that the
								 //    conection was lost
         
			_currStateInx = stateInxDeadState();
			if ( _currStateInx == -1 ) {
				print_obj(_objectName);
				cout << " Dead state does not exists " << endl; cout.flush();
				Alarm::message("FATAL",_objectName,
				"method setState() error");
			}
         
			setStateDiagMessageConverted(statestr);
		}                           //----- end $DEAD state
		
		else if ( stateInx(stateNm)  == -1 )
		{  //  state of this name is not declared

			_currStateInx = stateInxUndeclaredState();
			if ( _currStateInx == -1 ) {
				print_obj(_objectName);
				cout << " Undeclared state does not exists " << endl;
				Alarm::message("FATAL",_objectName,
				"method setState() error");
			}
         
			setStateDiagMessageConverted(statestr);
		}
		else {
			_currStateInx = stateInx(stateNm);
			setStateDiagMessageNormal(statestr);  //------ normal state
		}                                //---------- end normal state

	}


// Having accepted setState, the only possible internal state is "Idle"
// unless it was locked associated object
	if ( strcmp(_internalState,"Locked") ) {
		strcpy(_internalState,"Idle");
	}
	else {
//debug beg
		if ( dbg > 1 )
		{
			cout << " ********************* warning ****************" << endl;
			print_obj(_objectName);
			cout << " is locked ! " << endl;
		}
//debug end
	}
// If either of the actions queues has actions, make it executable
	if ( !_hpActionQ.isEmpty() || !_actionQ.isEmpty() ) {
		executableObjectQ.add(_objectName);
	}

	return;
}
//-------------------------------  execute  ----------------------------------

void SMIObject::execute() {

	int dbg; Options::iValue("d",dbg);

    State* pState;
//
// Are there any actions queued?
//
   if (_hpActionQ.isEmpty()  && _actionQ.isEmpty() ) {
#ifdef DEBUG
      print_obj(_objectName);
      cout << " no actions queued \n";        
      cout.flush();
#endif
      return;
   }
//
// First check if in a correct internal state  i.e. "Idle"
   if ( strcmp(_internalState,"Idle") ) {
#ifdef DEBUG
      print_obj(_objectName);
      cout << " is in internal state : " << _internalState << "\n";
      cout.flush();
#endif
      return;  // The action is left on the queue so it will be executed
//                sometimes in the future
   }
//---- Object is Idle and there are actions queued
   Name actionstr;
   int hpFlag = 0;
//
   if( !_hpActionQ.isEmpty() ) {    // Hight priority actions present
      hpFlag = 1;
      actionstr = _hpActionQ.remove();
   }
   else  { 
      actionstr = _actionQ.remove();
   }
//--------------
   if ( actionstr.exists("&lock/")) {
        _lock++;
        strcpy(_internalState,"Locked");
        Name clientObject;
        actionstr.element(1,'/',clientObject);        

        if ( dbg > 2 ) {
            print_obj(_objectName);
            cout << "(" << this << ")"
	    << " locked by  " << clientObject << "\n";
        }
	
	SMIObject* pClientObject = 
		(SMIObject*)(allSMIObjects.gimePointer(clientObject));
	pClientObject->reportingLockedObject(_objectName);
        return;
   }

	if ( dbg > 0 || Msg::dllmsgRxPtr != 0 ) {
		char timeStr[30];
		Name message;
		message = "<";
		message += _objectName;
		message += ">";
		Name actionNm;
		actionstr.element(0,'/',actionNm);
		pState = static_cast<State*>(_states.gimePointer(_currStateInx));
		if ( !pState->actionExists(actionNm) )
		{
			message += " can not execute action <";
			message += actionNm;
			message += "> in state <";
			message += pState->stateName();
			message += ">";
			if ( Msg::dllmsgRxPtr != 0 ) (*Msg::dllmsgRxPtr)(message.getString());
			if (dbg > 0)
			{
				gime_date_time(timeStr);
		  		cout << timeStr << " - " << message.getString() << endl;
			} 
		}
		else
		{
			Name whatKind = "";
			if ( hpFlag == 1 ) { whatKind = " high priority"; }
			message += " executing";
			message += whatKind;
			message += " action <";
			message += actionstr;
			message += ">";
			if ( Msg::dllmsgRxPtr != 0 && hpFlag == 0 ) (*Msg::dllmsgRxPtr)(message.getString());
			if ( (dbg == 1 && hpFlag == 0) || (dbg == 2 && hpFlag == 0) || dbg >= 3)
			{
				gime_date_time(timeStr);
		  		cout << timeStr << " - " << message.getString() << endl;
			} 
		}
	}
      

   _currentActionString = actionstr;


//
//------------
   int flag;

   if ( _associated == 0 ){             // Logical object
        _pCommHandler->publishBusy(this,actionstr.getString());
        executeState();
        return;
   }
   else {                               // Associated object
         pState = static_cast<State*>(_states.gimePointer(_currStateInx));
         flag = pState->execute_assoc( _currentActionString, _pCommHandler);
         if ( flag == -1 ) {
              _pCommHandler->invalidAction(this);
// Make the object executable again if there actions queued
              if ( !_hpActionQ.isEmpty() || !_actionQ.isEmpty() ) {
                   executableObjectQ.add(_objectName);
              }
              return;
         }
         strcpy(_internalState,"WaitSetState"); // valid action was successfully
//                                                 dispatched
#ifdef DEBUG
         print_obj(_objectName);
         cout << " sends action to the remote object \n";
         cout.flush();
#endif
   }

 return;

}
//
//
//-------------------------------------------------------------------------
//
void SMIObject::resume() {

	int dbg; Options::iValue("d",dbg);

//debug beg
	if ( dbg > 3 )
	{
		print_obj(_objectName);
		cout << " resuming action " << _currentActionString << endl;
	}
//debug end

   if ( strcmp(_internalState,"Suspended") ) {
       cout << " the object is not in Suspended internal state\n";
       cout.flush();
       Alarm::message("FATAL",_objectName,
          "method resume() error");
   }
	_suspendedInsType = "";
	_pointerToTypeISuspendedIF = NULL;
	_pointerToSuspendedWAIT = NULL;
	_pointerToSuspendedSLEEP = NULL;

   executeState();
   return;   
}

//========================== executeWhensFromFlagList =====  BF  May 2008  ==============

void SMIObject::executeWhensFromFlagList
                ( const std::vector<int>& flagList ) 
{
    
/* This makes sence only for Logical objects */
	if (_associated != 0) { return;}
   
	State* pState;

/*  check if in a correct internal state  */
	if ( strcmp(_internalState,"Idle") && strcmp(_internalState,"Locked")) {
//      cout << "  Whens for object : " << _objectName <<
//              " ...is not in a stable state to execute whens \n";
		return;  
	}
//---- Object is either Idle or Locked

	if ( !_hpActionQ.isEmpty() )
	{ //	cout << " There is already HP action queued" << endl;
		return;
	}

	pState = static_cast<State*>(_states.gimePointer(_currStateInx));
//   cout << "  Whens for object : " << _objectName <<
//           "  in state : " << pState->stateName() << "\n";
	pState->executeWhensFromFlagList( flagList );
	return;
}

//========================== executeAllWhens =====  BF  May 2008  ==============

void SMIObject::executeAllWhens() 
{
 
	int dbg; Options::iValue("d",dbg);

        if ( dbg > 5 ) {
		cout << 
		"start=================SMIObject::executeAllWhens()========"
		<< endl << endl; 
	}
/* This makes sence only for Logical objects */
	if (_associated != 0) { return;}
   
	State* pState;

/*  check if in a correct internal state  */
	if ( strcmp(_internalState,"Idle") && strcmp(_internalState,"Locked")) {
//      cout << "  Whens for object : " << _objectName <<
//              " ...is not in a stable state to execute whens \n";
		return;  
	}
//---- Object is either Idle or Locked

	if ( !_hpActionQ.isEmpty())
	{ //	cout << " There is already HP action queued" << endl;
		return;
	}

	pState = static_cast<State*>(_states.gimePointer(_currStateInx));
//   cout << "  Whens for object : " << _objectName <<
//           "  in state : " << pState->stateName() << "\n";
	pState->executeAllWhens();
	return;
}

//*****************************  queueAction *******************************

void SMIObject::queueAction ( Name& actionstr ) {

	int dbg; Options::iValue("d",dbg);

//debug beg
if ( dbg > 4 )
{
	print_obj(_objectName);
	cout  << " queuing actionString :" << actionstr << endl;
}
//debug end

   _actionQ.add(actionstr);
//   _actionQ.out();

   executableObjectQ.add(_objectName);

 return;

}


void SMIObject::queueAction ( char* actionstr ) {
   Name temp;
   temp = actionstr;
   queueAction(temp);
} 

//*****************************  queueHpAction *******************************

void SMIObject::queueHpAction ( Name& action_nm ) {
//   print_obj(_objectName);
//   cout  << " queuing high priority action :" << action_nm << "\n";
//   cout.flush();

   _hpActionQ.add(action_nm);

   executableObjectQ.add(_objectName);

   return;
}

void SMIObject::queueHpAction ( char* action_nm ) {
   Name temp;
   temp = action_nm;
   queueHpAction(temp);
   return;
}


//*****************************  lock   *******************************

int SMIObject::lock ( char* objname ) {
#ifdef DEBUG
   cout << "  Object : " << _objectName;
   cout.flush();
#endif
   if ( _hpActionQ.isEmpty() && _actionQ.isEmpty() ) {   // action queues empty

        if ( !strcmp(_internalState,"Idle")  || 
             !strcmp(_internalState,"Locked")) {
             _lock++;
             strcpy(_internalState,"Locked");

//             print_obj(_objectName);
//             cout << " locked by " << objname << "\n";
//             cout.flush();

             return 1;
        }
   }

   Name lockAction = "&lock/";
   Name objnameUpCase = objname;
   objnameUpCase.upCase();
   lockAction += objnameUpCase;
    _actionQ.add(lockAction);
    executableObjectQ.add(_objectName);

//    print_obj(_objectName);
//    cout << " lock queued by " << objname << "\n";
//    cout.flush();

    return 0;
}


//*****************************  unlock   *******************************

void SMIObject::unlock ( ) {

   if ( strcmp(_internalState,"Locked") ) {
      print_obj(_objectName);
      cout << " is not locked\n";
      cout.flush();
      Alarm::message("FATAL",_objectName,"method unlock() error");
   }

   _lock = _lock - 1;

   if ( _lock == 0 ) {
        strcpy(_internalState,"Idle");
   }

   executableObjectQ.add(_objectName);

//   print_obj(_objectName);
//   cout << " unlocking, Internal state after:" << _internalState << endl;
   cout.flush();

   return ;

}

Name& SMIObject::name () {
return _objectName;
}
//------------------------ associated() --------------  B. Franek  4-Feb-1999 -
  bool SMIObject::associated() const {
     return _associated;
  }
//-------------------------------------------------------------
  bool SMIObject::hasAssociatedAttribute() const {
      int numAt = _attributes.length();
      if ( numAt <= 0 ) { return 0; }
      
      for ( int i=0; i < numAt; i++ ) {
          if ( _attributes[i] == "ASSOCIATED" ) { return 1; }
      }
      
      return 0;
  }
//************************************************************************
Name SMIObject::externDomain () const {

  Name domain = "\0", tempName;

  tempName = _objectName;

  char* ptnr = strstr(&tempName[0],"::");

  if (ptnr) {
     *ptnr = '\0';
     domain = &tempName[0];
  }
  return domain;
}
//-------------------------------------------------------------------------

Name SMIObject::currentState() const {
    State* pState;
    pState = static_cast<State*>(_states.gimePointer(_currStateInx));
    return pState->stateName(); 
}

//*****************************  currentState  ****************************

State* SMIObject::pCurrentState() const {

    return  static_cast<State*>(_states.gimePointer(_currStateInx));

}
//*****************************  currentState  ****************************

int SMIObject::currentState ( Name& stateNm ) {

//  check if in a correct internal state  
   if ( isBusy() ) {
      return -1;   // The object is busy  
   }

    State* pState;
    pState = static_cast<State*>(_states.gimePointer(_currStateInx));

    
    stateNm = pState->stateName();

   return 1;

}
//-------------------------------  objCurrState  BF Nov 2020 --------------------------
Name SMIObject::objCurrState() const
{
    if ( isBusy() ) return "&busy";

    State* pState;
    pState = static_cast<State*>(_states.gimePointer(_currStateInx));

    return pState->stateName();    
}
//------------------------------  objCurrAction  BF Nov 2020  ---------------------
Name SMIObject::objCurrAction() const
{
    Name actionNm;

    if ( !isBusy() ) return "&none";

    _currentActionString.element(0,'/',actionNm);
    return actionNm;
}
//================================ isBusy =========== BF 03-Oct-2004 =======
bool SMIObject::isBusy() const {
    if ( strcmp(_internalState,"Idle") && strcmp(_internalState,"Locked")) {
      return 1;   // The object is busy  
    }
    return 0;
}
//===========================================================================
char* SMIObject::gimeInternalState()
{
	return _internalState;
}
//================================ hpActionsPending() ===== BF 03-Oct-2004 ===
bool SMIObject::hpActionsPending() {
    if (_hpActionQ.isEmpty()) {return 0;}
    return 1;
}
//---------------------------  executeState  --------------------------------
void SMIObject::executeState() {
//
    State* pState;
    pState = static_cast<State*>(_states.gimePointer(_currStateInx));
        int flag;
        Name newState;
//
        flag = pState->execute(_currentActionString, newState);
//           flag is either -1..action not found
//                           1..action terminated
//                           2..action suspended
//             cout << " flag : " << flag << "  newState : |" 
//                                      << newState << "| \n";  
//--------------
        switch (flag) {                             
        case 1 :                                     //--- action terminated
//             print_obj(_objectName);
//             cout << " action " << _currentActionString 
//                  << " terminated in state " << newState << endl; cout.flush();
//           first find out which state it is
             _currStateInx = stateInx(newState.getString());
//             cout << " _currStateInx = " << _currStateInx << " \n";
             if ( _currStateInx == -1 ) {    
                print_obj(_objectName);
                cout << " Action terminated with state :|" << newState << "| \n"
                     << " The object however does not have such state \n";
                cout.flush();
                Alarm::message("FATAL",_objectName,
		  "method executeState() error");
             }
// add the state change (can be the same state) to the state q
//             cout << " StateQ before : \n";
//             _pStateQ->out();
             { char objnm_temp[MAXRECL];
             _objectName.whatAreYou(objnm_temp, MAXRECL);
             _pStateQ->add(objnm_temp,newState.getString());}
#ifdef DEBUG
              print_obj(_objectName);
              cout << " end state " << newState << " ->StateQ \n";
              cout.flush();
#endif
//             cout << " \n StateQ after : \n";
//             _pStateQ->out();
// and wait for it
             strcpy(_internalState,"WaitSetState");
             break;
        case 2 :                                       //----- suspended
//             print_obj(_objectName);
//             cout << " action " << _currentActionString 
//                  << " suspended " << endl; cout.flush();
             strcpy(_internalState,"Suspended");
             break;
        case -1 :                                      //------- action not exists
              _pCommHandler->invalidAction(this);
// Make it executable again if there actions queued
              if ( !_hpActionQ.isEmpty() || !_actionQ.isEmpty() ) {
                   executableObjectQ.add(_objectName);
              }
              return;
              break;
        default :
             print_obj(_objectName);
             cout << " flag = " << flag << "  this is an ilegal value \n";
             cout.flush();
             Alarm::message("FATAL",_objectName,"method executeState() error");
             break;
        }
//
        return;
}

//------------------------ parmString -----------  B.Franek   4-Feb-1999 ---
   Name SMIObject::parmString() const {
      Name tempName;
      _objectParameters.buildParmString(tempName);
      return tempName;
   }

void SMIObject::parmString(Name& str) const {

     _objectParameters.buildParmString(str);
}

void SMIObject::parmString(char* str, int mxsize) const {
    Name tempName;

    parmString(tempName);
    tempName.whatAreYou(str,mxsize);
}

Parms* SMIObject::pObjectParameters() {
    return &_objectParameters;
}

void SMIObject::split
           ( const Name& statestr, Name& stateNm, Name& paramStr) const {
    Name ststr = statestr;

    char* ptnr = strstr(&ststr[0],"/");

    if ( ptnr ) {  // state string contains /   -> it has parms
       *ptnr = '\0';
       stateNm = ststr;
       *ptnr = '/';
       paramStr = ptnr;
    }
    else {
       stateNm = statestr;
       paramStr = "\0";
    }

    return;
} 

//----------------------------------------------------------------------------
  int SMIObject::stateInx( const Name& stname) const {

     int inx = -1;
     int numOfStates = _states.length();
     State* pState;
     
     for ( int i=0; i < numOfStates; i++ ) {
        pState = static_cast<State*>(_states.gimePointer(i));
        if ( stname == pState->stateName() ) { inx = i; break; }
     }

     return inx;
  } 


  int SMIObject::stateInx( const char stname[]) const {
     Name temp = stname;

     int inx = stateInx(temp);

     return inx;
  }

//---------------------------------------------------------------------------
  State* SMIObject::pTaggedState (const Name& tagname) const {
    int numOfStates = _states.length();
    State* pState;
    
     for ( int i=0; i < numOfStates; i++ ) {
         pState = static_cast<State*>(_states.gimePointer(i));
        if ( tagname == pState->tagName() ) { return pState; }
     }

     return 0;
  }
//-----------------------------------------------BF 27-July-2009 -------------------

  int SMIObject::stateInxUndeclaredState() const {
     int numOfStates = _states.length();
     State* pState;
     
     for ( int i=0; i<numOfStates; i++ ) {
        pState = static_cast<State*>(_states.gimePointer(i));
        if ( pState->undeclaredState() ) { return i; }
     }

     return -1;
  }
//---------------------------------------------------------------------------

  int SMIObject::stateInxDeadState() const {
     int numOfStates = _states.length();
     State* pState;
     
     for ( int i=0; i<numOfStates; i++ ) {
        pState = static_cast<State*>(_states.gimePointer(i));
        if ( pState->deadState() ) { return i; }
     }

     return -1;
  }
//-----------------------------------------------------BF 11-Oct-1998-------

  int SMIObject::stateInxInitialState() const {
     State* pState;
     int numOfStates = _states.length();

     for ( int ist=0; ist < numOfStates; ist++ ) {
	pState = static_cast<State*>(_states.gimePointer(ist));
        if ( pState->initialState() ) {
           return ist;
        }
     }
     return -1;
  }

//-------------------------------------------------------------------------

  int SMIObject::stateInxTaggedState() const {
     Name tagname("&VARIABLE");
     int numOfStates = _states.length();
     State* pState;

     for ( int i=0; i < numOfStates; i++ ) {
         pState = static_cast<State*>(_states.gimePointer(i));
        if ( tagname == pState->tagName() ) { return i; }
     }

     return -1;
  }

//-------------------------------------------------------------------------

  void SMIObject::setStateAlloc( const Name& statestr ) {
     Name stateNm,paramStr;
     State* pState;

     split(statestr,stateNm,paramStr);

     if ( stateNm == "$DEAD" ) {
        _currStateInx = stateInxDeadState();
        if (_currStateInx < 0 ) {
           cout << " Dead state has to be declared " << endl; cout.flush();
           Alarm::message("FATAL",_objectName,
	     "method setStateAlloc() error");
        }
     }
     else {
        if ( stateNm == "NOT_ALLOCATED" ) {
           _currStateInx = stateInx(stateNm);
        }
        else {
           _currStateInx = stateInxTaggedState();
	   pState = static_cast<State*>(_states.gimePointer(_currStateInx));
           pState->changeName(stateNm);
        }
     }

//   Having accepted setState, the only possible internal state is "Idle"
//
     strcpy(_internalState,"Idle");
//
//   If either of the actions queues has actions, make it executable
     if ( !_hpActionQ.isEmpty() || !_actionQ.isEmpty() ) {
        executableObjectQ.add(_objectName);
     }

  }
//------------------------------------------ BF  9-Apr-2008 -------------------
int SMIObject::addClientWhen(const char* whenObjName, const char* whenStateName, int whenInx)
{
	return _clientWhens.markWhen(whenObjName, whenStateName, whenInx);
}
//------------------------------------------ BF  11-Apr-2008 --------------
int SMIObject::updateReferencedObjectsandSets() const
{
	int numOfStates = _states.length();
	int ist;
	State* pState;
	
	for ( ist=0; ist<numOfStates; ist++)
	{
		pState = static_cast<State*>(_states.gimePointer(ist));
//      cout << pState << "  " << pState->stateName() << "\n";
		pState->updateReferencedObjectsandSets();
	}

	return 1;
}
//------------------------------------------  Bf  11-Apr-2008  --------------
int SMIObject::printClientWhens() const
{
	int numClientObjects =_clientWhens.numObjects();
	if ( numClientObjects == 0 )
	{
		cout << " No clients " << endl;
		return 1;
	}
	
	_clientWhens.out(" ");
	return 1;
}
//------------------------------------------  BF  8-May-2008  -----------------
int SMIObject::printSetNamesBelongingTo() 
{
	int numOfSets = _setsIBelongTo.numOfEntries();
	if (numOfSets <= 0)
	{
		cout << " No sets registred" << endl;
		return 1;
	}
	
	//Name temp(" ");
	_setsIBelongTo.out(); cout << endl;
	return 1;
} 
//------------------------------------------  BF  14-May-2008  ----------------
int SMIObject::gimeCurrentClientWhens( ClientWhens& clientWhens ) 
{

	int dbg; Options::iValue("d",dbg);

        if ( dbg > 5 ) {
		cout <<
		"start================SMIObject::gimeCurrentClientWhens(..)===="
		<< endl << endl;
	}
 
	// It is assumed that clientWhens are declared in calling function.
	// First we include the object 'static' client whens.
	
	clientWhens.mergeIn(_clientWhens);

	// next we have to merge-in all the whens of the object sets to which
	// the current object belongs.
	
	int numOfSets = _setsIBelongTo.numOfEntries();
	if (numOfSets <= 0)
	{
		return 1;
	}
	
	// Loop now over the sets
	NmdPtnr set;
	SMIObjectSet* pObjSet;
	
	
	_setsIBelongTo.reset();	
	while ( _setsIBelongTo.nextItem(set) )
	{
		pObjSet = static_cast<SMIObjectSet*>(set.pointer());
		const ClientWhens& setClientWhens = pObjSet->gimeClientWhensRef();
		clientWhens.mergeIn( setClientWhens );
	}
	return 1;
}
//------------------------------------------------ BF Sep 2008 ---------------
void SMIObject::setPointerToTypeISuspendedIF( IfIns* pointer)
{
	_pointerToTypeISuspendedIF = pointer;
	
	_suspendedInsType = "IF";
	return;
}
//------------------------------------------------ BF Sep 2008 ---------------
IfIns* SMIObject::gimePointerToTypeISuspendedIF() const
{
	return _pointerToTypeISuspendedIF;
}
//------------------------------------------------ BF Sep 2008 ---------------
int SMIObject::removeQueuedLock(const Name& requestorName) 
{
	int dbg; Options::iValue("d",dbg);

//debug beg
if ( dbg > 5 )
{
	cout << endl << "start ===============  removeQueuedLock  of  " << requestorName  << endl;

	cout << " actionQ of " << _objectName << " before " << endl; _actionQ.out(); cout << endl;
}
//debug end

	Name requestorNameUpCase = requestorName;
	requestorNameUpCase.upCase();
	
	Name lockAction ="&lock/";
	lockAction += requestorNameUpCase;
	
	if ( !_actionQ.removeItem(lockAction) )
	{
		cout << " *** Internal error : "
		<< " Object : " << _objectName
		<< " failed to remove : " << lockAction << endl;
		Alarm::message("FATAL",_objectName,
	            "method removeQueuedLock() error");
	}

//debug beg
if ( dbg > 5 )
{	
	cout << endl << " actionQ after " << endl; _actionQ.out(); cout << endl;
	cout << "return ===============  removeQueuedLock  of  " << requestorName  << endl << endl;
}
//debug end

	return 1;	
}
//------------------------------------------------ BF Jan 2009 ---------------
void SMIObject::setPointerToSuspendedWAIT( WaitIns* pointer)
{
	_pointerToSuspendedWAIT = pointer;
	
	_suspendedInsType = "WAIT";
	return;
}
//------------------------------------------------ BF Jan 2009 ---------------
void SMIObject::setPointerToSuspendedWAIT_FOR( WaitForIns* pointer)
{
	_pointerToSuspendedWAIT_FOR = pointer;
	
	_suspendedInsType = "WAIT_FOR";
	return;
}
//------------------------------------------------ BF Jan 2009 ---------------
void SMIObject::setPointerToSuspendedSLEEP( SleepIns* pointer)
{
	_pointerToSuspendedSLEEP = pointer;
	
	_suspendedInsType = "SLEEP";
	return;
}
//----------------------------------------------------------------------------
Name& SMIObject::suspendedInsType()
{
	return _suspendedInsType;
}
//---------------------------------------------------------------------------
void SMIObject::reportingLockedObject(Name& lockedObject)
{
	int dbg; Options::iValue("d",dbg);

if (dbg > 5 )
{
	cout << endl << " start========== SMIObject(" << _objectName 
	     << ")::reportingLockedObject(" << lockedObject << ")" << endl;
}

	int flg;	
	if ( _suspendedInsType == "WAIT" )
	{
		flg =
		 _pointerToSuspendedWAIT->reportingLockedObject(lockedObject);
	}
	else if ( _suspendedInsType == "IF" )
	{
		flg =
		 _pointerToTypeISuspendedIF->reportingLockedObject(lockedObject);
	}
	else
	{
		cout << " *** Internal fatal error " << endl
		<< " method SMIObject::reportingLockedObject of object " << _objectName
		<< endl << " suspended instruction type " << _suspendedInsType
		<< " is not legal" << endl;
		Alarm::message("FATAL",_objectName,
	              "method reportingLockedObject() error");

	}
	
	if ( flg == 1)
	{  // this means that there are no more objects waiting for locks and
	   // the instruction can be executed.
		resumeHandler.objectReadyToResume(_objectName);
	}

if (dbg > 5 )
{
	cout << " end============= SMIObject::reportingLockedObject" << endl;
}	
	return;
}
//---------------------------------------------------------------------------
int SMIObject::reportingObjectRemovedFromSet
	               ( const Name& remObjName, const Name& setName)
{
	int dbg; Options::iValue("d",dbg);

if (dbg > 5 )
{
	cout << endl << " start========== SMIObject(" << _objectName 
	     << ")::reportingObjectRemovedFromSet" << endl
	      << remObjName << " removed from " << setName << endl;
}

	int flg;	
	if ( _suspendedInsType == "WAIT" )
	{
		flg =
		 _pointerToSuspendedWAIT->reportingObjectRemovedFromSet
		                    (remObjName,setName);
	}
	else if ( _suspendedInsType == "IF" )
	{
		flg =
		 _pointerToTypeISuspendedIF->reportingObjectRemovedFromSet
		                    (remObjName,setName);
	}
	else if ( _suspendedInsType == "WAIT_FOR" ){ return 0; }
	else if ( _suspendedInsType == "SLEEP" ) { return 0; }
	else
	{
		cout << " *** Internal fatal error " << endl
		<< " method SMIObject::reportingObjectRemovedFromSet to object "
		 << _objectName
		<< endl << " suspended instruction type " << _suspendedInsType
		<< " is not legal" << endl;
		Alarm::message("FATAL",_objectName,
	             "method reportingObjectRemovedFromSet() error");
	}

if (dbg > 5 )
{
	cout << " end============= SMIObject::reportingObjectRemovedFromSet" << endl;
}	
	return flg;
}
//---------------------------------------------------------------------------
int SMIObject::reportingChangedSets( NameList& changedSetsList )
{
	int dbg; Options::iValue("d",dbg);

if (dbg > 5 )
{
	cout << endl << " start========== SMIObject(" << _objectName 
	     << ")::reportingChangedSets" << endl;
	    Name off(" "); changedSetsList.out(off);
}

	int flg;
	
	if ( _suspendedInsType == "WAIT_FOR" ) {}
	else
	{ 
if (dbg > 5 )
{
	cout << " not a suspended WAIT_FOR " << endl;
	cout << " end============= SMIObject::reportingChangedSets" << endl;
}	
		return 0;
	}
// at the moment this is only for WAIT_FOR with an intention to use it also for others.

	Name setName;
	SMIObjectSet* pSet;
	
	changedSetsList.reset();
	while ( changedSetsList.nextItem(setName) )
	{
		pSet = (SMIObjectSet*)allSMIObjectSets.gimePointer(setName);
		if ( pSet->isObjectClient(_objectName) )
		{
			flg = _pointerToSuspendedWAIT_FOR->reportingChangedSets();
			break;
		}
	}	
	

if (dbg > 5 )
{
	cout << " end============= SMIObject::reportingChangedSets" << endl;
}	
	return flg;
}
//---------------------------------------------------------------------------
void SMIObject::youJoinedSet( SMIObjectSet *pSet)
{
	Name setName;
	setName = pSet->name();
		
	NmdPtnr temp(setName,pSet);
	
	_setsIBelongTo.add(temp);
	
	return;
}
//---------------------------------------------------------------------------
void SMIObject::youLeftSet( SMIObjectSet *pSet)
{
	Name setName;
	setName = pSet->name();
	
	NmdPtnr temp(setName,pSet);

	_setsIBelongTo.remove(temp);
	
	return;
}
//----------------------------------------------------------------------------
bool SMIObject::memberOfSet(const Name& setNm)
{

//cout << endl << "start============= SMIObject::memberOfSet ===========" << endl;
//cout << " object " << _objectName << "   set " << setNm << endl;

	void* ptnr = _setsIBelongTo.pointer(setNm);
	
//Name temp(" "); _setsIBelongTo.out(temp); cout << " ptnr " << ptnr << endl;
	
	if (ptnr) { return true; }
	else      { return false; }
	
}
//-----------------------------------------------------------------------------
int SMIObject::setObjectParameter(const Name& name, const Name& value)
{
	int flg;
	
	flg = _objectParameters.setParCurrValue(name,value);
	return flg;
}
//----------------------------------------------------------------------------------
void SMIObject::youHaveClientWF(Name& clientName)
{
	int dbg; Options::iValue("d",dbg);

	int iflg = _clientObjectsWF.add(clientName);
	
	if (!iflg) 
	{
		cout << endl << " **** FATAL ERROR *****" << endl
		<< "    SMIObject::youHaveClientWF(Name& clientName) " << endl  
		<< " Object : " << _objectName
		 << " adding already existing client Object : " << clientName << endl;
		 Alarm::message("FATAL",_objectName,
		 "method youHaveClienfWF() error");
	}
if ( dbg>5 )
{
cout << endl
<< " Object : " << _objectName
		 << " adding client Object : " << clientName << endl;
}
	return;
}
//----------------------------------------------------------------------------------
void SMIObject::unregisterClientWF(Name& clientName)
{
	int dbg; Options::iValue("d",dbg);

	int iflg = _clientObjectsWF.remove(clientName);
	
	if (!iflg) 
	{
		cout << endl << " **** FATAL ERROR *****" << endl
		<< "    SMIObject::unregisterClientWF(Name& clientName) " << endl  
		<< " Object : " << _objectName
		 << " removing nonexisting client Object : " << clientName << endl;
		 Alarm::message("FATAL",_objectName,
		 "method unregisterClientWF() error"); 
	}
if ( dbg >5 )
{
cout << endl
<< " Object : " << _objectName
	 << " removed client Object : " << clientName << endl;
}
	return;
}

//-----------------------------------------------------------------------------
NameList& SMIObject::gimeWFClients()
{
	return _clientObjectsWF;
}
//-----------------------------------------------------------------------------
bool SMIObject::areYouReadyToResume()
{
	return _pointerToSuspendedWAIT_FOR->areYouReadyToResume();
	
}
//-----------------------------------------------------------------------------
void SMIObject::checkForLoop()
{

//	int loopMaxChanges = 
//	  Options::loopMaxChanges; // max no of changes allowed per second
//	int loopMaxChangesFatal =
//	  Options::loopMaxChangesFatal; // no of changes per second grater than
//					// this is fatal
//	float loopMinDuration =
//	  Options::loopMinDuration; // min loop duration for it to be called
//	                            // serious loop

	int loopMaxChanges; // max no of changes allowed per second
	Options::iValue("loopMaxChanges",loopMaxChanges);

	int loopMaxChangesFatal; // no of changes greater than this is fatal
	Options::iValue("loopMaxChangesFatal",loopMaxChangesFatal);

	float loopMinDuration; // min loop duration of serious loop
	Options::fValue("loopMinDuration",loopMinDuration);
	
//cout << " loopMaxChanges : " << loopMaxChanges << " loopMaxChangesFatal : " << loopMaxChangesFatal
//     << " loopMinDuration : " << loopMinDuration << endl;

	if ( loopMinDuration < 0. ) { return;}
						
	float timeThreshold; // time corresponding to loopMaxChanges

	timeThreshold = 1./loopMaxChanges;

	_fprevStateTime = _fstateTime;
	
	_fstateTime = TimeHandling::floatTime();

	double secondsFromPrevState = _fstateTime - _fprevStateTime;
	
//	cout << " Time since the last state change : " << secondsFromPrevState << "  secs" << endl;

/*	
// ---------------  slow it down  --------------------------------------------	
	int npom = (int)((_fstateTime - (long)_fstateTime)*1000.);
	
//	cout << " npom" << npom << "  _fstateTime " << _fstateTime << endl;
	srand(npom);
	int ipomMax = rand() % 10000 + 1;
	ipomMax = 1000*ipomMax;
//	cout << ipomMax << endl;
	
	for ( int ipom=0; ipom<ipomMax; ipom++)
	{
		double temp = sqrt(npom+ipom);
	}
// --------------------------------------------------------------------------------
*/
	if ( !_loopStarted )
	{ // There is no lo loop in progress
		if ( secondsFromPrevState < timeThreshold ) 
		{   // potential loop starting, register also the start of the loop
			_loopStarted = true;
			_fstartOfLoop = _fstateTime;
			_numChngs = 0;
//			cout << " ---------" << endl
//			 << " Potential loop starting " << endl;
		}
		return;	
	}
	 
	// we have a loop already going
	double sincestseconds = _fstateTime - _fstartOfLoop;
	_numChngs++;
//	cout << " Loop in progress, no of state changes since beginning " << _numChngs << endl;
	int noOfChngsPerSec;
	if (_numChngs > 0 && (_numChngs%100) == 0)
	{
		noOfChngsPerSec = _numChngs/sincestseconds;
		if ( noOfChngsPerSec > loopMaxChangesFatal )
		{
			cout << " **** FATAL error  object " << _objectName << endl;
			cout << " Deadly loop detected. No. of state changes/ sec " << noOfChngsPerSec <<
			endl << " Killing State Manager" << endl;
			Alarm::message("FATAL",_objectName,"Deadly loop detected");
//			exit(2);
		}
	}
	
	if ( secondsFromPrevState > 2*timeThreshold ) { 
		if (_seriousLoopStarted)
		{
			_seriousLoopStarted = false;
			cout << " INFO  Loop ended  object  " << _objectName  << endl;  
		}
		_loopStarted = false; return;
	} // end loop
	
	// the loop is still going

	if ( sincestseconds > loopMinDuration )
	{  // this is a serious loop
		if (_seriousLoopStarted)
		{
			cout << endl << " WARNING!!  Loop is still in effect   object "
			 << _objectName << endl << endl;
			Alarm::message("WARNING",_objectName,"Loop still in effect");
		}
		else
		{
			_seriousLoopStarted = true;
			cout << endl << " WARNING!!  Loop detected   object "
			 << _objectName << endl << endl;
			Alarm::message("WARNING",_objectName,"Loop detected");
		}
		_fstartOfLoop = _fstateTime;  // by redefining the start we supress warnings
		_numChngs = 0;
	}
	return;
}
//----------------------------  BF  Jan 2020  -----------------------
NameList SMIObject::gimeServerObjects() const
{
	NameList serverObjects, stateServerObjects;
	
	int numOfStates = _states.length();
	int ist;
	State* pState;
	
	for ( ist=0; ist<numOfStates; ist++)
	{
		pState = static_cast<State*>(_states.gimePointer(ist));
//      cout << pState << "  " << pState->stateName() << "\n";
		stateServerObjects = pState->gimeServerObjects();
		serverObjects.add(stateServerObjects);
	}
	
	
	
	return serverObjects;
}
//----------------------------  BF  Jan 2020  -----------------------
NameList SMIObject::gimeServerSets() const
{
	NameList serverSets, stateServerSets;
	
	int numOfStates = _states.length();
	int ist;
	State* pState;
	
	for ( ist=0; ist<numOfStates; ist++)
	{
		pState = static_cast<State*>(_states.gimePointer(ist));
//      cout << pState << "  " << pState->stateName() << "\n";
		stateServerSets = pState->gimeServerSets();
		serverSets.add(stateServerSets);
	}
	
	
	
	return serverSets;
}
//----------------------------  BF  Jan 2020  --------------------------
int SMIObject::removeYourselfFromYourServerClients() const
{
	NameList serverObjects, serverSets;
	Name objName, setName;
	Name myObjName;
	myObjName = _objectName;
	
	serverObjects = gimeServerObjects();
	
	serverObjects.reset();
	
	while ( serverObjects.nextItem(objName) )
	{
		SMIObject* pObj = allSMIObjects.gimePointer(objName);
		pObj->removeObjectFromYourClients(myObjName);
	}
//-----    and now Sets  ------------------------------------
	serverSets = gimeServerSets();
	
	serverSets.reset();
	
	while ( serverSets.nextItem(setName) )
	{
		SMIObjectSet* ptnSet =
		      (SMIObjectSet*)allSMIObjectSets.gimePointer(setName);
		ptnSet->removeObjectFromYourClients(myObjName);
	}
		
	
	return 1;
}
//-----------------------------  BF  Jan 2020  --------------------------
int SMIObject::removeObjectFromYourClients( const Name& objName )
{
	_clientWhens.removeClientObject(objName);
	return 1;
}
//----------------------------------------------------------

Action* SMIObject::gimePointerToFunction(const Name funNm) const
{
	Action* pFunction;
	pFunction = static_cast<Action*>(_functions.gimePointer(funNm));
	return pFunction;
}
