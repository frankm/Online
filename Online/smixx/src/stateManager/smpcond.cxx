//------------------------  SmpCond  Class ------------------------------------
#include "smpcond.hxx"
//------------------------------------------------
//                                                         B. Franek
//                                                         01-Aug-1996
//                                                  rewritten 13-Nov-2003
// Copyright Information:
//      Copyright (C) 1996-2003 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------------
SmpCond::SmpCond()
: _type(-1),
  _frozen(0),
  _pParentAction(0)
{
	return;
}
//-------
SmpCond::SmpCond(Action* pAct)
: _type(-1),
  _frozen(0),
  _pParentAction(pAct)
{
	return;
}
//----------------  Destructor  BF Mar 2020  -------------------------------
SmpCond::~SmpCond()
{
	return;
}
//--------------------------------------------------------------------------
int SmpCond::type() const {
	return _type;
}






