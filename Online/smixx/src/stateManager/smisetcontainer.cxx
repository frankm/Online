//
//                                                               July 2018
//                                                               B. Franek
// Copyright Information:
//      Copyright (C) 1996-2018 CCLRC. All Rights Reserved.
//-----------------------------------------------------------------------------
#include "smisetcontainer.hxx"
#include "nmdptnr.hxx"

SMISetContainer::SMISetContainer()
: _members(), _inxFirstOn(-1), _currentInx(-1)
{
	return;
}
	
SMISetContainer::SMISetContainer(const SMISetContainer& source)
: _members(source._members), _inxFirstOn(source ._inxFirstOn),
  _currentInx(source._currentInx)
{
	return;
}
//-----------------  Destructor  BF Mar 2020  ---------------------
SMISetContainer::~SMISetContainer() { return; }

//-----------------------------------------------------------------	
int SMISetContainer::numOfEntries() const
{
	if ( _inxFirstOn < 0 ) { return 0; }
	int numIn = 0;
	int size = (int)_members.size();
	
	for (int i=0; i < size; i++)
	{
		if (_members[i].isOn()) { numIn++ ;}
	}
	
	return numIn;
}
//--------------------------------------------------------------------	
int SMISetContainer::add(NmdPtnr& item)
{
	int size = (int)_members.size();
		
	for (int i=0; i < size; i++)
	{
		if (_members[i].name() == item.name())
		 {
		 	if ( _members[i].isOn())
			{// it is already in
				return 0;
			}
			else
			{
				_members[i].switchOn();
				calcFirstOn();
				return 1;
			} 
		 }
	}

//  This is a brand new member. Will go to the end	
	SMIObject* pointer = (SMIObject*)item.pointer();
	
	SMISetMember member(item.name(),pointer,true);
	_members.push_back(member);
	calcFirstOn();
	return 1;
}
//------------------------------------------------------------------
int SMISetContainer::remove(NmdPtnr& item)
{
	int size = (int)_members.size();
	
	for (int i=0; i < size; i++)
	{
		if (_members[i].name() == item.name())
		 {
		 	if ( _members[i].isOn())
			{
				_members[i].switchOff();
				calcFirstOn();
				return 1;
			}
			else
			{// it is already out
				return 0;
			} 
		 }
	}
	return 0;
}
//--------------------------------------------------------------------	
void SMISetContainer::removeAll()
{
	int size = (int)_members.size();
	
	for (int i=0; i < size; i++)
	{
		_members[i].switchOff();
	}
	
	_inxFirstOn = -1;
	return;
}
//----------------------------- BF Feb 2020 ------------------------
int SMISetContainer::erase( const Name& objName )
{
	int size = (int)_members.size();
	
	for (int i=0; i < size; i++)
	{
		if (_members[i].name() == objName)
		 {
		 	if ( _members[i].isOn())
			{ return 0; } // the object is 'switched on'
			else
			{
				_members.erase(_members.begin()+i);
				calcFirstOn();
				return 1;
			} 
		 }
	}
	
	return 1;   // object not found
}
//------------------------------------------------------------------	
void SMISetContainer::out(const char indent[])
{
	int size = (int)_members.size();
	
	 cout << indent << " No. of members in the Set : " << numOfEntries()
	 << endl;
	
	for (int i=0; i < size; i++)
	{
		 if ( _members[i].isOn())
		{
			 cout << indent
			  << "  " << _members[i].name()
			  << "  " << _members[i].pointer()
			  << endl;
		}
	}
	
	return;
}
//------------------------------------------------------------------	
void SMISetContainer::outFull(const char indent[])
{
	int size = (int)_members.size();
	
	for (int i=0; i < size; i++)
	{
		cout << indent
		<< "  " << _members[i].name()
		<< "  " << _members[i].pointer()
		<< "  " << _members[i].isOn()
		<< endl;
	}
	
	return;
}
//-------------------------------------------------------------------
bool SMISetContainer::isPresent(const Name& objName)
{
	int size = (int)_members.size();
	
	for (int i=0; i < size; i++)
	{
		if ( _members[i].name() == objName ) { return true; }
	}
	return false;
}
//-----------------------------------------------------------------------	
void SMISetContainer::reset()
{
	_currentInx = _inxFirstOn;
	return;
}
//-----------------------------------------------------------------------	
int SMISetContainer::nextItem(NmdPtnr& item)
{
	int size = (int)_members.size();
	
	if ( size == 0 ) {return 0;}
	if ( _currentInx < 0 ) {return 0;}

//----  First save the item pointed to by _currentInx	
	NmdPtnr tmpitem(_members[_currentInx].name(),
	                 _members[_currentInx].pointer());
	item = tmpitem;
//-- now search for the next object that in switched on and update the index	
	_currentInx++;
	for ( ; _currentInx<size ; _currentInx++)
	{
		if (_members[_currentInx].isOn()) {return 1;}
	}
//-- if no other object found, set _currentInx to -1. This will
//    cause the next call to return 0
	_currentInx = -1;
	return 1;
}
//----------------------------------------------------------
void SMISetContainer::calcFirstOn()
{
	_inxFirstOn = -1;
	
	int size = (int)_members.size();
	
	for (int i=0; i<size; i++)
	{
		if (_members[i].isOn()) {_inxFirstOn = i; return;}
	}
}	

