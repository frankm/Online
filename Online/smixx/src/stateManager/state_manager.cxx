//----------------------   M a i n   P r o g r a m  -----------------
//
//                                            Author : B. Franek
//                                            Date : February 1996
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//-------------------------------------------------------------------
#include "parameters.hxx"
#include "ut_sm.hxx"
#include "options.hxx"
#include "diag.hxx"
//----------------------  Globals -------------------------------
#include "commhandler.hxx"
#include "registrar.hxx"
   Registrar allSMIObjectSets;
#include "objectregistrar.hxx"
   ObjectRegistrar allSMIObjects;
    Registrar allSMIClasses;
#include "queue_name.hxx"
   Queue_Name executableObjectQ;
#include "ifhandler.hxx"
   IfHandler ifHandler;
#include "resumehandler.hxx"
	ResumeHandler resumeHandler;
   Name smiDomain;
   Name allocator("\0");
//---------------------------------------------------------------
#include "smixx_common.hxx"
#include <ctype.h>
#include "smiobject.hxx"
#include "smiobjectset.hxx"
#include "version.hxx"
#include "clientwhens.hxx"
#include "msg.hxx"
#ifdef _WIN32
  #include <windows.h>
#endif
#include "alarm.hxx"

void logicEngine();

void initiator(ifstream *p);

//-----------------------------------------------------------------------------

int main(int argc, const char* argv[]) {

  Name inputFile; Name dim_dns_node;
  

	Options::processCommandLine(argc,argv,smiDomain,inputFile);

char* p_dim_node; Options::cValue("dns",p_dim_node); dim_dns_node = p_dim_node;
    
    
//    cout << endl << Options::gimeExportString() << endl;
    
#ifdef _WIN32
  FreeConsole();
#endif

	int dbg; Options::iValue("d",dbg);

  if ( dbg < 0 ) { dbg = 3; } //if not specified, set it to 3 to make it BaBar compatible

  if (dbg > 1)
  {
	  cout << endl;
	  cout << " SMI++ State Manager Version - " << smixxVersion << "- Date : " << smixxDate << endl;
	  cout << endl;

	  Options::printOptions(); cout << endl;

	  cout << "    SMI Domain : " << smiDomain << endl;
	  cout << "    Input File : " << inputFile << endl << endl;

	  int unlockedIfs; Options::iValue("u", unlockedIfs);

	  if (unlockedIfs > 0)
	  {
		  cout << endl
			  << " +++ W A R N I N G ! +++" << endl
			  << "  Once a condition in an IF is evaluated, the IF's objects are UNLOCKED" << endl;
	  }
	  else { unlockedIfs = 0; }
  }
  else if (dbg > 0)
  {
	  char timeStr[30];
	  gime_date_time(timeStr);
	  cout << timeStr << " - ";
	  cout << "SMI++ State Manager (" << smixxVersion << ") Starting Up - Domain: " << smiDomain << endl;
  }
	
  if (dim_dns_node == "\0") {}
  else {
      CommHandler::setDnsNode(dim_dns_node.getString());
  }

	Msg::setup(smiDomain.getString());
	

//---------------------  Open the input SOBJ file -----------------------
  char* pStr = inputFile.getString();
	 
	if ( dbg > 0 || Msg::dllmsgRxPtr != 0 ) {
		Name message;
		message = "input file ";
		message += "<";
		message += inputFile;
		message += ">";
		if ( Msg::dllmsgRxPtr != 0 ) (*Msg::dllmsgRxPtr)(message.getString());
//		if (dbg > 0)
//		{
//		  	cout << message.getString() << endl;
//		} 
	}


  ifstream input_file(pStr);

  if (input_file.bad()) {
    cout << "Could not open file\n";
    Name temp = "-";
    Alarm::message("FATAL",temp," could not open SOBJ file");
  }
//------------------------------------------------------------------

	if ( dbg > 1 ) { print_msg("SMI++ Initiator started");}

   initiator(&input_file);  

	if ( dbg > 1 ) { print_msg("SMI++ Initiator Finished");}

// ================   Now establish client whens ================

	int numOfObjects;
	numOfObjects = allSMIObjects.numOfObjects();
	int io;
	SMIObject* pObj;

	for (io=0; io < numOfObjects; io++) {
		pObj = allSMIObjects.gimePointer(io);
		pObj->updateReferencedObjectsandSets();
	}
// ==============================================================
// =================  initialise objects Set Lists ==============
	int numOfSets = allSMIObjectSets.length();
	int is;
	SMIObjectSet* pSet;
	
	for (is = 0; is < numOfSets; is++ )
	{
		pSet = (SMIObjectSet*)allSMIObjectSets.gimePointer(is);
		pSet->informObjectsAboutMembership();
	}
// ===============================================================

// =========   Diagnostics   =============================

//	Diag::printClientWhensOfAllObjects();

//	Diag::printClientWhensOfAllObjectSets();
// print for every object the Object Sets it belongs to
/*
	cout << endl << endl << "   OBJECT SETS object belongs to" << endl;
	
	for (int io=0; io < numOfObjects; io++) {
		pObj = allSMIObjects.gimePointer(io);
		cout << endl << " Object " << pObj->name() << endl;
		pObj->printSetNamesBelongingTo();
	}	
*/	
// print for every object it's actual client whens

//	Diag::printActualClientWhensOfAllObjects();
// ****************************
/*  debug
   cout << "\n \n  The following objects are registered : \n \n";

  int numOfObjects;
  numOfObjects = allSMIObjects.numOfObjects();

  for (int io=0; io < numOfObjects; io++) {
     SMIObject* ptnr = allSMIObjects.gimePointer(io);
     bool isAssoc = ptnr->associated();
     bool hasAssocAtt = ptnr->hasAssociatedAttribute();
     cout << allSMIObjects.gimeName(io) << "  " << isAssoc << "  " 
     << hasAssocAtt << endl;
     if ( isAssoc != hasAssocAtt ) { cout << "********************************" <<
     endl;}
  }
*/
//************************************************************************

/*   cout << "\n \n  The following object sets are registered : \n \n";

  int numOfObjectSets;
  numOfObjectSets = allSMIObjectSets.length();


  for (int io=0; io < numOfObjectSets; io++) {
     SMIObjectSet* pSMIObjSet;
     void* ptn = allSMIObjectSets.gimePointer(io);
     pSMIObjSet = (SMIObjectSet*)ptn;
     Name temp ="  ";
     cout << endl;
     pSMIObjSet->out(temp);
  }

*/

	if ( dbg > 3 )
	{
		for (io=0; io < numOfObjects; io++) {
			pObj = allSMIObjects.gimePointer(io);
			pObj->whatAreYou();
		}
	}
	
	if ( dbg > 1 )
	{
		cout << endl;
		print_msg("SMI++   Logic Engine started execution");
	}

   logicEngine();


   exit(1);  // never happens


}
