//-------------------------- Action Class ----------------------------------
#include "smixx_common.hxx"
#include <stdio.h>
#include <assert.h>

#include "parameters.hxx"
#include "action.hxx"
#include "smiobject.hxx"
#include "inslist.hxx"
#include "alarm.hxx"
#include "options.hxx"
#include "instruction_return_status.hxx"
#include "ut_sm.hxx"
#include "param.hxx"
//
//                                                  Author: B. Franek
//                                                  Date : 23 September 1996
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//-----------------------------------------------------------------------------

Action::Action
      ( char lines[][MAXRECL],Name& action_nm,int& no_lines, 
        SMIObject *pobj, State* pstat){
//
// First, the Action class is instantiated and as a part of its initialisation,
// the Instruction block number 0 is also instantiated. Even though the action
// may contain many blocks, it points directly only to this block (0).
// The other blocks of the action are accessed hirachically from block(0)

   int NoOfBlocks;

    InsList* pblock;

   _objName = pobj->name();
   _pParentObject = pobj;
   _pParentState = pstat;
   
   _diagPrintOffset = 0;

	_suspend_flag = 0;
	
   _actionName = action_nm; 
	if ( !(action_nm == lines[1]) )
	{
      Alarm::message("FATAL",_pParentObject->name(),
      " initialisation of action");	
	}

#ifdef DEBUG
   cout << "    action : " << _actionName << "\n";
   cout.flush();
#endif

   int numOfPar;
   sscanf(lines[2],"%d",&numOfPar);

   int lin = 3;

   if ( numOfPar > 0 ) {
      Name par_type;
      Name par_name;
      Name par_default;
	  
      par_type = lines[lin];

      if ( par_type == "STRING" ||
           par_type == "INT" ||
           par_type == "FLOAT" ) { // New format incl the types
		   
         for ( int ip = 0; ip < numOfPar; ip++) {
		     par_type = lines[lin];
             lin++;
			 par_name = lines[lin];
             lin++;
			 par_default = lines[lin];
			 
// the following line is to make it compatible with older versions of SOBJ		
			 if (par_default == "&nodefault") par_default = paramcons::noval;
			  
             lin++;
             if ( par_default == paramcons::noval ) {
			 	Param tempPar(par_name,paramcons::noval,par_type);
				_currentParameters.addParam(tempPar);
             }
             else {
			 	Param tempPar(par_name,par_default,par_type);
				_currentParameters.addParam(tempPar);
             }
         }
      }
      else {  //old type without the type info....all parameters are strings
         par_type = "STRING";
		 
         for ( int ip = 0; ip < numOfPar; ip++) {
		     par_name = lines[lin];
             lin++;
			 par_default = lines[lin];
             lin++;
			 
// the following line is to make it compatible with older versions of SOBJ		
			 if (par_default == "&nodefault") par_default = paramcons::noval;
			  			 
             if ( par_default == paramcons::noval ) {
			 	Param tempPar(par_name,paramcons::noval,par_type);
				_currentParameters.addParam(tempPar);
             }
             else {
				Param tempPar(par_name,par_default,par_type);
				_currentParameters.addParam(tempPar);
             }
         }
      }
#ifdef DEBUG
      cout << " Action " << _actionName << "   parameters :" << endl;
      _currentParameters.out();
      cout.flush();
#endif
   }
   

  NoOfBlocks = 0;

//-----------  First loop over the action lines and instantiate all blocks ----
  for (int ii=lin;;ii++){
    if (!strcmp(lines[ii],"*END_ACTION")) {
       break;
    }
    else if (!strcmp(lines[ii],"*BLOCK")) {
// Now read two integers, making sure they are interpreted as decimal
      ii++;
      int curr_block,curr_level;
      sscanf(lines[ii],"%d %d",&curr_block,&curr_level);
//      cout << " Block :  " << curr_block << " level " << curr_level
//           << endl;

      assert ( curr_block == NoOfBlocks );

      
      pblock = new InsList
          (curr_block,curr_level,_pParentObject,_pParentState,this);
      assert( pblock != 0 );
      _allBlocks+= pblock;
      NoOfBlocks++;
    }
  }
   


   if ( NoOfBlocks == 0 ) {
      _pblock = 0;
   }
   else {
      _pblock = static_cast<InsList*>(_allBlocks[0]);
   }


//---------------------- Loop over the action lines ---------------------------

//cout << endl << _actionName << endl;
  int blkno;
  blkno = 0;

  for (int il=lin;;il++){
    if (!strcmp(lines[il],"*END_ACTION")) {
       no_lines = il+1;
       break;
    }
    else if (!strcmp(lines[il],"*BLOCK")) {
      int no_blklines;
      pblock = static_cast<InsList*>(_allBlocks[blkno]);
      pblock->initialise(&lines[il],_allBlocks,no_blklines);
//      cout << endl << "Block : " << blkno << "  " << pblock  << endl;
//      pblock->listInstructions();
      blkno++;
      il = il + no_blklines -1;
    }
    else {
      cout << " Action::Action  funny line encountered \n"
           << "|" << lines[il] << "|\n";
      cout.flush();
      Alarm::message("FATAL",_pParentObject->name(),
      " initialisation of action");
    }      
  }
//------------------------------------------------------------------------
//  if ( _actionName == "TEST" ) whatAreYou();

  return;
}  
//---------------------------  Destructor  BF  Feb 2020  -----------------
Action::~Action()
{
	if ( _pblock == 0 ) {return;}

//  all the action blocks were constructed in Action constructor,
//  so they have to be destructed here.
	
	int noBlocks = _allBlocks.length();
	
	for (int i=0; i<noBlocks; i++)
	{
		InsList* pb = static_cast<InsList*>(_allBlocks[i]);
		delete pb;
	}
	return;
}
//------------------------------------------------------------------------
void Action::whatAreYou() {
  cout << "    action : " << _actionName 
  << " " << (_currentParameters.outParmsString(2)).getString() << endl;

  if ( _pblock == 0 ) {return;}

  _pblock->listInstructions();
  return;
}
//----------------------------- outShort()  ----------------
Name Action::outShort() const
{
	Name temp;
	if (_pParentState) { temp = "Action : "; }
	else { temp = "Function : "; }
	temp += _actionName; temp += " ";
	temp += _currentParameters.outParmsString(2);
	return temp; 
}


//--------------------------------  execute  -----------------------------------
ActionReturnStatus_t Action::execute( Name& endState, const Name& actionstr) {
//
	ActionReturnStatus_t retFlg;
	
	int dbg; Options::iValue("d",dbg);
	Name mainIndent = nBlanks(_diagPrintOffset);
	int imainIndent = mainIndent.length();
	int isecIndent = imainIndent + 3;
	Name secIndent = nBlanks(isecIndent);

//beg debug
	if ( dbg > 3 )
	{
		cout << endl;
		cout << mainIndent;
		if (_suspend_flag == 0 ) { cout << " Executing "; }
		else                     { cout << " Resuming "; }
		cout << outShort() << endl;
		if (_suspend_flag == 0 )
		{
			char* inpar = actionstr.getString();
			inpar = strstr(inpar,"/");
			if(inpar)
			{
				cout << mainIndent 
				<< "           inc. parameters: " << inpar << endl;
			}
		}
	}
//end debug


  if ( _currentParameters.numOfEntries() > 0 ) {
     _currentParameters.setFromParmString(actionstr);
  }

// temp bodge

   if ( _objName == "&ALLOC" ) 
   { _currentParameters.initFromParmString(actionstr);}


  InstructionReturnStatus_t lastInsFlg;
  
  if (_pblock == 0)
  {  return actionTerminatedNoTermIns; }   // some crazy guys have no instructions
  
  lastInsFlg = _pblock->execute(endState);
//debug beg
if ( dbg > 3 )
{
		cout << mainIndent;
		if ( lastInsFlg == instructionSuspended ) { cout << " Suspending"; }
		else { cout << " Terminating"; }
		cout << outShort() << endl;
		cout << endl;
}
//debug end  

//debug beg
if ( dbg > 4 )
{
	Parms *pObjParms = _pParentObject->pObjectParameters();
	if (pObjParms->numOfEntries()) { 
		cout << secIndent << "Object parameters : " << endl;
		pObjParms->outParms(isecIndent);
	}
}
//debug end

	switch (lastInsFlg)
	{
		case normal:
			_suspend_flag = 0;
			retFlg = actionTerminatedNoTermIns;
			break;
			
		case instructionFinishesTerminatingAction:
			_suspend_flag = 0;
			retFlg = actionTerminated;
			break;
			
		case instructionSuspended:
			_suspend_flag = 1;
			retFlg = actionSuspended;
			break;
	}
	
	return retFlg;
}


//------------------------------- pCurrentParameters -------------------------
//

Parms* Action::pCurrentParameters()  {
   return &_currentParameters;
}
//---------------------------- actionString -------------------------------
//  action/par1/par2=/par3
//
void  Action::actionString (Name& actionstr) {
//
   actionstr = _actionName;

   if ( _currentParameters.numOfEntries() > 0 ) {
      Name parmString;
      _currentParameters.buildParmString(parmString);
      actionstr += parmString;
   }
//
   return;
}
//-----------------------------------------------------------------------------
Name& Action::actionName()
{
	return _actionName;
}
//-----------------------------------------------------------------------
void Action::setDiagPrintOffset(int offset)
{
	_diagPrintOffset = offset;
	return;
}
//----------------------------------------------------------------------
int Action::actionDiagPrintOffset() const
{
	return _diagPrintOffset;
}
//------------------------------------------------------------------
Parms* Action::pActionParameters() 
{
	return pCurrentParameters();
}
