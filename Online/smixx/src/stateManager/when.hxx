//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  September 1996
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
#ifndef WHEN_HH
#define WHEN_HH

#include "parameters.hxx"
class SMIObject;
class Condition;
#include "condition.hxx"
#include "doins.hxx"
#include "whenresponse.hxx"
//---------------------- W h e n   Class ---------------------------


class When {
public :
  When( char lines[][MAXRECL], int *wh_lines,
        SMIObject *pobj, State *pstat, Action *pact, 
        int whenInx);
  
  ~When();
  
  void whatAreYou(const char* indent);
  
  void getCurrentRefObjects(NameList& condCurrRefObjects);
  
/** The when is executed, i.e. its' condition is evaluated and if true then for DO when
    the correspondig action is put on HP queue.
    No check is made wheather the object that reached new state is present in the condition.
    @return   1 means that associated condition was true.
              0 meand that either the associated condition was false or it could not be
	      executed because one of its objects was transiting.
*/
	int executeWhen() const;
/**
    Every object or object set which is reffered to by this when condition is
    updated with this when address (its object, its state and its index)
*/
	int updateReferencedObjectsandSets() const;
/**
    returns true when it is 'stay_in_state' type when. otherwise returns false
*/
	bool specialWhen() const;
	
/**  will get the objects directly referenced by the when condition,
*/
	void getDirectlyRefObjects(NameVector& directlyRefObjects) const;

/**  will get the Object Sets referenced by the when condition. They could be Set unions.
*/
	void getRefObjectSets(NameVector& refObjectSets) const;

/** This relates to WaitFor whens.
    Returns the next move in case when the condition is true. This is 
    either '&CONTINUE' or 'state-name' into which to proceed.
*/
	Name nextMove() const;
	
private :

  Condition *_pCondition;
  
  WhenResponse *_pWhenResponse;

  SMIObject *_pParentObject;
  State*     _pParentState;
  Action*    _pParentAction;  // only meaningful for when in waitfor instruction
  
  Name _parentObjectName;   //name of the parent object...it is frequently
  
  Name _parentStateName;  // name of the parent state
  
  int _whenInx;   // when position within the parent state (starting with zero)

};
#endif
