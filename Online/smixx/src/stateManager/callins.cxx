//------------------------  Class   C a l l I n s  -----------------------------
#include "smixx_common.hxx"
#include <stdio.h>
#include <assert.h>
#include "utilities.hxx"
#include "ut_sm.hxx"
//-------------------- Externals -----------------
#include "objectregistrar.hxx"
   extern ObjectRegistrar allSMIObjects;
   extern  Name smiDomain;
#include "registrar.hxx"
   extern Registrar allSMIObjectSets;
//-------------------------------------------------
#include "callins.hxx"
#include "smiobject.hxx"
#include "parms.hxx"
#include "alarm.hxx"
#include "options.hxx"
#include "reservednames.hxx"
#include "action_return_status.hxx"
//-------------------------------------------------------------------------
//                                                      Date : September 2020
//                                                     Author: Boda Franek
//---------------------------- C a l l Ins -------------------------------------
//
CallIns::CallIns
		( char lines[][MAXRECL], int lev, int *no_lines, 
		  SMIObject *pobj, State *pstat, Action* pact){
	
	char tempstr[MAXRECL];
   
	int lin;
   
	if ( pstat == NULL ) {}  // will remove compiler warning
	
	_level = lev;
	
	_suspend_flag = 0;

	_pParentObject = pobj;
   
	_pParentState = pstat;

	_pParentAction = pact;

	sscanf(lines[1],"%s",tempstr);
	_functionNm = tempstr;
   
    _pFunction = _pParentObject->gimePointerToFunction(_functionNm);
	if (!_pFunction)
	{
		cout << " Function : " << _functionNm << " not declared" << endl;
		Alarm::message("FATAL",_pParentObject->name(),
	    " Processing Call ... problem with Function");	
	}
	
	sscanf(lines[2],"%d",&_numOfPara);

	lin = 3;

	Name paraNm, paraGenVal, paraGenType;

	if ( _numOfPara > 0) {
      // For every parameter Translator sends just its name and its generalized
      // value
	  
		for(int ipar=0; ipar<_numOfPara; ipar++) {
			strcpy(tempstr,lines[lin]);
			paraNm = tempstr;
			lin++;
			strcpy(tempstr,lines[lin]);
			paraGenVal = tempstr;

			if ( !strcmp(tempstr,"&SMIDOMAIN") ) {
				paraGenVal = "\"";
				paraGenVal += smiDomain;
				paraGenVal += "\"";
			}
        
			lin++;
			Param tempPar(paraNm,paraGenVal,paramcons::undef);
			_callInsParameters.addParam(tempPar);
		}
	}
	   
   *no_lines = lin;
   return;
}
//--------------------- Destructor  BF September 2020 -----------------
CallIns::~CallIns()
{
//  No objects created in constructor on the heap so nothing to destroy. 
    return;
}


//---------------------------- whatAreYou ---------------------------------

void CallIns::whatAreYou(){
   	Name temp = nBlanks(_level*4+10);
	char* ident = temp.getString();
	
	cout << ident << outShort().getString() << endl;
	return;
}

//----------------------------  execute  ---------------------------------------
//
//
int CallIns::execute( Name& endState ){

	InstructionReturnStatus_t retFlg;
	
	int dbg; Options::iValue("d",dbg);
	Name mainIndent = insDiagPrintOffset(_level, _pParentAction);
	
//debug beg
if ( dbg > 3 )
{
	cout << mainIndent;
	if ( _suspend_flag == 0 ) { cout << "executing: "; }
	else                      { cout << "resuming: "; }
	cout << outShort() << endl;
}
//debug end

//-------------------------------------
   Name functionString = _functionNm;  

	if (_numOfPara > 0) 
	{  // build parameter string for dispatch and append it to function name
		Name tempParString;
		createOutgoingParameters(tempParString);
		
		functionString += tempParString;

	}
//---------------------------------------------------------------------------

//   cout << " Function string : " << functionString << endl;
//
	int imainIndent = mainIndent.length();
	_pFunction->setDiagPrintOffset(imainIndent+3);
	ActionReturnStatus_t functionRetFlg;
	functionRetFlg = _pFunction->execute(endState,functionString);
	                       
//debug beg
if ( dbg > 3 )
{
	cout << mainIndent;
	if ( functionRetFlg == actionSuspended ) { cout << "suspending: "; }
	else { cout << "terminating: "; }
	cout << outShort() << endl;
}
//debug end

	switch (functionRetFlg)
	{
		case actionTerminatedNoTermIns:
			_suspend_flag = 0;
			retFlg = normal;
			break;
		
		case actionTerminated:
			_suspend_flag = 0;
			retFlg = instructionFinishesTerminatingAction;
			break;
		
		case actionSuspended:
			_suspend_flag = 1;
			retFlg = instructionSuspended;
			break;
	}
	return retFlg;        
}

//------------------------- functionName --------------------------------------
Name CallIns::functionName() const
{
	return _functionNm;
}
//--------------------------------------------------------------------------
void CallIns::createOutgoingParameters(Name& parmString)
{
//         call ACTION(ACTION_PAR_1 = 75,...
//                                ACTION_PAR_N = LOCAL_PAR,...)
//                               
//cout << endl << endl << " Entering createOutgoingParameters" << endl
//<<  " CALL parameters :" << endl;    _callInsParameters.out();

	int dbg; Options::iValue("d",dbg);

	Parms tmpPars; // temporary CALL parameters

if ( dbg > 7 )
{
    cout << " CALL parameters :" << endl;    _callInsParameters.outParms();
}
	Name funActParNm,           // action parameter name
	        indiVal;           // its indirect value 
	
	Name parSpecVal,         // specific value dispatched in DO
	     parSpecValType;     // specific value type:   INT, FLOAT or STRING
	
	int numPar = _callInsParameters.numOfEntries();
	
	// objective of this is to replace before the dispatch indirectvalues by
	// specific values and also supply the correct value type
	
	
	for ( int ipar=0; ipar<numPar; ipar++ )
	{
	//    .......funActParNm=indiVal.......
		Param callInsPar = _callInsParameters.getParam(ipar);
		funActParNm = callInsPar.paramName();
				
		int err;
		parSpecVal = callInsPar.paramValueAndType
				(allSMIObjects,
				 _pParentObject,
				_pParentState,
				_pParentAction,
				 parSpecValType,err);

		if ( err != 0 )
		{	 
			Alarm::message("FATAL",_pParentObject->name(),
				" Processing CALL ... problem with parameters");
		}		

		if ( parSpecVal == paramcons::noval )
		{
			cout << " *** Error : parameter " << callInsPar.paramName() 
			<< " does not have value" << endl;
			Alarm::message("FATAL",_pParentObject->name(),
				" Processing CALL ... parameter has no value");
		}
		Param tempPar(funActParNm,parSpecVal,parSpecValType.getString());
		tmpPars.addParam(tempPar);
		
	}

	tmpPars.buildParmString(parmString);

	return; 
}

//----------------------------------------------------------------------------
Name CallIns::outShort()
{
	Name temp;
	
	temp = "call ";
	temp += _functionNm;
	temp += " ";
	
	temp += _callInsParameters.outParmsString(1);

	return temp;
}
