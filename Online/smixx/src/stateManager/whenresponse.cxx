// whenresponse.cxx: implementation of the WhenResponce class.
//
//                                                  B. Franek
//                                                 October 2015
//
//////////////////////////////////////////////////////////////////////
#include "whenresponse.hxx"

WhenResponse::WhenResponse() {return;}

//--------------  Destructor  BF Mar 2020  -----------------------
WhenResponse::~WhenResponse() {return;}

void WhenResponse::executeResponse() {return;}

Name WhenResponse::nextMove() {return "NOT-APPLICABLE";}

bool WhenResponse::stay_in_state() {return false;}

Name WhenResponse::outShort() { return ""; }
