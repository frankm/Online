//---------------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  May 1996
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------
#include <stdio.h>
#include "ut_sm.hxx"
#include "termins.hxx"
#include "name.hxx"
#include "parms.hxx"
#include "param.hxx"
#include "smiobject.hxx"
#include "state.hxx"
#include "action.hxx"
#include "commhandler.hxx"
#include "options.hxx"

extern Name allocator;
extern CommHandler *pCommHandlerGl;

TermIns::TermIns( char lines[][MAXRECL], const int lev, 
           SMIObject* pObj, State* pState, Action* pAction, int& no_lines) {
   _level = lev;

    _pParentObject = pObj;

   _pParentState = pState;

   _pParentAction = pAction;

   _endState = lines[0];
//   cout <<  " state : " << _endState.outString() << " \n";

   no_lines = 1;
   return;
}
//--------------------  Destructor  BF  Mar 2020  -----------------
TermIns::~TermIns()
{
	return;
}
//----------------------------------------------------
void TermIns::whatAreYou(){
	Name temp = nBlanks(_level*4+10);
	char* ident = temp.getString();

	cout << ident 
	<< outShort() << endl;
}
//------------------------------ outShort() ---------------------------------
Name TermIns::outShort() const
{
	Name temp;
	
	temp = "move_to ";
	temp += _endState.outString();
	return temp;
}
//-------------------------------  execute  -----------------------------------

int TermIns::execute( Name& endState ){

	int dbg; Options::iValue("d",dbg);
	Name mainIndent = insDiagPrintOffset(_level, _pParentAction);
	Parms* parms = _pParentAction->pCurrentParameters();
	Name actualEndState = _endState.actualName(parms);
		
//debug beg
if ( dbg > 3 )
{
	cout << mainIndent;
	cout << "executing: " << outShort() << endl;
}
//debug end

   if ( _pParentObject->name() == "&ALLOC" ) {

/*
         cout << " Current parameters : " << endl;
         parms->out();
         cout.flush();
*/
      Name doubleq("\""),nullstr("\0");

      Name nameid("ID"),valid;
      Name nameAmpersId("&ID"),valAmpersId;
      Name nameAmpersConnId("&CONN_ID"),valAmpersConnId;
      char connId[257];
      int intConnId;

	  valid = parms->getParCurrValue(nameid);
	  int idPresent = 1;
	  if ( valid == paramcons::notfound ) { idPresent = 0; }
      valAmpersId = parms->getParCurrValue(nameAmpersId);
      valAmpersConnId = parms->getParCurrValue(nameAmpersConnId);	  

      valAmpersConnId.whatAreYou(connId,256);
      sscanf(connId,"%d",&intConnId);

      Name temp("&VARIABLE");

      if ( temp == actualEndState ) {  // this is allocate action...should check on that 
         State* ptgState = _pParentObject->pTaggedState(temp);  
         assert (ptgState != 0);
         
         if ( idPresent ) {
            valid.replace(doubleq,nullstr);   // get rid of the double quotes
            ptgState->changeName(valid);
            endState = valid;
         }
         else {
            valAmpersId.replace(doubleq,nullstr);   // get rid of the double quotes
            ptgState->changeName(valAmpersId);
            endState = valAmpersId;
         }
         valAmpersId.replace(doubleq,nullstr);   // get rid of the double quotes
         allocator = valAmpersId;

         pCommHandlerGl->domainAllocated(intConnId);
      }
      else {  // this must be RELEASE action
         endState = actualEndState;
         allocator = "\0";

         pCommHandlerGl->domainDeallocated(intConnId);
      }
   }
   else {
       endState = actualEndState; 
   }

return 1;         // terminate_action allways terminates
}

