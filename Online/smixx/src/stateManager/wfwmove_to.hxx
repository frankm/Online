// wfwmove_to.hxx: interface for the WFWMove_To class.
//
//                                                  B. Franek
//                                                 October 2015
//
//////////////////////////////////////////////////////////////////////
#ifndef WFWMOVE_TO_HH
#define WFWMOVE_TO_HH

#include "parameters.hxx"
#include "whenresponse.hxx"

class WFWMove_To : public WhenResponse
{
public:
	WFWMove_To( char lines[][MAXRECL], int &noLines);

	~WFWMove_To();
	
	Name nextMove();
	
	Name outShort();

protected :

	Name _stateNm;

};

#endif 
