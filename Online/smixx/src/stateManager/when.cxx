//--------------------------  W h e n  Class  -----------------------------------
#include "smixx_common.hxx"
#include <assert.h>
#include "when.hxx"
#include "smiobject.hxx"
#include "ut_sm.hxx"
#include "swstay_in_state.hxx"
#include "wfwmove_to.hxx"
#include "wfwcontinue.hxx"
#include "options.hxx"


extern ObjectRegistrar allSMIObjects;
extern Registrar allSMIObjectSets;
//                                                         24-September-1996
//                                                          B. Franek
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
//---------------------------------------------------------------------------
//
When::When( char lines[][MAXRECL], int *wh_lines,
            SMIObject *pobj, State *pstat, Action *pact,
            int whenInx){

   _pParentObject = pobj;
   _pParentState  = pstat;
   _pParentAction = pact;
   
   _parentObjectName = _pParentObject->name();
   
   _parentStateName = pstat->stateName();
   _whenInx = whenInx;

   int il,numCondlines;

   il = 1;

//    cout << "when:" << "\n";
//    cout << lines[il] << "\n" << lines[il+1] << "\n"<< lines[il+2] << "\n";
   _pCondition = new Condition(&lines[il],&numCondlines,
                _pParentObject,_pParentState, _pParentAction);
     il = il + numCondlines;

// Now what follows is the responce to the condition being true.
// There are 4 different possible responces:
//  DO,  STAY_IN_STATE,  MOVE_TO 'state-name' and CONTINUE, the most
// common being DO.
// For the reason of backward compatibility, the last 3 come from Translator
// disguised as DO instruction. They are recognised by the action name which
// is the second line after 'do'.
 
	Name actionNm = lines[il+1]; int noLines;
	
	if ( actionNm == "&NULL" ) //  STAY_IN_STATE
	{
		SWStay_in_State* pStay = new SWStay_in_State(&lines[il],noLines);
		_pWhenResponse = (WhenResponse*)pStay;
	}
	else if ( actionNm == "&CONTINUE" ) //  CONTINUE
	{
		WFWContinue* pCont = new WFWContinue(&lines[il],noLines);
		_pWhenResponse = (WhenResponse*)pCont;
	}
	else if ( actionNm.subString(0,8) == "&MOVE_TO" ) //  MOVE_TO 'state-name'
	{
		WFWMove_To* pMove = new WFWMove_To(&lines[il],noLines);
		_pWhenResponse = (WhenResponse*)pMove;
	}
	else
	{
		DoIns* pDoIns = new DoIns(&lines[il],0,&noLines,
		                          _pParentObject,_pParentState);
		_pWhenResponse = (WhenResponse*)pDoIns;
	}
	
	il = il + noLines;

	*wh_lines = il;
	
//	whatAreYou(" ");
   
   return;
}
//------------------  Destructor  BF  Mar 2020  ------------------------------
When::~When()
{
	delete _pCondition;
	delete _pWhenResponse;
	
	return;
}

//------------------------------ whatAreYou ---------------------------------
void When::whatAreYou(const char* indent){
   char condition[MAXRECL];
  
   _pCondition->whatAreYou(MAXRECL,condition);
   cout  << indent << "when " << condition ;
   
   cout << " " << (_pWhenResponse->outShort()).getString() << endl;
}


//------------------------ executeWhen ---------  B.F. May 2008  --------
int When::executeWhen( ) const {

	int dbg; Options::iValue("d",dbg);

	int flag;
	flag = _pCondition->evaluate();  // ret 0-false; 1-true; -1 one of 
                                         // the smiobjects in transition
					 
	if ( flag != 1) { return 0; }
	
if ( dbg > 6 )
{
print_obj(_parentObjectName);
cout << "(state " << _parentStateName 
<< ") when no." << _whenInx << " activated" << endl;
}

//  The condition evaluates to TRUE

	_pWhenResponse->executeResponse();
	return 1;
}
//===========================================================================
int When::updateReferencedObjectsandSets() const
{
// loop over the referenced objects
// get the pointer to the list first
	NameVector refObjects;
	_pCondition->getDirectlyRefObjects(refObjects);
	
	int numRefObjects = refObjects.length();
	int i = 0; Name name; SMIObject* pRefObject;
	
	for ( i = 0; i<numRefObjects; i++ )
	{
		name = refObjects[i];
		pRefObject = allSMIObjects.gimePointer(name);
		pRefObject->addClientWhen
		(_parentObjectName.getString(),_parentStateName.getString(),_whenInx);
	}

// now do the same for sets
	NameVector refSets;
	_pCondition->getRefObjectSets(refSets);
	
	int numRefObjectSets = refSets.length();
	SMIObjectSet* pRefObjectSet;
	
	for ( i = 0; i<numRefObjectSets; i++ )
	{
		name = refSets[i];
		pRefObjectSet = (SMIObjectSet *)allSMIObjectSets.gimePointer(name);
		pRefObjectSet->addClientWhen
		(_parentObjectName.getString(),_parentStateName.getString(),_whenInx);
	}

	return 1;
}

//------------------------ specialWhen() --------------------------
bool When::specialWhen() const
{ 
	if ( _pWhenResponse->stay_in_state() ) { return true; }
	else { return false; }
}
//---------------------------------------------------------------------
void When::getCurrentRefObjects(NameList& condCurrRefObjects)
{
	_pCondition->getCurrentRefObjects(condCurrRefObjects);
//cout << endl << " When::getCurrentRefObjects called" << endl;
//Name offset(" ");
//condCurrRefObjects.out(offset);
	return;
}
//------------------------ nextMove -------------  B.F. Sep 2015  --------
Name When::nextMove( ) const
{
	return _pWhenResponse->nextMove();
}
//---------------------------------------------------------------------------
void When::getDirectlyRefObjects(NameVector& directlyRefObjects) const
{
	_pCondition->getDirectlyRefObjects(directlyRefObjects);
	return;
}
//---------------------------------------------------------------------------
void When::getRefObjectSets(NameVector& refObjectSets) const
{
	_pCondition->getRefObjectSets(refObjectSets);
	return;
}
