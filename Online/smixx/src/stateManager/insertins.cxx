//------------------------  Class   I n s e r t I n s  -----------------------------
#include "smixx_common.hxx"
#include <stdio.h>
#include <assert.h>

//-------------------- Externals -----------------
#include "objectregistrar.hxx"
   extern ObjectRegistrar allSMIObjects;
   extern  Name smiDomain;
#include "registrar.hxx"
   extern Registrar allSMIObjectSets;
//-------------------------------------------------
#include "utilities.hxx"
#include "ut_sm.hxx"
#include "insertins.hxx"
#include "smiobject.hxx"
#include "smiobjectset.hxx"
#include "smiobjectsetsimple.hxx"
#include "parms.hxx"
#include "param.hxx"
#include "alarm.hxx"
#include "options.hxx"
#include "getvarelem.hxx"
//                                                      Date :  3 July 2001
//                                                     Author:  Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2003 CCLRC. All Rights Reserved.
//---------------------------- InsertIns -------------------------------------
//

InsertIns::InsertIns
       ( char lines[][MAXRECL], const int lev, int& no_lines,
         SMIObject* pobj, State *pstat, Action* pact){

	if ( pstat == NULL ) {}  // will remove compiler warning
	
	_level = lev;

	_pParentObject = pobj;

	_pParentAction = pact;

	sscanf(lines[1],"%d",&_insertRemove);
	
	_objectId = lines[2];
	_setId = lines[3];

	no_lines = 4;
	return;
}
//-------------------  Destructor  BF  Mar 2020  ------------------------
InsertIns::~InsertIns() 
{
	return;
}

//---------------------------- whatAreYou ---------------------------------

void InsertIns::whatAreYou(){
	Name temp = nBlanks(_level*4+10);
	char* ident = temp.getString();
	
	cout << ident << outShort() << endl;

	return;
}
//----------------------------------------------------------------------------
Name InsertIns::outShort() const
{
	Name temp;
	if (_insertRemove) 
	{ temp = "insert "; temp += _objectId.outString(); temp += " in ";}
	else
	{ temp = "remove "; temp += _objectId.outString(); temp += " from ";}
	
	temp += _setId.outString();
	return temp;
}
//----------------------------  execute  ---------------------------------------
int InsertIns::execute( Name& endState ){

	int dbg; Options::iValue("d",dbg);
	Name mainIndent = insDiagPrintOffset(_level, _pParentAction);

	endState = "not changed";
	
//debug beg
if( dbg > 5 )
{
	cout << endl << "start ======================= InsertIns::execute ===============";
}
//debug end
//debug beg
if ( dbg > 3 )
{
	cout << mainIndent;
	cout << "executing: " << outShort() << endl;
}
//debug end
	
	Name setNm("");
	void* ptnvSet = 0;
	
	int ret = GetVarElem::actualSetName( _setId,
								_pParentAction,
								"insert/remove",
								 setNm,ptnvSet);
								 
	if (!ret) return 0;
	SMIObjectSetSimple* ptnSet = (SMIObjectSetSimple*) ptnvSet;

//	cout << " Set before : " << endl; ptnSet->out(offset);
	
	
	if ( _objectId.name() == "&ALL" && _insertRemove == 0)
	{
		ptnSet->removeAll();
		return 0;
	}	
	
	Name objNm("");
	ret = GetVarElem::actualObjectName( _objectId,
								_pParentAction,
								"insert/remove",
								 objNm);
								 
	if (!ret) return 0;


	if (_insertRemove) {ptnSet->add(objNm);}
	else               {ptnSet->remove(objNm);}
//debug beg
if ( dbg > 4 )
{
	cout << "  Object " << objNm;
	if ( _insertRemove ) { cout << "  inserted into set  " << setNm << endl;}
	else { cout << "  removed from set  " << setNm << endl;}
}
//debug end
	
//debug beg
if ( dbg > 5 )
{
	cout << endl << " New " << setNm << " set : " << endl;
	ptnSet->out("      "); cout << endl;
	cout << "return ======================= InsertIns::execute ============";
	cout << endl << endl;
}
//debug end
	return 0;         // insert instruction allways finishes
}


