//
//  option.hxx
//  smiSM-Options
//
//  Created by Bohumil Franek on 28/03/2016.
//  Copyright © 2016 Bohumil Franek. All rights reserved.
//

#ifndef option_hxx
#define option_hxx

#include <stdio.h>
#include "name.hxx"


class Option
{
public:
    Option(const char Id[],
           const  char type[],
           const char defvalue[],
           const char name[],
           const char comment[] );

    ~Option();
    
    char* outString();
    
    int newValue(const char value[]);
    
    void* gimeConvValue();
    
    Name& gimeValue();
    
    char* cValue();
    
    int& iValue();
    
    float& fValue();
    
    Name& gimeId();
    
    Name& gimeType();
    
    Name gimeExportString();
    
private:
    
    int convertValue();
    
    Name _Id,_type,_defvalue,_value,_name,_comment;
    int _ivalue;
    float _fvalue;
    
    char _outputStr[80];
};
#endif /* option_hxx */
