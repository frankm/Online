//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_DUMALIGNDRV_H
#define ONLINE_GAUCHO_DUMALIGNDRV_H

/// Framework include files
#include "GaudiKernel/Service.h"
#include "GaudiKernel/IToolSvc.h"
#include "Gaucho/IGauchoMonitorSvc.h"
#include "GaudiKernel/IRunable.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IIncidentListener.h"
#include "OnlineAlign/IAlignUser.h"
#include "OnlineAlign/IAlignSys.h"

/// C/C++ include files
#include "RTL/rtl.h"
#include <cstdio>

/// Online namespace declaration
namespace Online  {

  class AlignDrv: public extends3<Service, IRunable, IIncidentListener, IAlignDrv>  {
  public:
    AlignDrv(const std::string& name, ISvcLocator* sl);
    virtual ~AlignDrv();
      
    /// Overrides of Service
    virtual StatusCode start() override;
    virtual StatusCode initialize() override;
    virtual StatusCode finalize() override;
    virtual StatusCode stop() override;

    /// Override from IRunable
    virtual StatusCode run() override;

    /// Override from IIncidentListener
    virtual void handle(const Incident& inc) override;

    /// Overrides of IAlignDrv
    virtual void setReferenceBase(long) override;
    virtual void writeReference() override;
    virtual void waitRunOnce() override;
    virtual void doContinue() override;
    virtual void doStop() override;

    /// Internal implementation
    virtual StatusCode pause();
    virtual void       setRunOnce();
    IGauchoMonitorSvc *getMonSvc();

  private:
    SmartIF<IGauchoMonitorSvc> m_monitoringSvc;
    SmartIF<IIncidentSvc>      m_incidentSvc;
    SmartIF<IToolSvc>          m_toolSvc;
    IAlignIterator            *m_fitter    { nullptr };
    std::string                m_partitionName;
    std::string                m_refFileName;
    std::string                m_fitterName;
    long                      *m_refBase   { nullptr };
    bool                       m_firstRef;
    bool                       m_runonce;
  };
}
#endif // ONLINE_GAUCHO_DUMALIGNDRV_H
