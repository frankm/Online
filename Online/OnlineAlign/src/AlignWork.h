//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_ALIGNWORK_H
#define ONLINE_GAUCHO_ALIGNWORK_H

/// Framework include files
#include <GaudiKernel/Service.h>
#include <GaudiKernel/IToolSvc.h>
#include <GaudiKernel/IRunable.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/IIncidentListener.h>

#include <Gaucho/IGauchoMonitorSvc.h>
#include <OnlineAlign/IAlignUser.h>
#include <OnlineAlign/IAlignSys.h>
#include <RTL/rtl.h>

/// Online namespace declaration
namespace Online   {

  class AlignWork: public extends3<Service, IRunable, IIncidentListener, IAlignWork>  {
  public:
    /// Service constructor
    AlignWork(const std::string& name, ISvcLocator* sl);
    /// Default destructor
    virtual ~AlignWork();

    /// Overrides of Service
    virtual StatusCode start() override;
    virtual StatusCode stop() override;
    virtual StatusCode initialize() override;
    virtual StatusCode finalize() override;

    /// Override from IRunable
    virtual StatusCode run() override;

    /// Override from IIncidentListener
    virtual void handle(const Incident& inc) override;

    const std::string& getPartitionName()   const  {
      return m_partitionName;
    }
    unsigned long getReference()   const   {
      return m_reference;
    }
    /// Overrides from IAlignWork
    virtual IGauchoMonitorSvc *getMonSvc()  override {
      return m_monitoringSvc.get();
    }
    virtual StatusCode readReference()   override;

    /// Internal implementation
    virtual StatusCode pause();
    virtual StatusCode continuing();

    void waitRunOnce();
    void setRunOnce();
    
    /// Data members
    SmartIF<IGauchoMonitorSvc> m_monitoringSvc;
    SmartIF<IIncidentSvc>      m_incidentSvc;
    SmartIF<IToolSvc>          m_toolSvc;
    IAlignFcn*                 m_fitterFcn  { nullptr };

    std::string                m_partitionName;
    std::string                m_refFileName;
    std::string                m_fitFcnName;
    lib_rtl_thread_t           m_thread     { nullptr };
    unsigned long              m_reference  { 0 };
    bool                       m_runonce    { false };
  };
}      // End namespace Online
#endif // ONLINE_GAUCHO_ALIGNWORK_H
