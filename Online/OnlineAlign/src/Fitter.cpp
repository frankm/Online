//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

#include "Fitter.h"
#include <OnlineAlign/IAlignSys.h>
#include <Gaucho/CounterTask.h>
#include <Gaucho/MonCounter.h>
#include <TMinuit.h>

// C/C++ include files
#include <stdexcept>
#include <cmath>

namespace LHCb   {
  class Fitter : public Online::Fitter  {
  public:
    using Online::Fitter::Fitter;
  };
}
DECLARE_COMPONENT( LHCb::Fitter )
DECLARE_COMPONENT( Online::Fitter )

using namespace Online;

namespace   {
  static Fitter *s_fitter { nullptr };

  int FitterThreadFunction(void *t)  {
    s_fitter = (Fitter*)t;
    s_fitter->run().ignore();
    return 1;
  }
}

Fitter::Fitter(const std::string& typ, const std::string& nam, const IInterface* par)
  : base_class(typ, nam, par)
{
  this->declareProperty("PartitionName", m_partitionName   =   "LHCbA" );
  this->declareProperty("ParamFile",     m_paramFileName   =   "bla.txt" );
  this->declareProperty("CounterNames",  m_counterNames    = { "aaa/Chi2"} );
  this->declareProperty("CounterDNS",    m_counterTaskDNS  =   "mon01");
  this->declareProperty("CounterTask",   m_counterTaskName =   "LHCbA_AligWrk_00");
  this->parent = SmartIF<IAlignDrv>((IInterface*)par);
  this->m_minuit = 0;
}

StatusCode Fitter::initialize()   {
  auto params = this->readParams();
  if ( !params.empty() )    {
    this->m_minuit = std::make_unique<TMinuit>(3);
    this->m_minuit->SetFCN(Chi2);
    for (size_t i=0; i < params.size(); i++ )  {
      char nam[2] = {char('a'+i), 0};
      this->m_minuit->DefineParameter(i,nam,params[i],10.0,0.0,0.0);
    }
    return StatusCode::SUCCESS;
  }
  return StatusCode::FAILURE;
}

void Fitter::Chi2(int& npar, double* /* grad */, double& fval, double* params, int /* flag */)  {
  char text[256];
  std::vector<double> pars(params, params+npar);
  s_fitter->parent->writeReference();
  if ( s_fitter->writeParams(pars).isSuccess() )   {
    s_fitter->info() << "Number of Parameters: " << npar << endmsg;
    for (int i = 0; i < npar; i++ )    {
      std::snprintf(text, sizeof(text), "Parameter %4d %15g", i, params[i]);
      s_fitter->info() << text << endmsg;
    }
  }
  else   {
    /// Error!
  }
  s_fitter->parent->doContinue();
  s_fitter->parent->waitRunOnce();
  fval = s_fitter->getIterationResult();
  if ( std::isnan(fval) )   {
    s_fitter->warning() << "Chi2: INVALID: Function value: (nan)" << endmsg;
    fval = 0.0;
    return;
  }
  s_fitter->info() << "Chi2: Function value: " << fval << endmsg;
}

StatusCode Fitter::run()    {
  double par, err;
  int res = this->m_minuit->Migrad();
  always() << "MIGRAD has finished with return code: " << res << endmsg;
  always() << "Parameters:" << endmsg;
  for( size_t i = 0, n = this->m_minuit->GetNumPars(); i<n; i++ )  {
    char text[132];
    this->m_minuit->GetParameter(i, par, err);
    std::snprintf(text, sizeof(text), "Param #%ld %15g +- %15g\n", long(i), par, err);
    always() << text << endmsg;
  }
  this->parent->doStop();
  this->always() << "+++ Finishing fitter loop. Activate STOP" << endmsg;
  return StatusCode::SUCCESS;
}

StatusCode Fitter::begin()   {
  ::lib_rtl_start_thread(FitterThreadFunction, this, &this->m_thread);
  return StatusCode::SUCCESS;
}

StatusCode Fitter::end()   {
  try    {
    if ( this->m_thread )   {
      ::lib_rtl_delete_thread(this->m_thread);
      this->m_thread = nullptr;
    }
  }
  catch(...)   {
  }
  return StatusCode::SUCCESS;
}

StatusCode Fitter::finalize()   {
  try    {
    this->m_minuit.reset();
    if ( this->m_thread )   {
      ::lib_rtl_delete_thread(this->m_thread);
      this->m_thread = nullptr;
    }
  }
  catch(...)   {
  }
  return StatusCode::SUCCESS;
}

void Fitter::setPartitionName(const std::string& partition)     {
  this->m_partitionName = partition;
}

double Fitter::getIterationResult()   {
  CounterTask task(this->m_counterTaskName, this->m_counterTaskDNS);
  std::vector<Online::CntrDescr*> cdesc;
  task.counters(this->m_counterNames, cdesc);
  if ( cdesc.size() > 0 )  {
    double result = cdesc[0]->d_data;
    always() << "Got " << cdesc.size() << " Counters Found matching:"
	     << " DNS: '"  << this->m_counterTaskDNS  << "'"
	     << " Task: '" << this->m_counterTaskName << "'"
	     << endmsg;
    // TODO: need to delete array elements ??
    return result;
  }
  else  {
    error() << "No ADDER counters Found matching:"
	    << " DNS: '"  << this->m_counterTaskDNS  << "'"
	    << " Task: '" << this->m_counterTaskName << "'"
	    << endmsg;
  }
  return 1.0;
}

StatusCode Fitter::writeParams(const std::vector<double> &params)  const {
  FILE *f = std::fopen(this->m_paramFileName.c_str(),"w");
  if ( f )    {
    for (size_t i=0; i < params.size(); i++)  {
      std::fprintf(f,"%15lf ",params[i]);
    }
    std::fprintf(f,"\n");
    std::fclose(f);
    return StatusCode::SUCCESS;
  }
  return StatusCode::FAILURE;
}

std::vector<double> Fitter::readParams() const  {
  FILE *f = std::fopen(this->m_paramFileName.c_str(),"r");
  if ( f )    {
    std::vector<double> params;
    while ( !std::feof(f) )  {
      double p;
      std::fscanf(f,"%lf",&p);
      if ( std::feof(f) ) break;
      params.insert(params.end(),p);
    }
    std::fclose(f);
    return params;
  }
  throw std::runtime_error("Failed to open parameter file: "+this->m_paramFileName);
}
