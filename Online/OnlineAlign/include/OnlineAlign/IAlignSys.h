//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_ONLINEALIGN_ALIGNSYS_H
#define ONLINE_ONLINEALIGN_ALIGNSYS_H

/// Framework include files
#include <GaudiKernel/IInterface.h>

// Forward declarations
class IGauchoMonitorSvc;

/// Online namespace declaration
namespace Online   {

  /// Alignment worker interface class
  class IAlignWork: virtual public IInterface  {
  public:
    DeclareInterfaceID(IAlignWork,1,0);
  public:
    virtual IGauchoMonitorSvc* getMonSvc() = 0;
    virtual StatusCode readReference() = 0;
  };

  /// Alignment driver interface class
  class GAUDI_API IAlignDrv: virtual public IInterface  {
  public:
    DeclareInterfaceID(IAlignDrv,1,0);
  public:
    virtual void setReferenceBase(long) = 0;
    virtual void writeReference() = 0;
    virtual void waitRunOnce() = 0;
    virtual void doContinue() = 0;
    virtual void doStop() = 0;
  };
}      // End namespace Online
#endif // ONLINE_ONLINEALIGN_ALIGNSYS_H
