//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_DIMCLIENT_H
#define RPC_DIMCLIENT_H

// Framework include files
#include <RPC/XMLRPC.h>
#include <RPC/JSONRPC.h>
#include <RPC/DimRequest.h>

// C/C++ include files
#include <memory>

/// Namespace for the dimrpc based implementation
namespace rpc  {

  ///  XMLRPC Client class based on DIM
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class DimClient   {
  public:
    class Handler;
    /// Implementation object
    Handler* implementation = 0;

  public:
    /// Default constructor
    DimClient() = default;
    /// Copy constructor
    DimClient(const DimClient& copy) = default;
    /// Assignment operator
    DimClient& operator=(const DimClient& copy) = default;
    /// Initializing constructor: No need to call open if this constructor is issued
    DimClient(const std::string& server, const std::string& dns="", int tmo=10000);
    /// Default destructor
    virtual ~DimClient();

    /// Modify debug flag
    virtual int setDebug(int value);
    /// Access debug flag
    virtual int debug()  const;
    /// Access name
    std::string name()  const;
    /// Set the caller properties
    void open(const std::string& server, const std::string& dns="");
    /// Set the caller properties
    void open(const std::string& server, const std::string& dns, int tmo);
    /// Connect client to given URI and execute RPC call
    virtual std::vector<unsigned char> request(const void* call, size_t len)  const;
    /// Connect client to given URI
    virtual std::vector<unsigned char> request(const std::string& call)  const;
    /// Connect client to given URI
    virtual xmlrpc::MethodResponse call(const xmlrpc::MethodCall& call)  const;
    /// Connect client to given URI
    virtual jsonrpc::MethodResponse call(const jsonrpc::MethodCall& call)  const;
  };
}       // End namespace rpc
#endif  /* RPC_DIMCLIENT_H       */
