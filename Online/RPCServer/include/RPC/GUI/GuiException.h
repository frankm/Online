//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_GUIEXCEPTION_H
#define RPC_GUIEXCEPTION_H

/// Framework include files
#include "CPP/Interactor.h"
#include "TGMsgBox.h"

/// C++ include files
#include <string>
#include <memory>


/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  /// The main instance of the XML-RPC Gui
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class GuiException {
  public:
    std::string msg;
    std::string title;
    EMsgBoxIcon icon = kMBIconAsterisk;
    int         buttons = kMBOk;

    /// Initializing constructor
    GuiException(int buttons, EMsgBoxIcon icon, const char* title, const char* fmt, ...);    
    /// Default constructor inhibited
    GuiException() = delete;
    /// Copy constructor inhibited
    GuiException(const GuiException& gui) = delete;
    /// Move constructor inhibited
    GuiException(GuiException&& gui) = default; 
    /// Default destructor
    ~GuiException() = default;
    
    /// Assignment operator inhibited
    GuiException& operator=(const GuiException& gui) = delete;
    /// Move assignment inhibited
    GuiException& operator=(GuiException&& gui) = default;
    /// Send message to display target
    void send(CPP::Interactor* target)  const;
  };
}       // End namespace xmlrpc
#endif  /* RPC_GUIEXCEPTION_H         */
