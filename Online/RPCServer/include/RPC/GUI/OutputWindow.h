//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_OUTPUTWINDOW_H
#define RPC_OUTPUTWINDOW_H

/// Framework include files
#include "CPP/Interactor.h"

/// ROOT include files
#include "TGFrame.h"
#include "TGButton.h"
#include "TGTextView.h"
#include "TGDockableFrame.h"

/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  /// Explorer of the readout clients at a given DNS node
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class OutputWindow : public TGDockableFrame, public CPP::Interactor  {
  public:
    TGGroupFrame* group = 0;
    TGTextView*   view  = 0;
    TGTextButton* exit  = 0;
  public:
    /// Standard initializing constructor
    OutputWindow(TGWindow* p);
    /// Default destructor
    virtual ~OutputWindow();
    /// Interactor interrupt handler callback
    virtual void handle(const CPP::Event& ev)  override;
    /// Handle docking and other signals
    void handleDocking();

    /// ROOT class definition
    ClassDefOverride(OutputWindow,0);
  };
}       // End namespace xmlrpc
#endif  /* RPC_OUTPUTWINDOW_H  */
