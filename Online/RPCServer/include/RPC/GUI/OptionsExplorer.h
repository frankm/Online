//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_OPTIONSEXPLORER_H
#define RPC_OPTIONSEXPLORER_H

/// Framework include files
#include "RPC/DataflowRPC.h"
#include "CPP/Interactor.h"

/// ROOT include files
#include "TGFrame.h"

// C/C++ include files
#include <memory>
#include <set>

class TGGroupFrame;
class TGTextButton;
class TGComboBox;
class TGLabel;

/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  /// Explorer of the readout clients at a given DNS node
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class OptionsExplorer : public TGCompositeFrame, public CPP::Interactor  {
  public:
    TGGroupFrame*     group  = 0;
    struct LineEntry  {
      bool              isValid = false;
      TGLabel*          label = 0;
      TGComboBox*       input = 0;
      TGTextButton*     clear = 0;
      TGTextButton*     load  = 0;
      TGLabel*          fill  = 0;
    };
    /// Line entry to set dim-dns node
    LineEntry dns;
    /// Line entry to select single client
    LineEntry bridgeClients;
    LineEntry rpcClients;
    LineEntry dimClients;
    LineEntry victimOpts;

    LineEntry node;
    LineEntry part;

    Pixel_t   disabled, enabled;
    xmlrpc::dataflow::DomainInfo domain_client;
    CPP::Interactor*           gui = 0;

  public:
    enum IDS {
      DNS_ID_OFFSET           = 100,
      DNS_LABEL               = DNS_ID_OFFSET+1,
      DNS_INPUT               = DNS_ID_OFFSET+2,
      DNS_LOAD                = DNS_ID_OFFSET+3,
      DNS_CLEAR               = DNS_ID_OFFSET+4,
      DNS_FILL                = DNS_ID_OFFSET+5,
      NODE_ID_OFFSET          = 200,
      NODE_LABEL              = NODE_ID_OFFSET+1,
      NODE_INPUT              = NODE_ID_OFFSET+2,
      NODE_LOAD               = NODE_ID_OFFSET+3,
      NODE_CLEAR              = NODE_ID_OFFSET+4,
      NODE_FILL               = NODE_ID_OFFSET+5,
      PART_ID_OFFSET          = 300,
      PART_LABEL              = PART_ID_OFFSET+1,
      PART_INPUT              = PART_ID_OFFSET+2,
      PART_LOAD               = PART_ID_OFFSET+3,
      PART_CLEAR              = PART_ID_OFFSET+4,
      PART_FILL               = PART_ID_OFFSET+5,
      CLIENTS_ID_OFFSET       = 400,
      CLIENTS_LABEL           = CLIENTS_ID_OFFSET+1,
      CLIENTS_INPUT           = CLIENTS_ID_OFFSET+2,
      CLIENTS_LOAD            = CLIENTS_ID_OFFSET+3,
      CLIENTS_CLEAR           = CLIENTS_ID_OFFSET+4,
      CLIENTS_FILL            = CLIENTS_ID_OFFSET+5,
      DIMCLIENTS_ID_OFFSET    = 500,
      DIMCLIENTS_LABEL        = DIMCLIENTS_ID_OFFSET+1,
      DIMCLIENTS_INPUT        = DIMCLIENTS_ID_OFFSET+2,
      DIMCLIENTS_LOAD         = DIMCLIENTS_ID_OFFSET+3,
      DIMCLIENTS_CLEAR        = DIMCLIENTS_ID_OFFSET+4,
      DIMCLIENTS_FILL         = DIMCLIENTS_ID_OFFSET+5,
      BRIDGECLIENTS_ID_OFFSET = 600,
      BRIDGECLIENTS_LABEL     = BRIDGECLIENTS_ID_OFFSET+1,
      BRIDGECLIENTS_INPUT     = BRIDGECLIENTS_ID_OFFSET+2,
      BRIDGECLIENTS_LOAD      = BRIDGECLIENTS_ID_OFFSET+3,
      BRIDGECLIENTS_CLEAR     = BRIDGECLIENTS_ID_OFFSET+4,
      BRIDGECLIENTS_FILL      = BRIDGECLIENTS_ID_OFFSET+5,

      VICTIMCLIENTS_ID_OFFSET = 800,
      VICTIMCLIENTS_LABEL     = VICTIMCLIENTS_ID_OFFSET+1,
      VICTIMCLIENTS_INPUT     = VICTIMCLIENTS_ID_OFFSET+2,
      VICTIMCLIENTS_LOAD      = VICTIMCLIENTS_ID_OFFSET+3,
      VICTIMCLIENTS_OPEN      = VICTIMCLIENTS_ID_OFFSET+4,
      VICTIMCLIENTS_FILL      = VICTIMCLIENTS_ID_OFFSET+5,

      VICTIMOPTS_ID_OFFSET    = 800,
      VICTIMOPTS_LABEL        = VICTIMOPTS_ID_OFFSET+1,
      VICTIMOPTS_INPUT        = VICTIMOPTS_ID_OFFSET+2,
      VICTIMOPTS_LOAD         = VICTIMOPTS_ID_OFFSET+3,
      VICTIMOPTS_OPEN         = VICTIMOPTS_ID_OFFSET+4,
      VICTIMOPTS_FILL         = VICTIMOPTS_ID_OFFSET+5,

      LAST
    };
    /// Standard initializing constructor
    OptionsExplorer(TGFrame* parent, CPP::Interactor* gui);
    /// Default destructor
    virtual ~OptionsExplorer();

    /// Access the list of selected nodes
    std::set<std::string> nodeList()  const;

    /// Access the list of selected tasks
    std::set<std::string> taskList()  const;

    /// Interactor interrupt handler callback
    virtual void handle(const CPP::Event& ev)  override;
    /// Deferred execution using wtc
    void exec_call(Int_t cmd);

    /// Load the DNS domains into the widget
    void loadDns();
    /// Callback to clear the DNS text field
    void clearDnsField();
    /// Callback to clear the node text field
    void clearNodeField();
    /// Callback to clear the partition text field
    void clearPartField();
    /// Load all DNS nodes (callback of "Load")
    void loadNodes();
    /// Load all DNS clients of a DNS (callback of "Load")
    void loadClients();
    /// Callback when the DNS name gets edited
    void dnsChanged(Int_t selected_id);
    /// Load RPC client options of a single client
    void loadRpcClientOptions();
    /// Load the DIM client options on demand
    void loadDimClientOptions();
    /// Load the bridge client options on demand
    void loadBridgeClientOptions();
    /// Load the victim node client options on demand
    void loadVictimOptions();
    /// Open the victim node client options on demand
    void openVictimOptions();

    /// Callback when the node name gets edited
    void nodeChanged(Int_t selected_id);
    /// Callback when the partition name gets edited
    void partitionChanged(Int_t selected_id);
    /// High level change of the selector. Erase all content
    void clearClientSelector();
    /// ROOT class definition
    ClassDefOverride(OptionsExplorer,0);
  };
}       // End namespace xmlrpc
#endif  /* RPC_OPTIONSEXPLORER_H  */
