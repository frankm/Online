//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_GUICOMMAND_H
#define RPC_GUICOMMAND_H

/// C++ include files
#include <string>

/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  enum {
    XMLRPCGUI_SHOW_CLIENT = 1,
    XMLRPCGUI_SHOW_RPCCLIENT,
    XMLRPCGUI_SHOW_DIMCLIENT,
    XMLRPCGUI_SHOW_BRIDGERPCCLIENT,
    XMLRPCGUI_SHOW_VICTIMOPTIONS,

    XMLRPCGUI_SHOW_EDITORPANE,
    XMLRPCGUI_SHOW_EXPLORERPANE,
    XMLRPCGUI_SHOW_VICTIMNODEPANE,
    XMLRPCGUI_SHOW_OUTPUTLINE,
    XMLRPCGUI_SHOW_OUTPUTBOTTOM,
    XMLRPCGUI_UPDATE_DNS_LIST,
    XMLRPCGUI_UPDATE_OUTPUT,

    XMLRPCGUI_SEND_NODELIST,
    XMLRPCGUI_HAVE_NODELIST,
    XMLRPCGUI_SEND_TASKLIST,
    XMLRPCGUI_HAVE_TASKLIST,

    XMLRPCGUI_EXCEPTION,
    XMLRPCGUI_LAST
  };

  /// The main instance of the XML-RPC Gui
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class GuiCommand  final {
  public:
    std::string dns, node, client, opts;
  public:
  GuiCommand(const std::string& d, const std::string& n, const std::string& c, const std::string& o="") 
    : dns(d), node(n), client(c), opts(o) {}
    /// Default constructor inhibited
    GuiCommand() = default;
    /// Copy constructor inhibited
    GuiCommand(const GuiCommand& gui) = default;
    /// Move constructor inhibited
    GuiCommand(GuiCommand&& gui) = default;    
    /// Default destructor
    ~GuiCommand() = default;

    /// Assignment operator inhibited
    GuiCommand& operator=(const GuiCommand& gui) = default;
    /// Move assignment inhibited
    GuiCommand& operator=(GuiCommand&& gui) = default;
  };
}       // End namespace xmlrpc
#endif  /* RPC_GUICOMMAND_H  */
