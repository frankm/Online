//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_RPCBRIDGEOPTIONSEDITOR_H
#define RPC_RPCBRIDGEOPTIONSEDITOR_H

/// Framework include files
#include "RPC/GUI/RPCOptionsEditor.h"

/// ROOT include files

// C++ ioncludes

/// Forward declarations

/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  /// Job options editor panel
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class RPCBridgeOptionsEditor : public RPCOptionsEditor  {

  public:
    /// Standard Constructor
    RPCBridgeOptionsEditor(TGWindow* p, CPP::Interactor* gui);
    /// Default destructor
    virtual ~RPCBridgeOptionsEditor();

    /** RPC interaction routines  */
    /// Create communication client
    virtual void createClient(GuiCommand* c)  override;

    /// ROOT class definition
    ClassDefOverride(RPCBridgeOptionsEditor,0);
  };
}       // End namespace xmlrpc
#endif  /* RPC_RPCBRIDGEOPTIONSEDITOR_H  */
