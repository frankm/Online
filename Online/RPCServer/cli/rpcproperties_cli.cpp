//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <RTL/rtl.h>
#include "PropertyCLI.h"

// C/C++ include files
#include <iostream>
#include <cerrno>

using namespace xmlrpc;
using namespace std;

namespace {

  size_t cli_print(void* /* arg */,int lvl,const char* fmt, va_list& args) {
    size_t result;
    std::string format;
    switch(lvl) {
    case LIB_RTL_VERBOSE:
      format = "VERBOSE ";
      break;
    case LIB_RTL_DEBUG:
      format = "DEBUG   ";
      break;
    case LIB_RTL_INFO:
      format = "INFO    ";
      break;
    case LIB_RTL_WARNING:
      format = "WARNING ";
      break;
    case LIB_RTL_ERROR:
      format = "ERROR   ";
      break;
    case LIB_RTL_FATAL:
      format = "FATAL   ";
      break;
    case LIB_RTL_ALWAYS:
      format = "ALWAYS  ";
      break;
    default:
      break;
    }
    format += fmt;
    format += "\n";
    char buffer[1024];
    result = ::vsnprintf(buffer,sizeof(buffer), format.c_str(), args);
    buffer[sizeof(buffer)-1] = 0;
    ::fprintf(stdout,"%s",buffer);
    return result;
  }
  void help()  {
    ::exit(EINVAL);
  }
  PropertyCLI setupCLI(int argc, char** argv)   {
    PropertyCLI access;
    RTL::CLI cli(argc,argv,help);
    cli.getopt("print",5, access.print);
    return access;
  }
}

extern "C" int rpcproperties_show_domains(int argc, char** argv)   {
  PropertyCLI access(setupCLI(argc,argv));
  return access.showDomains();
}

extern "C" int rpcproperties_show_nodes(int argc,char** argv)   {
  PropertyCLI access(setupCLI(argc,argv));
  std::string dns;
  RTL::CLI cli(argc,argv,help);
  cli.getopt("dns",3,dns);
  return access.showNodes(dns);
}

extern "C" int rpcproperties_show_tasks(int argc,char** argv)   {
  PropertyCLI access(setupCLI(argc,argv));
  string dns, node, match = "*";
  RTL::CLI cli(argc,argv,help);
  cli.getopt("dns",3,dns);
  cli.getopt("node",3,node);
  cli.getopt("match",3,match);

  return node.empty() 
    ? access.showTasksByRegex(dns,match)
    : access.showTasks(dns,node);
}

extern "C" int rpcproperties_show_options(int argc,char** argv)   {
  PropertyCLI access(setupCLI(argc,argv));
  string dns, task = "*";
  RTL::CLI cli(argc,argv,help);
  cli.getopt("dns",3,dns);
  cli.getopt("task",3,task);
  return access.showOpts(dns, task);
}

extern "C" int rpcproperties_set_option(int argc,char** argv)   {
  PropertyCLI access(setupCLI(argc,argv));
  string dns, task = "*", option;
  RTL::CLI cli(argc,argv,help);
  cli.getopt("dns",3,dns);
  cli.getopt("task",3,task);
  cli.getopt("option",3,option);
  return access.setOption(dns, task, option);
}

extern "C" int rpcproperties_download_options(int argc,char** argv)   {
  PropertyCLI access(setupCLI(argc,argv));
  string dns, task = "*", file;
  RTL::CLI cli(argc,argv,help);
  cli.getopt("dns",3,dns);
  cli.getopt("task",3,task);
  cli.getopt("file",3,file);
  return access.downloadOptions(dns, task, file);
}

extern "C" int rpcproperties_cli(int argc, char** argv)   {
  int result = EINVAL, print = LIB_RTL_INFO;
  string command;
  RTL::CLI cli(argc,argv,help);
  cli.getopt("command",3,command);
  cli.getopt("print",5, print);
  ::lib_rtl_install_printer(cli_print,nullptr);
  ::lib_rtl_set_log_level(print);
  if ( command == "show-domains" )
    result = rpcproperties_show_domains(argc, argv);
  else if ( command.substr(0,8)  == "show-nod" )
    result = rpcproperties_show_nodes(argc, argv);
  else if ( command.substr(0,8)  == "show-tas" )
    result = rpcproperties_show_tasks(argc, argv);
  else if ( command.substr(0,8)  == "show-opt" )
    result = rpcproperties_show_options(argc, argv);
  else if ( command.substr(0,10) == "set-option" )
    result = rpcproperties_set_option(argc, argv);
  else if ( command.substr(0,8)  == "download" )
    result = rpcproperties_set_option(argc, argv);
  if ( 0 == result ) return 0;
  help();
  return EINVAL;
}
