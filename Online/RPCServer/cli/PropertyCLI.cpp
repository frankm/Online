//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <RTL/rtl.h>
#include <HTTP/HttpHeader.h>
#include <Options/Analyzer.h>
#include <Options/Messages.h>
#include <Options/Catalog.h>
#include <Options/Units.h>
#include <Options/PragmaOptions.h>
#include <Options/Node.h>
#include "PropertyCLI.h"

// C/C++ include files
#include <iostream>
#include <cerrno>

using namespace xmlrpc;
using namespace std;

/// Default constructor
PropertyCLI::PropertyCLI()   {
}

/// Access the domain client
dataflow::DomainInfo& PropertyCLI::domainClient()   {
  if ( !m_domainClient.get() )  {
    auto cl = rpc::client<http::HttpClient>();
    (*cl)->userRequestHeaders.push_back(http::HttpHeader("Dim-DNS","ecs03"));
    (*cl)->userRequestHeaders.push_back(http::HttpHeader("Dim-Server","/RPCDomainInfo"));
    (*cl)->userRequestHeaders.push_back(http::HttpHeader("RPC-Port","2601"));
    (*cl)->open("ecs03.lbdaq.cern.ch", 2600);
    m_domainClient.handler = std::move(cl);
  }
  return m_domainClient;
}

/// Printout of a single client option
void PropertyCLI::printOption(const string& client, const string& name, const string& value)  const  {
  ::lib_rtl_output(LIB_RTL_ALWAYS," %s.%s = %s;",
		   client.c_str(), name.c_str(), value.c_str());
}

/// Access the task client
dataflow::PropertyContainer& PropertyCLI::taskClient(const string& dns, const string& task)   {
  if ( !m_taskClient.get() )   {
    auto cl = rpc::client<http::HttpClient>(dns, 2600);
    (*cl)->userRequestHeaders.push_back(http::HttpHeader("RPC-Server",task));
    m_taskClient = std::move(cl);
  }
  return m_taskClient;
}

/// Show the known DNS domains
int PropertyCLI::showDomains()  {
  dataflow::DomainInfo::Result res = this->domainClient().domains();
  if ( !res.empty() )   {
    int cnt = 0;
    for(const auto& d : res)  {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"%04d  %s",cnt,d.c_str());
      ++cnt;
    }
    return 0;
  }
  return EINVAL;
}

/// Show all nodes in a DNS domain
int PropertyCLI::showNodes(const string& dns)   {
  if ( !dns.empty() )  {
    PropertyCLI access;
    dataflow::DomainInfo::Result res = this->domainClient().nodes(dns);
    if ( !res.empty() )   {
      int cnt = 0;
      for(const auto& d : res)  {
	::lib_rtl_output(LIB_RTL_ALWAYS,"%04d  %s",cnt,d.c_str());
	++cnt;
      }
      return 0;
    }
    ::lib_rtl_output(LIB_RTL_WARNING,"The call did not return a result. "
		     "Was the dns '%s' correct?", dns.c_str());
    return EINVAL;
  }
  return EINVAL;
}

/// Listthe tasksof a given node
int PropertyCLI::showTasks(const string& dns,const string& node)  {
  if ( !dns.empty() )  {
    dataflow::DomainInfo::Result res = this->domainClient().tasks(dns, node);
    if ( !res.empty() )   {
      int cnt = 0;
      for(const auto& d : res)  {
	::lib_rtl_output(LIB_RTL_ALWAYS,"%04d  %s",cnt,d.c_str());
	++cnt;
      }
      return 0;
    }
    ::lib_rtl_output(LIB_RTL_WARNING,"The call did not return a result. "
		     "Was the dns '%s' and the match '%s' correct?", 
		     dns.c_str(), node.c_str());
    return EINVAL;
  }
  return EINVAL;
}

/// List the tasksof a given node according toa selection
int PropertyCLI::showTasksByRegex(const string& dns,const string& match)   {
  if ( !dns.empty() )  {
    dataflow::DomainInfo::Result res = this->domainClient().tasksByRegex(dns, match);
    if ( !res.empty() )   {
      int cnt = 0;
      for(const auto& d : res)  {
	::lib_rtl_output(LIB_RTL_ALWAYS,"%04d  %s",cnt,d.c_str());
	++cnt;
      }
      return 0;
    }
    ::lib_rtl_output(LIB_RTL_WARNING,"The call did not return a result. "
		     "Was the dns '%s' and the match '%s' correct?", 
		     dns.c_str(), match.c_str());
    return EINVAL;
  }
  return EINVAL;
}

/// Show all the options of a particular task
int PropertyCLI::showOpts(const string& dns, const string& task)   {
  if ( !dns.empty() )  {
    dataflow::PropertyContainer::Properties res = this->taskClient(dns, task).allProperties();
    if ( !res.empty() )   {
      for(const auto& p : res)  {
	printOption(p.client, p.name,p.value);
      }
      return 0;
    }
    ::lib_rtl_output(LIB_RTL_WARNING,"The call did not return a result. "
		     "Was the dns '%s' and the task '%s' correct?", 
		     dns.c_str(), task.c_str());
    return EINVAL;
  }
  return EINVAL;
}

/// Set a single option to an existing process
int PropertyCLI::setOption(const std::string& dns,
			   const std::string& task, 
			   const std::string& opt)
{
  size_t l1 = opt.find('.');
  size_t l2 = opt.find('=');
  string err = "Unknown error";
  dataflow::PropertyContainer& info = this->taskClient(dns, task);

  try  {
    if ( l1 != string::npos && l2 != string::npos )   {
      string cl = opt.substr(0,l1);
      string nam = opt.substr(l1+1,l2-l1-1);
      string val = opt.substr(l2+1);
      if ( print ) printOption(cl, nam, val);
      info.setProperty(cl, nam, val);
      return 0;
    }
  }
  catch(const std::exception& e)   {
    err = e.what();
  }
  ::lib_rtl_output(LIB_RTL_WARNING,"The call failed: %s "
		   "Was the dns '%s' and the task '%s' and the option '%s' is correct?", 
		   err.c_str(), dns.c_str(), task.c_str(), opt.c_str());
  return EINVAL;
}

/// Download options from file to an existing process
int PropertyCLI::downloadOptions(const string& dns,
				 const string& task,
				 const string& file)
{
  namespace gp = Gaudi::Parsers;
  string err = "Unknown error";
  try  {
    vector<rpc::ObjectProperty> properties;
    string       search_path = ::getenv("JOBOPTIONSPATH") ? ::getenv("JOBOPTIONSPATH") : ".";
    gp::PragmaOptions pragma;
    gp::Messages messages(2,::lib_rtl_output);
    gp::Catalog  catalog;
    gp::Units    units;
    gp::Node     ast;
    int sc = gp::ReadOptions(file, search_path, &messages, &catalog, &units, &pragma, &ast);
    // --------------------------------------------------------------------------
    if ( sc )    {
      if (pragma.IsPrintOptions()) {
	cout << "Print options" << endl << catalog << endl;
      }
      if (pragma.IsPrintTree()) {
	cout << "Print tree:" << endl << ast.ToString() << endl;
      }
      messages.AddInfo("Job options successfully read in from "+file);
      for (const auto&  cl : catalog)   {
	const string& client = cl.first;
	for (const auto& c : cl.second )   {
	  rpc::ObjectProperty p(client,c.NameInClient(),c.ValueAsString());
	  if ( print )  {
	    printOption(client,c.NameInClient(),c.ValueAsString());
	  }
	  properties.push_back(p);
	}
      }
      // Now download this options block:
      int res = this->taskClient(dns, task).setProperties(properties);
      if ( int(properties.size()) == res )    {
	::lib_rtl_output(LIB_RTL_ALWAYS,"Successfully sent %ld options to %s",
			 properties.size(),task.c_str());
	return 0;
      }
      ::lib_rtl_output(LIB_RTL_ALWAYS,"Process %s only accepted %ld options",
		       task.c_str(), properties.size());
    }
  }
  catch(const std::exception& e)   {
    err = e.what();
  }
  ::lib_rtl_output(LIB_RTL_ERROR,"The call failed: %s "
		   "Was the dns '%s' and the task '%s' and the options '%s' are correct?", 
		   err.c_str(), dns.c_str(), task.c_str(), file.c_str());
  return EINVAL;
}
