//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <RPC/HttpJsonRpcHandler.h>
#include <RPC/HttpXmlRpcHandler.h>
#include <RPC/HttpRpcHandler.h>
#include <XML/Printout.h>
#include <RTL/Logger.h>
#include <dim/dis.h>

// C/C++ include files
#include <iostream>
#include <cstring>
#include <cerrno>

namespace  {
  void help_server()  {
    ::printf
      ("xmlrpc_server -opt [-opt]\n"
       "   -host    <host-name>    Server host name. Default: 0.0.0.0          \n"
       "   -port    <port-number>  Server port       Default: 8000             \n"
       "   -utgid   <process-name> Dim server name   Default: HttpDimRpcBridge \n"
       "   -options <file>         Supply redirection information.             \n"
       "   -threads <number>       Number of additional helper threads         \n"
       "   -debug                  Set debug flag ON                           \n");
    ::exit(EINVAL);
  }
}

extern "C" int run_http_rpc_bridge(int argc, char** argv)   {
  using namespace dd4hep;
  int debug   = 0;
  int threads = 0;
  int port    = 8000;
  std::string host = "0.0.0.0", utgid="HttpRpcBridge", options;
  for(int i = 0; i < argc && argv[i]; ++i)  {
    if ( 0 == ::strncmp("-port",argv[i],4) )
      port = ::atol(argv[++i]);
    else if ( 0 == ::strncmp("-host",argv[i],4) )
      host = argv[++i];
    else if ( 0 == ::strncmp("-utgid",argv[i],4) )
      utgid = argv[++i];
    else if ( 0 == ::strncmp("-threads",argv[i],4) )
      threads = ::atol(argv[++i]);
    else if ( 0 == ::strncmp("-options",argv[i],4) )
      options = argv[++i];
    else if ( 0 == ::strncmp("-debug",argv[i],4) )
      debug = 1;
    else if ( 0 == ::strncmp("-help",argv[i],2) )
      help_server();
  }
  RTL::Logger::install_log(RTL::Logger::log_args(debug ? LIB_RTL_DEBUG : LIB_RTL_INFO));


  auto json_handler  = std::make_shared<rpc::RpcHandlerImp<rpc::HttpJsonRpcHandler> >();
  (*json_handler)->mode = rpc::HttpServer::BRIDGE;
  (*json_handler)->server_uri  = "/JSONRPC";
  (*json_handler)->debug       = debug != 0;
  auto xml_handler1  = std::make_shared<rpc::RpcHandlerImp<rpc::HttpXmlRpcHandler> >();
  (*xml_handler1)->mode = rpc::HttpServer::BRIDGE;
  (*xml_handler1)->server_uri  = "/XMLRPC";
  (*xml_handler1)->debug       = debug != 0;
  auto xml_handler2  = std::make_shared<rpc::RpcHandlerImp<rpc::HttpXmlRpcHandler> >();
  (*xml_handler2)->mode = rpc::HttpServer::BRIDGE;
  (*xml_handler2)->server_uri  = "/RPC2";
  (*xml_handler2)->debug       = (debug&1) != 0;

  auto handler       = std::make_unique<rpc::HttpServer::Handler>();
  handler->mountPoints.handlers.emplace((*json_handler)->server_uri, std::move(json_handler));
  handler->mountPoints.handlers.emplace((*xml_handler1)->server_uri, std::move(xml_handler1));
  handler->mountPoints.handlers.emplace((*xml_handler2)->server_uri, std::move(xml_handler2));

  rpc::HttpServer server(std::move(handler), host, port, rpc::HttpServer::SERVER);
#if 0
  if ( !options.empty() )    {
    rpc::RedirectionParser parser;
    auto result = parser.parse(options);
    for(auto& m : result)   {
      if ( m.find("server") != m.end() )
	server.addServerRedirection(m["server"], m["to"], m["port"]);
      if ( m.find("url") != m.end() )
	server.addUrlRedirection(m["url"], m["to"], m["port"], m["to_url"]);
    }
  }
#endif
  server.setDebug(debug != 0);
  printout(INFO,"HTTPSERVER",
	   "TEST> Starting test XMLRPC server with URI:%s:%d [%d threads]\n",
	   host.c_str(), port, threads);
  ::dis_start_serving(utgid.c_str());
  server.start(false, threads);
  return 0;
}
