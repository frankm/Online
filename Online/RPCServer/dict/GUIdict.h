//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

namespace xmlrpc {}

#include "RPC/GUI/MessageBoxImp.h"
#include "RPC/GUI/OptionsEditor.h"
#include "RPC/GUI/RPCOptionsEditor.h"
#include "RPC/GUI/DIMOptionsEditor.h"
#include "RPC/GUI/RPCBridgeOptionsEditor.h"
#include "RPC/GUI/VictimNodeEditor.h"
#include "RPC/GUI/OptionsExplorer.h"
#include "RPC/GUI/VictimNodeExplorer.h"

#ifdef __ROOTCLING__
#pragma link C++ namespace xmlrpc;

#pragma link C++ class xmlrpc::XMLRPCGUI;
#pragma link C++ class xmlrpc::OutputWindow;
#pragma link C++ class xmlrpc::MessageBoxImp;
#pragma link C++ class xmlrpc::OptionsEditor;
#pragma link C++ class xmlrpc::OptionsEditor::Row;
#pragma link C++ class xmlrpc::DIMOptionsEditor;
#pragma link C++ class xmlrpc::RPCOptionsEditor;
#pragma link C++ class xmlrpc::RPCBridgeOptionsEditor;
#pragma link C++ class xmlrpc::VictimNodeEditor;
#pragma link C++ class xmlrpc::OptionsExplorer;
#pragma link C++ class xmlrpc::OptionsExplorer::LineEntry;
#pragma link C++ class xmlrpc::VictimNodeExplorer;
#pragma link C++ class xmlrpc::VictimNodeExplorer::LineEntry;

#endif
