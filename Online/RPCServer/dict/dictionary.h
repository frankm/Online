//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

#include <CPP/ObjectProperty.h>
#include <RPC/ComponentClient.h>
#include <RPC/DimClient.h>
#include <RPC/DimRequest.h>
#include <RPC/DimServer.h>
#include <RPC/HttpClient.h>
#include <RPC/HttpRpcServer.h>
#include <RPC/XMLRPC.h>
#include <RPC/DataflowRPC.h>
#include <XML/XML.h>
#include <HTTP/HttpServer.h>

#ifdef __ROOTCLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

namespace {
  struct inst {
    std::vector<xmlrpc::ObjectProperty> foo;
  };
}
#pragma link C++ namespace dd4hep;
#pragma link C++ namespace dd4hep::xml;
#pragma link C++ namespace xmlrpc;

#pragma link C++ class dd4hep::xml::Strng_t+;
#pragma link C++ class dd4hep::xml::Handle_t+;
#pragma link C++ class dd4hep::xml::Tag_t+;
#pragma link C++ class dd4hep::xml::Attribute+;
#pragma link C++ class dd4hep::xml::Collection_t+;
#pragma link C++ class dd4hep::xml::Element+;
#pragma link C++ class dd4hep::xml::RefElement+;
#pragma link C++ class dd4hep::xml::Dimension+;
#pragma link C++ class dd4hep::xml::DetElement+;
#pragma link C++ class dd4hep::xml::Component+;
#pragma link C++ class dd4hep::xml::ChildValue+;
#pragma link C++ class dd4hep::xml::Document+;
#pragma link C++ class dd4hep::xml::DocumentHolder+;
#pragma link C++ class dd4hep::xml::DocumentHandler+;

#pragma link C++ class xmlrpc::Arg+;
#pragma link C++ class xmlrpc::Array+;
#pragma link C++ class xmlrpc::Call+;
#pragma link C++ class std::vector<xmlrpc::Call>+;
#pragma link C++ class xmlrpc::CallSequence+;
#pragma link C++ class xmlrpc::ComponentClient+;
#pragma link C++ class xmlrpc::DimClient+;
#pragma link C++ class xmlrpc::DimRequest+;
#pragma link C++ class xmlrpc::DimServer+;
#pragma link C++ class xmlrpc::HttpClient+;
#pragma link C++ class xmlrpc::HttpServer+;
#pragma link C++ class xmlrpc::MethodCall+;
#pragma link C++ class xmlrpc::MethodResponse+;
#pragma link C++ class xmlrpc::ObjectProperty+;
#pragma link C++ class std::vector<xmlrpc::ObjectProperty>+;
#pragma link C++ class xmlrpc::RPCClient+;
#pragma link C++ class xmlrpc::Structure+;
#pragma link C++ class xmlrpc::XmlCoder+;
#pragma link C++ class xmlrpc::DataflowRPC;
#pragma link C++ class xmlrpc::DomainInfo;
#pragma link C++ class xmlrpc::PropertyContainer;
#endif

/// Namespace for the AIDA detector description toolkit
namespace dd4hep {
  /// Namespace for the AIDA detector description toolkit supporting XML utilities
  namespace xml {

    /// Dummy class definition for cling. Class declaration otherwise is sufficient
    class XmlAttr {
    public:
      inline XmlAttr() {}
      inline ~XmlAttr() {}
    };
  }
}
