# ==========================================================================
#   AIDA Detector description implementation for LCD
# --------------------------------------------------------------------------
#  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
#  All rights reserved.
# 
#  For the licensing terms see $DD4hepINSTALL/LICENSE.
#  For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
# 
#  Author     : M.Frank
# 
# ==========================================================================

import ROOT
from ROOT import gSystem
gSystem.Load('libRPCTests')
from ROOT import xmlrpc as rpc

import cppyy
std = cppyy.gbl.std
std_vector = std.vector
std_list   = std.list
std_map    = std.map
std_pair   = std.pair
dataflow   = rpc.dataflow

# We are nearly there ....
name_space = __import__(__name__)
def import_namespace_item(ns,nam):  
  scope = getattr(name_space,ns)
  attr = getattr(scope,nam)
  setattr(name_space,nam,attr)
  return attr

def import_root(nam):
  #logging.info('import ROOT class %s in namespace %s',nam,str(name_space))
  setattr(name_space,nam,getattr(ROOT,nam))

import_namespace_item('rpc','Arg')
import_namespace_item('rpc','Array')
import_namespace_item('rpc','Call')
import_namespace_item('rpc','CallSequence')
import_namespace_item('rpc','ComponentClient')
import_namespace_item('rpc','DimClient')
import_namespace_item('rpc','DimRequest')
import_namespace_item('rpc','DimServer')
import_namespace_item('rpc','HttpClient')
import_namespace_item('rpc','HttpServer')
#import_namespace_item('rpc','MethodCall')
#import_namespace_item('rpc','MethodResponse')
import_namespace_item('rpc','ObjectProperty')
import_namespace_item('rpc','RPCClient')
import_namespace_item('rpc','Structure')
import_namespace_item('rpc','XmlCoder')
import_namespace_item('dataflow','DataflowRPC')
import_namespace_item('dataflow','DomainInfo')
import_namespace_item('dataflow','PropertyContainer')


def domainInfo(server='DomainInfo',dns='ecs03'):
  rpc = DimClient.create(server,dns)
  cl  = DomainInfo(rpc)
  return cl
