from __future__ import print_function
# ==========================================================================
#   AIDA Detector description implementation for LCD
# --------------------------------------------------------------------------
#  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
#  All rights reserved.
# 
#  For the licensing terms see $DD4hepINSTALL/LICENSE.
#  For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
# 
#  Author     : M.Frank
# 
# ==========================================================================
from builtins import str
from builtins import range
import dimxmlrpc

def connect(server='Test'):
  rpc = dimxmlrpc.DimClient.create(server)
  cl  = dimxmlrpc.ComponentClient(rpc)
  return cl

client = connect()

def test():
  calls = 0
  cc = client.clients()
  calls = calls + 1
  if len(cc) != 100:
    print('%-8d calls ---'%(calls,), 'DIMXMLRPC test: Error No.Clients != 100!')
    return
  count = 0
  for c in cc:
    pp = client.clientProperties(c)
    calls = calls + 1
    if len(pp) != 20:
      print('%-8d calls ---'%(calls,), 'DIMXMLRPC test: Error No.Properties(%s) != 20!'%(c,))
      return
    print('%-8d calls ---'%(calls,), 'DIMXMLRPC test: %s has %d options.'%(c,len(pp),))
    count += len(pp)
    for i in range(20):
      n = 'Property_%d'%(i,)
      p = client.property(c,n)
      calls = calls + 1
      check = 'property_'+c+'_Property_'+str(i)+'_value_'+str(i)
      if p.value != check:
        print('%-8d calls ---'%(calls,), 'DIMXMLRPC test: %s has BAD option: %s.%s=%s != %s'%(c,c,p.name,p.value,check,))
        return
    print('%-8d calls ---'%(calls,), 'DIMXMLRPC test: %s Options CHECKED.'%(c,))
  all = client.allProperties()
  calls = calls + 1
  if len(all) != count:
    print('%-8d calls ---'%(calls,), 'DIMXMLRPC test: Wrong number of all options: %s != %d'%(len(all),count,))
    return

  pp = client.clientProperties(cc[0])
  calls = calls + 1
  for p in pp:
    np = client.namedProperties(p.name)
    calls = calls + 1
    if len(np) != len(cc):
      print('%-8d calls ---'%(calls,), 'DIMXMLRPC test: Wrong number of named props (%s): %d != %d',(p.name,len(np),len(cc),))
      return

  print('SUCCESS! ALL TESTS PASSED!  :-)))')
