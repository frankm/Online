# ==========================================================================
#   AIDA Detector description implementation for LCD
# --------------------------------------------------------------------------
#  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
#  All rights reserved.
# 
#  For the licensing terms see $DD4hepINSTALL/LICENSE.
#  For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
# 
#  Author     : M.Frank
# 
# ==========================================================================
source /group/online/dataflow/cmtuser/OnlineRelease/setup.x86_64-centos7-gcc62-do0.vars;
export UTGID=${HOST}_${USER}_PropGUI_$$;
###cd ${RPCROOT};
###/home/beat/scripts/debug --args 
gentest libRPCgui.so rpcproperties_gui;
