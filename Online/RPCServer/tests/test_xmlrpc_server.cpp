//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "RPC/HttpXmlRpcHandler.h"
#include "test_rpc_server.h"

namespace  {
  void help_server()  {
    ::printf
      ("xmlrpc_server -opt [-opt]\n"
       "   -host=<host-name>     Server host name. Default: 0.0.0.0\n"
       "   -port=<port-number>   Server port       Default: 8000   \n"
       "   -debug                Enable debugging to stdout        \n");
    ::exit(EINVAL);
  }
}

extern "C" int test_xmlrpc_server(int argc, char** argv)   {
  using namespace dd4hep;
  int debug = 0;
  int port  = 8000;
  std::string host = "0.0.0.0";
  for(int i = 0; i < argc && argv[i]; ++i)  {
    if ( 0 == ::strncmp("-port",argv[i],4) )
      port = ::atol(argv[++i]);
    else if ( 0 == ::strncmp("-host",argv[i],4) )
      host = argv[++i];
    else if ( 0 == ::strncmp("-debug",argv[i],4) )
      debug = 1;
    else if ( 0 == ::strncmp("-help",argv[i],2) )
      help_server();
  }
  rpc::HttpServer server(std::make_unique<rpc::HttpXmlRpcHandler>(),
			 host,port,rpc::HttpServer::SERVER);
  server.setDebug(debug != 0);
  printout(INFO,"HTTPSERVER","TEST> Starting test XMLRPC server with URI:%s:%d\n",host.c_str(),port);
  return call_server(argc, argv, server);
}
