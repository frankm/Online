//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

#include "Objects.h"
#include "RPC/XMLRPC.h"

using namespace test_rpc;
using namespace std;


// Example how to implement data marshalling for an arbitrary class
namespace xmlrpc {
  template <> Object XmlCoder::to() const {
    Object val;
    xml_h struc = element.child(_rpcU(struct));
    for(xml_coll_t c(struc,_rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      if      ( n == "a" ) val.a = Arg::get_int(c);
      else if ( n == "b" ) val.b = Arg::get_int(c);
    }
    return val;
  }

  template <> void XmlCoder::from(const Object& b) const {
    Structure(element).add("a", b.a);
    Structure(element).add("b", b.b);
  }
}

XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(struct,Object)

XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(vector,Object)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,vector<Object>)

XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(list,Object)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,list<Object>)

XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(set,Object)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,set<Object>)
