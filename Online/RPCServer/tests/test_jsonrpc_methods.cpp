//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "RPC/JSONRPC.h"
#include "Score.h"

using namespace std;
using namespace jsonrpc;

namespace {
  /// Fill jsonrpc-structure with default values
  void fill_structure(Structure& a, int level=1);
  /// Fill jsonrpc-array with default values
  void fill_array(Array& a, int level=1);
  /// Decode jsonrpc-array to values
  void decodeArray(const string& prefix, json_h& array);
  /// Decode jsonrpc-structure to values
  void decodeStruct(const string& prefix, json_h& structure);

  template <typename T> void test_encode_response(const string& tag, const T& val)  {
    MethodResponse doc = MethodResponse::make(val);
    logger("+++ MethodResponse<"+tag+">: ") << endl
					    << doc.str()
					    << endl;
    ++s_test_count;
  }

  /// Decode jsonrpc-data iteratively to primitives
  void decodeItem(const string& prefix, json_h elt)  {
    auto typ = elt.type();
    if ( typ == json_h::value_t::null )  {
      logger(prefix) << " Type: " << (int)typ << " -> null            " << elt << endl;
    }
    else if ( typ == json_h::value_t::object )   {
      logger(prefix) << " Type: " << (int)typ << " -> object          " << elt << endl;
      decodeStruct(prefix+"(struct)",elt);
      return;
    }
    else if ( typ == json_h::value_t::array )   {
      logger(prefix) << " Type: " << (int)typ << " -> array           " << elt << endl;
      decodeArray(prefix+"(array)",elt);
      return;
    }
    else if ( typ == json_h::value_t::string )   {
      logger(prefix) << " Type: " << (int)typ << " -> string          " << elt << endl;
    }
    else if ( typ == json_h::value_t::boolean )   {
      logger(prefix) << " Type: " << (int)typ << " -> boolean         " << elt << endl;
    }
    else if ( typ == json_h::value_t::number_integer )   {
      logger(prefix) << " Type: " << (int)typ << " -> number_integer  " << elt << endl;
    }
    else if ( typ == json_h::value_t::number_unsigned )   {
      logger(prefix) << " Type: " << (int)typ << " -> number_unsigned " << elt << endl;
    }
    else if ( typ == json_h::value_t::number_float )   {
      logger(prefix) << " Type: " << (int)typ << " -> number_float    " << elt << endl;
    }
    else if ( typ == json_h::value_t::discarded )   {
      logger(prefix) << " Type: " << (int)typ << " -> discarded " << endl;
    }
    else   {
      logger(prefix) << " Type: " << (int)typ << " -> UNKNOWN data type " << endl;
    }
  }

  /// Decode jsonrpc-array to values
  void decodeArray(const string& prefix, json_h& array)  {
    for(auto& v : array)   {
      int count = 0;
      stringstream str;
      str << prefix << "[" << count << "]";
      decodeItem(str.str(),v);
      ++count;
    }
  }

  /// Decode jsonrpc-structure to values
  void decodeStruct(const string& prefix, json_h& structure)  {
    for (auto& [n,v] : structure.items()) {
      // According to the standard exactly 1 data element is allowed!
      decodeItem(prefix+"."+n, v);
    }
  }

  /// Fill jsonrpc-array with default values
  void fill_array(Array& a, int level)   {
    a.add(true);
    ++s_test_count;
    a.add('0');
    ++s_test_count;
    a.add((signed char)-1);
    ++s_test_count;
    a.add((unsigned char)1);
    ++s_test_count;
    a.add((short)-2);
    ++s_test_count;
    a.add((unsigned short)2);
    ++s_test_count;
    a.add((int)-3);
    ++s_test_count;
    a.add((unsigned int)3);
    ++s_test_count;
    a.add((long)-4);
    ++s_test_count;
    a.add((unsigned long)4);
    ++s_test_count;
    a.add((float)5);
    ++s_test_count;
    a.add((double)6);
    ++s_test_count;
    a.add(string("7"));
    ++s_test_count;
    if ( level >= 0 )   {
      Structure s;
      fill_structure(s, level-1);
      a.add(s);
    }
  }

  /// Fill jsonrpc-structure with default values
  void fill_structure(Structure& a, int level)   {
    a.add("member_bool",true);
    ++s_test_count;
    a.add("member_char",'0');
    ++s_test_count;
    a.add("member_signed_char",(signed char)-1);
    ++s_test_count;
    a.add("member_unsigned_char",(unsigned char)1);
    ++s_test_count;
    a.add("member_short",(short)-2);
    ++s_test_count;
    a.add("member_unsigned_short",(unsigned short)2);
    ++s_test_count;
    a.add("member_int",(int)-3);
    ++s_test_count;
    a.add("member_unsigned_int",(unsigned int)3);
    ++s_test_count;
    a.add("member_long",(long)-4);
    ++s_test_count;
    a.add("member_unsigned_long",(unsigned long)4);
    ++s_test_count;
    a.add("member_float",(float)5);
    ++s_test_count;
    a.add("member_double",(double)6);
    ++s_test_count;
    a.add("member_string",string("7"));
    ++s_test_count;
    if ( level >= 0 )   {
      Array arr;
      fill_array(arr, level-1);
      a.add("member_array",arr);
    }
  }
  MethodResponse create_nested_array(bool prt=true)  {
    Array array, sub_array_1, sub_array_2, sub_array_3, ss_array_1;

    fill_array(sub_array_1,3);
    fill_array(sub_array_2,3);
    fill_array(sub_array_3,3);
    fill_array(ss_array_1,3);
    sub_array_1.add(ss_array_1);
    array.add(sub_array_1);
    array.add(sub_array_2);
    array.add(sub_array_3);
    MethodResponse doc = MethodResponse::make(array);
    if ( prt )  {
      logger("MethodResponse<create_nested_array>: ") << doc.str() << endl;
    }
    return doc;
  }

  MethodResponse create_nested_struct(bool prt=true)  {
    Structure structure, sub_structure_1, sub_structure_2, sub_structure_3, ss_structure_1;
    fill_structure(sub_structure_1,3);
    fill_structure(sub_structure_2,3);
    fill_structure(sub_structure_3,3);
    fill_structure(ss_structure_1,3);
    sub_structure_1.add("subsub_structure_1",ss_structure_1);
    structure.add("sub_structure_1",sub_structure_1);
    structure.add("sub_structure_2",sub_structure_2);
    structure.add("sub_structure_3",sub_structure_3);
    MethodResponse doc = MethodResponse::make(structure);
    if ( prt )  {
      logger("MethodResponse<create_nested_struct>: ") << endl << doc.str() << endl;
    }
    return doc;
  }

  template <typename T> void test_decode_response(const string& tag, const T& val)  {
    MethodResponse doc = MethodResponse::make(val);
    string dsc = "MethodResponse<"+tag+"> (): ";
    logger(dsc) << endl << doc.str() << endl;
    decodeItem("response",doc.value());
  }

}

extern "C" int test_jsonrpc_encode_response(int /* argc */, char** /* argv */)   {
  test_encode_response("bool",       true);
  test_encode_response("int",        int(123456));
  test_encode_response("long",       long(123456));
  test_encode_response("double",     double(123456.789));
  test_encode_response("string",     string("Hello world!"));
  test_encode_response("Array",      Array({"a", "b", "c", "d"}));
  test_encode_response("Array",      Array({1, 2, 3, 4, 5, 6, 7}));
  test_encode_response("Structure",  Structure({{"a", 1}, {"b", 2}, {"c", 3}, {"d", 4}}));
  return 0;
}


extern "C" int test_jsonrpc_encode_array(int /* argc */, char** /* argv */)   {
  create_nested_array();
  return 0;
}

extern "C" int test_jsonrpc_encode_struct(int /* argc */, char** /* argv */)   {
  create_nested_struct();
  return 0;
}

extern "C" int test_jsonrpc_decode_response(int /* argc */, char** /* argv */)   {
  test_decode_response("bool",       true);
  test_decode_response("int",        int(123456));
  test_decode_response("long",       long(123456));
  test_decode_response("double",     double(123456.789));
  test_decode_response("string",     string("Hello world!"));
  test_decode_response("Array",      Array());
  test_decode_response("Structure",  Structure());
  return 0;
}

extern "C" int test_jsonrpc_decode_struct(int /* argc */, char** /* argv */)   {
  MethodResponse doc = create_nested_struct(false);
  decodeItem("value",doc.value());
  return 0;
}

extern "C" int test_jsonrpc_decode_array(int /* argc */, char** /* argv */)   {
  MethodResponse doc = create_nested_array(false);
  decodeItem("value",doc.value());
  return 0;
}

extern "C" int test_jsonrpc_decode(int argc, char** argv)   {
  cout << "test_jsonrpc_endode_array" << endl;
  test_jsonrpc_encode_array(argc, argv);

  cout << "test_jsonrpc_decode_array" << endl;
  test_jsonrpc_decode_array(argc, argv);

  cout << "test_jsonrpc_encode_struct" << endl;
  test_jsonrpc_encode_struct(argc, argv);

  cout << "test_jsonrpc_decode_struct" << endl;
  test_jsonrpc_decode_struct(argc, argv);

  cout << "test_jsonrpc_encode_response" << endl;
  test_jsonrpc_encode_response(argc, argv);

  cout << "test_jsonrpc_decode_response" << endl;
  test_jsonrpc_decode_response(argc, argv);
  return 0;
}
