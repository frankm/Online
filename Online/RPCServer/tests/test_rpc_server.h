//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================
#ifndef test_rpc_server_h
#define test_rpc_server_h

// Framework includes
#include "Objects.h"
#include <RPC/XMLRPC.h>

// C/C++ include files
#include <iostream>
#include <cstring>

/// Server invocation
template <typename T> int call_example_server(int, char**, T& server)   {
  test_rpc::Callable callable;
  std::cout << "++++++++++++ Testing standard XMLRPC call sequence." << std::endl;
  server.define("call_exception_method",      xmlrpc::Call(&callable).make(&test_rpc::Callable::call_exception_method));
  server.define("call_void",                  xmlrpc::Call(&callable).make(&test_rpc::Callable::call_void));
  server.define("call_void_const",            xmlrpc::Call(&callable).make(&test_rpc::Callable::call_void_const));
  //server.define("call_void_int",            xmlrpc::Call(&callable).make(&test_rpc::Callable::call_void_int));

  // This allows to export polymorphic functions:
  void (test_rpc::Callable::*c0)(int) =      &test_rpc::Callable::call_void_int;
  void (test_rpc::Callable::*c1)(int,int) =  &test_rpc::Callable::call_void_int;
  server.define("call_void_int",              xmlrpc::Call(&callable).make(c0));
  server.define("call_void_int1",             xmlrpc::Call(&callable).make(c0));
  server.define("call_void_int2",             xmlrpc::Call(&callable).make(c1));

  server.define("call_int_int_const",         xmlrpc::Call(&callable).make(&test_rpc::Callable::call_int_int_const));
  server.define("call_void_string",           xmlrpc::Call(&callable).make(&test_rpc::Callable::call_void_string));
  server.define("call_int_int_double_string", xmlrpc::Call(&callable).make(&test_rpc::Callable::call_int_int_double_string));
  server.define("call_Object_Object",         xmlrpc::Call(&callable).make(&test_rpc::Callable::call_Object_Object));
  server.define("call_Object_set",            xmlrpc::Call(&callable).make(&test_rpc::Callable::call_Object_set));
  server.define("call_Object_list",           xmlrpc::Call(&callable).make(&test_rpc::Callable::call_Object_list));
  server.define("call_Object_vector",         xmlrpc::Call(&callable).make(&test_rpc::Callable::call_Object_vector));
  server.define("call_exit",                  xmlrpc::Call(&callable).make(&test_rpc::Callable::call_exit));
  server.start();
  return 0;
}

/// Server invocation
template <typename T> int call_property_server(int, char**, T& server)   {
  test_rpc::PropertyContainer callable;
  std::cout << "++++++++++++ Testing property access and modifications." << std::endl;
  for(int i=0; i<100; ++i)  {
    char cnam[64], pnam[64], val[256];
    ::snprintf(cnam,sizeof(cnam),"Client_%d",i);
    for(int j=0; j<20; ++j)  {
      ::snprintf(pnam,sizeof(pnam),"Property_%d",j);
      ::snprintf(val,sizeof(val),"property_%s_%s_value_%d",cnam,pnam,j);      
      rpc::ObjectProperty* p = new rpc::ObjectProperty();
      p->client = cnam;
      p->name   = pnam;
      p->value  = val;
      callable.prop.push_back(p);
    }
  }
  server.define("clients",            xmlrpc::Call(&callable).make(&test_rpc::PropertyContainer::clients));
  server.define("allProperties",      xmlrpc::Call(&callable).make(&test_rpc::PropertyContainer::allProperties));
  server.define("namedProperties",    xmlrpc::Call(&callable).make(&test_rpc::PropertyContainer::namedProperties));
  server.define("clientProperties",   xmlrpc::Call(&callable).make(&test_rpc::PropertyContainer::clientProperties));
  server.define("property",           xmlrpc::Call(&callable).make(&test_rpc::PropertyContainer::property));
  server.define("setPropertyObject",  xmlrpc::Call(&callable).make(&test_rpc::PropertyContainer::setPropertyObject));
  server.define("setProperty",        xmlrpc::Call(&callable).make(&test_rpc::PropertyContainer::setProperty));
  server.define("forceExit",          xmlrpc::Call(&callable).make(&test_rpc::PropertyContainer::call_exit));
  server.start();
  return 0;
}

template <typename T> int call_server(int argc, char** argv, T& server)   {
  std::string test;
  for(int i = 0; i < argc && argv[i]; ++i)  {
    if ( 0 == ::strncmp("-test",argv[i],4) )  {
      test = argv[++i];
      break;
    }
  }
  if ( test == "properties" )
    return call_property_server(argc, argv, server);
  else if ( test == "example" )
    return call_example_server(argc, argv, server);
  return call_example_server(argc, argv, server);
}

#endif // test_rpc_server_h
