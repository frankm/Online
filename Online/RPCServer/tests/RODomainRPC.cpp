//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//  ROMon
//-------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//=========================================================================
#ifndef ROMON_ROMONBRIDGE_H
#define ROMON_ROMONBRIDGE_H 1

// Framework include files
#include <RTL/strdef.h>
#include <CPP/Interactor.h>
#include <ROMon/FMCMonListener.h>
#include <RPC/JSONRPC.h>

// C++ include files
#include <vector>
#include <memory>
#include <thread>
#include <mutex>
#include <set>

/*
 *   ROMon namespace declaration
 */
namespace ROMon {
  class RODomainRPC;

  template <typename T> struct DimContext  {
    T            obj;
    int          id  = 0;
  };

  class RpcListener : public FMCMonListener, public ROUpdateHandler {
  public:
    std::mutex                                   m_mtx;
    DimContext<std::string>                      m_nodes_wincc;
    DimContext<std::string>                      m_tasks_wincc;
    std::set<std::string>                        m_nodes;
    std::map<std::string,std::set<std::string> > m_tasks;
    std::string                                  m_prefix;
    RODomainRPC*                                 m_parent;

  public:
    /// Standard constructor with initialization
    RpcListener(RODomainRPC* par, bool verbose, const std::string& prefix, const std::string& name);
    /// Default destructor
    virtual ~RpcListener();
    std::mutex& lock() { return m_mtx; }
    const std::set<std::string>& nodes() const { return m_nodes; }
    const std::map<std::string,std::set<std::string> >& tasks() const { return m_tasks; }

    /// Add service to be bridged
    void addClient(const std::string& svc);
    /// Update handler
    void update(void* param) override;
    /// Feed data to DIS when updating data
    static void feed(void* tag, void** buf, int* size, int* first);
  };

  /**@class RODomainRPC RODomainRPC.h GaudiOnline/RODomainRPC.h
   *
   * Readout monitor DIM server for a single node
   *
   * @author M.Frank
   */
  class RODomainRPC : public CPP::Interactor {
  protected:
    /// Cluster container type definition
    typedef std::map<std::string,RpcListener*> Servers;
    /// Cluster container
    Servers                 m_servers;
    /// Process name
    std::string             m_name;
    /// Prefix for resulting service names
    std::string             m_prefix;
    /// Printout level
    long                    m_print;
    std::unique_ptr<std::thread> m_httpThread;
    std::mutex              m_mtx;
    std::set<std::string>   m_domains;
    DimContext<std::string> m_domains_wincc;
    int                     m_status_id = 0;
    struct {
      long numRequest = 0;
      long numSuccess = 0;
      long numError   = 0;
    } m_counters;
    struct ClusterAdder {
      RODomainRPC* bridge;
      ClusterAdder(RODomainRPC* b) : bridge(b) {}
      void operator()(const std::string& n) const { bridge->addCluster(RTL::str_upper(n)); }
    };
  public:
    /// Standard constructor with initialization
    RODomainRPC(int argc, char** argv);
    /// Default destructor
    virtual ~RODomainRPC();
    /// Access the name
    const std::string& name() const  {  return m_name; }
    /// Add cluster data points to bridge
    void addCluster(const std::string& name);
    /// Help printout in case of -h /? or wrong arguments
    static void help();
    /// Interactor override ....
    virtual void handle(const CPP::Event& ev) override;
    /// Access all known DNS domains
    std::set<std::string> domains();
    nlohmann::json domains2();
    /// Access all nodes known to a DNS domain
    std::set<std::string> nodes(const std::string& dns);
    /// Access all nodes known to a DNS domain matching the regular expression
    std::set<std::string> nodesByRegex(const std::string& dns, const std::string& regex);
    /// Access all tasks known to a DNS domain matching the correct node
    std::set<std::string> tasks(const std::string& dns, const std::string& node);
    /// Access all tasks known to a DNS domain matching the regular expression
    std::set<std::string> tasksByRegex(const std::string& dns, const std::string& regex);
  };
}      // End namespace ROMon
#endif /* ROMON_ROMONBRIDGE_H */

//====================================================================
//  ROMon
//--------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//====================================================================

// C++ include files
#include <iostream>
#include <stdexcept>

// Framework includes
#include <dim/dic.h>
#include <dim/dis.h>
#include <RTL/rtl.h>
#include <RTL/Logger.h>
#include <CPP/Event.h>
#include <CPP/IocSensor.h>
//#include "ROMon/RODomainRPC.h"
#include <ROMon/Constants.h>
#include <ROMon/PartitionListener.h>
#include <MBM/bmstruct.h>
#include <ROMon/ROMon.h>
#include <RPC/HttpRpcHandler.h>
#include <RPC/HttpXmlRpcHandler.h>
#include <RPC/HttpJsonRpcHandler.h>
#include <RPC/DimServer.h>

#include <algorithm>

using namespace ROMon;
using namespace std;
typedef FMCMonListener::Descriptor DSC;
typedef RODimListener::Clients     Clients;

namespace {
  struct StringCheck   {
    const string& match;
    StringCheck(const string& m) : match(m) {}
  };
  struct RegexCheck : public StringCheck {
    RegexCheck(const string& m) : StringCheck(m)  {}
    bool operator()(const std::string& n) const  { return n==match; }
  };
  struct NameCheck : public StringCheck {
    NameCheck(const string& m) : StringCheck(m)  {}
    bool operator()(const std::string& n) const  { return n==match; }
  };

  void feedString(void* tag, void** buff, int* size, int* ) {
    static const char* data = "";
    DimContext<string>* it = *(DimContext<string>**)tag;
    if ( it ) {
      *buff = (void*)(it->obj.c_str());
      *size = it->obj.length()+1;
      return;
    }
    *buff = (void*)data;
    *size = 0;
  }
  template <typename T, typename C> T* setupHandler(C& rpc_call, T* server)   {
    server->define("domains",      rpc_call.make(&RODomainRPC::domains));
    server->define("nodes",        rpc_call.make(&RODomainRPC::nodes));
    server->define("nodesByRegex", rpc_call.make(&RODomainRPC::nodesByRegex));
    server->define("tasks",        rpc_call.make(&RODomainRPC::tasks));
    server->define("tasksByRegex", rpc_call.make(&RODomainRPC::tasksByRegex));
    return server;
  }
}

/// Standard constructor with initialization
RpcListener::RpcListener(RODomainRPC* p, bool verbose, const string& prefix, const string& sf)
  : FMCMonListener(verbose), m_prefix(prefix), m_parent(p)
{
  string nam;
  m_infoTMO = 0;
  setItem("");
  setMatch("*");
  setUpdateHandler(this);
  nam = m_parent->name()+"/WinCCOA/" + sf + "/Nodes";
  m_nodes_wincc.id  = ::dis_add_service(nam.c_str(),"C",0,0,feedString,(long)&m_nodes_wincc);

  nam = m_parent->name()+"/WinCCOA/" + sf + "/Tasks";
  m_tasks_wincc.id  = ::dis_add_service(nam.c_str(),"C",0,0,feedString,(long)&m_tasks_wincc);

  nam = prefix + "/" + sf + "/ROpublish";
  addClient(nam);
  ::lib_rtl_output(LIB_RTL_DEBUG,"[RODomainRPC] Added services for subfarm:%s",sf.c_str());
}

/// Default destructor
RpcListener::~RpcListener()  {
}

/// Add service to be bridged
void RpcListener::addClient(const string& svc) {
  addHandler(svc,svc);
  Clients::iterator i=m_clients.find(svc);
  if ( i != m_clients.end() )  {
    ::lib_rtl_output(LIB_RTL_VERBOSE,"[RODomainRPC] Added service:%s",svc.c_str());
  }
}

/// Update handler
void RpcListener::update(void* param) {
  Item* it = (Item*)param;
  Descriptor* d = it->data<Descriptor>();
  if ( d && d->data ) {
    const Nodeset& ns = *(Nodeset*)d->data; 
    map<string,set<string> > tasks;
    string      nodes_wincc;
    string      tasks_wincc;
    set<string> nodes;

    nodes.insert(ns.name);
    for(Nodeset::Nodes::const_iterator i=ns.nodes.begin(); i!=ns.nodes.end(); i=ns.nodes.next(i)) {
      const Node& n = (*i);
      const Node::Tasks* procs = n.tasks();
      nodes.insert(n.name);
      if ( !nodes_wincc.empty() ) nodes_wincc += "|";
      nodes_wincc += n.name;
      string node_up = RTL::str_upper(n.name)+"_";
      string node_lo = RTL::str_lower(n.name)+"_";
      set<string>& tsk = tasks[n.name];
      for(Node::Tasks::const_iterator ip=procs->begin(); ip!=procs->end(); ip=procs->next(ip))   {
	const FSMTask& t = (*ip);
	string tname = t.name;
	if ( tname.find(node_up) != string::npos || tname.find(node_lo) != string::npos )  {
	  tsk.insert(tname);
	}
	if ( !tasks_wincc.empty() ) tasks_wincc += "|";
	tasks_wincc += t.name;
      }
    }
    ::lib_rtl_output(LIB_RTL_VERBOSE,"[RODomainRPC] Nodes[%s]: %s",ns.name, nodes_wincc.c_str());
    ::lib_rtl_output(LIB_RTL_VERBOSE,"[RODomainRPC] Tasks[%s]: %s",ns.name, tasks_wincc.c_str());
    lock_guard<mutex> lck(m_mtx);
    m_nodes_wincc.obj = nodes_wincc;
    m_tasks_wincc.obj = tasks_wincc;
    m_nodes = nodes;
    m_tasks = tasks;
  }
}

/// Standard constructor
RODomainRPC::RODomainRPC(int argc, char** argv) : m_print(LIB_RTL_WARNING)  {
  string PUBLISHING_NODE = "ECS03", from=PUBLISHING_NODE, to=PUBLISHING_NODE;
  string addr = "0.0.0.0", type="http";
  int    port = 2600, threads = 0, compress = 1;
  RTL::CLI cli(argc, argv, RODomainRPC::help);
  m_name = "/"+RTL::processName();
  cli.getopt("prefix", 3, m_prefix="");
  cli.getopt("print",  3, m_print);
  cli.getopt("from",   3, from);
  cli.getopt("to",     2, to);
  cli.getopt("utgid",  3, m_name);
  cli.getopt("type",   3, type);
  cli.getopt("port",   3, port);
  cli.getopt("address",3, addr);
  cli.getopt("threads",3, threads);
  cli.getopt("compress",3, compress);
  bool debug = cli.getopt("debug",2) != 0;
  string dbg_opt = debug ? "[Debugging ON]" : "[Debugging OFF]";
  if ( m_name[0] != '/' ) m_name = '/'+m_name;
  RTL::Logger::install_log(RTL::Logger::log_args(m_print));
  RTL::Logger::print_startup(" -- Domain RPC service of type: %s [%s]",type.c_str(),dbg_opt.c_str());
  ::dic_set_dns_node(from.c_str());
  ::dis_set_dns_node(to.c_str());
  m_status_id  = ::dis_add_service((m_name+"/Counters").c_str(),"L",&m_counters,sizeof(m_counters),0,0);
  {
    lock_guard<mutex> lck(m_mtx);
    xmlrpc::Call  xmlrpc_call(this);
    jsonrpc::Call jsonrpc_call(this);
    string name = m_name+"/WinCCOA/Domains";
    m_domains_wincc.id  = ::dis_add_service(name.c_str(),"C",0,0,feedString,(long)&m_domains_wincc);
    if ( type.find("http") != string::npos )   {
      auto json_handler = std::make_shared<rpc::RpcHandlerImp<rpc::HttpJsonRpcHandler> >();
      (*json_handler)->compression.enable = (compress != 0);
      (*json_handler)->debug              = debug;
      (*json_handler)->define("domains2", jsonrpc_call.make(&RODomainRPC::domains2));

      auto xml_handler  = std::make_shared<rpc::RpcHandlerImp<rpc::HttpXmlRpcHandler> >();
      (*xml_handler)->compression.enable = (compress != 0);
      (*xml_handler)->debug              = debug;
      setupHandler(jsonrpc_call, json_handler->get());
      setupHandler(xmlrpc_call,  xml_handler->get());

      auto handler = std::make_unique<rpc::HttpServer::Handler>();
      handler->mountPoints.handlers.emplace("/JSONRPC", json_handler);
      handler->mountPoints.handlers.emplace("/XMLRPC",  xml_handler);
      handler->mountPoints.handlers.emplace("/RPC2",    xml_handler);
      rpc::HttpServer* server
	= new rpc::HttpServer(move(handler), addr, port, rpc::HttpServer::SERVER);
      server->setDebug(debug);
      server->start(true);
      //setupServer(call, server)->setDebug(debug);
      m_httpThread.reset(new thread([server,threads] {
	    ::lib_rtl_output(LIB_RTL_INFO,
			     "[RODomainRPC] Running HTTP xmlrpc service with %d additional threads.",threads);
	    server->run(threads); 
	  }));
    }
    if ( type.find("dim") != string::npos )   {
      auto* server = setupHandler(xmlrpc_call, new rpc::DimServer(m_name, "/RPC2", to));
      server->setDebug(debug);
      server->start(true);
      server = setupHandler(xmlrpc_call,  new rpc::DimServer(m_name, "/XMLRPC", to));
      server->setDebug(debug);
      server->start(true);
      server = setupHandler(jsonrpc_call, new rpc::DimServer(m_name, "/JSONRPC", to));
      server->setDebug(debug);
      server->start(true);
    }
  }
  PartitionListener p(this,"Subfarms","*",true);
  string svc = "/"+RTL::str_upper(RTL::nodeNameShort());
  if ( m_name[0] != '/' ) svc += '/';
  svc += m_name;
  ::dis_start_serving(svc.c_str());
}

/// Default destructor
RODomainRPC::~RODomainRPC() {
  m_httpThread.reset();
}

/// Add cluster data points to bridge
void RODomainRPC::addCluster(const string& sf) {
  lock_guard<mutex> lck(m_mtx);
  if ( m_domains.find(sf) == m_domains.end() )   {
    m_servers.insert(make_pair(sf,new RpcListener(this, m_print<LIB_RTL_INFO, m_prefix, sf)));
    if ( !m_domains_wincc.obj.empty() ) m_domains_wincc.obj += "|";
    m_domains_wincc.obj += sf;
    m_domains.insert(sf);
    ::dis_update_service(m_domains_wincc.id);
  }
  ::lib_rtl_output(LIB_RTL_DEBUG,"[RODomainRPC] Added subfarm:%s",sf.c_str());
}

/// Interactor override ....
void RODomainRPC::handle(const CPP::Event& ev) {
  typedef vector<string> StringV;
  switch(ev.eventtype) {
  case TimeEvent:
    //if (ev.timer_data == ??? ) {}
    return;
  case IocEvent:
    switch(ev.type) {
    case CMD_CONNECT: {
      std::unique_ptr<StringV> farms{(StringV*)ev.data};
      for_each(farms->begin()+1,farms->end(),ClusterAdder(this));
      ::dis_start_serving(m_name.c_str());
      return;
    }
    default:
      break;
    }
    break;
  default:
    break;
  }
}

/// Access all known DNS domains
set<string> RODomainRPC::domains()   {
  lock_guard<mutex> lck(m_mtx);
  ::lib_rtl_output(LIB_RTL_INFO,"[RODomainRPC] domains: %ld entries",m_domains.size());
  ++m_counters.numRequest;
  ++m_counters.numSuccess;
  return m_domains;
}

/// Access all known DNS domains
nlohmann::json RODomainRPC::domains2()   {
  lock_guard<mutex> lck(m_mtx);
  ::lib_rtl_output(LIB_RTL_INFO,"[RODomainRPC] domains: %ld entries",m_domains.size());
  ++m_counters.numRequest;
  ++m_counters.numSuccess;
  return m_domains;
}

/// Access all nodes known to a DNS domain
set<string> RODomainRPC::nodes(const string& dns)  {
  set<string> res;
  lock_guard<mutex> lck1(m_mtx);
  Servers::iterator i = m_servers.find(dns);
  ++m_counters.numRequest;
  if ( i != m_servers.end() )  {
    RpcListener* l = (*i).second;
    lock_guard<mutex> lck2(l->lock());
    ::lib_rtl_output(LIB_RTL_DEBUG,"[RODomainRPC] domains: %ld entries",l->nodes().size());
    ++m_counters.numSuccess;
    return l->nodes();
  }
  ++m_counters.numError;
  ::lib_rtl_output(LIB_RTL_ERROR,"[RODomainRPC] domains: Unknown server: %s",dns.c_str());
  return res;
}

/// Access all nodes known to a DNS domain matching the regular expression
set<string> RODomainRPC::nodesByRegex(const string& dns, const string& regex) {
  set<string> res;
  lock_guard<mutex> lck1(m_mtx);
  Servers::iterator i = m_servers.find(dns);
  ++m_counters.numRequest;
  if ( i != m_servers.end() )  {
    RpcListener* l = (*i).second;
    lock_guard<mutex> lck2(l->lock());
    RegexCheck match(regex);
    for(const auto& n : l->nodes() )
      if ( match(n) ) res.insert(n);
  }
  (i==m_servers.end()) ? ++m_counters.numError : ++m_counters.numSuccess;
  ::lib_rtl_output(i==m_servers.end() ? LIB_RTL_ERROR : LIB_RTL_DEBUG,
		   "[RODomainRPC] nodesByRegex: %ld entries [Server %s]",
		   res.size(), i==m_servers.end() ? "NOT FOUND" : "found");
  return res;
}

/// Access all tasks known to a DNS domain matching the correct node
set<string> RODomainRPC::tasks(const string& dns, const string& node)  {
  set<string> res;
  lock_guard<mutex> lock1(m_mtx);
  Servers::iterator i = m_servers.find(dns);
  ++m_counters.numRequest;
  if ( i != m_servers.end() )  {
    RpcListener* l = (*i).second;
    lock_guard<mutex> lock2(l->lock());
    const auto ic = l->tasks().find(node);
    if ( ic != l->tasks().end() )
      res = (*ic).second;
  }
  (i==m_servers.end()) ? ++m_counters.numError : ++m_counters.numSuccess;
  ::lib_rtl_output(i==m_servers.end() ? LIB_RTL_ERROR : LIB_RTL_DEBUG,
		   "[RODomainRPC] tasks(by node): %ld entries [Server %s]",
		   res.size(), i==m_servers.end() ? "UNKNOWN" : "found");
  return res;
}

/// Access all tasks known to a DNS domain matching the regular expression
set<string> RODomainRPC::tasksByRegex(const string& dns, const string& regex) {
  set<string> res;
  RegexCheck match(regex);
  lock_guard<mutex> lock1(m_mtx);
  Servers::iterator i = m_servers.find(dns);
  ++m_counters.numRequest;
  if ( i != m_servers.end() )  {
    RpcListener* l = (*i).second;
    lock_guard<mutex> lock2(l->lock());
    for(const auto& n : l->tasks() )   {
      for(const auto& t : n.second )  {
	if ( match(t) ) res.insert(t);
      }
    }
  }
  (i==m_servers.end()) ? ++m_counters.numError : ++m_counters.numSuccess;
  ::lib_rtl_output(i==m_servers.end() ? LIB_RTL_ERROR : LIB_RTL_DEBUG,
		   "[RODomainRPC] tasks(by regex): %ld entries [Server %s]",
		   res.size(), i==m_servers.end() ? "UNKNOWN" : "found");
  return res;
}

/// Help printout in case of -h /? or wrong arguments
void RODomainRPC::help() {
  ::lib_rtl_output(LIB_RTL_ALWAYS,"romon_domainrpc -opt [-opt]\n"
                   "     -from=<string>     Node name from which the datapoints should be consumed.\n"
                   "     -to=<string>       Node to which these data points should be published.\n"
                   "     -print=<integer>   Printout value and verbosity.\n"
                   "     -prefix=<string>   Prefix for services to connect.\n\n");
}

/// Main entry point to start the application
extern "C" int romon_domainrpc(int argc, char** argv) {
  RODomainRPC mon(argc,argv);
  IocSensor::instance().run();
  return 1;
}

