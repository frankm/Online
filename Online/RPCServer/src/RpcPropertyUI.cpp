//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <RPC/RpcPropertyUI.h>
#include <stdexcept>
#include <RPC/XMLRPC.h>
#include <RPC/JSONRPC.h>
#include <RPC/DimServer.h>
#include <RPC/HttpRpcServer.h>
#include <RPC/HttpXmlRpcHandler.h>
#include <RPC/HttpJsonRpcHandler.h>

using namespace std;

namespace rpc   {

  unique_ptr<DimServer> create_srv(unique_ptr<DimServer::Handler>&& handler, const vector<string>& args)   {
    return make_unique<DimServer>(move(handler), args[0], args[1]);
  }

  unique_ptr<HttpServer> create_srv(unique_ptr<HttpServer::Handler>&& handler, const vector<string>& args)   {
    return ( args.size() > 3 )
      ? make_unique<HttpServer>(move(handler), args[0], args[1], args[2], args[3])
      : make_unique<HttpServer>(move(handler), args[0], args[1], args[2]);
  }

  template <typename SERVER, typename HANDLER, typename CALL> inline
  unique_ptr<RpcPropertyUI<SERVER,CALL> > create_rpc_property_ui(UI* ui, unique_ptr<HANDLER>&& handler, const vector<string>& args)    {
    auto rpc_ui    = make_unique<RpcPropertyUI<SERVER,CALL> >();
    rpc_ui->call   = make_unique<CALL>(ui);
    rpc_ui->server = create_srv(move(handler), args);
    rpc_ui->start_server();
    return rpc_ui;
  }
}

extern "C" rpc::RpcUI* create_rpc_ui(const string& type, rpc::UI* ui, const vector<string>& args)    {
  using namespace rpc;
  if ( ::strcasecmp(type.c_str(),"dimxmlrpc") == 0 )  {
    auto handler = make_unique<DimServer::Handler>(args.size() > 2 ? args[2] : "/RPC2");
    return create_rpc_property_ui<DimServer,DimServer::Handler,xmlrpc::Call>(ui, move(handler), args).release();
  }
  if ( ::strcasecmp(type.c_str(),"dimjsonrpc") == 0 )   {
    auto handler = make_unique<DimServer::Handler>(args.size() > 2 ? args[2] : "/JSONRPC");
    return create_rpc_property_ui<DimServer,DimServer::Handler,jsonrpc::Call>(ui, move(handler), args).release();
  }
  if ( ::strcasecmp(type.c_str(),"httpxmlrpc") == 0 )
    return create_rpc_property_ui<HttpServer,HttpXmlRpcHandler,xmlrpc::Call>(ui, make_unique<HttpXmlRpcHandler>(), args).release();
  if ( ::strcasecmp(type.c_str(),"httpjsonrpc") == 0 )
    return create_rpc_property_ui<HttpServer,HttpJsonRpcHandler,jsonrpc::Call>(ui, make_unique<HttpJsonRpcHandler>(), args).release();
  return nullptr;
}
