//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#include "HTTP/HttpClient.h"
#include "RPC/HttpRpcHandler.h"

#include <sstream>

namespace rpc {

  class HttpHttpBridgeHandler : public HttpRpcHandler   {
  public:
    /// Debug flag
    int debug = 0;

  public:
    /// Client object to forward processing request
    http::HttpClient client;

    /// Standard constructor
    HttpHttpBridgeHandler(const std::string& dns, 
			  const std::string& srv, 
			  const std::string& uri, 
			  const std::string& port,
			  int dbg = 0);
    /// Standard destructor
    virtual ~HttpHttpBridgeHandler() = default;
    /// Enable debugging
    virtual void setDebug(int val)  override  { this->debug = val; }
    /// Execute RPC call
    virtual continue_action handle_request(const http::Request& req, http::Reply& rep)  override;
  };
}

/// Standard constructor
inline rpc::HttpHttpBridgeHandler::HttpHttpBridgeHandler(const std::string& dns_name,
							 const std::string& server_name,
							 const std::string& uri_name,
							 const std::string& srv_port,
							 int dbg)
	   : debug(dbg), client(dns_name, srv_port)
{
  this->uri = uri_name;
  client.userRequestHeaders.push_back(http::HttpClient::Header("Dim-Server",server_name));
  client.setDebug(this->debug);
  client.mount = this->uri;
}

/// Execute RPC call
inline rpc::HttpHttpBridgeHandler::continue_action
rpc::HttpHttpBridgeHandler::handle_request(const http::Request& req, http::Reply& rep)
{
  using namespace chrono;
  std::stringstream str;
  try  {
    std::string acceptedEncoding, dataEncoding;
    auto* hdr = req.header("Accepted-Encoding");

    if ( hdr )   {
      acceptedEncoding = hdr->value;
    }
    if ( debug > 3 )   {
      char*  rp = (char*)req.content.data();
      std::string rq(rp, rp+req.content.size());
      printout(INFO,"RPCClient","HttpHttpBridgeHandler: Handling request(%s:%s): %s",
	       client.host.c_str(), client.port.c_str(), rq.c_str());
    }
    else if ( debug > 1 )  {
      printout(INFO,"RPCClient","HttpHttpBridgeHandler: Handling request(%s:%s)",
	       client.host.c_str(), client.port.c_str());
    }
    rep.content = client.request(req.content.data(), req.content.size(), acceptedEncoding, dataEncoding);
    copy(client.userReplyHeaders.begin(),client.userReplyHeaders.end(),back_inserter(rep.userHeaders));
    if ( !dataEncoding.empty() )  {
      rep.userHeaders.emplace_back(http::HttpHeader("Content-Encoding", dataEncoding));
    }
    return continue_action::write;
  }
  catch(const std::exception& e)   {
    str << e.what();
  }
  catch( ... )   {
    std::error_code errcode(errno, std::system_category());
    str << "RPC fault [" << errcode.value() << "] (UNKOWN Exception): " << errcode.message();
  }
  throw std::runtime_error(str.str());
}
