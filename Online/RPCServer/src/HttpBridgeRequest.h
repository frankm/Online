//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// This is the bridge implementation to HTTP
#include "HttpHttpRpcBridge.h"

// This is the bridge implementation to DIM
#include "HttpDimRpcBridge.h"

/// Handle a request and produce a reply.
void rpc::HANDLER::handle_bridge_request(const std::string& dns,
					 const std::string& server,
					 const http::Request& req,
					 http::Reply& rep)
{
  if ( !server.empty() )   {
    HttpRpcHandler* h = 0;
    std::string srv_id = server + req.uri;
    if ( dns.empty() )   { {
	/// Handle http-dim request. Locked access due to multithreading.
	Lock lck(lock);
	auto i = dimBridge.handlers.find(srv_id);
	if ( i == dimBridge.handlers.end() )  {
	  auto ptr = std::make_unique<rpc::HttpDimBridgeHandler>(server, req.uri, debug);
	  h = ptr.get();
	  dimBridge.handlers.emplace(srv_id, std::move(ptr));
	}
	else  {
	  h = (*i).second.get();
	}
      }
      h->setDebug(debug);
      h->handle_request(req, rep);
      return;
    }
    /// Handle http-http request. Locked access due to multithreading.
    { std::string port = "2600";
      for(const auto& hdr : req.headers)  {
	if ( hdr.name == "RPC-Port" )  {
	  port = hdr.value;
	  break;
	}
      }
      Lock lck(lock);
      auto i = httpBridge.handlers.find(srv_id);
      if ( i == httpBridge.handlers.end() )  {
	auto ptr = std::make_unique<rpc::HttpHttpBridgeHandler>(dns, server, req.uri, port, debug);
	h = ptr.get();
	httpBridge.handlers.emplace(srv_id, std::move(ptr));
      }
      else  {
	h = (*i).second.get();
      }
    }
    if ( debug > 1 )   {
      printout(ALWAYS,"HttpHttpBridge",
	       "Sending request: %s **to** %s", &req.content[0], dns.c_str());
    }
    h->setDebug(debug);
    h->handle_request(req, rep);
    return;
  }
  throw std::runtime_error("XML-RPC: Invalid request. Missing \"Dim-Server\" http header.");
}
