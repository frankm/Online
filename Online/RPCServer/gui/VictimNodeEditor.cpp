//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "RPC/GUI/VictimNodeEditor.h"
#include "RPC/GUI/GuiException.h"
#include "RPC/GUI/GuiCommand.h"
#include "RPC/GUI/GuiMsg.h"

#include "RPC/DataflowRPC.h"
#include "RPC/DimClient.h"
#include "HTTP/HttpClient.h"

#include "CPP/IocSensor.h"
#include "CPP/Event.h"
#include "RTL/rtl.h"

#include "Options/Analyzer.h"
#include "Options/Messages.h"
#include "Options/Catalog.h"
#include "Options/Units.h"
#include "Options/PragmaOptions.h"
#include "Options/Node.h"

/// ROOT include files
#include "TGLabel.h"
#include "TGButton.h"
#include "TGMsgBox.h"
#include "TGComboBox.h"
#include "TGTextEdit.h"
#include "TGTextEntry.h"
#include "TApplication.h"
#include "TGTableLayout.h"
#include "TGPicture.h"

#include <fstream>

ClassImp(xmlrpc::VictimNodeEditor)

#define ICON_SIZE  UInt_t(20),UInt_t(20)

using namespace std;
using namespace xmlrpc;

/// Standard initializing constructor
VictimNodeEditor::VictimNodeEditor(TGWindow* parent, CPP::Interactor* gui_interactor)
  : OptionsEditor(parent, gui_interactor)
{
  color_inhibit = color_changed;
}

/// Default destructor
VictimNodeEditor::~VictimNodeEditor()   {
}

/// Initialize the display, setup the widgets
void VictimNodeEditor::init(int num_rows)   {
  TGPicturePool* p = gClient->GetPicturePool();
  this->OptionsEditor::init(num_rows);
  nav_reload->SetText("Reload from file");
  source.combo -> SetEnabled(kTRUE);
  source.combo -> SetEditable(kTRUE);
  source.combo -> SetEditDisabled(0);
  source.combo -> EnableTextInput(kTRUE);
  source.combo -> GetTextEntry()->SetText("");

  addOpt   = new TGTextButton   (editor, "&Add", -1);
  addOpt->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "click_addOption()");
  addOpt->SetToolTipText("Add new option from editor\nto the options panel.");

  addOptP  = new TGPictureButton(editor, p->GetPicture("bld_newtab.png", ICON_SIZE));
  addOptP->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "click_addOption()");
  addOptP->SetToolTipText("Add new option from editor\nto the options panel.");

  editor->AddFrame(addOpt,  new TGTableLayoutHints(5, 6, 2, 3, kLHintsRight|kLHintsTop|kLHintsExpandX, 1,1,1,1));
  editor->AddFrame(addOptP, new TGTableLayoutHints(6, 7, 2, 3, kLHintsLeft|kLHintsTop|kLHintsExpandX, 1,1,1,1));

  nav_send->SetText("&Send full record");
  edit.editor->SetReadOnly(kFALSE);
  edit.label->SetEnabled(kTRUE);
  edit.label->SetEditable(kTRUE);
  edit.label->SetEditDisabled(0);
  int default_height = edit.label->GetDefaultHeight();
  optsFile  = new TGTextEntry(editor, "...default options file...");
  editor->AddFrame(optsFile, new TGTableLayoutHints(0, 3, 4, 5, kLHintsLeft|kLHintsTop|kLHintsExpandX, 1,1,1,1));
  optsFile->Resize(320,default_height);
}

/// Reset the content of the options editor
void VictimNodeEditor::reset_options_editor(const string& cl, const string& opt_val)  {
  this->OptionsEditor::reset_options_editor(cl, opt_val);
  edit.editor->SetReadOnly(kFALSE);
}

/// Initialize single row of the options table
OptionsEditor::Row VictimNodeEditor::init_options_table_row(TGGroupFrame* grp, int i)   {
  TGPicturePool* p = gClient->GetPicturePool();
  char text[256];
  Row row;
  ::snprintf(text,sizeof(text), "# %d component option", i+1);
  row.id      = i;
  row.isValid = true;
  row.label   = new TGTextEntry(grp, text);
  row.label->SetToolTipText("Job option client\nForm:<client>.<option name>");
  row.label->SetBackgroundColor(color_enabled);
  row.label->SetEnabled(kFALSE);

  row.option  = new TGTextEntry(grp, "....");
  row.option->SetToolTipText("Job option value");
  row.option->SetBackgroundColor(color_enabled);
  row.option->SetEnabled(kFALSE);

  row.edit    = new TGTextButton(grp, "Edit", 1000+i);
  row.edit->Connect ("Clicked()", "xmlrpc::OptionsEditor", this, "click_editOption()");
  row.edit->SetToolTipText( "Click to load the option\ninto the editor panel");

  row.edit2   = new TGPictureButton(grp, p->GetPicture("bld_edit.png", ICON_SIZE), 2000+i);
  row.edit2->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "click_editOption()");
  row.edit2->SetToolTipText("Click to load the option\ninto the editor panel");

  row.send    = new TGTextButton(grp, "Disable", 3000+i);
  row.send->Connect ("Clicked()", "xmlrpc::VictimNodeEditor", this, "click_handleOption()");
  row.send->SetToolTipText( "Click to disable the option\nfrom being sent to the process");

  row.send2   = new TGPictureButton(grp, p->GetPicture("bld_stop.png", ICON_SIZE), 4000+i);
  row.send2->Connect("Clicked()", "xmlrpc::VictimNodeEditor", this, "click_handleOption()");
  row.send2->SetToolTipText("Click to disable the option\nfrom being sent to the process");

  grp->AddFrame(row.label,  new TGTableLayoutHints(0, 1, i, i+1, kLHintsLeft |kLHintsCenterY, 1,10,1,1)); 
  grp->AddFrame(row.option, new TGTableLayoutHints(1, 2, i, i+1, kLHintsLeft |kLHintsCenterY|kLHintsExpandX|kLHintsExpandY, 1,1,1,1)); 
  grp->AddFrame(row.edit,   new TGTableLayoutHints(2, 3, i, i+1, kLHintsRight|kLHintsCenterY, 10, 1, 1, 1)); 
  grp->AddFrame(row.edit2,  new TGTableLayoutHints(3, 4, i, i+1, kLHintsRight|kLHintsCenterY,  0, 1, 1, 1)); 
  grp->AddFrame(row.send,   new TGTableLayoutHints(4, 5, i, i+1, kLHintsRight|kLHintsCenterY, 10, 1, 1, 1));
  grp->AddFrame(row.send2,  new TGTableLayoutHints(5, 6, i, i+1, kLHintsRight|kLHintsCenterY,  0, 1, 1, 1));
  return row;
}

/// GUI handler: Handle option enable/disable
void VictimNodeEditor::click_handleOption()    {
  TGTextButton*  b = (TGTextButton*)gTQSender;
  Int_t idx = b->WidgetId()%1000;
  Row&   row = rows[idx];
  string nam = row.label->GetText();
  for(auto& p : properties)  {
    string pnam = p.client+"."+p.name;
    if ( pnam == nam )   {
      if ( (p.flag&OPTION_ENABLED) )   {
	p.flag &= ~OPTION_ENABLED;
	p.flag |=  OPTION_DISABLED;
      }
      else if ( (p.flag&OPTION_DISABLED) )   {
	p.flag |=  OPTION_ENABLED;
	p.flag &= ~OPTION_DISABLED;
      }
      show_options_table_row(row, p);
      return;
    }
  }
  GuiException(kMBOk,kMBIconStop,"RPC: Handle Option",
	       "Cannot locate property: %s = %s",
	       nam.c_str(), row.option->GetText()).send(gui);
}

/// Show single row in the options table
void VictimNodeEditor::show_options_table_row(const Row& row, const ObjectProperty& p)   {
  TGPicturePool* pool = gClient->GetPicturePool();
  this->OptionsEditor::show_options_table_row(row, p);
  if ( (p.flag&OPTION_ENABLED) )   {
    row.send->SetText("Disable");
    row.send2->SetPicture(pool->GetPicture("bld_stop.png", ICON_SIZE));
  }
  else if ( (p.flag&OPTION_DISABLED) )   { 
    row.send->SetText("Enable");
    row.send2->SetPicture(pool->GetPicture("bld_plus.png", ICON_SIZE));
  }
}

/// Save the not-disabled options to a file
void VictimNodeEditor::saveOptionsFile()   {
  save_options_file(OPTION_ENABLED, true);
}

/// Send options to victim processes
void VictimNodeEditor::sendChanges()   {
  Properties updates;
  for(auto& p : properties)  {
    if ( (p.flag&OPTION_ENABLED) )
      updates.push_back(p);
  }
  if ( updates.size() > 0 )   {
    process = source.combo -> GetTextEntry()->GetText();
    auto cl = rpc::client<http::HttpClient>(dns, 2600);
    (*cl)->userRequestHeaders.emplace_back("Dim-Server",process);
    client = std::move(cl);
    if ( client )  {
      int result = client.setProperties(updates);
      if ( updates.size() == size_t(result) )   {
	for(auto& p : properties)
	  p.flag &= ~OPTION_CHANGED;
	update_sent_options();
	GuiException(kMBOk,kMBIconAsterisk,"RPC victim: Send Changes",
		     "Successfully sent %ld options\nto %s\n"
		     "[ALL OK. Changes were sent]",
		     updates.size(),process.c_str()).send(gui);
	client.reset();
	return;
      }
      GuiException(kMBOk,kMBIconExclamation,"RPC victim: Send Changes",
		   "Send Changes: Sent %ld options to\n%s. Result=%d\n"
		   "[Some options were not accepted by client]",
		   updates.size(), process.c_str(), result).send(gui);
      client.reset();
      return;
    }
    GuiException(kMBOk,kMBIconStop,"RPC victim: Send Changes",
		 "No valid RPC client present [Internal Error].\n"
		 "Cannot send changes.").send(gui);
    return;
  }
  GuiException(kMBOk,kMBIconStop,"RPC victim: Send Changes",
	       "No updates pending. Nothing to be sent.").send(gui);

}

/// Create communication client
void VictimNodeEditor::createClient(GuiCommand* c)  {
  dns     = c->dns;
  node    = c->node;
  process = c->client;
  GuiMsg("VictimNodeEditor::load: %s -- %s -- %s", dns.c_str(), node.c_str(), c->opts.c_str()).send(gui);
  properties.clear();
  optsBatch = -1;
  source.dns  -> SetText(dns.c_str());
  source.node -> SetText(node.c_str());
  //source.proc -> SetText(process.c_str());
  source.combo -> RemoveAll();
  source.combo -> AddEntry(process.c_str(),0);
  source.combo -> Select(0, kFALSE);
  source.combo -> GetTextEntry()->SetText(process.c_str());
  optsFile     -> SetText(c->opts.c_str());
  CPP::IocSensor::instance().send(gui,XMLRPCGUI_SEND_TASKLIST,(Interactor*)this);
  CPP::IocSensor::instance().send(this,EDITOR_LOAD_OPTIONS);
}

/// Send single option to the target process
void VictimNodeEditor::sendOption(const Row& )  {
}

/// Load options of a single client
void VictimNodeEditor::loadOptions()   {
  namespace gp = Gaudi::Parsers;
  string file = optsFile->GetText();
  string search_path = ".";
  gp::Messages messages(2,::lib_rtl_output);
  gp::Catalog catalog;
  gp::Units units;
  gp::PragmaOptions pragma;
  gp::Node ast;
  properties.clear();
  GuiMsg("VictimNodeEditor Reading options from the file %s", file.c_str()).send(gui);
  int sc = gp::ReadOptions(file, search_path, &messages, &catalog, &units, &pragma, &ast);
  // --------------------------------------------------------------------------
  if ( sc )    {
    if (pragma.IsPrintOptions()) {
      cout << "Print options" << endl << catalog << endl;
    }
    if (pragma.IsPrintTree()) {
      cout << "Print tree:" << endl << ast.ToString() << endl;
    }
    messages.AddInfo("Job options successfully read in from "+file);
    for (const auto&  cl : catalog)   {
      const string& theclient = cl.first;
      for (const auto& c : cl.second )   {
	ObjectProperty p(theclient,c.NameInClient(),c.ValueAsString());
	p.flag = OPTION_ENABLED;
	properties.push_back(p);
      }
    }
    TGPicturePool* pool = gClient->GetPicturePool();
    for( auto& row : rows )   {  
      row.label->SetEnabled  (kFALSE);
      row.option->SetEnabled (kFALSE);
      row.edit->SetEnabled   (kTRUE);
      row.edit2->SetEnabled  (kTRUE);
      row.label->SetBackgroundColor(color_enabled);
      row.option->SetBackgroundColor(color_enabled);
      row.send->SetText("Disable");
      row.send2->SetPicture(pool->GetPicture("bld_stop.png", ICON_SIZE));
    }
    optsBatch = -1;
    show_next_table_batch();
    return;
  }
  else {
    messages.AddFatal("Job options error occurred.");
  }
}


/// Create communication client
void VictimNodeEditor::handle(const Event& ev)  {
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type)   {
    case XMLRPCGUI_HAVE_NODELIST:  {
      unique_ptr<set<string> > items(ev.iocPtr<set<string> >());
      return;
    }
    case XMLRPCGUI_HAVE_TASKLIST:  {
      int cnt = 0;
      unique_ptr<set<string> > items(ev.iocPtr<set<string> >());
      source.combo -> RemoveAll();
      for(const auto& i : *items )
	source.combo -> AddEntry(i.c_str(),cnt++);
      if ( cnt > 0 ) source.combo -> Select(0, kFALSE);
      return;
    }
    default:
      break;
    }
  }
  default:
    break;
  }
  OptionsEditor::handle(ev);
}

