//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "RPC/GUI/OutputWindow.h"
#include "RPC/GUI/GuiCommand.h"
#include "RPC/GUI/GuiMsg.h"
#include "CPP/Event.h"

// ROOT include files
#include "TApplication.h"

// C/C++ include files
#include <memory>

ClassImp(xmlrpc::OutputWindow)

using namespace std;
using namespace xmlrpc;
#define MAX_LINE_COUNT 300

/// Standard initializing constructor
OutputWindow::OutputWindow(TGWindow* p)
  : TGDockableFrame(p,-1,kVerticalFrame)
{
  Pixel_t background, foreground;
  TGLayoutHints* hints = new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,0,0,0,0);
  gClient->GetColorByName("#c0c0c0", background);
  gClient->GetColorByName("#000000", foreground);
  group = new TGGroupFrame(this, "Output Logger");
  view  = new TGTextView(group, 500, 94, 99, kFixedWidth | kFixedHeight);
  view->SetBackground(background);
  view->SetForegroundColor(foreground);
  group->AddFrame(view, hints);
  AddFrame(group, hints);
  EnableUndock(kTRUE);
  EnableHide(kFALSE);
  Connect("Undocked()", "xmlrpc::OutputWindow", this, "handleDocking()");
  //exit = new TGTextButton(this, "&Close", -1);
  //exit->Connect("Clicked()", "TApplication", gApplication, "Terminate()");
  //AddFrame(exit, new TGLayoutHints(kLHintsRight,3,3,3,3));
  Resize(GetDefaultSize().fWidth,200);
}


/// Default destructor
OutputWindow::~OutputWindow()   {
}

/// Interactor interrupt handler callback
void OutputWindow::handle(const CPP::Event& ev)    {
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type) {
    case XMLRPCGUI_SHOW_OUTPUTLINE:  {
      unique_ptr<string> c(ev.iocPtr<string>());
      if ( !c->empty() )   {
	TGText* text = view->GetText();
	view->AddLine(c->c_str());
	::printf("%ld: %s\n",text->RowCount(),c->c_str());
	if ( text->RowCount()%10 == 0 )  {
	  IocSensor::instance().send(this,XMLRPCGUI_SHOW_OUTPUTBOTTOM);
	}
      }
      return;
    }
    case XMLRPCGUI_SHOW_OUTPUTBOTTOM:
      view->ShowBottom();
      return;
    case XMLRPCGUI_UPDATE_OUTPUT:
      view->Update();
      return;
    default:
      break;
    }
    printf("OutputWindow::handle. Unhandled IOC event: %d\n",ev.type);
    return;
  }
  case TimeEvent: {
    long cmd = long(ev.timer_data);
    switch(cmd) {
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
  GuiMsg("OutputWindow::handle. Unhandled event: %d\n",ev.eventtype).send(this);
}

/// Handle docking and other signals
void OutputWindow::handleDocking()   {
  this->UndockContainer();
}
