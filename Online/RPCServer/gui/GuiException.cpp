//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "RPC/GUI/GuiException.h"
#include "RPC/GUI/GuiCommand.h"
#include "CPP/IocSensor.h"

/// C++ include files
#include <string>
#include <cstdarg>

using namespace xmlrpc;

/// Initializing constructor
GuiException::GuiException(int b, EMsgBoxIcon i, const char* t, const char* fmt, ...)
  : title(t), icon(i), buttons(b)
{
  char text[4096];
  va_list args;
  va_start(args, fmt);
  ::vsnprintf(text,sizeof(text),fmt,args);
  va_end(args);
  msg = text;
}

extern TGMainFrame* global_main_window;
#include "TGText.h"
/// Send message to display target
void GuiException::send(CPP::Interactor* target)  const  {
  if ( target )   {
    GuiException* c = new GuiException(buttons, icon, title.c_str(), msg.c_str(), '\0');
    IocSensor::instance().send(target,XMLRPCGUI_EXCEPTION,c);
#if 0
    TGWindow* f = dynamic_cast<TGWindow*>(target);
    //TGWindow* f = dynamic_cast<TGWindow*>(global_main_window);
    if ( f )  {
      Int_t retval = 0;
      const GuiException* e = this;
      new TGMsgBox(gClient->GetRoot(), f,
		   e->title.c_str(), e->msg.c_str(),
		   e->icon, e->buttons, &retval);
    }
    else  {
      ::printf("No Interactor as message target.\n");
    }
#endif
  }
}
