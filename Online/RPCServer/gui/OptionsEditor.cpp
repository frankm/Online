//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <RPC/GUI/OptionsEditor.h>
#include <RPC/GUI/GuiException.h>
#include <RPC/GUI/GuiMsg.h>
#include <HTTP/HttpClient.h>
#include <CPP/Event.h>
#include <RTL/rtl.h>

/// ROOT include files
#include <TSystem.h>
#include <TGLabel.h>
#include <TGMsgBox.h>
#include <TGString.h>
#include <TGButton.h>
#include <TGPicture.h>
#include <TGTextEntry.h>
#include <TGTextEdit.h>
#include <TGComboBox.h>
#include <TGFileDialog.h>

#include <TApplication.h>
#include <TGTableLayout.h>

#include <memory>
#include <cstring>
#include <fstream>
#include <iostream>
#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/stream.hpp>

ClassImp(xmlrpc::OptionsEditor)

using namespace std;
using namespace xmlrpc;

#define ICON_SIZE  UInt_t(20),UInt_t(20)

namespace {
  const char *Option_filetypes[] = { "All files",     "*",
				     "Options files", "*.opts",
				     "Text files",    "*.[tT][xX][tT]",
				     0,               0 };
}

/// Standard initializing constructor
OptionsEditor::OptionsEditor(TGWindow* parent, CPP::Interactor* g)
  : TGCompositeFrame(parent, 100, 100, kVerticalFrame), gui(g)
{
  gClient->GetColorByName("#c0c0c0", color_disabled);
  gClient->GetColorByName("white",   color_enabled);
  gClient->GetColorByName("#F05050", color_changed);
  gClient->GetColorByName("#50F050", color_sent);
}

/// GUI handler: Default destructor
OptionsEditor::~OptionsEditor()   {
}

/// Initialize the display, setup the widgets
void OptionsEditor::init(int num_rows)   {
  TGPicturePool* p = gClient->GetPicturePool();
  rows.reserve(num_rows);

  /// On top we put the options editor
  editor = new TGGroupFrame(this, new TGString("Options Editor: Click on Edit in the lower panel to load value"));
  editor->SetLayoutManager(new TGTableLayout(editor, 6, 7));

  source_label.dns  = new TGTextEntry     (editor, "DIM DNS node:");
  source_label.dns->SetToolTipText("DIM Domain we shall interact with");

  source_label.node = new TGTextEntry     (editor, "Target node:");
  source_label.node->SetToolTipText("Target node in the DIM Domain\nwe shall interact with");

  source_label.proc = new TGTextEntry     (editor, "DIM server:");
  source_label.proc->SetToolTipText("Process name to change options");

  source.dns        = new TGTextEntry     (editor, "...dns node...");
  source.dns->SetEnabled(kFALSE);
  source.dns->SetBackgroundColor(color_sent);
  source.dns->SetToolTipText("DIM Domain we shall interact with");

  source.node       = new TGTextEntry     (editor, "...processing node...");
  source.node->SetEnabled(kFALSE);
  source.node->SetToolTipText("Target node in the DIM Domain\nwe shall interact with");
  source.node->SetBackgroundColor(color_sent); 

  //source.proc       = new TGTextEntry     (editor, "...dim server name...");
  source.combo = new TGComboBox(editor, 0);
  source.combo->SetBackgroundColor(color_sent);
  source.combo->SetEnabled(kFALSE);
  source.combo->SetEditable(kFALSE);
  source.combo->SetEditDisabled(1);

  edit.isValid      = true;
  edit.label        = new TGTextEntry     (editor, "<Option Client> <Option name>");
  edit.label->SetToolTipText("Current option loaded in the editor");
  edit.label->SetEnabled(kFALSE);
  edit.label  -> SetBackgroundColor(color_enabled);

  edit.editor       = new TGTextEdit      (editor, 250, 110, kSunkenFrame|kDoubleBorder|kLHintsExpandY);
  edit.editor->SetBackgroundColor(color_enabled);
  edit.editor->SetReadOnly(kTRUE);

  edit.edit         = new TGTextButton    (editor, "&Reload", -1);
  edit.edit->SetToolTipText("Reload unchanged value from the table");
  edit.edit->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "click_reloadOption()");
  edit.edit->SetEnabled(kFALSE);

  edit.edit2        = new TGPictureButton (editor, p->GetPicture("refresh1.xpm",  ICON_SIZE));
  edit.edit2->SetToolTipText("Reload unchanged value from the table");
  edit.edit2->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "click_reloadOption()");
  edit.edit2->SetEnabled(kFALSE);

  edit.send         = new TGTextButton    (editor, "&Save",   -1);
  edit.send->SetToolTipText("Save changed value back to the table");
  edit.send->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "click_saveOption()");
  edit.send->SetEnabled(kFALSE);

  edit.send2        = new TGPictureButton (editor, p->GetPicture("interrupt.xpm", ICON_SIZE));
  edit.send2->SetToolTipText("Save changed value back to the table");
  edit.send2->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "click_saveOption()");
  edit.send2->SetEnabled(kFALSE);

  editor->AddFrame(source_label.dns,  new TGTableLayoutHints(0, 1, 0, 1, kLHintsLeft |kLHintsTop,  1,10,1,1)); 
  editor->AddFrame(source_label.node, new TGTableLayoutHints(0, 1, 1, 2, kLHintsLeft |kLHintsTop,  1,10,1,1)); 
  editor->AddFrame(source_label.proc, new TGTableLayoutHints(0, 1, 2, 3, kLHintsLeft |kLHintsTop,  1,10,1,1)); 
  editor->AddFrame(source.dns,        new TGTableLayoutHints(1, 2, 0, 1, kLHintsLeft |kLHintsTop,  1,10,1,1)); 
  editor->AddFrame(source.node,       new TGTableLayoutHints(1, 2, 1, 2, kLHintsLeft |kLHintsTop,  1,10,1,1)); 
  editor->AddFrame(source.combo,      new TGTableLayoutHints(1, 2, 2, 3, kLHintsLeft |kLHintsTop,  1,10,1,1)); 

  editor->AddFrame(edit.label,        new TGTableLayoutHints(0, 2, 3, 4, kLHintsLeft |kLHintsTop,  1,10,1,1)); 
  editor->AddFrame(edit.editor,       new TGTableLayoutHints(2, 3, 0, 5, kLHintsLeft |kLHintsTop|kLHintsExpandX|kLHintsExpandY, 1,1,1,1)); 
  editor->AddFrame(edit.edit,         new TGTableLayoutHints(3, 4, 3, 4, kLHintsRight|kLHintsTop, 10,1,1,1)); 
  editor->AddFrame(edit.edit2,        new TGTableLayoutHints(4, 5, 3, 4, kLHintsRight|kLHintsTop,  0,1,1,1)); 
  editor->AddFrame(edit.send,         new TGTableLayoutHints(5, 6, 3, 4, kLHintsRight|kLHintsTop, 10,1,1,1));
  editor->AddFrame(edit.send2,        new TGTableLayoutHints(6, 7, 3, 4, kLHintsRight|kLHintsTop,  0,1,1,1));
  editor->AddFrame(new TGLabel(editor,""), new TGTableLayoutHints(0, 2, 4, 5, kLHintsExpandX|kLHintsExpandY,0,0,0,0));
  editor->AddFrame(new TGLabel(editor,""), new TGTableLayoutHints(3, 7, 4, 5, kLHintsExpandX|kLHintsExpandY,0,0,0,0));

  /// Initialize the option table and its rows
  init_options_table(num_rows);

  /// Add buttons line to navigate options
  nav_group = new TGGroupFrame(this, new TGString("Options management and client interaction"));
  nav_group->SetLayoutManager(new TGTableLayout(nav_group, 2, 11));

  nav_first   = new TGTextButton(nav_group, "&First", -1);
  nav_first->SetToolTipText( "Click move to the beginning\nof the options set");
  nav_first->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "show_first_table_batch()");

  nav_firstp  = new TGPictureButton(nav_group, p->GetPicture("first_t.xpm", ICON_SIZE));
  nav_firstp->SetToolTipText( "Click move to the beginning\nof the options set");
  nav_firstp->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "show_first_table_batch()");

  nav_prev    = new TGTextButton(nav_group, "&Previous", -1);
  nav_prev->SetToolTipText( "Click move to the previous\nbatch of the options set");
  nav_prev->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "show_previous_table_batch()");

  nav_prevp   = new TGPictureButton(nav_group, p->GetPicture("previous_t.xpm", ICON_SIZE));
  nav_prevp->SetToolTipText( "Click move to the previous\nbatch of the options set");
  nav_prevp->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "show_previous_table_batch()");

  nav_label   = new TGLabel(nav_group," ... No options present ...");
  nav_label->SetBackgroundColor(color_sent);

  nav_next    = new TGTextButton(nav_group, "&Next", -1);
  nav_next->SetToolTipText( "Click move to the next\nbatch of the options set");
  nav_next->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "show_next_table_batch()");

  nav_nextp   = new TGPictureButton(nav_group, p->GetPicture("next_t.xpm", ICON_SIZE));
  nav_nextp->SetToolTipText( "Click move to the next\nbatch of the options set");
  nav_nextp->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "show_next_table_batch()");

  nav_last    = new TGTextButton(nav_group, "&Last", -1);
  nav_last->SetToolTipText( "Click move to the last\nbatch of the options set");
  nav_last->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "show_last_table_batch()");

  nav_lastp   = new TGPictureButton(nav_group, p->GetPicture("last_t.xpm", ICON_SIZE));
  nav_lastp->SetToolTipText( "Click move to the last\nbatch of the options set");
  nav_lastp->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "show_last_table_batch()");

  nav_explore = new TGTextButton(nav_group, "&Back to Explorer", -1);
  nav_explore->SetToolTipText( "Show the explorer pane");
  nav_explore->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "click_backToExplorer()");

  nav_reload  = new TGTextButton(nav_group, "&Reload", -1);
  nav_reload->SetToolTipText( "Reload the options set from source");
  nav_reload->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "click_reloadClientOptions()");

  nav_send    = new TGTextButton(nav_group, "&Send Changes", -1);
  nav_send->SetToolTipText( "Send all active changes to the process");
  nav_send->Connect("Clicked()", "xmlrpc::OptionsEditor",   this, "click_sendChanges()");

  nav_save  = new TGTextButton   (nav_group, "&Save to File", -1);
  nav_save->Connect("Clicked()", "xmlrpc::VictimNodeEditor", this, "click_saveOptionsFile()");
  nav_save->SetToolTipText("Save enabled options to file");

  nav_savep = new TGPictureButton(nav_group, p->GetPicture("bld_save.png", ICON_SIZE));
  nav_savep->Connect("Clicked()", "xmlrpc::VictimNodeEditor", this, "click_saveOptionsFile()");  
  nav_savep->SetToolTipText("Save enabled options to file");

  nav_group->AddFrame(nav_first,  new TGTableLayoutHints(1, 2, 0, 1, kLHintsRight|kLHintsCenterY|kLHintsExpandX,   0, 3, 3, 3));
  nav_group->AddFrame(nav_firstp, new TGTableLayoutHints(2, 3, 0, 1, kLHintsLeft |kLHintsCenterY,                  0, 3, 3, 3));
  nav_group->AddFrame(nav_prev,   new TGTableLayoutHints(3, 4, 0, 1, kLHintsRight|kLHintsCenterY|kLHintsExpandX,   0, 3, 3, 3));
  nav_group->AddFrame(nav_prevp,  new TGTableLayoutHints(4, 5, 0, 1, kLHintsLeft |kLHintsCenterY,                  0, 3, 3, 3));
  nav_group->AddFrame(nav_label,  new TGTableLayoutHints(5, 6, 0, 1, kLHintsCenterX|kLHintsCenterY|kLHintsExpandX, 0, 3, 3, 3));
  nav_group->AddFrame(nav_nextp,  new TGTableLayoutHints(6, 7, 0, 1, kLHintsRight|kLHintsCenterY,                  0, 3, 3, 3));
  nav_group->AddFrame(nav_next,   new TGTableLayoutHints(7, 8, 0, 1, kLHintsLeft |kLHintsCenterY|kLHintsExpandX,   0, 3, 3, 3));
  nav_group->AddFrame(nav_lastp,  new TGTableLayoutHints(8, 9, 0, 1, kLHintsRight|kLHintsCenterY,                  0, 3, 3, 3));
  nav_group->AddFrame(nav_last,   new TGTableLayoutHints(9, 10,0, 1, kLHintsLeft |kLHintsCenterY|kLHintsExpandX,   0, 3, 3, 3));

  nav_group->AddFrame(nav_explore,new TGTableLayoutHints(3, 5, 1, 2, kLHintsRight|kLHintsCenterY|kLHintsExpandX,   0, 3, 3, 3));
  nav_group->AddFrame(nav_reload, new TGTableLayoutHints(5, 6, 1, 2, kLHintsCenterX|kLHintsCenterY|kLHintsExpandX, 0, 3, 3, 3));
  nav_group->AddFrame(nav_send,   new TGTableLayoutHints(6, 8, 1, 2, kLHintsLeft|kLHintsCenterY|kLHintsExpandX,    0, 3, 3, 3));
  nav_group->AddFrame(nav_savep,  new TGTableLayoutHints(8, 9, 1, 2, kLHintsRight|kLHintsTop|kLHintsExpandX, 1,1,1,1));
  nav_group->AddFrame(nav_save,   new TGTableLayoutHints(9,10, 1, 2, kLHintsLeft|kLHintsTop|kLHintsExpandX, 1,1,1,1));

  AddFrame(editor,     new TGTableLayoutHints(3, 4, 0, 0, kLHintsRight|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY, 1,1,1,1));
  AddFrame(prop_group, new TGTableLayoutHints(3, 4, 0, 0, kLHintsRight|kLHintsCenterY|kLHintsExpandX, 1, 1, 1, 1));
  AddFrame(nav_group,  new TGTableLayoutHints(2, 1, 0, 0, kLHintsCenterX|kLHintsCenterY|kLHintsExpandX, 1, 1, 1, 1));
  
  nav_label->Resize(500,nav_label->GetDefaultHeight());

  int default_height = edit.label->GetDefaultHeight();
  source_label.dns  -> Resize(100,default_height);
  source_label.node -> Resize(100,default_height);
  source_label.proc -> Resize(100,default_height);
  source.dns  -> Resize(210,default_height);
  source.node -> Resize(210,default_height);
  source.combo -> Resize(210,default_height);

  edit.label  -> Resize(320,default_height);
  edit.editor -> Resize(500, edit.editor->GetDefaultHeight());
  edit.edit   -> Resize(edit.edit->GetDefaultSize());
  edit.send   -> Resize(edit.send->GetDefaultSize());
  for (int i = 0; i < num_rows; ++i)    {
    Row& row = rows[i];
    row.label  -> Resize(320,row.label->GetDefaultHeight());
    row.option -> Resize(500,row.option->GetDefaultHeight());
    row.edit   -> Resize(row.edit->GetDefaultSize());
    row.send   -> Resize(row.send->GetDefaultSize());
  }
}

/// Initialize the table of job options with num_rows rows
void OptionsEditor::init_options_table(int num_rows)    {
  /// Main table with the options
  prop_group = new TGGroupFrame(this, new TGString("Custom Component Option Settings"));
  prop_group->SetLayoutManager(new TGTableLayout(prop_group, num_rows, 6));
  for (int i = 0; i < num_rows; ++i)    {
    Row row = init_options_table_row(prop_group, i);
    rows.push_back(row);
  }
}

/// Initialize single row of the options table
OptionsEditor::Row OptionsEditor::init_options_table_row(TGGroupFrame* grp, int i)   {
  TGPicturePool* p = gClient->GetPicturePool();
  char text[256];
  Row row;
  ::snprintf(text,sizeof(text), "# %d ...component ... option...", i+1);
  row.id      = i;
  row.isValid = true;
  row.label   = new TGTextEntry(grp, text);
  row.label->SetBackgroundColor(color_enabled);
  row.label->SetEnabled(kFALSE);

  row.option  = new TGTextEntry(grp, "....");
  row.option->SetToolTipText("Job option value");
  row.option->SetBackgroundColor(color_enabled);
  row.option->SetEnabled(kFALSE);

  row.edit    = new TGTextButton(grp, "Edit", 1000+i);
  row.edit->Connect ("Clicked()", "xmlrpc::OptionsEditor", this, "click_editOption()");
  row.edit->SetToolTipText( "Click to load the option\ninto the editor panel");

  row.edit2   = new TGPictureButton(grp, p->GetPicture("bld_edit.png", ICON_SIZE), 2000+i);
  row.edit2->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "click_editOption()");
  row.edit2->SetToolTipText("Click to load the option\ninto the editor panel");

  row.send    = new TGTextButton(grp, "Send", 3000+i);
  row.send->Connect ("Clicked()", "xmlrpc::OptionsEditor", this, "click_sendOption()");
  row.send->SetToolTipText( "Click to send the option to the process");

  row.send2   = new TGPictureButton(grp, p->GetPicture("bld_save.xpm", ICON_SIZE), 4000+i);
  row.send2->Connect("Clicked()", "xmlrpc::OptionsEditor", this, "click_sendOption()");
  row.send2->SetToolTipText( "Click to send the option to the process");

  grp->AddFrame(row.label,  new TGTableLayoutHints(0, 1, i, i+1, kLHintsLeft |kLHintsCenterY, 1,10,1,1)); 
  grp->AddFrame(row.option, new TGTableLayoutHints(1, 2, i, i+1, kLHintsLeft |kLHintsCenterY|kLHintsExpandX|kLHintsExpandY, 1,1,1,1)); 
  grp->AddFrame(row.edit,   new TGTableLayoutHints(2, 3, i, i+1, kLHintsRight|kLHintsCenterY, 10, 1, 1, 1)); 
  grp->AddFrame(row.edit2,  new TGTableLayoutHints(3, 4, i, i+1, kLHintsRight|kLHintsCenterY,  0, 1, 1, 1)); 
  grp->AddFrame(row.send,   new TGTableLayoutHints(4, 5, i, i+1, kLHintsRight|kLHintsCenterY, 10, 1, 1, 1));
  grp->AddFrame(row.send2,  new TGTableLayoutHints(5, 6, i, i+1, kLHintsRight|kLHintsCenterY,  0, 1, 1, 1));
  return row;
}

/// Clear the entire options table
void OptionsEditor::clear_options_table()    {
  properties.clear();
  optsBatch = -1;
  show_table_batch(0);
}

/// Clear single row content in the options table
void OptionsEditor::clear_options_table_row(const Row& row)   {
  Pixel_t  color = getBackgroundColor(OPTION_ENABLED);
  row.label->SetBackgroundColor(color);
  row.option->SetBackgroundColor(color);
  row.label->Clear();
  row.option->Clear();
}

/// Reset the content of the options editor
void OptionsEditor::reset_options_editor(const string& cl, const string& opt_val)  {
  edit.label->SetText(cl.c_str());
  edit.editor->SetText(new TGText(opt_val.c_str()));
  edit.editor->SetReadOnly(kTRUE);
  edit.editor->Update();
  edit.edit->SetEnabled(kFALSE);
  edit.edit2->SetEnabled(kFALSE);
  edit.send->SetEnabled(kFALSE);
  edit.send2->SetEnabled(kFALSE);
  gClient->NeedRedraw(edit.label);
  gClient->NeedRedraw(edit.editor);
  currentEdit = "";
}

/// Show single row in the options table
void OptionsEditor::show_options_table_row(const Row& row, const ObjectProperty& p)   {
  Pixel_t  color = getBackgroundColor(p.flag);
  if ( row.label )    {
    string   label = p.client + "." + p.name;
    row.label->SetBackgroundColor(color);
    if ( label != row.label->GetText() )
      row.label->SetText(label.c_str());
    row.label->SetEdited(kTRUE);
    gClient->NeedRedraw(row.label);
  }
  if ( row.option )   {
    row.option->SetBackgroundColor(color);
    if ( p.value != row.option->GetText() )
      row.option->SetText(p.value.c_str());
    row.option->SetEdited(kTRUE);
    gClient->NeedRedraw(row.option);
  }
}


/// GUI handler: Load the first batch of options into the matrix
void OptionsEditor::show_first_table_batch()    {
  show_table_batch(optsBatch=0);
}

/// GUI handler: Load the last batch of options into the matrix
void OptionsEditor::show_last_table_batch()    {
  int num_batch = int(properties.size()/rows.size());
  if ( 0 == (properties.size()%rows.size()) ) --num_batch;
  show_table_batch(optsBatch=num_batch);
}

/// GUI handler: Load the next batch of options into the matrix
void OptionsEditor::show_next_table_batch()    {
  if ( (1+optsBatch)*rows.size() > properties.size() )  {
    GuiMsg("show_next_table_batch: You are at the end of the options set..... [%d]",optsBatch).send(gui);
    return;
  }
  show_table_batch(++optsBatch);
}

/// GUI handler: Load the previous batch of options into the matrix
void OptionsEditor::show_previous_table_batch()    {
  if ( optsBatch-1 < 0 )   {
    GuiMsg("show_previous_table_batch: You are at the beginning of the options set..... [%d]",optsBatch).send(gui);
    return;
  }
  lock_guard<mutex> lck(lock);
  show_table_batch(--optsBatch);
}

/// Show a batch of options in the options table
void OptionsEditor::show_table_batch(int batch)    {
  char text[256];
  size_t off = batch*rows.size();
  size_t cnt = std::min(rows.size(),properties.size()-off);
  ::snprintf(text,sizeof(text),"Showing %ld rows out of %ld: %ld -> %ld",
	     cnt, properties.size(), off, off+cnt-1);
  nav_label->SetText(text);
  nav_group->Layout();
  gClient->NeedRedraw(nav_label);
  for (size_t i=0; i < cnt; ++i)
    show_options_table_row(rows[i], properties[i+off]);
  for (size_t i=cnt; i < rows.size(); ++i)
    clear_options_table_row(rows[i]);
}

/// Update after successful sending of the changes
void OptionsEditor::update_sent_options()   {
  for(auto& p : properties)   {
    if ( (p.flag&OPTION_CHANGED) == OPTION_CHANGED )  {
      p.flag &= ~OPTION_CHANGED;
      p.flag |= OPTION_ENABLED|OPTION_SENT;
    }
    else  {
      p.flag |= OPTION_ENABLED;
    }
  }
  show_table_batch(optsBatch);
}

/// Save the not-disabled options to a file
void OptionsEditor::save_options_file(unsigned int mask, bool mask_exact)  const {
  static TString dir(".");
  TGWindow* win = dynamic_cast<TGWindow*>(gui);
  TGFileInfo fi;
  fi.fFileTypes = Option_filetypes;
  fi.fIniDir    = StrDup(dir);
  printf("fIniDir = %s\n", fi.fIniDir);
  new TGFileDialog(gClient->GetRoot(), win ? win->GetParent() : GetParent(), kFDOpen, &fi);
  GuiMsg("Open file: %s (dir: %s)", fi.fFilename ? fi.fFilename : "(None)", fi.fIniDir).send(gui);
  dir = fi.fIniDir;
  if ( fi.fFilename != 0 )   {
    ofstream os;
    os.open(fi.fFilename, ios::out);
    if ( os.is_open() )   {
      char text[128];
      os << "//" << endl
	 << "// --------------- Options saved by the porperty GUI " 
	 << ::lib_rtl_timestr_r(text,sizeof(text)) << endl
	 << "//                 " << node << "   " << process << endl;
      for(auto& p : properties)  {
	if ( mask_exact && (p.flag&mask) == mask )
	  os << p.client << "." << p.name << "\t\t = " << p.value << ";" << endl;
	else if ( !mask_exact && (p.flag&mask) )
	  os << p.client << "." << p.name << "\t\t = " << p.value << ";" << endl;
      }
      os.close();
      return;
    }
    GuiException(kMBOk,kMBIconStop,"Editor: Save Options File",
		 "Failed to open the options file:\n%s\nError: %s\nCannot write options!",
		 fi.fFilename, gSystem->GetErrorStr()).send(gui);
  }
  GuiMsg("Option file saving cancelled!").send(gui);
}

/// GUI handler: Load the next batch of options into the matrix
void OptionsEditor::reloadOption()    {
  string nam = edit.label->GetText();
  if ( optsBatch >= 0 )  {
    lock_guard<mutex> lck(lock);
    size_t frst = optsBatch*rows.size();
    for( size_t i=0; i<rows.size(); ++i )  {
      if ( frst+i < properties.size() )    {
	ObjectProperty& p = properties[frst+i];
	string label = p.client + "." + p.name;
	if ( label == currentEdit )  {
	  editOption(rows[i]);
	  return;
	}
      }
    }
  }
  GuiException(kMBOk,kMBIconStop,"Editor: Reload Option",
	       "Cannot locate property:\n %s = \n %s\nCurrent Option Client:'%s'\n"
	       "You can only edit options\nshown in the table below!",
	       nam.c_str(), edit.editor->GetText()->AsString().Data(),
	       currentEdit.c_str()).send(gui);
}

/// GUI handler: Load the previous batch of options into the matrix
void OptionsEditor::saveOption()    {
  string nam  = edit.label->GetText();
  if ( optsBatch >= 0 )  {
    lock_guard<mutex> lck(lock);
    size_t frst = optsBatch*rows.size();
    for( size_t i=0; i<rows.size(); ++i )  {
      if ( properties.size() > frst+i )   {
	ObjectProperty& p = properties[frst+i];
	string label = p.client + "." + p.name;
	Row& row = rows[i];
	const char* fmt = "Unchanged option %s.%s = %s [row:%ld]";
	if ( label == currentEdit )  {
	  size_t loc  = nam.find('.');
	  string cl   = nam.substr(0,loc);
	  string opt  = nam.substr(loc+1);
	  TString val  = edit.editor->GetText()->AsString();
	  if ( !(p.client == cl && p.name == opt && p.value == val) )  {
	    p.flag  |=  OPTION_CHANGED;
	    p.flag  &= ~OPTION_SENT;
	    p.client = cl;
	    p.name   = opt;
	    p.value  = val.Data();
	    fmt = "Updated option %s.%s = %s [row:%ld]";
	  }
	  GuiMsg(fmt, p.client.c_str(), p.name.c_str(), p.value.c_str(), i).send(gui);
	  show_options_table_row(row, p);
	  reset_options_editor("----","");
	  return;
	}
      }
    }
  }
  GuiException(kMBOk,kMBIconStop,"Editor: Save Option",
	       "Cannot locate property:\n %s = \n %s\nCurrent Option Client:'%s'\n"
	       "You can only edit options\nshown in the table below!",
	       nam.c_str(), edit.editor->GetText()->AsString().Data(),
	       currentEdit.c_str()).send(gui);
}

/// GUI handler: Add an option from the data in the editor GUI
void OptionsEditor::addOption()    {
  string  nam = edit.label->GetText();
  for( const Row& row : rows )   {
    if ( nam == row.label->GetText() )   {
      GuiException(kMBOk,kMBIconStop,"Editor: Add Option",
		   "An option with this client pattern <client>.<name>\n"
		   "is already present: %s = \n%s\n"
		   "\n\nAction denied!\n\n",
		   nam.c_str(), row.option->GetText()).send(gui);
      return;
    }
  }
  TString val = edit.editor->GetText()->AsString();
  size_t  loc = nam.find('.');
  if ( loc != string::npos )   {
    ObjectProperty p(nam.substr(0,loc),nam.substr(loc+1),val.Data());
    p.flag |=  OPTION_CHANGED|OPTION_ENABLED;
    p.flag &= ~OPTION_SENT;
    properties.push_back(p);
    show_last_table_batch();
    reset_options_editor("----","");
    return;
  }
  GuiException(kMBOk,kMBIconStop,"Editor: Add Option",
	       "The <client>.<name> value is incorrect:\n%s = \n%s",
	       nam.c_str(), edit.editor->GetText()->AsString().Data()).send(gui);
}

/// Edit option on request as indicated by the row
void OptionsEditor::editOption(const Row& row)   {
  currentEdit = row.label->GetText();
  edit.edit->SetEnabled(kTRUE);
  edit.edit2->SetEnabled(kTRUE);
  edit.send->SetEnabled(kTRUE);
  edit.send2->SetEnabled(kTRUE);
  edit.label->SetText(row.label->GetText());
  edit.editor->SetReadOnly(kFALSE);
  edit.editor->SetText(new TGText(row.option->GetText()));
  edit.editor->Update();
  gClient->NeedRedraw(edit.label);
  gClient->NeedRedraw(edit.editor);
}

/// Create/Reset communication client. Basic implementation ONLY.
void OptionsEditor::createClient(GuiCommand* c)   {
  dns = c->dns;
  node = c->node;
  process = c->client;
  properties.clear();
  optsBatch = -1;
  source.dns  -> SetText(dns.c_str());
  source.node -> SetText(node.c_str());
  //source.proc -> SetText(process.c_str());
  source.combo -> RemoveAll();
  source.combo -> AddEntry(process.c_str(),0);
  source.combo -> Select(0, kFALSE);
}

/// Translate options flag to row color
Pixel_t OptionsEditor::getBackgroundColor(int flag)   const    {
  if      ( flag&OPTION_SENT     ) return color_sent;
  else if ( flag&OPTION_CHANGED  ) return color_changed;
  else if ( flag&OPTION_DISABLED ) return color_disabled;
  else if ( flag&OPTION_ENABLED  ) return color_enabled;
  return color_enabled;
}

/// Save the not-disabled options to a file
void OptionsEditor::saveOptionsFile()   {
  save_options_file(OPTION_ENABLED, false);
}

/// GUI handler: Save the not-disabled options to a file
void OptionsEditor::click_saveOptionsFile()    {
  CPP::IocSensor::instance().send(this,EDITOR_SAVE_OPTIONS);
}

/// GUI handler: Reload all client options
void OptionsEditor::click_reloadClientOptions()    {
  CPP::IocSensor::instance().send(this,EDITOR_LOAD_OPTIONS);
}

/// GUI handler: Send all changed options
void OptionsEditor::click_sendChanges()   {
  CPP::IocSensor::instance().send(this,EDITOR_SEND_CHANGES);
}

/// GUI handler: Back to explorer tab
void OptionsEditor::click_backToExplorer()   {
  CPP::IocSensor::instance().send(gui,XMLRPCGUI_SHOW_EXPLORERPANE);
}

/// GUI handler: Send option to the target
void OptionsEditor::click_saveOption()    {
  CPP::IocSensor::instance().send(this,EDITOR_SAVE_OPTION);
}

/// GUI handler: Send option to the target
void OptionsEditor::click_reloadOption()    {
  CPP::IocSensor::instance().send(this,EDITOR_RELOAD_OPTION);
}

/// GUI handler: Add an option from the data in the editor GUI
void OptionsEditor::click_addOption()    {
  CPP::IocSensor::instance().send(this,EDITOR_ADD_OPTION);
}

/// GUI handler: Send option to the target
void OptionsEditor::click_sendOption()    {
  TGTextButton *b = (TGTextButton *)gTQSender;
  Int_t idx = b->WidgetId()%1000;
  Row& row = rows[idx];
  GuiMsg("click: Send Option: Row: %d  %s.%s",idx,row.label->GetText(),row.option->GetText()).send(gui);
  CPP::IocSensor::instance().send(this,EDITOR_SEND_OPTION,&row);
}

/// GUI handler: Load selected option into editor
void OptionsEditor::click_editOption()   {
  TGTextButton *b = (TGTextButton *)gTQSender;
  Int_t idx = b->WidgetId()%1000;
  Row& row = rows[idx];
  GuiMsg("click: Edit Option: row: %d  %s.%s",idx,row.label->GetText(),row.option->GetText()).send(gui);
  CPP::IocSensor::instance().send(this,EDITOR_EDIT_OPTION,&row);
}

/// Interactor interrupt handler callback
void OptionsEditor::handle(const CPP::Event& ev)    {
  switch(ev.eventtype) {
  case IocEvent: {
    try {    
      switch(ev.type) {
      case XMLRPCGUI_SHOW_CLIENT:
	try  {
	  unique_ptr<GuiCommand> c(ev.iocPtr<GuiCommand>());
	  clear_options_table();
	  createClient(c.get());
	}
	catch(const std::exception& e)   {
	  GuiException(kMBOk,kMBIconStop,"ERROR: Create Client",
		       "Failed to create option client:\n%s",e.what()).send(gui);
	}
	return;

      case EDITOR_EDIT_OPTION:
	try  {
	  editOption(*ev.iocPtr<Row>());
	}
	catch(const std::exception& e)   {
	  GuiException(kMBOk,kMBIconStop,"ERROR: Edit Option",
		       "Failed to edit option:\n%s",e.what()).send(gui);
	}
	return;

      case EDITOR_RELOAD_OPTION:
	try  {
	  reloadOption();
	}
	catch(const std::exception& e)   {
	  GuiException(kMBOk,kMBIconStop,"ERROR: Reload Option",
		       "Failed to reload option:\n%s",e.what()).send(gui);
	}
	return;

      case EDITOR_SAVE_OPTION:
	try  {
	  saveOption();
	}
	catch(const std::exception& e)   {
	  GuiException(kMBOk,kMBIconStop,"ERROR: Save Option",
		       "Failed to save option:\n%s",e.what()).send(gui);
	}
	return;

      case EDITOR_SEND_OPTION:
	try  {
	  sendOption(*ev.iocPtr<Row>());
	}
	catch(const std::exception& e)   {
	  GuiException(kMBOk,kMBIconStop,"ERROR: Send Option",
		       "Failed to send option:\n%s",e.what()).send(gui);
	}
	return;

      case EDITOR_CLEAR_OPTIONS:
	clear_options_table();
	return;

      case EDITOR_LOAD_OPTIONS:
	try  {
	  loadOptions();
	}
	catch(const std::exception& e)   {
	  GuiException(kMBOk,kMBIconStop,"RPC: Load Options",
		       "Failed to load the option set:\n%s",e.what()).send(gui);
	  CPP::IocSensor::instance().send(this,EDITOR_CLEAR_OPTIONS);
	}
	return;

      case EDITOR_SAVE_OPTIONS:
	try  {
	  saveOptionsFile();
	}
	catch(const std::exception& e)   {
	  GuiException(kMBOk,kMBIconStop,"RPC: Save Changes",
		       "Failed to send options:\n%s",e.what()).send(gui);
	}
	return;

      case EDITOR_SEND_CHANGES:
	try  {
	  sendChanges();
	}
	catch(const std::exception& e)   {
	  GuiException(kMBOk,kMBIconStop,"RPC: Send Changes",
		       "Failed to send changes:\n%s",e.what()).send(gui);
	}
	return;

      default:
	break;
      }
    }
    catch(const std::exception& e)   {
      GuiException(kMBOk,kMBIconAsterisk,"IOC handling exception",e.what()).send(gui);
    }
    break;
  }
  case TimeEvent: {
    long cmd = long(ev.timer_data);
    switch(cmd) {
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
}
