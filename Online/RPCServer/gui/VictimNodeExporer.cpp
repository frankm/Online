//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "RPC/GUI/VictimNodeExplorer.h"
#include "RPC/GUI/GuiException.h"
#include "RPC/GUI/GuiCommand.h"
#include "RPC/GUI/GuiMsg.h"

#include "RPC/DimClient.h"
#include "HTTP/HttpClient.h"

#include "CPP/TimeSensor.h"
#include "CPP/IocSensor.h"
#include "CPP/Event.h"

/// ROOT include files
#include "TGLabel.h"
#include "TGButton.h"
#include "TGMsgBox.h"
#include "TGTextEntry.h"
#include "TGComboBox.h"
#include "TApplication.h"
#include "TGTableLayout.h"

ClassImp(xmlrpc::VictimNodeExplorer)

using namespace std;
using namespace xmlrpc;

namespace {
#if 0
  string dns_service(const string& d)   {
    string dd = d;
    for(size_t i=0; i<dd.size(); ++i) dd[i] = ::tolower(dd[i]);
    return "/RO/"+dd+"/ROpublish";
  }
#endif
  string _selectedText(TGComboBox* c)  {
    TGLBEntry* selected = c->GetSelectedEntry();
    return selected ? selected->GetTitle() : "";
  }
#if 0
  void _clr(TGComboBox* c)  {
    TGTextEntry* e = c->GetTextEntry();
    if ( e ) e->SetText("");
  }
  string str_upper(string d)   {
    for(size_t i=0; i<d.size(); ++i) d[i] = ::toupper(d[i]);
    return d;
  }
#endif
}

/// Standard initializing constructor
VictimNodeExplorer::VictimNodeExplorer(TGWindow* p,CPP::Interactor* g)
  : TGCompositeFrame(p, 100, 100, kVerticalFrame), gui(g)
{
  TGCompositeFrame* envelope   = new TGCompositeFrame(this, 60, 20, kVerticalFrame);

  gClient->GetColorByName("#c0c0c0", disabled);
  gClient->GetColorByName("white", enabled);

  group   = new TGGroupFrame(envelope, "Select client from DNS inventory");
  group->SetLayoutManager(new TGTableLayout(group, 7, 6));

  dns.label = new TGLabel(group,         "Enter DNS node",     DNS_LABEL);
  dns.input = new TGComboBox(group,      DNS_INPUT);
  dns.reload = new TGTextButton(group,   "Reload",             DNS_RELOAD);
  dns.load  = new TGTextButton(group,    "Load Nodes",         DNS_LOAD);
  dns.fill  = new TGLabel(group,         "",                   DNS_FILL);
  dns.input->Connect("Selected(Int_t)",    "xmlrpc::VictimNodeExplorer", this, "dnsChanged(Int_t)");
  dns.reload->Connect("Clicked()",         "xmlrpc::VictimNodeExplorer", this, "loadDns()");
  dns.load->Connect("Clicked()",           "xmlrpc::VictimNodeExplorer", this, "loadNodes()");
  dns.input->SetMaxWidth(100);
  group->AddFrame(dns.label,  new TGTableLayoutHints(0, 1, 0, 1, kLHintsLeft |kLHintsCenterY|kLHintsExpandX, 1,40,1,1));
  group->AddFrame(dns.input,  new TGTableLayoutHints(1, 2, 0, 1, kLHintsLeft |kLHintsCenterY|kLHintsExpandY, 1,1,10,1));
  group->AddFrame(dns.reload, new TGTableLayoutHints(2, 3, 0, 1, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(dns.load,   new TGTableLayoutHints(3, 4, 0, 1, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(dns.fill,   new TGTableLayoutHints(4, 5, 0, 1, kLHintsRight|kLHintsCenterY|kLHintsExpandX, 10,1,1,1));

  node.label  = new TGLabel(group,      "Select node",        NODE_LABEL);
  node.input  = new TGComboBox(group,   NODE_INPUT);
  node.reload = new TGTextButton(group, "Reload",             NODE_RELOAD);
  node.load   = new TGTextButton(group, "Set Victim Node",    NODE_LOAD);
  node.fill   = new TGLabel(group,      "",                   NODE_FILL);
  node.reload->Connect("Clicked()",        "xmlrpc::VictimNodeExplorer", this, "loadNodes()");
  node.load->Connect("Clicked()",          "xmlrpc::VictimNodeExplorer", this, "setVictimNode()");
  node.input->SetMaxWidth(100);
  group->AddFrame(node.label,  new TGTableLayoutHints(0, 1, 1, 2, kLHintsLeft |kLHintsCenterY|kLHintsExpandX, 1,40,1,1));
  group->AddFrame(node.input,  new TGTableLayoutHints(1, 2, 1, 2, kLHintsLeft |kLHintsCenterY|kLHintsExpandY, 1,1,10,1));
  group->AddFrame(node.reload, new TGTableLayoutHints(2, 3, 1, 2, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(node.load,   new TGTableLayoutHints(3, 4, 1, 2, kLHintsRight|kLHintsCenterY, 10,1,1,1)); 
  group->AddFrame(node.fill,   new TGTableLayoutHints(4, 5, 1, 2, kLHintsRight|kLHintsCenterY|kLHintsExpandX, 10,1,1,1));

  envelope->AddFrame(group, new TGLayoutHints(kLHintsRight|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY, 1,1,1,1));
  AddFrame(envelope, new TGLayoutHints(kLHintsLeft|kLHintsTop|kLHintsExpandX, 1, 1, 1, 1));
  dns.input->Resize(200,22);
  node.input->Resize(200,22);

  auto cl = rpc::client<http::HttpClient>();
  // Use bridge to RPC server:
  (*cl)->userRequestHeaders.emplace_back("Dim-DNS","ecs03");
  (*cl)->userRequestHeaders.emplace_back("Dim-Server","/RPCDomainInfo");
  (*cl)->userRequestHeaders.emplace_back("RPC-Port","2601");
  (*cl)->open("ecs03.lbdaq.cern.ch", 2600);
  domain_client.handler = std::move(cl);
  //DimClient* cl = new DimClient("/RPCDomainInfo","ecs03");
  IocSensor::instance().send(this,DNS_LOAD);
}

/// Default destructor
VictimNodeExplorer::~VictimNodeExplorer()   {
}

/// Deferred execution using wtc
void VictimNodeExplorer::exec_call(Int_t cmd)   {
  printf("Running exec: %d\n",cmd);
  CPP::IocSensor::instance().send(this,cmd);
}

/// Interactor interrupt handler callback
void VictimNodeExplorer::handle(const CPP::Event& ev)   {
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type) {
    case DNS_LOAD:
      loadDns();
      break;
    case NODE_RELOAD:
    case NODE_LOAD:
      loadNodes();
      break;
    default:
      break;
    }
    break;
  }
  case TimeEvent: {
    long cmd = long(ev.timer_data);
    switch(cmd) {
    case DNS_LOAD:
      IocSensor::instance().send(this,cmd);
      break;
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
}

/// Load the DNS domains into the widget
void VictimNodeExplorer::loadDns()   {
  try {
    set<string> domains = domain_client.domains();
    Int_t cnt = 0;
    dns.input->RemoveAll();
    for(auto in=domains.begin(); in!=domains.end(); ++in, ++cnt)
      dns.input->AddEntry((*in).c_str(), cnt);
    dns.input->SetEnabled(kTRUE);
    if ( !domains.empty() )
      dns.input->Select(0, kFALSE);
  }
  catch(const std::exception& e)    {
    GuiException(kMBOk,kMBIconAsterisk,"Options explorer exception",e.what()).send(gui);
  }
}

/// Callback when the text of the dns node changed
void VictimNodeExplorer::dnsChanged(Int_t selected_id)  {  
  string dns_node = _selectedText(dns.input);
  GuiMsg("VictimNodeExplorer::dnsChanged: ID=%d [%s]",selected_id,dns_node.c_str()).send(gui);
  if ( !dns_node.empty() )  {
    CPP::IocSensor::instance().send(this,NODE_LOAD);
  }
}

/// Button callback to load nodes of a domain
void VictimNodeExplorer::loadNodes()  {
  string dns_node = _selectedText(dns.input);
  if ( !dns_node.empty() )   {
    try  {
      set<string> nodes  = domain_client.nodes(dns_node);
      Int_t cnt = 0, sel = node.input->GetSelected();
      node.input->RemoveAll();
      GuiMsg("loadNodes: Got %ld nodes",nodes.size()).send(gui);
      for(auto in=nodes.begin(); in != nodes.end(); ++in, ++cnt)  {
	node.input->AddEntry((*in).c_str(), cnt);
      }
      if ( sel >= 0 ) node.input->Select(sel);
      else if ( !nodes.empty() ) node.input->Select(0);
      //node.input->SetEnabled(kTRUE);
      //dns.input->SetEnabled(kFALSE);
    }
    catch(const std::exception& e)    {
      GuiException(kMBOk,kMBIconAsterisk,
		   "Options explorer exception",
		   e.what()).send(gui);
    }
    return;
  }
  GuiException(kMBOk,kMBIconAsterisk,
	       "Options explorer exception",
	       "No valid DNS node selected!").send(gui);
}

/// Callback to clear text field
void VictimNodeExplorer::clearNodeField()   {
  node.input->RemoveAll();
}

/// Load all DNS clients of a DNS (callback of "Load")
void VictimNodeExplorer::setVictimNode()    {
}
