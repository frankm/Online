#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/MBMDump
--------------
#]=======================================================================]

add_library(MBMDump SHARED
        src/BankListWindow.cpp
        src/BankTypesWindow.cpp
        src/BankWindow.cpp
        src/BaseMenu.cpp
        src/DisplayMenu.cpp
        src/DisplayWindow.cpp
        src/EventListWindow.cpp
        src/FileMainMenu.cpp
        src/FileMenu.cpp
        src/Format.cpp
        src/FormatMenu.cpp
        src/MBMDump.cpp
        src/MBMMainMenu.cpp
        src/PrintMenu.cpp
        src/Requirement.cpp)
if(NOT "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" )
    target_compile_options(MBMDump PRIVATE -Wno-format-truncation)
endif()
target_include_directories(MBMDump PRIVATE include/)
target_include_directories(MBMDump INTERFACE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/>
  $<INSTALL_INTERFACE:include/>
)
target_link_libraries(MBMDump PRIVATE Online::OnlineBase Online::EventData Online::UPI)
add_library(Online::MBMDump ALIAS MBMDump)
install(TARGETS MBMDump EXPORT Online LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}")

add_executable(mbm_dump main/mbmdump.cpp)
target_link_libraries(mbm_dump PRIVATE Online::MBMDump)
install(TARGETS   mbm_dump RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}")

