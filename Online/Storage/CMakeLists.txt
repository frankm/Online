#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/Storage
--------------
#]=======================================================================]

online_library(StorageClient SHARED
        src/client/client.cpp
        src/client/client_async.cpp
        src/client/client_sync.cpp
        src/client/communication.cpp
        src/client/compress.cpp
        src/client/fdb_client.cpp)
target_link_libraries(StorageClient
 PUBLIC     Boost::headers
            Boost::system
            Online::HTTP
 PRIVATE    Online::OnlineBase
            ZLIB::ZLIB
)
target_compile_definitions(StorageClient PRIVATE STORAGECLIENT)

online_library(StorageServer SHARED
        src/server/fdb_db_server.cpp
        src/server/fdb_dbase.cpp
        src/server/fdb_fs_server.cpp
        src/server/fdb_sqldb.cpp
        src/server/sqlite_test.cpp
)
target_link_libraries(StorageServer
  PUBLIC    Online::OnlineBase
            Online::StorageClient
  PRIVATE   Boost::filesystem
            Online::sqldb_sqlite
            Online::dim
)
target_compile_definitions(StorageServer PRIVATE STORAGECLIENT)
if(TARGET PkgConfig::mysqlclient)
    target_compile_definitions(StorageServer PRIVATE STORAGE_HAVE_MYSQL)
endif()

online_library(StorageCli SHARED
        src/cli/Setup.cpp
        src/cli/fdb_cli.cpp
        src/cli/fdb_client_test.cpp
        src/cli/fdb_dbase_test.cpp
)
target_link_libraries(StorageCli PUBLIC Online::StorageClient)
online_install_includes(include)

