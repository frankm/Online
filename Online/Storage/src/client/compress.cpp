//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Storage
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Storage/compress.h>

// C/C++ include files
#include <system_error>
#include <sstream>
#include <cstring>
#include <cerrno>
#include <zlib.h>

using namespace std;

namespace {
  static const std::string base64_chars = 
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "0123456789+/";


  static inline bool is_base64(unsigned char c) {
    return (isalnum(c) || (c == '+') || (c == '/'));
  }
}

/// Namespace for the HTTP utilities
namespace Online   {

  /// Namespace for the storage interface
  namespace storage   {

    /// Namespace for the HTTP compression utilities
    namespace compress   {

      std::string base64_encode(const unsigned char* bytes_to_encode, unsigned int in_len) {
	std::string ret;
	int i = 0;
	int j = 0;
	unsigned char char_array_3[3];
	unsigned char char_array_4[4];

	while (in_len--) {
	  char_array_3[i++] = *(bytes_to_encode++);
	  if (i == 3) {
	    char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
	    char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
	    char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
	    char_array_4[3] = char_array_3[2] & 0x3f;

	    for(i = 0; (i <4) ; i++)
	      ret += base64_chars[char_array_4[i]];
	    i = 0;
	  }
	}

	if (i)    {
	  for(j = i; j < 3; j++)
	    char_array_3[j] = '\0';

	  char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
	  char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
	  char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
	  char_array_4[3] = char_array_3[2] & 0x3f;

	  for (j = 0; (j < i + 1); j++)
	    ret += base64_chars[char_array_4[j]];

	  while((i++ < 3))
	    ret += '=';
	}
	return ret;
      }

      std::vector<unsigned char> base64_decode(const std::string& encoded_string) {
	size_t in_len = encoded_string.size();
	size_t i = 0;
	size_t j = 0;
	int in_ = 0;
	unsigned char char_array_4[4], char_array_3[3];
	std::vector<unsigned char> ret;

	while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
	  char_array_4[i++] = encoded_string[in_]; in_++;
	  if (i ==4) {
	    for (i = 0; i <4; i++)
	      char_array_4[i] = static_cast<unsigned char>(base64_chars.find(char_array_4[i]));

	    char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
	    char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
	    char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

	    for (i = 0; (i < 3); i++)
	      ret.push_back(char_array_3[i]);
	    i = 0;
	  }
	}

	if (i) {
	  for (j = i; j <4; j++)
	    char_array_4[j] = 0;
	  for (j = 0; j <4; j++)
	    char_array_4[j] = static_cast<unsigned char>(base64_chars.find(char_array_4[j]));

	  char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
	  char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
	  char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

	  for (j = 0; (j < i - 1); j++) ret.push_back(char_array_3[j]);
	}
	return ret;
      }


#define CHUNK 0x4000

      /* These are parameters to inflateInit2. See
	 http://zlib.net/manual.html for the exact meanings. */

#define windowBits       15
#define ENABLE_ZLIB_GZIP 32
#define GZIP_ENCODING    16

      /// compress a byte buffer
      vector<unsigned char> compress(const string& encoding, 
				     const vector<unsigned char>& data, 
				     string& used_encoding)   {
	vector<unsigned char> result;
	used_encoding = "";
	if ( !encoding.empty() )   {
	  stringstream str;
	  bool gzip    = encoding.find("gzip")    != string::npos;
	  bool deflate = encoding.find("deflate") != string::npos;
	  if ( gzip || deflate )   {
	    unsigned char out[CHUNK+1];
	    int status, window = windowBits + (gzip ? GZIP_ENCODING : 0);
	    z_stream strm;

	    result.reserve(1024*1024);
	    strm.zalloc = Z_NULL;
	    strm.zfree  = Z_NULL;
	    strm.opaque = Z_NULL;
	    strm.avail_in = 0;
	    strm.next_in  = (unsigned char*)data.data();
	    status = ::deflateInit2 (&strm, 
				     Z_DEFAULT_COMPRESSION,
				     Z_DEFLATED,
				     window, 8,
				     Z_DEFAULT_STRATEGY);
	    if ( status < 0 )   {
	      str << "HTTP [Failed to initialize zlib/gzip " 
		  << make_error_code(errc(EINVAL)).message() << "]";
	      goto Default;
	    }
	    strm.avail_in = data.size();
	    strm.next_in  = (unsigned char*)data.data();
	    strm.avail_in = data.size();
	    do {
	      strm.avail_out = CHUNK;
	      strm.next_out  = out;
	      status = ::deflate(&strm, Z_FINISH);
	      if ( status < 0 )   {
		str << "XMLRPC [Failed to deflate buffer with zlib/gzip "
		    << make_error_code(errc(EINVAL)).message() << "]";
		::deflateEnd (&strm);
		goto Default;
	      }
	      copy(out, out+CHUNK-strm.avail_out, back_inserter(result));
	    }  while (strm.avail_out == 0);
	    ::deflateEnd(&strm);
	    used_encoding = gzip ? "gzip" : "deflate";
	    return result;
	  }
	}
      Default:
	result = data;
	return result;
      }

      /// Inflate the response
      vector<unsigned char> decompress(const string& encoding, 
				       const vector<unsigned char>& data)  {
	if ( !encoding.empty() )   {
	  stringstream str;
	  bool gzip    = encoding.find("gzip")    != string::npos;
	  bool deflate = encoding.find("deflate") != string::npos;
	  if ( gzip || deflate )   {
	    vector<unsigned char> result;
	    unsigned char out[CHUNK+1];
	    z_stream strm;

	    result.reserve(1024*1024);
	    ::memset(&strm,0,sizeof(strm));
	    strm.zalloc   = Z_NULL;
	    strm.zfree    = Z_NULL;
	    strm.opaque   = Z_NULL; 
	    strm.next_in  = (unsigned char*)&data[0];
	    strm.avail_in = 0;
	    int window = windowBits + (gzip ? ENABLE_ZLIB_GZIP : 0);
	    int status = ::inflateInit2(&strm, window);
	    if ( status < 0 )   {
	      str << "XMLRPC [Failed to initialize zlib/gzip "
		  << make_error_code(errc(EINVAL)).message() << "]";
	      goto Default;
	    }
	    strm.avail_in = data.size();
	    strm.next_in  = (unsigned char*)&data[0];
	    do {
	      strm.avail_out = CHUNK;
	      strm.next_out  = out;
	      status         = ::inflate (&strm, Z_NO_FLUSH);
	      switch (status) {
	      case Z_OK:
	      case Z_STREAM_END:
		break;
	      case Z_BUF_ERROR:
	      default:
		inflateEnd(&strm);
		str << "XMLRPC [Failed inflate buffer with zlib/gzip : " << status;
		goto Default;
	      }
	      copy(out, out+CHUNK-strm.avail_out, back_inserter(result));
	    }  while (strm.avail_out == 0);
	    ::inflateEnd(&strm);
	    return result;
	  }
	  else if ( encoding.find("identity") != string::npos )   {
	    goto Default;
	  }
	}
      Default:
	return data;
      }

      std::vector<unsigned char> to_vector(const char* data)   {
	std::vector<unsigned char> r;
	if ( data )   {
	  size_t len = ::strlen(data);
	  r.reserve(len+1);
	  for(const char *c=data; *c; ++c) r.push_back((unsigned char)(*c));
	}
	return r;
      }
      std::vector<unsigned char> to_vector(const std::string& data)   {
	std::vector<unsigned char> r;
	if ( !data.empty() )   {
	  r.reserve(data.length()+1);
	  for(const char *c=data.c_str(); *c; ++c) r.push_back((unsigned char)(*c));
	}
	return r;
      }

    }
  }
}
