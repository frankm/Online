//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include "Setup.h"

// C/C++ include files
#include <iostream>
#include <boost/filesystem.hpp>

using namespace std;
using namespace Online::storage;
using namespace Online::fdb_test;
//
//==========================================================================
