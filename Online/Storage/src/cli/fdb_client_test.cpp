//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
/*
gentest.exe libStorageCli.so fdb_test_parse_uri "-url=http://pluscc08.lbdaq.cern.ch:12345/path/to/file/data.xml?query=5&data"

*/
// Framework inclde files
#include "Setup.h"

// C/C++ include files
#include <iostream>
#include <filesystem>
#include <sys/types.h>
#include <sys/stat.h>

using namespace std;
using namespace Online::storage;
using namespace Online::fdb_test;
//
//==========================================================================
extern "C" int fdb_test_path(int argc, char** argv)    {
  RTL::CLI cli(argc, argv, [](int, char**) {});
  string name;
  cli.getopt("path", 3, name);
  if ( !name.empty() )   {
    string dir;
    vector<string> v;
    filesystem::path p(name);
    p = p.parent_path();
    for( auto i=p.begin(); i != p.end(); ++i )   {
      cout << (*i) << endl;
      v.emplace_back(i->string());
    }
    dir = "";
    struct stat stat;
    for( size_t i=1; i < v.size(); ++i )  {
      dir += "/";
      dir += v[i];
      if ( -1 == ::stat(dir.c_str(), &stat) )   {
	cout << dir << endl;
      }
    }
  }
  return 0;
}
//
//==========================================================================
extern "C" int fdb_test_parse_uri(int argc, char** argv)    {
  RTL::CLI cli(argc, argv, [](int, char**) {});
  string url;

  cli.getopt("url", 3, url);
  if ( !url.empty() )   {
    uri_t u(url);
    cout << "Protocol:'" << u.protocol << "' ";
    cout << "Host:'"     << u.host     << "' ";
    cout << "Port:'"     << u.port     << "' ";
    cout << "Path:'"     << u.path     << "' ";
    cout << "Query:'"    << u.query    << "' " << endl;
  }
  return 0;
}
//
//==========================================================================
extern "C" int fdb_test_client_get(int argc, char** argv)    {
  Setup  setup("fdb_test_client_get",argc, argv, [](int, char**)  {
    ::fprintf(stderr,
	      "fdb_test_client_get -opt [-opt]                             \n"
	      "     -server=<name:port>    Server access specification.    \n"
	      "     -url=<url-spec>        URL of the file to receive      \n"
	      "     -print=<level>         Set print level 1=DEBUG, 5=FATAL\n");
  });
  uri_t url(setup.server);
  auto   client = client::create<client::sync>(url.host, url.port, 10000, setup.debug);
  auto   reply  = client->get(setup.url);
  reply.content.push_back(0);
  reply.print(cout,"fdb_client_get_test: ");
  return 0;
}
//
//==========================================================================
extern "C" int fdb_test_client_get_next(int argc, char** argv)    {
  Setup  setup("fdb_test_next_get",argc, argv);
  auto   client = setup.client();
  size_t length = 0;
  time_t date = 0;

  auto reply  = client.next_object_delete(setup.url, date, length);
  setup.print(reply).check(reply);
  return 0;
}
//
//==========================================================================
extern "C" int fdb_test_client_save(int argc, char** argv)    {
  Setup  setup("fdb_test_client_save", argc, argv);
  struct stat stat;
  int    ret_stat = ::stat(setup.file.c_str(), &stat);
  if ( -1 == ret_stat )   {
    int err = errno;
    ::lib_rtl_output(LIB_RTL_ERROR,
		     "fdb_test_save: Cannot access file: %s [%s]",
		     setup.file.c_str(), make_error_code(errc(err)).message().c_str());
    return err;
  }
  uri_t url(setup.server);
  auto client = client::create<client::sync>(url.host, url.port, 10000, setup.debug);
  client::reqheaders_t hdrs  {
    { http::constants::content_length, stat.st_size           },
    { http::constants::content_type,   "rawdata/lhcb"         },
    { http::constants::date,           header_t::now()        },
    { http::constants::expect,         "100-continue"         }
  };
  auto reply = client->request(http::constants::put, setup.url, hdrs);
  reply.content.push_back(0);
  reply.print(cout,"fdb_test_client_save: ");
  return 0;
}
