//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Package    : Storage
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_STORAGE_COMMUNICATION_H
#define ONLINE_STORAGE_COMMUNICATION_H

// C/C++ include files
#include <string>
#include <vector>
#include <chrono>
#include <system_error>

// Framework include files. Careful: Do not import BOOST dependencies here!
#include <HTTP/Uri.h>
#include <HTTP/HttpHeader.h>
#include <HTTP/HttpReply.h>

/// Namespace for the http server and client apps
namespace http  {
  class HttpConnection;
}

/// Online Namespace
namespace Online   {

  /// Namespace for the storage implementation
  namespace storage  {

    bool error_check_enable(bool value);
    bool error_code_ok(const std::error_code& ec, bool debug=true);
    typedef http::Uri              uri_t;
    typedef http::HttpConnection   connection_t;
    typedef http::HttpHeader       header_t;
    typedef http::HttpReply        reply_t;
    using   http::to_vector;
  }    // End namespace storage
}      // End namespace Online
#endif // ONLINE_STORAGE_COMMUNICATION_H
