//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Storage
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_STORAGE_FDB_CLIENT_H
#define ONLINE_STORAGE_FDB_CLIENT_H

/// Framework include files
#include <Storage/client.h>

/// C/C++ include files
#include <functional>

/// Online Namespace
namespace Online   {

  /// Namespace for the storage interface
  namespace storage   {

    ///  file database interface implementation
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.02.2021
     */
    class fdb_client   {
    public:
      typedef std::unique_ptr<client>(create_t)(const std::string& host, const std::string& port, int tmo, int dbg);

    public:
      std::function<create_t>  create_client  {client::create<client::sync>};
      std::unique_ptr<client>  fdbclient;
      std::string              fdbserver;
      int                      dataPort;
      int                      dataTMO {1000};
      int                      debug   {0};
      int                      version {0};

    public:
      /// Default constructor
      fdb_client() = delete;
      /// Initializing constructor
      fdb_client(int vsn);
      /// Default MOVE constructor
      fdb_client(fdb_client&& copy) = default;
      /// NO COPY constructor
      fdb_client(const fdb_client& copy) = delete;
      /// Default destructor
      virtual ~fdb_client() = default;
      /// Default MOVE assignment
      fdb_client& operator = (fdb_client&& copy) = default;
      /// NO COPY assignment
      fdb_client& operator = (const fdb_client& copy) = delete;

      /// Save object record to database and return the location to store the data
      reply_t save_object_record(const std::string& location, std::string& url, std::size_t len, const client::reqheaders_t& opts={});
      /// Save object data to disk at the specified location
      reply_t save_object_data(const std::string& location, const void* data, std::size_t len, const client::reqheaders_t& opts={});
      /// Save object data to disk at the specified location
      reply_t save_object(const std::string& location, std::vector<unsigned char>&& data);
      /// Save object data to disk at the specified location
      reply_t save_object(const std::string& location, const void* data, std::size_t length);      

      /// Read object data from the specified location without removing disk object
      reply_t get_object(const std::string& location, std::time_t& date, std::size_t& length);
      /// Read AND Delete object data from disk at the specified location
      reply_t delete_object(const std::string& location, std::time_t& date, std::size_t& length);
      /// Read object data from the specified location without removing disk object
      reply_t next_object_get(std::string& location, std::time_t& date, std::size_t& length);
      /// Read object data from the specified location with removing disk object
      reply_t next_object_delete(std::string& location, std::time_t& date, std::size_t& length);

      /// Helper: Delete the specified object data from the database
      reply_t db_object_delete(const std::string& prefix, std::time_t& date, std::size_t& length);
    };

  }     // End namespace storage
}       // End namespace Online
#endif  // ONLINE_STORAGE_FDB_CLIENT_H
