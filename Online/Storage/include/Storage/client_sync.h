//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_STORAGE_CLIENT_SYNC_H
#define ONLINE_STORAGE_CLIENT_SYNC_H

/// Framework include files
#include <Storage/client.h>

/// C/C++ include files
#include <vector>

/// Online Namespace
namespace Online   {

  /// Namespace for the storage interface
  namespace storage   {

    ///  Concrete storage client class based on HTTP
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    class client_sync : virtual public client   {
    public:
      class io_t;

      std::unique_ptr<io_t> io;
      /// Exec generic statement and receive client http reply header
      reply_t get_reply()  const;
      /// Send data buffer
      reply_t::status_type send(const void* data, size_t len)  const;
      /// Receive data buffer
      reply_t::status_type receive(reply_t& reply)  const;

    public:
      /// NO Default constructor
      client_sync() = delete;
      /// NO Move constructor
      client_sync(client_sync&& copy) = delete;
      /// NO Copy constructor
      client_sync(const client_sync& copy) = delete;
      /// NO move operator
      client_sync& operator=(client_sync&& copy) = delete;
      /// NO assignment operator
      client_sync& operator=(const client_sync& copy) = delete;
      /// Initializing constructor: No need to call open if this constructor is issued
      client_sync(const std::string& host, const std::string& port, int tmo, int dbg);
      /// Default destructor
      virtual ~client_sync();

      /// Set the caller properties
      virtual std::error_code open() const override;

      /// Exec generic statement
      virtual reply_t request(const std::string& cmd, const std::string& url,
			      const reqheaders_t& headers=reqheaders_t()) const override;

      /// Connect client to given URI and execute RPC call
      virtual reply_t get(const std::string& url,
			  const reqheaders_t& headers) const override;

      /// Connect client to given URI and execute RPC call
      virtual reply_t put(const std::string& url,
			  const void* data, size_t len,
			  const reqheaders_t& headers) const override;

      /// Connect client to given URI and execute UPDATE statement
      virtual reply_t update(const std::string& url,
			     const void* data, size_t len,
			     const reqheaders_t& headers)  const  override;

      /// Connect client to given URI and execute DELETE statement
      virtual reply_t del(const std::string& url,
			  const reqheaders_t& headers) const override;
    };

  }     // End namespace storage
}       // End namespace Online
#endif  // RPC_RPC_CLIENT_SYNC_H

