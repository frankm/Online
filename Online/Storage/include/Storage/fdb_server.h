//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Storage
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_STORAGE_FDB_SERVER_H
#define ONLINE_STORAGE_FDB_SERVER_H

/// Framework include files
#include <RTL/rtl.h>
#include <HTTP/HttpServer.h>
#include <Storage/communication.h>

/// C/C++ include files
#include <string>

namespace http   {
  
  template <typename T> class basic_http_server;

  /// file database interface implementation
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.02.2021
   */
  template <typename T> 
  class basic_http_server<T>::handler_t : public HttpRequestHandler  {

  public:
    typedef typename T::traits         traits;
    typedef typename traits::dbase_t   dbase_t;
    typedef typename traits::monitor_t monitor_t;

  public:
    
    /// Reference to the database instance
    std::unique_ptr<dbase_t>   dbase;
    
    /// Reference to the monitoring instance
    std::unique_ptr<monitor_t> monitor;
    
  public:

    /// Standard constructor
    explicit handler_t() = default;
    /// Standard destructor
    virtual ~handler_t();

    /// Specific handler for GET requests
    virtual continue_action handle_get(const request_t& req, reply_t& rep) override  {
      return this->HttpRequestHandler::handle_get(req, rep);
    }
    /// Specific handler for PUT requests
    virtual continue_action handle_put(const request_t& req, reply_t& rep) override  {
      return this->HttpRequestHandler::handle_put(req, rep);
    }
    /// Specific handler for POST requests
    virtual continue_action handle_post(const request_t& req, reply_t& rep) override  {
      return this->HttpRequestHandler::handle_post(req, rep);
    }
    /// Specific handler for POST requests
    virtual continue_action handle_update(const request_t& req, reply_t& rep) override  {
      return this->HttpRequestHandler::handle_post(req, rep);
    }
    /// Specific handler for DELETE requests
    virtual continue_action handle_delete(const request_t& req, reply_t& rep) override  {
      return this->HttpRequestHandler::handle_delete(req, rep);
    }
    /// Handle a request and produce a reply.
    virtual continue_action handle_request(const request_t& req, reply_t& rep) override  {
      return this->HttpRequestHandler::handle_request(req, rep);
    }
    /// Handle possible statistics summaries.
    virtual void handle_stat(const request_t& req, reply_t& rep)  override  {
      this->HttpRequestHandler::handle_stat(req, rep);
    }

  };
}

/// Framework include files
#include <RTL/rtl.h>
#include <RTL/Logger.h>

/// Online Namespace
namespace Online   {

  /// Namespace for the storage interface
  namespace storage   {

    struct fs  {      class traits;    };
    struct db  {      class traits;    };
    using http::basic_http_server;

    /// Create and configure a server instance
    template <typename T> class SrvRun  {
    public:
      std::unique_ptr<http::basic_http_server<T> > ptr;
      std::string local;
      std::string server;
      std::string file_dir;
      std::string dbase;
      size_t      buffer_size       {16*1024};
      int         num_threads       {0};
      int         application_debug {0};
      int         connection_debug  {0};
      bool        startup           {false};
    public:
      void create(int argc, char* argv[])   {
	std::string localhost = RTL::nodeName();
	int         prt = LIB_RTL_INFO;

	local  = localhost+":80";

	RTL::CLI cli(argc, argv, [](int, char**) {});
	startup = cli.getopt("start",  5) != 0;
	cli.getopt("connection_debug", 16, connection_debug);
	cli.getopt("application_debug",17, application_debug);
	cli.getopt("database",          4, dbase);
	cli.getopt("server",            4, server);
	cli.getopt("local",             4, local);
	cli.getopt("threads",           7, num_threads);
	cli.getopt("files",             5, file_dir);
	cli.getopt("print",             4, prt);
	cli.getopt("buffersize",        6, buffer_size);

	RTL::Logger::install_log(RTL::Logger::log_args(prt));
	if ( !file_dir.empty() && 0 != ::chdir(file_dir.c_str()) )    {
	  std::string err = std::make_error_code(std::errc(errno)).message();
	  ::lib_rtl_output(LIB_RTL_ERROR,
			   "+++ Failed to change to working directory: '%s' [%s]",
			   file_dir.c_str(), err.c_str());
	  ::exit(EINVAL);
	}
	std::stringstream str;
	if ( !file_dir.empty() ) str << " File repository: " << file_dir;
	if ( !server.empty()   ) str << " Server URI: "      << server;
	if ( !local.empty()    ) str << " Local URI: "       << local;
	::lib_rtl_output(LIB_RTL_ALWAYS, "+++%s", str.str().c_str());

	uri_t uri(local);
	ptr = std::make_unique<http::basic_http_server<T> >(uri.host, uri.port);
	ptr->implementation->buffer_size = buffer_size;
	ptr->set_debug(connection_debug);
	::lib_rtl_output(LIB_RTL_ALWAYS, "+++ Starting FDB server<%s> on %s:%s with %d threads.",
			 typeid(T).name(),uri.host.c_str(), uri.port.c_str(), num_threads);
      }
      void start()   {
	ptr->start(false, num_threads);
      }
    };
  }     // End namespace storage
}       // End namespace Online
#endif  // ONLINE_STORAGE_FDB_SERVER_H
