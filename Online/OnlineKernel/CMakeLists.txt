#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/OnlineKernel
-------------------
#]=======================================================================]

online_library(OnlineKernel SHARED
        src/RTL/Base64.cpp
        src/RTL/Elf.cpp
        src/RTL/rtl_dirmanip.cpp
        src/MBM/AsciiMonitor.cpp
        src/MBM/AsyncProd.cpp
        src/MBM/Client.cpp
        src/MBM/Consumer.cpp
        src/MBM/Producer.cpp
        src/MBM/SyncProd.cpp
        src/MICFSM/micfsm.cpp)
target_link_libraries(OnlineKernel PUBLIC Online::OnlineBase)
#
# =======================================================================
online_reflex_dictionary(OnlineKernelDictGen
  INCLUDES    ${CMAKE_CURRENT_SOURCE_DIR}/dict/dictionary.h  ${CMAKE_CURRENT_SOURCE_DIR}/dict/PyInteractor.cpp
  SELECTION   ${CMAKE_CURRENT_SOURCE_DIR}/dict/dictionary.xml
  USES        Online::OnlineBase Online::OnlineKernel Python::Python ROOT::Core)

online_library(OnlineKernelDict OnlineKernelDictGen.cxx)
target_link_libraries(OnlineKernelDict Online::OnlineBase Online::OnlineKernel Python::Python ROOT::Core)
#
#===============================================================================
online_install_includes(include)
online_install_python(python)
