//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef SQLDB_ORACLE_INTERFACE_H
#define SQLDB_ORACLE_INTERFACE_H

/// Framework include files
#include <sqldb/sqldb.h>

///  Technology abstraction layer
/**
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class oracle : public sqldb::traits  {
 public:
  typedef struct oracle_db   dbase_t;
  typedef struct oracle_stmt statement_t;
};

#endif // SQLDB_ORACLE_INTERFACE_H
