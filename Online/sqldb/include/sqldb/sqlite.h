//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef SQLDB_SQLITE_INTERFACE_H
#define SQLDB_SQLITE_INTERFACE_H

/// Framework include files
#include <sqldb/sqldb.h>

typedef struct sqlite3      sqlite3;
typedef struct sqlite3_stmt sqlite3_stmt;

///  Technology abstraction layer
/**
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class sqlite : public sqldb::traits  {
 public:
  typedef sqlite3      dbase_t;
  typedef sqlite3_stmt statement_t;
};

#endif // SQLDB_SQLITE_INTERFACE_H
