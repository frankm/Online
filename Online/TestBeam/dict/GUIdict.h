//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
// Disable some diagnostics for ROOT dictionaries
#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#pragma GCC diagnostic ignored "-Wdeprecated"
#pragma GCC diagnostic ignored "-Wunused"
#endif

namespace xmlrpc {}

#include "TestBeam/gui/MessageBoxImp.h"
#include "TestBeam/gui/NodeFSMPanel.h"
#include "TestBeam/gui/OutputWindow.h"
#include "TestBeam/gui/NodeGUI.h"

// -------------------------------------------------------------------------
// Regular dictionaries
// -------------------------------------------------------------------------
#if defined(__CINT__) || defined(__MAKECINT__) || defined(__CLING__) || defined(__ROOTCLING__)
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ namespace testbeam;

#pragma link C++ enum  testbeam::GuiIocCommands;
#pragma link C++ class testbeam::OutputWindow;
#pragma link C++ class testbeam::MessageBoxImp;
#pragma link C++ class testbeam::NodeFSMPanel;
#pragma link C++ class testbeam::NodeGUI;

#endif
