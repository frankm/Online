//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TestBeam-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "TestBeam/gui/NodeFSMPanel.h"
#include "TestBeam/gui/GuiException.h"
#include "TestBeam/gui/GuiCommand.h"
#include "TestBeam/gui/GuiMsg.h"
#include "TestBeam/gui/TaskManager.h"

#include "CPP/TimeSensor.h"
#include "CPP/IocSensor.h"
#include "CPP/Event.h"
#include "RTL/strdef.h"
#include "RTL/rtl.h"
#include "dim/dic.h"
#include "dim/dis.h"

/// ROOT include files
#include "TSystem.h"
#include "TGLabel.h"
#include "TGButton.h"
#include "TGMsgBox.h"
#include "TGTextEntry.h"
#include "TGComboBox.h"
#include "TApplication.h"
#include "TGTableLayout.h"
#include "TGFileDialog.h"

#include <iostream>
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

ClassImp(testbeam::NodeFSMPanel)

using namespace std;
using namespace testbeam;

static constexpr const char* TASK_DEAD    = "DEAD";
static constexpr const char* TASK_RUNNING = "RUNNING";

namespace {
  string guiserv_name()   {
    return "TestBeamGUI_"+RTL::processName();
  }
  string _tmKill()   {
    struct stat stat;
    if ( 0 == ::stat("/opt/FMC/bin/tmKill", &stat) )
      return "LD_LIBRARY_PATH=/opt/FMC/lib /opt/FMC/bin/tmKill";
    if ( 0 == ::stat("/usr/local/bin/tmKill", &stat) )
      return "LD_LIBRARY_PATH=NONE /usr/local/bin/tmKill";
    ::printf("No tmKill command found -- check node setup! Using default: 'tmKill'\n");
    return "tmKill";
  }
  string _tmStart()   {
    struct stat stat;
    if ( 0 == ::stat("/opt/FMC/bin/tmStart", &stat) )
      return "LD_LIBRARY_PATH=/opt/FMC/lib   /opt/FMC/bin/tmStart";
    if ( 0 == ::stat("/usr/local/bin/tmStart", &stat) )
      return "LD_LIBRARY_PATH=NONE /usr/local/bin/tmStart";
    ::printf("No tmStart command found -- check node setup! Using default: 'tmStart'\n");
    return "tmStart";
  }
  string _tmSrv()   {
    struct stat stat;
    if ( 0 == ::stat("/opt/FMC/sbin/tmSrv", &stat) )
      return "-DLD_LIBRARY_PATH=/opt/FMC/lib /opt/FMC/sbin/tmSrv";
    if ( 0 == ::stat("/usr/local/sbin/tmSrv", &stat) )
      return "/usr/local/sbin/tmSrv";
    ::printf("No tmSrv command found -- check node setup! Using default: 'tmSrv'\n");
    return "tmSrv";
  }
  string _selectedText(TGComboBox* c)  {
    TGLBEntry* selected = c->GetSelectedEntry();
    return selected ? selected->GetTitle() : "";
  }
  /// DIM service update handler
  void run_no_update(void* tag, void** buf, int* size, int* first)  {
    NodeFSMPanel* h = *(NodeFSMPanel**)tag;
    if ( *first )  {
    }
    if ( h )  {
      *size = sizeof(h->m_currentRun);
      *buf  = &h->m_currentRun;
    }
  }
  /// DIM command service callback
  void dim_feed(void* tag, void* address, int* size) {
    NodeFSMPanel::dim_info_t* h = *(NodeFSMPanel::dim_info_t**)tag;
    if ( address && *size )    {
      if ( h->fmt == 'A' )
	h->data = (char*)address;
      else
	h->data = "ALIVE";
    }
    else  {
      h->data = TASK_DEAD;
    }
    IocSensor::instance().send(h->recipient,h->replyID, h);
  }
  /// DIM command service callback
  void dim_task_list(void* tag, void* address, int* size) {
    NodeFSMPanel* panel = *(NodeFSMPanel**)tag;
    if ( address && *size )    {
      vector<char>* data = new vector<char>();
      data->reserve(*size+1);
      char* ptr = (char*)address;
      data->insert(data->end(), ptr, ptr + *size);
      /*
      const char* start = (char*)address;
      size_t count = 0;
      size_t len = *size;
      string pid, exe, utgid, state;
      for ( const char* p=start; p<start+len; ++count )   {
	size_t rest = (count%6);
	switch(rest)    {
	case 0:
	  pid     = ::atol(p);
	  break;
	case 1:
	  exe     = p;
	  break;
	case 2:
	  utgid   = p;
	  break;
	case 3:
	  break;
	case 4:
	  state   = p;
	  break;
	case 5:
	  if ( state.find("terminated") == string::npos)  {
	    data->insert(data->end(), utgid.begin(), utgid.end());
	    data->push_back(0);
	  }
	  break;
	default:
	  break;
	}
	p += ::strlen(p)+1;
      }
      */
      IocSensor::instance().send(panel, NodeFSMPanel::TMSRV_LIST, data);
    }
  }
}

/// Standard initializing constructor
NodeFSMPanel::NodeFSMPanel(TGFrame* pParent, CPP::Interactor* g, RTL::CLI& opts)
  : TGCompositeFrame(pParent, 100, 100, kVerticalFrame), gui(g)
{
  m_pid      = ::lib_rtl_pid(); 
  m_ctrlType = CONTROLLER_TYPE;
  opts.getopt("dns",         3, m_DNS);
  opts.getopt("maindns",     3, m_mainDNS);
  opts.getopt("node",        2, m_node);
  opts.getopt("user",        2, m_user);
  opts.getopt("instances",   2, m_numSlaves);
  opts.getopt("runinfo",     2, m_runinfo);
  opts.getopt("partition",   2, m_partition);
  opts.getopt("architecture",2, m_architecture);
  opts.getopt("startup",     2, m_startup);
  opts.getopt("controller",  2, m_ctrlType);
  opts.getopt("ctrl_script", 9, m_ctrlScript);
  opts.getopt("replacements",9, m_replacements);
  m_DNS          = RTL::str_upper(m_DNS);
  m_mainDNS      = RTL::str_upper(m_mainDNS);
  m_node         = RTL::str_upper(m_node);
  m_runinfo      = gSystem->ExpandPathName(m_runinfo.c_str());
  m_architecture = gSystem->ExpandPathName(m_architecture.c_str());
  gClient->GetColorByName("#c0c0c0", disabled);
  gClient->GetColorByName("white",   enabled);
  gClient->GetColorByName("#30B030", green);
  gClient->GetColorByName("#C04040", red);
  gClient->GetColorByName("#4040F0", blue);
  gClient->GetColorByName("#F0F000", yellow);
  gClient->GetColorByName("#C0C000", executing);
  gClient->GetColorByName("#000000", black);
  white       = enabled;
  m_tanSrvCom = new dim_info_t(TANSRV_STATUS, this, 'I');
  m_tmSrvCom  = new dim_info_t(TMSRV_STATUS,  this, 'I');
  m_logSrvCom = new dim_info_t(LOGSRV_STATUS, this, 'I');
  m_ctrlCom   = new dim_info_t(CTRL_STATUS,   this, 'A');
  m_ctrlTasks = new dim_info_t(CTRL_TASKS,    this, 'A');
  cout << "Replacements: " << m_replacements << endl;
}

/// Set the UTGID names according to the parameter settings
void NodeFSMPanel::set_utgid_names()   {
  m_logFifo     = "/tmp/log"+m_partition+".fifo";
  m_tmSrvName   = "/FMC/"+RTL::str_upper(m_node)+"/task_manager";
  //m_logSrvName = "/FMC/"+RTL::str_upper(m_node)+"/logger/"+RTL::str_lower(m_partition);
  m_logSrvName  = "/"+RTL::str_upper(m_node)+"/"+RTL::str_upper(m_partition)+"/LOGS";
  m_mbmmonName  = "/"+RTL::str_upper(m_node)+"/"+RTL::str_upper(m_partition)+"/MBMMon";
  m_mbmdmpName  = "/"+RTL::str_upper(m_node)+"/"+RTL::str_upper(m_partition)+"/Mbmdmp";
  m_storageName = RTL::str_upper(m_partition)+"/Storage";
  m_logViewName = "/"+RTL::str_upper(m_node)+"/"+RTL::str_upper(m_partition)+"/LogViewer";
  m_tanSrvName  = "/"+RTL::str_upper(m_node)+"/"+RTL::str_upper(m_partition)+"/Tan";
  m_didName     = "/"+RTL::str_upper(m_node)+"/"+RTL::str_upper(m_partition)+"/DID";
  m_ctrlName    = m_partition + "_" + m_node + "_Controller";
}

void NodeFSMPanel::init()   {
  string utgid;
  TGGroupFrame* para_group   = new TGGroupFrame(this, "Processing infrastructure");
  para_group->SetLayoutManager(new TGTableLayout(para_group, 11, 9));

  set_utgid_names();
  
  maindns.label  = new TGLabel(    para_group,  "Main DNS:",            MAINDNS_LABEL);
  maindns.input  = new TGTextEntry(para_group,  m_mainDNS.c_str(),      MAINDNS_INPUT);
  maindns.input->SetMaxWidth(100);

  dns.label      = new TGLabel(    para_group,  "Local DNS:",           DNS_LABEL);
  dns.input      = new TGTextEntry(para_group,  m_DNS.c_str(),          DNS_INPUT);
  dns.input->SetMaxWidth(100);

  node.label     = new TGLabel(    para_group,  "Node",                 NODE_LABEL);
  node.input     = new TGTextEntry(para_group,  m_node.c_str(),         NODE_INPUT);

  stringstream str;
  str << m_numSlaves;
  numslave.label = new TGLabel(    para_group,  "Num.slaves",           NUMSLAVE_LABEL);
  numslave.input = new TGTextEntry(para_group,  str.str().c_str(),      NUMSLAVE_INPUT);

  script.label   = new TGLabel(    para_group,  "Startup script",       NUMSLAVE_LABEL);
  script.input   = new TGTextEntry(para_group,  m_ctrlScript.c_str(),   SCRIPT_INPUT);

  arch.label     = new TGLabel(    para_group,  "Architecture",         ARCH_LABEL);
  arch.input     = new TGTextEntry(para_group,  m_architecture.c_str(), ARCH_INPUT);

  part.label     = new TGLabel(    para_group,  "Partition",            PART_LABEL);
  part.input     = new TGTextEntry(para_group,  m_partition.c_str(),    PART_INPUT);
  
  replace.label  = new TGLabel(    para_group,  "Replacements",         REPLACE_LABEL);
  replace.input  = new TGTextEntry(para_group,  m_replacements.c_str(), REPLACE_INPUT);

  str.str("");
  str << m_currentRun;
  runnumber.label  = new TGLabel(    para_group,  "Run number",         RUNNUMBER_LABEL);
  runnumber.input  = new TGTextEntry(para_group, str.str().c_str(),     RUNNUMBER_INPUT);

  arch.input->Connect(     "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "architectureChanged(const char*)");
  script.input->Connect(   "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "scriptChanged(const char*)");
  maindns.input->Connect(  "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "mainDnsChanged(const char*)");
  dns.input->Connect(      "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "dnsChanged(const char*)");
  node.input->Connect(     "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "hostChanged(const char*)");
  numslave.input->Connect( "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "numslaveChanged(const char*)");
  part.input->Connect(     "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "partitionChanged(const char*)");
  replace.input->Connect(  "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "replacementChanged(const char*)");
  runnumber.input->Connect("TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "runnumberChanged(const char*)");

  apply          = new TGTextButton(para_group, "Apply parameters", APPLY_PARAMS);
  apply->Connect("Clicked()", "testbeam::NodeFSMPanel", this, "applyParams()");

  did.label      = new TGLabel(para_group,      "Local DID",    DID_LABEL);
  did.start      = new TGTextButton(para_group, "Start",        DID_START);
  did.kill       = new TGTextButton(para_group, "Kill",         DID_KILL);
  did.start->Connect("Clicked()", "testbeam::NodeFSMPanel", this, "did_start()");
  did.kill->Connect("Clicked()", "testbeam::NodeFSMPanel", this, "did_kill()");

  utgid = m_partition+"_"+m_node+"_Controller";
  ctrl.label     = new TGLabel(     para_group, "Controller",   CTRL_LABEL);
  ctrl.utgid     = new TGTextEntry( para_group, utgid.c_str());
  ctrl.start     = new TGTextButton(para_group, "Start",        CTRL_START);
  ctrl.status    = new TGTextEntry( para_group, TASK_DEAD);
  ctrl.kill      = new TGTextButton(para_group, "Kill",         CTRL_KILL);
  ctrl.start->Connect("Clicked()", "testbeam::NodeFSMPanel", this, "controller_start()");
  ctrl.kill->Connect( "Clicked()", "testbeam::NodeFSMPanel", this, "controller_kill()");
  ctrl.start->SetEnabled(kFALSE);
  ctrl.kill->SetEnabled(kFALSE);

  logSrv.label   = new TGLabel(     para_group, "logSrv",       LOGSRV_LABEL);
  logSrv.utgid   = new TGTextEntry( para_group, m_logSrvName.c_str());
  logSrv.start   = new TGTextButton(para_group, "Start",        LOGSRV_START);
  logSrv.status  = new TGTextEntry( para_group, TASK_DEAD);
  logSrv.kill    = new TGTextButton(para_group, "Kill",         LOGSRV_KILL);
  logSrv.start->Connect("Clicked()", "testbeam::NodeFSMPanel", this, "logSrv_start()");
  logSrv.kill->Connect( "Clicked()", "testbeam::NodeFSMPanel", this, "logSrv_kill()");
  logSrv.start->SetEnabled(kFALSE);
  logSrv.kill->SetEnabled(kFALSE);

  tmSrv.label    = new TGLabel(     para_group, "tmSrv",        TMSRV_LABEL);
  tmSrv.utgid    = new TGTextEntry( para_group, m_tmSrvName.c_str());
  tmSrv.start    = new TGTextButton(para_group, "Start",        TMSRV_START);
  tmSrv.status   = new TGTextEntry( para_group, TASK_DEAD);
  tmSrv.kill     = new TGTextButton(para_group, "Kill",         TMSRV_KILL);
  tmSrv.start->Connect("Clicked()", "testbeam::NodeFSMPanel", this, "tmSrv_start()");
  tmSrv.kill->Connect( "Clicked()", "testbeam::NodeFSMPanel", this, "tmSrv_kill()");
  tmSrv.start->SetEnabled(kFALSE);
  tmSrv.kill->SetEnabled(kFALSE);

  logViewer.label   = new TGLabel(     para_group, "logViewer", LOGVIEWER_LABEL);
  logViewer.utgid   = new TGTextEntry( para_group, m_logViewName.c_str());
  logViewer.start   = new TGTextButton(para_group, "Start",     LOGVIEWER_START);
  logViewer.status  = new TGTextEntry( para_group,TASK_DEAD);
  logViewer.kill    = new TGTextButton(para_group, "Kill",      LOGVIEWER_KILL);
  logViewer.start->Connect("Clicked()", "testbeam::NodeFSMPanel", this, "logViewer_start()");
  logViewer.kill->Connect( "Clicked()", "testbeam::NodeFSMPanel", this, "logViewer_kill()");
  logViewer.start->SetEnabled(kFALSE);
  logViewer.kill->SetEnabled(kFALSE);

  tanSrv.label      = new TGLabel(     para_group, "tanSrv",    TANSRV_LABEL);
  tanSrv.utgid      = new TGTextEntry( para_group, m_tanSrvName.c_str());
  tanSrv.start      = new TGTextButton(para_group, "Start",     TANSRV_START);
  tanSrv.status     = new TGTextEntry( para_group,  TASK_DEAD);
  tanSrv.kill       = new TGTextButton(para_group, "Kill",      TANSRV_KILL);
  tanSrv.start->Connect("Clicked()", "testbeam::NodeFSMPanel", this, "tanSrv_start()");
  tanSrv.kill->Connect( "Clicked()", "testbeam::NodeFSMPanel", this, "tanSrv_kill()");
  tanSrv.start->SetEnabled(kFALSE);
  tanSrv.kill->SetEnabled(kFALSE);

  mbmmon.label      = new TGLabel(     para_group, "mbmmon",    MBMMON_LABEL);
  mbmmon.utgid      = new TGTextEntry( para_group, m_mbmmonName.c_str());
  mbmmon.start      = new TGTextButton(para_group, "Start",     MBMMON_START);
  mbmmon.status     = new TGTextEntry( para_group,  TASK_DEAD);
  mbmmon.kill       = new TGTextButton(para_group, "Kill",      MBMMON_KILL);
  mbmmon.start->Connect("Clicked()", "testbeam::NodeFSMPanel", this, "mbmmon_start()");
  mbmmon.kill->Connect( "Clicked()", "testbeam::NodeFSMPanel", this, "mbmmon_kill()");
  mbmmon.start->SetEnabled(kFALSE);
  mbmmon.kill->SetEnabled(kFALSE);

  mbmdmp.label      = new TGLabel(     para_group, "mbmdmp",    MBMDMP_LABEL);
  mbmdmp.utgid      = new TGTextEntry( para_group, m_mbmdmpName.c_str());
  mbmdmp.start      = new TGTextButton(para_group, "Start",     MBMDMP_START);
  mbmdmp.status     = new TGTextEntry( para_group,  TASK_DEAD);
  mbmdmp.kill       = new TGTextButton(para_group, "Kill",      MBMDMP_KILL);
  mbmdmp.start->Connect("Clicked()", "testbeam::NodeFSMPanel", this, "mbmdmp_start()");
  mbmdmp.kill->Connect( "Clicked()", "testbeam::NodeFSMPanel", this, "mbmdmp_kill()");
  mbmdmp.start->SetEnabled(kFALSE);
  mbmdmp.kill->SetEnabled(kFALSE);

  storage.label     = new TGLabel(     para_group, "storage",   STORAGE_LABEL);
  storage.utgid     = new TGTextEntry( para_group, m_storageName.c_str());
  storage.start     = new TGTextButton(para_group, "Start",     STORAGE_START);
  storage.status    = new TGTextEntry( para_group,  TASK_DEAD);
  storage.kill      = new TGTextButton(para_group, "Kill",      STORAGE_KILL);
  storage.start->Connect("Clicked()", "testbeam::NodeFSMPanel", this, "storage_start()");
  storage.kill->Connect( "Clicked()", "testbeam::NodeFSMPanel", this, "storage_kill()");
  storage.start->SetEnabled(kFALSE);
  storage.kill->SetEnabled(kFALSE);
#define _PAD 0
#define _PADL 15
#define _PADY  5
  para_group->AddFrame(maindns.label,    new TGTableLayoutHints(0, 1, 0, 1, kLHintsLeft|kLHintsCenterY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(maindns.input,    new TGTableLayoutHints(1, 2, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(dns.label,        new TGTableLayoutHints(0, 1, 1, 2, kLHintsLeft|kLHintsCenterY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(dns.input,        new TGTableLayoutHints(1, 2, 1, 2, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(node.label,       new TGTableLayoutHints(0, 1, 2, 3, kLHintsLeft|kLHintsCenterY,_PAD,_PADY,_PADY,_PAD));
  para_group->AddFrame(node.input,       new TGTableLayoutHints(1, 2, 2, 3, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(part.label,       new TGTableLayoutHints(0, 1, 3, 4, kLHintsLeft|kLHintsCenterY,_PAD,_PADY,_PADY,_PAD));
  para_group->AddFrame(part.input,       new TGTableLayoutHints(1, 2, 3, 4, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(numslave.label,   new TGTableLayoutHints(0, 1, 4, 5, kLHintsLeft|kLHintsCenterY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(numslave.input,   new TGTableLayoutHints(1, 2, 4, 5, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(replace.label,    new TGTableLayoutHints(0, 1, 5, 6, kLHintsLeft|kLHintsCenterY,_PAD,_PADY,_PADY,_PAD));
  para_group->AddFrame(replace.input,    new TGTableLayoutHints(1, 2, 5, 6, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));

  para_group->AddFrame(runnumber.label,  new TGTableLayoutHints(0, 1, 6, 7, kLHintsLeft|kLHintsCenterY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(runnumber.input,  new TGTableLayoutHints(1, 5, 6, 7, kLHintsLeft|kLHintsCenterY|kLHintsExpandY|kLHintsExpandX, _PAD,_PAD,_PADY,_PAD));

  para_group->AddFrame(script.label,     new TGTableLayoutHints(0, 1, 8, 9, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(script.input,     new TGTableLayoutHints(1, 5, 8, 9, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(arch.label,       new TGTableLayoutHints(0, 1, 9,10, kLHintsLeft|kLHintsCenterY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(arch.input,       new TGTableLayoutHints(1, 5, 9,10, kLHintsLeft|kLHintsCenterY|kLHintsExpandY|kLHintsExpandX, _PAD,_PAD,_PADY,_PAD));

  para_group->AddFrame(did.label,        new TGTableLayoutHints(5, 6, 8, 9, kLHintsCenterX|kLHintsCenterY|kLHintsExpandY|kLHintsExpandX, _PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(did.start,        new TGTableLayoutHints(6, 7, 8, 9, kLHintsCenterX|kLHintsCenterY|kLHintsExpandY|kLHintsExpandX, _PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(did.kill,         new TGTableLayoutHints(7, 8, 8, 9, kLHintsCenterX|kLHintsCenterY|kLHintsExpandY|kLHintsExpandX, _PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(apply,            new TGTableLayoutHints(5, 8, 9,10, kLHintsCenterX|kLHintsCenterY|kLHintsExpandY|kLHintsExpandX, _PADL,_PAD,_PADY,_PAD));

  para_group->AddFrame(logSrv.label,     new TGTableLayoutHints(3, 4, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(logSrv.utgid,     new TGTableLayoutHints(4, 5, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(logSrv.start,     new TGTableLayoutHints(5, 6, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(logSrv.kill,      new TGTableLayoutHints(6, 7, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(logSrv.status,    new TGTableLayoutHints(7, 9, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 

  para_group->AddFrame(tmSrv.label,      new TGTableLayoutHints(3, 4, 1, 2, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(tmSrv.utgid,      new TGTableLayoutHints(4, 5, 1, 2, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(tmSrv.start,      new TGTableLayoutHints(5, 6, 1, 2, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(tmSrv.kill,       new TGTableLayoutHints(6, 7, 1, 2, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(tmSrv.status,     new TGTableLayoutHints(7, 9, 1, 2, kLHintsLeft|kLHintsCenterY|kLHintsExpandY|kLHintsExpandX, _PADL,_PAD,_PAD,_PAD)); 

  para_group->AddFrame(tanSrv.label,     new TGTableLayoutHints(3, 4, 2, 3, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(tanSrv.utgid,     new TGTableLayoutHints(4, 5, 2, 3, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(tanSrv.start,     new TGTableLayoutHints(5, 6, 2, 3, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(tanSrv.kill,      new TGTableLayoutHints(6, 7, 2, 3, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(tanSrv.status,    new TGTableLayoutHints(7, 9, 2, 3, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 

  para_group->AddFrame(logViewer.label,  new TGTableLayoutHints(3, 4, 3, 4, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(logViewer.utgid,  new TGTableLayoutHints(4, 5, 3, 4, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(logViewer.start,  new TGTableLayoutHints(5, 6, 3, 4, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(logViewer.kill,   new TGTableLayoutHints(6, 7, 3, 4, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(logViewer.status, new TGTableLayoutHints(7, 9, 3, 4, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 

  para_group->AddFrame(mbmmon.label,     new TGTableLayoutHints(3, 4, 4, 5, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(mbmmon.utgid,     new TGTableLayoutHints(4, 5, 4, 5, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(mbmmon.start,     new TGTableLayoutHints(5, 6, 4, 5, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(mbmmon.kill,      new TGTableLayoutHints(6, 7, 4, 5, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(mbmmon.status,    new TGTableLayoutHints(7, 9, 4, 5, kLHintsLeft|kLHintsCenterY|kLHintsExpandY, _PADL,_PAD,_PAD,_PAD)); 

  para_group->AddFrame(mbmdmp.label,     new TGTableLayoutHints(3, 4, 5, 6, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(mbmdmp.utgid,     new TGTableLayoutHints(4, 5, 5, 6, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(mbmdmp.start,     new TGTableLayoutHints(5, 6, 5, 6, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(mbmdmp.kill,      new TGTableLayoutHints(6, 7, 5, 6, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(mbmdmp.status,    new TGTableLayoutHints(7, 9, 5, 6, kLHintsLeft|kLHintsCenterY|kLHintsExpandY, _PADL,_PAD,_PAD,_PAD)); 

  para_group->AddFrame(storage.label,    new TGTableLayoutHints(3, 4, 6, 7, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(storage.utgid,    new TGTableLayoutHints(4, 5, 6, 7, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(storage.start,    new TGTableLayoutHints(5, 6, 6, 7, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(storage.kill,     new TGTableLayoutHints(6, 7, 6, 7, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(storage.status,   new TGTableLayoutHints(7, 9, 6, 7, kLHintsLeft|kLHintsCenterY|kLHintsExpandY, _PADL,_PAD,_PAD,_PAD)); 

  para_group->AddFrame(ctrl.label,       new TGTableLayoutHints(3, 4, 7, 8, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(ctrl.utgid,       new TGTableLayoutHints(4, 5, 7, 8, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(ctrl.start,       new TGTableLayoutHints(5, 6, 7, 8, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(ctrl.kill,        new TGTableLayoutHints(6, 7, 7, 8, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(ctrl.status,      new TGTableLayoutHints(7, 9, 7, 8, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 

  TGGroupFrame* cmds_group = new TGGroupFrame(this, "Commands");
  cmds_group->SetLayoutManager(new TGTableLayout(cmds_group, 2, 9));
  ctrl_label   = new TGLabel(cmds_group, "Controller");
  ctrl_command = new TGComboBox(cmds_group, CONTROLLER_COMMAND);
  ctrl_command->AddEntry("launch",   CONTROLLER_LAUNCH);
  ctrl_command->AddEntry("load",     CONTROLLER_LOAD);
  ctrl_command->AddEntry("configure",CONTROLLER_CONFIG);
  ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
  ctrl_command->AddEntry("unload",   CONTROLLER_UNLOAD);
  ctrl_command->Select(CONTROLLER_LAUNCH);
  ctrl_command->Connect("Selected(Int_t)",  "testbeam::NodeFSMPanel", this, "commandChanged(Int_t)");
  ctrl_killall   = new TGTextButton(cmds_group, "Kill all",    CONTROLLER_KILLALL);
  ctrl_autostart = new TGTextButton(cmds_group, "AutoStart",    CTRL_AUTOSTART);
  ctrl_autostop  = new TGTextButton(cmds_group, "AutoStop",    CTRL_AUTOSTOP);
  ctrl_killall->Connect(   "Clicked()", "testbeam::NodeFSMPanel", this, "kill_all_tasks()");
  ctrl_autostart->Connect( "Clicked()", "testbeam::NodeFSMPanel", this, "autoStart()");
  ctrl_autostop->Connect(  "Clicked()", "testbeam::NodeFSMPanel", this, "autoStop()");

  ctrl_status = new TGTextEntry(cmds_group);
  cmds_group->AddFrame(ctrl_label,    new TGTableLayoutHints(0, 1, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PADL,1,3,1));
  cmds_group->AddFrame(ctrl_command,  new TGTableLayoutHints(1, 2, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PADL,1,3,1));
  cmds_group->AddFrame(ctrl_status,   new TGTableLayoutHints(2, 3, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PADL,1,3,1));
  cmds_group->AddFrame(ctrl_autostart,new TGTableLayoutHints(5, 6, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PADL,1,3,1));
  cmds_group->AddFrame(ctrl_autostop, new TGTableLayoutHints(6, 7, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PADL,1,3,1));
  cmds_group->AddFrame(ctrl_killall,  new TGTableLayoutHints(7, 8, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PADL,1,3,1));
  m_cmds_group = cmds_group;
  TGGroupFrame* child_group = new TGGroupFrame(this, new TGString("Controller's Child States"));
  int num_rows = NUM_ENTRIES_CHILDREN;
  child_group->SetLayoutManager(new TGTableLayout(child_group, (num_rows+2)/2, 6));
  for (int i = 0; i < num_rows; ++i)    {
    Child c;
    int  col = i%2==0 ? 0 : 3;
    int  row = i/2;
    c.id = i;
    c.name  = new TGTextEntry(child_group, row == 0 ? "Process name" : "");
    c.name->SetToolTipText("Child name");
    c.name->SetBackgroundColor(enabled);
    c.name->SetEnabled(kFALSE);
    c.name->SetForegroundColor(black);
    c.name->SetBackgroundColor(row == 0 ? disabled : white);

    c.state = new TGTextEntry(child_group, row == 0 ? "State" : "");
    c.state->SetToolTipText("Child state");
    c.state->SetBackgroundColor(enabled);
    c.state->SetEnabled(kFALSE);
    c.state->SetForegroundColor(black);
    c.state->SetBackgroundColor(row == 0 ? disabled : white);

    c.meta = new TGTextEntry(child_group, row == 0 ? "Meta state" : "");
    c.meta->SetToolTipText("Child meta state");
    c.meta->SetBackgroundColor(enabled);
    c.meta->SetEnabled(kFALSE);
    c.meta->SetForegroundColor(black);
    c.meta->SetBackgroundColor(row == 0 ? disabled : white);

    child_group->AddFrame(c.name,  new TGTableLayoutHints(col,   col+1, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY, 15,1,0,0)); 
    child_group->AddFrame(c.state, new TGTableLayoutHints(col+1, col+2, row, row+1, kLHintsLeft|kLHintsCenterY, 1,5,0,0)); 
    child_group->AddFrame(c.meta,  new TGTableLayoutHints(col+2, col+3, row, row+1, kLHintsLeft|kLHintsCenterY, 1,5,0,0)); 
    children.push_back(c);
  }

  AddFrame(para_group, new TGLayoutHints(kLHintsRight|kLHintsTop|kLHintsExpandX, 1,1,1,1));
  AddFrame(cmds_group, new TGLayoutHints(kLHintsRight|kLHintsTop|kLHintsExpandX, 1,1,1,1));
  AddFrame(child_group, new TGLayoutHints(kLHintsRight|kLHintsTop|kLHintsExpandX|kLHintsExpandY, 1,1,1,1));

  maindns.input->Resize(250,   maindns.input->GetDefaultHeight());
  dns.input->Resize(250,       dns.input->GetDefaultHeight());
  node.input->Resize(250,      node.input->GetDefaultHeight());
  part.input->Resize(250,      part.input->GetDefaultHeight());
  numslave.input->Resize(150,  numslave.input->GetDefaultHeight());
  replace.input->Resize(250,   replace.input->GetDefaultHeight());
  runnumber.input->Resize(150, replace.input->GetDefaultHeight());
  script.input->Resize(600,    script.input->GetDefaultHeight());
  arch.input->Resize(600,      arch.input->GetDefaultHeight());

  double height = tmSrv.kill->GetDefaultHeight();
  tmSrv.kill  ->Resize(120, height);
  tmSrv.start ->Resize(220, height);
  tmSrv.utgid ->Resize(270, height);
  tmSrv.status->Resize(100, height);
  tmSrv.utgid ->SetEnabled(kTRUE);
  tmSrv.status->SetEnabled(kFALSE);
  tmSrv.status->SetBackgroundColor(red);
  tmSrv.status->SetForegroundColor(white);

  height = logSrv.kill->GetDefaultHeight();
  logSrv.kill  ->Resize(120, height);
  logSrv.start ->Resize(220, height);
  logSrv.utgid ->Resize(270, height);
  logSrv.status->Resize(100, height);
  logSrv.utgid ->SetEnabled(kTRUE);
  logSrv.status->SetEnabled(kFALSE);
  logSrv.status->SetBackgroundColor(red);
  logSrv.status->SetForegroundColor(white);

  height = logViewer.kill->GetDefaultHeight();
  logViewer.kill  ->Resize(120, height);
  logViewer.start ->Resize(220, height);
  logViewer.utgid ->Resize(270, height);
  logViewer.status->Resize(100, height);
  logViewer.utgid ->SetEnabled(kTRUE);
  logViewer.status->SetEnabled(kFALSE);
  logViewer.status->SetBackgroundColor(red);
  logViewer.status->SetForegroundColor(white);

  height = tanSrv.kill->GetDefaultHeight();
  tanSrv.kill  ->Resize(120, height);
  tanSrv.start ->Resize(220, height);
  tanSrv.utgid ->Resize(270, height);
  tanSrv.status->Resize(100, height);
  tanSrv.utgid ->SetEnabled(kTRUE);
  tanSrv.status->SetEnabled(kFALSE);
  tanSrv.status->SetBackgroundColor(red);
  tanSrv.status->SetForegroundColor(white);

  height = mbmmon.kill->GetDefaultHeight();
  mbmmon.kill  ->Resize(120, height);
  mbmmon.start ->Resize(220, height);
  mbmmon.utgid ->Resize(270, height);
  mbmmon.status->Resize(100, height);
  mbmmon.utgid ->SetEnabled(kTRUE);
  mbmmon.status->SetEnabled(kFALSE);
  mbmmon.status->SetBackgroundColor(red);
  mbmmon.status->SetForegroundColor(white);

  height = mbmdmp.kill->GetDefaultHeight();
  mbmdmp.kill  ->Resize(120, height);
  mbmdmp.start ->Resize(220, height);
  mbmdmp.utgid ->Resize(270, height);
  mbmdmp.status->Resize(100, height);
  mbmdmp.utgid ->SetEnabled(kTRUE);
  mbmdmp.status->SetEnabled(kFALSE);
  mbmdmp.status->SetBackgroundColor(red);
  mbmdmp.status->SetForegroundColor(white);

  height = storage.kill->GetDefaultHeight();
  storage.kill  ->Resize(120, height);
  storage.start ->Resize(220, height);
  storage.utgid ->Resize(270, height);
  storage.status->Resize(100, height);
  storage.utgid ->SetEnabled(kTRUE);
  storage.status->SetEnabled(kFALSE);
  storage.status->SetBackgroundColor(red);
  storage.status->SetForegroundColor(white);

  height = ctrl.status->GetDefaultHeight();
  ctrl.kill  ->Resize(120, height);
  ctrl.start ->Resize(220, height);
  ctrl.utgid ->Resize(270, height);
  ctrl.status->Resize(100, height);
  ctrl.utgid ->SetEnabled(kTRUE);
  ctrl.status->SetEnabled(kFALSE);
  ctrl.status->SetBackgroundColor(red);
  ctrl.status->SetForegroundColor(white);

  apply->SetMinWidth(350);

  TGFont *font = gClient->GetFont("-*-arial-bold-r-*-*-24-*-*-*-*-*-iso8859-1");
  ctrl_command->Resize(220,20);
  gClient->NeedRedraw(ctrl_command);
  ctrl_status->Resize(300,45);
  ctrl_status->SetFont(font);
  ctrl_status->SetAlignment(kTextCenterX);
  gClient->NeedRedraw(ctrl_status);
  ctrl_killall->Resize(220,20);
  gClient->NeedRedraw(ctrl_killall);
  for (int i = 0; i < num_rows; ++i)    {
    const Child& c = children[i];
    c.name->Resize(240,c.name->GetDefaultHeight()-1);
    c.state->Resize(120,c.state->GetDefaultHeight()-1);
    c.meta->Resize(120,c.state->GetDefaultHeight()-1);
    gClient->NeedRedraw(c.name);
    gClient->NeedRedraw(c.state);
    gClient->NeedRedraw(c.meta);
  }
  ::dis_start_serving(guiserv_name().c_str());
}

/// Default destructor
NodeFSMPanel::~NodeFSMPanel()   {
  if ( 0 != m_runnumber_id      ) ::dis_remove_service(m_runnumber_id);
  if ( 0 != m_storageCom->svcID ) ::dic_release_service(m_storageCom->svcID);
  if ( 0 != m_tanSrvCom->svcID  ) ::dic_release_service(m_tanSrvCom->svcID);
  if ( 0 != m_tmSrvList_id      ) ::dic_release_service(m_tmSrvList_id);
  if ( 0 != m_tmSrvCom->svcID   ) ::dic_release_service(m_tmSrvCom->svcID);
  if ( 0 != m_logSrvCom->svcID  ) ::dic_release_service(m_logSrvCom->svcID);
  if ( 0 != m_ctrlCom->svcID    ) ::dic_release_service(m_ctrlCom->svcID);
  if ( 0 != m_ctrlTasks->svcID  ) ::dic_release_service(m_ctrlTasks->svcID);
  delete m_storageCom;
  delete m_tanSrvCom;
  delete m_tmSrvCom;
  delete m_logSrvCom;
  delete m_ctrlCom;
  delete m_ctrlTasks;
}

void NodeFSMPanel::enableParams()   {
  maindns.input->SetEnabled(kTRUE);
  numslave.input->SetEnabled(kTRUE);
  dns.input->SetEnabled(kTRUE);
  node.input->SetEnabled(kTRUE);
  part.input->SetEnabled(kTRUE);
  replace.input->SetEnabled(kTRUE);
  script.input->SetEnabled(kTRUE);
  arch.input->SetEnabled(kTRUE);

  ctrl.utgid->SetEnabled(kTRUE);
  ctrl.start->SetEnabled(kFALSE);
  ctrl.kill->SetEnabled(kFALSE);

  tmSrv.utgid->SetEnabled(kTRUE);
  tmSrv.start->SetEnabled(kFALSE);
  tmSrv.kill->SetEnabled(kFALSE);

  logSrv.utgid->SetEnabled(kTRUE);
  logSrv.start->SetEnabled(kFALSE);
  logSrv.kill->SetEnabled(kFALSE);

  logViewer.utgid->SetEnabled(kTRUE);
  logViewer.start->SetEnabled(kFALSE);
  logViewer.kill->SetEnabled(kFALSE);

  tanSrv.kill->SetEnabled(kFALSE);
  tanSrv.utgid->SetEnabled(kTRUE);
  tanSrv.start->SetEnabled(kFALSE);

  mbmmon.kill->SetEnabled(kFALSE);
  mbmmon.utgid->SetEnabled(kTRUE);
  mbmmon.start->SetEnabled(kFALSE);

  mbmdmp.kill->SetEnabled(kFALSE);
  mbmdmp.utgid->SetEnabled(kTRUE);
  mbmdmp.start->SetEnabled(kFALSE);

  storage.kill->SetEnabled(kFALSE);
  storage.utgid->SetEnabled(kTRUE);
  storage.start->SetEnabled(kFALSE);
  apply->SetText("Apply parameters");
  if ( 0 != m_runnumber_id     ) ::dis_remove_service(m_runnumber_id);
  m_runnumber_id = 0;
  if ( 0 != m_storageCom->svcID ) ::dic_release_service(m_storageCom->svcID);
  m_storageCom->svcID = 0;
  if ( 0 != m_tanSrvCom->svcID ) ::dic_release_service(m_tanSrvCom->svcID);
  m_tanSrvCom->svcID = 0;
  if ( 0 != m_tmSrvList_id     ) ::dic_release_service(m_tmSrvList_id);
  m_tmSrvList_id = 0;
  if ( 0 != m_tmSrvCom->svcID  ) ::dic_release_service(m_tmSrvCom->svcID);
  m_tmSrvCom->svcID = 0;
  if ( 0 != m_logSrvCom->svcID ) ::dic_release_service(m_logSrvCom->svcID);
  m_logSrvCom->svcID = 0;
  if ( 0 != m_ctrlCom->svcID   ) ::dic_release_service(m_ctrlCom->svcID);
  m_ctrlCom->svcID = 0;
  if ( 0 != m_ctrlTasks->svcID ) ::dic_release_service(m_ctrlTasks->svcID);
  m_ctrlTasks->svcID = 0;
  m_applied = false;
}

void NodeFSMPanel::applyParams()  {
  if ( m_applied )   {
    string c1 = tmSrv.status->GetText();
    string c2 = logSrv.status->GetText();
    string c3 = ctrl.status->GetText();
    if ( c1 == TASK_DEAD && c2 == TASK_DEAD && c3 == TASK_DEAD )   {
      enableParams();
      return;
    }
    GuiException(kMBOk,kMBIconAsterisk,"Invalid Request",
		 "You can only re-edit the parameters\n"
		 "Once all the dependent processes are dead:\n"
		 " -- logSrv \n"
		 " -- tmSrv \n"
		 " -- DAQ controller \n"
		 "This request is ignored.").send(gui);
    return;
  }
  m_applied = true;
  apply->SetText("Edit parameters");
  maindns.input->SetEnabled(kFALSE);
  numslave.input->SetEnabled(kFALSE);
  dns.input->SetEnabled(kFALSE);
  node.input->SetEnabled(kFALSE);
  part.input->SetEnabled(kFALSE);
  replace.input->SetEnabled(kFALSE);
  arch.input->SetEnabled(kFALSE);
  script.input->SetEnabled(kFALSE);

  ctrl.utgid->SetEnabled(kFALSE);
  ctrl.start->SetEnabled(kTRUE);
  ctrl.kill->SetEnabled(kTRUE);

  tmSrv.utgid->SetEnabled(kFALSE);
  tmSrv.start->SetEnabled(kTRUE);
  tmSrv.kill->SetEnabled(kTRUE);

  logSrv.utgid->SetEnabled(kFALSE);
  logSrv.start->SetEnabled(kTRUE);
  logSrv.kill->SetEnabled(kTRUE);

  logViewer.utgid->SetEnabled(kFALSE);
  logViewer.start->SetEnabled(kTRUE);
  logViewer.kill->SetEnabled(kTRUE);

  tanSrv.kill->SetEnabled(kTRUE);
  tanSrv.utgid->SetEnabled(kFALSE);
  tanSrv.start->SetEnabled(kTRUE);

  mbmmon.kill->SetEnabled(kTRUE);
  mbmmon.utgid->SetEnabled(kFALSE);
  mbmmon.start->SetEnabled(kTRUE);

  mbmdmp.kill->SetEnabled(kTRUE);
  mbmdmp.utgid->SetEnabled(kFALSE);
  mbmdmp.start->SetEnabled(kTRUE);

  storage.kill->SetEnabled(kTRUE);
  storage.utgid->SetEnabled(kFALSE);
  storage.start->SetEnabled(kTRUE);
  set_utgid_names();
  if ( 0 != m_runnumber_id     ) ::dis_remove_service(m_runnumber_id);
  if ( 0 != m_tanSrvCom->svcID ) ::dic_release_service(m_tanSrvCom->svcID);
  if ( 0 != m_tmSrvList_id     ) ::dic_release_service(m_tmSrvList_id);
  if ( 0 != m_tmSrvCom->svcID  ) ::dic_release_service(m_tmSrvCom->svcID);
  if ( 0 != m_logSrvCom->svcID ) ::dic_release_service(m_logSrvCom->svcID);
  if ( 0 != m_ctrlCom->svcID   ) ::dic_release_service(m_ctrlCom->svcID);
  if ( 0 != m_ctrlTasks->svcID ) ::dic_release_service(m_ctrlTasks->svcID);
  string svc;
  m_tanSrvCom->svcID = ::dic_info_service((m_tanSrvName+"/VERSION_NUMBER").c_str(), MONITORED,0,0,0,dim_feed,(long)m_tanSrvCom, 0, 0);
  m_tmSrvCom->svcID  = ::dic_info_service((m_tmSrvName+ "/success").c_str(),        MONITORED,0,0,0,dim_feed,(long)m_tmSrvCom, 0, 0);
  m_tmSrvList_id     = ::dic_info_service((m_tmSrvName+ "/list").c_str(),           MONITORED,1,0,0,dim_task_list,(long)this, 0, 0);
  //m_tmSrvList_id   = ::dic_info_service((m_tmSrvName+ "/longList").c_str(),           MONITORED,1,0,0,dim_task_list,(long)this, 0, 0);
  m_logSrvCom->svcID = ::dic_info_service((m_logSrvName+"/VERSION_NUMBER").c_str(), MONITORED,0,0,0,dim_feed,(long)m_logSrvCom, 0, 0);
  svc = m_ctrlName+CONTROLLER_STATUS_OPT;
  m_ctrlCom->svcID   = ::dic_info_service(svc.c_str(), MONITORED,0,0,0,dim_feed,(long)m_ctrlCom, 0, 0);
  m_ctrlTasks->svcID = ::dic_info_service((m_ctrlName+"/tasks").c_str(), MONITORED,1,0,0,dim_feed,(long)m_ctrlTasks, 0, 0);
  m_mainDNS_id       = m_mainDNS.empty() ? -1 : ::dic_add_dns(m_mainDNS.c_str(), ::dim_get_dns_port());
  m_runnumber_id     = ::dis_add_service((m_partition+"/RunInfo/RunNumber").c_str(),"I",0,0,run_no_update,(long)this);
  ::dis_update_service(m_runnumber_id);
  ::dis_start_serving(guiserv_name().c_str());
}

void NodeFSMPanel::updateDependentValues()  {
  string v, value;
  v = "/FMC/" + RTL::str_upper(m_node) + "/" + RTL::str_upper(m_partition);
  if ( tmSrv.utgid )  {
    value = v + "/task_manager";
    m_tmSrvName = value;
    tmSrv.utgid->SetText(value.c_str());
    gClient->NeedRedraw(tmSrv.utgid);
  }
  if ( logSrv.utgid )  {
    value = v + "/LOGS";
    m_logSrvName = value;
    logSrv.utgid->SetText(value.c_str());
    gClient->NeedRedraw(logSrv.utgid);
  }
  v = "/" + RTL::str_upper(m_node) + "/" + RTL::str_upper(m_partition);
  if ( logViewer.utgid )  {
    value = v + "/logViewer";
    m_logViewName = value;
    logViewer.utgid->SetText(value.c_str());
    gClient->NeedRedraw(logViewer.utgid);
  }
  if ( mbmmon.utgid )  {
    value = v + "/MBMMon";
    m_mbmmonName = value;
    mbmmon.utgid->SetText(value.c_str());
    gClient->NeedRedraw(mbmmon.utgid);
  }
  if ( mbmdmp.utgid )  {
    value = v + "/Mbmdmp";
    m_mbmdmpName = value;
    mbmdmp.utgid->SetText(value.c_str());
    gClient->NeedRedraw(mbmdmp.utgid);
  }
  if ( storage.utgid )  {
    value = v + "/Storage";
    m_storageName = value;
    storage.utgid->SetText(value.c_str());
    gClient->NeedRedraw(storage.utgid);
  }
  if ( tanSrv.utgid )  {
    value = v + "/Tan";
    m_tanSrvName = value;
    tanSrv.utgid->SetText(value.c_str());
    gClient->NeedRedraw(tanSrv.utgid);
  }
  if ( ctrl.utgid )  {
    value = m_partition + "_" + m_node + "_Controller";
    m_ctrlName = value;
    ctrl.utgid->SetText(value.c_str());
    gClient->NeedRedraw(ctrl.utgid);
  }
  m_didName     = v + "/DID";
}

void NodeFSMPanel::updateChildren(const string& value)   {
  string data=value, task, name, state, meta;
  size_t idx = 0, idq = data.find('|',0), idd, ide, ichld = 2, stop=0;
  if ( !data.empty() && data != TASK_DEAD )  {
    do   {
      task = data.substr(idx,idq-idx);
      idx = idq + 1;
      idd = task.find('/');
      name  = task.substr(0,idd);
      ide   = task.find('/',idd+1);
      state = task.substr(idd+1, ide-idd-1);
      if ( ide != string::npos ) meta  = task.substr(ide + 1);
      else meta = "";
      if ( ichld < NUM_ENTRIES_CHILDREN )  {
	const Child& c = children[ichld];
	c.name->SetText(name.c_str());
	c.state->SetText(state.c_str());
	c.meta->SetText(meta.c_str());
      }
      ++ichld;
      idq = data.find('|', idx);
      if ( idq == string::npos && stop == 0 ) idq += idx, stop=1;
    }  while( idq != string::npos );
  }
  for(size_t i=ichld; i<NUM_ENTRIES_CHILDREN; ++i)  {
    const Child& c = children[i];
    c.name->SetText("");
    c.state->SetText("");
    c.meta->SetText("");
  }
  for( const auto& c : children )   {
    gClient->NeedRedraw(c.name);
    gClient->NeedRedraw(c.state);
    gClient->NeedRedraw(c.meta);
  }
}

void NodeFSMPanel::updateCtrlCommand(std::string state)   {
  ctrl_command->RemoveAll();
  ctrl_status->SetText(state.c_str());
  ctrl_command->Disconnect("Selected(Int_t)");
  if ( state == TASK_DEAD )   {
    ctrl_command->AddEntry("launch",   CONTROLLER_LAUNCH);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->Select(CONTROLLER_LAUNCH);
    ctrl_status->SetBackgroundColor(blue);
    ctrl_status->SetForegroundColor(white);
    m_autoRun = 0;
  }
  else if ( state == "OFFLINE" )   {
    ctrl_command->AddEntry("load",     CONTROLLER_LOAD);
    ctrl_command->AddEntry("unload",   CONTROLLER_UNLOAD);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->AddEntry("destroy",  CONTROLLER_DESTROY);
    ctrl_command->Select(CONTROLLER_LOAD);
    ctrl_status->SetBackgroundColor(blue);
    ctrl_status->SetForegroundColor(white);
  }
  else if ( state == "NOT_READY" )   {
    ctrl_command->AddEntry("configure",CONTROLLER_CONFIG);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->AddEntry("unload",   CONTROLLER_UNLOAD);
    ctrl_command->AddEntry("destroy",  CONTROLLER_DESTROY);
    ctrl_command->AddEntry("create",   CONTROLLER_CREATE);
    ctrl_command->Select(CONTROLLER_CONFIG);
    ctrl_status->SetBackgroundColor(yellow);
    ctrl_status->SetForegroundColor(black);
  }
  else if ( state == "CREATED" )   {
    ctrl_command->AddEntry("configure",CONTROLLER_CONFIG);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->AddEntry("unload",   CONTROLLER_UNLOAD);
    ctrl_command->AddEntry("destroy",  CONTROLLER_DESTROY);
    ctrl_command->Select(CONTROLLER_CONFIG);
    ctrl_status->SetBackgroundColor(yellow);
    ctrl_status->SetForegroundColor(black);
  }
  else if ( state == "READY" )   {
    ctrl_command->AddEntry("start",    CONTROLLER_START);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->AddEntry("unload",   CONTROLLER_UNLOAD);
    ctrl_command->AddEntry("destroy",  CONTROLLER_DESTROY);
    ctrl_command->Select(CONTROLLER_START);
    ctrl_status->SetBackgroundColor(blue);
    ctrl_status->SetForegroundColor(white);
  }
  else if ( state == "RUNNING" )   {
    ctrl_command->AddEntry("stop",     CONTROLLER_STOP);
    ctrl_command->AddEntry("pause",    CONTROLLER_PAUSE);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->Select(CONTROLLER_STOP);
    ctrl_status->SetBackgroundColor(green);
    ctrl_status->SetForegroundColor(white);
    m_autoRun = 0;
  }
  else if ( state == "PAUSED" )   {
    ctrl_command->AddEntry("continue", CONTROLLER_CONTINUE);
    ctrl_command->AddEntry("stop",     CONTROLLER_STOP);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->AddEntry("pause",    CONTROLLER_PAUSE);
    ctrl_command->Select(CONTROLLER_STOP);
    ctrl_status->SetBackgroundColor(blue);
    ctrl_status->SetForegroundColor(white);
    m_autoRun = 0;
  }
  else if ( state == "ERROR" )   {
    ctrl_command->AddEntry("recover",  CONTROLLER_RECOVER);
    ctrl_command->AddEntry("stop",     CONTROLLER_STOP);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->Select(CONTROLLER_RECOVER);
    ctrl_status->SetBackgroundColor(red);
    ctrl_status->SetForegroundColor(white);
    m_autoRun = 0;
  }
  gClient->NeedRedraw(ctrl_command);
  gClient->NeedRedraw(ctrl_status);
  ctrl_command->Connect("Selected(Int_t)",  "testbeam::NodeFSMPanel", this, "commandChanged(Int_t)");
}

void NodeFSMPanel::controllerCommand(const std::string& cmd)  {
  string dp = m_ctrlName + CONTROLLER_COMMAND_OPT;
  int ret = ::dic_cmnd_service((char*)dp.c_str(),(void*)cmd.c_str(),cmd.length()+1);
  if ( ret == 1 )  {
    ctrl_status->SetText("Executing transition");
    ctrl_status->SetBackgroundColor(executing);
    ctrl_status->SetForegroundColor(black);
    GuiMsg("Successfully sent command: %s->%s",dp.c_str(),cmd.c_str()).send(gui);
    return;
  }
  GuiMsg("FAILED to send command: %s->%s",dp.c_str(),cmd.c_str()).send(gui);
}

/// Update tmSrv status pane
void NodeFSMPanel::updateTaskStatus(TaskEntry& entry, const std::string& state)    {
  if ( !state.empty() )   {
    if ( state == TASK_DEAD )   {
      entry.start->SetEnabled(kTRUE);
      entry.kill->SetEnabled(kFALSE);
      entry.status->SetText(TASK_DEAD);
      entry.status->SetBackgroundColor(red);
      IocSensor::instance().send(this, CHECK_ENABLE_PARAM, this);
    }
    else  {
      entry.start->SetEnabled(kFALSE);
      entry.kill->SetEnabled(kTRUE);
      entry.status->SetText(TASK_RUNNING);
      entry.status->SetBackgroundColor(green);
    }
  }
  gClient->NeedRedraw(entry.status);
}

void NodeFSMPanel::commandChanged(int value)   {
  string sel = _selectedText(ctrl_command);
  GuiMsg("Received command request [%d]: %s", value, sel.c_str()).send(gui);
  if ( sel == "launch" )
    controller_start();
  else
    controllerCommand(sel);
}

string NodeFSMPanel::killDefaults()  const   {
  string def = "-s 9 ";
  return def;
}

string NodeFSMPanel::startDefaults()  const   {
  string def = "-E " + m_logFifo + " -O " + m_logFifo + " -n " + m_user + " ";
  return def;
}

/// Get the controller running in auto mode
void NodeFSMPanel::autoStart()   {
  string state = ctrl_status->GetText();
  GuiMsg("Check autostart in state: %s",state.c_str()).send(gui);
  m_autoRun = CTRL_AUTOSTART;
  if ( state == "DEAD" )
    controller_start();
  else if ( state == "OFFLINE" )
    controllerCommand("load");
  else if ( state == "NOT_READY" )
    controllerCommand("configure");
  else if ( state == "READY" )
    controllerCommand("start");
  else if ( state == "RUNNING" )
    m_autoRun = 0;
}

/// Get the controller offline in auto mode
void NodeFSMPanel::autoStop()   {
  string state = ctrl_status->GetText();
  GuiMsg("Check autostart in state: %s",state.c_str()).send(gui);
  m_autoRun = CTRL_AUTOSTOP;
  if ( state == "DEAD" )
    m_autoRun = 0;
  else if ( state == "OFFLINE" )
    controllerCommand("destroy");
  else if ( state == "NOT_READY" )
    controllerCommand("unload");
  else if ( state == "READY" )
    controllerCommand("reset");
  else if ( state == "RUNNING" )
    controllerCommand("stop");
}

/// Start tmSrv process
void NodeFSMPanel::tmSrv_start()   {
  if ( m_tmSrvCom->data == TASK_DEAD )   {
    stringstream cmd;
    if ( m_mainDNS.empty() )  {
      cmd << "export DIM_DNS_NODE=" << m_node << ";export UTGID=" << m_tmSrvName << ";"
	  << _tmSrv() << " -l 2 -N " << m_node << " -p 2 -u online -U root --no-auth --no-ptrace-workaround&";
      system(cmd.str().c_str());
    }
    else   {
      cmd << _tmStart() << " -N " << m_mainDNS << " -m " << m_node << " " << startDefaults()
	  << " -n root -g root -DDIM_DNS_NODE=" << m_node << " -u " << m_tmSrvName << " "
	  << _tmSrv() << " -l 2 -N " << m_node << " -p 2 -u online -U root --no-auth --no-ptrace-workaround";
      system(cmd.str().c_str());
    }
    return;
  }
  GuiException(kMBOk,kMBIconAsterisk,"Invalid Request",
	       "The task manager is already running.\n"
	       "This request is ignored.").send(gui);
}

/// Kill tmSrv process
void NodeFSMPanel::tmSrv_kill()   {
  string cmd;
  if ( m_mainDNS.empty() )   {
    cmd = "pkill -9 tmSrv";
    system(cmd.c_str());
    return;
  }
  cmd = _tmKill() + " -N "+m_mainDNS+" -m "+m_node+" " + killDefaults() + m_tmSrvName;
  system(cmd.c_str());
  //FiniteStateMachine::TaskManager(m_node, m_mainDNS_id).kill(m_tmSrvName, 9);
}

/// Start logSrv process
void NodeFSMPanel::logSrv_start()   {
  if ( m_logSrvCom->data == TASK_DEAD )   {
    stringstream cmd;
    if ( m_mainDNS.empty() )   {
      cmd << "export DIM_DNS_NODE="  << m_node    << "; "
	  << "export UTGID="         << m_logSrvName << ";";
      cmd << "export LOGFIFO="       << m_logFifo << "; "
	  << "export DIM_HOST_NODE=" << m_node    << "; ";
      cmd << "/group/online/dataflow/scripts/NodeLogger.sh&";
      system(cmd.str().c_str());
    }
    else   {
      cmd << _tmStart() << " -N " << m_mainDNS << " -m " << m_node 
	<< " -e -o -n online -DDIM_DNS_NODE=" << m_node << " -u " << m_logSrvName << " "
	<< "-DLOGFIFO="         << m_logFifo   << " "
	<< "-DDIM_HOST_NODE="   << m_node      << " "
	<< "-DPUBLISH_SERVICE=" << m_partition << "/LOGS "
	<< "/group/online/dataflow/scripts/NodeLogger.sh";
      system(cmd.str().c_str());
    }
    return;
  }
  GuiException(kMBOk,kMBIconAsterisk,"Invalid Request",
	       "The log server is already running.\n"
	       "This request is ignored.").send(gui);
}

/// Kill logSrv process
void NodeFSMPanel::logSrv_kill()   {
  string cmd;
  if ( m_mainDNS.empty() )
    cmd = "pkill -9 logSrv";
  else
    cmd = _tmKill() + " -N "+m_mainDNS+" -m "+m_node+" " + killDefaults() + m_logSrvName;
  system(cmd.c_str());
}

/// Start controller process
void NodeFSMPanel::controller_start()   {
  string state = m_ctrlCom->data;
  if ( state == TASK_DEAD )   {
    stringstream cmd;
    if ( m_startup.empty() )   {
      cmd << startDefaults();
      if ( ::getenv("BINARY_TAG") )   {
	cmd << " -DBINARY_TAG=" << ::getenv("BINARY_TAG");
      }
      cmd << " -u "         << m_ctrlName
	  << "   "          << m_ctrlScript
	  << " -type="      << m_ctrlType
	  << " -partition=" << m_partition
	  << " -taskinfo="  << m_architecture
	  << " -count="     << m_numSlaves
	  << " -runinfo="   << m_runinfo
	  << " -logfifo="   << m_logFifo
	  << " -smidns="    << m_DNS
	  << " -tmsdns="    << m_DNS
	  << " -dimdns="    << m_DNS
	  << " -replacements=NUMBER_OF_SLAVES:" << m_numSlaves;
      if ( !m_replacements.empty() )   {
	cmd << "," << m_replacements;
      }
    }
    else   {
      cmd << startDefaults() << " -u " << m_ctrlName << " " << m_startup;
    }
    startProcess(cmd.str());
    return;
  }
  GuiException(kMBOk,kMBIconAsterisk,"Invalid Request",
	       "The controller is already running.\n"
	       "Current state: %s\n"
	       "This request is ignored.", state.c_str()).send(gui);
}

/// Kill controller process
void NodeFSMPanel::controller_kill()   {
  string cmd = killDefaults() + m_ctrlName;
  killProcess(cmd);
}

/// Start tanSrv process
void NodeFSMPanel::tanSrv_start()   {
  stringstream cmd;
  cmd << "xterm -title \"" << m_tanSrvName << "\" -geo 132x12 -e \""
      << "export UTGID="   << m_tanSrvName << "; "
      << "exec -a TanServer gentest libOnlineBase.so tan_nameserver -a -tcp -d -m"
      << "\"&";
  system(cmd.str().c_str());
  ::printf("%s\n",cmd.str().c_str());
}

/// Kill tanSrv process
void NodeFSMPanel::tanSrv_kill()   {
  stringstream cmd;
  cmd << killDefaults() << m_tanSrvName;
  killProcess(cmd.str());
}

/// Start mbmmon process
void NodeFSMPanel::mbmmon_start()   {
  stringstream cmd;
  cmd << "export UTGID=" << m_mbmmonName << ";"
      << "xterm  -ls -132 -geometry 132x65 -title \"" << m_mbmmonName << "\" -e \""
      << "exec -a mbmmon gentest libOnlineKernel.so mbm_mon_scr -p=" << m_partition
      << "\"&";
  system(cmd.str().c_str());
  ::printf("%s\n",cmd.str().c_str());
}

/// Kill mbmmon process
void NodeFSMPanel::mbmmon_kill()   {
  stringstream cmd;
  cmd << killDefaults() << m_mbmmonName;
  killProcess(cmd.str());
}

/// Start mbmdmp process
void NodeFSMPanel::mbmdmp_start()   {
  stringstream cmd;
  cmd << "export UTGID=" << m_mbmdmpName << ";"
      << "xterm  -ls -132 -geometry 132x35 -title \"" << m_mbmdmpName << "\" -e \""
      << "exec -a mbm_dump mbm_dump"
      << " -name="      << "mbm_dump"
      << " -buffer="    << "Output_" << m_partition
      << " -type="      << "MDF"
      << " -partition=" << m_partition
      << "\"&";
  system(cmd.str().c_str());
  ::printf("%s\n",cmd.str().c_str());
}

/// Kill mbmdmp process
void NodeFSMPanel::mbmdmp_kill()   {
  stringstream cmd;
  cmd << killDefaults() << m_mbmdmpName;
  killProcess(cmd.str());
}

/// Start storage process
void NodeFSMPanel::storage_start()   {
  stringstream cmd;
#if 0
  cmd << "export UTGID=" << m_storageName << ";"
      << "xterm  -ls -132 -geometry 132x35 -title \"" << m_storageName << "\" -e \""
      << "export UTGID="   << m_storageName << "; "
      << "exec -a StorageFS genRunner libStorageServer.so fdb_fs_file_server "
      << "-local=0.0.0.0:8100 "
      << "-server=xxeb09:8000 "
      << "-files=/daqarea1/objects "
      << "-logger=" << m_logFifo << " "
      << "-threads=6 -print=3 "
      << "\"&";
  system(cmd.str().c_str());
#endif
  cmd << startDefaults()
      << " -u "              << m_storageName
      << " -D LOGFIFO="      << m_logFifo
      << " -D DIM_DNS_NODE=" << m_DNS
      << " /group/online/dataflow/scripts/StorageFS.sh";
  startProcess(cmd.str());
  ::printf("%s\n",cmd.str().c_str());
}

/// Kill storage process
void NodeFSMPanel::storage_kill()   {
  stringstream cmd;
  cmd << killDefaults() << m_storageName;
  killProcess(cmd.str());
}

/// Start local DID process
void NodeFSMPanel::did_start()   {
  stringstream cmd;
  cmd << "export UTGID=" << m_didName << "; did -dns=" << m_DNS << "&";
  system(cmd.str().c_str());
}

/// Kill did process
void NodeFSMPanel::did_kill()   {
  stringstream cmd;
  cmd << killDefaults() << m_didName;
  killProcess(cmd.str());
}

/// Start logViewer process
void NodeFSMPanel::logViewer_start()   {
  stringstream cmd;
  cmd << "xterm -sl 20000 -ls -132 -geometry 230x50 -title \"" << m_logViewName << "\" "
      << "-e \"export UTGID=" << m_logViewName << "; "
      << "/group/online/dataflow/scripts/log_viewer.sh -C -N"
      << " -d "      << m_node
      << " -broker " << m_node
      << " -m "      << m_node
      << "\"&";
  ::printf("%s\n",cmd.str().c_str());
  system(cmd.str().c_str());
}
//dis_update_service gentest libROLogSrv.so run_output_logger -C -N -d $HOST -b  $HOST -m $HOST
//exec -a LogViewer_22344 gentest libROLogSrv.so run_output_logger -C -N -b pluscc04 -O 1 -S (.*)/LOGS -S pluscc04


/// Kill logViewer process
void NodeFSMPanel::logViewer_kill()   {
  stringstream cmd;
  cmd << killDefaults() << m_logViewName;
  killProcess(cmd.str());
}

void NodeFSMPanel::runnumberChanged(const char* value)   {
  string runno = value;
  int run = 0;
  if ( 1 == ::sscanf(runno.c_str(),"%d",&run) )  {
    m_currentRun = run;
    ::dis_update_service(m_runnumber_id);
    return;
  }
  GuiException(kMBOk,kMBIconAsterisk,"Invalid Value",
	       "The given run number is invalid.\n"
	       "Current run is: %d\n", m_currentRun).send(gui);
}

void NodeFSMPanel::setArchitecture(const std::string& value)   {
  m_architecture = value;
  if ( arch.input )  {
    arch.input->SetText(m_architecture.c_str());
    gClient->NeedRedraw(arch.input);
  }
  updateDependentValues();
}

void NodeFSMPanel::architectureChanged(const char* value)   {
  m_architecture = value;
}

void NodeFSMPanel::setScript(const std::string& value)   {
  m_ctrlScript = value;
  if ( script.input )  {
    script.input->SetText(m_ctrlScript.c_str());
    gClient->NeedRedraw(script.input);
  }
  updateDependentValues();
}

void NodeFSMPanel::scriptChanged(const char* value)   {
  m_ctrlScript = value;
}

void NodeFSMPanel::setPartition(const std::string& value)   {
  m_partition = value;
  if ( part.input )  {
    part.input->SetText(m_partition.c_str());
    gClient->NeedRedraw(part.input);
  }
  updateDependentValues();
}

void NodeFSMPanel::partitionChanged(const char* value)   {
  m_partition = value;
  updateDependentValues();
  if ( 0 != m_runnumber_id ) ::dis_remove_service(m_runnumber_id);
  m_runnumber_id = ::dis_add_service((m_partition+"/RunInfo/RunNumber").c_str(),"I",0,0,run_no_update,(long)this);
  ::dis_start_serving(guiserv_name().c_str());
  ::dis_update_service(m_runnumber_id);
}

void NodeFSMPanel::setReplacement(const std::string& value)   {
  m_replacements = value;
  if ( part.input )  {
    part.input->SetText(m_replacements.c_str());
    gClient->NeedRedraw(part.input);
  }
  updateDependentValues();
}

void NodeFSMPanel::replacementChanged(const char* value)   {
  m_replacements = value;
  updateDependentValues();
}

void NodeFSMPanel::setHost(const std::string& value)   {
  m_node = value;
  if ( node.input )  {
    node.input->SetText(m_node.c_str());
    gClient->NeedRedraw(node.input);
  }
  updateDependentValues();
}

void NodeFSMPanel::hostChanged(const char* value)   {
  m_node = value;
  updateDependentValues();
}

void NodeFSMPanel::setDns(const std::string& value)   {
  m_DNS = value;
  if ( dns.input )   {
    dns.input->SetText(m_DNS.c_str());
    gClient->NeedRedraw(dns.input);
  }
  updateDependentValues();
}

void NodeFSMPanel::dnsChanged(const char* value)   {
  m_DNS = value;
}

void NodeFSMPanel::setMainDns(const std::string& value)   {
  m_mainDNS = value;
  if ( maindns.input )   {
    maindns.input->SetText(m_mainDNS.c_str());
    gClient->NeedRedraw(maindns.input);
  }
  updateDependentValues();
}

void NodeFSMPanel::mainDnsChanged(const char* value)    {
  m_mainDNS = value;
}

void NodeFSMPanel::setRunInfo(const std::string& value)   {
  m_runinfo = value;
}

void NodeFSMPanel::setNumberOfSlaves(int value)   {
  m_numSlaves = value;
  if ( numslave.input )  {
    stringstream str;
    str << m_numSlaves;
    numslave.input->Disconnect("TextChanged(const char*)");
    numslave.input->SetText(str.str().c_str());
    gClient->NeedRedraw(numslave.input);
    numslave.input->Connect("TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "numslaveChanged(const char*)");
  }
}

void NodeFSMPanel::setStartup(const std::string& value)   {
  m_startup = value;
}

void NodeFSMPanel::setUser(const std::string& value)   {
  m_user = value;
  updateDependentValues();
}

void NodeFSMPanel::numslaveChanged(const char* value)    {
  ::sscanf(value,"%ld",&m_numSlaves);
}

void NodeFSMPanel::kill_all_tasks()   {
  killProcess(killDefaults()+" "+m_partition+"_"+m_node+"_*");
  //logViewer_kill();
  //tmSrv_kill();
  //logSrv_kill();
}

void NodeFSMPanel::startProcess(const std::string& args)   {
  string dp = "/FMC/"+RTL::str_upper(m_node)+"/task_manager/start";
  int ret = ::dic_cmnd_service(dp.c_str(),(char*)args.c_str(),args.length()+1);
  if ( ret == 1 )  {
    GuiMsg("Successfully started process: %s->%s",dp.c_str(),args.c_str()).send(gui);
    return;
  }
  GuiMsg("FAILED to start process: %s->%s",dp.c_str(),args.c_str()).send(gui);
}

void NodeFSMPanel::killProcess(const std::string& args)   {
  string dp = "/FMC/"+RTL::str_upper(m_node)+"/task_manager/kill";
  int ret = ::dic_cmnd_service(dp.c_str(),(void*)args.c_str(),args.length()+1);
  if ( ret == 1 )  {
    GuiMsg("Successfully killed process: %s->%s",dp.c_str(),args.c_str()).send(gui);
    return;
  }
  GuiMsg("FAILED to kill process: %s->%s",dp.c_str(),args.c_str()).send(gui);
}

/// Interactor interrupt handler callback
void NodeFSMPanel::handle(const CPP::Event& ev)   {
  dim_info_t* r = 0;
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type) {
    case TMSRV_LIST:   {
      lock_guard<mutex> lock(dim_protection);
      auto* data = ev.iocPtr<vector<char> >();
      const char* tasks = &data->at(0);
      size_t len = data->size();
      map<string, std::pair<TaskEntry*, const char*> > status;
      status[m_tanSrvName]  = make_pair(&tanSrv,    TASK_DEAD);
      status[m_logViewName] = make_pair(&logViewer, TASK_DEAD);
      status[m_mbmmonName]  = make_pair(&mbmmon,    TASK_DEAD);
      status[m_mbmdmpName]  = make_pair(&mbmdmp,    TASK_DEAD);
      status[m_storageName] = make_pair(&storage,   TASK_DEAD);
      for(const char* t = tasks; t != 0 && t<tasks+len; )   {
	auto i = status.find(t);
	if ( i != status.end() )
	  (*i).second.second = TASK_RUNNING;
	t = ::strchr(t, '\0');
	if ( t ) ++t;
      }
      for ( const auto& s : status )   {
	updateTaskStatus(*(s.second.first), s.second.second);
      }
      delete data;
      break;
    }
    case LOGSRV_STATUS:   {
      lock_guard<mutex> lock(dim_protection);
      r = ev.iocPtr<dim_info_t>();
      if ( r ) updateTaskStatus(logSrv, r->data);
      break;
    }
    case TMSRV_STATUS:   {
      lock_guard<mutex> lock(dim_protection);
      r = ev.iocPtr<dim_info_t>();
      if ( r ) updateTaskStatus(tmSrv, r->data);
      break;
    }
    case TANSRV_STATUS:    {
      lock_guard<mutex> lock(dim_protection);
      r = ev.iocPtr<dim_info_t>();
      if ( r ) updateTaskStatus(tanSrv, r->data);
      break;
    }
    case STORAGE_STATUS:    {
      lock_guard<mutex> lock(dim_protection);
      r = ev.iocPtr<dim_info_t>();
      if ( r ) updateTaskStatus(storage, r->data);
      break;
    }
    case CTRL_STATUS:    {
      lock_guard<mutex> lock(dim_protection);
      r = ev.iocPtr<dim_info_t>();
      GuiMsg("New Controller status: %s",r->data.c_str()).send(gui);
      if ( r )   {
	updateTaskStatus(ctrl, r->data);
	if ( m_autoRun && r->data != TASK_DEAD )
	  IocSensor::instance().send(this, m_autoRun, this);
	updateCtrlCommand(r->data);
      }
      break;
    }
    case CTRL_AUTOSTART:
      autoStart();
      break;

    case CTRL_AUTOSTOP:
      autoStop();
      break;

    case CHECK_ENABLE_PARAM:
      break;

    case CTRL_TASKS:    {
      lock_guard<mutex> lock(dim_protection);
      r = ev.iocPtr<dim_info_t>();
      updateChildren(r->data);
      break;
    }
    default:
      break;
    }
    break;
  }
  case TimeEvent: {
    long cmd = long(ev.timer_data);
    switch(cmd) {
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
}
