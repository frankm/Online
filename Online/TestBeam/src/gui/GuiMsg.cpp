//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TestBeam-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "TestBeam/gui/GuiMsg.h"
#include "TestBeam/gui/GuiCommand.h"
#include "CPP/IocSensor.h"

/// C++ include files
#include <string>
#include <cstdarg>

using namespace testbeam;

/// Initializing constructor
GuiMsg::GuiMsg(const char* fmt, ...)
  : std::unique_ptr<std::string>(new std::string())
{
  char text[4096];
  va_list args;
  va_start(args, fmt);
  ::vsnprintf(text,sizeof(text),fmt,args);
  va_end(args);
  *(this->std::unique_ptr<std::string>::get()) = text;
}

/// Send message to display target
void GuiMsg::send(CPP::Interactor* target)  const  {
  if ( target )   {
    GuiMsg* m = const_cast<GuiMsg*>(this);
    std::string* s = m->release();
    if ( !s->empty() )   {
      IocSensor::instance().send(target,GUI_SHOW_OUTPUTLINE,s);
    }
  }
}

/// Access the string behind
std::string* GuiMsg::release(){
  return this->std::unique_ptr<std::string>::release();
}

/// Access the string behind
std::string* GuiMsg::get(){
  return this->std::unique_ptr<std::string>::get();
}

/// Set the string to a new value
GuiMsg& GuiMsg::str(const std::string& value){
  *(this->std::unique_ptr<std::string>::get()) = value;
  return *this;
}

/// Set the string to a new value
GuiMsg& GuiMsg::str(const char* fmt, ...){
  char text[4096];
  va_list args;
  va_start(args, fmt);
  ::vsnprintf(text,sizeof(text),fmt,args);
  *(this->std::unique_ptr<std::string>::get()) = text;
  va_end(args);
  return *this;
}

/// Access the string behind
std::string& GuiMsg::str(){
  return *(this->std::unique_ptr<std::string>::get());
}

/// Access the string behind
const std::string& GuiMsg::str()  const{
  return *(this->std::unique_ptr<std::string>::get());
}

