//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TestBeam-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "TestBeam/gui/NodeGUI.h"
#include "TestBeam/gui/NodeFSMPanel.h"
#include "TestBeam/gui/GuiException.h"
#include "TestBeam/gui/GuiCommand.h"
#include "TestBeam/gui/GuiMsg.h"

#include "TestBeam/gui/MessageBox.h"
#include "TestBeam/gui/OutputWindow.h"
#include "CPP/IocSensor.h"
#include "CPP/Event.h"
#include "RTL/rtl.h"
#include "RTL/strdef.h"

/// ROOT include files
#include "TGTab.h"
#include "TGMenu.h"
#include "TApplication.h"

ClassImp(testbeam::NodeGUI)

using namespace std;
using namespace testbeam;

/// Standard Initializing constructor
NodeGUI::NodeGUI(TGFrame* parent, RTL::CLI& opts)
  : TGCompositeFrame(parent, 100, 100, kVerticalFrame), CPP::Interactor()

{
  TGCompositeFrame *tab_frame;
  tabs = new TGTab(this, 600, 900);
  tab_frame = tabs->AddTab("Connection");
  nodeControl = new NodeFSMPanel(tab_frame, this, opts);
  nodeControl->init();

  tab_frame->AddFrame(nodeControl, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,1,1,1,1));

  output = new OutputWindow(this);
  AddFrame(tabs,   new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,1,1,1,1));
  AddFrame(output, new TGLayoutHints(kLHintsCenterX|kLHintsBottom|kLHintsExpandX,1,1,1,1));
  Connect("CloseWindow()", "testbeam::NodeGUI", this, "CloseWindow()");
  tabs->Resize(tabs->GetDefaultWidth(),550);
}

/// Default destructor
NodeGUI::~NodeGUI()   {
  if ( menuFile    ) delete menuFile;
  if ( menuHelp    ) delete menuHelp;
  if ( guiMenuBar  ) delete guiMenuBar;
  if ( nodeControl ) delete nodeControl;
  if ( output      ) delete output;
}

/// Interactor interrupt handler callback
void NodeGUI::handle(const CPP::Event& ev)    {
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type) {
    case GUI_SHOW_OUTPUTLINE:
      IocSensor::instance().send(output,ev.type,ev.data); 
      break;
    case GUI_UPDATE_OUTPUT:
      IocSensor::instance().send(output,ev.type,ev.data); 
      break;
    case GUI_EXCEPTION:  {
      unique_ptr<GuiException> e(ev.iocPtr<GuiException>());
      //GuiMsg("+======================================================================").send(this);
      //GuiMsg("|   GUI Exception: %s",e->title.c_str()).send(this);
      //GuiMsg("|   %s",e->msg.c_str()).send(this);
      //GuiMsg("+======================================================================").send(this);
      Int_t retval = 0;
      MessageBox(gClient->GetRoot(), GetParent(),
		 e->title.c_str(), e->msg.c_str(),
		 e->icon, e->buttons, &retval);
      break;
    }
    default:
      break;
    }
    return;
  }
  case TimeEvent: {
    long cmd = long(ev.timer_data);
    switch(cmd) {
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
  GuiMsg("NodeGUI::handle..Unhandled Event... %d\n",ev.eventtype).send(this);
}

/// Create a menu bar and add it to the parent
TGMenuBar* NodeGUI::menuBar()   {
  if ( 0 == guiMenuBar )  {
    TGWindow* win = const_cast<TGWindow*>(GetMainFrame());
    TGCompositeFrame* frame = dynamic_cast<TGCompositeFrame*>(win);
    guiMenuBar = new TGMenuBar(GetParent(), 1, 1, kHorizontalFrame);
    frame->AddFrame(guiMenuBar, new TGLayoutHints(kLHintsTop | kLHintsExpandX));
  }
  return guiMenuBar;
}

/// Create the menues of the application
void NodeGUI::createMenus(TGMenuBar* bar)   {
  TGPicturePool* p = gClient->GetPicturePool();
  // Create menubar and popup menus. 
  menuFile = new TGPopupMenu(gClient->GetRoot());
  menuFile->AddEntry(new TGHotString("&Open...\tCTRL-O"),        M_FILE_OPEN,   0, p->GetPicture("open.xpm"));
  menuFile->AddEntry(new TGHotString("&Save\tCTRL-S"),           M_FILE_SAVE,   0, p->GetPicture("save.xpm"));
  menuFile->AddEntry(new TGHotString("S&ave as...\tCTRL-a"),     M_FILE_SAVEAS, 0, p->GetPicture("filesaveas.xpm"));
  menuFile->AddEntry(new TGHotString("&Close\tCTRL-C"),          M_FILE_CLOSE,  0, p->GetPicture("ed_delete.png"));
  menuFile->AddSeparator();
  menuFile->AddEntry(new TGHotString("&Print\tCTRL-P"),          M_FILE_PRINT,  0, p->GetPicture("ed_print.png"));
  menuFile->AddEntry(new TGHotString("P&rint setup\tCTRL-r"),    M_FILE_PRINTSETUP, 0, p->GetPicture("ed_print.png"));
  menuFile->AddSeparator();
  menuFile->AddEntry(new TGHotString("E&xit\tCTRL-x"),           M_FILE_EXIT,   0, p->GetPicture("ed_delete.png"));

  menuHelp = new TGPopupMenu(gClient->GetRoot());
  menuHelp->AddEntry(new TGHotString("&Contents\tCTRL-C"),       M_HELP_CONTENTS, 0, p->GetPicture("ed_help.png"));
  menuHelp->AddEntry(new TGHotString("&Search...\tCTRL-S"),      M_HELP_SEARCH, 0, p->GetPicture("ed_find.png"));
  menuHelp->AddSeparator();
  menuHelp->AddEntry(new TGHotString("&About\tCTRL-A"),          M_HELP_ABOUT, 0, p->GetPicture("about.xpm"));

#if 0
  menuFile->DisableEntry(M_FILE_OPEN);
  menuFile->DisableEntry(M_FILE_SAVE);
  menuFile->DisableEntry(M_FILE_SAVEAS);
  menuFile->DisableEntry(M_FILE_PRINT);
  menuFile->DisableEntry(M_FILE_PRINTSETUP);

  menuHelp->DisableEntry(M_HELP_CONTENTS);
  menuHelp->DisableEntry(M_HELP_SEARCH);
#endif
  // The hint objects are used to place
  // and group the different menu widgets with respect to eachother.
  bar->AddPopup(new TGHotString("&File"), menuFile, new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 4, 0, 0));
  bar->AddPopup(new TGHotString("&Help"), menuHelp, new TGLayoutHints(kLHintsTop | kLHintsRight));

  menuFile->Connect("Activated(Int_t)","testbeam::NodeGUI",this,"handleMenu(Int_t)");
  menuHelp->Connect("Activated(Int_t)","testbeam::NodeGUI",this,"handleMenu(Int_t)");
}

/// Menu callback handler
void NodeGUI::handleMenu(Int_t id)   {
  switch(id)   {
  case M_FILE_OPEN:
    notImplemented("The \"File\" command is not implemented");
    break;
  case M_FILE_SAVE:
    notImplemented("The \"Save\" command is not implemented");
    break;
  case M_FILE_SAVEAS:
    notImplemented("The \"Save As\" command is not implemented");
    break;
  case M_FILE_PRINT:
    notImplemented("The \"Print\" command is not implemented");
    break;
  case M_FILE_PRINTSETUP:
    notImplemented("The \"Print Setup\" command is not implemented");
    break;
  case M_FILE_CLOSE:
    CloseWindow();
    break;
  case M_FILE_EXIT:
    terminate();   // terminate theApp no need to use SendCloseMessage()
    break;
  case M_HELP_CONTENTS:
    notImplemented("The \"Help Contents\" command is not implemented");
    break;
  case M_HELP_SEARCH:
    notImplemented("The \"Help Search\" command is not implemented");
    break;
  case M_HELP_ABOUT:
    aboutBox();
    break;
  default:
    break;
  }
}

/// Show a message box indicating this command is not implemented
void NodeGUI::notImplemented(const std::string& msg)   {
  Int_t retval = 0;
  this->Disconnect("CloseWindow()");
  this->Connect("CloseWindow()", "testbeam::NodeGUI", this, "tryToClose()");
  new TGMsgBox(gClient->GetRoot(), GetParent(),
	       "Command not implemented", msg.c_str(),
	       kMBIconAsterisk, kMBOk, &retval);
  this->Disconnect("CloseWindow()");
  this->Connect("CloseWindow()", "testbeam::NodeGUI", this, "CloseWindow()");
}

/// Show the about box of this application
void NodeGUI::aboutBox()   {
  Int_t retval = 0;
  this->Disconnect("CloseWindow()");
  this->Connect("CloseWindow()", "testbeam::NodeGUI", this, "tryToClose()");
  new TGMsgBox(gClient->GetRoot(), GetParent(),
	       "About NodeGUI....",
	       "NodeGUI\n\n"
	       "The simple way to interact\n"
	       "with remote processes\n\n"
	       "M.Frank CERN/LHCb",
	       kMBIconExclamation, kMBOk, &retval);
  this->Disconnect("CloseWindow()");
  this->Connect("CloseWindow()", "testbeam::NodeGUI", this, "CloseWindow()");
}

/// The user try to close the main window, while a message dialog box is still open.
void NodeGUI::tryToClose()   {
  ::printf("Can't close the window '%s' : a message box is still open\n", GetMainFrame()->GetName());
}

/// Close the window
void NodeGUI::CloseWindow()   {
  gApplication->Terminate();
  //CloseWindow();
}

/// Terminate the App no need to use SendCloseMessage()
void NodeGUI::terminate()   {
  gApplication->Terminate();
  //CloseWindow();
}
