//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TESTBEAM-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef TESTBEAM_NODEGUI_H
#define TESTBEAM_NODEGUI_H

/// Framework include files
#include "CPP/Interactor.h"
#include "RTL/rtl.h"

/// ROOT include files
#include "TGFrame.h"
#include <string>

/// Forward declarations
class TGTab;
class TGMenuBar;
class TGPopupMenu;

/// Namespace for the TestBeam software
namespace testbeam  {

  /// Forward declarations
  class OutputWindow;
  class NodeFSMPanel;

  /// The main instance of the XML-RPC Gui
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    18.11.2018
   */
  class NodeGUI : public TGCompositeFrame, public CPP::Interactor  {
  public:
    /// Reference to the menu bar
    TGMenuBar*    guiMenuBar = 0;
    TGPopupMenu*  menuFile   = 0;
    TGPopupMenu*  menuHelp   = 0;
    
    /// Reference to the tab pane
    TGTab*        tabs = 0;
    ///
    NodeFSMPanel* nodeControl = 0;
    /// Reference to the output logger window.
    OutputWindow* output = 0;

    enum ETestCommandIdentifiers {
      M_FILE_OPEN,
      M_FILE_SAVE,
      M_FILE_SAVEAS,
      M_FILE_CLOSE,
      M_FILE_PRINT,
      M_FILE_PRINTSETUP,
      M_FILE_EXIT,

      M_HELP_CONTENTS,
      M_HELP_SEARCH,
      M_HELP_ABOUT,
    };

  public:
    /// Default constructor inhibited
#ifdef G__ROOT
    NodeGUI() = default;
#else
    NodeGUI() = delete;
#endif
    /// Copy constructor inhibited
    NodeGUI(const NodeGUI& gui) = delete;
    /// Move constructor inhibited
    NodeGUI(NodeGUI&& gui) = delete;    
    /// Standard Initializing constructor
    NodeGUI(TGFrame* parent, RTL::CLI& opts);
    /// Default destructor
    virtual ~NodeGUI();

    /// Assignment operator inhibited
    NodeGUI& operator=(const NodeGUI& gui) = delete;
    /// Move assignment inhibited
    NodeGUI& operator=(NodeGUI&& gui) = delete;

    /// Interactor interrupt handler callback
    virtual void handle(const CPP::Event& ev)  override;
    /// Create a menu bar and add it to the parent
    TGMenuBar* menuBar();
    /// Create the menues of the application
    void createMenus(TGMenuBar* bar);
    /// Menu callback handler
    void handleMenu(Int_t id);

    /// Show the about box of this application
    void aboutBox();
    /// Show a message box indicating this command is not implemented
    void notImplemented(const std::string& msg);
    /// The user try to close the main window, while a message dialog box is still open.
    void tryToClose();
    /// Terminate the App no need to use SendCloseMessage()
    void terminate();
    /// Close the window
    void CloseWindow();

    /// ROOT class definition
    ClassDefOverride(NodeGUI,0);
  };
}       // End namespace testbeam
#endif  /* TESTBEAM_NODEGUI_H  */
