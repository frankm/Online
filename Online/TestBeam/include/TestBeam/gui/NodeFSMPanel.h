//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TESTBEAM-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef TESTBEAM_NODEFSMPANEL_H
#define TESTBEAM_NODEFSMPANEL_H

/// Framework include files
#include "TestBeam/gui/config.h"
#include "CPP/Interactor.h"
#include "RTL/rtl.h"

/// ROOT include files
#include "TGFrame.h"

// C/C++ include files
#include <memory>
#include <mutex>

class TGGroupFrame;
class TGTextButton;
class TGComboBox;
class TGTextEntry;
class TGLabel;

/// Namespace for the TestBeam software
namespace testbeam  {

  /// Explorer of the readout clients at a given DNS node
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    18.11.2018
   */
  class NodeFSMPanel : public TGCompositeFrame, public CPP::Interactor  {
  public:
    TGGroupFrame*     group    = nullptr;
    struct Child  {
      int               id     = 0;
      TGTextEntry*      name   = nullptr;
      TGTextEntry*      state  = nullptr;
      TGTextEntry*      meta   = nullptr;
    };
    struct LineEntry  {
      TGLabel*          label  = nullptr;
      TGTextEntry*      input  = nullptr;
    };
    struct TaskEntry  {
      TGLabel*          label  = nullptr;
      TGTextEntry*      utgid  = nullptr;
      TGTextEntry*      status = nullptr;
      TGTextButton*     start  = nullptr;
      TGTextButton*     kill   = nullptr;
    };
    struct dim_info_t  {
      int replyID = 0;
      int svcID   = 0;
      Interactor* recipient = nullptr;
      char fmt    = 'I';
      std::string data;
    dim_info_t(int id, Interactor* r, char f='I') : replyID(id), recipient(r), fmt(f) {}
    };
    std::mutex          dim_protection;
    CPP::Interactor*    gui            = nullptr;
    TGTextButton*       apply          = nullptr;
    LineEntry           maindns, dns, node, part, replace, runnumber, script, arch, numslave;
    TaskEntry           tmSrv, logSrv, logViewer, mbmmon, mbmdmp, tanSrv, storage, ctrl, did;
    std::vector<Child>  children;
    TGComboBox*         ctrl_command   = nullptr;
    TGLabel*            ctrl_label     = nullptr;
    TGTextEntry*        ctrl_status    = nullptr;
    TGTextButton*       ctrl_killall   = nullptr;
    TGTextButton*       ctrl_autostart = nullptr;
    TGTextButton*       ctrl_autostop  = nullptr;
    TGGroupFrame*       m_cmds_group   = nullptr;
    Pixel_t             disabled, enabled, executing, green, red, blue, yellow, white, black;
    dim_info_t*         m_storageCom   = nullptr;
    dim_info_t*         m_tanSrvCom    = nullptr;
    dim_info_t*         m_tmSrvCom     = nullptr;
    dim_info_t*         m_logSrvCom    = nullptr;
    dim_info_t*         m_ctrlCom      = nullptr;
    dim_info_t*         m_ctrlTasks    = nullptr;

    long                m_numSlaves    = 0;
    int                 m_pid          = -1;
    int                 m_currentRun   = 1234;
    int                 m_autoRun      = 0;
    bool                m_applied      = false;
    std::string         m_runinfo;
    std::string         m_startup;
    std::string         m_architecture;
    std::string         m_partition;
    std::string         m_node;
    std::string         m_user;
    std::string         m_DNS;
    std::string         m_mainDNS;
    std::string         m_ctrlType, m_ctrlScript;

    std::string         m_userName;
    std::string         m_logFifo;
    std::string         m_tmSrvName;
    std::string         m_tanSrvName;
    std::string         m_logSrvName;
    std::string         m_logViewName;
    std::string         m_mbmmonName;
    std::string         m_mbmdmpName;
    std::string         m_storageName;
    std::string         m_didName;
    std::string         m_ctrlName;
    std::string         m_replacements;

    int                 m_mainDNS_id = -1;
    int                 m_tmSrvList_id = 0;
    int                 m_runnumber_id = 0;
    
  public:
    enum IDS {
      PART_ID_OFFSET          = 100,
      MAINDNS_LABEL           = PART_ID_OFFSET+1,
      MAINDNS_INPUT           = PART_ID_OFFSET+2,
      DNS_LABEL               = PART_ID_OFFSET+3,
      DNS_INPUT               = PART_ID_OFFSET+4,
      PART_LABEL              = PART_ID_OFFSET+5,
      PART_INPUT              = PART_ID_OFFSET+6,
      NODE_LABEL              = PART_ID_OFFSET+7,
      NODE_INPUT              = PART_ID_OFFSET+8,
      NUMSLAVE_LABEL          = PART_ID_OFFSET+9,
      NUMSLAVE_INPUT          = PART_ID_OFFSET+10,
      ARCH_LABEL              = PART_ID_OFFSET+11,
      ARCH_INPUT              = PART_ID_OFFSET+12,
      SCRIPT_LABEL            = PART_ID_OFFSET+13,
      SCRIPT_INPUT            = PART_ID_OFFSET+14,
      REPLACE_LABEL           = PART_ID_OFFSET+15,
      REPLACE_INPUT           = PART_ID_OFFSET+16,
      RUNNUMBER_LABEL           = PART_ID_OFFSET+15,
      RUNNUMBER_INPUT           = PART_ID_OFFSET+16,

      TMSRV_ID_OFFSET         = 200,
      TMSRV_LABEL             = TMSRV_ID_OFFSET+1,
      TMSRV_STATUS            = TMSRV_ID_OFFSET+2,
      TMSRV_START             = TMSRV_ID_OFFSET+3,
      TMSRV_KILL              = TMSRV_ID_OFFSET+4,
      TMSRV_LIST              = TMSRV_ID_OFFSET+51,

      LOGSRV_ID_OFFSET        = 300,
      LOGSRV_LABEL            = LOGSRV_ID_OFFSET+1,
      LOGSRV_STATUS           = LOGSRV_ID_OFFSET+2,
      LOGSRV_START            = LOGSRV_ID_OFFSET+3,
      LOGSRV_KILL             = LOGSRV_ID_OFFSET+4,

      LOGVIEWER_ID_OFFSET     = 400,
      LOGVIEWER_LABEL         = LOGVIEWER_ID_OFFSET+1,
      LOGVIEWER_STATUS        = LOGVIEWER_ID_OFFSET+2,
      LOGVIEWER_START         = LOGVIEWER_ID_OFFSET+3,
      LOGVIEWER_KILL          = LOGVIEWER_ID_OFFSET+4,

      MBMMON_ID_OFFSET        = 500,
      MBMMON_LABEL            = MBMMON_ID_OFFSET+1,
      MBMMON_STATUS           = MBMMON_ID_OFFSET+2,
      MBMMON_START            = MBMMON_ID_OFFSET+3,
      MBMMON_KILL             = MBMMON_ID_OFFSET+4,

      MBMDMP_ID_OFFSET        = 600,
      MBMDMP_LABEL            = MBMDMP_ID_OFFSET+1,
      MBMDMP_STATUS           = MBMDMP_ID_OFFSET+2,
      MBMDMP_START            = MBMDMP_ID_OFFSET+3,
      MBMDMP_KILL             = MBMDMP_ID_OFFSET+4,

      TANSRV_ID_OFFSET        = 700,
      TANSRV_LABEL            = TANSRV_ID_OFFSET+1,
      TANSRV_STATUS           = TANSRV_ID_OFFSET+2,
      TANSRV_START            = TANSRV_ID_OFFSET+3,
      TANSRV_KILL             = TANSRV_ID_OFFSET+4,

      STORAGE_ID_OFFSET       = 800,
      STORAGE_LABEL           = STORAGE_ID_OFFSET+1,
      STORAGE_STATUS          = STORAGE_ID_OFFSET+2,
      STORAGE_START           = STORAGE_ID_OFFSET+3,
      STORAGE_KILL            = STORAGE_ID_OFFSET+4,

      CTRL_ID_OFFSET          = 900,
      CTRL_LABEL              = CTRL_ID_OFFSET+1,
      CTRL_STATUS             = CTRL_ID_OFFSET+2,
      CTRL_START              = CTRL_ID_OFFSET+3,
      CTRL_KILL               = CTRL_ID_OFFSET+4,
      CTRL_COMMAND,
      CTRL_TASKS,
      CTRL_AUTOSTART,
      CTRL_AUTOSTOP,

      DID_LABEL,
      DID_START,
      DID_KILL,

      CHECK_ENABLE_PARAM,
      NUM_ENTRIES_CHILDREN   = 30,
      LAST
    };
    /// Standard initializing constructor
    NodeFSMPanel(TGFrame* parent, CPP::Interactor* gui, RTL::CLI& opts);

    /// Default destructor
    virtual ~NodeFSMPanel();

    void init();
    /// Set the UTGID names according to the parameter settings
    void set_utgid_names();

    void updateChildren(const std::string& value);
    void updateTaskStatus(TaskEntry& entry, const std::string& state);
    
    void runnumberChanged(const char* value);

    void setArchitecture(const std::string& value);
    void architectureChanged(const char* value);

    void setScript(const std::string& value);
    void scriptChanged(const char* value);

    void setPartition(const std::string& value);
    void partitionChanged(const char* value);

    void setReplacement(const std::string& value);
    void replacementChanged(const char* value);

    void setHost(const std::string& value);
    void hostChanged(const char* value);

    void setMainDns(const std::string& value);
    void mainDnsChanged(const char* value);

    void setDns(const std::string& value);
    void dnsChanged(const char* value);

    void setNumberOfSlaves(int value);
    void numslaveChanged(const char* value);

    void setRunInfo(const std::string& value);
    void runInfoChanged(const char* value);

    void setStartup(const std::string& value);
    void setUser(const std::string& value);


    void commandChanged(int value);


    void applyParams();
    void enableParams();
    void kill_all_tasks();

    void updateDependentValues();
    void updateCtrlCommand(std::string state);
    std::string userName()  const   {   return m_userName; }
    std::string logFifo()  const    {   return m_logFifo;  }
    std::string startDefaults() const;
    std::string killDefaults() const;

    /// Get the controller running in auto mode
    void autoStart();
    /// Get the controller offline in auto mode
    void autoStop();

    /// Start tmSrv process
    void tmSrv_start();
    /// Kill tmSrv process
    void tmSrv_kill();

    /// Start logSrv process
    void logSrv_start();
    /// Kill logSrv process
    void logSrv_kill();

    /// Start controller process
    void controller_start();
    /// Kill controller process
    void controller_kill();

    /// Start logViewer process
    void logViewer_start();
    /// Kill logViewer process
    void logViewer_kill();

    /// Start mbmmon process
    void mbmmon_start();
    /// Kill mbmmon process
    void mbmmon_kill();

    /// Start mbmdmp process
    void mbmdmp_start();
    /// Kill mbmdmp process
    void mbmdmp_kill();

    /// Start storage process
    void storage_start();
    /// Kill storage process
    void storage_kill();

    /// Start tanSrv process
    void tanSrv_start();
    /// Kill tanSrv process
    void tanSrv_kill();

    /// Start local DID process
    void did_start();
    /// Kill did process
    void did_kill();

    void controllerCommand(const std::string& cmd);
    void startProcess(const std::string& args);
    void killProcess(const std::string& args);

    /// Interactor interrupt handler callback
    virtual void handle(const CPP::Event& ev)  override;

    /// ROOT class definition
    ClassDefOverride(NodeFSMPanel,0);
  };
}       // End namespace testbeam
#endif  /* TESTBEAM_NODEFSMPANEL_H  */
