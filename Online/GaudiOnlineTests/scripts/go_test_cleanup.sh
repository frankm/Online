#!/bin/bash
#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
echo "....starting cleanup.....";
#
####/bin/ls -1t *.dat;
#
/bin/rm -f *.dat;
/bin/rm -f Cnew.xml;
echo "....cleanup finished.....";
