//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

#include "MDFWriter.h"
DECLARE_COMPONENT( Online::MDFWriter )

#include "MIFWriter.h"
DECLARE_COMPONENT( Online::MIFWriter )
