//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_MIFWRITER_H
#define ONLINE_MIFWRITER_H

#include "GaudiKernel/Algorithm.h"
#include <map>

namespace Gaudi {
  class IIODataManager;
  class IDataConnection;
}

/// Online namespace
namespace Online    {

  /**@class MIFWriter
    *
    *
    * @author:  M.Frank
    * @version: 1.0
    */
  class MIFWriter : public Algorithm   {
  public:
    typedef Gaudi::IDataConnection Connection;

  protected:
    typedef std::map<int,std::string> FidMap;

    FidMap m_fidMap;
    /// Reference to file manager service
    Gaudi::IIODataManager* m_ioMgr;
    /// Name of the IO manager service
    std::string   m_ioMgrName;
    /// Stream descriptor (Initializes networking)
    Connection*   m_connection;
    /// Connection parameters
    std::string   m_connectParams;

  public:

    /// Standard algorithm constructor
    MIFWriter(const std::string& name, ISvcLocator* pSvcLocator);

    /// Standard Destructor
    virtual ~MIFWriter();

    /// Initialize
    StatusCode initialize() override;

    /// Finalize
    StatusCode finalize() override;

    // Execute procedure
    StatusCode execute() override;
  };
}
#endif // ONLINE_MIFWRITER_H
