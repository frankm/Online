//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

// Include files
#include "GaudiKernel/MsgStream.h"
#include "MDF/RawDataConnection.h"
#include "MDF/RawDataSelector.h"
#include "MIFHeader.h"
#include "MDF/MDFIO.h"
#include "Helpers.h"
#include <map>
#include <memory>

///  Online namespace declaration
namespace Online  {
  /** @class MIFSelector
   */
  class MIFSelector : public LHCb::RawDataSelector  {
  public:
    /** Create a new event loop context
     * @param[in,out] refpCtxt  Reference to pointer to store the context
     *
     * @return StatusCode indicating success or failure
     */
    StatusCode createContext(Context*& refpCtxt) const override;

    /// Service Constructor
    MIFSelector( const std::string& nam, ISvcLocator* svcloc )
      : RawDataSelector( nam, svcloc)   {     }

    /// Standard destructor
    virtual ~MIFSelector()  {}
  };

  /** @class MIFContext
   *
   *  @author  M.Frank
   *  @version 1.0
   */
  class MIFContext : public LHCb::RawDataSelector::LoopContext, protected LHCb::MDFIO {
  protected:
    typedef Gaudi::IDataConnection Connection;
    typedef std::map<int,Connection*> FidMap;
    typedef std::pair<MDFDescriptor, Connection*> io_context_t;

    long long            m_fileOffset;
    MIFHeader*           m_header = 0;
    const MIFSelector*   m_mifSel;
    FidMap               m_fidMap;
    int                  m_mifFID;
    IMessageSvc*         m_msg = 0;
  private:
    /// Usage of copy constructor is not allowed
    MIFContext(const MIFContext&)
      : LHCb::RawDataSelector::LoopContext(0),
        LHCb::MDFIO(MDFIO::MDF_RECORDS,""), m_fileOffset(0), m_mifSel(0), m_mifFID(0)
    {
      m_header = 0;
      m_msg = 0;
    }
    /// Assignment is not allowed
    MIFContext& operator=(const MIFContext&)  {
      return *this;
    }
  public:
    /// Standard constructor
    MIFContext(const MIFSelector* pSel)
      : LHCb::RawDataSelector::LoopContext(pSel), LHCb::MDFIO(MDFIO::MDF_RECORDS,pSel->name()),
        m_fileOffset(0), m_mifSel(pSel), m_mifFID(0)
    {
      m_header = (MIFHeader*)new char[1024];
      m_msg = m_mifSel->msgSvc();
    }
    /// Standard destructor
    virtual ~MIFContext();
    MDFDescriptor getDataSpace(void* const ioDesc, size_t len) override {
      io_context_t* par   = (io_context_t*)ioDesc;
      size_t        addon = m_sel->additionalSpace();
      if ( (len + addon) > size_t(par->first.second) ) {
        par->first.first  = (char*)::realloc( par->first.first, len + addon );
        par->first.second = len + addon;
      }
      return MDFDescriptor( par->first.first, par->first.second );
    }
    StatusCode connect(const std::string& spec) override;
    /// Receive event and update communication structure
    StatusCode receiveData(IMessageSvc* msg) override;
    /// Skip N events
    StatusCode skipEvents(IMessageSvc* msg,int numEvt) override;
    /// Read raw byte buffer from input stream
    StatusCode readBuffer(void* const ioDesc, void* const data, size_t len) override;
    long long  offset()  const override { return m_fileOffset;     }
  };
}

using namespace Online;
using namespace Gaudi;

/// Create a new event loop context
StatusCode MIFSelector::createContext(Context*& refpCtxt) const {
  refpCtxt = new MIFContext(this);
  return StatusCode::SUCCESS;
}

/// Standard destructor
MIFContext::~MIFContext()  {
  delete [] (char*)m_header;
  for(auto& c : m_fidMap)  {
    m_ioMgr->disconnect(c.second).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    delete c.second;
  }
  m_fidMap.clear();
}

/// Read raw byte buffer from input stream
StatusCode
MIFContext::readBuffer(void* const ioDesc, void* const data, size_t len) {
  io_context_t* io = (io_context_t*)ioDesc;
  return m_ioMgr->read(io->second,data,len);
}

StatusCode MIFContext::connect(const std::string& spec)  {
  int fid = genChecksum(1,spec.c_str(),spec.length()+1);
  FidMap::iterator i = m_fidMap.find(fid);
  if ( i == m_fidMap.end() )  {
    std::unique_ptr<IDataConnection> c(new LHCb::RawDataConnection(m_sel,spec));
    StatusCode sc = m_ioMgr->connectRead(false,c.get());
    if ( sc.isSuccess() )  {
      MsgStream log(m_msg,"MIFSelector");
      log << MSG::ALWAYS << "Connected to:" << spec << " " << c->pfn()
	/* << " " << fid */
	  << endmsg;
      m_fidMap.emplace(fid,c.release());
      m_mifFID = fid;
      return sc;
    }
    MsgStream err(m_msg,"MIFSelector");
    err << MSG::ERROR << "Failed to connect to:" << spec << endmsg;
    return sc;
  }
  return StatusCode::SUCCESS;
}

/// Receive event and update communication structure
StatusCode MIFContext::receiveData(IMessageSvc* msg)  {
  char buff[1024];
  MIFHeader *hdr = (MIFHeader*)buff;
  FidMap::iterator i = m_fidMap.find(m_mifFID);
  setupMDFIO(msg,0);
  m_data.first  = 0;
  m_data.second = 0;
  if ( i != m_fidMap.end() )  {
    Connection* c = (*i).second;
  Next:
    if ( m_ioMgr->read(c,hdr,sizeof(int)+2*sizeof(char)).isSuccess() )  {
      io_context_t io_context(MDFDescriptor(0,0),nullptr);
      if ( m_ioMgr->read(c,buff+sizeof(int)+2*sizeof(char),hdr->size()).isSuccess() )  {
        i = m_fidMap.find(hdr->fid());
        if ( i == m_fidMap.end() )  {
          if ( hdr->type() == MIFHeader::MIF_FID )  {
            int id = m_mifFID;  // need to save MIF FID - will be overwritten in connect()
            StatusCode sc = connect(hdr->data<char>());
            m_mifFID = id;
            if ( !sc.isSuccess() )  {
              return sc;
            }
            goto Next;
          }
          else  {
            MsgStream err(msg,"MIFSelector");
            err << MSG::ERROR << "Unknown input specification:"
                << std::hex << std::setw(8) << hdr->fid() << endmsg;
            return StatusCode::FAILURE;
          }
        }
        io_context.second = (*i).second;
      }
      m_fileOffset = m_ioMgr->seek(io_context.second,0,SEEK_CUR);
      m_data = readBanks(&io_context, 0 == m_fileOffset);
      if ( m_data.second > 0 )  {
        return StatusCode::SUCCESS;
      }
    }
  }
  return StatusCode::FAILURE;
}

/// Skip N events
StatusCode MIFContext::skipEvents(IMessageSvc* /* msg */,int numEvt)  {
  for(int i=0; i<numEvt; ++i)  {
  }
  return StatusCode::FAILURE;
}

DECLARE_COMPONENT( Online::MIFSelector )
