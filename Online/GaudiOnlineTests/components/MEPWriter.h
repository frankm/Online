//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================
#ifndef MEP_MEPWRITER_H
#define MEP_MEPWRITER_H

#include "MDFWriter.h"
#include <map>

/// LHCb namespace
namespace Online    {

  // Forward declarations
  class RawEvent;

  /// Writer to output MEP event frames for testing the data processing infrastructure
  /**@class MEPWriter
    *
    *
    * @author:  M.Frank
    * @version: 1.0
    */
  class MEPWriter : public MDFWriter   {

  private:
    typedef std::map<unsigned int, LHCb::RawEvent*> Events;
    /// Property: packing factor for merged MEPS
    int           m_packingFactor;
    /// Property: flag tio indicate if TAE events should be forced
    bool          m_makeTAE;
    /// Intermediate buffer to store raw events
    Events        m_events;
    /// Event ID
    unsigned int  m_evID;
    /// half window size for TAE
    int           m_halfWindow;
    /// Number of MEP events produced
    int           m_numEvent;

  public:

    /// Standard algorithm constructor
    MEPWriter(const std::string& name, ISvcLocator* pSvcLocator);

    /// Standard Destructor
    virtual ~MEPWriter();

    /// Initialize procedure
    StatusCode initialize() override;

    /// Finalize procedure
    StatusCode finalize() override;

    /// Execute procedure
    StatusCode execute() override;
  };
}      // End namespace LHCb
#endif // MEP_MEPWRITER_H
