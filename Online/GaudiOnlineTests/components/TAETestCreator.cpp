//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

// Include files from Gaudi
#include "Gaudi/Algorithm.h"
#include <GaudiKernel/DataObjectHandle.h>
#include "Helpers.h"

/// Online namespace declaration
namespace Online   {

  ///  Creates and fills TAE RawEvents from source
  /** @class TAETestCreator TAETestCreator.cpp
   *
   *  \author Markus Frank
   *  \date   2005-10-13
   */
  class TAETestCreator : public Gaudi::Algorithm {
    Gaudi::Property<int>   m_numTAEEvents{   this, "TAEEvents",       3,     "TAE half window size"};
    Gaudi::Property<bool>  m_removeDAQStatus{this, "RemoveDAQStatus", false, "Remove DAQ status bank"};
    DataObjectReadHandle<LHCb::RawEvent>  m_rawEvent{this, "RawLocation", LHCb::RawEventLocation::Default};

  public:
    using Algorithm::Algorithm;

    /// Main execution
    StatusCode execute(EventContext const& /* ctxt */) const override {
      MsgStream log(msgSvc(),name());
      int nevts = abs(m_numTAEEvents)>7 ? 7 : abs(m_numTAEEvents);
      auto raw = m_rawEvent.get();
      if ( raw )  {
        // Need to remove all MDF header banks first
        if ( m_removeDAQStatus ) {
          for(const auto* b : raw->banks(LHCb::RawBank::DAQ))  {
            if ( b->version() == DAQ_STATUS_BANK )
              raw->removeBank(b);
          }
        }
	for(const auto* bank : raw->banks(LHCb::RawBank::ODIN) )  {
	  Tell1Bank* b = (Tell1Bank*)bank;
	  b->begin<RunInfo>()->TAEWindow = nevts;
	}
        for(int i=-nevts; i<=nevts; ++i)  {
          if ( i != 0 )  {
            LHCb::RawEvent* copy = 0;
            if ( cloneRawEvent(raw,copy) )  {
              std::string loc = rootFromBxOffset(i)+"/"+LHCb::RawEventLocation::Default;
              if ( eventSvc()->registerObject(loc,copy).isSuccess() )  {
                continue;
              }
              delete copy;
              log << MSG::ERROR << "Failed to register RawEvent at " << loc << endmsg;
            }
            log << MSG::ERROR << "Failed to clone RawEvent." << endmsg;
          }
        }
        return StatusCode::SUCCESS;
      }
      log << MSG::ERROR << "Failed to access " << LHCb::RawEventLocation::Default << endmsg;
      return StatusCode::FAILURE;
    }
  };
}

DECLARE_COMPONENT( Online::TAETestCreator )
