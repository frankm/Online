//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <Tell1Data/Tell1Decoder.h>
#include <MDF/StreamDescriptor.h>
#include <Event/RawEvent.h>

/// C/C++ include files
#include <map>

/// Online namespace declaration
namespace Online  {

  using LHCb::StreamDescriptor;

  /// Determine number of banks from rawEvent object
  size_t numberOfBanks(const LHCb::RawEvent* evt);
  /// Determine number of bank types from rawEvent object
  size_t numberOfBankTypes(const LHCb::RawEvent* evt);

  /// Clone rawevent structure
  int cloneRawEvent(LHCb::RawEvent* source, LHCb::RawEvent*& result);
#if 0
  /// read MEP record from input stream
  int readMEPrecord(StreamDescriptor& dsc, const StreamDescriptor::Access& con);

  /// Decode MEP into bank collections
  int decodeMEP( const Tell1MEP* me,
		 unsigned int&   partitionID,
		 std::map<unsigned int,LHCb::RawEvent* >& events);

  /// Encode entire mep from map of events
  int encodeMEP(const std::map<unsigned int, LHCb::RawEvent*>& events,
		unsigned int partID,
		void*        alloc_ctxt,
		void*       (*alloc)(void* ctxt, size_t len),
		Tell1MEP**   mep_event);
#endif
}
