//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

// Include files from Gaudi
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <Tell1Data/Tell1Decoder.h>
#include <Event/RawEvent.h>

/// Online namespace declaration
namespace Online    {

  /// Determine length of the sequential buffer from RawEvent object
  inline size_t rawEventLength(const LHCb::RawEvent* evt)    {
    size_t  len = 0;
    for(size_t i=LHCb::RawBank::L0Calo; i<LHCb::RawBank::LastType; ++i)  {
      len += rawEventLengthBanks(evt->banks(LHCb::RawBank::BankType(i)));
    }
    return len;
  }

 /** @class RawEventTestCreator RawEventTestCreator.cpp
  *  Creates and fills dummy RawEvent
  *
  *  @author Markus Frank
  *  @date   2005-10-13
  */
  class RawEventTestCreator : public Gaudi::Algorithm {
    /// Flag to test bank removal
    Gaudi::Property<bool>                 m_removeBank{this, "RemoveBank", false, ""};
    DataObjectWriteHandle<LHCb::RawEvent> m_rawEvent{this, "RawLocation", LHCb::RawEventLocation::Default};
  public:
    using Algorithm::Algorithm;

    /// Main execution
    StatusCode execute(EventContext const& /* ctxt */) const override  {
      int i, cnt, *p;
      static int trNumber = -1;
      unsigned int trMask[] = {~0u,~0u,~0u,~0u};
      unsigned int run_no = 1 + (++trNumber)/10000;
      auto raw  = std::make_unique<LHCb::RawEvent>();
      Tell1Bank*  bank = 0;
      for(i=0; i<16; ++i)  {
        bank = (Tell1Bank*)raw->createBank(i, LHCb::RawBank::Rich, 1, (i+1)*64, 0);
        for(p=bank->begin<int>(), cnt=0; p != bank->end<int>(); ++p)  {
          *p = cnt++;
        }
        raw->adoptBank((LHCb::RawBank*)bank, true);
      }
      if ( m_removeBank ) raw->removeBank((LHCb::RawBank*)bank);
      for(i=0; i<9; ++i)  {
        bank = (Tell1Bank*)raw->createBank(i, LHCb::RawBank::PrsE, 1, (i+1)*32, 0);
        for(p=bank->begin<int>(), cnt=0; p != bank->end<int>(); ++p)  {
          *p = cnt++;
        }
        raw->adoptBank((LHCb::RawBank*)bank, true);
      }

      bank = (Tell1Bank*)raw->createBank(0, LHCb::RawBank::ODIN, 2, sizeof(RunInfo), 0);
      RunInfo* run = bank->begin<RunInfo>();
      ::memset(run,0,sizeof(RunInfo));
      run->Run     = 123;
      run->Orbit   = trNumber/10;
      run->L0ID    = trNumber;
      run->bunchID = trNumber%100;
      run->triggerType = 0;
      raw->adoptBank((LHCb::RawBank*)bank, true);

      // raw->removeBank(bank);
      size_t len = rawEventLength(raw.get());
      Tell1Bank* hdrBank = (Tell1Bank*)
	raw->createBank(0,
			LHCb::RawBank::DAQ, 
			DAQ_STATUS_BANK, 
			sizeof(EventHeader)+sizeof(EventHeader::Header1), 0);
      EventHeader* hdr = (EventHeader*)hdrBank->data();
      hdr->setChecksum(0);
      hdr->setCompression(0);
      hdr->setHeaderVersion(3);
      hdr->setSpare(0);
      hdr->setDataType(EventHeader::BODY_TYPE_BANKS);
      hdr->setSubheaderLength(sizeof(EventHeader::Header1));
      hdr->setSize(len);
      EventHeader::SubHeader h = hdr->subHeader();
      h.H1->setTriggerMask(trMask);
      h.H1->setRunNumber(run_no);
      h.H1->setOrbitNumber(trNumber/10);
      h.H1->setBunchID(trNumber%100);
      raw->adoptBank((LHCb::RawBank*)hdrBank, true);
      m_rawEvent.put(std::move(raw));
      return StatusCode::SUCCESS;
    }
  };
}

DECLARE_COMPONENT( Online::RawEventTestCreator )
