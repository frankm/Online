//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

#include "Helpers.h"
using namespace std;

/// Clone rawevent structure
int Online::cloneRawEvent(LHCb::RawEvent* source, LHCb::RawEvent*& result)  {
  if ( source )  {
    std::unique_ptr<LHCb::RawEvent> raw(new LHCb::RawEvent());
    for(int i=LHCb::RawBank::L0Calo; i<LHCb::RawBank::LastType; ++i) {
      for( const auto* bank : source->banks(LHCb::RawBank::BankType(i)) )
	raw->addBank(bank);
    }
    result = raw.release();
    return 1;
  }
  return 0;
}
#if 0
/// read MEP record from input stream
int Online::readMEPrecord(LHCb::StreamDescriptor& dsc, const LHCb::StreamDescriptor::Access& con)  {
  unsigned int len = 0;
  dsc.setLength(0);
  if ( con.ioDesc > 0 )  {
    if ( StreamDescriptor::read(con,&len,sizeof(len)) )  {
      if ( len+sizeof(len) > size_t(dsc.max_length()) )  {
	dsc.allocate(sizeof(len) + size_t(len*1.5));
      }
      Tell1MEP* me = (Tell1MEP*)dsc.data();
      me->setSize(len);
      if ( StreamDescriptor::read(con,me->first(),len) )  {
	dsc.setLength(len+sizeof(len));
	return 1;
      }
    }
  }
  return 0;
}

/// Decode MEP into bank collections
int Online::decodeMEP( const Tell1MEP* me,
		       unsigned int&   partitionID,
		       map<unsigned int,LHCb::RawEvent*>& events)
{
  map<unsigned int,std::vector<Tell1Bank*> > evts;
  if ( decodeMEP(me,partitionID, evts) )  {
    for(const auto& e : evts)  {
      LHCb::RawEvent* ev = events[e.first] = new LHCb::RawEvent();
      auto& banks = e.second;
      for(auto* b : banks)  {
        ev->adoptBank((LHCb::RawBank*)b, false);
      }
    }
    return 1;
  }
  return 0;
}

/// Encode entire mep from map of events
int Online::encodeMEP(const map<unsigned int, LHCb::RawEvent*>& events,
		      unsigned int partID,
		      void*        alloc_ctxt,
		      void*       (*alloc)(void* ctxt, size_t len),
		      Tell1MEP**   mep_event)
{
  typedef vector<Tell1Bank*>             BankV;
  typedef map<unsigned int, BankV >    BankMap;
  typedef map<unsigned int, BankMap >  BankMap2;
  size_t evtlen = Tell1MEP::sizeOf();
  BankMap2 m;
  for(const auto& ev : events)   {
    unsigned int eid = ev.first;
    LHCb::RawEvent*    evt = const_cast<LHCb::RawEvent*>(ev.second);
    for(size_t t=LHCb::RawBank::L0Calo; t<LHCb::RawBank::LastType; ++t)  {
      for(const auto* b : evt->banks(LHCb::RawBank::BankType(t)))  {
	m[b->sourceID()][eid].push_back((Tell1Bank*)b);
	evtlen += b->totalSize();
      }
    }
  }
  evtlen += m.size()*Tell1MultiFragment::sizeOf();
  for(const auto& e : m) 
    evtlen += e.second.size()*Tell1Fragment::sizeOf();

  void* memory = (*alloc)(alloc_ctxt, evtlen);
  unsigned short packing = (unsigned short)events.size();
  Tell1MEP* me = new(memory) Tell1MEP(0);
  Tell1MultiFragment* mf = me->first();
  for(auto j=m.begin(); j != m.end(); ++j, mf = me->next(mf)) {
    BankMap& bm = (*j).second;
    bool first = true;
    mf->setSize(0);
    mf->setPacking(packing);
    mf->setPartitionID(partID);
    Tell1Fragment* f = mf->first();
    for(BankMap::iterator l=bm.begin(); l != bm.end(); ++l, f = mf->next(f))   {
      unsigned int eid = (*l).first;
      if ( first ) {
	mf->setEventID(eid);
	first = false;
      }
      f->setEventID((unsigned short)(eid&0xFFFF));
      f->setSize(0);
      Online::encodeFragment((*l).second, f);
      mf->setSize(mf->size()+f->size()+f->sizeOf());
    }
    me->setSize(me->size()+mf->size()+mf->sizeOf()-mf->hdrSize());
    // printf("MF:%p  %p\n",mf,((char*)me)+evtlen);
  }
  *mep_event = me;
  return 1;
}
#endif

/// Determine number of banks from rawEvent object
size_t Online::numberOfBanks(const LHCb::RawEvent* evt)   {
  size_t count = 0;
  for(size_t i=LHCb::RawBank::L0Calo; i<LHCb::RawBank::LastType; ++i)  {
    count += evt->banks(LHCb::RawBank::BankType(i)).size();
  }
  return count;
}

/// Determine number of bank types from rawEvent object
size_t Online::numberOfBankTypes(const LHCb::RawEvent* evt) {
  size_t count = 0;
  for(size_t i=LHCb::RawBank::L0Calo; i<LHCb::RawBank::LastType; ++i)  {
    if ( !evt->banks(LHCb::RawBank::BankType(i)).empty() ) count++;
  }
  return count;
}
