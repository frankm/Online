//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : OnlineKernel
//
//  Author     : Markus Frank
//==========================================================================
// Include files
#include <RTL/ProcessGroup.h>

#include <cmath>
#include <ctime>
#include <cstdio>
#include <signal.h>
#include "RTL/rtl.h"
#include "RTL/Logger.h"
#ifndef _WIN32
#include <sys/wait.h>
#endif
#include <iostream>
#include <sstream>
#include <cerrno>

#include "RTL/DllAccess.h"
using namespace std;
using namespace RTL;
using OnlineBase::currentCommand;

extern "C" int rtl_test_process_group(int, char** ) {
  Process *p2;
  std::stringstream str;
  const char *a1[] = {"5",0}, *a2[]={"10",0}, *a3[]={"15",0};
  ::lib_rtl_set_log_level(LIB_RTL_INFO);
  ProcessGroup pg;
  pg.add(   new Process("SLEEPER_1","/bin/sleep",a1));
  pg.add(p2=new Process("SLEEPER_2","/bin/sleep",a2));
  pg.add(   new Process("SLEEPER_3","/bin/sleep",a3));
  time_t start = ::time(0);
  pg.start();
  Process::setDebug(true);
  ::lib_rtl_sleep(200);
  str << "Stop process " << p2->name() << endl;
  p2->stop();
  ::lib_rtl_sleep(200);
  pg.wait();
  pg.removeAll();
  time_t stop = ::time(0);
  if ( ::fabs(float(stop-start)) > 14E0 ) {
    str << "All processes ended" << endl;
  }
  else {
    str << "Subprocess execution failed." << endl;
  }
  ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
  return 0;
}

// Dummy task executed by next test (rather than the stupid "sleep"
extern "C" int rtl_test_process_sleep(int argc, char** argv) {
  const char* nam = ::lib_rtl_getenv("UTGID");
  if ( 0 == nam ) nam = "Unknown";
  if ( argc > 1 ) {
    int nsec;
    ::sscanf(argv[1],"%d",&nsec);
    //::lib_rtl_sleep(100*nsec);
    ::lib_rtl_sleep(1000*nsec);
    ::printf("%-12s Process starting...\n",nam);
    ::printf("%-12s arg0:%s\n",nam,argv[0]);
    ::printf("%-12s arg1:%s\n",nam,argv[1]);
    ::printf("%-12s Process sleeping for %d seconds.\n",nam,nsec);
    ::printf("%-12s Process exiting....\n",nam);
    return 0;
  }
  ::printf("%-12s Process exiting....  [WRONG ARGUMENTS]\n",nam);
  return 0;
}

extern "C" int rtl_test_sub_processes(int, char** ) {
  Process *p2, *p4, *p6;
  std::stringstream str;
  const char *a1[]={"libGaudiOnlineTests.so","rtl_test_process_sleep","4",0};
  const char *a2[]={"libGaudiOnlineTests.so","rtl_test_process_sleep","5",0};
  const char *a3[]={"libGaudiOnlineTests.so","rtl_test_process_sleep","6",0};
  const char *a4[]={"libGaudiOnlineTests.so","rtl_test_process_sleep","7",0};
  const char *a5[]={"libGaudiOnlineTests.so","rtl_test_process_sleep","8",0};
  const char *a6[]={"libGaudiOnlineTests.so","rtl_test_process_sleep","9",0};

  ProcessGroup pg;
  ::lib_rtl_set_log_level(LIB_RTL_INFO);
  string cmd(currentCommand());
  ::lib_rtl_signal_log(false);
  Process::setDebug(true);
  pg.add(   new Process("SLEEPER_0xFEED0001",cmd.c_str(),a1,"/dev/null"));
  pg.add(p2=new Process("SLEEPER_0xFEED0002",cmd.c_str(),a2));
  pg.start();
  pg.add(   new Process("SLEEPER_0xFEED0003",cmd.c_str(),a3,"/dev/null"));
  pg.add(p4=new Process("SLEEPER_0xFEED0004",cmd.c_str(),a4));
  pg.start();
  pg.add(   new Process("SLEEPER_0xFEED0005",cmd.c_str(),a5,"/dev/null"));
  pg.add(p6=new Process("SLEEPER_0xFEED0006",cmd.c_str(),a6));
  pg.start();
  time_t start = ::time(0);
  ::lib_rtl_sleep(4000);
  str.str("");
  str << "Stop process " << p2->name() << endl;
  ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
  p2->stop();
  ::lib_rtl_sleep(100);
  str.str("");
  str << "Stop process " << p4->name() << endl;
  ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
  p4->stop();
  ::lib_rtl_sleep(100);
  str.str("");
  str << "Stop process " << p6->name() << endl;
  ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
  p6->stop();
  ::lib_rtl_sleep(15000);
  pg.wait();
  pg.removeAll();
  time_t stop = ::time(0);
  str.str("");
  if ( ::fabs(float(stop-start)) < 30E0 )
    str << "All processes ended" << endl;
  else
    str << "Subprocess execution failed:" << ::fabs(float(stop-start)) << "seconds." << endl;
  ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
  return 0;
}
