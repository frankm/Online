//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <RTL/rtl.h>
#include <Tell1Data/RawFile.h>

extern "C" int test_rawfile_mkdir(int argc, char** argv) {
  RTL::CLI cli(argc, argv, [] {
    });
  std::string dir;
  cli.getopt("directory",1,dir);
  if ( dir.empty() )  cli.call_help();

  int ret = Online::RawFile::mkdir(dir.c_str(), 0777, false);
  if ( ret )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"FAILED to create directory: %s [%s]",
		     dir.c_str(), RTL::errorString(errno).c_str());
    cli.call_help();
  }
  ::lib_rtl_output(LIB_RTL_ALWAYS,"Created directory: %s",dir.c_str());
  return 0;
}

extern "C" int test_rawfile_rmdir(int argc, char** argv) {
  RTL::CLI cli(argc, argv, [] {
    });
  std::string dir;
  cli.getopt("directory",1,dir);
  if ( dir.empty() )  cli.call_help();

  int ret = Online::RawFile::rmdir(dir.c_str(), true, false);
  if ( ret )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"FAILED to remove directory: %s [%s]",
		     dir.c_str(), RTL::errorString(errno).c_str());
    cli.call_help();
  }
  ::lib_rtl_output(LIB_RTL_ALWAYS,"Removed directory: %s",dir.c_str());
  return 0;
}
