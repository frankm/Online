#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/GaudiOnlineTests
-----------------------
#]=======================================================================]

online_library(GaudiOnlineTests
        src/Asio_echo_client.cpp
        src/Asio_echo_server.cpp
        src/Asio_http_async_client.cpp
        src/Asio_tan_alloc_tst.cpp
        src/Asio_unix_socket_test.cpp
        src/SignalTest.cpp
        src/TanDB_tst.cpp
        src/Tan_alloc_tst.cpp
        src/Tan_inquire_tst.cpp
        src/TestAsioTAN.cpp
        src/TestBits.cpp
        src/TestBits2.cpp
        src/TestGbl.cpp
        src/TestProcess.cpp
        src/TestQue.cpp
        src/TestRawFile.cpp
        src/TestScreen.cpp
        src/TestSemaphore.cpp
        src/TestSensors.cpp
        src/TestSocketTAN.cpp
        src/TestStdoutBuffering.cpp
        src/TestSysCalls.cpp
        src/TestTAN.cpp
        src/TestThread.cpp
        src/ams_bounce.cpp
        src/ams_qmtest.cpp
        src/ams_sender.cpp
        src/ams_test.cpp
        src/asio_test_async_connect.cpp
        src/asio_test_endpoint_iterator.cpp
        src/check_lib_symbols.cpp
        src/dim_integer_service.cpp
        src/mbm_AsyncProd.cpp
        src/mbm_SyncProd.cpp
        src/mbm_cons.cpp
        src/mbm_simple_test.cpp
        src/pthread_cancellation.cpp
        src/scr_test.cpp
        src/wttest.cpp)
target_compile_options(GaudiOnlineTests  PRIVATE -Wno-address-of-packed-member)
target_link_libraries(GaudiOnlineTests
  PRIVATE   Boost::headers
            Online::dim
            Online::OnlineBase
            Online::OnlineKernel
            Online::EventData
            ROOT::Core
            Threads::Threads
)

online_gaudi_module(GaudiOnlineTestsComp
        components/Components.cpp
        components/GaudiMain.cpp
        components/Helpers.cpp
        components/MDFWriter.cpp
        components/MIFSelector.cpp
        components/MIFWriter.cpp
        components/RawEventTestCreator.cpp
        components/TAETestCreator.cpp
        components/CounterTypeTest.cpp
        components/HistogramTypeTest.cpp)
target_link_libraries(GaudiOnlineTestsComp
        Gaudi::GaudiKernel
        Gaudi::GaudiUtilsLib
        LHCb::DAQEventLib
        LHCb::MDFLib
        Online::EventData
	Online::GauchoLib
        ROOT::Core
	-lrt)
target_compile_options(GaudiOnlineTestsComp     PRIVATE -Wno-address-of-packed-member)

online_install_python(python)
online_install_scripts(scripts)

online_executable(online_test main/online_test.cpp)
target_link_libraries(online_test PRIVATE Online::OnlineBase)

gaudi_add_tests(QMTest)

online_env(SET GAUDIONLINE_OPTS "${CMAKE_CURRENT_SOURCE_DIR}/options")
online_env(SET GAUDIONLINETESTSROOT "${CMAKE_CURRENT_SOURCE_DIR}")
#
#set_property(TEST GaudiOnlineTests.chkpt_02_restore APPEND PROPERTY DEPENDS GaudiOnlineTests.chkpt_01_save)
#set_property(TEST GaudiOnlineTests.chkpt_03_restore APPEND PROPERTY DEPENDS GaudiOnlineTests.chkpt_02_restore)
#
# The following tests all start a TANServer so they cannot run concurrently
set_tests_properties(
    GaudiOnlineTests.onlinekernel.ams_test
    GaudiOnlineTests.onlinekernel.boost_asio_tan_test
    GaudiOnlineTests.onlinekernel.rtl_tan_test
    GaudiOnlineTests.onlinekernel.rtl_test_socket_tan
    PROPERTIES
        RESOURCE_LOCK TANServer
)
#
# The following tests all start a MBM so they cannot run concurrently
set_tests_properties(
    GaudiOnlineTests.onlinekernel.mbm_full_test
    GaudiOnlineTests.onlinekernel.mbm_install
    GaudiOnlineTests.onlinekernel.mbm_simple_test
    PROPERTIES
        RESOURCE_LOCK MBM
)

