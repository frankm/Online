//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

#include <Gaucho/MonitorInterface.h>
#include <Gaucho/TaskSaveTimer.h>
#include <Gaucho/MonitorClass.h>
#include <Gaucho/ObjService.h>
#include <Gaucho/MonSys.h>
#include <RTL/strdef.h>
#include <atomic>

using namespace Online;

MonitorInterface::MonitorInterface(const std::string &utgid, SubSysParams&& cntpars, SubSysParams&& histpars)
  : m_CntrPars(cntpars), m_HistPars(histpars), m_UTGID(utgid)
{
  m_runAware      = m_CntrPars.runAware;
  m_CntrPars.type = MONSUBSYS_Counter;
  m_CntrSubSys    = std::make_shared<CounterSubSys>(m_CntrPars);
  MonSys::instance().add(m_CntrSubSys);

  histpars.type   = MONSUBSYS_Histogram;
  m_HistSubSys    = std::make_shared<HistSubSys>(histpars);
  MonSys::instance().add(m_HistSubSys);
}

MonitorInterface::~MonitorInterface()   {
  MonSys::instance().remove(m_CntrSubSys);
  MonSys::instance().remove(m_HistSubSys);
}

void MonitorInterface::i_unsupported(const std::string& /*name*/,
    const std::type_info& /*typ, const std::string &owner*/)
{
}

size_t MonitorInterface::undeclare(const std::string& entry)   {
  size_t removed = 0;
  if ( m_HistSubSys )
    removed = m_HistSubSys->remove(entry);
  if ( !removed && m_CntrSubSys )
    removed = m_CntrSubSys->remove(entry);
  return removed;
}

size_t MonitorInterface::undeclareAll(std::string &oname)  {
  size_t removed = 0;
  if ( m_HistSubSys )  {
    m_HistSubSys->stop();
    removed += m_HistSubSys->removeAll(oname);
  }
  if ( m_CntrSubSys )  {
    m_CntrSubSys->stop();
    removed += m_CntrSubSys->removeAll(oname);
  }
  return removed;
}

int MonitorInterface::start()   {
  DimServer::autoStartOff();
  if (m_started)    {
    MonSys::instance().start();
    DimServer::autoStartOn();
    //    DimServer::start();
    return 1;
  }
  MonSys::instance().setup(this->m_UTGID.c_str());
  if (m_CntrSubSys != 0) m_CntrSubSys->setup("Counter",this->m_CntrPars.expandnames);
  if (m_HistSubSys != 0) m_HistSubSys->setup("Histos");
  DimServer::autoStartOn();
  DimServer::start();
  MonSys::instance().start();
  m_started = true;
  return 1;
}

int MonitorInterface::stop()   {
  MonSys::instance().stop();
  return 1;
}

void MonitorInterface::applyCounterClasses(std::vector<std::string> &options)  {
  if (options.size() > 0)  {
    auto clist = this->makeClassList(options);
    for ( auto& cl : clist )    {
      cl->configure(MONSUBSYS_Counter);
      cl->runAware = m_CntrPars.runAware;
      m_CntrSubSys->addClass(std::move(cl));
    }
  }
}

void MonitorInterface::applyHistogramClasses(std::vector<std::string> &options)  {
  if (options.size() > 0)  {
    auto clist = this->makeClassList(options);
    for ( auto& cl : clist )    {
      cl->configure(MONSUBSYS_Histogram);
      cl->runAware = m_HistPars.runAware;
      m_HistSubSys->addClass(std::move(cl));
    }
  }
}

void MonitorInterface::enableRates(bool enable)    {
  m_CntrSubSys->have_rates = enable;
}

std::list<std::shared_ptr<MonitorClass> > MonitorInterface::makeClassList(std::vector<std::string> &option) const  {
  /*
   * Class Definition Option Syntax:
   * "<Class Name><blank><update interval><blank> list of regular expressions separated by <blanks>"
   */
  std::string name, def;
  std::list<std::shared_ptr<MonitorClass> > result;
  for (size_t i = 0; i < option.size(); i++)  {
    int intv = 0;
    std::list<std::string> l;
    auto op = RTL::str_split(option[i].c_str(), " ");
    if ( op.size() > 1 )   {
      name = op[0];
      ::sscanf(op[1].c_str(), "%d", &intv);
      for (size_t j = 2; j < op.size(); j++)
        l.insert(l.end(), op.at(j));
    }
    result.insert(result.end(), std::make_shared<MonitorClass>(name, intv, l));
  }
  return result;
}

void MonitorInterface::setRunNo(int runno)  {
  m_runnr = runno;
  MonSys::instance().setRunNo(runno);
}

void MonitorInterface::eorUpdate(int runno)   {
  MonSys::instance().eorUpdate(runno);
}

void MonitorInterface::update(unsigned long ref)  {
  MonSys::instance().update(ref);
}

void MonitorInterface::resetHistos()  {
  MonSys::instance().reset();
}

void MonitorInterface::lock(void)   {
  MonSys::instance().lock();
}

void MonitorInterface::unlock(void)   {
  MonSys::instance().unlock();
}

void MonitorInterface::startSaving(const std::string &dir,
				   const std::string &part,
				   const std::string &task,
				   int period, std::shared_ptr<DimService>& sav)
{
  if ( !m_saveTimer )  {
    m_saveTimer = std::make_unique<TaskSaveTimer>(m_HistSubSys,period);
    m_saveTimer->partition  = part;
    m_saveTimer->rootdir    = dir;
    m_saveTimer->taskname   = task;
    m_saveTimer->savesetSvc = sav;
  }
  m_saveTimer->start();
}

void MonitorInterface::stopSaving()  {
  if ( m_saveTimer ) m_saveTimer->stop();
  m_saveTimer.reset();
}
