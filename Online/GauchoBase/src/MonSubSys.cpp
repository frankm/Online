//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/MonSubSys.h>
#include <Gaucho/MonSys.h>
#include <Gaucho/Utilities.h>
#include <Gaucho/MonitorClass.h>

using namespace Online;

MonSubSys::MonSubSys(SubSysParams &p) : classMgr(p)  {
  this->runNumber = 0;
  this->type      = p.type;
  this->lockid    = std::make_unique<BRTLLock>();
  this->classMgr.runAware = p.runAware;
}

MonSubSys::~MonSubSys()    {
  locked_execution(this, [this]() { this->classMgr.clear(); });
#if 0
  locked_execution(this, [this]() {
      if ( this->type == MONSUBSYS_Counter)    {
	for (auto& i : this->objects )
	  i.second->delete_OutputService();
      }
      this->objects.clear();
    });
#endif
}

void MonSubSys::start()    {
  if (!this->start_done)  {
    this->classMgr.start();
    start_done = true;
  }
}

void MonSubSys::stop()  {
  if (this->start_done)  {
    this->classMgr.stop();
    this->start_done = false;
  }
}

void MonSubSys::reset()  {
  this->classMgr.reset();
}

size_t MonSubSys::removeAll(const std::string& owner_name)  {
  return this->classMgr.removeAll(owner_name);
}

void MonSubSys::clear()  {
  this->classMgr.clear();
}

void MonSubSys::eorUpdate(int runo)  {
  this->classMgr.eorUpdate(runo);
}

void MonSubSys::update()   {
  this->classMgr.update();
}

void MonSubSys::update(unsigned long ref)   {
  this->classMgr.update(ref);
}

void MonSubSys::setRunNo(int runno)   {
  this->runNumber = runno;
  this->classMgr.setRunNo(runno);
}

void MonSubSys::addClass(std::string &nam, int intv, std::list<std::string>&sels)  {
  this->addClass(std::make_shared<MonitorClass>(nam, intv, sels));
}

void MonSubSys::addClass(std::shared_ptr<MonitorClass>&& c)  {
  this->classMgr.add(std::move(c));
}

