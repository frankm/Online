//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

#include <Gaucho/TaskSaveTimer.h>
#include <Gaucho/Utilities.h>
#include <Gaucho/MonitorClass.h>
#include <Gaucho/ObjService.h>

using namespace Online;

TaskSaveTimer::TaskSaveTimer(std::shared_ptr<MonSubSys> add, int timer_period)
  : GenTimer(timer_period * 1000, TIMER_TYPE_PERIODIC), HistSaver(add)
{
  m_dueTime = 0;
  m_dontdimlock = true;
  m_subsys = std::move(add);
}

TaskSaveTimer::~TaskSaveTimer()  {
}

void TaskSaveTimer::timerHandler(void)   {
  // Need protection against m_subsys==0:
  // If the save time runs, but no histograms were
  // ever published, m_subsys (histograms) is NULL!
  if ( this->m_subsys )   {
    if ( !this->m_dontdimlock )
      dim_lock();
    this->makeDirs(this->m_subsys->runNumber);
    auto file = openFile();
    if ( !this->m_dontdimlock )
      dim_unlock();
    for (const auto& i : this->m_subsys->classMgr.getMap() )   {
      locked_execution(this->m_subsys, [this,i]() {
              if (i.second->genSrv) {
              i.second->genSrv->serialize(this->buffer, 0);
              }
              });
      if ( this->buffer.used() > 0 )  {
	file->cd();
	this->savetoFile(this->buffer.begin());
      }
    }
    this->closeFile(std::move(file));
    this->updateSaveSet();
    this->m_subsys->reset();
  }
}
