//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/MonClassMgr.h>
#include <Gaucho/ObjSerializer.h>
#include <Gaucho/MonitorClass.h>
#include <Gaucho/MonSubSys.h>
#include <Gaucho/MonBase.h>
#include <Gaucho/RateMgr.h>
#include <Gaucho/ObjRPC.h>

using namespace std;
using namespace Online;

MonClassMgr::MonClassMgr(const SubSysParams& p)  {
  string blk = "thisorthat";
  list<string>  l({blk});
  string nam = MonitorClass::DefaultClassName;

  this->type = p.type;
  this->runAware = p.runAware;
  this->defaultClass = std::make_shared<MonitorClass>(nam, p.updatePeriod,l);
  this->defaultClass->runAware = p.runAware;
  this->defaultClass->configure(p.type);
  this->classMap[nam] = this->defaultClass;
}

MonClassMgr::~MonClassMgr()   {
  this->classMap.clear();
}

std::shared_ptr<MonitorClass> MonClassMgr::getClass(const std::string& nam)   {
  for ( auto& i : this->classMap )   {
    if ( i.second->matchName(nam) )
      return i.second;
  }
  return this->defaultClass;
}

void MonClassMgr::reset()   {
  for ( auto& i : this->classMap )
    i.second->reset();
}


size_t MonClassMgr::clear()   {
  size_t removed = 0;
  for ( auto& i : this->classMap )
    removed += i.second->clear();
  return removed;
}

size_t MonClassMgr::removeAll(const std::string& owner_name)  {
  size_t removed = 0;
  for ( auto& i : this->classMap )
    removed += i.second->clearAll(owner_name);
  return removed;
}

void MonClassMgr::add(std::shared_ptr<MonitorClass>&& cl )  {
  auto i = this->classMap.find(cl->ID);
  if ( i == this->classMap.end() )  {
    this->classMap[cl->ID] = cl;
    cl->runAware = this->runAware;
  }
}

void MonClassMgr::setRunNo(int runno)  {
  for (auto& i : this->classMap)
    i.second->setRunNo(runno);
}

void MonClassMgr::eorUpdate(int runo)   {
  setRunNo(runo);
  for (auto& i : this->classMap)
    i.second->eorUpdate(runo);
}

void MonClassMgr::update()   {
  for (auto& i : this->classMap)
    i.second->update();
}

void MonClassMgr::update(unsigned long ref)  {
  for (auto& i : this->classMap)
    i.second->update(ref);
}

void MonClassMgr::start()   {
  for (auto& i : this->classMap)
    i.second->start();
}

void MonClassMgr::stop()  {
  for (auto& i : this->classMap)
    i.second->stop();
}

void MonClassMgr::setup(const std::string& n, bool expandnames, const std::string &einfix, const string &pname)  {
  string name = (this->type == MONSUBSYS_Counter) ? "Counter" : "Histos";
  string nam  = "MON_" + pname + "/" + name + "/HistCommand";
  if ( !this->rpc )   {
    this->rpc = make_unique<ObjRPC>(make_unique<ObjSerializer>(this, expandnames), nam.c_str(), "I:1;C", "C");
  }
  for ( auto& i : this->classMap )
    i.second->setup(n, expandnames, einfix);
}
