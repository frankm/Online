//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/HistTask.h>
#include <Gaucho/MonHist.h>
#include <Gaucho/HistSerDes.h>

#include <numeric>

using namespace Online;

HistTask::~HistTask()   {
}

HistTask::HistTask(const std::string& task,const std::string& dns, int tmo)
  : TaskRPC("Histos", task, dns, tmo)
{
}

int HistTask::Histos(const std::vector<std::string>& names,std::vector<TObject*>& histos)   {
  if (m_RPC != 0)   {
    int cmdlen = std::accumulate(names.begin(), names.end(), names.size()+sizeof(RPCCommRead), sum_string_length);
    RPCCommRead *cmd = RPCComm::make_command<RPCCommRead>(cmdlen, RPCCRead);
    cmd->copy_names(cmd->which, names);
    m_RPC->setData(cmd, cmdlen);
    int status  = m_RPC->analyseReply();
    if ( status == 0 )  {
      for ( auto k : m_RPC->hists )    {
	TObject *o = (TObject*)HistSerDes::de_serialize(k.second);
	histos.push_back(o);
      }
    }
    return status;
  }
  return 1;
}

int HistTask::Histos(const std::vector<std::string>& names,std::map<std::string,TObject*>& histos)  {
  int status = this->Histos(names);
  if ( status == 0 )  {
    for( const auto& k : m_RPC->hists )    {
      TObject *o = (TObject*)HistSerDes::de_serialize(k.second);
      histos.emplace(k.first, o);
    }
  }
  return status;
}
