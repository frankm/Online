//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

#include "Gaucho/RateMgr.h"
#include "Gaucho/MonRate.h"
#include "Gaucho/MonitorClass.h"

namespace {
  bool is_rate(MONTYPE typ)   {
    switch(typ)   {
    case C_RATEFLOAT:
    case C_RATEDOUBLE:
    case C_RATEDOUBLESTAR:
      return true;
    default:
      return false;
    }
  }
}

Online::RateMgr::RateMgr(MonitorClass* cl)
  : monClass(cl)
{
}

void Online::RateMgr::makeRates(unsigned long dt)   {
  for ( auto& o : this->monClass->entities )    {
    if ( is_rate(o.second->type) ) 
      ((MonRateBase*)o.second.get())->makeRate(dt);
  }
}

void Online::RateMgr::zero()   {
  for ( auto& o : this->monClass->entities )    {
    if ( is_rate(o.second->type) ) 
      ((MonRateBase*)o.second.get())->zero();
  }
}

void Online::RateMgr::CreateOutputServices(const std::string& infix)    {
  for ( auto& o : this->monClass->entities )    {
    if ( is_rate(o.second->type) ) 
      ((MonRateBase*)o.second.get())->create_OutputService(infix);
  }
}
