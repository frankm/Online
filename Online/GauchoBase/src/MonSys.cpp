//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "Gaucho/MonSys.h"
#include "Gaucho/MonSubSys.h"
#include <dim/dis.hxx>

#include <algorithm>
using namespace Online;

MonSys::MonSys()  {
  state = "Constructed";
}

void MonSys::add(std::shared_ptr<MonSubSys> ss)   {
  for(auto& p : subSystems)  {
    if (p->type == ss->type)
      return;
  }
  subSystems.insert(subSystems.end(),std::move(ss));
}

void MonSys::start()  {
  DimServer::setWriteTimeout(20);
  DimServer::autoStartOn();
  DimServer::start(name.c_str());
  for(auto& p : subSystems)
    p->start();
  state = "Started";
}

void MonSys::stop()  {
  DimServer::autoStartOff();
  for(auto& p : subSystems)
    p->stop();
  state = "Stopped";
}

void MonSys::remove(std::shared_ptr<MonSubSys> ss)  {
  auto it = std::find(subSystems.begin(), subSystems.end(), std::move(ss));
  if ( it != subSystems.end() )
    subSystems.erase(it);
}

void MonSys::lock()  {
  for(auto& p : subSystems)
    p->lock();
}

void MonSys::unlock()   {
  for(auto& p : subSystems)
    p->unlock();
}

MonSys& MonSys::instance()   {
  static MonSys s;
  return s;
}

MonSys *MonSys::setup(const std::string& n)  {
  name = n;
  state = "Set-up";
  return this;
}

void MonSys::clear()   {
  for(auto& p : subSystems)
    p->clear();
}

void MonSys::reset()   {
  for(auto& p : subSystems)
    p->reset();
}

void MonSys::update()  {
  for(auto& p : subSystems)
    p->update();
}

void MonSys::update(unsigned long ref)
{
  for(auto& p : subSystems)
    p->update(ref);
}

void MonSys::eorUpdate(int runo)   {
  for(auto& p : subSystems)
    p->eorUpdate(runo);
}

void MonSys::setRunNo(int runo)   {
  for(auto& p : subSystems)
    p->setRunNo(runo);
}
