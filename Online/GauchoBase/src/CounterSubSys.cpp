//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/CounterSubSys.h>
#include <Gaucho/MonitorClass.h>
#include <Gaucho/RateMgr.h>
#include <RTL/rtl.h>

Online::CounterSubSys::CounterSubSys(SubSysParams &p):
  MonSubSys(p), ratePrefix(p.ratePrefix),
  expandInfix(p.expandInfix), updatePeriod(p.updatePeriod), expandnames(p.expandnames)
{
  this->dontclear = false;
}

void Online::CounterSubSys::add_counter(std::unique_ptr<MonBase>&& h, std::unique_ptr<MonRateBase>&& r)   {
  auto cls = this->classMgr.getClass(h->name);
  cls->add(std::move(h));
  if ( r )  {
    cls->add(std::move(r));
  }
}

size_t Online::CounterSubSys::remove(const std::string &nam)   {
  auto ratename = this->ratePrefix+nam;
  auto cls = this->classMgr.getClass(nam);
  cls->remove(ratename);
  auto ret = cls->remove(nam);
  if ( ret )   {
    return 1;
  }
  return 0;
}

void Online::CounterSubSys::setup(const std::string& n, bool )   {
  this->classMgr.setup(name=n, expandnames, expandInfix, RTL::processName());
}
