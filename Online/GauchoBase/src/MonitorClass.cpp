//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework include files
#include <Gaucho/MonitorClass.h>
#include <Gaucho/ObjSerializer.h>
#include <Gaucho/ObjService.h>
#include <Gaucho/Utilities.h>
#include <Gaucho/MonSubSys.h>
#include <Gaucho/MonTimer.h>
#include <Gaucho/MonBase.h>
#include <Gaucho/RateMgr.h>
#include <RTL/strdef.h>

/// C/C++ include files
#include <limits>

using namespace std;
using namespace Online;

static long s_empty;
string MonitorClass::DefaultClassName = "**DEFAULT**";


MonitorClass::MonitorClass(const string &id, int intv, list<string> &sel)   {
  this->ID = id;
  this->interval = intv;
  this->entityExpression = sel;
  this->expand           = false;
  this->type             = 0;
  this->runNumber        = 0;
  this->lockid           = std::make_unique<BRTLLock>();
  regex_constants::match_flag_type matchFlags = 
    regex_constants::match_flag_type(regex_constants::icase | regex_constants::ECMAScript
    //      | regex_constants::optimize | regex_constants::match_not_null
				     );
  for (const auto& s : sel )  {
    regex *r = new regex(s, (regex_constants::syntax_option_type)matchFlags);
    this->class_regex.push_back(r);
  }
}

MonitorClass::~MonitorClass()    {
}

//void MonitorClass::setType(int t)    {
//  this->type = t;
//}

void MonitorClass::configure(int typ)   {
  this->type = typ;
  if (this->type == MONSUBSYS_Counter)  {
    if ( !this->rateManager )   {
      this->rateManager = std::make_unique<RateMgr>(this);
    }
  }
}

void MonitorClass::setup(const string& clazz_name, bool expnd, const string &infix)  {
  string nodename = RTL::nodeNameShort();
  string utgid    = RTL::processName();
  this->expandInfix = infix;
  this->expand      = expnd;
  this->name        = clazz_name;
  this->timer = make_unique<MonTimer>(this, this->interval);

  if ( this->expand )  {
    if (this->type == MONSUBSYS_Counter)   {
      for ( auto& h : this->entities )
        h.second->create_OutputService(expandInfix);
    }
  }
  if (runAware)
    this->serviceFMT = "MON_"+utgid+"/"+this->name+"/<runno>/Data";
  else
    this->serviceFMT = "MON_"+utgid+"/"+this->name+"/Data";

  string run = to_string(this->runNumber);
  string nam = RTL::str_replace(this->serviceFMT, "<runno>", run);

  if (this->ID != MonitorClass::DefaultClassName)
    nam += "."+this->ID;

  this->lockid->name = nam;
  if ( !this->genSrv )    {
    this->genSrv = make_unique<ObjService>(make_unique<ObjSerializer>(this, this->expand), nam, "C", &s_empty, 4);
  }
  if ( !this->eor_service )   {
    nam = "MON_" + utgid + "/" + this->name + "/EOR";
    if (this->ID != MonitorClass::DefaultClassName)
      nam = "MON_" + utgid + "/" + this->name + "/EOR." + this->ID;
    this->eor_service = make_unique<ObjService>(make_unique<ObjSerializer>(this, false), nam, "C", &s_empty, 4);
  }
  this->eor_service->setEORflag(true);
}

void MonitorClass::start()   {
  if ( this->timer ) this->timer->start();
}

void MonitorClass::stop()    {
  if ( this->timer )  {
    this->timer->stop();
  }
  if ( this->rateManager )    {
    this->genSrv->setRunNo(this->runNumber);
    this->rateManager->zero();
  }
  this->update();
}

void MonitorClass::reset()   {
  for(auto& i : this->entities )
    i.second->reset();
}

void MonitorClass::add(std::unique_ptr<MonBase>&& m)   {
  if ( m )   {
    auto i = this->entities.find(m->name);
    if ( i != this->entities.end() )
      i->second->delete_OutputService();
    this->entities[m->name] = std::move(m);
  }
}

size_t MonitorClass::clear()   {
  size_t ret = this->entities.size();
  for(auto& i : this->entities )
    i.second->delete_OutputService();
  this->entities.clear();
  return ret;
}

size_t MonitorClass::clearAll(const std::string &owner_name)    {
  if ( owner_name.size() == 0 || owner_name == "*" )    {
    return this->clear();
  }
  size_t ret = 0;
  std::vector<std::string> items;
  items.reserve(this->entities.size());
  for ( auto& i : this->entities )   {
    if ( i.first.find(owner_name) != std::string::npos )
      items.push_back(i.first);
  }
  for ( auto& i : items )   {
    auto iter = this->entities.find(i);
    iter->second->delete_OutputService();
    this->entities.erase(iter);
    ++ret;
  }
  return ret;
}

std::unique_ptr<MonBase> MonitorClass::remove(const std::string &nam)   {
  std::unique_ptr<MonBase> ret;
  auto i = this->entities.find(nam);
  if ( i != this->entities.end() )   {
    ret.reset(i->second.release());
    this->entities.erase(i);
  }
  return ret;
}

bool MonitorClass::matchName(const string &nam)  {
//  regex_constants::match_flag_type matchFlags;
//  matchFlags = (regex_constants::match_flag_type) regex_constants::icase;
//      | (regex_constants::match_flag_type) regex_constants::optimize
//      | (regex_constants::match_flag_type) regex_constants::match_not_null;
  for (const auto* r : this->class_regex )   {
    smatch sm;
    bool stat = ::regex_match(nam, sm, *r);
    if ( stat )    {
      return true;
    }
  }
  return false;
}

void MonitorClass::makeRates(unsigned long dt)   {
  if ( this->rateManager ) this->rateManager->makeRates(dt);
}

void MonitorClass::setRunNo(unsigned int runno)  {
  if ( !runAware )  {
    this->runNumber = runno;
    return;
  }
  if ( runno != this->runNumber )   {
    if (this->genSrv == 0 )    {
      this->runNumber = runno;
      return;
    }
    string run = to_string(runno);
    string nam = RTL::str_replace(this->serviceFMT, "<runno>", run);
    if ( this->ID.size() != 0 )   {
      nam += "."+this->ID;
    }
    this->genSrv = make_unique<ObjService>(make_unique<ObjSerializer>(this, this->expand), nam, "C", &s_empty, 4);
    this->genSrv->setRunNo(runno);
  }
  this->runNumber = runno;
}

void MonitorClass::eorUpdate(int runno)   {
  this->eor_service->setRunNo(runno);
  this->setRunNo(runno);
  locked_execution(this, [this]() { this->eor_service->serialize(0); });
  this->eor_service->update();
}

void MonitorClass::update()  {
  locked_execution(this, [this]() {
      this->genSrv->setRunNo(this->runNumber);
      this->genSrv->serialize(0);
    });
  this->genSrv->update();
}

void MonitorClass::update(unsigned long ref)  {
  long r = ref;
  this->genSrv->setRunNo(this->runNumber);
  this->genSrv->setTime(r);
  locked_execution(this, [this]() { this->genSrv->serialize(0); });
  this->genSrv->update();
}
