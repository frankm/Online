//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_HISTSAVER_H
#define ONLINE_GAUCHO_HISTSAVER_H

#include <CPP/mem_buff.h>
#include <TFile.h>
#include <string>

/// Forward declarations
class DimService;

/// Online namespace declaration
namespace Online   {

  /// Forward declarations
  class MonLockable;

  /// Generic histogram saver.
  /**
   *  Generally locks all operations. If you want to lock the saving process 
   *  yourself, then reset HistSaver::lockable.
   */
  class HistSaver  {
  protected:
    mem_buff    buffer            { };
    std::string filename          { };
    std::string currDir           { };
    std::string currFileName      { };
    bool        mustUpdateSaveSet { true };


    /// Unlocked directory creation function
    void   i_makeDirs(int runno);

  public:
    std::string                  rootdir       { };
    std::string                  partition     { };
    std::string                  taskname      { };
    std::shared_ptr<DimService>  savesetSvc    { };
    std::shared_ptr<MonLockable> lockable      { };
    size_t                       numSaveCycle  { 0 };
    size_t                       numSavedHisto { 0 };
    bool                         EOR           { false };

  public:
    /// Initializing constructor
    HistSaver(std::shared_ptr<MonLockable> lockable);
    /// Default destructor
    virtual ~HistSaver();

    void   makeDirs(int runno);
    std::unique_ptr<TFile> openFile();
    void   closeFile(std::unique_ptr<TFile>&& file);
    size_t savetoFile(const void *buff);
    void   updateSaveSet();
  };
}
#endif     // ONLINE_GAUCHO_HISTSAVER_H
