//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * MyDimErrorHandler.h
 *
 *  Created on: Oct 12, 2016
 *      Author: beat
 */

#ifndef ONLINE_GAUCHOAPPL_GAUCHOAPPL_MYDIMERRORHANDLER_H_
#define ONLINE_GAUCHOAPPL_GAUCHOAPPL_MYDIMERRORHANDLER_H_
#include "dim/dim.hxx"
#include "RTL/rtl.h"


/// Online namespace declaration
namespace Online  {

  class MyDimErrorHandler : public DimErrorHandler
  {
  public:
    bool m_flag;
    void errorHandler(int /*severity*/, int code, char *msg) override
    {
      int sev = LIB_RTL_INFO;
      if (m_flag)
	{
	  ::lib_rtl_output(sev,"DIM Message Code %x %s\n",code,msg);
	}
      return;
    }
    MyDimErrorHandler () : DimErrorHandler()
      {
	m_flag = true;
      }
    void start()
    {
      m_flag = true;
    }
    void stop()
    {
      m_flag = false;
    }
  };
}
#endif /* ONLINE_GAUCHOAPPL_GAUCHOAPPL_MYDIMERRORHANDLER_H_ */
