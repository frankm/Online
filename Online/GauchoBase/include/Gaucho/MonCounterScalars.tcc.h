//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================

// implementation of MonCounter methods for scalar types

/// Online namespace declaration
namespace Online  {

  template<typename T> MONTYPE MonCounter<T>::Type() const {
    if constexpr (std::is_same_v<T, int>) {
	return C_INT;
      } else if constexpr (std::is_same_v<T, long> || std::is_same_v<T, long long>) {
	return C_LONGLONG;
      } else if constexpr (std::is_same_v<T, float>) {
	return C_FLOAT;
      } else if constexpr (std::is_same_v<T, double>) {
	return C_DOUBLE;
      } else if constexpr (std::is_same_v<T, unsigned int>) {
	return C_UINT;
      } else if constexpr (std::is_same_v<T, unsigned long>) {
	return C_ULONG;
      } else {
      static_assert(false_v<T>, "Unsupported scalar type in MonCounter");
    }
  }

  template <typename T> MonCounter<T>::MonCounter(const std::string& nam, const std::string& tit, const T& dat)
    : MonCounterBase(Type(), nam, tit, std::max(8u, serialSize()), serialSize(), (T*)&dat, 0) {}

  template <typename T> unsigned int MonCounter<T>::serializeData(DimBuffBase *pp, const void *src, int)  const {
    long* dst = add_ptr<long>(pp, pp->dataoff);
    *dst      = 0L;        // Zero first 8 data bytes
    *(T*)dst  = *(T*)src;  // Then copy value to lower bytes
    return pp->reclen;
  }

  template <typename T> void MonCounter<T>::create_OutputService(const std::string & infix)    {
    this->dim_service = 0;
    std::string nam = m_srvcprefix + infix + this->name;
    if (nam.length() <= 128 /* maximum length of a DIM service Name*/) {
      if constexpr (std::is_same_v<T, int> || std::is_same_v<T, unsigned int>) {
	  this->dim_service.reset(new DimService(nam.c_str(), (int&)*(T*)m_contents));
	} else if constexpr (std::is_same_v<T, long long>) {
	  this->dim_service.reset(new DimService(nam.c_str(), (long long&)*(T*)m_contents));
	} else if constexpr (std::is_same_v<T, long> || std::is_same_v<T, unsigned long>) {
	  this->dim_service.reset(new DimService(nam.c_str(), (long long&)(*((long long*)m_contents))));
	} else if constexpr (std::is_same_v<T, float>) {
	  this->dim_service.reset(new DimService(nam.c_str(), (float&)*(T*)m_contents));
	} else if constexpr (std::is_same_v<T, double>) {
	  this->dim_service.reset(new DimService(nam.c_str(), (double&)*(T*)m_contents));
	} else {
	static_assert(false_v<T>, "Unsupported scalar type in MonCounter::create_OutputService");
      }
    }
  }
}
