//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_HISTJSON_H
#define ONLINE_GAUCHO_HISTJSON_H

#include <Gaucho/MonTypes.h>
#include <Gaucho/dimhist.h>
#include <nlohmann/json.hpp>

/// Online namespace declaration
namespace Online  {

  class JsonHistDeserialize   {
  public:
    typedef nlohmann::json  json;

  public:
    static json _basic_hist(const DimHistbuff1* b);
    static json _get_axis(const DimHistbuff1* b, const DimAxis& axis);
    static json _get_stats(const DimHistbuff1 *b);
    static json _get_doubles(const DimHistbuff1 *b, size_t offset=0);
    static std::pair<std::string,std::string> _get_title(const DimHistbuff1 *b);
    static json de_serialize(const void* ptr);
  };
}
#endif // ONLINE_GAUCHO_HISTJSON_H
