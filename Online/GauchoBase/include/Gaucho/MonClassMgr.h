//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHOBASE_GAUCHO_MONCLASSMGR_H_
#define ONLINE_GAUCHOBASE_GAUCHO_MONCLASSMGR_H_

#include <memory>
#include <string>
#include <map>
#include <set>

/// Online namespace declaration
namespace Online   {

  class SubSysParams;
  class MonitorClass;
  class CounterSubSys;
  class HistSubSys;
  class MonBase;
  class ObjRPC;

  class MonClassMgr  {
    friend class ObjSerializer;
    friend class TaskSaveTimer;
    friend class CounterSubSys;
    friend class HistSubSys;

  private:

    std::map<std::string, std::shared_ptr<MonitorClass> > classMap;
    std::shared_ptr<MonitorClass> getClass(const std::string& mon_name);

    const std::map<std::string,std::shared_ptr<MonitorClass> >& getMap()  const  {
      return this->classMap;
    }


  public:
    std::set<MonBase*>            objects;
    std::unique_ptr<ObjRPC>       rpc;
    std::shared_ptr<MonitorClass> defaultClass;

    int   type;
    bool  runAware = false;

    MonClassMgr(const SubSysParams& parameters);
    virtual ~MonClassMgr();

    void add(std::shared_ptr<MonitorClass>&& clazz);
    size_t removeAll(const std::string &owner_name);
    size_t clear();

    void reset();
    void setRunNo(int runno);
    void eorUpdate(int runo);
    void update();
    void update(unsigned long ref);
    void start();
    void stop();
    void setup(const std::string& name, bool expandnames, const std::string &einfix, const std::string &pname);
    void registerEntry(std::unique_ptr<MonBase>&& m);

    void unregisterEntry(const std::string &name);
    void unregisterAll();
  };
}
#endif /* ONLINE_GAUCHOBASE_GAUCHO_MONCLASSMGR_H_ */
