//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#pragma once

#include <Gaucho/MonSubSys.h>
#include <Gaucho/MonCounter.h>
#include <Gaucho/MonRate.h>

#include <set>

/// Online namespace declaration
namespace Online  {

  class CounterSubSys : public MonSubSys  {
  public:
    std::string            ratePrefix;
    std::string            expandInfix;
    int                    updatePeriod;
    bool                   expandnames  { false };
    bool                   have_rates   { true  };

  public:

    CounterSubSys(SubSysParams &params);
    virtual ~CounterSubSys() = default;

    template<typename T> void add_counter(const std::string& name, const std::string &anme, const T&  var);
    template<typename T> void add_counter(const std::string& name, const std::string &anme, const std::string &fmt, T var, int siz);
    template<typename T> void add_counter(const std::string& name, const std::string &anme, const T &var1, const T&var2);
    void add_counter(std::unique_ptr<MonBase>&& h, std::unique_ptr<MonRateBase>&& r);
    void setup(const std::string& n, bool expand=false) override;
    virtual size_t remove(const std::string& name)  override;
  };

  /// generic addEntry
  template<typename T> void CounterSubSys::add_counter(const std::string& nam, const std::string &desc, const T&  var) {
    std::unique_ptr<MonRateBase> mr;
    std::unique_ptr<MonBase>     cnt = std::make_unique<MonCounter<T> >(nam,desc,var);
    if ( this->have_rates )   {
      mr  = std::make_unique<MonRate<T> >   (this->ratePrefix+nam, desc, var, cnt.get());
    }
    this->add_counter(std::move(cnt), std::move(mr));
  }

  /// addEntry for arrays
  template<typename T> void CounterSubSys::add_counter(const std::string& nam, const std::string &desc,
						       const std::string &fmt, T const var, int siz) {
    std::unique_ptr<MonRateBase> mr;
    std::unique_ptr<MonBase>     cnt = std::make_unique<MonCounter<T> >(nam,desc,fmt,var,siz);
    if ( this->have_rates )   {
      mr  = std::make_unique<MonRate<T> >   (this->ratePrefix+nam, desc, fmt, var, siz);
    }
    this->add_counter(std::move(cnt), std::move(mr));
  }

  /// addEntry for pairs
  template<typename T> void CounterSubSys::add_counter(const std::string& nam, const std::string &desc, const T&  var1, const T&  var2) {
    std::pair<T*,T*> *p = new std::pair<T*,T*>((T*)&var1,(T*)&var2);
    std::unique_ptr<MonRateBase> mr;
    std::unique_ptr<MonBase>     cnt = std::make_unique<MonCounter<std::pair<T*,T*> > >(nam, desc, *p);
    this->add_counter(std::move(cnt), std::move(mr));
  }
}
