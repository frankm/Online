//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================

#include <utility>

/// Online namespace declaration
namespace Online  {

  /// specialization of MonCounter class for pairs
  template <typename T> struct MonCounter<std::pair<T*,T*>> : MonCounterBase {

    using pairType = std::pair<T*,T*>;
  MonCounter(const std::string& nam, const std::string& tit, const pairType& data )
    : MonCounterBase{Type(), nam, tit, std::max(8u, serialSize()), serialSize(), (pairType*)&data} {}

    MONTYPE Type() const {
      if constexpr (std::is_same_v<T, int>) {
	  return C_INTPAIR;
	} else if constexpr (std::is_same_v<T, long>) {
	  return C_LONGPAIR;
	} else {
	static_assert(false_v<T>, "Unsupported type for pair specialization of MonCounter");
      }
    }

    unsigned int serialSize()  const {
      return 2*sizeof(T); 
    }

    unsigned int serializeData(DimBuffBase *pp, const void *src, int)  const override {
      pairType* s = (pairType*)src;
      long*     d = add_ptr<long>(pp, pp->dataoff);
      *d = *s->first;
      *(d + 1) = *s->second;
      return pp->reclen;
    }

    void create_OutputService(const std::string &) override {
      this->dim_service.reset();
    }
  };
}
