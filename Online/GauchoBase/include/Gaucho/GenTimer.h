//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef GENTIMER_H
#define GENTIMER_H
#include "RTL/rtl.h"
#include "Gaucho/BRTL_Lock.h"
#include <sys/time.h>
#include <thread>
#define TIMER_PREC_SEC 0
#define TIMER_PREC_MILLI 1
#define TIMER_PREC_MICRO 2
#define TIMER_PREC_NANO 3
#define TIMER_TYPE_PERIODIC (1<<0)
#define TIMER_MODIFYER_SYNCHRONIZED (1<<1)

/// Online namespace declaration
namespace Online  {

  class GenTimer   {
  public:
    bool periodic;
    bool synched;
    bool forceExit   { true };
    bool timeout     { false };
  protected:
    unsigned long  period;

    unsigned long  m_dueTime    { 0 };
    unsigned long  m_lastfire { 0 };
    unsigned long  m_lastdelta { 0 };
    unsigned long *m_extlastdelta { nullptr };
    int            m_ThreadBalance  { 0 };
    bool           m_stopSeen   { false };
    pid_t          pid { 0 };
    std::unique_ptr<std::thread> m_thread;
    int            type;
    void           makeDeltaT();
  public:
    bool m_dontdimlock { true };
    GenTimer(int period = 10000,int typ=TIMER_TYPE_PERIODIC+TIMER_MODIFYER_SYNCHRONIZED);
    // Period is in milli-seconds...
    virtual ~GenTimer(void);
    virtual void timerHandler ( void ) = 0;
    virtual void start();
    virtual void startPeriodic(int period);
    virtual void stop();
    bool isRunning()   const   {   return this->m_thread.get() != nullptr && !this->forceExit; }
    void setExtLastDelta(unsigned long *a){m_extlastdelta = a;return;};
    unsigned long getDeltaTime(int incr=0);
    void *ThreadRoutine();
    unsigned long GetLastDelta(){return m_lastdelta;};
    int NanoSleep(struct timespec *req, struct timespec *rem);
  };
}
#endif
