//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef SOURCE_DIRECTORY__ONLINE_GAUCHO_GAUCHO_OBJMAP_H_
#define SOURCE_DIRECTORY__ONLINE_GAUCHO_GAUCHO_OBJMAP_H_

#include <memory>
#include <string>
#include <map>

/// Online namespace declaration
namespace Online   {

  class MonBase;
  typedef std::map<std::string,std::unique_ptr<MonBase> >  MonitorObjects;
}

#endif /* SOURCE_DIRECTORY__ONLINE_GAUCHO_GAUCHO_OBJMAP_H_ */
