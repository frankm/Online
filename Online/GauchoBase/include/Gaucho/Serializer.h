//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_SERIALIZER_H
#define ONLINE_GAUCHO_SERIALIZER_H

#include <string>
#include <vector>

/// Online namespace declaration
namespace Online  {

  /// Forward declarations
  class mem_buff;

  /// Object serialization base class
  class Serializer  {
  public:
    /// Default destructor
    virtual ~Serializer() = default;
    /// Serialize object set to local buffer
    virtual std::pair<size_t,void*> serialize_obj(size_t offset, bool clear=false) = 0;
    /// Serialize object set to external buffer
    virtual std::pair<size_t,void*> serialize_obj(mem_buff& buff, size_t offset, bool clear=false)  const = 0;
    /// Serialize single object identified by name to local buffer
    virtual std::pair<size_t,void*> serialize_obj(const std::string& name, size_t offset, bool clear=false) = 0;
    /// Serialize a set of objects identified by match to name to local buffer
    virtual std::pair<size_t,void*> serialize_obj(const std::vector<std::string> &nams, size_t offset, bool clear=false) = 0;
    /// Serialize a set of objects identified by name to local buffer
    virtual std::pair<size_t,void*> serialize_match(const std::string &match, size_t offset, bool clear=false) = 0;
    /// Serialize directory (inventory of objects identified by name to local buffer
    virtual std::pair<size_t,void*> serialize_dir(size_t offset) = 0;
    /// Access to the internal buffer
    virtual std::pair<size_t,const void*> data()   const = 0;
    /// Update expansions
    virtual void updateExpansions() = 0;
  };
}
#endif   // ONLINE_GAUCHO_SERIALIZER_H
