//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_TASKSAVETIMER_H
#define ONLINE_GAUCHO_TASKSAVETIMER_H

#include <Gaucho/GenTimer.h>
#include <Gaucho/HistSaver.h>
#include <Gaucho/MonSubSys.h>

/// Online namespace declaration
namespace Online   {

  class TaskSaveTimer : public GenTimer, public HistSaver  {
  protected:
    std::shared_ptr<MonSubSys>  m_subsys;

  public:
    TaskSaveTimer(std::shared_ptr<MonSubSys> tis, int period = 900);
    virtual ~TaskSaveTimer();
    void timerHandler() override;
  };
}
#endif
