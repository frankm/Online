//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef GAUCHO_RATEMGR_H
#define GAUCHO_RATEMGR_H 1

#include <string>

/// Online namespace declaration
namespace Online  {

  class MonitorClass;

  class RateMgr  {
  public:
    unsigned long deltaT = 0;
  private:
    MonitorClass* monClass { nullptr };
  public:

    RateMgr(MonitorClass* cl);
    virtual ~RateMgr() = default;
    void zero();
    void makeRates(unsigned long dt);
    void CreateOutputServices(const std::string& infix);
  };
}

#endif //GAUCHO_RATEMGR_H
