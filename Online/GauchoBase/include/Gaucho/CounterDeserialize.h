//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_COUNTERDESERIALIZE_H
#define ONLINE_GAUCHO_COUNTERDESERIALIZE_H

#include <string>
#include <memory>
#include <algorithm>

/// Online namespace declaration
namespace Online  {

  class CntrDescr  {
  public:
    std::string name      = "";
    unsigned int type     = 0;
    int i_data            = 0;
    unsigned int ui_data  = 0;
    long l_data           = 0;
    unsigned long ul_data = 0;
    float f_data          = 0.0;
    double d_data         = 0.0;
    std::unique_ptr<unsigned char[]> ptr {};
    int nel               = 0;
    std::pair<int,int> ip_data;
    union  {
      struct _avg_data {
	long   entries;
	double sum;
	double mean;
      } average;
      struct _stat_data {
	long   entries;
	double sum;
	double sum2;
	double mean;
      } stat;
      struct _sigma_data {
	long   entries;
	double sum;
	double sum2;
	double mean;
      } sigma;
      struct _binomial_data {
	long   entries;
	long   entries_true;
	long   entries_false;
      } binomial;
    } gaudi;
    std::pair<long,long> lp_data;
    CntrDescr() = default;
    ~CntrDescr() = default;
  };

  class CounterSerDes  {
  public:
    static CntrDescr *de_serialize(void* pointer, char* name = 0);
  };
}
#endif // ONLINE_GAUCHO_COUNTERDESERIALIZE_H
