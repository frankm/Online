//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_MONINFO_H
#define ONLINE_GAUCHO_MONINFO_H

#include <Gaucho/MonInfoHandler.h>
#include <dim/dic.hxx>
#include <string>

/// Online namespace declaration
namespace Online  {

  class MonInfo;
  class MonInfoHandler;

  class MonInfo : public DimUpdatedInfo  {
    MonInfoHandler* handler  { nullptr };
  public:
    virtual ~MonInfo() = default;
    MonInfo(const std::string& target, MonInfoHandler* handler = nullptr);
    MonInfo(const std::string& target, int period, MonInfoHandler* handler = nullptr);
    void infoHandler () override;
    static void setShutdownInProgress(bool value);
  };
}
#endif  // ONLINE_GAUCHO_MONINFO_H
