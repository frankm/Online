//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/HistTaskJson.h>
#include <RTL/rtl.h>
#if 0
#include <TH1D.h>
#include <TH2D.h>
#include <TRint.h>
#include <TProfile.h>
#include <TBrowser.h>
#include <TDirectory.h>
#endif
#include <iostream>
#include <iomanip>

using namespace Online;

static void print_tasks(const std::set<std::string>& tasks)   {
  size_t i = 0;
  for( const auto& t : tasks )
    ::printf("Task[%3ld]  %s\n", ++i, t.c_str());
}

int main(int argc, char *argv[]) // Taskname, DNS Node
{
  RTL::CLI cli(argc, argv, [] (){
      std::cout 
	<< " Usage: taskHistos -arg [-arg]  \n"
	<< "  -d(ns)=<dns>         DNS name to interrogate for services\n"
	<< "  -t(ask)=<task-name>  Supply task name for detailed counter printout\n"
	<< "  -s(how)              Display the retrieved histograms."
	<< std::endl;
    });
  std::string dns, task;
  bool show = cli.getopt("show",1);
  bool help = cli.getopt("help",1);
  cli.getopt("task", 1, task);
  cli.getopt("dns",  1, dns);
  if ( help )  {
    cli.call_help();
  }
  if ( dns.empty() )  {
    std::cout << "Insufficient Number of arguments" << std::endl;
    cli.call_help();
  }


  if ( task.empty() )   {
    std::set<std::string> tasks;
    HistTaskJson::taskList(dns,tasks);
    print_tasks(tasks);
  }
  else   {
    auto h = std::make_unique<HistTaskJson>(task, dns);
    std::vector<std::string> hists;
    hists.clear();
    int status = h->directory(hists);
    if (status == 1)  {
      printf("Task does not exist...\n");
      return 1;
    }
    for (std::size_t i=0; i < hists.size(); i++)
      std::cout << hists[i] << std::endl;

    std::cout << "Number of Histograms for Task " << task << ": " << hists.size() << std::endl;

    std::vector<std::string> hsts;
    std::cout << "Trying to retrieve the histograms" << std::endl;
    for (std::size_t i=0; i < hists.size(); i++)
      hsts.push_back(hists[i]);

    std::vector<nlohmann::json> robjs;
    h->histos(hsts, robjs);
    std::cout << "Retrieved " << robjs.size() << " Histograms" << std::endl;
    for (std::size_t i=0; i < robjs.size(); i++)      {
      std::cout	<< "Histogram [" << i << "]: " << hsts[i] << std::endl;
    }
    if ( show )    {
      for (std::size_t i=0; i < robjs.size(); i++)      {
	std::cout	<< "============================================================" << std::endl
			<< "Histogram [" << i << "]: " << hsts[i] << std::endl
			<< robjs[i]
			<< std::endl;
      }
#if 0
      int one = 1;
      char cmd[256];
      TH1D::SetDefaultSumw2();
      TH2D::SetDefaultSumw2();
      TProfile::SetDefaultSumw2();
      TRint theApp("App", &one, argv);
      ::lib_rtl_sleep(100);
      ::snprintf(cmd,sizeof(cmd),
		 "std::vector<TH1*>& gHistos = *(std::vector<TH1*>*)%p;",
		 (void*)&robjs);
      gInterpreter->ProcessLine(cmd);
      std::cout << std::endl
		<< "***** Use the ROOT interpreter variable " << std::endl
		<< "*****      'std::vector<TH1*>& gHistos'"  << std::endl
		<< "***** to access and draw histograms!"     << std::endl;
      new TBrowser();
      theApp.Run();
#endif
    }
  }
  return 0;
}
