#ifndef CINTBOOTSTRAP_H 
#define CINTBOOTSTRAP_H 1

#if defined(__clang__) || defined(__CLING__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winconsistent-missing-override"
#elif defined(__GNUC__) && __GNUC__ >= 5
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsuggest-override"
#endif

#include "Camera/MessagePresenter.h"
#include "Camera/ElogDialog.h"

#if defined(__clang__) || defined(__CLING__)
#pragma clang diagnostic pop
#elif defined(__GNUC__) && __GNUC__ >= 5
#pragma GCC diagnostic pop
#endif

#endif /*CINTBOOTSTRAP_H_*/
