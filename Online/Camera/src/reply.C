

#include "nio.h"

#include <string>
#include <iostream>
#include <list>
#include <vector>

#include <pthread.h>

#include <limits.h>
#include <stdlib.h>

using namespace std;

class reply : public proto{
  
private:
  
  std::string allowed;
  
  static const std::size_t BUFFERSIZE = 3000;

  std::string webdir;
  std::string nbuf;
  std::string buf;

  int u{0};

public:

  reply(nio * n,int unique):proto(n)
  {
    u = unique;

    if ( webdir.empty() ) { webdir.reserve(BUFFERSIZE); }
    if ( allowed.empty() ) { allowed.reserve(512); }
    
    const auto wdir = mapc.get("websdir");
    const auto rdir = mapc.get("httproot");
    
    if (rdir != "")
    {
      allowed = rdir;
    }
    else 
    {
      allowed  = std::string(getenv("PWD"));
      allowed += "/data/";
    }
    if (wdir != ""){
      webdir = wdir;
    } 
    else {
      webdir = std::string( getenv("PWD") );
    }
  };

  void * start();
  void end();
  void reset(){
    proto::reset();
  }
};

void * reply::start(){
  //printf("starting communication\n");

  if ( nbuf.empty() ) { nbuf.reserve(BUFFERSIZE); }
  if ( buf.empty()  ) { buf.reserve(2*BUFFERSIZE); }
  if ( allowed.empty() ) { allowed.reserve(512); }

  auto r = getline((char*)buf.c_str(),2500);
  if (r<=0) return nullptr;  
  while ( (buf[r-1] =='\r') || (buf[r-1] == '\n') ){
    buf[r--] = '\0';
  }
  //  printf(">%s<\n",buf);
  if (strncmp(buf.c_str(),"GET ",4)!=0) return nullptr;
  
  string instr = buf;
  
  if (sscanf(buf.c_str(),"%*s %s",(char*)nbuf.c_str())!=1) return nullptr;
  
  buf  = webdir;
  buf += nbuf;

  string filestr = buf;
  // cout <<">"<< filestr<<"<" <<endl;
  
  
  char pbuf[PATH_MAX];
  if (realpath(buf.c_str(), pbuf)==nullptr) {
    std::cerr << "Could not resolve path "<<buf<<std::endl;
    perror("realpath");
    return nullptr;
  }
  
  if (strncmp(pbuf,allowed.c_str(),strlen(allowed.c_str()))!=0){
    cerr << "Not allowed "<<buf<<endl;
    cerr << pbuf<< " not in " <<allowed<<endl;
    return nullptr;
  }
  
  FILE *F = fopen(filestr.c_str(),"rb");
  
  if (!F) return nullptr;

  {
    char _buf[512];
    while ((r=fread(_buf,1,512,F))>0)
      getnio()->wr(_buf,r);
  }
    
  return nullptr;
}

void reply::end(){
  getnio()->shut_close();
}
