//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <string>
#include <thread>
#include <mutex>
#include <vector>
#include "string.h"
#include "stdio.h"
#include "unistd.h"
#include "stdlib.h"
#include "ctype.h"
#include "dim/dis.hxx"
#include "../NewLib/NodeThread.h"
#include "../NewLib/log.h"

//#include "src/NewLib/GlobalVars.h"
extern char *MyUTGID;
using namespace std;
std::string myUTGID;
class lan;
class LANPlus;
int verbose;
vector<string> StrSplit(string &s, char* tok)
{
  vector<string> ret;
  string ss = s;
  size_t tk = ss.find(tok);
  while(tk!=string::npos)
  {
    ret.push_back(ss.substr(0,tk));
    ss=ss.substr(tk+1,string::npos);
    tk = ss.find(tok);
  }
  ret.push_back(ss);
  return ret;
}
void ToLower(string &s)
{
  for (size_t i=0;i<s.size();i++)
  {
    s[i]=tolower(s[i]);
  }
}
void ToUpper(string &s)
{
  for (size_t i=0;i<s.size();i++)
  {
    s[i]=toupper(s[i]);
  }
}
class NodeListElement : public NodeConfiguration
{
  public:
    NodeListElement(string &N, string &U, string &P, string *sp,string &IF,
        int statepoll=1, int setuppoll=10):NodeConfiguration(N,U,P,sp,IF,statepoll,setuppoll)
    {
      m_thread = 0;
    };
    NodeListElement():NodeConfiguration()
    {
      m_thread = 0;
    };
    thread *m_thread;
};
#if 0
void FarmSetup(char * farms, vector<NodeListElement*> &nodelist, NodeListElement &templ)
{
  size_t k;
  char r;
  char nodn[256];
  for (k = 0; k < strlen(farms); k++)
  {
    r = farms[k];
    switch (r)
    {
      case 'a':
      case 'b':
      case 'c':
      case 'd':
      case 'e':
      {
        for (int j = 1; j <= 10; j++)
        {
          for (int i = 1; i <= 28; i++)
          {
            sprintf(nodn, "hlt%c%02d%02d-IPMI", r, j, i);
//            nodes.push_back(string(nodn));
            string nn = nodn;
            ToUpper(nn);
            nodelist.push_back(new NodeListElement(nn,templ.UserName,templ.Password,templ.ServicePrefix));
//            threads.push_back(new thread(NodeThread, new NodeConfiguration(nn,uname,pword,ServicePrefix)
//                ,setup_fail,setup_fail_mtx));
          }
        }
        break;
      }
      case 'f':
      {
        for (int j = 1; j <= 12; j++)
        {
          int nods = 32;int fPrintf(const char *format, ...);
          if (j == 7)
            nods = 28;
          for (int i = 1; i <= nods; i++)
          {
            sprintf(nodn, "hltf%02d%02d-IPMI", j, i);
            string nn = nodn;
            ToUpper(nn);
//            nodes.push_back(string(nodn));
            nodelist.push_back(new NodeListElement(nn,templ.UserName,templ.Password,templ.ServicePrefix));
//            threads.push_back(new thread(NodeThread, new NodeConfiguration(nn,uname,pword,ServicePrefix)
//                ,setup_fail,setup_fail_mtx));
          }
        }
      }
    }
  }

}
#endif
size_t fnd;
void ParseConfigFile(char *supFile,vector<NodeListElement*> &nodelist, NodeListElement &templ)
{
  FILE * f;
  char lin[2048];
  f=fopen(supFile,"r");
  string line;
  line.reserve(2048);
//  char *stat;
  if (f == 0) return;
  fgets(lin,2048,f);
  while (!feof(f))
  {
    line = lin;
    line = line.substr(0,line.find("\n"));
    fnd = line.find("#");

    line = line.substr(0,fnd);
    if (line.size() == 0)
    {
      line.reserve(2048);
      fgets(lin,2048,f);
      continue;
    }
    vector<string> splt = StrSplit(line,(char*)"=");
    if (splt.size()>1)
    {
      if (splt[0].find("IPMI_USER")!=string::npos)
      {
        string opt = splt[1];
        size_t blk;
        blk=opt.find(" ");
        while (blk != string::npos)
        {
          opt.replace(blk,1,"");
          blk=opt.find(" ");
        }
        templ.UserName = opt;
      }
      else if (splt[0].find("IPMI_PASSWD")!=string::npos)
      {
        string opt = splt[1];
        size_t blk;
        blk=opt.find(" ");
        while (blk != string::npos)
        {
          opt.replace(blk,1,"");
          blk=opt.find(" ");
        }
        templ.Password = opt;
      }
      else if (splt[0].find("IPMI_INTERFACE")!=string::npos)
      {
        string opt = splt[1];
        size_t blk;
        blk=opt.find(" ");
        while (blk != string::npos)
        {
          opt.replace(blk,1,"");
          blk=opt.find(" ");
        }
        ToUpper(opt);
        templ.Interface = opt;
      }
      else if (splt[0].find("IPMI_CIPHER")!=string::npos)
      {
        string opt = splt[1];
        size_t blk;
        blk=opt.find(" ");
        while (blk != string::npos)
        {
          opt.replace(blk,1,"");
          blk=opt.find(" ");
        }
        templ.cipher = atoi(opt.c_str());
      }
      else if (splt[0].find("IPMI_CMD")!=string::npos)
      {
        Printf("Option IPMI_CMD ignored");
      }
      else
      {
        Printf("Illegal Option %s",line.c_str());
      }
      line.reserve(2048);
      fgets(lin,2048,f);
      continue;
    }
    else
    {
//hostName,userName,passWord,port,authType,privLvl,oemType
      vector<string> splt = StrSplit(line,(char*)",");
      string nn="";
      NodeListElement *el;
      if (splt.size()>0)
      {
        el = new NodeListElement(nn,templ.UserName,templ.Password,templ.ServicePrefix,templ.Interface);
      }
      else
      {
        line.reserve(2048);
        fgets(lin,2048,f);
        continue;
      }
      for (size_t i=0;i<splt.size();i++)
      {
        switch(i)
        {
          case 0:
          {
            string nname = splt[i];
//            ToUpper(nname);
            *el = templ;
            vector<string> nsplt = StrSplit(nname,(char*)"/");
            switch(nsplt.size())
            {
              case 1:
              {
                ToUpper(nsplt[0]);
                el->NodeName = nsplt[0];
                break;
              }
              case 2:
              {
                ToUpper(nsplt[0]);
                el->NodeName = nsplt[0];
                el->Interface = nsplt[1];
                break;
              }
              case 3:
              {
                ToUpper(nsplt[0]);
                el->NodeName = nsplt[0];
                el->Interface = nsplt[1];
                el->cipher = atoi(nsplt[2].c_str());
                break;
              }
              case 0:
              {
                break;
              }
              default:
              {
                break;
              }
            }
            break;
          }
          case 1:
          {
            if (splt[i].find("NULL") == string::npos)
            {
              el->UserName = splt[i];
            }
            break;
          }
          case 2:
          {
            if (splt[i].find("NULL") == string::npos)
            {
              el->Password = splt[i];
            }
            break;
          }
          case 3:
          {
            if (splt[i].find("NULL") == string::npos)
            {
              Printf("Port Option in Node definition ignored\n");
            }
            break;
          }
          case 4:
          {
            if (splt[i].find("NULL") == string::npos)
            {
              Printf("authType Option in Node definition ignored\n");
            }
            break;
          }
          case 5:
          {
            if (splt[i].find("NULL") == string::npos)
            {
              Printf("privLvl Option in Node definition ignored\n");
            }
            break;
          }
          case 6:
          {
            if (splt[i].find("NULL") == string::npos)
            {
              Printf("oemType Option in Node definition ignored\n");
            }
            break;
          }
          default:
          {
            Printf("Too many items in node definition. Subsequent items ignored\n");
            break;
          }
        }
      }
      nodelist.push_back(el);
    }
    line.reserve(2048);
    fgets(lin,2048,f);
  }
  fclose(f);
}

int main(int argc, char **argv)
{
#define OPTIONS "r:c:d:s:n:p:l"
//  int status;
  string *ServerName = new string("MyIPMIServer");
  string *ServicePrefix = new string("MyIPMIServer");
  string *setup_fail=new string("");
  string stat_fail;
  mutex *setup_fail_mtx = new mutex;;
  vector<string> nodes;
  vector<thread*> threads;
  string uname("root");
  string pword("lhcbipmi");
//  string nnod1("hlta0101");
//  string nnod2("hlta0906");
  vector<NodeListElement*> nodelist;
  if (argc == 1)
  {
    Printf("Usage: ipmiSrv <options>\n");
    Printf("Options:\n");
    Printf("\t-r <Farm Rows> e.g. abc\n");
    Printf("\t-c <configuration file>\n");
    Printf("\t-d <Polling delay in seconds>\n");
    Printf("\t-s <Setup retry delay> \n");
    Printf("\t-n <Dim Server Name>\n");
    Printf("\t-p <Dim Node Service Prefix>\n");
    Printf("\nNote: options -r and -c are mutually exclusive\n");
    return 0;
  }
  int c;
  char *farms=0;
  char *supFile=0;
  int pollDel=1;
  int supDel=10;
  char *sname=0;
  char *servpf=0;
  log_init(0,0,0);
  char *utg = getenv("UTGID");
  if (utg == 0)
  {
    pid_t pid=getpid();
    myUTGID = "p_"+to_string(pid);
  }
  else
  {
    myUTGID = utg;
  }
  MyUTGID = (char*)myUTGID.c_str();
  while ((c = getopt(argc,argv,OPTIONS)) != -1)
  {
    switch(c)
    {
      case 'r':
      {
        farms = optarg;
        break;
      }
      case 'c':
      {
        supFile = optarg;
        break;
      }
      case 'd':
      {
        sscanf("%d",optarg,&pollDel);
        break;
      }
      case 's':
      {
        sscanf("%d",optarg,&supDel);
        break;
      }
      case 'n':
      {
        sname = optarg;
        ServerName = new string(sname);
        break;
      }
      case 'p':
      {
        servpf = optarg;
        ServicePrefix = new string(servpf);
        break;
      }
      case 'l':
      {
        int lvl;
        sscanf("%d",optarg,&lvl);
        log_level_set(lvl);
        break;
      }
    };
  };
  if ((farms!=0) && (supFile != 0))
  {
    Printf ("the Farms and configuration file options are mutually exclusive\n");
    return 0;
  }
  DimServer::autoStartOn();
  DimServer::start((char*)ServerName->c_str());
//#if 0
//#endif
//  thread t1 = thread(NodeThread,nnod1);
//  thread t2 = thread(NodeThread,nnod2);
//  new thread(NodeThread,"hltd0112");
//  new thread(NodeThread,"hlte0602");
//  new thread(NodeThread,"hlte0603");
//  new thread(NodeThread,"hltd0922");
  string oldstr;
  string failsvcname = *ServerName;
  failsvcname +="/SetupFaileds";
  string nn="";
  string ln = "";
  NodeListElement NodeTemplate(nn,uname,pword,ServicePrefix,ln);
  ParseConfigFile(supFile,nodelist,NodeTemplate);
//  for (size_t ii=0;ii<nodelist.size();ii++)
//  {
//    Printf ("%s\n",nodelist[ii]->NodeName.c_str());
//  }
  DimService servc(failsvcname.c_str(),"C",(char*)setup_fail->c_str(),1);

#define IF_lan 1
#define IF_lanplus 2
//  int IFdialect;
  for (size_t i=0;i<nodelist.size();i++)
  {
    if (nodelist[i]->Interface == "LANPLUS")
    {
      nodelist[i]->m_thread = new thread(NodeThread<LANPlus>,nodelist[i],setup_fail,setup_fail_mtx);
    }
    else if (nodelist[i]->Interface == "LAN")
    {
      nodelist[i]->m_thread = new thread(NodeThread<lan>,nodelist[i],setup_fail,setup_fail_mtx);
    }
    else
    {
      nodelist[i]->m_thread = new thread(NodeThread<lan>,nodelist[i],setup_fail,setup_fail_mtx);
    }
  }
  while (1)
  {
//    sleep(0xffffffff);
    sleep(2);
    setup_fail_mtx->lock();
    if (oldstr != *setup_fail)
    {
      servc.updateService((char*)setup_fail->c_str(),setup_fail->size());
//      Printf("===> Setup Failing Nodes: %s\n",setup_fail->c_str());
      oldstr=*setup_fail;
    }
    setup_fail_mtx->unlock();
  }
    return 0;
}
