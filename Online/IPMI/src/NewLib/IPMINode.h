//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef IPMISRV_H
#define IPMISRV_H
#include <string>
using namespace std;
#include "NodeConfiguration.h"
//  #include "ipmilight.h"
enum IPMIState
{
  uninited,
  inited,
  on,
  off
};
enum PowerState
{
  Power_unknown = -1,
  Power_off,
  Power_on
};
class DimService;
class DimCommand;
template <class T> class ipmi_intf;
template <class T> class IPMINode
{
  public:
#include "newnew.h"
    string m_Name;
    string m_intName;
    string m_username;
    string m_password;
    ipmi_intf<T> *m_ipmiInterface;
    ipmi_intf<T> *m_retintf;
    IPMINode (string &nname,string &uname,string &passw);
    ~IPMINode();
    IPMIState m_state;
    PowerState m_pstate;
    DimService *m_service;
    DimService *m_lastUpdate;
    DimCommand *m_cmd;
    string m_servName;
    string m_cmdName;
    int m_svalue;
    int m_lastread;
    int turnOn();
    int turnOff();
    int cycle();
    int reset();
    int softOff();
    int getPowerState();
    int setup(char *DimserverName, NodeConfiguration &conf);
    int shutdown();
};
#endif
