/*
 * LANBase.h
 *
 *  Created on: Jul 16, 2021
 *      Author: beat
 */

#ifndef ONLINE_IPMI_SRC_NEWLIB_LANBASE_H_
#define ONLINE_IPMI_SRC_NEWLIB_LANBASE_H_
struct ipmi_rs;
struct ipmi_intf;
struct LANBase
{
    virtual ~LANBase();
    virtual int open(struct ipmi_intf *intf);
    virtual int setup(struct ipmi_intf * intf)=0;
    virtual int keepalive(struct ipmi_intf * intf)=0;
    virtual int send_packet(struct ipmi_intf * intf, uint8_t * data, int data_len)=0;
    virtual struct ipmi_rs * recv_packet(struct ipmi_intf * intf)=0;
    virtual struct ipmi_rs * poll_recv(struct ipmi_intf * intf)=0;
    virtual struct ipmi_rs * send_ipmi_cmd(struct ipmi_intf * intf, struct ipmi_rq * req)=0;
    virtual struct ipmi_rs * send_payload(struct ipmi_intf * intf,
                                                      struct ipmi_v2_payload * payload)=0;
    virtual void getIpmiPayloadWireRep(
                                      struct ipmi_intf       * intf,
                                      struct ipmi_v2_payload * payload,  /* in  */
                                      uint8_t  * out,
                                      struct ipmi_rq * req,
                                      uint8_t    rq_seq,
                                      uint8_t curr_seq)=0;
    virtual void getSolPayloadWireRep(
                                      struct ipmi_intf       * intf,
                                     uint8_t          * msg,
                                     struct ipmi_v2_payload * payload)=0;
    virtual void read_open_session_response(struct ipmi_rs * rsp, int offset)=0;
    virtual void read_rakp2_message(struct ipmi_rs * rsp, int offset, uint8_t alg)=0;
    virtual void read_rakp4_message(struct ipmi_rs * rsp, int offset, uint8_t alg)=0;
    virtual void read_session_data(struct ipmi_rs * rsp, int * offset, struct ipmi_session *s)=0;
    virtual void read_session_data_v15(struct ipmi_rs * rsp, int * offset, struct ipmi_session *s)=0;
    virtual void read_session_data_v2x(struct ipmi_rs * rsp, int * offset, struct ipmi_session *s)=0;
    virtual void read_ipmi_response(struct ipmi_rs * rsp, int * offset)=0;
    virtual void read_sol_packet(struct ipmi_rs * rsp, int * offset)=0;
    virtual struct ipmi_rs * recv_sol(struct ipmi_intf * intf)=0;
    virtual struct ipmi_rs * send_sol(
                                                  struct ipmi_intf * intf,
                                                  struct ipmi_v2_payload * payload)=0;
    virtual int check_sol_packet_for_new_data(
                                         struct ipmi_intf * intf,
                                         struct ipmi_rs *rsp)=0;
    virtual void ack_sol_packet(
                                struct ipmi_intf * intf,
                                struct ipmi_rs * rsp)=0;
    virtual void set_max_rq_data_size(struct ipmi_intf * intf, uint16_t size)=0;
    struct ipmi_rq_entry *ipmi_lanplus_build_v15_ipmi_cmd(
                                    struct ipmi_intf * intf,
                                    struct ipmi_rq * req);

};


#endif /* ONLINE_IPMI_SRC_NEWLIB_LANBASE_H_ */
