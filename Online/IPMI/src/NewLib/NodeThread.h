//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * NodeThread.h
 *
 *  Created on: Feb 11, 2016
 *      Author: beat
 */

#ifndef SOURCE_DIRECTORY__ONLINE_IPMI_SRC_NEWLIB_NODETHREAD_H_
#define SOURCE_DIRECTORY__ONLINE_IPMI_SRC_NEWLIB_NODETHREAD_H_

#include <string>
#include <mutex>
#include "NodeConfiguration.h"
using namespace std;
template <class T> void NodeThread(NodeConfiguration *config, string *setup_fail,
    mutex *setup_fail_mtx);
#endif /* SOURCE_DIRECTORY__ONLINE_IPMI_SRC_NEWLIB_NODETHREAD_H_ */
