//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Context.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_ZMQ_CONTEXT_H
#define ONLINE_DATAFLOW_ZMQ_CONTEXT_H

#include <ZMQ/zmq.hpp>

/// ZMQ namespace definition
namespace ZMQ  {

  /// ZeroMQ context wrapper
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup DATAFLOW
   */
  class Context  {
  public:
    /// Satndard constructor (empty)
    Context() {}
    /// Context accessor
    static zmq::context_t& context();
    /// ZeroMQ socket creator
    static zmq::socket_t socket(int typ);
    /// Property setter: Set number of threads
    static void setNumberOfThreads(int nthreads);
  };
  inline Context zmq()  { return Context(); }
}       // End namespace ZMQ     
#endif  /* ONLINE_DATAFLOW_ZMQ_CONTEXT_H  */
