//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  NETSelector.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_NETSELECTOR_H
#define ONLINE_DATAFLOW_NETSELECTOR_H

// Framework include files
#include <Dataflow/EventRunable.h>
#include <MBM/Requirement.h>
#include <CPP/Interactor.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <list>
#include <mutex>
#include <ctime>

///  Online namespace declaration
namespace Online  {

  /// Buffer Manager service for the dataflow
  /**
   * @author  Markus Frank
   * @version 1.0
   */
  class NETSelector
    : public EventRunable,  public CPP::Interactor   {

  public:
    typedef Context::EventData     EventData;
    typedef MBM::Requirement       Requirement;

    /// Network data entry descriptor
    /**
     * @author  Markus Frank
     * @version 1.0
     */
    class RecvEntry : public EventData {
    public:
      std::string source;
      /// Initializing constructor
      RecvEntry(const std::string& s, int len, char* ptr);
      /// Default destructor
      ~RecvEntry();
    public:
      RecvEntry() = delete;
      RecvEntry(RecvEntry&& c) = delete;
      RecvEntry(const RecvEntry& c) = delete;
      RecvEntry& operator=(RecvEntry&& c) = delete;
      RecvEntry& operator=(const RecvEntry& c) = delete;
    };

  protected:
    /// Property: Input buffer name
    std::string  m_input;
    /// Decode data buffer
    bool         m_decode;
    /// Property to manipulate handling of event timeouts
    bool         m_handleTMO;
    /// Property to indicate to pause on error
    bool         m_gotoPause;
    /// Property to sleep for some time before pause [unit: milli-seconds]
    int          m_pauseSleep;
    /// Property Event type of received event
    int          m_event_type;
    /// Property Triggermask of received event
    std::vector<int> m_event_mask;
    /// Property: Cancel event access on partner task death
    int          m_cancelDeath;
    /// Property: Time delay to regularly renew event request
    int          m_eventTimeout;
    /// Property: Number of receive threads used (default: 2)
    int          m_numThreads;
    /// Property: Printout frequence
    float        m_printFreq;
    /// Requirement properties
    std::string  m_Rqs[8];

    /// Time stamp of the last event received
    time_t       m_lastEvent;

    /// Decoded requirements
    Requirement  m_Reqs[8], m_request;
    /// Number of requirements
    int          m_nreqs;

    /// Monitoring quantity: Number of events requested
    int          m_reqCount;
    int          m_recvCount;
    int          m_recvError;
    long         m_recvBytes;

    /// Lock handle to suspend/resume operations
    lib_rtl_event_t m_haveEvent;
    lib_rtl_event_t m_freeEvent;
    std::mutex      m_dataLock;
    std::list<RecvEntry*> m_data;
    bool            m_cancelled;

  public:

    /// Start network activity
    virtual int net_start() = 0;
    /// Rearm network event receiving
    virtual int net_rearm() = 0;
    /// Shutdown network event receiving
    virtual int net_shutdown() = 0;

    /** Event access interface for sub-classes */
    /// Event access interface: Get next event from wherever
    virtual int getEvent(Context::EventData& event)  override;
    /// Event access interface: Release event. Default implementation is empty
    virtual int freeEvent(Context::EventData& event)  override;
    /// Event access interface: Shutdown event access in case of forced exit request
    virtual int shutdown()  override;
    /// Event access interface: Rearm event request to data source
    virtual int rearmEvent()  override;
    /// Event access interface: Cancel I/O operations of the dataflow component
    virtual int cancelEvents()  override;

    /// Notification callback when event provider dies
    void taskDead(const std::string& who);
    /// Notification callback to handle event data
    void handleData(const std::string& src, size_t siz, char* buff);

  public:
    /// Initializing constructor
    NETSelector(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~NETSelector();

    /// Initialize the component
    virtual int initialize()  override;
    /// Start the data flow component.
    virtual int start()  override;
    /// Stop the data flow component.
    virtual int stop()  override;
    /// Finalize the component
    virtual int finalize()  override;
    /// Interactor handler routine
    virtual void handle(const CPP::Event& ev)  override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;

  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_NETSELECTOR_H
