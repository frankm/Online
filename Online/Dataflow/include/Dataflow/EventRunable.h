//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  EventRunable.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_EVENTRUNABLE_H
#define ONLINE_DATAFLOW_EVENTRUNABLE_H

// Framework include files
#include <Dataflow/DataflowComponent.h>

// C/C++ include files
#include <memory>
#include <thread>
#include <mutex>

///  Online namespace declaration
namespace Online  {

  /// Event processing service
  /**
   * @author  Markus Frank
   * @version 1.0
   */
  class EventRunable : public DataflowComponent  {
  protected:
    /// Event processing flags
    enum ProcessFlags {
      PROCESS_EVENTS = 1<<0,
      HALT_EVENTS = 1<<1,
      STOP_EVENTS = 1<<2
    };

    /// Event data guard with possibility to copy to local buffers
    /**
     * @author  Markus Frank
     * @version 1.0
     */
    struct DataGuard : public Context::EventData  {
      bool owns;
      /// Default constructor
      DataGuard();
      /// Default destructor
      ~DataGuard()  { if ( owns && data ) delete [] data; }
      /// Copy the event data to local buffer
      void copy();
    };

    /// Event loop thread
    std::shared_ptr<std::thread> m_loop;
    /// Event loop lock
    std::mutex m_lock;
    /// Property: Number of allowed errors before exiting the event loop
    long m_nerrStop     = -1;
    /// Property: Force event loop exit after a timeout
    int  m_forceTMOExit = 0;
    /// Property: Copy the event data to local buffers if requested
    int  m_copyData     = false;
    /// Property: Delay "stop" transition in seconds
    int  m_delayStop    = 0;
    /// Property: Delay "cancel" transition in seconds
    int  m_delayCancel  = 0;
    
    /// Monitoring item: Number of event started to process
    long m_evtIN        = 0;
    /// Monitoring item: Number of events finished to process
    long m_evtOUT       = 0;
    /// Monitoring quantity: Number of events requested
    long m_reqCount     = 0;
    /// Monitoring quantity: Number of events processing
    long m_evtCount     = 0;
    /// Monitoring quantity: Number of events freed/released
    long m_freeCount    = 0;

    /// Monitoring item: Number of event processing errors
    long m_errCount     = 0;

    /// Cancellation time
    time_t m_cancelled  = 0;
    /// Processing status
    int  m_process      = STOP_EVENTS;
    /// Flag: force exit after a DAQ_EXIT incident
    bool m_forceExit    = false;
    /// Flag: indicates the existence of a timeout
    bool m_eventTMO     = false;


    /** Event access interface for sub-classes */
    /// Event access interface: Check if the event loop should be continued
    virtual int continueProcessing();
    /// Event access interface: Execute the event loop
    virtual int processEvents();
    /// Event access interface: Delayed cancel of event processing
    virtual int cancelEvents();

    /// Event access interface: Rearm event request to data source
    virtual int rearmEvent();
    /// Event access interface: Get next event from wherever
    virtual int getEvent(Context::EventData& event) = 0;
    /// Event access interface: Release event. Default implementation is empty
    virtual int freeEvent(Context::EventData& event);
    /// Event access interface: Shutdown event access in case of forced exit request
    virtual int shutdown();

  public:
    /// Initializing constructor
    EventRunable(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~EventRunable();
    /// Initialize the component
    virtual int initialize()  override;
    /// Start the data flow component.
    virtual int start()  override;
    /// Stop the data flow component.
    virtual int stop()  override;
    /// Finalize the component
    virtual int finalize()  override;
    /// Cancel I/O operations of the dataflow component
    virtual int cancel()  override;
    /// Execute runable. Start processing thread
    virtual int run()  override;
    /// Pause the component. Halts the event loop after processing the current event
    virtual int pause()  override;
    /// Re-enable the event loop and continue processing
    virtual int continuing()  override;

    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;
  };

  /// Default constructor
  inline EventRunable::DataGuard::DataGuard() 
    : Context::EventData(), owns(false) {}

}      // end namespace Online

#endif //  ONLINE_DATAFLOW_EVENTRUNABLE_H
