//	============================================================
//
//	FileWriterSvc.h
//	------------------------------------------------------------
//
//	Package   : GaudiOnline
//
//	Author    : Markus Frank
//
//	===========================================================
#ifndef ONLINE_DATAFLOW_TRIGGERBITCOUNTER
#define ONLINE_DATAFLOW_TRIGGERBITCOUNTER 1

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <Dataflow/FileWriterMgr.h>
#include <Tell1Data/Tell1Bank.h>
#include <RTL/rtl.h>

#include <Dataflow/FileWriterMgr.h>
#include <map>
#include <list>
#include <ctime>
#include <cstring>
#include <filesystem>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */


// Forward declarations
namespace MBM
{
  class Consumer;
  class EventDesc;
}

/// Online namespace declaration
namespace Online
{
  template<typename T> class RunListItem
  {
    public:
      int    m_runno = 0;
      T      m_count = 0;
      time_t m_LastCnt = 0;
      RunListItem(int runno, T &Count, time_t LastCnt)      {
        m_runno = runno;
        m_count = Count;
        m_LastCnt = LastCnt;
      }
      RunListItem(int runno)  {
        m_runno = runno;
      }
      void reset()   {
        m_runno = 0;
        m_LastCnt = 0;
        memset(&m_count,0,sizeof(T));
      }
  };

  template<typename T> class RunMap: public std::map<unsigned int, void*>
  {
    public:
      FILE *m_f;
      std::string m_fn;
      int m_Fdesc;
      void *m_RunPtr;
      size_t m_secsiz;
      RunListItem<T> *m_lastPtr;
      void SetFile(std::string fn)
      {
        m_fn = fn;
        std::string dyr = std::filesystem::path(m_fn).parent_path().string();
        try
        {
          std::filesystem::create_directories(dyr);
        } catch (...)
        {
          ::lib_rtl_output(LIB_RTL_FATAL, "Cannot Create Directory %s\n",
              dyr.c_str());
        }
      }
      void OpenBackingStore()
      {
        size_t page_size = (size_t) sysconf (_SC_PAGESIZE);
        m_secsiz = (10000*sizeof(RunListItem<T>)+page_size)& ~page_size;
        m_Fdesc = open(m_fn.c_str(),O_RDWR+O_CREAT+O_EXCL,S_IRWXU+S_IRWXG+S_IRWXO);
        if (m_Fdesc >=0)  // File didn't exist, was created..
        {
          close(m_Fdesc);
          int status = truncate(m_fn.c_str(),m_secsiz);
          if (status != 0)
          {
            int ierr = errno;
            ::lib_rtl_output(LIB_RTL_FATAL, "Cannot extend file %s Errno %d\n",m_fn.c_str(),ierr);
          }
          m_Fdesc = open(m_fn.c_str(),O_RDWR+O_CREAT,S_IRWXU+S_IRWXG+S_IRWXO);
          if (m_Fdesc < 0)
          {
            int ierr = errno;
            ::lib_rtl_output(LIB_RTL_FATAL, "Cannot Open file %s Errno %d\n",m_fn.c_str(),ierr);
          }
          m_RunPtr = mmap(0,m_secsiz,PROT_READ+PROT_WRITE,MAP_SHARED,m_Fdesc,0);
          if (m_RunPtr==MAP_FAILED)
          {
            int ierr = errno;
            ::lib_rtl_output(LIB_RTL_FATAL, "Cannot map file %s Errno %d\n",m_fn.c_str(),ierr);
          }
          memset(m_RunPtr,0,m_secsiz);
          this->clear();
          m_lastPtr = (RunListItem<T> *)m_RunPtr;
        }
        else
        {
          this->clear();
          m_Fdesc = open(m_fn.c_str(),O_RDWR+O_CREAT,S_IRWXU+S_IRWXG+S_IRWXO);
          if (m_Fdesc <0)
          {
            ::lib_rtl_output(LIB_RTL_FATAL, "Cannot Open file %s\n",m_fn.c_str());
            m_RunPtr = 0;
          }
          else
          {
            m_RunPtr = mmap(0,m_secsiz,PROT_READ+PROT_WRITE,MAP_SHARED,m_Fdesc,0);
            RunListItem<T> *rptr = (RunListItem<T> *)m_RunPtr;
            m_lastPtr = rptr;
            while (rptr->m_runno >0)
            {
              insert(std::make_pair(rptr->m_runno,rptr));
              rptr++;
              m_lastPtr = rptr;
            }
          }
        }
      }
    RunListItem<T> *NewEntry(int rn)
    {
      RunListItem<T> *p = m_lastPtr;
      m_lastPtr = p+1;
      p->reset();
      p->m_runno = rn;
      return p;
    }
    void Sync()
    {
      msync((void*)m_RunPtr,m_secsiz,MS_ASYNC);
    }
  };

  class TriggerBitCounter: public FileWriterMgr::Processor
  {
    protected:
      /// Monitoring quantity: Number of Event in
      long m_EvIn;
      std::string m_BackupFile;
      std::string m_BackupDirectory;

    public:

      ///Clear Counters
      void clearCounters();
      /// Standard Constructor
      TriggerBitCounter(const std::string& name, Context& svc);
      /// Standard Destructor
      virtual ~TriggerBitCounter();
      /// Service overload: initialize()
      virtual int initialize() override;
      /// Service overload: start()
      virtual int start() override;
      /// Service overload: stop()
      virtual int stop() override;
      /// Service overload: finalize()
      virtual int finalize() override;
  };
}      // End namespace Online
#endif //  ONLINE_DATAFLOW_TRIGGERBITCOUNTER
