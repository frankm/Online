//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MBMClient.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_MBMCLIENT_H
#define ONLINE_DATAFLOW_MBMCLIENT_H

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <MBM/mepdef.h>
#include <MBM/bmdef.h>

// C/C++ include files
#include <map>

// Forward declarations
namespace MBM {
  class Producer;
  class Consumer;
  class Requirement;
}

///  Online namespace declaration
namespace Online  {

  /// Buffer Manager service for the dataflow
  /** @class MBMClient MBMClient.h Dataflow/MBMClient.h
   *
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class MBMClient : public DataflowComponent  {

  public:
    typedef std::map<std::pair<std::string,std::string>, MBM::Producer*> SharedProducers;
    typedef std::vector<std::string>               ConsRequirement;
    typedef std::map<std::string, ConsRequirement> ConsRequirements;
    typedef std::map<std::string,BMID>             MappedBuffers;

  private:
    /// Property: Process name used to include into MEP/MBM buffers
    std::string                m_procName         {};
    /// Property: partition name used to connect to buffer managers (If set overrides partition identifier)
    std::string                m_partitionName    {};
    /// Property: Allows to choose if the buffers should be connected on "initialize" or "start"
    std::string                m_connectWhen      {};
    /// Property: Allows to choose if the buffers should be disconnected on "finalize" or "stop"
    std::string                m_disconnectWhen   {};
    /// Property: Buffer manager communication type (FIFO or ASIO)
    std::string                m_communication    {};
    /// Property: Container of buffer names to connect on initialize
    std::vector<std::string>   m_buffers          {};
    /// Property: Container of consumer requirements. Only valid if the buffers are held!
    ConsRequirements           m_consRequirements {};
    /// Property: Flag to indicate if buffer names should contain partition ID
    bool                       m_partitionBuffers {false};
    /// Property: partition identifier used to connect to buffer managers
    int                        m_partitionID      {0};
    /// Property: Flag to enable/disable buffer initialization and access
    int                        m_enabled          {1};
    /// Property: Flag to enable/disable MBM cancel requests (default: OFF)
    int                        m_inhibitCancel    {0};
    
    /// Buffer to store MBM identifiers
    std::vector<BMID>          m_bmIDs            {};
    /// Map between buffer identifiers and the corresponding name
    MappedBuffers              m_buffMap          {};
    /// Map of shared producer objects
    SharedProducers            m_sharedProducers  {};

    /// Initialize the MBM client
    int i_init();
    /// Finalize the MBM client
    int i_fini();

  public:

    /// Initializing constructor
    MBMClient(const std::string& name, Context& framework);
    /// Default destructor
    virtual ~MBMClient();
    /// Access to optional MBM buffer identifiers
    const std::map<std::string,BMID>& buffers()  const { return m_buffMap;  }
    /// Access to optional MBM buffer identifiers
    const std::vector<BMID>& bmIDs()  const    {   return m_bmIDs;          }
    /// Access to partition ID as specified in the job options
    int partitionID()  const                   {   return m_partitionID;    }
    /// Access to partition name as specified in the job options
    const std::string& partitionName() const   {   return m_partitionName;  }
    /// Access to process name
    const std::string& processName()  const    {   return m_procName;       }
    /// Connect when ???
    const std::string& connectWhen()  const    {   return m_connectWhen;    }

    /// Initialize the MBM client
    virtual int initialize()  override;
    /// Start the MBM client
    virtual int start()  override;
    /// Finalize the MBM client
    virtual int finalize()  override;
    /// Stop the MBM client
    virtual int stop()  override;

    /// Create proper buffer name depending on partitioning
    std::string bufferName(const std::string& nam)  const;
    /// Connect to optional MBM buffer
    int connectBuffer(const std::string& nam);
    /// Connect to specified buffers
    int connectBuffers();
    /// Cancel MBM connections
    int cancelBuffers();
    /// Queue cancel MBM connections
    int requestCancelBuffers();
    /// Access an existing buffer manager inclusion
    BMID access(const std::string& buffer);
    /// Include into the buffer manager. To use you should know what you are doing.
    BMID include(const std::string& buffer,const std::string& instance);
    /// Exclude from the buffer manager. To use you should know what you are doing.
    int exclude(BMID bmid);
    /// Create shared producer. These producers are owned by the MEPManager!
    MBM::Producer* createSharedProducer(const std::string& buffer,const std::string& instance);
    /// Create producer
    MBM::Producer* createProducer(const std::string& buffer,const std::string& instance);
    /// Create consumer attached to a specified buffer
    MBM::Consumer* createConsumer(const std::string& buffer,const std::string& instance);
  };

}      // end namespace Online
#endif //  ONLINE_DATAFLOW_MBMCLIENT_H
