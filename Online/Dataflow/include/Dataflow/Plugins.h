//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Plugins.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_PLUGINS_H
#define ONLINE_DATAFLOW_PLUGINS_H

// Framework include files
#include <DD4hep/Plugins.h>

///  Online namespace declaration
namespace Online  {
  class DataflowComponent;
  class DataflowContext;
  namespace Plugins {
    /// Wrapper around DD4hep plugin mechanism to create data flow components
    DataflowComponent* createComponent(const std::string& name_type, DataflowContext& ctxt);
  }  // End namespace Plugins
}    // End namespace Online
namespace {
  DD4HEP_PLUGIN_FACTORY_ARGS_2(Online::DataflowComponent*,const char*,Online::DataflowContext*) { return new P(a0,*a1); }
}

#define DECLARE_DATAFLOW_NAMED_COMPONENT(name,clazz)			\
  DD4HEP_PLUGINSVC_FACTORY(clazz,name,Online::DataflowComponent*(const char*,Online::DataflowContext*),__LINE__)

#define DECLARE_DATAFLOW_NAMED_COMPONENT_NS(ns,name,clazz)		\
  namespace { using ns::clazz; DECLARE_DATAFLOW_NAMED_COMPONENT(name,clazz) }

#define DECLARE_DATAFLOW_COMPONENT(name)         DECLARE_DATAFLOW_NAMED_COMPONENT(name,name)
#define DECLARE_DATAFLOW_COMPONENT_NS(ns,name)   DECLARE_DATAFLOW_NAMED_COMPONENT_NS(ns,name,name)

#endif //  ONLINE_DATAFLOW_PLUGINS_H
