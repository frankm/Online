//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataSender.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DATASENDER_H
#define ONLINE_DATAFLOW_DATASENDER_H

#ifndef TRANSFER_NS
#error This header file is not intended for user inclusion, but only to instantiate component factories!
#endif

// Framework include files
#include <Dataflow/Sender.h>
#include <WT/wt_facilities.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <cstring>

///  Online namespace declaration
namespace Online  {

  using namespace TRANSFER_NS;

  /// Implementation of a generic network DataSender component
  /**
   *  Standard component template to implment DataSender component objects.
   *  The template is meant to be used by various DataTransfer
   *  implementations such as
   *
   *  - NET/DataTransfer
   *
   *  - ASIO/Transfer
   *
   *  - ZMQTransfer/Transfer
   *
   *  All these implementations have the same interface, which
   *  allows to transfer data between nodes.
   *
   *  @author Markus Frank
   */
  class DataSender : public Sender  {

    /// Static task-dead handler
    static void handle_death(const netheader_t& hdr, void* param, netentry_t*)   {
      DataSender* p = reinterpret_cast<DataSender*>(param);
      p->taskDead(hdr.name);
    }

    /// Statis event request handler
    static void handle_req(const netheader_t& hdr, void* param, netentry_t* entry)  {
      DataSender* p = reinterpret_cast<DataSender*>(param);
      int sc = net_receive(p->m_netPlug, entry, 0);
      if ( sc == NET_SUCCESS )  {
	p->handleRequest(p->m_recipients.size(),hdr.name,hdr.name);
      }
    }
    /// Pointer to netplug device
    NET*     m_netPlug;
    /// Number of thread instances used by the network library
    int      m_nThreads;

  public:
    /// Standard algorithm constructor
    DataSender(const std::string& nam, DataflowContext& ctxt) 
    : Sender(nam, ctxt), m_netPlug(0)  
    {
      declareProperty("NumThreads",m_nThreads=2);
    }

    /// Standard Destructor
    virtual ~DataSender()   {}

    /// Subscribe to network requests
    virtual int subscribeNetwork()  override   {
      std::string self = RTL::dataInterfaceName()+"::"+RTL::processName();
      m_netPlug = net_init(self, m_nThreads);
      net_subscribe(m_netPlug,this,WT_FACILITY_CBMREQEVENT,handle_req,handle_death);
      if ( !m_target.empty() )  {
        const char* req = "EVENT_SOURCE";
        int sc = net_send(m_netPlug,req,strlen(req)+1,m_target,WT_FACILITY_CBMCONNECT);
        return sc==NET_SUCCESS ? DF_SUCCESS : DF_ERROR;
      }
      return DF_SUCCESS;
    }

    /// Unsubscribe from network requests
    virtual int unsubscribeNetwork()  override {
      if ( m_netPlug )  {
	net_unsubscribe(m_netPlug,this,WT_FACILITY_CBMREQEVENT);
	net_close(m_netPlug);
	m_netPlug = 0;
      }
      return DF_SUCCESS;
    }

    /// Send data to target destination
    virtual int sendData(const Recipient& tar, const void* data, size_t len)  override  {
      int sc = net_send(m_netPlug,data,len,tar.name,WT_FACILITY_CBMEVENT);
      return sc==NET_SUCCESS ? DF_SUCCESS : DF_ERROR;
    }

    /// Networking layer overload [Net producer+consumer]: Cancel current I/O
    virtual int cancelNetwork()   override  {
      net_cancel(m_netPlug);
      return DF_SUCCESS;
    }
  };
}      // end namespace Online

#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_COMPONENT_NS(Online,DataSender)

#endif //  ONLINE_DATAFLOW_DATASENDER_H
