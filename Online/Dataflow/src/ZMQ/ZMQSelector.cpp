//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ZMQSelector.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data server 
//               using boost::asio asynchronous data I/O
//
//  Author     : Markus Frank
//==========================================================================
#define TRANSFER_NS    ZMQ
#include "NET/Transfer.h"
#define DataSelector   Dataflow_ZMQSelector
#include "Dataflow/DataSelector.h"
