//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ZMQServer.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data server 
//               using boost::asio asynchronous data I/O
//
//  Author     : Markus Frank
//==========================================================================

#define TRANSFER_NS        ZMQ
#include "NET/Transfer.h"
#define EventServer        Dataflow_ZMQEventServer
#define EventRequestServer Dataflow_ZMQEventRequestServer
#include "Dataflow/EventServer.h"
