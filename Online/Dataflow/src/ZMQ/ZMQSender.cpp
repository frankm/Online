//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ZMQSender.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data sender 
//               using ZeroMQ asynchronous data I/O
//
//  Author     : Markus Frank
//==========================================================================

#define TRANSFER_NS        ZMQ
#include "NET/Transfer.h"
#define DataSender Dataflow_ZMQSender
#include "Dataflow/DataSender.h"
