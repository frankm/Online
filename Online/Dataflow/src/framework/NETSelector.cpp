//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  NETSelector.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/NETSelector.h>
#include <Dataflow/Incidents.h>
#include <CPP/TimeSensor.h>
#include <CPP/Event.h>
#include <WT/wtdef.h>
#include <MBM/bmstruct.h>

// C/C++ include files
#include <cstring>
using namespace std;
using namespace Online;

/// Initializing constructor
NETSelector::RecvEntry::RecvEntry(const std::string& s, int len, char* ptr)
  : EventData(), source(s) 
{
  release = 1;
  length  = len;
  data    = ptr;
  header  = (EventHeader*)ptr;
}

/// Default destructor
NETSelector::RecvEntry::~RecvEntry() {
}

/// Initializing constructor
NETSelector::NETSelector(const string& nam, Context& ctxt)
  : EventRunable(nam, ctxt)
{
  declareProperty("Input",         m_input        = "");
  declareProperty("CancelOnDeath", m_cancelDeath  = false);
  declareProperty("EventTimeout",  m_eventTimeout = 30);
  declareProperty("HandleTimeout", m_handleTMO    = false);
  declareProperty("Pause",         m_gotoPause    = false);
  declareProperty("PauseSleep",    m_pauseSleep   = 0);
  declareProperty("NumThreads",    m_numThreads   = 2);
  declareProperty("PrintFreq",     m_printFreq    = 0.0);
  declareProperty("EventType",     m_event_type   = EVENT_TYPE_EVENT);
  declareProperty("EventMask",     m_event_mask   = {~0x0, ~0x0, ~0x0, ~0x0});
  declareProperty("REQ1",          m_Rqs[0]       = "");
  declareProperty("REQ2",          m_Rqs[1]       = "");
  declareProperty("REQ3",          m_Rqs[2]       = "");
  declareProperty("REQ4",          m_Rqs[3]       = "");
  declareProperty("REQ5",          m_Rqs[4]       = "");
  declareProperty("REQ6",          m_Rqs[5]       = "");
  declareProperty("REQ7",          m_Rqs[6]       = "");
  declareProperty("REQ8",          m_Rqs[7]       = "");
}

/// Default destructor
NETSelector::~NETSelector()   {
}

void NETSelector::handleData(const string& src, size_t siz, char* buff)  {
  if ( !this->m_cancelled )  {
    RecvEntry* e = 0;
    /// Add entry to the data queue (must lock!)
    {
      lock_guard<mutex> lck (this->m_dataLock);
      this->m_data.push_back(e=new RecvEntry(src,siz,buff));
      this->m_recvBytes += siz;
      ++this->m_recvCount;
    }
    // Set event flag for the data processing thread that data are present
    if ( !lib_rtl_is_success(lib_rtl_set_event(this->m_haveEvent)) )
      this->error("Cannot activate event lock.");
    // Re-arm now to have at least one event in the stomach
    this->rearmEvent();
    // Now wait until the event is released again after processing/copy
    // Necessary to avoid pile-up of data processing requests
    if ( !lib_rtl_is_success(lib_rtl_wait_for_event(this->m_freeEvent)) )
      this->error("Cannot activate event lock.");
  }
}

void NETSelector::taskDead(const string& who)  {
  this->error("The event data source %s died ....",who.c_str());
}

/// Get next event from wherever
int NETSelector::getEvent(Context::EventData& ev)   {
  int sc = 0;
  bool empty = true; 
  while ( empty )  {{
      lock_guard<mutex> lck (this->m_dataLock);
      empty = this->m_data.empty();
    }
    if ( empty )  {
      ::lib_rtl_sleep(5);
      if ( this->m_cancelled )  {
	return DF_CANCELLED;
      }
    }
  }
  sc = lib_rtl_wait_for_event(m_haveEvent);
  if ( lib_rtl_is_success(sc) )   {
    lock_guard<mutex> lck (this->m_dataLock);
    if ( this->m_data.empty() && this->m_cancelled )  {
      return DF_CANCELLED;
    }
    else if ( this->m_data.empty() )  {
      // error: this should never happen!
      return this->error("No event to be processed in the event buffer!");
    }
    this->m_lastEvent = time(0);
    unique_ptr<RecvEntry> e(this->m_data.front());
    this->m_data.pop_front();
    ev         = *e;
    e->data    = 0;
    ev.length  = e->length;
    ev.type    = this->m_event_type;
    ev.mask[0] = this->m_event_mask[0];
    ev.mask[1] = this->m_event_mask[1];
    ev.mask[2] = this->m_event_mask[2];
    ev.mask[3] = this->m_event_mask[3];
    ev.release = 1;
    return DF_SUCCESS;
  }
  return DF_ERROR;
}

/// Shutdown event access in case of forced exit request
int NETSelector::shutdown()   {
  this->m_lastEvent = time(0);
  TimeSensor::instance().remove(this);
  return net_shutdown();
}

/// Release event
int NETSelector::freeEvent(Context::EventData& event)   {
  if ( !lib_rtl_is_success(lib_rtl_set_event(this->m_freeEvent)) )
    this->error("Cannot activate event lock.");
  detail::deletePtr(event.data);
  event.length = 0;
  return DF_SUCCESS;
}

/// Rearm event request to data source
int NETSelector::rearmEvent()  {
  try  {
    ++this->m_reqCount;
    int sc = net_rearm();
    if ( sc == DF_SUCCESS )  {
      this->debug("Sent event request %d to %s [Got:%d Events].",
		  this->m_reqCount, this->m_input.c_str(), this->m_recvCount);
      return DF_SUCCESS;
    }
    return this->error("Failed to send event request to %s.",this->m_input.c_str());
  }
  catch(const exception& e)  {
    return this->error("Failed to read next event: %s",e.what());
  }
  catch(...)  {
    return this->error("Failed to read next event - Unknown exception.");
  }
}

/// Initialize the NET server
int NETSelector::initialize()  {
  int sc = EventRunable::initialize();
  if ( sc == DF_SUCCESS )  {
    for(int j=0; j<BM_MASK_SIZE;++j)  {
      this->m_request.trmask[j] = 0;
      this->m_request.vetomask[j] = ~0;
    }
    for ( this->m_nreqs=0; this->m_nreqs<8; ++this->m_nreqs )  {
      if ( !this->m_Rqs[this->m_nreqs].empty() )   {
	this->m_Reqs[this->m_nreqs].parse(this->m_Rqs[this->m_nreqs]);
	continue;
      }
      break;
    }
    this->m_request = this->m_Reqs[0];
    for (int i=0; i<this->m_nreqs; ++i )  {
      const MBM::Requirement& r = m_Reqs[i];
      for(int k=0; k<BM_MASK_SIZE; ++k)  {
        m_request.trmask[k]   |= r.trmask[k];
        m_request.vetomask[k] &= r.vetomask[k];
      }
    }
    while ( m_event_mask.size() < 4 ) m_event_mask.push_back(~0x0);

    // Create lock to steer suspend/resume operations
    if ( !lib_rtl_is_success(lib_rtl_create_event(0,&m_haveEvent)) )
      return this->error("Cannot create lock to synchronize I/O operations.");
    if ( !lib_rtl_is_success(lib_rtl_create_event(0,&m_freeEvent)) )
      return this->error("Cannot create lock to synchronize I/O operations.");

    declareMonitor("RequestCount",m_reqCount=0,  "Event request counter");
    declareMonitor("EventsRcv",   m_recvCount=0, "Total number of items received.");
    declareMonitor("ErrorsRcv",   m_recvError=0, "Total number of receive errors.");
    declareMonitor("BytesRcv",    m_recvBytes=0, "Total number of bytes received from clients.");
    m_cancelled = false;
  }
  return sc;
}

/// Initialize the event processor
int NETSelector::start()  {
  int sc = EventRunable::start();
  m_lastEvent = time(0);
  if ( sc == DF_SUCCESS )  {
    if ( m_eventTimeout > 0 )  {
      TimeSensor::instance().add(this,m_eventTimeout,(void*)this);
    }
    m_cancelled = false;
    sc = net_start();
    if ( sc == DF_SUCCESS )    {
      sc = rearmEvent();
      return (m_eventTimeout > 0) ? DF_SUCCESS : sc;
    }
    return DF_ERROR;
  }
  return sc;
}

/// Initialize the event processor
int NETSelector::stop()  {
  shutdown();
  return EventRunable::stop();
}

/// Finalize the event processor
int NETSelector::finalize()  {
  m_input = "";
  m_nreqs = 0;
  return EventRunable::finalize();
}

/// Cancel I/O operations of the dataflow component
int NETSelector::cancelEvents()  {
  m_cancelled = true;
  {
    lock_guard<mutex> lck (m_dataLock);
    if ( !m_data.empty() )  {
      for( auto e : m_data ) { detail::deletePtr(e->data); detail::deletePtr(e); }
      m_data.clear();
    }
  }
  // Trigger event processing thread to get out of wait
  lib_rtl_set_event(m_haveEvent);
  // Trigger receiver thread to get out of wait
  lib_rtl_set_event(m_freeEvent);
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void NETSelector::handle(const DataflowIncident& inc)   {
  this->EventRunable::handle(inc);
}

/// Interactor handler routine
void NETSelector::handle(const CPP::Event& ev)         {
  if ( ev.eventtype == TimeEvent )  {
    long now  = time(0);
    long diff = now - m_lastEvent;
    if ( diff > m_eventTimeout )  {
      warning("Renew event requests due to timeout....no event since "
	      "%d seconds [Timeout:%d].",int(diff),m_eventTimeout);
      if ( !m_input.empty() && !m_cancelled )  {
	net_shutdown();
	net_start();
	rearmEvent();
	//stop();
	//start();
      }
    }
    if ( m_eventTimeout > 0 )  {
      TimeSensor::instance().add(this,m_eventTimeout,this);
    }
  }
}
