//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Sender.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/Sender.h>
#include <Tell1Data/Tell1Decoder.h>

// C/C++ include files
#include <cstring>

using namespace std;
using namespace Online;

typedef lock_guard<mutex> Lock;

/// Initializing constructor
Sender::Sender(const string& nam, Context& ctxt)
  : Component(nam, ctxt),
    m_sendReq(0), m_sendError(0), m_sendBytes(0), m_sendEvents(0)
{
  this->declareProperty("DataSink",          this->m_target);
  this->declareProperty("DataSinks",         this->m_sinks);
  this->declareProperty("UseEventRequests",  this->m_useEventRequests  = false);
  this->declareProperty("SendErrorDelay",    this->m_sendErrorDelay    = 1000);
  this->declareProperty("Delay",             this->m_sendDelay         = 0);
  this->declareProperty("PauseOnError",      this->m_pauseOnError      = true);
  this->declareProperty("RoutingBits",       this->m_routingBits       = 0);
  this->declareProperty("RetrySending",      this->m_retrySending      = 10000);
  this->declareProperty("SendingEnabled",    this->m_sendEnabled       = true);
  this->declareProperty("SendPartialFrames", this->m_sendPartialFrames = true);
}

/// Default destructor
Sender::~Sender()   {
}

/// Initialize the MBM server
int Sender::initialize()  {
  int sc = this->Component::initialize();
  if ( this->m_sendEnabled && sc == DF_SUCCESS )   {
    // Do NOT call base class initialization: we are not writing to file/socket!
    this->m_recipients.clear();
    this->declareMonitor("Events", "OUT", this->m_sendReq=0,   "Total number of items sent to receiver(s).");
    this->declareMonitor("ErrorsOut",     this->m_sendError=0, "Total number of send errors to receiver(s).");
    this->declareMonitor("BytesOut",      this->m_sendBytes=0, "Total number of bytes sent to receiver(s).");
    this->declareMonitor("BadHeader",     this->m_badHeader=0, "Total number of events with bad header.");
    if ( this->m_target.empty() )  {
      this->info("No data sink specified. Clients will have to subscribe themselves.");
    }
    this->m_sendEvents = true;
    try  {
      this->debug("Register to data sink %s.",this->m_target.c_str());
      if ( (sc=this->subscribeNetwork()) != DF_SUCCESS )  {
	::lib_rtl_output(LIB_RTL_OS,"Failed to register to data sink %s.",this->m_target.c_str());
	return sc;
      }
      if ( !this->m_useEventRequests )  {
	if ( !this->m_target.empty() )  {
	  this->handleRequest(this->m_recipients.size(),this->m_target,"EVENT_REQUEST");
	}
	for(auto i=this->m_sinks.begin(); i!=this->m_sinks.end(); ++i)  {
	  this->handleRequest(this->m_recipients.size(),*i,"EVENT_REQUEST");
	}
      }
      return sc;
    }
    catch(const exception& e)  {
      this->error(e, "Failed initialization.");
    }
    catch(...)  {
      this->error("Unknown exception during initialization.");
    }
  }
  return sc;
}

/// Finalize the MBM server
int Sender::finalize()  {
  this->m_sendEvents = false;
  this->unsubscribeIncidents();
  this->m_recipients.clear();
  if ( this->m_sendEnabled )   {
    this->unsubscribeNetwork();
  }
  this->undeclareMonitors();
  this->undeclareMonitors("Events");
  return this->Component::finalize();
}

/// Cancel I/O operations of the dataflow component
int Sender::cancel()  {
  this->m_sendEvents = false;
  if ( this->m_sendEnabled )   {
    this->cancelNetwork();
  }
  this->info("Executing cancel");
  return DF_SUCCESS;
}

/// Stop gracefully execution of the dataflow component
int Sender::pause()  {
  return DF_SUCCESS;
}

/// Handle client request to receive event over the network
int Sender::handleRequest(long clientID,const string& source,const char* buf)  {
  if ( ::strncasecmp(buf,"EVENT_REQUEST",13) == 0 )
    return this->addRequest(Recipient(this,source,clientID));
  this->warning("Received unknown request: %s from %s.",buf ? buf : "???", source.c_str());
  return DF_ERROR;
}

/// Add request to request queue
int Sender::addRequest(const Recipient& task)  {
  Lock lck(this->m_lock);  // Lock recipient queue to prevent damage
  this->m_recipients.push_back(task);
  return DF_SUCCESS;
}

/// Callback on task dead notification
int Sender::taskDead(const string& task_name)  {
  this->info("Datasink client: %s dies.",task_name.c_str());
  return DF_SUCCESS;
}

/// Send data record to network client
int Sender::execute(const Context::EventData& event)  {
  int retryCount = this->m_retrySending;
  if ( !this->m_sendEnabled )   {
    return DF_SUCCESS;
  }
  if ( this->m_recipients.empty() && this->m_sendEvents )    {
    this->error("No known recipient...Waiting for receiver.");
    while( this->m_recipients.empty() && this->m_sendEvents && --retryCount > 0 )    {
      ::lib_rtl_sleep(this->m_sendErrorDelay);
    }
    if ( this->m_recipients.empty() )    {
      this->error("Failed to send MDF record [No known recipient].");
      return DF_SUCCESS;
    }
  }
  if ( !this->m_sendEvents ) {
    this->error("Ignore request to send event -- got DAQ_CANCEL earlier.");
    return DF_SUCCESS;
  }
  /// Now send the data ....
  Recipient recipient;
  {
    Lock lck(this->m_lock);  // Lock recipient queue to prevent damage
    recipient = this->m_recipients.front();
    this->m_recipients.pop_front();
  }
  int num_bad_headers = 0;
  const char* start = event.data;
  const char* end   = start + event.length;
  while ( start < end )   {
    const EventHeader* h = (const EventHeader*)start;
    if ( 0 != this->m_routingBits )  {
      auto         trm = h->subHeader().H1->triggerMask();
      unsigned int trMask[4] = {trm[0],trm[1],trm[2],(unsigned int)m_routingBits};
      h->subHeader().H1->setTriggerMask(trMask);
    }
    if ( h->subHeader().H1->runNumber() == 0 )    {
      ++this->m_badHeader;
      ++num_bad_headers;
    }
    start += h->size0();
  }
  retryCount = this->m_retrySending;
  int sc = DF_SUCCESS;
  try   {
  Retry:
    if ( num_bad_headers > 0 && m_sendPartialFrames )    {
      this->warning("Event with %d BAD MDF header(s) seen. [Skipped].", num_bad_headers);
      start = event.data;
      end   = start + event.length;
      while ( start < end )   {
	const EventHeader* h = (const EventHeader*)start;
	if ( h->subHeader().H1->runNumber() != 0 )    {
	  sc = sendData(recipient, h, h->size0());
	  if ( sc != DF_SUCCESS )
	    break;
	}
	start += h->size0();
      }
      sc = DF_SUCCESS; // We do not retry if there are bad frames.
    }
    else   {
      sc = sendData(recipient, event.data, event.length);
    }
    if ( sc != DF_SUCCESS )   {
      this->info("Failed to send MDF to %s [SendEvents:%s  Retry:%s].",
		 recipient.name.c_str(), RTL::yes_no(this->m_sendEvents),
		 this->m_sendEvents && RTL::yes_no(retryCount>0));
    Sleep_Again:
      if ( this->m_sendEvents )   {
	::lib_rtl_sleep(this->m_sendErrorDelay);
	++this->m_sendError;

	/// Try to re-establish the connection
	if ( --retryCount > 0 )   {
	  if ( (retryCount%15)==0 )  {
	    this->unsubscribeNetwork();
	    if ( (sc=this->subscribeNetwork()) != DF_SUCCESS )  {
	      this->info("Retry %d: Failed to register to data sinks.",
			 this->m_retrySending=retryCount);
	      goto Sleep_Again;
	    }
	    if ( !this->m_useEventRequests )  {
	      if ( !this->m_target.empty() )  {
		this->handleRequest(this->m_recipients.size(),this->m_target,"EVENT_REQUEST");
	      }
	      for(auto i=this->m_sinks.begin(); i!=this->m_sinks.end(); ++i)  {
		this->handleRequest(this->m_recipients.size(),*i,"EVENT_REQUEST");
	      }
	    }
	  }
	  else   {
	    goto Sleep_Again;
	  }
	  goto Retry;
	}
	this->error("Failed to send MDF to %s [SendEvents:%s].",
	      recipient.name.c_str(),this->m_sendEvents ? "YES" : "NO");
	if ( this->m_pauseOnError )  {
	  this->fireIncident("DAQ_PAUSE");
	}
	return DF_ERROR;
      }
      /// Add recipient again to queue of receivers
      return this->m_useEventRequests ? DF_SUCCESS : this->addRequest(recipient);
    }
    ++m_sendReq;
    m_sendBytes += event.length;
  }
  catch(const exception& exc)    {
    this->error("Exception while sending event: %s", exc.what());
  }
  catch(...)    {
    this->error("UNKNOWN exception while sending event.");
  }
  if ( this->m_sendDelay > 0 )   {
    ::lib_rtl_sleep(this->m_sendDelay);
  }
  /// Mode without requests: add entry again at end....
  return this->m_useEventRequests ? DF_SUCCESS : this->addRequest(recipient);
}
