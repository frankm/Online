//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataflowManager.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
//
// Framework include files
#include <Dataflow/DataflowManager.h>
#include <Dataflow/ComponentHandler.h>
#include <Dataflow/DataflowTask.h>
#include <Dataflow/ControlPlug.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/Plugins.h>
#include <Tell1Data/Tell1Decoder.h>
#include <PCIE40Data/pcie40.h>
#include <PCIE40Data/sodin.h>
#include <MBM/bmdef.h>

// C/C++ include files
#include <functional>

using namespace std;
using namespace Online;
#include <setjmp.h>
#include <signal.h>
#include <string.h>

namespace  {

  static jmp_buf __ReadCheckEnv;

  static void segvTestHandler(int , siginfo_t *, void *)  {
    longjmp(__ReadCheckEnv,27);
  }

  int CheckRead(void *add)  {
    struct sigaction act;
    struct sigaction oldact;
    memset (&act,0,sizeof(act));
    act.sa_sigaction = &segvTestHandler;
    act.sa_flags = SA_SIGINFO;
    sigaction(SIGSEGV,&act,&oldact);
    int ret=setjmp(__ReadCheckEnv);
    if (ret == 27)
      {
	sigaction(SIGSEGV,&oldact,0);
	return 1;
      }
    char dummy = *(char*)add;
    if ( dummy ) {}
    ::sigaction(SIGSEGV,&oldact,0);
    return 0;
  }

  int _get_run_number(const DataflowContext::EventData& event)   {
    EventHeader*          mdf = event.header;
    if ( mdf->is_mdf() )   {
      // We got an MDF header. Determine the run number and pass it to the monitoring service
      return mdf->subHeader().H1->runNumber();
    }
    const pcie40::mep_header_t* mep = event.as<pcie40::mep_header_t>();
    if ( mep->is_valid() )    {
      if ( mep->num_source > 0 )   {
	for(uint32_t i=0; i<mep->num_source; ++i)    {
	  const auto    *mfp  = mep->multi_fragment(i);
	  const uint8_t *typs = mfp->types();
	  if ( mfp->packingFactor() > 0 && *typs != RawBank40::ODIN )
	    continue;
	  const uint8_t version = mfp->header.version;
	  if ( version < 7 )   {
	    const auto *odin = (const RunInfo*)mfp->data();
	    return odin->run_number();
	  }
	  else   {
	    const auto *odin = (const pcie40::sodin_t*)mfp->data();
	    return odin->run_number();
	  }
	}
	// In case there is no ODIN:
	return 0;
      }
      return -1;
    }
    if ( event.type == EVENT_TYPE_MEP )   {
      return 0;
    }
    return -1;
  }
}

/// Initializing constructor
DataflowManager::DataflowManager(const string& nam, const ComponentNames& infrastructure)
  : DataflowComponent(nam,*(m_contextPtr=new DataflowContext(*this)))
{
  Component* opt = 0, *mon = 0, *inc = 0, *log = 0;
  ComponentHandler handler(context);
  for(const auto& i : infrastructure)  {
    Component* c = handler.construct(i);
    if ( c->name == "Logger" ) log = c;
    if ( c->name == "JobOptions" ) opt = c;
    if ( c->name == "Monitoring" ) mon = c;
    if ( c->name == "IncidentHandler" ) inc = c;
    always.push_back(c);
  }
  if ( !log || !context.logger )  {
    if ( !log ) log = Component::create("Dataflow_OutputLogger/Logger",context);
    context.logger  = log->query<Context::Logger>();
  }
  if ( !opt || !context.options )  {
    if ( !opt ) opt = Component::create("Dataflow_JobOptions/JobOptions",context);
    context.options = opt->query<Context::Options>();
  }
  if ( !mon || !context.monitor )  {
    if ( !mon ) mon = Component::create("Dataflow_Monitoring/Monitoring",context);
    context.monitor = mon->query<Context::Monitor>();
  }
  if ( !inc || !context.incidents )  {
    if ( !inc ) inc = Component::create("Dataflow_IncidentHandler/IncidentHandler",context);
    context.incidents = inc->query<Context::Incident>();
  }
  declareProperty("Setup",            setupNames);
  declareProperty("Outputs",          outputNames);
  declareProperty("Services",         serviceNames);
  declareProperty("Algorithms",       algorithmNames);
  declareProperty("Runable",          runableName="");
  declareProperty("TaskType",         taskType="");
  declareProperty("MaxBadEvent",      m_maxBadEvent);
  declareProperty("EnableAlgorithms", enableAlgorithms);
  declareProperty("EnableProcessing", enabled);
  state = Control::ST_OFFLINE;
}

/// Default destructor
DataflowManager::~DataflowManager()  {
  ComponentHandler handler(context);
  undeclareMonitors();
  context.options   = 0;
  context.monitor   = 0;
  context.incidents = 0;
  for(const auto& i : setup)  handler.destruct(i);
  for(const auto& i : always) handler.destruct(i);
  setup.clear();
  always.clear();
  delete m_contextPtr;
}

/// Add a managed component: Set state according to my state and then alog with the others.
DataflowComponent* 
DataflowManager::getManagedComponent(const std::string& type_name, bool create)  {
  ComponentHandler handler(context);
  Component* component = 0;
  try {
    component = getComponentUnchecked(type_name);
  }
  catch(...)  {
  }
  if ( !component && create )  {
    component = handler.construct(type_name);
  }
  else if ( component )   {
    return component;
  }
  else if ( !component )  {
    throwError("Failed to create component of type %s",type_name.c_str());
    return 0;
  }
  managed.push_back(component);
  switch(state)  {
  case Control::ST_OFFLINE:
  case Control::ST_NOT_READY:
    break;
      
  case Control::ST_READY:
    handler.initialize(component);
    break;
  case Control::ST_STOPPED:
    handler.initialize(component);
    break;
  case Control::ST_ACTIVE:
  case Control::ST_RUNNING:
  case Control::ST_PAUSED:
  case Control::ST_ERROR:
    handler.initialize(component);
    handler.start(component);
    break;
  default:
    break;
  }
  return component;
}

/// Access known component by name. If not found exception!
DataflowComponent* DataflowManager::getComponent(const string& nam)  const  {
  Component* c = getComponentUnchecked(nam);
  if ( c ) return c;
  throwError("Component %s is not known. Severe problem.",nam.c_str());
  return 0;
}

/// Access known component by name. If not found exception!
DataflowComponent* DataflowManager::getComponentUnchecked(const string& nam)  const  {
  string n = nam;
  size_t idx = n.find('/');
  if ( idx != string::npos ) n = nam.substr(idx+1);
  for(const auto& i : setup)       { if ( i->name == n ) return i; }
  for(const auto& i : always)      { if ( i->name == n ) return i; }
  for(const auto& i : managed)     { if ( i->name == n ) return i; }
  for(const auto& i : services)    { if ( i->name == n ) return i; }
  for(const auto& i : algorithms)  { if ( i->name == n ) return i; }
  for(const auto& i : outputs)     { if ( i->name == n ) return i; }
  return 0;
}

/// Execute actions on all components. Invoke callback
long DataflowManager::for_each(ComponentManip& handler)   {
  long result = 0;
  result += for_each(SETUP,     handler);
  result += for_each(ALWAYS,    handler);
  result += for_each(MANAGED,   handler);
  result += for_each(OUTPUT,    handler);
  result += for_each(SERVICE,   handler);
  result += for_each(ALGORITHM, handler);
  return result;
}

/// Execute actions on all components. Invoke callback on predefined set
long DataflowManager::for_each(int typ, ComponentManip& handler)   {
  long ret, result = 0;
  if ( typ&SETUP )  {
    for(const auto& i : setup)  {
      ret = handler(typ,i);
      if ( ret < 0 ) return ret;
      result += ret;
    }
  }
  if ( typ&ALWAYS )  {
    for(const auto& i : always)  {
      ret = handler(typ,i);
      if ( ret < 0 ) return ret;
      result += ret;
    }
  }
  if ( (enabled&SERVICE) && (typ&SERVICE) )  {
    for(const auto& i : services)   {
      ret = handler(typ,i);
      if ( ret < 0 ) return ret;
      result += ret;
    }
  }
  if ( (enabled&MANAGED) && (typ&MANAGED) )  {
    for(const auto& i : managed)    {
      ret = handler(typ,i);
      if ( ret < 0 ) return ret;
      result += ret;
    }
  }
  if ( (enabled&ALGORITHM) && (typ&ALGORITHM) )  {
    for(const auto& i : algorithms) {
      ret = handler(typ,i);
      if ( ret < 0 ) return ret;
      result += ret;
    }
  }
  if ( (enabled&OUTPUT) && (typ&OUTPUT) )  {
    for(const auto& i : outputs)    {
      ret = handler(typ,i);
      if ( ret < 0 ) return ret;
      result += ret;
    }
  }
  return result;
}

/// Execute transition action
int DataflowManager::action(const char* transition, handler_func_t pmf) {
  try  {
    ComponentHandler handler(context);
    if ( enabled & SERVICE )
      for(const auto& i : services)   (handler.*pmf)(i);
    if ( enabled & MANAGED )
      for(const auto& i : managed)    (handler.*pmf)(i);
    if ( enabled & ALGORITHM )
      for(const auto& i : algorithms) (handler.*pmf)(i);
    if ( enabled & OUTPUT )
      for(const auto& i : outputs)    (handler.*pmf)(i);
    debug("%-14s Transition successfully completed (%d services, %d algorithms %d outputs)",
	  transition, int(services.size()), int(algorithms.size()), int(outputs.size()));
    return DF_SUCCESS;
  }
  catch(const exception& e)   {
    error(e,"(%s)",transition);
    error("%s: Error condition encountered. Inhibit further processing.",transition);
  }
  catch(...)  {
    error("%s: UNKOWN error condition encountered. Inhibit further processing.",transition);
  }
  return DF_ERROR;  
}

/// Execute full transition action
int DataflowManager::invoke(const char* transition, handler_func_t func, int target_state)   {
  int sc = DF_SUCCESS;
  if ( enabled )   {
    sc = action(transition, func);
  }
  if ( sc == DF_SUCCESS ) state = target_state;
  return sc;
}

/// Configure the instance (create components)
int DataflowManager::configure()   {
  ComponentHandler handler(context);
  if (setProperties() != DF_SUCCESS) throwError("Failed to set properties.");
  // If overridden from default, ignore "enabled"
  if ( enableAlgorithms != 0xFF )   {
    enabled = SETUP|ALWAYS|RUNABLE|MANAGED|SERVICE
      | ((enableAlgorithms!=0) ? ALGORITHM|OUTPUT : 0);
  }
  subscribeIncident("DAQ_ERROR");
  subscribeIncident("DAQ_PAUSE");
  subscribeIncident("DAQ_ENABLE");
  subscribeIncident("DAQ_STOP_TRIGGER");
  subscribeIncident("DAQ_START_TRIGGER");
  /// Update properties of the infrastructure
  for(const auto& i : always) handler.setProperties(i);
  for(const auto& i : always) handler.initialize(i);
  for(const auto& i : always) handler.start(i);
  /// Intialize setup components
  for(const auto& i : setupNames) setup.push_back(handler.construct(i));
  for(const auto& i : setup)  handler.initialize(i);
  for(const auto& i : setup)  handler.start(i);
  /// Start dataflow task instance
  if ( !taskType.empty() )  {
    Component* c = handler.construct(taskType);
    context.task = c->query<DataflowTask>();
    context.task->setProperties();
  }
  declareMonitor("CurrentRun",currentRun,"Current run number to be processed.");
  //  context.monitor->setRunNo(currentRun);
  state = Control::ST_NOT_READY;
  debug("DataflowManager executed successfully configure. Eventprocessing: %s",
	enabled ? "ENABLED" : "DISABLED");
  return DF_SUCCESS;
}

/// Initialize the data flow component. 
int DataflowManager::initialize()   {
  try  {
    if (setProperties() != DF_SUCCESS)
      throwError("Failed to set properties.");
    // If overridden from default, ignore "enabled"
    if ( enableAlgorithms != 0xFF )   {
      enabled = SETUP|ALWAYS|RUNABLE|MANAGED|SERVICE
	| ((enableAlgorithms!=0) ? ALGORITHM|OUTPUT : 0);
    }
    if ( enabled )   {
      ComponentHandler handler(context);
      if ( enabled & SERVICE )
	for(const auto& i : serviceNames)   services.push_back(handler.construct(i));
      if ( enabled & ALGORITHM )
	for(const auto& i : algorithmNames) algorithms.push_back(handler.construct(i));
      if ( enabled & OUTPUT )
	for(const auto& i : outputNames)    outputs.push_back(handler.construct(i));
      if ( action("SetProperties",&ComponentHandler::setProperties) != DF_SUCCESS )
	return DF_ERROR;
      state = Control::ST_READY;
      return action("Initialize", &ComponentHandler::initialize);
    }
    state = Control::ST_READY;
    return DF_SUCCESS;
  }
  catch(const exception& e)   {
    error(e,"Initialize: Error condition encountered. Inhibit further processing.");
  }
  catch(...)  {
    error("Initialize: UNKOWN error condition encountered. Inhibit further processing.");
  }
  return DF_ERROR;
}

/// Start the data flow component. 
int DataflowManager::start()   {
  int sc = DF_SUCCESS;
  if ( enabled )   {
    context.monitor->setRunNo(currentRun);
    sc = action("Start", &ComponentHandler::start);
  }
  if ( sc == DF_SUCCESS ) state = Control::ST_RUNNING;
  return sc;
}

/// Stop the data flow component. 
int DataflowManager::stop()   {
  int sc = action("Stop", &ComponentHandler::stop);
  state = Control::ST_READY;
  return sc;
}

/// Finalize the data flow component.
int DataflowManager::finalize()   {
  int sc = DF_SUCCESS;
  if ( enabled )   {
    sc = action("Finalize", &ComponentHandler::finalize);
    if ( sc != DF_SUCCESS )  {}
    sc = action("Delete",   &ComponentHandler::destruct);
    if ( sc != DF_SUCCESS )  {}
  }
  managed.clear();
  outputs.clear();
  algorithms.clear();
  services.clear();
  state = Control::ST_NOT_READY;
  return sc;
}

/// Terminate the data flow component. 
int DataflowManager::terminate()  {
  try  {
    if ( enabled )   {
      ComponentHandler handler(context);
      for(const auto& i : setup)  handler.stop(i);
      for(const auto& i : setup)  handler.finalize(i);
    }
    state = Control::ST_OFFLINE;
    return DF_SUCCESS;
  }
  catch(const exception& e)   {
    error(e,"Terminate: Error condition while processing transition.");
  }
  catch(...)  {
    error("Terminate: UNKOWN error condition while processing transition.");
  }
  return DF_ERROR;  
}

/// Pause the data flow component. 
int DataflowManager::pause()   {
  return invoke("Pause", &ComponentHandler::pause, Control::ST_PAUSED);
}

/// Cancel the data flow component. 
int DataflowManager::cancel()   {
  return invoke("Cancel", &ComponentHandler::cancel, Control::ST_PAUSED);
}

/// Enable the data flow component. 
int DataflowManager::enable()   {
  return invoke("Enable", &ComponentHandler::enable, Control::ST_RUNNING);
}

/// Continuing the data flow component. 
int DataflowManager::continuing()   {
  return invoke("Continue", &ComponentHandler::continuing, Control::ST_RUNNING);
}

/// Pause the data flow component. 
int DataflowManager::run()   {
  if ( (enabled&RUNABLE) && !runableName.empty() )  {
    Component* c = getComponent(runableName);
    if ( c ) return c->run();
    return 0;
  }
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void DataflowManager::handle(const DataflowIncident& inc)  {
  debug("Got incident: %s of type %s", inc.name.c_str(), inc.type.c_str());
  if (inc.type == "DAQ_ERROR" )  {
  }
  else if ( inc.type == "DAQ_PAUSE" )  {
  }
  else if (inc.type == "DAQ_ENABLE")  {
  }
  else if (inc.type == "DAQ_STOP_TRIGGER")  {
  }
  else if (inc.type == "DAQ_START_TRIGGER")  {
  }
}

/// Update run number for publication
void DataflowManager::setRunNumber(uint32_t runno)    {
  if ( this->currentRun != runno )  {
    this->currentRun = runno;
    context.monitor->setRunNo(runno);
  }
}

/// Access current run number
uint32_t DataflowManager::currentRunNumber() const  {
  return this->currentRun;
}

/// Execute event. 
int DataflowManager::execute(const DataflowContext::EventData& event)   {

  try  {
    ComponentHandler handler(context);
    int rchk = CheckRead(event.header);
    if (rchk != 0)    {
      error("=======> Got Event without Data (event start at address %p not readable).", (void*)event.data);
      return DF_ERROR;
    }
    int runno = _get_run_number(event);
    if ( runno >= 0 )   {
      setRunNumber(runno);
      m_numBadEvent = 0;
    }
    else   {
      // We got a MEP event. Should never happen!
      if ( ++m_numBadEvent <= m_maxBadEvent )  {
	return error("ERROR: Got %ld consecutive NON MDF events. [Should not happen].", m_numBadEvent); 
      }
      warning("Got an event which is not an MDF event. [Should not happen].");
      return DF_SUCCESS;
    }
    for(const auto& i : services)   i->processingFlag = 0;
    for(const auto& i : managed)    i->processingFlag = 0;
    for(const auto& i : algorithms) i->processingFlag = 0;
    for(const auto& i : outputs)    i->processingFlag = 0;
    for(const auto& i : algorithms) handler.execute(i,event);
    for(const auto& i : outputs)    handler.execute(i,event);
    return DF_SUCCESS;
  }
  catch(const exception& e)   {
    error(e,"Execute: Error condition while processing event loop.");
  }
  catch(...)  {
    error("Execute: UNKOWN error condition while processing event loop.");
  }
  return DF_ERROR;  
}
