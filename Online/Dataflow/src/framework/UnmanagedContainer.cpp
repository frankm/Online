//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  UnmanagedContainer.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "Dataflow/UnmanagedContainer.h"
#include "Dataflow/ComponentHandler.h"

// C/C++ include files
#include <stdexcept>

using namespace std;
using namespace Online;

/// Initializing constructor
UnmanagedContainer::UnmanagedContainer(const string& nam, Context& ctxt)
  : Component(nam, ctxt)
{
  declareProperty("Components",componentNames);
}

/// Default destructor
UnmanagedContainer::~UnmanagedContainer()   {
}

/// Execute transition action
int UnmanagedContainer::action(const char* transition, handler_func_t pmf) {
  try  {
    ComponentHandler handler(context);
    for(const auto& i : components)   (handler.*pmf)(i);
    return DF_SUCCESS;
  }
  catch(const exception& e)   {
    error(e,"%s: UnmanagedContainer failed.",transition);
  }
  catch(...)  {
    error("%s: UnmanagedContainer failed [UNKOWN error]",transition);
  }
  return DF_ERROR;  
}

/// Initialize the MBM server
int UnmanagedContainer::initialize()  {
  int sc = Component::initialize();
  if ( sc == DF_SUCCESS )   {
    ComponentHandler handler(context);
    for(const auto& i : componentNames)  {
      Component* c = Component::create(i,context);
      components.push_back(c);
    }
  }
  if ( sc == DF_SUCCESS ) 
    sc = action("SetProperties",&ComponentHandler::setProperties);
  if ( sc == DF_SUCCESS )
    sc = action("Initialize", &ComponentHandler::initialize);
  return sc;
}

/// Start the MBM server
int UnmanagedContainer::start()  {
  int sc = Component::start();
  if ( sc == DF_SUCCESS )
    sc = action("Start", &ComponentHandler::start);
  return sc;
}

/// Finalize the MBM server
int UnmanagedContainer::finalize()  {
  action("Finalize", &ComponentHandler::finalize);
  unsubscribeIncidents();
  return Component::finalize();
}

/// Stop the MBM server
int UnmanagedContainer::stop()  {
  action("Stop", &ComponentHandler::stop);
  return Component::stop();
}

/// Pause the data flow component. Default implementation is empty.
int UnmanagedContainer::pause()  {
  action("Pause", &ComponentHandler::pause);
  return DataflowComponent::pause();
}

/// Cancel the data flow component. Default implementation is empty.
int UnmanagedContainer::cancel()  {
  action("Cancel", &ComponentHandler::cancel);
  return DataflowComponent::cancel();
}

/// Continuing the data flow component. Default implementation is empty.
int UnmanagedContainer::continuing()  {
  action("Continue", &ComponentHandler::continuing);
  return DataflowComponent::continuing();
}

/// Data processing overload: Send data record to network client
int UnmanagedContainer::execute(const Context::EventData& event)  {
  try  {
    int ret = DF_SUCCESS;
    ComponentHandler handler(context);
    for(const auto& i : components)  {
      int sc = handler.execute(i,event);
      if ( sc != DF_SUCCESS )  {
	processingFlag &= ~DF_PASSED;
	ret = sc;
      }
    }
    return ret;
  }
  catch(const exception& e)   {
    error(e,"Execute: Error processing event.");
  }
  catch(...)  {
    error("Execute: UNKOWN error processing event.");
  }
  return DF_SUCCESS;
}
