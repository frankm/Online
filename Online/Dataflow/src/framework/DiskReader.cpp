//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  BurstReader.h
//--------------------------------------------------------------------------
//
//  Package    : GaudiOnline
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DiskReader.h>
#include <Dataflow/DataflowTask.h>
#include <Dataflow/ControlPlug.h>
#include <Dataflow/MBMClient.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/Plugins.h>
#include <Tell1Data/Tell1Decoder.h>
#include <PCIE40Data/pcie40.h>
#include <RTL/readdir.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <RZip.h>

/// C/C++ include files
#include <fcntl.h>
#include <cerrno>
#include <cstring>
#include <sstream>
#include <filesystem>
#include <dim/dic.h>
#include <dim/dis.h>

namespace fs = std::filesystem;
using namespace Online;
using namespace std;

namespace   {
  static constexpr int peek_size = int(8*sizeof(int));
}

bool DiskReader::check_consumers(const MBM::BufferInfo& info, int pid, int evtyp)   {
  return info.num_consumers_partid_evtype(pid,evtyp) != 0;
}
bool DiskReader::check_consumers_partid(const MBM::BufferInfo& info, int pid)   {
  return info.num_consumers_partid(pid) != 0;
}

unsigned char* DiskReader::MBMAllocator::operator()(size_t length)  {
  int status = MBM_ERROR;
  try   {
    status = reader->m_producer->spaceRearm(length);
  }
  catch (const exception& e)        {
    reader->error("Exception while reading input files (spaceRearm): %s BuffSize:%ld. ",
		  e.what(), length);
    ::lib_rtl_sleep(1000);
  }
  if ( status != MBM_NORMAL )  {
    return 0;
  }
  MBM::EventDesc& dsc = reader->m_producer->event();
  return (unsigned char*)dsc.data;
}

/// DIM command service callback
void DiskReader::go_handler(void* tag, void* address, int* size) {
  if ( address && tag && *size > 0 ) {
    DiskReader* rdr = *(DiskReader**)tag;
    rdr->m_goValue = *(int*)address;
  }
}

/// DIM command service callback
void DiskReader::run_number_change_handler(void* tag, void* address, int* size) {
  if ( address && tag && *size > 0 ) {
    DiskReader* rdr = *(DiskReader**)tag;
    rdr->m_runNumberValue = *(int*)address;
  }
}

/// DIM service update handler
void DiskReader::run_no_update(void* tag, void** buf, int* size, int* first)  {
  DiskReader* h = *(DiskReader**)tag;  
  if ( *first )  {
  }
  if ( h )  {
    *size = sizeof(h->m_currentRun);
    *buf  = &h->m_currentRun;
  }
}

/// Standard Constructor
DiskReader::DiskReader(const string& nam, DataflowContext& ctxt)
 : DataflowComponent(nam, ctxt)
{
  declareProperty("InputType",          m_inputType        = AUTO_INPUT_TYPE );
  declareProperty("Buffer",             m_buffer           = "Mep");
  declareProperty("Directories",        m_dirList );
  declareProperty("FilePrefix",         m_filePrefix       = "Run_");
  declareProperty("BrokenHosts",        m_brokenHostsFile  = "");
  declareProperty("DeleteFiles",        m_deleteFiles      = true);
  declareProperty("SaveRest",           m_saveRest         = true);
  declareProperty("ConsumerWait",       m_maxConsWait      = 20);
  declareProperty("MaxPauseWait",       m_maxPauseWait     = 1000);
  declareProperty("AllowedRuns",        m_allowedRuns);
  declareProperty("InitialSleep",       m_initialSleep     = 10);
  declareProperty("PauseSleep",         m_pauseSleep       = 5);
  declareProperty("Rescan",             m_rescan           = 1);
  declareProperty("ScanRecursive",      m_scanRecursive    = 0);
  declareProperty("GoService",          m_goService        = "");
  declareProperty("PackingFactor",      m_packingFactor    = 1);
  declareProperty("AllocationSizekB",   m_preAllocSize     = 2048);
  declareProperty("EventsPerFile",      m_max_events_per_file = -1);
  declareProperty("CheckedBuffers",     m_mbmNames);
  declareProperty("OnOpenFailedDelete", m_openFailDelete   = 0);  
  declareProperty("RequireConsumers",   m_requireConsumers = true);
  declareProperty("MuDelay",            m_muDelay          = 0);
  declareProperty("MMapFiles",          m_mmapFiles        = 0);
  declareProperty("ReuseFile",          m_reuse_file       = false);
  declareProperty("PatchOdin",          m_patch_odin       = 0);
  declareProperty("RunNumberService",   m_runNumberService = "");
  declareProperty("Stream",             m_stream           = "RAW");
  declareProperty("RunType",            m_runType          = "");
  declareProperty("PartitionName",      m_partitionName    = "LHCb");
  m_allowedRuns.push_back("*");
  ::lib_rtl_create_lock(0, &m_lock);
}

/// Standard Destructor
DiskReader::~DiskReader()  {
  if ( m_lock ) {
    ::lib_rtl_delete_lock(m_lock);
  }
}

/// Interprete file name with replacement strings if present
string DiskReader::interprete_file_tags(const string& file)   const {
  string file_name = file;
  file_name  = RTL::str_replace(file_name,  "${STREAM}",    this->m_stream);
  file_name  = RTL::str_replace(file_name,  "${RUNTYPE}",   this->m_runType);
  file_name  = RTL::str_replace(file_name,  "${PARTITION}", this->m_partitionName);
  return file_name;
}

/// Interprete file name with replacement strings if present
string DiskReader::interprete_file_name(const string& file, const string& run)  const   {
  string file_name(interprete_file_tags(file));
  if ( run[0] != '*' )    {
    char text[32];
    long runno = ::strtol(run.c_str(), 0, 10);
    ::snprintf(text, sizeof(text), "%010ld", (runno/1000)*1000);
    file_name  = RTL::str_replace(file_name, "${RUN1000}",   text);
    ::snprintf(text, sizeof(text), "%010ld", (runno/1000)*1000);
    file_name  = RTL::str_replace(file_name, "${RUN1000_8}", text);
    ::snprintf(text, sizeof(text), "%010ld", runno);
    file_name  = RTL::str_replace(file_name,  "${RUN}",       text);
    ::snprintf(text, sizeof(text), "%010ld", runno);
    file_name  = RTL::str_replace(file_name, "${RUN_8}",     text);
  }
  return file_name;
}

/// IService implementation: initialize the service
int DiskReader::initialize()   {
  int sc;
  string broken_hosts;
  if ( (sc = Component::initialize()) != DF_SUCCESS )
    return error("Failed to initialize service base class.");
  else if ( !context.mbm )
    return error("Failed to access MBM client.");
  broken_hosts = m_brokenHostsFile;

  m_filePrefix = interprete_file_tags(m_filePrefix);
  for(auto& dir: m_dirList)
    dir = interprete_file_tags(dir);
  
  if ( !broken_hosts.empty() ) {
    struct stat file;
    if ( 0 == ::stat(broken_hosts.c_str(),&file) ) {
      const string node = RTL::nodeNameShort();
      int fd = ::open(broken_hosts.c_str(),O_RDONLY);
      if ( -1 == fd )  {
        return error("Failed to access broken node file: %s [Error ignored]",broken_hosts.c_str());
      }
      char* data = new char[file.st_size+1];
      int rc = ::read(fd,data,file.st_size);
      if ( 1 == rc ) {
        data[file.st_size] = 0;
        for(int i=0; i<file.st_size; ++i) 
          data[i] = ::toupper(data[i]);
        for(char* ptr=(char*)node.c_str(); *ptr; ++ptr)
          *ptr = ::toupper(*ptr);
        if ( ::strstr(data,node.c_str()) ) {
          warning("Node is disabled and will not process any deferred files.");
          m_disabled = true;
        }
      }
      delete [] data;
      ::close(fd);
    }
    else {
      error("Failed to access broken node file:%s [Error ignored]",broken_hosts.c_str());
    }
  }
  m_preAllocSize = m_preAllocSize*1024;
  m_producer = context.mbm->createProducer(m_buffer, RTL::processName());
  if (0 == m_producer)  {
    return error("Fatal error: Failed to create MBM producer object.");
  }
  subscribeIncident("DAQ_PAUSE");
  subscribeIncident("DAQ_CANCEL");
  subscribeIncident("DAQ_STOP_TRIGGER");
  subscribeIncident("DAQ_START_TRIGGER");
  declareMonitor("Bursts","IN",    m_burstsIN=0,       "Number of bursts read");
  declareMonitor("Events","IN",    m_eventsIN=0,       "Number of events read");
  declareMonitor("Events","OUT",   m_eventsOUT=0,      "Number of events processed");
  declareMonitor("EvtCountFile",   m_evtCountFile=0,   "Number of events processed from current input");
  declareMonitor("FilesOpened",    m_filesOpened=0,    "Number of open file attempts");
  declareMonitor("FilesClosed",    m_filesClosed=0,    "Number of files closed");
  declareMonitor("FilesOpenOK",    m_filesOpenOK=0,    "Number of SUCCESSFUL open file attempts");
  declareMonitor("FilesOpenERROR", m_filesOpenERROR=0, "Number of FAILED open file attempts");

  ::lib_rtl_lock(m_lock);
  // m_files.clear();
  m_goto_paused = false;
  m_goValue = GO_PROCESS;
  if ( !m_goService.empty() )  {
    m_goValue = GO_DONT_PROCESS;
    m_goSvcID = ::dic_info_service(m_goService.c_str(),MONITORED,0,0,0,go_handler,(long)this,0,0);
  }
  string nam = RTL::processName() + "/CurrentRunNumber";
  m_runSvcID = ::dis_add_service(nam.c_str(),"I",0,0,run_no_update,(long)this);
  if ( !m_runNumberService.empty() )   {
    m_runNumberSvcID = ::dic_info_service(m_runNumberService.c_str(),MONITORED,0,0,0,
					  run_number_change_handler,(long)this,0,0);
    declareMonitor("RunNumberValue", m_runNumberValue, "Run number as received from DIM service");
  }
  // Buffer information blocks
  {
    std::lock_guard<std::mutex> lock(m_infoLock);
    if ( m_mbmNames.empty() ) m_mbmNames.push_back(m_buffer);
    m_mbmInfos = new MBM::BufferInfo[m_mbmNames.size()];
    for(size_t i=0; i<m_mbmNames.size(); ++i)  {
      string thename = m_mbmNames[i];
      string bm_name = context.mbm->bufferName(thename);
      m_mbmInfo[thename] = &m_mbmInfos[i];
      m_mbmInfos[i].attach(bm_name.c_str());
    }
  }
  checkAllowedRuns();
  return sc;
}

/// IService implementation: finalize the service
int DiskReader::finalize()  {
  unsubscribeIncidents();    {
    /// Lock buffer info: other threads may still use it
    std::lock_guard<std::mutex> lock(m_infoLock);
    m_mbmInfo.clear();
    if ( m_mbmInfos )  {
      delete [] m_mbmInfos;
      m_mbmInfos = 0;
    }  
    if ( m_runSvcID )  {
      ::dis_remove_service(m_runSvcID);
      m_runSvcID = 0;
    }
    if ( m_goSvcID )  {
      ::dic_release_service(m_goSvcID);
      m_goSvcID = 0;
    }
    if ( m_runNumberSvcID )  {
      ::dic_release_service(m_runNumberSvcID);
      m_runNumberSvcID = 0;
    }
    m_receiveEvts = false;
    m_goto_paused = false;
    m_goValue = GO_DONT_PROCESS;
  }
  if ( m_producer )  {
    //m_producer->exclude();
    detail::deletePtr(m_producer);
  }
  // m_files.clear();
  undeclareMonitors();
  undeclareMonitors("Events");
  undeclareMonitors("Bursts");
  /// Clear decompression buffer
  if ( deCompress.second ) delete [] deCompress.second;
  deCompress.second = nullptr;
  return Component::finalize();
}

/// Start the data flow component.
int DiskReader::start()  {
  this->m_goto_paused = false;
  this->m_receiveEvts = true;
  this->m_goValue     = this->m_goService.empty() ? GO_PROCESS : GO_DONT_PROCESS;
  if ( !this->m_runNumberService.empty() )   {
    int retry = 0;
    while ( this->m_runNumberValue <= 0 && ++retry < 1000 )  {
      ::lib_rtl_sleep(100);
      if ( (retry%25) == 0 )   {
	this->warning("Waiting for run number update.......");
      }
    }
    if ( this->m_runNumberValue <= 0 )   {
      return this->error("Failed to access run number service");
    }
  }
  ::lib_rtl_unlock(this->m_lock);
  return DF_SUCCESS;
}

/// IService implementation: finalize the service
int DiskReader::stop()   {
  this->m_goto_paused = false;
  this->m_receiveEvts = false;
  this->m_goValue     = GO_DONT_PROCESS;
  this->unsubscribeIncidents();
  if (this->m_receiveEvts)  {
    if (context.mbm)    {
      //context.mbm->cancelBuffers();
      int count = 100;
      while( --count >= 0 )  {
	if ( this->m_prodLock.try_lock() )  {
	  this->context.mbm->cancelBuffers();
	  this->m_prodLock.unlock();
	  break;
	}
	::lib_rtl_sleep(20);
      }
      // This only happens if the event loop is stuck somewhere.
      if ( count < 0 ) this->context.mbm->cancelBuffers();
    }
  }
  ::lib_rtl_unlock(this->m_lock);
  return DF_SUCCESS;
}

/// Stop gracefully execution of the dataflow component
int DiskReader::pause()  {
  this->m_goto_paused = true;
  this->m_receiveEvts = false;
  this->m_goValue     = GO_DONT_PROCESS;
  if ( this->context.mbm )  {
    // This is not terribly elegant, but we have to stop the DIM callback
    // and wait until the pipeline is empty....
    if (this->context.mbm)    {
      int count = 100;
      while( --count >= 0 )  {
	if ( this->m_prodLock.try_lock() )  {
	  this->context.mbm->cancelBuffers();
	  this->m_prodLock.unlock();
	  break;
	}
	::lib_rtl_sleep(20);
      }
      // This only happens if the event loop is stuck somewhere.
      if ( count < 0 ) this->context.mbm->cancelBuffers();
      this->info("Waiting until event pipeline is empty.....");
      this->waitForPendingEvents(this->m_maxPauseWait);
    }
    ::lib_rtl_unlock(this->m_lock);
    this->info("No events pending anymore...continue with PAUSE processing.....");
  }
  return DF_SUCCESS;
}

/// Continue the data flow component.
int DiskReader::continuing()  {
  this->m_goto_paused = false;
  this->m_receiveEvts = true;
  this->m_goValue     = this->m_goService.empty() ? GO_PROCESS : GO_DONT_PROCESS;
  ::lib_rtl_unlock(m_lock);
  this->info("Continue with EVENT processing.....");
  return DF_SUCCESS;
}

/// Cancel I/O operations of the dataflow component
int DiskReader::cancel()  {
  this->m_goValue = GO_DONT_PROCESS;
  this->m_receiveEvts = false;
  if (this->context.mbm)    {
    this->context.mbm->requestCancelBuffers();
    //context.mbm->cancelBuffers();
  }
  this->info("Executed cancellation of pending I/O requests.");
  ::lib_rtl_unlock(this->m_lock);
  return DF_SUCCESS;
}

/// Close file if the mapping should not be explicitly maintained
long DiskReader::closeFile(RawFile& file, bool drop_mapping)    {
  if ( file.isOpen() || file.isMapped() ) ++this->m_filesClosed;
  file.reset(drop_mapping);
  return 0;
}

/// Patch the string array with allowed runs
void DiskReader::checkAllowedRuns()   {
  for(size_t i=0; i < this->m_allowedRuns.size(); ++i)   {
    const char* crun = this->m_allowedRuns[i].c_str();
    if ( ::isdigit(crun[0]) && crun[0] != '*' )   {
      char text[PATH_MAX];
      long runno = ::strtol(crun,0,10);
      ::snprintf(text,sizeof(text),"%s%07ld_",this->m_filePrefix.c_str(),runno);
      this->m_allowedRuns[i] = text;
      this->info("Add run %s to allowed run-list.",this->m_allowedRuns[i].c_str());
    }
  }  
}

void DiskReader::waitForPendingEvents(int seconds)    {
  size_t count;
  do    {
    count = 0;   {
      std::lock_guard<std::mutex> lock(this->m_infoLock);
      for(auto i=this->m_mbmInfo.begin(); i != this->m_mbmInfo.end(); ++i)
	count += (*i).second->num_events();
    }
    if ( count > 0 ) ::lib_rtl_sleep(1000);
  } while ( count > 0 && --seconds >= 0 );
  ::lib_rtl_sleep(1000);
}

/// Incident handler implemenentation: Inform that a new incident has occured
void DiskReader::handle(const DataflowIncident& inc)
{
  this->info("Got incident: %s of type %s", inc.name.c_str(), inc.type.c_str());
  if (inc.type == "DAQ_CANCEL" )  {
  }
  else if ( inc.type == "DAQ_PAUSE" )  {
    this->m_goValue = GO_DONT_PROCESS;
    this->m_receiveEvts = false;
  }
  else if (inc.type == "DAQ_ENABLE")  {
    this->m_receiveEvts = true;
  }
  else if (inc.type == "DAQ_STOP_TRIGGER")  {
    this->m_receiveEvts = false;
  }
  else if (inc.type == "DAQ_START_TRIGGER")  {
    this->m_receiveEvts = true;
  }
}

/// Wait until consumers are ready (if required)
int DiskReader::waitForConsumers(const std::string& buffer_name)  {
  /// Before we start, we need to check if there are consumers present:
  if ( this->m_requireConsumers )  {
    int cons_wait = m_maxConsWait;
    int partid = context.mbm->partitionID();
    while( --cons_wait>=0 )  {
      { // Ensure proper scope to release lock before sleep
	std::lock_guard<std::mutex> lock(m_infoLock);
	auto it = this->m_mbmInfo.find(buffer_name);
	if ( it != this->m_mbmInfo.end() )   {
	  MBM::BufferInfo* info = it->second;
	  if ( info && check_consumers_partid(*info,partid) )   {
	    return DF_SUCCESS;
	  }
	}
	if ( this->m_mbmInfo.empty() || !this->m_receiveEvts )   {
	  return DF_ERROR; // Force failure in case if cancel, stop etc.
	}
      }
      ::lib_rtl_sleep(1000);
    }
    if ( cons_wait <= 0 )  {
      return DF_ERROR;
    }
  }
  return DF_SUCCESS;
}

/// If the GO service is present, wait until processing is enabled
int DiskReader::waitForGo()  {

  if ( this->m_initialSleep > 0 )   {
    ::lib_rtl_sleep(1000*this->m_initialSleep);
  }
  
  if ( !this->m_goService.empty() )   {
    while( this->m_receiveEvts && (GO_PROCESS != this->m_goValue) )  {
      ::lib_rtl_sleep(100);
    }
    if ( !this->m_receiveEvts || (GO_PROCESS != this->m_goValue) )  {
      // If a 'stop' came in the meantime we have to terminate.
      return DF_SUCCESS;
    }
  }
  
  if ( this->waitForConsumers(m_buffer) != DF_SUCCESS )   {
    this->fireIncident("DAQ_ERROR");
    return this->error("GO:   No consumers for buffer: %s partition %s to process event. --> ERROR",
		       this->m_buffer.c_str(), this->context.mbm->partitionName().c_str());
  }
  return DF_CONTINUE;
}

/// Update run number service
void DiskReader::updateRunNumber(int new_run)   {
  this->m_currentRun = new_run;
  if ( this->m_runSvcID ) ::dis_update_service(this->m_runSvcID);
}

/// On pause: save rest of unprocessed data
void DiskReader::save_file_rest(RawFile& file)   {
  if ( file.isOpen() )  {
    if ( this->m_deleteFiles && this->m_saveRest ) file.saveRestOfFile();
    ++this->m_filesClosed;
    file.reset(true);
  }
}

static long load_mdf_event(RawFile&     file,
		      std::pair<long,unsigned char*>  output,
		      std::pair<long,unsigned char*>& decomp )
{
  auto*  mdf_hdr   = (EventHeader*)output.second;
  long   status    = 0;
  int    compress  = mdf_hdr->compression();
  long   data_size = mdf_hdr->recordSize();
  if ( compress > 0 )   {
    int  new_size  = 0;
    int  tgt_size  = 0;
    int  src_size  = data_size;
    long hdr_len   = mdf_hdr->sizeOf(mdf_hdr->headerVersion());

    if ( decomp.first < data_size )   {
      decomp.first  = data_size*1.2;
      if ( decomp.second ) delete [] decomp.second;
      decomp.second = new unsigned char[decomp.first];
    }
    ::memcpy(decomp.second, output.second, peek_size);
    status = file.read(decomp.second + peek_size, data_size - peek_size);
    if ( status < data_size - peek_size )   {
      return -1;
    }
    if ( hdr_len-peek_size > 0 )   {
      ::memcpy(output.second, decomp.second, hdr_len);
    }
    if ( 0 == ::R__unzip_header(&src_size, decomp.second + hdr_len, &tgt_size) )   {
      if ( output.first < (long)tgt_size )   {
	return 0;
      }
      ::R__unzip( &src_size, decomp.second + hdr_len,
		  &tgt_size, output.second + hdr_len,
		  &new_size);
      if ( new_size > 0 )   {
	EventHeader* hdr = (EventHeader*)output.second;
	hdr->setHeaderVersion( 3 );
	hdr->setDataType(EventHeader::BODY_TYPE_BANKS);
	hdr->setSubheaderLength(sizeof(EventHeader::Header1));
	hdr->setSize(new_size);
	hdr->setCompression(0);
	hdr->setChecksum(0);
	hdr->setSpare(0);
	return hdr_len + new_size;
      }
    }
    return -1;
  }
  if ( output.first < data_size )   {
    return 0;
  }
  status = file.read(output.second + peek_size, data_size - peek_size);
  if ( status < data_size - peek_size )   {
    return -1;
  }
  return data_size;
}

/// Load event data from file into the buffer manager
DiskReader::LoadResult
DiskReader::load_event_data(RawFile::Allocator& allocator, RawFile& file)   {
  int      size[peek_size/sizeof(int)];
  uint8_t *data_ptr, *alloc_ptr;
  off_t    position = file.position();
  int      status   = file.read(size, peek_size);

  if ( status < int(peek_size) )   {
    this->closeFile(file, !m_reuse_file);
    return { 0, -1, RawFile::NO_INPUT_TYPE };
  }
  //
  // Auto detect data type: first check for PCIE40 MEP format
  auto* mep_hdr = (pcie40::mep_header_t*)size;
  if ( mep_hdr->is_valid() )  {
    int data_size = mep_hdr->size*sizeof(uint32_t);
    data_ptr = allocator(data_size);
    if ( !data_ptr )   {
      file.position(position);
      return { 0, -1, RawFile::NO_INPUT_TYPE };
    }
    ::memcpy(data_ptr, size, peek_size);
    status = file.read(data_ptr+peek_size, data_size-peek_size);
    if ( status < int(data_size-peek_size) )   {
      this->closeFile(file, !m_reuse_file);
      return { 0, -1, RawFile::NO_INPUT_TYPE };
    }
    ++this->m_burstsIN;
    mep_hdr = (pcie40::mep_header_t*)data_ptr;
    if ( mep_hdr->num_source > 0 )   {
      int packing  = mep_hdr->multi_fragment(0)->packingFactor();
      return { size_t(data_size), packing, RawFile::MEP_INPUT_TYPE };      
    }
    return { size_t(data_size), 1, RawFile::MEP_INPUT_TYPE };
  }
  //
  // Now check for MDF format: 3 times same size at the beginning of the record
  auto* mdf_hdr = (EventHeader*)size;
  if ( mdf_hdr->is_mdf() )   {
    int  num_evts   = 1;
    int  compress   = mdf_hdr->compression();
    long hdr_len    = mdf_hdr->sizeOf(mdf_hdr->headerVersion());
    long alloc_size = mdf_hdr->recordSize();
    long data_size  = 0;

    if ( compress > 0 )   {
      alloc_size = hdr_len + (mdf_hdr->compression() + 1) * mdf_hdr->size();
    }
    if ( m_packingFactor > 1 )   {
      alloc_size = std::max(alloc_size, m_preAllocSize);
    }
    alloc_ptr = data_ptr = allocator(alloc_size);
    if ( !alloc_ptr )   {
      file.position(position);
      return { 0, -1, RawFile::NO_INPUT_TYPE };
    }
    ::memcpy(data_ptr, size, peek_size);
    long length = load_mdf_event(file, MemBuffer(alloc_size,data_ptr), deCompress);
    if ( length <= 0 )   {
      length == 0 ? file.position(position) : this->closeFile(file, !m_reuse_file);
      return { 0, -1, RawFile::NO_INPUT_TYPE };
    }
    data_ptr  += length;
    data_size += length;
    for ( ; num_evts < m_packingFactor; )   {
      position = file.position();
      if ( alloc_size-data_size < peek_size )   {
	++this->m_burstsIN;
	return { size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
      }
      status   = file.read(data_ptr, peek_size);
      if ( status < peek_size )   {
	this->closeFile(file, !m_reuse_file);
	++this->m_burstsIN;
	return { size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
      }
      mep_hdr = (pcie40::mep_header_t*)data_ptr;
      if ( mep_hdr->magic == mep_hdr->MagicPattern )   {
	file.position(position);
	++this->m_burstsIN;
	return { size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
      }
      mdf_hdr = (EventHeader*)data_ptr;
      if ( !mdf_hdr->is_mdf() )   {
	error("Corrupted file found: %s at position %ld. Drop %ld bytes.",
		file.cname(), position, long(file.data_size())-file.position());
	this->closeFile(file, !m_reuse_file);
	++this->m_burstsIN;
	return { size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
      }
      if ( alloc_size-data_size < mdf_hdr->recordSize() )  {
	file.position(position);
	++this->m_burstsIN;
	return { size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
      }
      length = load_mdf_event(file, MemBuffer(alloc_size-data_size,data_ptr), deCompress);
      if ( length <= 0 )   {
	length == 0 ? file.position(position) : this->closeFile(file, !m_reuse_file);
	++this->m_burstsIN;
	return { size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
      }
      data_ptr  += length;
      data_size += length;
      ++num_evts;
    }
    ++this->m_burstsIN;
    return { size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
  }
#if 0
  else if ( size[0] > 0 && size[0] < 1024*1024*1024*500 )  {
    // Now the only thing left is Tell1 MEP format
    static int id = -1;
    static constexpr int peek_mep = peek_size - sizeof(int);
    int data_size = sizeof(MEPEVENT)+sizeof(Tell1MEP)+size[0];
    MEPEVENT* e   = (MEPEVENT*)(alloc_ptr=allocator(data_size));
    if ( e )   {
      Tell1MEP* me  = (Tell1MEP*)e->data;
      me->setSize(size[0]);
      e->refCount = 1;
      e->evID     = ++id;
      e->begin    = 0;
      e->packing  = -1;
      e->valid    = 1;
      e->magic    = mep_magic_pattern();
      for (size_t j = 0; j < MEP_MAX_PACKING; ++j)   {
	e->events[j].begin = 0;
	e->events[j].evID = 0;
	e->events[j].status = EVENT_TYPE_OK;
	e->events[j].signal = 0;
      }
      data_ptr = (uint8_t*)me->start();
      ::memcpy(data_ptr, &size[1], peek_mep);
      status = file.read(data_ptr + peek_mep, me->size() - peek_mep);
      if ( status < int(me->size() - peek_mep) )   {
	// End-of file, continue with next file
	this->closeFile(file, !m_reuse_file);
	return { 0, -1, RawFile::NO_INPUT_TYPE };
      }
      data_ptr += me->size();
      ++this->m_burstsIN;
      return { size_t(data_ptr-alloc_ptr), 1, RawFile::MEP_INPUT_TYPE };
    }
  }
#endif
  //
  // Last chance: we have a corrrupted MDF record. Try to sweep to the next MDF frame
  long skip = file.scanToNextMDF();
  if ( skip > 0 )  { // Just move on to next record!
    warning("Corrupted file found: %s at position %ld. "
	    "Skip %ld bytes after sweep to next MDF record.",
	    file.cname(), position, skip);
    return this->load_event_data(allocator, file);
  }
  error("Corrupted file found: %s at position %ld. Skip rest of file!",
	file.cname(), position);
  this->closeFile(file, !m_reuse_file);
  return { 0, -1, RawFile::NO_INPUT_TYPE };
}

/// Declare event frame to the buffer manager to trigger clients
int DiskReader::declare_mbm_data(const char* fname, const LoadResult& load)   {
  int             pid = context.mbm->partitionID();
  MBM::EventDesc& dsc = this->m_producer->event();
  EventHeader*    hdr = (EventHeader*)dsc.data;

  dsc.len = load.length;
  if ( hdr->is_mdf() )  {
    // If the first bank is a MDF header, we copy the real trigger mask
    this->m_currentRun = hdr->subHeader().H1->runNumber();
    context.manager.setRunNumber(this->m_currentRun);
    if ( dsc.len == hdr->recordSize() )  {
      ::memcpy(dsc.mask, &hdr->subHeader().H1->triggerMask()[0], sizeof(dsc.mask));
    }
    else   {
      dsc.mask[0] = ~0x0;
      dsc.mask[1] = ~0x0;
      dsc.mask[2] = ~0x0;
      dsc.mask[3] = ~0x0;
    }
    //dsc.type = (1 == load.packing && 1 == m_packingFactor) ? EVENT_TYPE_EVENT : EVENT_TYPE_BURST;
    dsc.type = EVENT_TYPE_EVENT;
  }
  else  {
    // If MEP, we emulate a trigger mask with the partition ID
    dsc.type    = EVENT_TYPE_MEP;
    dsc.mask[0] = pid;
    dsc.mask[1] = ~0x0;
    dsc.mask[2] = ~0x0;
    dsc.mask[3] = ~0x0;
  }
  // Check if there are consumers pending before declaring the event.
  // This should be a rare case, since there ARE (were?) consumers.
  // Though: In this case the event is really lost!
  // But what can I do...
  int cons_wait = this->waitForConsumers(m_buffer);
  if ( cons_wait != DF_SUCCESS )  {
    this->error("DECL: No consumers present for buffer: %s partition %s Event type:%d. [Required by setup]",
		this->m_buffer.c_str(), this->context.mbm->partitionName().c_str(), dsc.type);
    return DF_CONTINUE;
  }
  debug("++ Declared MBM data %p -> %p  [%ld bytes]", dsc.data, ((char*)dsc.data)+dsc.len, dsc.len);
  int status = MBM_ERROR;
  {
    lock_guard<mutex> lck(m_prodLock);
    //
    // Declare the event to the buffer manager
    //
    try {
      status = this->m_producer->declareEvent();
    }
    catch (const exception& e)        {
      info("Exception while delareEvent: %s File:%s.", e.what(), fname);
      status = MBM_ERROR;
    }
    catch(...)   {
      info("UNKNOWN Exception while delareEvent: File:%s.", fname);
      status = MBM_ERROR;
    }
    if (status == MBM_REQ_CANCEL )    {
      return DF_SUCCESS;
    }
    else if (status != MBM_NORMAL)    {
      return DF_CONTINUE;
    }
    //
    // Now send space
    //
    try  {
      status = this->m_producer->sendSpace();
    }
    catch (const exception& e)        {
      info("Exception while sendSpace: %s File:%s.", e.what(), fname);
      status = MBM_ERROR;
    }
    catch(...)   {
      info("UNKNOWN Exception while sendSpace: File:%s.", fname);
      status = MBM_ERROR;
    }
    if (status == MBM_REQ_CANCEL )    {
      return DF_SUCCESS;
    }
  }
  if (status != MBM_NORMAL)    {
    return DF_CONTINUE;
  }
  return DF_SUCCESS;
}
#include <PCIE40Data/sodin.h>
#include <Tell1Data/RunInfo.h>

namespace {

  bool _patch_odin_pcie40(void* data_ptr, size_t /* length */, int num, int external_runno)  {
    using namespace pcie40;
    static uint32_t curr_run = 0;
    static int32_t  num_evts = 0;
    static int64_t  now = ::clock();
    static int64_t  eid = 0;
    mep_header_t*   mep = (mep_header_t*)data_ptr;
    std::size_t     num_fragments = mep->num_source;

    if ( num_fragments > 0 )   {
      std::size_t packing  = mep->multi_fragment(0)->packingFactor();
      if ( packing > 0 )   {
	for(uint32_t i=0; i<num_fragments; ++i)    {
	  auto* mfp = mep->multi_fragment(i);
	  if ( mfp->is_valid() && *mfp->types() == pcie40::bank_t::ODIN )   {
	    frontend_data_t   *curr   = mfp->data();
	    const uint16_t    *lens   = mfp->sizes();
	    const uint8_t     version = mfp->header.version;
	    const std::size_t align   = mfp->header.alignment;
	    if ( external_runno > 0 )   {
	      curr_run = external_runno;
	      num_evts = 0;
	      eid = 0;
	    }
	    else if( 0 == curr_run || num_evts > num )  {
	      curr_run = (version < 7)
		? curr->begin<RunInfo>()->run_number()
		: curr->begin<pcie40::sodin_t>()->run_number();
	      num_evts = 0;
	      eid = 0;
	    }
	    for (std::size_t j=0, n=mfp->packingFactor(); j<n; ++j, ++lens)  {
	      auto  length = *lens;
	      ++num_evts;
	      if (version < 7)  {
		curr->begin<RunInfo>()->Run     = curr_run;
		curr->begin<RunInfo>()->GPSTime = ++now;
		curr->begin<RunInfo>()->L0ID    = ++eid;
	      }
	      else   {
		curr->begin<pcie40::sodin_t>()->_run_number = curr_run;
		curr->begin<pcie40::sodin_t>()->_gps_time   = ++now;
		curr->begin<pcie40::sodin_t>()->_event_id   = ++eid;
	      }
	      curr = curr->next(length, align);
	    }
	    return true;
	  }
	}
      }
    }
    return false;
  }

  bool _patch_odin_tell1(void* data_ptr, size_t length, int num, int external_runno)  {
    static uint32_t curr_run = 0;
    static int32_t  num_evts = 0;
    uint8_t* start = (uint8_t*)data_ptr;
    uint8_t* end   = start + length;

    while ( start < end )   {
      const EventHeader* header = (const EventHeader*)start;
      auto [evt_start, evt_end] = header->data_frame();
      long  evt_len = header->size0();
      if ( !header->is_mdf() )
	return false;
      else if ( evt_len <= 0 ) 
	return false;

      if ( external_runno > 0 )   {  // Run number e.g. from DIM
	num_evts = 0;
	curr_run = external_runno;
	header->subHeader().H1->setRunNumber(curr_run);
      }
      else if ( 0==curr_run || num_evts > num )  {
	num_evts = 0;
	curr_run = header->subHeader().H1->runNumber();
      }
      while ( evt_start < evt_end )  {
	Tell1Bank* bank = (Tell1Bank*)evt_start;
	int len = bank->totalSize();
	if ( len <= 0 )  {
	  return false;
	}
	if ( bank->type() == Tell1Bank::ODIN )   {
	  ++num_evts;
	  (bank->version() < 7) 
	    ? bank->begin<RunInfo>()->Run = curr_run
	    : bank->begin<pcie40::sodin_t>()->_run_number = curr_run;
	}
	evt_start += len;
      }
      start += evt_len;
    }
    return true;
  }
}

/// Handle MBM request: load event data and declare to MBM
int DiskReader::handle_mbm_request(RawFile& input)  {
  MBMAllocator allocator(this);
  off_t pos  = input.position();
  auto  load = this->load_event_data(allocator, input);

  if ( load.packing <= 0 )    {
    this->closeFile(input, !m_reuse_file);
    return DF_CONTINUE;
  }

  if ( this->m_patch_odin > 0 || this->m_runNumberValue > 0 )   {
    auto& e = this->m_producer->event();
    if ( load.type == RawFile::MDF_INPUT_TYPE )
      _patch_odin_tell1(e.data, load.length, this->m_patch_odin, this->m_runNumberValue);
    else if ( load.type == RawFile::MEP_INPUT_TYPE )
      _patch_odin_pcie40(e.data, load.length, this->m_patch_odin, this->m_runNumberValue);
    if ( this->m_currentRun != this->m_runNumberValue )  {
      this->m_currentRun = this->m_runNumberValue;
      this->updateRunNumber(this->m_currentRun);
    }
  }

  this->m_eventsIN  += load.packing;
  this->m_inputType  = load.type;

  int status = this->declare_mbm_data(input.cname(), load);
  switch( status )   {
  case DF_CONTINUE:
    return DF_CONTINUE;

  case DF_SUCCESS:
    this->m_eventsOUT    += load.packing;
    this->m_evtCountFile += load.packing;
    // If we have exceeded the total number of events per file, close it!
    if ( this->m_max_events_per_file > 0 && this->m_evtCountFile > this->m_max_events_per_file )
      this->closeFile(input, !m_reuse_file);
    return DF_SUCCESS;

  case DF_ERROR:
    input.position(pos);
    this->save_file_rest(input);
    /// Before actually declaring ERROR, we wait until no events are pending anymore.
    this->waitForPendingEvents(this->m_maxConsWait);
    return DF_ERROR;

  default:
    break;
  }
  return DF_SUCCESS;
}

DiskReader::DiskIO::DiskIO(DiskReader& r)
  : reader(r),
    allowedRuns(r.m_allowedRuns),
    dirList(r.m_dirList),
    badFiles(r.m_badFiles),
    filePrefix(r.m_filePrefix),
    map_file(r.m_mmapFiles),
    fail_delete(r.m_openFailDelete),
    ok_delete(r.m_deleteFiles),
    scan_recursive(r.m_scanRecursive)
{
}

/// Scan directory for matching files
size_t DiskReader::DiskIO::scanDirectory(const string& dir_name)   {
  fs::path dir_path(dir_name);
  error_code ec;
  if ( fs::exists(dir_path, ec) && fs::is_directory(dir_path, ec) )   {
    auto pref = fs::path(dir_path).append(this->filePrefix).string();
    bool take_all = (this->allowedRuns.size() > 0 && this->allowedRuns[0]=="*");

    for(auto const& entry : fs::directory_iterator{dir_path,ec})   {
      fs::path path(entry);
      if ( path.string() == "." )
	continue;
      if ( path.string() == ".." )
	continue;

      /// Check for directories: if required recurse
      if ( fs::is_directory(path, ec) )   {
	if ( !this->scan_recursive )
	  continue;
	if ( fs::is_empty(path, ec) )
	  continue;
	this->scanDirectory(path.string());
	continue;
      }
      /// Got a link or a real file
      if ( !pref.empty() && 0 != ::strncmp(path.c_str(),pref.c_str(),pref.length()) ) {
	continue;
      }
      else if ( !take_all )  {
	bool take_run = false;
	for(const auto& r : this->allowedRuns ) {
	  if ( path.string().find(r) != string::npos ) {
	    take_run = true;
	    break;
	  }
	}
	if ( !take_run ) continue;
      }
      RawFile file(path.string());
      // Remove the identified bad ones from the list...
      if ( this->badFiles.find(file.name()) != this->badFiles.end() )
	continue;
      this->files.insert(make_pair(path.string(),file));
    }
    this->reader.info("Scanned directory %s: %ld files.", dir_name.c_str(), this->files.size());
    return this->files.size();
  }
  this->reader.info("Failed to open directory: %s  [%s]",
		    dir_path.c_str(), ec.message().c_str());
  return 0;
}

/// Scan directory for matching files
size_t DiskReader::DiskIO::scanFiles()   {
  files.clear();
  if ( !reader.m_disabled )   {
    for(size_t i=0; i<dirList.size(); ++i)  {
      scanDirectory(dirList[i]);
    }
    if ( !reader.m_rescan ) reader.m_disabled = true;
    return files.size();
  }
  reader.info("Scanning directory list disabled!");
  return 0;
}

/// Open a new data file
RawFile DiskReader::DiskIO::openFile()   {
  string err;
  while ( files.size() > 0 )  {
    auto iter = files.begin();
    auto file = (*iter).second;
    files.erase(iter);
     ++reader.m_filesOpened;
    int fd = map_file ? file.openMapped() : file.open();
    if ( -1 == fd )    {
      ++reader.m_filesOpenERROR;
      if ( fail_delete != 0 )   {
	int sc = file.unlink();
        if (sc != 0)        {
	  err = RTL::errorString();
          reader.error("CANNOT OPEN/UNLINK file: [ignored] %s: [%s]",
		       file.cname(), !err.empty() ? err.c_str() : "????????");
	}
      }
      else if ( badFiles.find(file.name()) == badFiles.end() )  {
	// We only want to see the warning once!
	err = RTL::errorString();
	reader.warning("CANNOT OPEN file %s: [%s] --> Try the next one in the list.",
		       file.cname(), !err.empty() ? err.c_str() : "????????");
	badFiles.insert(file.name());
      }
      continue;
    }
    else  {
      if ( ok_delete )  {
        int sc = file.unlink();
        if ( sc == 1 )  {
	  ++reader.m_filesOpenERROR;
	  continue;
	}
	else if ( sc != 0 )  {
	  // Suspicion of bad disk:
	  auto dir = fs::path(file.name()).parent_path();
	  ::close(fd);
	  if ( fs::exists(dir) )   {
	    auto ff = files;
	    string dir_name = dir.string();
	    reader.info("File: %s SKIPPED for HLT processing. Remove input of device:%s",
			file.cname(), dir_name.c_str());
	    for( const auto& i : files )  {
	      if ( i.second.name().find(dir_name) == 0 )  {
		auto j = ff.find(i.first);
		if ( j != ff.end() ) ff.erase(j);
	      }
	    }
	    files = ff;
	    StringV dirs;
	    for(size_t i=0; i<dirList.size(); ++i)  {
	      if ( dirList[i] != dir_name ) dirs.push_back(dirList[i]);
	    }
	    dirList = dirs;
	  }
	  ++reader.m_filesOpenERROR;
	  continue;
	}
      }
#if _XOPEN_SOURCE >= 600 || _POSIX_C_SOURCE >= 200112L 
      if ( fd > 0 )   {
	// The specified data will be accessed only once. 
	::posix_fadvise(fd, 0, 0, POSIX_FADV_NOREUSE);
	// The application expects to access the specified data sequentially.
	::posix_fadvise(fd, 0, 0, POSIX_FADV_SEQUENTIAL);
      }
#endif
      ++reader.m_filesOpenOK;
      return file;
    }
    err = RTL::errorString();
    reader.error("FAILED to open file: %s for deferred HLT processing: %s.",
		 file.cname(), !err.empty() ? err.c_str() : "????????");
  }
  if ( files.size() == 0 )  {
    static int s_opening = 0;
    struct OpeningTracer {
      OpeningTracer()  { ++s_opening; }
      ~OpeningTracer() { --s_opening; }
    };
    OpeningTracer _hold;
    if ( s_opening < 2 )   {
      reader.info("Rescanning directory list.....");
      scanFiles();
      if ( files.size() > 0 )  {
	return openFile();
      }
    }
  }
  return RawFile();
}

/// IRunable implementation : Run the class implementation
int DiskReader::i_run()  {
  int status;
  DiskIO  io(*this);
  RawFile current_input;

  m_eventsIN = 0;
  m_goto_paused = true;
  m_receiveEvts = true;

  status = waitForGo();
  if ( status != DF_CONTINUE )  {
    return status;
  }

  while (1)  {
    bool files_processed = (GO_PROCESS==m_goValue) && (io.scanFiles() == 0);

    // Handle case to go to paused or change of the GO value
    if ( (m_goto_paused && files_processed) || (m_goValue != GO_PROCESS) )   {
      save_file_rest(current_input);
      updateRunNumber(0);
      // Before actually declaring PAUSED, we wait until no events are pending anymore.
      waitForPendingEvents(m_maxPauseWait);
      // Go to state PAUSED, all the work is done
      if ( m_goto_paused )  {
        // Sleep a bit before goung to pause
        info("Sleeping before going to PAUSE....");
        ::lib_rtl_sleep(1000*m_pauseSleep);
        fireIncident("DAQ_PAUSE");
	if ( context.task )   {
	  context.task->declareState(Control::ST_PAUSED);
	}
      }
      info("Quitting...");
      break;
    }
    else if ( files_processed ) {
      info("Locking event loop. Waiting for work....");
      updateRunNumber(0);
      ::lib_rtl_lock(m_lock);
    }
    // Again check if the GO value changed, since we may have been waiting in the lock!
    if ( !m_receiveEvts || (m_goValue == GO_DONT_PROCESS) )    {
      save_file_rest(current_input);
      info("Quitting...");
      break;
    }
    updateRunNumber(0);
    m_evtCountFile = 0;
    files_processed = (GO_PROCESS != m_goValue) && io.scanFiles() == 0;
    if ( files_processed )    {
      info("Exit event loop. No more files to process.");
      break;
    }
    // loop over the events
    while ( m_receiveEvts && (m_goValue != GO_DONT_PROCESS) )   {
      if ( m_muDelay > 0 )  {
	::usleep(m_muDelay);
      }
      if ( !current_input.isOpen() && (m_goValue == GO_PROCESS) )   {
        current_input = io.openFile();
	current_input.setReuse(m_reuse_file);
        m_evtCountFile = 0;
        if ( !current_input.isOpen() )   {
          files_processed = (m_goValue != GO_PROCESS) || (io.scanFiles() == 0);
          if ( files_processed )    {
            break;
          }
        }
	else   {
	  int runno = 0;
	  size_t idx = current_input.name().rfind('/')+1+m_filePrefix.length();
	  const char* ptr = current_input.name().c_str()+idx;
	  updateRunNumber(runno);
	  if ( 1 != ::sscanf(ptr, "%08d", &runno) )  {
	    ::sscanf(ptr, "%010d", &runno);
	  }
	}
      }
      else if ( m_reuse_file && m_mmapFiles && current_input.pointer() == current_input.end() )   {
	current_input.seek(0,SEEK_SET);
      }
      if ( current_input.isOpen() )  {
	status = handle_mbm_request(current_input);
	switch(status)   {
	case DF_SUCCESS:
	  continue;
	case DF_CONTINUE:
	  continue;
	case DF_ERROR:
	  m_receiveEvts = false;
	  /// Go to state ERROR, all the work is done
	  fireIncident("DAQ_ERROR");
	  return DF_ERROR;
	}
      }
      save_file_rest(current_input);
      // Bad file: Cannot read input (m_eventsIN==0)
      updateRunNumber(0);
      m_evtCountFile = 0;
    }
  }
  /// Before actually declaring PAUSED, we wait until no events are pending anymore.
  waitForPendingEvents(m_maxPauseWait);
  return DF_SUCCESS;
}

/// Runable implementation : Run the class implementation
int DiskReader::run()   {
  try  {
    int sc = i_run();
    return sc;
  }
  catch (const exception& e)  {
    error("Exception while reading MDF files: %s",e.what());
    throw;
  }
  catch (...)  {
    error("UNKNWON Exception while reading MDF files.");
    throw;
  }
}
