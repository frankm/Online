//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataflowComponent.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DataflowComponent.h>
#include <Dataflow/PropertyPrintout.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/Printout.h>
#include <Dataflow/Plugins.h>

#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TProfile3D.h>
#include <atomic>

using namespace std;
using namespace Online;

namespace {
  /// Helper function to steer printing
  bool may_print(const DataflowComponent* c, int level)  {
    int lvl = c->context.logger->level;
    return level >= c->outputLevel || level >= lvl;
  }
}

/// Initializing constructor
DataflowComponent::DataflowComponent(const string& nam, Context& ctxt)
  : name(nam), context(ctxt)
{
  declareProperty("OutputLevel", outputLevel=INFO);
}

/// Default destructor
DataflowComponent::~DataflowComponent()   {
}

/// Create (and register) a new component
DataflowComponent* 
DataflowComponent::create(const std::string& name_type_id, Context& ctxt)   {
  return Plugins::createComponent(name_type_id, ctxt);
}

/// Initialize the data flow component. Default implementation is empty.
int DataflowComponent::initialize()  {
  return setProperties();
}

/// Start the data flow component. Default implementation is empty.
int DataflowComponent::start()  {
  return DF_SUCCESS;
}

/// Stop the data flow component. Default implementation is empty.
int DataflowComponent::stop()  {
  return DF_SUCCESS;
}

/// Finalize the data flow component. Default implementation is empty.
int DataflowComponent::finalize()  {
  return DF_SUCCESS;
}

/// Pause the data flow component. Default implementation is empty.
int DataflowComponent::pause()  {
  return DF_SUCCESS;
}

/// Cancel the data flow component. Default implementation is empty.
int DataflowComponent::cancel()  {
  return DF_SUCCESS;
}

/// Enable the data flow component. Default implementation is empty.
int DataflowComponent::enable()  {
  return DF_SUCCESS;
}

/// Continuing the data flow component. Default implementation is empty.
int DataflowComponent::continuing()  {
  return DF_SUCCESS;
}

/// Execute runable. Default implementation throws an exception.
int DataflowComponent::run()   {
  return throwError("The default run-method is not supposed to be executed, "
		    "but overloaded by runable components.");
}

/// Execute event. Default implementation is empty.
int DataflowComponent::execute(const Context::EventData& /* event */)   {
  return DF_SUCCESS;
}

#define IMPLEMENT_SINGLE_MONITOR(x)					\
  namespace Online  {							\
    template <> const DataflowComponent&				\
    DataflowComponent::declareMonitor<x>(const std::string& n,		\
					 const x& v,			\
					 const std::string& d)  const {	\
      context.monitor->declare(this->name,n,&v,typeid(v),d);		\
      return *this;							\
    }									\
    template <> const DataflowComponent&				\
    DataflowComponent::declareMonitor<x>(const std::string& o,		\
					 const std::string& n,		\
					 const x& v,			\
					 const std::string& d)  const {	\
      context.monitor->declare(o,n,&v,typeid(v),d);			\
      return *this;							\
    }}

#define IMPLEMENT_PAIR_MONITOR(x)					\
  namespace Online  {							\
    template <> const DataflowComponent&				\
    DataflowComponent::declareMonitor<x>(const std::string& n,		\
					 const x& v1,			\
					 const x& v2,			\
					 const std::string& d)  const { \
      context.monitor->declare(this->name,n,&v1,&v2,typeid(v1),d);	\
      return *this;							\
    }									\
    template <> const DataflowComponent&				\
    DataflowComponent::declareMonitor<x>(const std::string& o,		\
					 const std::string& n,		\
					 const x& v1,			\
					 const x& v2,			\
					 const std::string& d)  const { \
      context.monitor->declare(o,n,&v1,&v2,typeid(v1),d);		\
      return *this;							\
    }}
#if 0
#define IMPLEMENT_POINTER_MONITOR(x)					\
  namespace Online  {							\
    template <> const DataflowComponent&				\
    DataflowComponent::declareMonitor<x*>(const std::string& n,		\
					  const x*& v,			\
					  const std::string& d) const { \
      context.monitor->declare(this->name,n,v,typeid(x),d);		\
      return *this;							\
    }									\
    template <> const DataflowComponent&				\
    DataflowComponent::declareMonitor<x*>(const std::string& o,		\
					  const std::string& n,		\
					  const x*& v,			\
					  const std::string& d) const { \
      context.monitor->declare(o,n,v,typeid(x),d);			\
      return *this;							\
    }									\
  }
#endif
#define IMPLEMENT_POINTER_MONITOR(x)                    \
namespace Online  {                           \
  template <> const DataflowComponent&                \
  DataflowComponent::declareMonitor<x>(const std::string& n,     \
                    const std::string &fmt, \
                    const x* v,          \
                    size_t s,        \
                    const std::string& d) const { \
    context.monitor->declare(this->name,n,fmt,(const void*)v,s,typeid(x*),d);     \
    return *this;                         \
  }                                   \
  template <> const DataflowComponent&                \
  DataflowComponent::declareMonitor<x>(const std::string& o,     \
                    const std::string& n,     \
                    const std::string &fmt, \
                    const x* v,          \
                    size_t s,        \
                    const std::string& d) const { \
    context.monitor->declare(o,n,fmt,(void*)v,s,typeid(x*),d);          \
    return *this;                         \
  }                                   \
}
#define IMPLEMENT_MONITOR(x)			\
  IMPLEMENT_SINGLE_MONITOR(x)			\
  IMPLEMENT_SINGLE_MONITOR(vector<x>)

IMPLEMENT_MONITOR(char)
IMPLEMENT_MONITOR(unsigned char)
IMPLEMENT_MONITOR(short)
IMPLEMENT_MONITOR(unsigned short)
IMPLEMENT_MONITOR(int)
IMPLEMENT_MONITOR(unsigned int)
IMPLEMENT_MONITOR(long)
IMPLEMENT_MONITOR(unsigned long)
IMPLEMENT_MONITOR(float)
IMPLEMENT_MONITOR(double)
IMPLEMENT_PAIR_MONITOR(int)
IMPLEMENT_PAIR_MONITOR(long)
//IMPLEMENT_SINGLE_MONITOR(string)
IMPLEMENT_SINGLE_MONITOR(TH1D)
IMPLEMENT_SINGLE_MONITOR(TH2D)
IMPLEMENT_SINGLE_MONITOR(TH3D)
IMPLEMENT_SINGLE_MONITOR(TProfile)
IMPLEMENT_SINGLE_MONITOR(TProfile2D)
IMPLEMENT_SINGLE_MONITOR(TProfile3D)
IMPLEMENT_MONITOR(atomic<int>)
IMPLEMENT_MONITOR(atomic<long>)
IMPLEMENT_MONITOR(atomic<float>)
IMPLEMENT_MONITOR(atomic<double>)
IMPLEMENT_POINTER_MONITOR(int)
IMPLEMENT_POINTER_MONITOR(long)
IMPLEMENT_POINTER_MONITOR(long long)
IMPLEMENT_POINTER_MONITOR(unsigned int)
IMPLEMENT_POINTER_MONITOR(unsigned long)
IMPLEMENT_POINTER_MONITOR(float)
IMPLEMENT_POINTER_MONITOR(double)

/// Declare monitoring structure with a given format to the monitoring component
#if 0
const DataflowComponent&
DataflowComponent::declareMonitor(const string& nam, 
				  const string& fmt, 
				  const void*        val, 
				  size_t             bytes,
				  const string& desc) const
{
  context.monitor->declare(this->name, nam, fmt, val, bytes, desc);
  return *this;
}
#endif
/// Undeclare monitoring information item
void DataflowComponent::undeclareMonitor(const std::string& owner, const std::string& nam) const   {
  context.monitor->undeclare(owner,nam);
}

/// Undeclare monitoring information item
void DataflowComponent::undeclareMonitor(const std::string& nam) const   {
  context.monitor->undeclare(this->name,nam);
}

/// Undeclare monitoring information
void DataflowComponent::undeclareMonitors(const std::string& owner)  const  {
  context.monitor->undeclare(owner);
}

/// Undeclare monitoring information
void DataflowComponent::undeclareMonitors()  const  {
  context.monitor->undeclare(this->name);
}

/// Set my own properties from the context
int DataflowComponent::setProperties()  {
  return context.options->setProperties(name, properties);
}

/// Check property for existence
bool DataflowComponent::hasProperty(const string& nam) const   {
  return properties.exists(nam);
}

/// Access single property
Property& DataflowComponent::property(const string& nam)   {
  return properties[nam];
}

/// Dump the properties of the component
void DataflowComponent::printProperties()  {
  PropertyPrintout printer(name);
  properties.for_each(printer);
}

/// Incident handler interface: Incident-subscription
int DataflowComponent::subscribeIncident(const string& incident_name)    {
  if ( !incident_name.empty() ) 
    return context.incidents->subscribe(this,incident_name);
  return error("subscribeIncident> Invalid incident name: '%s'.",incident_name.c_str());
}

/// Incident handler interface: un-subscribe from incident
int DataflowComponent::unsubscribeIncident(const string& incident_name)   {
  if ( !incident_name.empty() )
    return context.incidents->unsubscribe(this,incident_name);
  return error("unsubscribeIncident> Invalid incident name: '%s'.",incident_name.c_str());
}

/// Incident handler interface: un-subscribe this component from all incident
int DataflowComponent::unsubscribeIncidents()    {
  return context.incidents->unsubscribe(this);
}

/// Invoke incident
int DataflowComponent::fireIncident(const DataflowIncident& incident)   {
  return context.incidents->fire(this, incident);
}

/// Invoke incident
int DataflowComponent::fireIncident(const string& type)  {
  return fireIncident(DataflowIncident(name,type));
}

/// Incident handler callback: Inform that a new incident has occured
void DataflowComponent::handle(const DataflowIncident& /* incident */)   {
}

/// Always printout handling
void DataflowComponent::always(const char* msg,...) const   {
  va_list args;
  va_start(args, msg);
  printout(ALWAYS, name, msg, args);
}

/// Always printout handling
void DataflowComponent::always(const char* msg, va_list& args) const   {
  printout(ALWAYS, name, msg, args);
}

/// Debug printout handling
void DataflowComponent::debug(const char* msg,...) const   {
  if ( may_print(this,DEBUG) )  {
    va_list args;
    va_start(args, msg);
    printout(DEBUG, name, msg, args);
  }
}

/// Debug printout handling
void DataflowComponent::debug(const char* msg, va_list& args) const   {
  if ( may_print(this,DEBUG) )  {
    printout(DEBUG, name, msg, args);
  }
}

/// Info printout handling
void DataflowComponent::info(const char* msg, ...) const   {
  if ( may_print(this,INFO) )  {
    va_list args;
    va_start(args, msg);
    printout(INFO, name, msg, args);
    va_end(args);
  }
}

#if 0
/// Info printout handling
void DataflowComponent::info(const string& msg, ...) const   {
  if ( may_print(this,INFO) )  {
    va_list args;
    va_start(args, &msg);
    printout(INFO, name, msg, args);
    va_end(args);
  }
}
#endif
/// Info printout handling
void DataflowComponent::info(const char* msg, va_list& args) const  {
  if ( may_print(this,INFO) )  {
    printout(INFO, name, msg, args);
  }
}

/// Warning printout handling
void DataflowComponent::warning(const char* msg, ...) const   {
  if ( may_print(this,WARNING) )  {
    va_list args;
    va_start(args, msg);
    printout(WARNING, name, msg, args);
    va_end(args);
  }
}

/// Warning printout handling
void DataflowComponent::warning(const char* msg, va_list& args) const   {
  if ( may_print(this,WARNING) )  {
    printout(WARNING, name, msg, args);
  }
}
#if 0
/// Warning printout handling
void DataflowComponent::warning(const string& msg, ...) const   {
  if ( may_print(this,WARNING) )  {
    va_list args;
    va_start(args, &msg);
    printout(WARNING, name, msg, args);
    va_end(args);
  }
}

/// Error handling. Returns error code
int DataflowComponent::error(const string& msg, ...) const   {
  if ( may_print(this,ERROR) )  {
    va_list args;
    va_start(args, &msg);
    printout(ERROR, name, msg, args);
    va_end(args);
  }
  return DF_ERROR;
}
#endif
/// Error handling. Returns error code
int DataflowComponent::error(const char* msg, ...) const   {
  if ( may_print(this,ERROR) )  {
    va_list args;
    va_start(args, msg);
    printout(ERROR, name, msg, args);
    va_end(args);
  }
  return DF_ERROR;
}

/// Error handling. Technically returns error code. Throws internally exception
int DataflowComponent::error(const char* msg, va_list& args) const   {
  if ( may_print(this,ERROR) )  {
    printout(ERROR, name, msg, args);
  }
  return DF_ERROR;
}

/// Error handling: print exception. Technically returns error code.
int DataflowComponent::error(const exception& e, const char* msg, ...) const   {
  if ( may_print(this,ERROR) )  {
    string fmt = "Exception: ";
    fmt += e.what();
    fmt += " ";
    fmt += msg;
    va_list args;
    va_start(args, msg);
    printout(ERROR, name, fmt.c_str(), args);
    va_end(args);
  }
  return DF_ERROR;
}
#if 0
/// Error handling: print exception. Technically returns error code.
int DataflowComponent::error(const exception& e, const string& msg, ...) const   {
  if ( may_print(this,ERROR) )  {
    string fmt = "Exception: ";
    fmt += e.what();
    fmt += " ";
    fmt += msg;
    va_list args;
    va_start(args, &msg);
    printout(ERROR, name, fmt.c_str(), args);
    va_end(args);
  }
  return DF_ERROR;
}
#endif

/// Fatal handling. Technically returns error code. Throws internally exception
int DataflowComponent::fatal(const char* msg, va_list& args) const   {
  if ( may_print(this,FATAL) )  {
    printout(FATAL, name, msg, args);
  }
  return DF_ERROR;
}
#if 0
/// Fatal handling. Returns error code
int DataflowComponent::fatal(const string& msg, ...) const   {
  if ( may_print(this,FATAL) )  {
    va_list args;
    va_start(args, &msg);
    printout(FATAL, name, msg, args);
    va_end(args);
  }
  return DF_ERROR;
}
#endif
/// Fatal handling. Returns error code
int DataflowComponent::fatal(const char* msg, ...) const   {
  if ( may_print(this,FATAL) )  {
    va_list args;
    va_start(args, msg);
    printout(FATAL, name, msg, args);
    va_end(args);
  }
  return DF_ERROR;
}

/// Error handling. Technically returns error code. Throws internally exception
int DataflowComponent::throwError(const string& msg) const   {
  error(msg.c_str());
  throw runtime_error(msg);
  return DF_ERROR;
}

/// Error handling. Technically returns error code. Throws internally exception
int DataflowComponent::throwError(const char* msg,...) const   {
  va_list args;
  va_start(args, msg);
  except(name.c_str(), msg, args);
  va_end(args);
  return DF_ERROR;
}

/// Error handling. Technically returns error code. Throws internally exception
int DataflowComponent::throwError(const char* msg, va_list& args) const   {
  except(name.c_str(), msg, args);
  return DF_ERROR;
}

/// Generic output logging
void DataflowComponent::output(int level, const char* msg,...)   const   {
  if ( may_print(this,level) )  {
    va_list args;
    va_start(args, msg);
    printout(PrintLevel(level), name, msg, args);
    va_end(args);
  }
}
