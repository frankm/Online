//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  EventContext.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include "Dataflow/EventContext.h"
#include "DD4hep/Primitives.h"

using namespace std;
using namespace Online;

/// Default destructor
EventContext::EventItem::~EventItem()   {
}

/// Initializing constructor
EventContext::EventContext() 
{
}

/// Default destructor
EventContext::~EventContext()  {
  for(auto& d : data) delete d.second;
  data.clear();
}

/// Retrieve item by key
EventContext::EventItem* 
EventContext::item(unsigned int key, const std::type_info& typ)  const {
  const auto i = data.find(key);
  if ( i == data.end() ) invalidHandleError(typ);
  typeinfoCheck((*i).second->type, typ, "Cannot access Event item");
  return (*i).second;
}

/// Check existence of an item by key
bool EventContext::exists(unsigned int key)  const   {
  return data.find(key) != data.end();
}

/// Check existence of an item by key and type
bool EventContext::exists(unsigned int key, const std::type_info& typ)  const   {
  const auto i = data.find(key);
  if ( i == data.end() ) return false;
  if ( (*i).second->type != typ ) return false;
  return true;
}

