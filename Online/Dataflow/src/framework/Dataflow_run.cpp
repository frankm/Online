//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Dataflow_run_manager.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "Dataflow/DataflowManager.h"
#include "Dataflow/DataflowTask.h"
#include "Dataflow/Printout.h"
#include "RTL/Logger.h"
#include "RTL/strdef.h"
#include "RTL/rtl.h"

/// C/C++ include files
#include <stdexcept>
#include <thread>
#include <memory>

using namespace std;
using namespace Online;

namespace {

  void help()  {
    ::printf("dataflow_run_task -opt=<value> [-opt]                        \n"
	     "   -options=<file-name>    Main processing options.          \n"
	     "   -class=<task-class>     Task class (Class0,Class1,Class2) \n"
	     "   -automatic              Auto-startup mode (self config)   \n"
	     "\n");
    exit(0);
  }

  struct Dataflow_setup  {
    string class_type    {};
    string option_file   {};
    string printing      {"INFO"};
    string plug_type     {"Dataflow_DimControlPlug"};
    string option_type   {"Dataflow_JobOptions"};
    string monitor_type  {"Dataflow_Monitoring"};
    string logger_type   {"Dataflow_OutputLogger"};
    string inc_hdlr_type {"Dataflow_IncidentHandler"};
    bool   automatic     = false;
    bool   terminate     = false;
    bool   debug         = false;
    bool   print_options = false;
    bool   have_pause    = false;
    int print_level = INFO;
    void configure()   {
      switch(::toupper(printing[0]))  {
      case '7':
      case 'A':  print_level=ALWAYS;   break;
      case '6':
      case 'F':  print_level=FATAL;    break;
      case '5':
      case 'E':  print_level=ERROR;    break;
      case '4':
      case 'W':  print_level=WARNING;  break;
      case '3':
      case 'I':  print_level=INFO;  break;
      case '2':
      case 'D':  print_level=DEBUG;    break;
      case '1':
      case 'V':  print_level=VERBOSE;  break;
      default:   print_level=DEBUG;    break;
      }
      if ( debug )   {
	bool wait_for_debugger = true;
	auto debug_thread = make_unique<thread>([]{ 
	    char text[1024];
	    ::snprintf(text,sizeof(text),"gdb --pid %d",::lib_rtl_pid());
	    ::printf("[ERROR] %s\n\n",text);
	    ::fflush(stdout);
	  });
	while(wait_for_debugger)  {
	  ::lib_rtl_sleep(100);
	}
	debug_thread->join();
      }

      auto logger = std::make_shared<RTL::Logger::LogDevice>();
      RTL::Logger::LogDevice::setGlobalDevice(logger, print_level);
      RTL::Logger log(RTL::Logger::LogDevice::getGlobalDevice(),"Controller", print_level);

      if ( plug_type.find('/') == string::npos ) plug_type += "/Plug";
      if ( logger_type.find('/')  == string::npos ) logger_type  += "/Logger";
      if ( option_type.find('/')  == string::npos ) option_type  += "/JobOptions";
      if ( monitor_type.find('/')  == string::npos ) monitor_type  += "/Monitoring";
      if ( inc_hdlr_type.find('/')  == string::npos ) inc_hdlr_type  += "/IncidentHandler";
    }
    typedef vector<pair<string,string> > Opts;
    int run(Opts&& opts)   {
      int sc;
      DataflowManager manager("Manager",{logger_type,option_type,inc_hdlr_type,monitor_type});
      DataflowContext& ctxt = manager.context;

      ctxt.logger->level = print_level;
      if ( !option_file.empty() )   {
        sc = ctxt.options->load(option_file);
	if ( sc != DF_SUCCESS ) 
	  return manager.error("Failed to load job options: %s.",option_file.c_str());
      }
      if ( !opts.empty() )    {
	for ( const auto& o : opts )   {
	  size_t idx = o.first.find('.');
	  const string& comp  = o.first.substr(0,idx);
	  const string& name  = o.first.substr(idx+1);
	  ctxt.options->addProperty(comp, {name,o.second});
	}
	opts.clear();
      }
      if ( !class_type.empty() )  {
	manager.properties["TaskType"] = "Dataflow_"+class_type+"/Task";
      }
      // Now we can start:
      if ( (sc=manager.configure()) != DF_SUCCESS ) 
	return manager.error("Failed to configure manager. [sc=%d]",sc);
      if ( ctxt.task )  {
	ctxt.task->properties["Options"]      = option_file;
	ctxt.task->properties["AutoShutdown"] = terminate;
	ctxt.task->properties["AutoStart"]    = automatic;
	ctxt.task->properties["PlugType"]     = plug_type;
	ctxt.task->properties["HavePause"]    = have_pause;
	ctxt.task->run();
      }
      return 1;
    }
  };
}

extern "C" int dataflow_run_task(int argc, char** argv)  {
  Dataflow_setup setup;
  RTL::CLI cli(argc,argv,help);
  setup.automatic  = cli.getopt("automatic",3) != 0;
  setup.terminate  = cli.getopt("terminate",3) != 0;
  setup.debug      = cli.getopt("debug",3) != 0;
  setup.have_pause = cli.getopt("pause",3) != 0;

  cli.getopt("class",      3, setup.class_type);
  cli.getopt("options",    4, setup.option_type);
  cli.getopt("opts",       4, setup.option_file);
  cli.getopt("incidents",  3, setup.inc_hdlr_type);
  cli.getopt("plug",       3, setup.plug_type);
  cli.getopt("setup",      3, setup.option_type);
  cli.getopt("monitoring", 3, setup.monitor_type);
  cli.getopt("msgsvc",     3, setup.logger_type);
  cli.getopt("print",      3, setup.printing);
  setup.configure();
  return setup.run(vector<pair<string,string> >());
}

///  Online namespace declaration
namespace Online  {
  int dataflow_python_task(vector<pair<string,string> >&& options)   {    
    if ( !options.empty() )   {
      Dataflow_setup setup;
      vector<pair<string,string> > client_opts;

      ::lib_rtl_output(LIB_RTL_INFO,"Executing python startup of dataflow task....\n");
      for(const auto& o : options)   {
	if ( o.first.find("__MAIN__.") == string::npos )
	  break;
	else if ( o.first == "__MAIN__.PrintOptions" )
	  setup.print_options = ::toupper(o.second[0]) == 'T';
      }
      {
	vector<pair<string,string> > unknown_opts;
	for(const auto& o : options)   {
	  size_t idx = o.first.find('.');
	  const string& comp  = o.first.substr(0,idx);
	  const string& name  = o.first.substr(idx+1);
	  if ( setup.print_options )  {
	    ::lib_rtl_output(LIB_RTL_INFO," %s.%s = %s\n",comp.c_str(), name.c_str(), o.second.c_str());
	  }
	  if ( comp == "__MAIN__" )   {
	    if ( name == "OutputLogger" )
	      setup.logger_type = o.second;
	    else if ( name == "Monitoring" )
	      setup.monitor_type = o.second;
	    else if ( name == "JobOptions" )
	      setup.option_type = o.second;
	    else if ( name == "ControlPlug" )
	      setup.plug_type = o.second;
	    else if ( name == "OutputLevel" )
	      setup.printing = o.second;
	    else if ( name == "IncidentHandler" )
	      setup.inc_hdlr_type = o.second;
	    else if ( name == "Class" )
	      setup.class_type = o.second;
	    else if ( name == "Options" )
	      setup.option_file = o.second;
	    else if ( name == "PrintOptions" )
	      setup.print_options = ::toupper(o.second[0]) == 'T';
	    else if ( name == "Debug" )
	      setup.debug = ::toupper(o.second[0]) == 'T';
	    else if ( name == "Automatic" )
	      setup.automatic = ::toupper(o.second[0]) == 'T';
	    else if ( name == "Terminate" )
	      setup.terminate = ::toupper(o.second[0]) == 'T';
	    else if ( name == "HavePause" )
	      setup.have_pause = ::toupper(o.second[0]) == 'T';
	    else if ( name == "name" )
	      continue;
	    else
	      unknown_opts.push_back(o);
	    continue;
	  }
	  client_opts.push_back(o);
	}
	if ( !unknown_opts.empty() )   {
	  ::lib_rtl_output(LIB_RTL_ERROR,"Program configuration error: Unknwon options found\n");
	  for ( const auto& o : unknown_opts )   {
	    size_t idx = o.first.find('.');
	    const string& comp  = o.first.substr(0,idx);
	    const string& name  = o.first.substr(idx+1);
	    ::lib_rtl_output(LIB_RTL_ERROR,"ERROR: Unknown program option %s.%s = %s\n",
			     comp.c_str(), name.c_str(), o.second.c_str());
	  }
	  throw std::runtime_error("ERROR: Failed to configure dataflow task!");
	}
      }
      options.clear();
      setup.configure();
      return setup.run(move(client_opts));
    }
    throw std::runtime_error("ERROR: No job options supplied!");
  }
}
