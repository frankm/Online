//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MBMServer.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_LUMICOUNTER
#define ONLINE_DATAFLOW_LUMICOUNTER 1

// Framework include files
#include "Dataflow/TriggerBitCounter.h"
#include "dim/dim.hxx"

///  Online namespace declaration
namespace Online  {

  /// Luminosity counter class
  /** @class LumiCounter LumiCounter.h  components/LumiCounter.h
   *
   *
   * @author  Beat Jost
   * @version 1.0
   */
  class SynchTimer;
  class LumiCounter: public TriggerBitCounter   {
    public:
      typedef unsigned long        CounterType[4];
      typedef RunListItem<CounterType> LumiRunItem;

      /// The run-history file
      FILE*               m_RunHistory;
      /// Declare Monitoring information on the current run
      CounterType         m_Counter;
      /// Declare Monitoring information on the last set of runs
      RunMap<CounterType> m_RunMap;
      /// Current run number
//      unsigned int        m_RunNumber;
      bool m_dontCloseRuns;
      bool m_SynchPerEvent;
      int m_SynchInterval;
      SynchTimer *m_SynchTimer;
      /// Find Tell1 Bank in event buffer
      Tell1Bank *findOdinBank(void *currevt, int evlen);
      bool m_MultiInstance;
      /// Standard Constructor
      LumiCounter(const std::string& nam, Context& ctxt);
      /// Default destructor
      virtual ~LumiCounter();
      /// Initialize the component
      virtual int initialize()  override;
      /// Start the component
      virtual int start()  override;
      /// Stop the component
      virtual int stop()  override;
      /// Finalize the component
      virtual int finalize()  override;
      /// Process single event. Input buffer is ALWAYS in mdf bank format.
      virtual int execute(const Context::EventData& e)  override;
  };
  class SynchTimer : public DimTimer
  {
    public:
      LumiCounter *m_LumiCounter;
      int m_time;
      SynchTimer(LumiCounter *lc, int tim = 2)
      {
        m_LumiCounter = lc;
        m_time = tim;
      }
      void strt()
      {
        DimTimer::start(m_time);
      }
      void timerHandler() override
      {
        m_LumiCounter->m_RunMap.Sync();
        strt();
      }
  };
}      // End namespace LHCb
#endif //  ONLINE_DATAFLOW_LUMICOUNTER
