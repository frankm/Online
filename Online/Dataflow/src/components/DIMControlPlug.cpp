//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DimControlPlug.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DIMCONTROLPLUG_H
#define ONLINE_DATAFLOW_DIMCONTROLPLUG_H

// Framework include files
#include <Dataflow/ControlPlug.h>

/// Forward declarations

///  Online namespace declaration
namespace Online  {

  /// DIM command target for dataflow applications
  /** @class DimControlPlug DimControlPlug.h Dataflow/DimControlPlug.h
   *
   *
   *  \author  Markus Frank
   *  \version 1.0
   *  \ingroup DATAFLOW
   */
  class DimControlPlug : public ControlPlug  {
  public:
    int m_command;
    int m_status;
    int m_monitor;
  public:
    /// Initializing constructor
    DimControlPlug(const std::string& name, Context& context);
    /// Default destructor
    virtual ~DimControlPlug();
    /// (Re)connect DIM services
    virtual int connect()  override;
    /// Disconnect DIM services
    virtual int disconnect()  override;
    /// Publish the current state
    virtual int publishState()  override;
    /// Publish the monitor information
    virtual int publishMonitor()  override;
    /// Invoke transtion to target state or other commands
    virtual int invoke(const std::string& cmd)  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_DIMCONTROLPLUG_H


//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DimControlPlug.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
//#include "DimControlPlug.h"
#include <Dataflow/Plugins.h>
#include <Dataflow/Printout.h>
#include <dim/dis.h>
#include <cstring>

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_DimControlPlug,DimControlPlug)

using namespace std;
using namespace Online;

namespace {

  void feedCommand(void* tag, void* address, int* size)  {
    if ( tag && address && size )  {
      DimControlPlug* plug = *(DimControlPlug**)tag;
      string cmd = string((char*)address).substr(0,*size);
      plug->invoke(cmd);
    }
  }
  void feedStatus(void* tag, void** address, int* size, int* /* first */)  {
    if ( tag && address && size )  {
      DimControlPlug* plug = *(DimControlPlug**)tag;
      const string& s = plug->stateName();
      *address = (void*)s.c_str();
      *size = s.length()+1;
    }
  }
}

/// Initializing constructor
DimControlPlug::DimControlPlug(const std::string& nam, Context& ctxt)
  : ControlPlug(nam, ctxt), m_command(0), m_status(0), m_monitor(0)
{
  connect();
}

/// Default destructor
DimControlPlug::~DimControlPlug()  {
  disconnect();
}

/// Invoke transtion to target state or other commands
int DimControlPlug::invoke(const string& cmd)   {
  using namespace Control;
  if ( DF_SUCCESS == ControlPlug::invoke(cmd) )  {
    return DF_SUCCESS;
  }
  else if ( cmd == "stop_trigger" )   {
    std::string* value = new std::string("DAQ_STOP_TRIGGER");
    send(FIRE_INCIDENT, value);
    return DF_SUCCESS;
  }
  else if ( cmd == "start_trigger" )   {
    std::string* value = new std::string("DAQ_START_TRIGGER");
    send(FIRE_INCIDENT, value);
    return DF_SUCCESS;
  }
  else if ( ::strncmp(cmd.c_str(),"incident__",10) == 0 )  {
    send(FIRE_INCIDENT, new std::string(cmd.c_str()+10));
    return DF_SUCCESS;
  }
  else   {
    declareState(ST_ERROR);
    declareSubState(UNKNOWN_ACTION);
    return DF_SUCCESS;
  }
}

/// (Re)connect DIM services
int DimControlPlug::connect()  {
  if ( !m_command )  {
    std::string svcname;
    dim_init();
    svcname   = context.utgid;
    m_command = ::dis_add_cmnd(svcname.c_str(),"C",feedCommand,(long)this);
    svcname   = context.utgid+"/status";
    m_status  = ::dis_add_service(svcname.c_str(),"C",0,0,feedStatus,(long)this);
    svcname   = context.utgid+"/fsm_status";
    m_monitor = ::dis_add_service(svcname.c_str(),"L:2;I:1;C",&m_monitor,sizeof(m_monitor),0,0);
    ::dis_start_serving(context.utgid.c_str());
  }
  return DF_SUCCESS;
}

/// Disconnect DIM services
int DimControlPlug::disconnect()   {
  if ( m_monitor ) { ::dis_remove_service(m_monitor); m_monitor = 0; }
  if ( m_status  ) { ::dis_remove_service(m_status);  m_status = 0;  }
  if ( m_command ) { ::dis_remove_service(m_command); m_command = 0; }
  ::dis_stop_serving();
  return DF_SUCCESS;
}

/// Publish the current state
int DimControlPlug::publishState()   {
  //debug("PUBLISH state: %s",m_stateName.c_str());
  ::dis_update_service(m_status);
  return DF_SUCCESS;
}

/// Publish the monitor information
int DimControlPlug::publishMonitor()  {
  ::dis_update_service(m_monitor);
  return DF_SUCCESS;
}

