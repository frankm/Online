//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  AsioSender.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data sender 
//               using boost::asio asynchronous data I/O
//
//  Author     : Markus Frank
//==========================================================================

#define TRANSFER_NS        BoostAsio
#include <NET/Transfer.h>
#define DataSender Dataflow_AsioSender
#include <Dataflow/DataSender.h>
