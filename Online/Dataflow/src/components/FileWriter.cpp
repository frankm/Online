//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Filewriter.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Beat Jost
//==========================================================================

/// Framework includes
#include "FileWriter.h"
#include <Dataflow/DataflowManager.h>
#include <Dataflow/Incidents.h>
#include <Tell1Data/Tell1Decoder.h>

#include <MBM/Consumer.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

#include <ctime>
#include <regex>
#include <cerrno>
#include <fstream>
#include <filesystem>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/statvfs.h>
#include <dim/dic.hxx>

namespace fs = std::filesystem;

using namespace Online;

class FileWriter::SteeringInfo : DimInfo    {
public:
  int&        variable;
  FileWriter& parent;

  SteeringInfo(const std::string& nam, int &var, FileWriter& par)
    : DimInfo(nam.c_str(), -1), variable(var), parent(par)
  {
  }
  void infoHandler() override  {
    if ( (variable=getInt()) != 0 )      {
      parent.clearCounters();
    }
  }
};

/// RunDesc class definition
/** 
 *  @author Beat Jost
 *  @version 1.0
 */
class FileWriter::RunDesc  {
public:
  FileDescr   *file_desc     { nullptr };
  long         num_files     { 0 };
  long         num_events    { 0 };
  long         bytes_written { 0 };
  unsigned int run_number     { 0 };
  RunDesc() = default;
  RunDesc(int runno) : run_number(runno) {}
};

/// FileDescr class definition
/** 
 *  @author Beat Jost
 *  @version 1.0
 */
class FileWriter::FileDescr    {
public:
  std::string    fileName;
  RunDesc       *runDescriptor  { nullptr };
  unsigned int   run_number      { 0 };
  unsigned int   seq_number      { 0 };
  int            fileHandle     { 0 };
  int            state          { 0 };
  long           bytes_written  { 0 };
  std::time_t    closeAt        { 0 };
  std::time_t    lastWrite      { 0 };
  FileDescr(int handle, const std::string& name, int st, RunDesc *r) 
    : fileName(name), runDescriptor(r), run_number(r->run_number), 
      seq_number(r->num_files+1), fileHandle(handle), state(st)
  {
  }
};

// Standard Constructor
FileWriter::FileWriter(const std::string& nam, Context& ctxt)
  : Processor(nam, ctxt)
{
  this->m_filePrefix = "/alignment/${PARTITION}/${STREAM}/${RUN}/Run_${RUN}_${NODE}_${SEQ}.dat";
  this->declareProperty("FilePrefix",     this->m_filePrefix);
  this->declareProperty("DeviceList",     this->m_deviceList     = { "/hlt2"});
  this->declareProperty("SizeLimit",      this->m_sizeLimit      = 250);
  this->declareProperty("FileCloseDelay", this->m_fileCloseDelay = 10);
  this->declareProperty("MaxFilesOpen",   this->m_maxFilesOpen   = 1000);
  this->declareProperty("MaxEvents",      this->m_maxevts        = -1);
  this->declareProperty("EventFraction",  this->m_evfrac         = 1.0);
  this->declareProperty("DIMSteering",    this->m_dimSteering    = 0);
  this->declareProperty("DIMMonitoring",  this->m_dimMonitoring  = 0);
  this->declareProperty("NodePattern",    this->m_nodePattern    = "..eb..");
  this->declareProperty("ExitOnError",    this->m_ExitOnError    = true);
  this->declareProperty("Stream",         this->m_stream         = "RAW");
  this->declareProperty("RunType",        this->m_runType        = "");
  this->declareProperty("PartitionName",  this->m_partitionName  = "LHCb");
  this->m_thread = std::make_unique<std::thread>([this] { this->close_files(); });
}

// Standard Destructor
FileWriter::~FileWriter()   {
  this->m_texit = true;
  this->m_thread->join();
  this->m_thread.reset();
}

int FileWriter::initialize()   {
  int sc = Processor::initialize();
  std::string node = RTL::nodeNameShort();
  this->m_writeEnabled = true;
  if ( !this->m_nodePattern.empty() )   {
    const char* pattern = this->m_nodePattern.c_str();
    auto reg_nodePattern = std::regex(pattern, std::regex_constants::icase);
    this->m_writeEnabled = std::regex_search(node, reg_nodePattern);
  }
  if (sc == DF_SUCCESS)    {
    std::vector<std::string> to_keep;
    this->declareMonitor("EvtsIn",      this->m_evIn = 0,           "Number of Events received.");
    this->declareMonitor("EvtsOut",     this->m_evOut = 0,          "Number of Events written.");
    this->declareMonitor("BytesOut",    this->m_BytesOut = 0,       "Number of Bytes Written to File");
    this->declareMonitor("BadFrames",   this->m_num_bad_frames = 0, "Number bad frames");
    this->declareMonitor("FilesOpen",   this->m_filesOpen,          "Number of files opened");
    this->declareMonitor("FilesClosed", this->m_filesClosed,        "Number of files closed");
    this->m_sizeLimit *= 1024 * 1024;
    for (std::string dir_name : m_deviceList)  {
      dir_name = RTL::str_replace(dir_name, "${NODE}",      node);
      dir_name = RTL::str_replace(dir_name, "${STREAM}",    this->m_stream);
      dir_name = RTL::str_replace(dir_name, "${RUNTYPE}",   this->m_runType);
      dir_name = RTL::str_replace(dir_name, "${PARTITION}", this->m_partitionName);
      this->info("Check directory: %s",dir_name.c_str());

      std::error_code ec;
      fs::path dir(dir_name);
      if ( fs::exists(dir) )    {
	auto path = fs::path(dir).lexically_normal();
	this->info("Use   directory: %s", path.c_str());
	to_keep.push_back(path.string());
      }
      else if ( fs::create_directories(dir, ec) || ec.value() == EEXIST )    {
	auto path = fs::path(dir).lexically_normal();
	this->info("Use   directory: %s", path.c_str());
	to_keep.push_back(path.string());
      }
      else    {
	this->error("Cannot Create Directory %s", dir.c_str());
      }
    }
    this->m_deviceList = to_keep;
    if ( this->m_deviceList.empty() )   {
      this->m_disabled = true;
    }
    return DF_SUCCESS;
  }
  return this->error("Failed to initialize service base class.");
}

int FileWriter::start()   {
  this->Processor::start();
  if ( this->m_dimSteering != 0 )    {
    if ( !this->m_steeringSvc )	{
      std::string svcnam = this->m_partitionName + "/" + name + "/Control";
      this->m_steeringSvc = std::make_unique<SteeringInfo>(svcnam, this->m_steeringData, *this);
    }
    this->m_maxevts = -1;
    this->m_evOut = 0;
  }
  return DF_SUCCESS;
}

void FileWriter::clearCounters()   {
  this->m_evOut = 0;
  this->m_evIn = 0;
}

int FileWriter::stop()    {
  // Scan the run list and check for the files
  // which were not written the longest.
  for(const auto& r : this->m_runList )   {
    if ( r.second->file_desc != nullptr )   {
      this->markClose(r.second->file_desc, 0);
    }
  }
  return this->Processor::stop();
}

int FileWriter::finalize()   {
  this->m_steeringSvc.reset();
  this->clearCounters();
  this->unsubscribeIncidents();
  this->undeclareMonitors();
  return this->Processor::finalize();
}

void FileWriter::closeOutput()   {
  this->markClose(this->m_fileDesc, 0);
  this->m_fileDesc = nullptr;
}

/// Process single event. Input buffer is ALWAYS in mdf event or burst format.
int FileWriter::execute(const Context::EventData& e)   {
  if ( this->m_dimSteering != 0 && this->m_steeringData == 0 )   {
    closeOutput();
    return DF_SUCCESS;
  }
  else if ( !this->m_writeEnabled )   {
    closeOutput();
    return DF_SUCCESS;
  }
  this->m_evIn++;
  if ( this->m_disabled )   {
    closeOutput();
    return DF_SUCCESS;
  }
  else if ( this->m_maxevts > 0 && this->m_evt_number >= this->m_maxevts )   {
    closeOutput();
    return DF_SUCCESS;
  }
  /// Event accepted. Write it to output
  EventHeader *mdf  = (EventHeader*)e.data;
  if ( !mdf->is_mdf() )   {
    this->error("Bad MDF frame encountered. Skipping frame with %ld bytes.", size_t(e.length));
    if ( ++this->m_num_bad_frames > 3 ) ::lib_rtl_sleep(10000);
    return DF_SUCCESS;
  }
  int         status = DF_SUCCESS;
  std::size_t count  = 0;
  std::size_t len    = e.length;
  char*       beg    = (char*)e.data;
  char*       end    = beg + len;
  Context::EventData event;
  while ( beg < end )  {
    auto* hdr = (EventHeader*)beg;
    if ( !hdr->is_mdf() )    {
      this->error("Event buffer does not have MDF format. Cannot write output!");
      if ( ++this->m_num_bad_frames > 3 ) ::lib_rtl_sleep(10000);
      break;
    }
    auto m = hdr->subHeader().H1->triggerMask();
    ++m_evIn;
    ++count;
    event.release = 0;
    event.data    = beg;
    event.header  = hdr;
    event.length  = hdr->size0();
    event.type    = EVENT_TYPE_EVENT;
    event.mask[0] = m[0];
    event.mask[1] = m[1];
    event.mask[2] = m[2];
    event.mask[3] = m[3];
    beg          += hdr->size0();
    if ( !matchRequirements(event) )
      continue;
    m_evt_number++;
    if ( this->m_evfrac < 1e0 )   {
      double frac = double(::rand()) / double(RAND_MAX);
      if ( frac > this->m_evfrac )
	continue;
    }
    int ret = this->writeEvent(hdr);
    if ( ret != DF_SUCCESS )   {
      status = ret;
    }
    else   {
      this->m_num_bad_frames = 0;
    }
  }
  return status;
}

/// Write single event to disk. Input buffer is ALWAYS in mdf bank format.
int FileWriter::writeEvent(const EventHeader* mdf)   {
  if ( !mdf->is_mdf() )    {
    this->error("Event buffer does not have MDF format. Cannot write output!");
    return DF_ERROR;
  }
  std::time_t now = ::time(nullptr);
  unsigned int runno = mdf->subHeader().H1->runNumber();
  RunDesc  *run_desc = this->m_runList[runno];
  if ( run_desc == 0 )    {
    this->m_runList[runno] = run_desc = new RunDesc(runno);
    this->createMonitoringInfo(runno);
  }

  this->m_fileDesc = run_desc->file_desc;
  /// If there is no file attached to this run: open a new one
  if ( m_fileDesc == 0 )    {
    run_desc->file_desc = nullptr;
    this->m_fileDesc = this->openFile(run_desc);
    if ( this->m_fileDesc )   {
      long num_close_queue = 0;
      {
	std::lock_guard<std::mutex> lock(this->m_listlock);
	num_close_queue = this->m_fileCloseList.size();
      }
      run_desc->file_desc = this->m_fileDesc;
      ++run_desc->num_files;
      ++this->m_filesOpen;
      if ( (this->m_filesOpen - num_close_queue) > this->m_maxFilesOpen )   {
	/// Scan the run list and check for the files
	/// which were not written the longest.
	std::map<unsigned int,std::vector<FileDescr*> > files;
	for(const auto& r : m_runList )   {
	  if ( r.second->file_desc )   {
	    files[r.second->file_desc->lastWrite].emplace_back(r.second->file_desc);
	  }	 
	}
	for(auto i=files.begin(); i!=files.end(); ++i)   {
	  for( auto* f : i->second )
	    this->markClose(f, 0);
	  if ( (this->m_filesOpen - num_close_queue) < this->m_maxFilesOpen )
	    break;
	}
      }
    }
  }
  if (this->m_fileDesc == 0)   {
    auto ec = std::make_error_code(std::errc(errno));
    this->error("File Write Error: Cannot open output file: Run: %d Error: %d: [%s]",
		runno, ec.value(), ec.message().c_str());
    ::lib_rtl_sleep(2000);
    return DF_ERROR;
  }

  ssize_t status = this->write(this->m_fileDesc, mdf, mdf->recordSize());
  if (status != -1)    {
    this->m_evOut                  += 1;
    this->m_BytesOut               += mdf->recordSize();

    this->m_fileDesc->lastWrite     = now;
    this->m_fileDesc->bytes_written += mdf->recordSize();

    run_desc->num_events++;
    run_desc->bytes_written   += mdf->recordSize();

    if ( this->m_fileDesc->bytes_written > this->m_sizeLimit )
      this->markClose(this->m_fileDesc, this->m_fileCloseDelay);
    this->m_fileDesc = nullptr;
    return DF_SUCCESS;
  }
  return this->error("Failed to write event for run:%d to file %s.",
		     runno, this->m_fileDesc->fileName.c_str());
}

void FileWriter::close_files()  {
  while ( !this->m_texit )    {  {
      /// This needs to be protected. Beware of scope!
      std::time_t now = ::time(0);
      std::lock_guard<std::mutex> lock(this->m_listlock);
      for (auto i = this->m_fileCloseList.begin(); i != this->m_fileCloseList.end(); ++i)  {
	auto* file = *i;
	if (now > file->closeAt)    {
	  this->debug("Close file run:%6d seq:%5d %s [%7d kB]",
		      file->run_number, file->seq_number, file->fileName.c_str(),
		      int(file->bytes_written/1024));
	  ::close(file->fileHandle);
	  ++this->m_filesClosed;
	  this->m_fileCloseList.erase(i);
	  delete file;
	  i = this->m_fileCloseList.begin();
	  if ( 0 == (this->m_filesClosed%100) )   {
	    this->info("Wrote a total of %ld files Events: %8ld [%7.1f GB]",
		       this->m_filesClosed, this->m_evOut,
		       double(this->m_BytesOut)/double(1024*1024*1024));
	  }
	}
      }
    }
    ::lib_rtl_sleep(1000);
  }
}

void FileWriter::markClose(FileDescr* file, int delay)   {
  if ( file )    {
    /// First part is all synchronous
    file->runDescriptor->file_desc = nullptr;
    file->closeAt = ::time(0) + delay;
    file->state   = C_CLOSED;
    --this->m_filesOpen;
    this->debug("Mark file for delete run:%6d seq:%5d %s [%ld kB]",
		file->run_number, file->seq_number, file->fileName.c_str(),
		int(file->bytes_written/1024));

    /// This needs to be protected
    std::lock_guard<std::mutex> lock(this->m_listlock);
    this->m_fileCloseList.push_back(file);
  }
}

FileWriter::FileDescr *FileWriter::openFile(RunDesc* run_desc)    {
  char text[80];
  std::string node = RTL::nodeNameShort();
  unsigned int runn = run_desc->run_number;
  std::string  file_name = this->m_filePrefix;
  std::string  file_time = this->fileTime();

  file_name  = RTL::str_replace(file_name,  "${TIME}",      file_time);
  file_name  = RTL::str_replace(file_name,  "${NODE}",      node);
  file_name  = RTL::str_replace(file_name,  "${STREAM}",    this->m_stream);
  file_name  = RTL::str_replace(file_name,  "${RUNTYPE}",   this->m_runType);
  file_name  = RTL::str_replace(file_name,  "${PARTITION}", this->m_partitionName);
  ::snprintf(text, sizeof(text), "%010d", int((runn/1000)*1000));
  file_name  = RTL::str_replace(file_name,  "${RUN1000}",   text);
  ::snprintf(text, sizeof(text), "%010d", runn);
  file_name  = RTL::str_replace(file_name,  "${RUN}",       text);
  ::snprintf(text, sizeof(text), "%010ld", run_desc->num_files);
  file_name  = RTL::str_replace(file_name,  "${SEQ}",       text);
  ::snprintf(text, sizeof(text), "%06d", ::lib_rtl_pid());
  file_name  = RTL::str_replace(file_name,  "${PID}",       text);

  int    indx = getDevice(this->m_deviceList);
  if ( indx < 0 )    {
    if ( m_ExitOnError )   {
      this->fatal("===========> FATAL: Cannot find a device with free space. Exiting...");
      ::exit(0);
    }
    else    {
      this->info("Cannot find a device with free space.");
      return nullptr;
    }
  }

  std::string flname  = this->m_deviceList[indx] + "/" + file_name;
  fs::path file = fs::path(flname).lexically_normal();
  fs::path dir  = file.parent_path();
  if ( !dir.empty() && !fs::exists(dir) )   {
    std::error_code ec;
    if ( !fs::create_directories(dir, ec) )  	{
      // Another node may have created the directory in the meantime. Double check!
      if ( !fs::exists(dir) )   {
	this->error("Cannot Create Directory %s %d: [%s]",
		    dir.c_str(), ec.value(), ec.message().c_str());
	return nullptr;
      }
    }
  }

  int fh = ::open(file.c_str(),
		  O_RDWR + O_CREAT + O_APPEND + O_LARGEFILE + O_NOATIME,
		  S_IRWXU | S_IRWXG | S_IRWXO);
  if ( fh ==-1 )    {
    auto ec = std::make_error_code(std::errc(errno));
    std::string fmt = "===========> Cannot open file %s %d: [%s]";
    if (errno == EROFS)  {
      fmt = "===========> File System is Read-Only. Cannot open file %s %d: [%s]";
    }
    this->warning(fmt.c_str(), file.c_str(), errno, ec.message().c_str());
    return nullptr;
  }
  
  FileDescr *desc = new FileDescr(fh, file_name, C_OPEN, run_desc);
  this->debug("Open file [%s] Seq:%5d  %s", this->name.c_str(), desc->seq_number, file.c_str());
  return desc;
}

void FileWriter::getRODevices()   {
  std::ifstream infile("/proc/mounts");
  std::string   line;

  this->m_roDevices.clear();
  while ( getline(infile,line) )    {
    const char* ptr = line.c_str();
    std::size_t idx = line.find(' ');
    std::size_t idq = line.find(' ', idx+1);
    line[idx] = line[idq] = 0;
    for ( const auto& dev : m_deviceList )   {
      if ( dev.find(ptr+idx+1) != std::string::npos )    {
	if ( line.find(" ro,", idq+1) == std::string::npos )  {
	  this->m_roDevices.insert(dev);
	}
      }
    }
  }
  infile.close();
}

bool FileWriter::roDevice(const std::string& dev)  {
  return (this->m_roDevices.find(dev) != this->m_roDevices.end());
}

int FileWriter::getDevice(std::vector<std::string> &devlist)  {
  struct statvfs fsstat;
  fsblkcnt_t fblock = 0;
  std::size_t maxindx = -1;
  this->getRODevices();
  for (std::size_t i = 0; i < devlist.size(); i++)    {
    std::size_t idx = devlist[i].find("/",1);
    std::string root = idx == std::string::npos ? devlist[i] : devlist[i].substr(0,idx);
    if ( this->roDevice(devlist[i]) )   {
      int stat = ::statvfs(root.c_str(), &fsstat);
      if (stat == 0)   {
	if ( fsstat.f_bfree > fblock )   {
	  fblock = fsstat.f_bfree;
	  maxindx = i;
	}
      }
    }
  }
  return maxindx;
}

void FileWriter::createMonitoringInfo(unsigned int runn)   {
  if ( this->m_dimMonitoring )  {
    RunDesc *run_desc = this->m_runList[runn];
    std::string namePref = std::to_string(runn);
    std::string mnam = namePref + "/NoFiles";
    this->declareMonitor(namePref + "NoFiles", run_desc->num_files, "Number of Files");
    mnam = namePref + "/Events";
    this->declareMonitor(mnam, run_desc->num_events, "Number of Events");
    mnam = namePref + "/Bytes";
    this->declareMonitor(mnam, run_desc->bytes_written, "Number of Bytes");
  }
}

std::string FileWriter::fileTime()   {
  struct timeval tv;
  char buffer[128];
  ::gettimeofday(&tv, nullptr);
  struct tm *timeinfo = ::localtime(&tv.tv_sec);
  ::strftime(buffer, sizeof(buffer), "%Y%m%d-%H%M%S-", timeinfo);
  ::snprintf(buffer+::strlen(buffer), sizeof(buffer)-::strlen(buffer),"%03ld", tv.tv_usec/1000);
  return buffer;
}

void FileWriter::handleFileWriteError()  {
#if 0
  std::string node = RTL::nodeNameShort();
  for (unsigned int i = 0; i < node.size(); i++)
    node[i] = toupper(node[i]);
  std::string cmdname = node + "_MEPRx_01/setOverflow";
  DimClient::sendCommand(cmdname.c_str(), 2);
#endif
}

ssize_t FileWriter::write(FileDescr* dsc, const void *buf, size_t n)   {
  const char *cbuf = (const char*) buf;
  std::size_t towrite   = n;
  int    status    = 0;
  int    nintr_max = 2;
  int    fd = dsc->fileHandle;

  while (towrite > 0)    {
    status = ::write(fd, cbuf, towrite);
    if (status > 0)	{
      towrite = towrite - status;
      cbuf += status;
      continue;
    }
    else if (status == 0)   {
      this->info("0 Bytes written! Ignoring MEP");
      status = -1;
      return status;
    }
    else if (status == -1)	{
      auto err = std::make_error_code(std::errc(errno));
      if (err.value() == EIO || err.value() == ENOSPC)    {
	this->info("File Write Error (IO or NoSpace): Errno = %d [%s]",err.value(),err.message().c_str());
	this->handleFileWriteError();
	status = -1;
	return status;
      }
      else if ( err.value() == EINTR )    {
	nintr_max--;
	if (nintr_max > 0)   {
	  continue;
	}
	else	{
	  status = -1;
	  return status;
	}
      }
      else    {
	this->info("File Write Error: Errno = %d [%s]",err.value(),err.message().c_str());
	this->handleFileWriteError();
	status = -1;
	return status;
      }
    }
  }
  status = 0;
  return status;
}
