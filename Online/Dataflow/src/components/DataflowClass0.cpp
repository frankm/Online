//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Receiver.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DataflowTask.h>

///  Online namespace declaration
namespace Online  {

  /// Functor to print properties of a component
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup DD4HEP_SIMULATION
   */
  class DataflowClass0 : public DataflowTask  {
  public:
    /// Initializing constructor
    DataflowClass0(const std::string& nam, Context& ctxt);
    /// Default destructor
    virtual ~DataflowClass0();
    /// Configure the instance (create components)
    virtual int configure()  override;
    /// Terminate the dataflow task instance.
    virtual int terminate()  override;
    /// Final act of the process: unload image
    virtual int unload()  override;
  };
}      // end namespace Online
//==========================================================================

/// Framework include files
#include <Dataflow/ControlPlug.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <unistd.h>

using namespace std;
using namespace Online;

/// Initializing constructor
DataflowClass0::DataflowClass0(const std::string& nam, Context& ctxt)
  : DataflowTask(nam,ctxt)
{
}

/// Default destructor
DataflowClass0::~DataflowClass0()  {
}

/// Configure the instance (create components)
int DataflowClass0::configure()  {
  int ret = app_configure();
  if ( ret == DF_SUCCESS )  {
    ret = app_initialize();
    if ( ret == DF_SUCCESS )  {
      ret = app_start();
      if ( ret == DF_SUCCESS )  {
	return DataflowTask::configure();
      }
    }
  }
  return failed();
}

/// Terminate the dataflow task instance.
int DataflowClass0::terminate()  {
  app_stop();
  app_finalize();
  app_terminate();
  return DataflowTask::terminate();
}

/// Final act of the process: unload image
int DataflowClass0::unload()  {
  app_terminate();
  declareState(Control::ST_OFFLINE);
  ::lib_rtl_sleep(100);
  ::_exit(0);
  return DF_SUCCESS;
}


#include "Dataflow/Plugins.h"
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_Class0,DataflowClass0)
