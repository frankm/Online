//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  AsioSelector.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data server
//               using private socket data I/O
//
//  Author     : Markus Frank
//==========================================================================
#define TRANSFER_NS        BoostAsio
#include <NET/Transfer.h>
#define DataSelector Dataflow_AsioSelector
#include <Dataflow/DataSelector.h>
