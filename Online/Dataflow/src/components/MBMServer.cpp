//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MBMServer.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "MBMServer.h"
#include <MBM/Requirement.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <cctype>
#include <cstdio>
#include <cstring>

using namespace Online;

/// Initializing constructor
MBMServer::MBMServer(const std::string& nam, Context& ctxt)
  : Component(nam, ctxt), m_partition_id(0x103)
{
  this->declareProperty("PartitionID",          this->m_partition_id);
  this->declareProperty("PartitionName",        this->m_partition_name="");
  this->declareProperty("InitFlags",            this->m_init_flags);
  this->declareProperty("PartitionBuffers",     this->m_partition_buffers=false);
  this->declareProperty("ConsumerRequirements", this->m_consumer_requirements);
  this->declareProperty("AccidentBuffers",      this->m_accident_buffers);
  this->declareProperty("AccidentOutput",       this->m_accident_output);
}

/// Default destructor
MBMServer::~MBMServer()   {
}

/// Initialize the MBM server
int MBMServer::initialize()  {
  info("MBM Server: in initialize.");
  this->Component::initialize();
  this->info("Initializing MBM Buffers.");
  if ( !this->m_init_flags.empty() )  {
    std::size_t ikey = 0;
    char        *items[64], part_id[16];
    std::string tmp = this->m_init_flags;
    std::snprintf(part_id, sizeof(part_id), "%X", this->m_partition_id);
    for(char* tok = ::strtok((char*)tmp.c_str()," "); tok; tok = ::strtok(NULL," ")) {
      if ( this->m_partition_buffers && ::toupper(tok[1]) == 'I' )  {
	std::string bm_name = this->bufferName(tok);
        items[ikey++] = std::strcpy(new char[bm_name.length()+1],bm_name.c_str());
        continue;
      }
      items[ikey++] = std::strcpy(new char[std::strlen(tok)+1],tok);
    }
    for(std::size_t i=0; i<ikey; ++i)  {
      if ( ::strchr(items[i],' ') != 0 ) {
        *strchr(items[i],' ') = 0;
      }
    }
    this->m_server_bm = ::mbm_multi_install(ikey, items);
    for(std::size_t j=0; j<ikey; ++j)
      delete [] items[j];
    if ( this->m_server_bm.empty() ) {
      return this->error("Failed to initialize MBM buffers.");
    }
    info("MBM Server: done initializing.");
    return this->setConsumerRequirements();
  }
  this->info("MBM Server: done initializing.");
  return DF_SUCCESS;
}

/// Start MBM server
int MBMServer::start()   {
  this->info("MBM Server: in start.");
  this->subscribeAccidents();
  int sc = this->Component::start();
  this->context.manager.setRunNumber(100);
  this->info("MBM Server: ending start.");
  return sc;
}

/// Stop MBM server
int MBMServer::stop()   {
  this->unsubscribeAccidents();
  this->unsubscribeIncidents();
  this->undeclareMonitors();
  return Component::stop();
}

/// Finalize the MBM server
int MBMServer::finalize()  {
  /// Stop MBM servers
  for(const auto& bm : this->m_server_bm)
    ::mbmsrv_stop_dispatch(bm.second);

  /// Unlink from shared memory
  for(const auto& bm : this->m_server_bm)   {
    ::mbmsrv_unlink_memory(bm.second);
    ::mbmsrv_destroy(bm.second);
  }
  this->m_server_bm.clear();
  return this->Component::finalize();
}

/// Create proper buffer name depending on partitioning
std::string MBMServer::bufferName(const std::string& nam)  const   {
  std::string bm_name = nam;
  if ( this->m_partition_buffers ) {
    bm_name += "_";
    if ( this->m_partition_name.empty() )   {
      char part_id[16];
      ::snprintf(part_id,sizeof(part_id),"%X",this->m_partition_id);
      bm_name += part_id;
    }
    else  {
      bm_name += this->m_partition_name;
    }
  }
  return bm_name;
}

/// Apply consumer requirements to MBM buffers
int MBMServer::setConsumerRequirements()   {
  const auto& part = this->m_partition_name;
  auto        node = RTL::str_upper(RTL::nodeNameShort());
  for(const auto& requirement : this->m_consumer_requirements)  {
    auto        buf = this->bufferName(requirement.first);
    const auto& rq  = requirement.second;
    ServedBuffers::const_iterator j = this->m_server_bm.find(buf);
    if ( j == this->m_server_bm.end() )  {
      this->warning("Buffer "+buf+" does not exist. Failed to set consumer requirement [Ignored].");
      continue;
    }
    else if ( rq.size() < 2 )  {
      return this->error("Insufficient information from job options. Failed to set consumer requirement.");
    }
    else if ( ((rq.size())%2) != 0 )  {
      return this->error("Inconsistent job options. Failed to set consumer requirement.");
    }
    for(std::size_t k=0; k<rq.size(); k += 2)  {
      const std::string& tsk = rq[k];
      std::string req = rq[k+1];
      MBM::Requirement r;
      req = RTL::str_replace(req,"<part>",part);
      req = RTL::str_replace(req,"<node>",node);
      r.parse(req);
      info("Setting consumer requirement [%s] Task: %s REQ:%s", buf.c_str(), tsk.c_str(), req.c_str());
      int sc = this->setConsumerRequirement((*j).second, tsk, r);
      if ( sc != DF_SUCCESS )   {
        return error("Failed to set consumer requirement: "+req);
      }
    }
  }
  return DF_SUCCESS;
}

/// Apply single consumer reuirement to buffer
int MBMServer::setConsumerRequirement(ServerBMID srvBM, 
				      const std::string& task, 
				      const MBM::Requirement& r)  {
  int status = ::mbmsrv_require_consumer(srvBM, 
					 task.c_str(),
					 this->m_partition_id,
					 r.evtype,
					 r.trmask);
  if ( status != MBM_NORMAL )   {
    return this->error("Failed to set BM consumer requirements.");
  }
  return DF_SUCCESS;
}

namespace {
  /// Static Accident handler
  void server_accidents(void* param, ServerBMID bmid, long offset, std::size_t len, int typ, const unsigned int mask[])   {
    void* address = nullptr;
    MBMServer* srv = (MBMServer*)param;
    ::mbmsrv_event_address(bmid, offset, &address);
    srv->handleAccident(bmid, address, len, typ, mask);
  }
}

/// Subscribe to 'accident' events (client crashes etc.)
int MBMServer::subscribeAccidents()    {
  if ( !this->m_accident_buffers.empty() )    {

    /// Subscribe to accidents for the requested buffers
    for( const auto& b : this->m_accident_buffers )    {
      auto buf = this->bufferName(b);
      auto j = this->m_server_bm.find(buf);
      if ( j == this->m_server_bm.end() )  {
	this->warning("Buffer "+buf+" does not exist. Failed to attach accident handler [ignored].");
	continue;
      }
      ::mbmsrv_subscribe_accidents((*j).second, this, server_accidents);
    }

    /// Map output buffer to copy accident events
    if ( !this->m_accident_output.empty() )    {
      auto buf = this->bufferName(this->m_accident_output);
      auto j = this->m_server_bm.find(buf);
      if ( j == this->m_server_bm.end() )  {
	this->warning("Accident output buffer "+buf+" does not exist. Cannot save failed events [ignored].");
      }
      else   {
	int  com = mbmsrv_communication_type((*j).second);
	int  pid = this->m_partition_id;
	auto nam = RTL::processName();
	this->m_output_prod = std::make_unique<MBM::Producer>(buf, nam, pid, com);
	if ( this->m_output_prod->id() == MBM_INV_DESC )   {
	  this->warning("FAILED to map accident output buffer "+buf+" [ignored].");
	  this->m_output_prod.reset();
	}
	else   {
	  this->warning("Successfully connected to accident output buffer "+buf+" [ignored].");
	}
      }
    }
  }
  return DF_SUCCESS;
}

/// Unsubscribe from 'accident' events (client crashes etc.)
int MBMServer::unsubscribeAccidents()   {
  /// Unsubscribe from accidents of the requested buffers
  for( const auto& b : this->m_accident_buffers )    {
    auto buf = this->bufferName(b);
    auto j = this->m_server_bm.find(buf);
    if ( j != this->m_server_bm.end() )  {
      ::mbmsrv_unsubscribe_accidents((*j).second);
    }
  }
  /// Release producer for output buffer
  if ( this->m_output_prod )   {
    this->m_output_prod.reset();
  }
  return DF_SUCCESS;
}

/// Handle accident events
void MBMServer::handleAccident(ServerBMID bmid, const void* address, std::size_t len, int typ, const unsigned int mask[])   {
  if ( bmid )   {
    char bm_name[128];
    ::mbmsrv_buffer_name(bmid, bm_name, sizeof(bm_name));
    this->error("[%s] Client accident event: %p len: %9ld typ: %3d mask: [%08X,%08X,%08X,%08X]",
		bm_name, address, len, typ, mask[0], mask[1], mask[2], mask[3]);
    if ( this->m_output_prod )   {
      try  {
	std::lock_guard<std::mutex> lock(this->m_output_lock);
	int sc = this->m_output_prod->getSpace(len);
	if ( sc != MBM_REQ_CANCEL )    {
	  MBM::EventDesc& e = this->m_output_prod->event();
	  e.type = EVENT_TYPE_ERROR;
	  e.len  = len;
	  ::memcpy(e.mask, mask, sizeof(e.mask));
	  ::memcpy(e.data, address, len);
	  this->m_output_prod->sendEvent();
	}
      }
      catch(const std::exception& e)  {
	this->error("Error declaring accident event: %s.", e.what());
      }
      catch(...)  {
	this->error("Unknown exception declaring accident event.");
      }
    }
  }
}
