//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Tell1BankDecoder.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_BANKDECODER_H
#define ONLINE_DATAFLOW_BANKDECODER_H

// Framework include files
#include "Dataflow/DataflowComponent.h"

// C/C++ include files
#include <vector>

///  Online namespace declaration
namespace Online  {

  /// Forward declarations
  class Tell1Bank;

  /// Tell1BankDecoder component to be applied in the manager sequence
  /**
   *  Decode a frame of Tell1 banks in the LHCb online system.
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class Tell1BankDecoder : public DataflowComponent  {
  protected:
    typedef std::vector<Tell1Bank*> Banks;
    /// Property: String representation of the key to be added to the event context
    std::string  m_keyString;
    /// Key value derived from the string property
    unsigned int m_key;
  public:
    /// Initializing constructor
    Tell1BankDecoder(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~Tell1BankDecoder();
    /// Initialize the decoder component
    virtual int initialize()  override;
    /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_BANKDECODER_H
//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Tell1BankDecoder.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "Tell1Data/Tell1Decoder.h"
#include "Dataflow/EventContext.h"
#include "Dataflow/Plugins.h"
#include "DD4hep/Primitives.h"

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_Tell1BankDecoder,Tell1BankDecoder)

/// Initializing constructor
Tell1BankDecoder::Tell1BankDecoder(const std::string& nam, Context& ctxt)
: Component(nam,ctxt), m_key(0)
{
  declareProperty("Key",m_keyString="/Event/RawBanks");
}

/// Default destructor
Tell1BankDecoder::~Tell1BankDecoder()   {
}


/// Initialize the decoder component
int Tell1BankDecoder::initialize()  {
  int sc = Component::initialize();
  m_key = detail::hash32(m_keyString);
  return sc;
}

/// Data processing overload: process event
int Tell1BankDecoder::execute(const Context::EventData& event)  {
  if ( event.data )  {
    if ( event.context )  {
      Banks& banks = event.context->add<Banks>(m_key);
      int sc = Online::decodeRawBanks(event.data,event.data+event.length,banks);
      if ( sc == DF_SUCCESS )  {
	return DF_SUCCESS;
      }
    }
  }
  return DF_ERROR;
}
