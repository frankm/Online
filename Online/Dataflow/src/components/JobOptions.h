//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  JobOptions.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_JOBOPTIONS_H
#define ONLINE_DATAFLOW_JOBOPTIONS_H

// Framework include files
#include <Dataflow/DataflowComponent.h>

/// Forward declarations

///  Online namespace declaration
namespace Online  {

  /// Job options service for the dataflow
  /** @class JobOptions JobOptions.h Dataflow/JobOptions.h
   *
   *
   *  \author  Markus Frank
   *  \version 1.0
   *  \ingroup DATAFLOW
   */
  class JobOptions : public DataflowComponent, public DataflowContext::Options  {
  private:
    /// The gaudi job options catalog
    typedef std::pair<std::string,std::string> Prop;
    typedef std::vector<Prop>                  PropertiesT;
    std::map<std::string, PropertiesT>         m_catalog;

  public:
    /// Initializing constructor
    JobOptions(const std::string& name, Context& framework);

    /// Default destructor
    virtual ~JobOptions();

    /// Retrieve all clients with options
    std::vector<std::string> getClients() const;

    /// Output method to dump options
    std::ostream& fillStream( std::ostream& o ) const;

    /** Interface implementation  */
    /// Access the property of one client. Returns NULL if none
    const PropertiesT& getProperties( const std::string& client) const  override;

    /// Add a client property to the options catalog
    virtual void addProperty(const std::string& client, Prop&& p)  override;

    /// Clear all stored properties
    virtual void clear()  override;

    /// Load a job options file
    virtual int load(const std::string& options, const std::string& path="")  override;

    /// Set the component's properties
    virtual int setProperties(const std::string& client, PropertyManager& properties)  override;
  };

}      // end namespace Online

#endif //  ONLINE_DATAFLOW_JOBOPTIONS_H
