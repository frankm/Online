//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  LumiCounter.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Beat Jost
//               Markus Frank
//==========================================================================
// Include files
#include "LumiCounter.h"
#include <MBM/mepdef.h>
#include <MBM/Consumer.h>
#include <MBM/Requirement.h>
#include <Tell1Data/Tell1Decoder.h>
#include <RTL/rtl.h>

#include <ctime>
#include <cmath>
#include <cstdio>
#include <cerrno>
#include <climits>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>

using MBM::EventDesc;
using MBM::Requirement;
using namespace std;
using namespace Online;


#define AddPtr(ptr,offs) (void*)((char *)ptr +offs)
LumiCounter::LumiCounter(const string& nam, Context& ctxt)
  : TriggerBitCounter(nam, ctxt), m_RunHistory(0)
{
  declareProperty("DontCloseRuns",m_dontCloseRuns=false);
  declareProperty("SynchInterval",m_SynchInterval = 2);
  declareProperty("SynchPerEvent",m_SynchPerEvent = true);
  declareProperty("MultiInstance",m_MultiInstance=false);
  m_SynchTimer = 0;
}

// Standard Destructor
LumiCounter::~LumiCounter()
{
}

int LumiCounter::initialize()
{
  TriggerBitCounter::initialize();
  if (m_MultiInstance)
  {
    char pnam[2048];
    ::lib_rtl_get_process_name(pnam,sizeof(pnam));
    m_BackupFile = m_BackupFile+"_"+pnam;
  }
  m_RunMap.SetFile(m_BackupDirectory + m_BackupFile);
  m_RunMap.OpenBackingStore();
  if (!m_SynchPerEvent)
  {
    m_SynchTimer = new SynchTimer(this,m_SynchInterval);
  }
  declareMonitor("EventsMatched", m_EvIn,"Event Matched Requirements");

  return DF_SUCCESS;
}

int LumiCounter::start()
{
  TriggerBitCounter::start();
  for (auto i : m_RunMap)
  {
    string rnstr = std::to_string(((LumiRunItem*) i.second)->m_runno);
//    LumiRunItem *p = ((LumiRunItem*) i.second);
//    declareMonitor("LumiCount/" + rnstr, "x", (void*) p->m_count,
//        sizeof(p->m_count), "LumiEvent for run " + rnstr);
  }
  if (m_SynchTimer != 0)
  {
    m_SynchTimer->strt();
  }
  return DF_SUCCESS;
}

int LumiCounter::finalize()
{
  if (m_SynchTimer != 0)
  {
    delete m_SynchTimer;
  }
  undeclareMonitors();
  return TriggerBitCounter::finalize();
}

/// Process single event
int LumiCounter::stop()
{
//  context.monitor->update("", m_RunNumber, this);
  if (m_SynchTimer != 0)
  {
    m_SynchTimer->stop();
  }
  m_RunMap.Sync();
  return TriggerBitCounter::stop();
}

/// Process single event. Input buffer is ALWAYS in mdf bank format.
int LumiCounter::execute(const Context::EventData& e)
{
  time_t ctim = time(0);
  if ( !matchRequirements(e) )
  {
    return DF_SUCCESS;
  }
  m_EvIn++;
  LumiRunItem *ri = 0;
  Tell1Bank *ev = (Tell1Bank*) e.data;
  EventHeader *mdf = (EventHeader*) ev->data();
  unsigned int runnr = mdf->subHeader().H1->runNumber();
  auto rmapi = m_RunMap.find(runnr);
  if (rmapi == m_RunMap.end())
  {
    ri = m_RunMap.NewEntry(runnr);
    ri->m_LastCnt = ctim;
    m_RunMap[runnr] = ri;
//    string rnstr = std::to_string(runnr);
//    declareMonitor("LumiCount/" + rnstr, "x", (void*) ri->m_count,
//        sizeof(ri->m_count), "LumiEvent for run " + rnstr);
  }
  else
  {
    ri = (LumiRunItem *) rmapi->second;
  }
#if 0
  if (runnr > m_RunNumber)
  {
    if (m_RunNumber != 0)
    {
      context.monitor->update("", m_RunNumber, this);
    }
    m_RunNumber = runnr;
  }
#endif
  Tell1Bank *odin = findOdinBank(e.data, e.length);
  if (odin == 0)
  {
    ::lib_rtl_output(LIB_RTL_ERROR, "Cannot Find ODIN Bank.");
    return DF_SUCCESS;
  }
  RunInfo *od = (RunInfo*) odin->data();
#if 0
  list<int> toDelete;
  for (auto i : m_RunMap)
  {
    if (i.first != m_RunNumber)
    {
      if (!m_dontCloseRuns)
      {
        if ((ctim - ((LumiRunItem*) i.second)->m_LastCnt) > 100)
        {
          toDelete.insert(toDelete.end(), i.first);
          string rnstr = std::to_string(i.first);
          undeclareMonitor("LumiCount/" + rnstr);
        }
      }
    }
  }
  for (auto i : toDelete)
  {
    rmapi = m_RunMap.find(i);
    if (rmapi != m_RunMap.end())
      m_RunMap.erase(rmapi);
  }
#endif
  ri->m_LastCnt = ctim;
  ri->m_count[od->bxType]++;
  if (this->m_SynchPerEvent)
  {
    m_RunMap.Sync();
  }
  return DF_SUCCESS;
}

Tell1Bank *LumiCounter::findOdinBank(void *currevt, int evlen)
{
  void *evend = AddPtr(currevt, evlen);
  Tell1Bank *cbank = (Tell1Bank *) currevt; //(Tell1Bank*)AddPtr(currevt,MDFHdLen);
  while (cbank < evend)
  {
    if (cbank->magic() == Tell1Bank::MagicPattern)
    {
      if (cbank->type() == Tell1Bank::ODIN)
      {
        return cbank;
      }
      else
      {
        cbank = (Tell1Bank*) AddPtr(cbank,
            ((cbank->size() + cbank->hdrSize() + 3) & 0xfffffffc));
        continue;
      }
    }
  }
  return 0;
}
