//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  SocketSelector.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data server
//               using private socket data I/O
//
//  Author     : Markus Frank
//==========================================================================
#define TRANSFER_NS        DataTransfer
#include "NET/Transfer.h"
#define DataSelector Dataflow_SocketSelector
#include "Dataflow/DataSelector.h"
