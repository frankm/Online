//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  UpgradeMEPSelector.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_MEPSELECTOR_H
#define ONLINE_DATAFLOW_MEPSELECTOR_H

// Framework include files
#include <Dataflow/EventRunable.h>
#include <Tell1Data/Tell1Decoder.h>
#include <PCIE40Data/pcie40.h>
#include <MBM/Requirement.h>

// C/C++ include files
#include <vector>
#include <mutex>

// Forward declarations
namespace MBM { class Consumer; }

///  Online namespace declaration
namespace Online  {

  /// Buffer Manager service for the dataflow
  /**
   * @author  Markus Frank
   * @version 1.0
   */
  class UpgradeMEPSelector : public EventRunable  {
  protected:
    typedef MBM::Requirement Requirement;
    typedef MBM::Consumer    Consumer;
    /// Property: Input buffer name
    std::string m_input;
    /// Decode data buffer
    bool        m_decode     {false};
    /// Property to manipulate handling of event timeouts
    bool        m_handleTMO  {false};
    /// Property to indicate to pause on error
    bool        m_gotoPause  {false};
    /// Property to sleep for some time before pause [unit: milli-seconds]
    int         m_pauseSleep {0};
    /// Dummy property for backwards compatibility
    int         m_printFreq  {1000000};
    /// Requirement properties
    std::string m_Rqs[8];
    /// Decoded requirements
    Requirement m_Reqs[8];
    /// Number of requirements
    int         m_nreqs      {0};
    /// Monitoring quantity: Number of events requested
    int         m_reqCount   {0};

    /// Consumer to access the buffer manager
    Consumer*   m_consumer   {nullptr};
    /// Event buffer
    char*       m_buffer     {nullptr};
    /// Event buffer length
    size_t      m_bufflen    {0};
    /// Event buffer from MEP decoding
    std::unique_ptr<pcie40::event_collection_t> m_events;
    std::mutex  m_lock;

    /** Event access interface for sub-classes */
    /// Event access interface: Get next event from wherever
    virtual int getEvent(Context::EventData& event) override;
    /// Event access interface: Release event. Default implementation is empty
    virtual int freeEvent(Context::EventData& event) override;
    /// Event access interface: Shutdown event access in case of forced exit request
    virtual int shutdown() override;

  public:
    /// Initializing constructor
    UpgradeMEPSelector(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~UpgradeMEPSelector();

    /// Start the data flow component.
    virtual int start()  override;
    /// Stop the data flow component.
    virtual int stop()  override;
    /// Cancel I/O operations of the dataflow component
    virtual int cancel()  override;
  };
}      // end namespace Online

#endif //  ONLINE_DATAFLOW_MEPSELECTOR_H

/// Framework includes
#include <Dataflow/MBMClient.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/Printout.h>
#include <Dataflow/Plugins.h>
#include <MBM/Consumer.h>
#include <MBM/mepdef.h>
#include <RTL/rtl.h>

#include <PCIE40Data/pcie40decoder.h>

// C/C++ include files
#include <set>
#include <cstring>

// Instantiation of a static factory class used by clients to create instances of this service
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_UpgradeMEPSelector,UpgradeMEPSelector)

using namespace std;
using namespace Online;

/// Initializing constructor
UpgradeMEPSelector::UpgradeMEPSelector(const string& nam, Context& ctxt)
  : EventRunable(nam, ctxt), m_consumer(0), m_buffer(0), m_bufflen(0)
{
  declareProperty("REQ1",   m_Rqs[0] = "");
  declareProperty("REQ2",   m_Rqs[1] = "");
  declareProperty("REQ3",   m_Rqs[2] = "");
  declareProperty("REQ4",   m_Rqs[3] = "");
  declareProperty("REQ5",   m_Rqs[4] = "");
  declareProperty("REQ6",   m_Rqs[5] = "");
  declareProperty("REQ7",   m_Rqs[6] = "");
  declareProperty("REQ8",   m_Rqs[7] = "");
  declareProperty("Input",  m_input = "");
  declareProperty("Pause",  m_gotoPause = false);
  declareProperty("Decode", m_decode = false);
  declareProperty("PrintFreq",m_printFreq = 0.0);
  declareProperty("PauseSleep",m_pauseSleep = 0);
  declareProperty("HandleTimeout",m_handleTMO = false);
}

/// Default destructor
UpgradeMEPSelector::~UpgradeMEPSelector()   {
}

/// Get next event from wherever
int UpgradeMEPSelector::getEvent(Context::EventData& ev)   {
  const MBM::EventDesc& e = m_consumer->event();

  // Restart:
  while ( !m_events.get() || m_events->empty() )  {
    int sc = m_consumer->getEvent();
    if ( sc == MBM_NORMAL )  {
      if ( e.type == EVENT_TYPE_MISC )   {
      }
      else if ( e.type == EVENT_TYPE_MEP )   {
	pcie40::decoder_t decoder;
	pcie40::mep_header_t* mep = (pcie40::mep_header_t*)e.data;
	const pcie40::multi_fragment_t *mfp = mep->multi_fragment(0);
	uint16_t packing = mfp->header.packing;
	decoder.initialize(m_events, packing);
	decoder.decode(m_events, mep);
      }
    }
    else if ( sc == MBM_REQ_CANCEL )  {
      ev = Context::EventData();
      return DF_CANCELLED;
    }
  }
  return DF_ERROR;
}

/// Cancel I/O operations of the dataflow component
int UpgradeMEPSelector::cancel()  {
  if ( context.mbm )  {
    context.mbm->cancelBuffers();
    //lock_guard<mutex> evt_lock(m_lock);
    //m_events.clear();
  }
  return DF_SUCCESS;
}

/// Shutdown event access in case of forced exit request
int UpgradeMEPSelector::shutdown()   {
  if ( context.mbm )  {
    context.mbm->stop();
    context.mbm->finalize();
  }
  return DF_SUCCESS;
}

/// Release event
int UpgradeMEPSelector::freeEvent(Context::EventData& /* event */)   {
  if ( m_events.get() && m_events->empty() && m_consumer )  {
    int sc = m_consumer->freeEvent();
    if ( sc == MBM_NORMAL || sc == MBM_REQ_CANCEL ) 
      return DF_SUCCESS; 
    return DF_ERROR;
  }
  return DF_SUCCESS;
}

/// Initialize the event processor
int UpgradeMEPSelector::start()  {
  int sc = EventRunable::start();
  if ( sc == DF_SUCCESS )  {
    declareMonitor("RequestCount",m_reqCount=0,"Event request counter");
    if ( sc == DF_SUCCESS )  {
      for ( m_nreqs=0; m_nreqs<8; ++m_nreqs )  {
	if ( !m_Rqs[m_nreqs].empty() )   {
	  m_Reqs[m_nreqs].parse(m_Rqs[m_nreqs]);
	  continue;
	}
	break;
      }
      m_consumer = context.mbm->createConsumer(m_input, RTL::processName());
      if ( 0 == m_consumer )  {
	throwError("Failed to create MBM Consumer for buffer %s",m_input.c_str());
      }
      for(int i=0; i<m_nreqs; ++i)
	m_consumer->addRequest(m_Reqs[i]);
    }
  }
  return sc;
}

/// Initialize the event processor
int UpgradeMEPSelector::stop()  {
  if ( m_consumer )   {
    try   {
      for( int i=0; i<m_nreqs; ++i )
	m_consumer->delRequest(m_Reqs[i]);
    }
    catch(...)   { }
    delete m_consumer;
    m_consumer = nullptr;
  }
  if ( m_buffer ) {
    ::free(m_buffer);
    m_buffer = 0;
  }
  m_bufflen = 0;
  m_nreqs = 0;
  return EventRunable::stop();
}
