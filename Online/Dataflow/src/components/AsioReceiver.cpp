//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  AsioReceiver.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data receiver 
//               using boost::asio asynchronous data I/O
//
//  Author     : Markus Frank
//==========================================================================

#define TRANSFER_NS BoostAsio
#include <NET/Transfer.h>
#define DataReceiver Dataflow_AsioReceiver
#include <Dataflow/DataReceiver.h>
