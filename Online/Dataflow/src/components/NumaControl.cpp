//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  NumaControl.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_NUMACONTROL_H
#define ONLINE_DATAFLOW_NUMACONTROL_H

// Framework include files
#include <Dataflow/DataflowComponent.h>

// C/C++ include files
#include <vector>

///  Online namespace declaration
namespace Online  {

  /// Forward declarations

  /// NumaControl component to be applied in the manager sequence
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class NumaControl : public DataflowComponent  {
  protected:
    /// Property: Option to steer the printout.
    std::string      printOption;
    /// Property: String representation of the key to be added to the event context
    std::string      when;
    std::vector<int> cpuSlots {};
    std::vector<int> cpuMask  {};
    bool             bindCPU    = true;
    bool             bindMemory = true;

    /// Apply NUMA configuration settings according to job options
    void apply_numa_config();
    /// Short summary of process status
    void printStatus()  const;
    /// Print the current NUMA environment the process is executing on
    void printSettings() const;

  public:
    /// Initializing constructor
    NumaControl(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~NumaControl();
    /// Initialize the numa controls component
    virtual int initialize()  override;
    /// Start the numa controls component
    virtual int start()  override;
    /// Stop the numa controls component
    virtual int stop()  override;
    /// Finalize the numa controls component
    virtual int finalize()  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_NUMACONTROL_H
//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  NumaControl.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/Plugins.h>
#include <DD4hep/Primitives.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

#include <cerrno>
#include <set>

using namespace std;
using namespace Online;

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_NumaControl,NumaControl)

/// Initializing constructor
NumaControl::NumaControl(const string& nam, Context& ctxt)
: Component(nam,ctxt)
{
  declareProperty("When",       when="initialize");
  declareProperty("BindCPU",    bindCPU);
  declareProperty("BindMemory", bindMemory);
  declareProperty("CPUSlots",   cpuSlots);
  declareProperty("CPUMask",    cpuMask);
  declareProperty("Print",      printOption);
}

/// Default destructor
NumaControl::~NumaControl()   {
}

/// Initialize the numa controls component
int NumaControl::initialize()  {
  int sc = Component::initialize();
  if ( when.find("initialize") != string::npos )
    apply_numa_config();
  return sc;
}

/// Start the numa controls component
int NumaControl::start()  {
  int sc = Component::start();
  if ( when.find("start") != string::npos )
    apply_numa_config();
  return sc;
}

/// Stop the numa controls component
int NumaControl::stop()  {
  int sc = Component::stop();
  if ( when.find("stop") != string::npos )
    apply_numa_config();
  return sc;
}

/// Finalize the numa controls component
int NumaControl::finalize()  {
  int sc = Component::finalize();
  if ( when.find("finalize") != string::npos )
    apply_numa_config();
  return sc;
}


// We only enable the following section if numa is found.
#ifdef HAVE_NUMA
// System headers
#include <numa.h>

/// Print bitmask content
static string c_mask(bitmask* m)  {
  string msk;
  for(unsigned int i=0; i < m->size; ++i)   {
    if ( i > 64 ) continue;
    msk += char(::numa_bitmask_isbitset(m,i) ? '1' : '0');
  }
  return msk;
}

/// Short summary of process status
void NumaControl::printStatus()  const  {
  pid_t pid = ::lib_rtl_pid();
  info("| numa_preferred:              %16d",numa_preferred());
  info("| numa_get_membind:            %s",c_mask(numa_get_membind()).c_str());
  bitmask* m = numa_allocate_cpumask();
  ::numa_sched_getaffinity(pid, m);
  info("| numa_sched_getaffinity:      %s",c_mask(m).c_str());
  ::numa_free_cpumask(m);
  info("+--------------------------------------- PID:%d",pid);
}

/// Print the current NUMA environment the process is executing on
void NumaControl::printSettings() const  {
  if ( -1 == numa_available() )   {
    info("Numa is not available on this node. No settings applied.");
    return;
  }
  info("+-------------------- Numa Node info ---------------------------");
  info("| numa is available on this node. Properties are as follows:");
  info("| numa_max_possible_node:      %16d",::numa_max_possible_node());
  info("| numa_num_possible_nodes:     %16d",::numa_num_possible_nodes());
  info("| numa_max_node:               %16d",::numa_max_node());
  info("| numa_num_configured_nodes:   %16d",::numa_num_configured_nodes());
  info("| numa_num_configured_cpus:    %16d",::numa_num_configured_cpus());
  info("| numa_num_task_cpus:          %16d",::numa_num_task_cpus());
  info("| numa_num_task_nodes:         %16d",::numa_num_task_nodes());
  info("| numa_get_mems_allowed:       %s",c_mask(::numa_get_mems_allowed()).c_str());
  info("| numa_all_nodes_ptr:          %s",c_mask(::numa_all_nodes_ptr).c_str());
  info("| numa_no_nodes_ptr:           %s",c_mask(::numa_no_nodes_ptr).c_str());
  info("| numa_all_cpus_ptr:           %s",c_mask(::numa_all_cpus_ptr).c_str());
  info("| numa_get_run_node_mask:      %s",c_mask(::numa_get_run_node_mask()).c_str());
  info("| numa_get_interleave_mask:    %s",c_mask(::numa_get_interleave_mask()).c_str());
  bitmask* cm = ::numa_allocate_cpumask();
  for(size_t i=0, n=::numa_max_node(); i<=n && i; ++i)  {
    long free_mem=0, total_mem = ::numa_node_size(i,&free_mem);
    ::numa_bitmask_clearall(cm);
    ::numa_node_to_cpus(i,cm);
    info("| numa_node_size(%2ld):          %16ld   Free:%ld",i,total_mem,free_mem);
    info("| numa_node_to_cpus(%2ld):       %s",i,c_mask(cm).c_str());
  }
  ::numa_bitmask_free(cm);
  info("+-------------------- Numa Task info ---------------------------");
  printStatus();
}

/// Apply NUMA configuration settings according to job options
void NumaControl::apply_numa_config()   {
  if ( -1 == numa_available() )   {
    warning("Numa is not available on this node. No settings applied.");
    return;
  }
  if ( cpuSlots.empty() && cpuMask.empty() )    {
    return;
  }
  string opt = RTL::str_lower(printOption);

  /// Now optional printout
  if ( opt.find("all") != string::npos )  printSettings();
  if ( opt.find("initial") != string::npos )  printStatus();

  /// Setup the bitmasks for the nodes to run on:
  int  task_nodes = ::numa_num_task_nodes();
  bool bad_config = false;
  for(auto i : cpuSlots )   {
    if ( i >= task_nodes )   {
      warning("NUMA did not detect a CPU in slot %d. Check your job options!",i);
      bad_config = true;
    }
  }
  if ( bad_config )   {
    warning("Abort NUMA binding due to bad configuration.");
    return;
  }
  bitmask* nm     = ::numa_allocate_nodemask();
  bitmask* nmfree = ::numa_allocate_nodemask();
  bitmask* cm     = ::numa_allocate_cpumask();
  ::numa_bitmask_clearall(nm);
  ::numa_bitmask_clearall(cm);
  ::numa_bitmask_clearall(nmfree);
  if ( !cpuMask.empty() )    {
    std::set<int> slots;  
    for(size_t i=0; i < cpuMask.size(); ++i )   {
      if ( cpuMask[i] != 0 )  {
	int node = ::numa_node_of_cpu(i);
	::numa_bitmask_setbit(cm, i);
	::numa_bitmask_setbit(nm, node);
	slots.insert(node);
      }
    }
    cpuSlots.clear();
    for(int i : slots)    {
      cpuSlots.push_back(i);
    }
  }
  else if ( !cpuSlots.empty() )   {
    ::numa_bitmask_clearall(cm);
    for(int i : cpuSlots )   {
      if ( 0 == ::numa_node_to_cpus(i,cm) )   {
	::numa_bitmask_setbit(nm, i);
      }
    }
  }

  /// Setup the bitmask with all the nodes to free when migrating pages:
  ::numa_bitmask_clearall(nmfree);
  for(size_t b=0, n=::numa_max_node(); b<=n && b<nm->size; ++b)  {
    if ( !::numa_bitmask_isbitset(nm,b) )  {
      ::numa_bitmask_setbit(nmfree,b);
    }
  }

  int ret;
  /// Propagate the CPU bindings unless switched off
  if ( bindCPU )  {
    info("| Set numa node     mask to:   %s",c_mask(nm).c_str());
    info("| Set numa affinity mask to:   %s",c_mask(cm).c_str());
    ret = ::numa_run_on_node_mask(nm);
    if ( ret != 0 )   {
      warning("FAIL: NUMA failed bind task to CPU slots %s: [%s]",
	      c_mask(nm).c_str(), RTL::errorString().c_str());
    }
    ret = ::numa_sched_setaffinity(::lib_rtl_pid(),cm);
    if ( ret != 0 )   {
      warning("FAIL: NUMA failed set task affinity to mask %s: [%s]",
	      c_mask(cm).c_str(), RTL::errorString().c_str());
    }
  }
  /// Apply the memory binding unless switched off
  if ( bindMemory )   {
    ::numa_set_localalloc();
    ::numa_set_membind(nm);
    ret = ::numa_migrate_pages(::lib_rtl_pid(), nmfree, nm);
    if ( ret != 0 )   {
      warning("FAIL: NUMA failed to migrate pages: %s",RTL::errorString().c_str());
    }
  }
  ::numa_bitmask_free(cm);
  ::numa_bitmask_free(nm);
  /// Now optional printout
  if ( opt.find("result") != string::npos )  printStatus();
}
#else
namespace  {
  void prt_warn(const NumaControl*  ctrl)   {
    static bool first = true;
    if ( first )   {
      first = false;
      ctrl->warning("*** NUMA was not found during the build. NUMA is NOT enabled!");
    }
  }
}
/// Apply NUMA configuration settings according to job options
void NumaControl::apply_numa_config()   {  prt_warn(this);   }
/// Short summary of process status
void NumaControl::printStatus()  const  {  prt_warn(this);   }
#endif
