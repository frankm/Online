//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  BurstWriter.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_BURSTWRITER_H
#define ONLINE_DATAFLOW_BURSTWRITER_H

// Framework include files
#include <Dataflow/DataflowComponent.h>

/// C/C++ include files
#include <random>

// Forward declarations

///  Online namespace declaration
namespace Online  {

  // Forward declarations

  /// BurstWriter to group coherent action sequences
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class BurstWriter : public DataflowComponent  {
  protected:
    std::string    m_directory;
    std::string    m_prefix;
    std::default_random_engine m_generator;
    std::uniform_real_distribution<double> m_distribution;
    long           m_maxFileSize = 0;
    long           m_bufferSize = 0;
    long           m_fileBytes = 0;
    long           m_bufferLen = 0;
    unsigned char* m_buffer = 0;
    unsigned char* m_bufferPtr = 0;
    double         m_fraction = 1.0;
    int            m_file = 0;
    int            m_maxfileSizeMB = 0;
    unsigned int   m_curr_run_number = 0;
    bool           m_dailyFiles = false;
  public:
    /// Write multi event buffer to file. Eventually open a new file....
    int writeBuffer();
    /// Flush existing file buffer to disk and close file
    int flush_file();

  public:
    /// Initializing constructor
    BurstWriter(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~BurstWriter();
    /// Initialize the data flow component. Default implementation is empty.
    virtual int initialize()  override;
    /// Start the data flow component. Default implementation is empty.
    virtual int start()  override;
    /// Stop the data flow component. Default implementation is empty.
    virtual int stop()  override;
    /// Finalize the data flow component. Default implementation is empty.
    virtual int finalize()  override;
    /// Cancel the data flow component. 
    virtual int cancel()  override;
    /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_BURSTWRITER_H
