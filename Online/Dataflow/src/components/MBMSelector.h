//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MBMSelector.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_MBMSELECTOR_H
#define ONLINE_DATAFLOW_MBMSELECTOR_H

// Framework include files
#include <Dataflow/EventRunable.h>
#include <MBM/Requirement.h>

// C/C++ include files

// Forward declarations
namespace MBM { class Consumer; }

///  Online namespace declaration
namespace Online  {

  /// Buffer Manager service for the dataflow
  /**
   * @author  Markus Frank
   * @version 1.0
   */
  class MBMSelector : public EventRunable  {
  protected:
    typedef MBM::Requirement Requirement;
    typedef MBM::Consumer    Consumer;
    /// Property: Input buffer name
    std::string m_input;
    /// Decode data buffer
    bool        m_decode;
    /// Property to manipulate handling of event timeouts
    bool        m_handleTMO;
    /// Property to indicate to pause on error
    bool        m_gotoPause;
    /// Property to sleep for some time before pause [unit: milli-seconds]
    int         m_pauseSleep;
    /// Dummy property for backwards compatibility
    int         m_printFreq;
    /// Requirement properties
    std::string m_Rqs[8];
    /// Decoded requirements
    Requirement m_Reqs[8];
    /// Number of requirements
    int         m_nreqs;

    /// Consumer to access the buffer manager
    Consumer*   m_consumer;

    /** Event access interface for sub-classes */
    /// Event access interface: Get next event from wherever
    virtual int getEvent(Context::EventData& event)  override;
    /// Event access interface: Release event. Default implementation is empty
    virtual int freeEvent(Context::EventData& event)  override;
    /// Event access interface: Shutdown event access in case of forced exit request
    virtual int shutdown()  override;
    /// Event access interface: Cancel I/O operations of the dataflow component
    virtual int cancelEvents()  override;

  public:
    /// Initializing constructor
    MBMSelector(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~MBMSelector();

    /// Start the data flow component.
    virtual int start()  override;
    /// Stop the data flow component.
    virtual int stop()  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_MBMSELECTOR_H
