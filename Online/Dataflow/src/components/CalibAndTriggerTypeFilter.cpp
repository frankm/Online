//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  RoutingBitsPrescaler.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include "Dataflow/DataflowComponent.h"

// C/C++ include files
#include <vector>

/// Online namespace declaration
namespace Online  {

  class CalibAndTriggerTypeFilter : public DataflowComponent {
  public: 
    /// Standard constructor
    CalibAndTriggerTypeFilter(const std::string& name, Context& ctxt); 
    virtual ~CalibAndTriggerTypeFilter() {} ///< Destructor
    virtual int execute(const Context::EventData& event)  override;    ///< Algorithm execution
  protected:
    std::vector< int > m_triggerTypesToPass;
    std::vector< int > m_calibTypesToPass;
  };
}

#include "Tell1Data/Tell1Decoder.h"

using namespace std;
using namespace Online;

CalibAndTriggerTypeFilter::CalibAndTriggerTypeFilter(const string& nam,	Context& ctxt)
  : DataflowComponent(nam, ctxt), m_triggerTypesToPass(), m_calibTypesToPass()
{
  declareProperty("TriggerTypesToPass", m_triggerTypesToPass = { 4 , 7 } );
  declareProperty("CalibTypesToPass" ,  m_calibTypesToPass = { 2 , 3 } ) ;
}

int CalibAndTriggerTypeFilter::execute(const Context::EventData& event) {
  processingFlag &= ~DF_PASSED;
  if ( event.data )  {
    // We do not pass unless ODIN information fits!
    processingFlag &= ~DF_PASSED;

    for(const char* ptr=event.data, *end=event.data+event.length; ptr<end; )  {
      Tell1Bank *b = (Tell1Bank*)ptr;
      ptr += b->totalSize();
      if ( b->type() == Tell1Bank::ODIN )  {
	RunInfo *odin = b->begin<RunInfo>();
	// Check trigger type first
	if ( find( m_triggerTypesToPass.begin(), 
		   m_triggerTypesToPass.end(), 
		   odin->triggerType) != m_triggerTypesToPass.end() )  {
	  processingFlag |= DF_PASSED;
	  if ( ( odin->triggerType==7 ) && //calibration type check 
	       ( find( m_calibTypesToPass.begin(), 
		       m_calibTypesToPass.end(), 
		       odin->readoutType) == m_calibTypesToPass.end() ) )
	    processingFlag &= ~DF_PASSED;
	}
	else {
	  processingFlag &= ~DF_PASSED;
	}
      }
    }
  }
  return DF_SUCCESS;
}

#include "Dataflow/Plugins.h"
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_CalibAndTriggerTypeFilter,CalibAndTriggerTypeFilter)

