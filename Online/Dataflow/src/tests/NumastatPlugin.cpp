//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  NumastatPlugin.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_NUMASTATPLUGIN_H
#define ONLINE_DATAFLOW_NUMASTATPLUGIN_H

// Framework include files
#include "Dataflow/DataflowComponent.h"

///  Online namespace declaration
namespace Online  {

  /// Forward declarations
  class Bank;

  /// NumastatPlugin component to be applied in the manager sequence
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class NumastatPlugin : public DataflowComponent  {
  protected:
    /// Property: Option to steer the exit.
    std::string      when;
    /// Exit the application
    void call_stat();

  public:
    /// Initializing constructor
    NumastatPlugin(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~NumastatPlugin();
    /// Initialize the numa controls component
    virtual int initialize()  override;
    /// Start the numa controls component
    virtual int start()  override;
    /// Stop the numa controls component
    virtual int stop()  override;
    /// Finalize the numa controls component
    virtual int finalize()  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_NUMASTATPLUGIN_H
//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  NumastatPlugin.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "Dataflow/Plugins.h"
#include "DD4hep/Primitives.h"
#include "RTL/rtl.h"

using namespace std;
using namespace Online;

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_NumastatPlugin,NumastatPlugin)

/// Initializing constructor
NumastatPlugin::NumastatPlugin(const string& nam, Context& ctxt)
: Component(nam,ctxt)
{
  declareProperty("When",  when="start");
}

/// Default destructor
NumastatPlugin::~NumastatPlugin()   {
}

/// Apply NUMA configuration settings according to job options
void NumastatPlugin::call_stat()   {
  char text[64];
  ::snprintf(text,sizeof(text),"numastat -p %d",lib_rtl_pid());
  ::system(text);
}

/// Initialize the numa controls component
int NumastatPlugin::initialize()  {
  int sc = Component::initialize();
  if ( when.find("initialize") != string::npos )
    call_stat();
  return sc;
}

/// Start the numa controls component
int NumastatPlugin::start()  {
  int sc = Component::start();
  if ( when.find("start") != string::npos )
    call_stat();
  return sc;
}

/// Stop the numa controls component
int NumastatPlugin::stop()  {
  int sc = Component::stop();
  if ( when.find("stop") != string::npos )
    call_stat();
  return sc;
}

/// Finalize the numa controls component
int NumastatPlugin::finalize()  {
  int sc = Component::finalize();
  if ( when.find("finalize") != string::npos )
    call_stat();
  return sc;
}
