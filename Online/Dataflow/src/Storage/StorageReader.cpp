//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  StorageReader.h
//--------------------------------------------------------------------------
//
//  Package    : GaudiOnline
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_STORAGEREADER_H
#define ONLINE_DATAFLOW_STORAGEREADER_H

// Framework includes
#include <Dataflow/DiskReader.h>

// C/C++ include files
#include <list>
#include <mutex>
#include <thread>
#include <atomic>

///   Online namespace declaration
namespace Online  {

  /// Basic HLT2 file reader for the deferred triggering
  /** Class definition of StorageReader.
   *
   * This is the online extension of the runnable of the application manager.
   * The runable handles the actual run() implementation of the
   * ApplicationMgr object.
   *
   * @author Markus Frank
   *
   * @version 1.5
   */
  class StorageReader : public DiskReader  {
  public:
  protected:
    friend class MBMAllocator;

    /// Data buffer structure
    struct Buffer   {
      /// Pointer to the start of the event buffer
      std::vector<uint8_t> data;
      /// File name for debugging
      std::string          name;
      /// Associated run number
      int32_t runNumber    {0};
      /// Default constructor
      Buffer() = default;
      /// Move constructor
      Buffer(Buffer&& copy) = default;
      /// Inhibit copy constructor
      Buffer(const Buffer& copy) = delete;
      /// Inhibit copy assignment
      Buffer& operator=(Buffer&& copy) = default;
      /// Inhibit move assignment
      Buffer& operator=(const Buffer& copy) = delete;
    };

    /// Property: Server uri:   <host-name>:<port>
    std::string               m_server;
    /// Property: Preferred input data type
    std::string               m_dataType;
    /// Property: Minimal file size in MBytes
    std::size_t               m_minFileSizeMB;
    /// Property: Number of parallel event buffers
    std::size_t               m_num_buffers     { 0 };
    /// Property: Number of parallel reader threads
    std::size_t               m_num_threads     { 0 };
    /// Property: Debug FDB client
    int                       m_debugClient;
    /// Property: Poll timeout to detect transfer buffers [microseconds]
    int                       m_poll_tmo        { 200 };
    /// Property: Have threaded file queues
    int                       m_fdb_version     { 0 };
    
    /// Flag to enable buffer loading in asynchronous mode
    int                       m_loadEnabled     { true };
    /// Counter the number of reading actions
    std::atomic<int>          m_is_reading      { 0 };

    /// Buffer with references of read handling threads
    std::vector<std::unique_ptr<std::thread> >  m_threads;
    /// Mutex to lock the event buffer when filling/saving
    std::mutex                m_bufferLock      {   };

    /// Free buffers to be used for reading fresh data
    std::list<Buffer>         m_free            {   };
    /// Filled buffers to be worked down and fed to MBM
    std::list<Buffer>         m_todo            {   };

    /// Mutex to lock run lists
    std::mutex                m_runlistLock     {   };
    /// List of exhausted/bad runs when reading from HLT1 storage
    std::set<std::string>     m_bad_runs;

  protected:
    /// Runable implementation : Run the class implementation
    virtual int    i_run()  override;
    /// Patch the string array with allowed runs
    virtual void checkAllowedRuns()  override   {   }

    int open_file_posix_root(const std::string& file_name, Buffer& buffer);
    int open_file_posix_raw(const std::string& file_name, Buffer& buffer);

    /// Open file from dedicated HLT1 storage with network transfer
    int open_file_net(Buffer& buffer);
    /// Default file opening using posix
    int open_file_posix(Buffer& buffer);
    /// Callback to open new storage file
    int i_open_file(Buffer& buffer);
    /// Callback to open new storage file
    int open_file(Buffer& buffer);
    /// Asynchronous load data from file to free buffers
    void async_load_buffers();
  public:
    /// Standard algorithm constructor
    StorageReader(const std::string& nam, Context& ctxt);
    /// Default destructor
    virtual ~StorageReader() = default;
    /// DiskReader implementation: initialize the service
    virtual int initialize()  override;
    /// DiskReader implementation: start the service
    virtual int start()  override;
    /// DiskReader implementation: stop the service
    virtual int stop()  override;
    /// DiskReader implementation: finalize the service
    virtual int finalize()  override;
    /// Cancel I/O operations of the dataflow component
    virtual int cancel()  override;
    /// Stop gracefully execution of the dataflow component
    virtual int pause()  override;
  };
}      // End namespace Online
#endif // ONLINE_DATAFLOW_STORAGEREADER_H

//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  StorageReader.h
//--------------------------------------------------------------------------
//
//  Package    : GaudiOnline
//
//  Author     : Markus Frank
//==========================================================================

// Framework includes
#include <Dataflow/Plugins.h>
#include <Dataflow/MBMClient.h>
#include <Dataflow/DataflowTask.h>
#include <Storage/fdb_client.h>
#include <RTL/strdef.h>

/// ROOT include files
#include <TSystem.h>
#include <TFile.h>
#include <TROOT.h>
#include <TUri.h>

/// C/C++ include files
#include <ctime>

namespace {
  static constexpr std::size_t MBYTE = (1024e0*1024e0);
}

using namespace Online;

// Instantiation of a static factory class used by clients to create instances of this service
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_StorageReader,StorageReader)

/// Initializing constructor
StorageReader::StorageReader(const std::string& nam, Context& ctxt)
  : DiskReader(nam, ctxt)
{
  this->declareProperty("Server",          this->m_server);
  this->declareProperty("FDBVersion",      this->m_fdb_version   = 0);
  this->declareProperty("DebugClient",     this->m_debugClient   = 0);
  this->declareProperty("DataType",        this->m_dataType      = "network");
  this->declareProperty("MinFileSizeMB",   this->m_minFileSizeMB = 5);
  this->declareProperty("NumBuffers",      this->m_num_buffers   = 0);
  this->declareProperty("NumThreads",      this->m_num_threads   = 0);
  ROOT::EnableThreadSafety();
}

/// DiskReader implementation: initialize the service
int StorageReader::initialize()   {
  if ( this->DiskReader::initialize() != DF_SUCCESS )
    return this->error("Failed to initialize DiskReader base class.");  
  if ( this->m_num_buffers > 0 )     {
    for(std::size_t i = 0; i < this->m_num_buffers; ++i)
      this->m_free.emplace_back(Buffer());
  }
  this->m_bad_runs.clear();
  return DF_SUCCESS;
}

/// DiskReader implementation: finalize the service
int StorageReader::finalize()     {
  if ( this->DiskReader::finalize() != DF_SUCCESS )
    return this->error("Failed to finalize DiskReader base class.");  
  this->m_free.clear();
  return DF_SUCCESS;
}

/// DiskReader implementation: start the service
int StorageReader::start()   {
  if ( this->DiskReader::start() != DF_SUCCESS )
    return this->error("Failed to start DiskReader base class.");
  this->m_todo.clear();
  this->m_is_reading  = 0;
  this->m_eventsIN    = 0;
  this->m_loadEnabled = true;
  this->m_receiveEvts = true;
  this->m_goto_paused = false;
  this->m_bad_runs.clear();
  if ( this->m_num_buffers > 0 )     {
    for(std::size_t i = 0; i < this->m_num_threads; ++i)
      this->m_threads.emplace_back(std::make_unique<std::thread>([this]() { this->async_load_buffers(); }));
  }
  return DF_SUCCESS;
}

/// DiskReader implementation: stop the service
int StorageReader::stop()  {
  this->m_loadEnabled = false;
  this->m_receiveEvts = false;
  while ( !this->m_todo.empty() )   {
  }
  for(auto& t : this->m_threads)   {
    t->join();
    t.reset();
  }
  this->m_threads.clear();
  return DiskReader::stop();
}

/// Stop gracefully execution of the dataflow component
int StorageReader::pause()  {
  this->m_loadEnabled = false;
  while ( this->m_free.size() != this->m_num_buffers )  {
    ::lib_rtl_sleep(100);
  }
  return this->DiskReader::pause();
}

/// Cancel I/O operations of the dataflow component
int StorageReader::cancel()  {
  this->m_loadEnabled = false;
  return this->DiskReader::cancel();
}

int StorageReader::open_file_net(Buffer& buffer)  {
  using namespace storage;
  uri_t url(this->m_server);
  fdb_client client(this->m_fdb_version);

  //++m_num_file_open;
  client.fdbclient     = client::create<client::sync>(url.host, url.port, 10000, this->m_debugClient);
  client.create_client = std::function(client::create<client::sync>);
  for(const auto& run : this->m_allowedRuns )   {
    bool is_good_run = false;   {
      std::lock_guard<std::mutex> lock(this->m_runlistLock);
      is_good_run = this->m_bad_runs.find(run) == this->m_bad_runs.end();
    }
    if ( is_good_run )    {
      std::time_t date   = 0;
      std::size_t length = 0;
      std::string loc    = run + '/'; /// interprete_file_name(m_filePrefix, run);
      info("Checking for files matching pattern: '%s'", loc.c_str());
      auto reply = client.next_object_delete(loc, date, length);
      if ( reply.status == reply.ok )   {
	if ( length < this->m_minFileSizeMB*MBYTE )  {
	  info("Drop file: %s  %ld MB [too-small]", loc.c_str(), length/MBYTE);
	}
	buffer.name = loc;
	buffer.data = std::move(reply.content);
	buffer.runNumber  = std::strtol(run.c_str(), 0, 10);
	info("Loaded file: %s [%ld MB]", loc.c_str(), length/MBYTE);
	return DF_SUCCESS;
      }
      else if ( reply.status == reply.gone )   {
	std::lock_guard<std::mutex> lock(this->m_runlistLock);
	this->m_bad_runs.insert(run);
      }
#if 0
      else if ( reply.status == reply.not_found )   {
	std::lock_guard<std::mutex> lock(this->m_runlistLock);
	this->m_bad_runs.insert(run);
      }
#endif
    }
  }
  buffer.runNumber = -1;
  return DF_ERROR;
}


int StorageReader::open_file_posix_root(const std::string& loc, Buffer& buffer)   {
  TUrl    url(loc.c_str());
  TString prot = url.GetProtocol();
  /// If there is a data protocol present, we invoke
  /// ROOT to open and read the file as a whole.
  TString spec;
  TString opts = "filetype=raw";
  TString temp = url.GetOptions();
  if ( temp.Length() > 0 ) {
    opts += "&";
    opts += url.GetOptions();
  }
  url.SetOptions(opts);
  if ( 0 == prot.Length() || prot == "file" || prot == "http" ) {
    spec = loc.c_str();
    spec += "?filetype=raw";
  }
  else   {
    spec = url.GetUrl();
  }
  std::unique_ptr<TFile> file(TFile::Open(spec, "READ"));
  if ( file.get() && !file->IsZombie() )   {
    Long64_t max_chunk = 512*MBYTE;
    Long64_t file_length = file->GetSize();
    Long64_t offset = 0;
    buffer.data.resize(file_length);
    while(offset < file_length)   {
      Long64_t chunk = (file_length-offset >= max_chunk) ? max_chunk : file_length-offset;
      if ( file->ReadBuffer((char*)&buffer.data.at(offset), chunk) == 0 )  {
	offset += chunk;
	if ( offset >= file_length )   {
	  warning("Loaded file: %s [%ld MB]", file->GetName(), buffer.data.size()/MBYTE);
	  return DF_SUCCESS;
	}
	continue;
      }
      break;
    }
    return error("Failed to read data from TFile: %s", file->GetName());
  }
  return error("Failed to open TFile: %s  [Protocol error]", loc.c_str());
}

int StorageReader::open_file_posix_raw(const std::string& loc, Buffer& buffer)   {
  RawFile input(loc);
  if ( input.open() )    {
    std::size_t file_length = input.data_size();
    buffer.data.resize(file_length);
    std::size_t read_length = input.read(&buffer.data.at(0), file_length);
    if ( read_length == file_length )   {
      ++this->m_filesClosed;
      input.close();
      return DF_SUCCESS;
    }
    return this->error("Failed to read data from file: %s", loc.c_str());
  }
  return this->error("Failed to open file: %s  [Protocol error]", loc.c_str());
}

int StorageReader::open_file_posix(Buffer& buffer)  {
  std::unique_ptr<storage::client> client;
  storage::uri_t  url(this->m_server);
  std::string     location;

  buffer.runNumber = -1;
  for(auto i = std::begin(this->m_allowedRuns); i != std::end(this->m_allowedRuns); ++i )   {
    const auto& run = *i;
    storage::client::reqheaders_t hdrs;
    std::string loc, file_name = interprete_file_name(this->m_filePrefix, run);
    this->info("Checking for files matching pattern: '%s'", file_name.c_str());
    loc = "/next?prefix="+file_name;
    hdrs.emplace_back(http::constants::location, file_name);

    client = storage::client::create<storage::client::sync>(url.host, url.port, 10000, m_debugClient);
    storage::reply_t reply = client->request(http::constants::del, loc, hdrs);
    if(  reply.status == reply.ok || reply.status == reply.temp_redirect )  {
      for ( const auto& h : reply.headers )   {
	if ( h.name == http::constants::location )   {
	  loc = "file://"+h.value;
	  int status = (loc.find("://") == std::string::npos)
	    ? this->open_file_posix_raw(loc, buffer)
	    : this->open_file_posix_root(loc, buffer);
	  if ( status == DF_SUCCESS )    {
	    this->warning("Loaded file: %s [%ld MB]", loc.c_str(), buffer.data.size()/MBYTE);
	    buffer.name = loc;
	    if ( run == "*" )   {
	      size_t idx = loc.rfind("/Run_")+1;
	      const char* ptr = loc.c_str() + ((idx != std::string::npos) ? idx : 0);
	      if ( 1 != std::sscanf(ptr, "%07d", &buffer.runNumber) )  {
		if ( 1 != std::sscanf(ptr, "%08d", &buffer.runNumber) )  {
		  std::sscanf(ptr, "%010d", &buffer.runNumber);
		}
	      }
	    }
	    else   {
	      buffer.runNumber = ::strtol(run.c_str(), 0, 10);
	    }
	    return DF_SUCCESS;
	  }
	}
      }
    }
    else if ( reply.status == storage::reply_t::not_found )   {
      auto err = storage::reply_t::stock_status(reply.status);
      this->warning("No more files for run: %s [%s]", run.c_str(),
		    err.substr(0,err.length()-2).c_str());
      this->m_allowedRuns.erase(i);
      i = begin(this->m_allowedRuns);
    }
    else  {
      auto err = storage::reply_t::stock_status(reply.status);
      this->warning("Unknown fsDB error: %s [%d] run: %s", 
		    err.substr(0,err.length()-2).c_str(), 
		    int(reply.status), run.c_str());
    }
    client.reset();
    ::lib_rtl_sleep(100);
  }
  /// Error no location header!
  return this->error("Failed to open new data file.");
}

int StorageReader::i_open_file(Buffer& buffer)   {
  int ret = DF_ERROR;
  int partid = this->context.mbm->partitionID();
  std::string typ = RTL::str_lower(this->m_dataType);

  if ( this->m_requireConsumers )   {
    int cons_status = this->waitForConsumers(m_buffer);
    /// Check if there are consumers waiting before we request a file to be processed
    if ( cons_status != DF_SUCCESS )  {
      this->error("OPEN: No consumers for buffer: %s partition %s ID:%04X present. Do not request event data!",
		  this->m_buffer.c_str(), this->context.mbm->partitionName().c_str(), partid);
      ++this->m_filesOpenERROR;
      return DF_ERROR;
    }
  }
  if ( typ == "network" )   {
    ++this->m_filesOpened;
    ret = this->open_file_net(buffer);
    ++this->m_filesClosed;
    (ret == DF_SUCCESS) ? ++this->m_filesOpenOK : ++this->m_filesOpenERROR;
    return ret;
  }
  else if ( typ == "nfs" || typ == "posix" )   {
    ++this->m_filesOpened;
    ret = this->open_file_posix(buffer);
    ++this->m_filesClosed;
    (ret == DF_SUCCESS) ? ++this->m_filesOpenOK : ++this->m_filesOpenERROR;
    return ret;
  }
  return error("Failed to open next input file with type: %s.", typ.c_str());
}

void StorageReader::async_load_buffers()    {
  int partid = this->context.mbm->partitionID();
  while( this->m_loadEnabled )   {
    bool got_buffer = false;
    Buffer buffer;

    if ( this->m_requireConsumers )   {
      int cons_wait = this->waitForConsumers(m_buffer);
      if ( cons_wait != DF_SUCCESS )  {
	this->error("LOAD: No consumers for buffer: %s partition %s ID:%04X present. Do not request event data!",
		    this->m_buffer.c_str(), this->context.mbm->partitionName().c_str(), partid);
	if ( !this->m_receiveEvts ) this->m_loadEnabled = false;
	continue;
      }
    }
    /// Consumers are present....start loading if buffers are present
    {
      std::lock_guard<std::mutex> lock(this->m_bufferLock);
      if ( !this->m_free.empty() )    {
	buffer = std::move(this->m_free.front());
	this->m_free.pop_front();
	got_buffer = true;
      }
    }

    if ( !got_buffer )  {
      if ( !this->m_receiveEvts || this->m_goto_paused || !this->m_loadEnabled )
	return;
      ::lib_rtl_sleep(m_poll_tmo);
      continue;
    }
    {    // If we got a cancel in between: Stop reading
      std::lock_guard<std::mutex> lock(this->m_bufferLock);
      if ( !this->m_receiveEvts || this->m_goto_paused || !this->m_loadEnabled )  {
	this->m_free.emplace_back(std::move(buffer));      
	return;
      }
      ++this->m_is_reading ;
    }
    if ( this->i_open_file(buffer) == DF_SUCCESS )   {
      std::lock_guard<std::mutex> lock(this->m_bufferLock);
      this->m_todo.emplace_back(std::move(buffer));
    }
    else   {
      this->m_goto_paused = true;
      return;
    }
    --this->m_is_reading ;
    ::lib_rtl_sleep(m_poll_tmo);
  }
  // If PAUSE was requested, we do not drop buffers 
  // (ONLY on stop: m_goto_paused = false && m_receiveEvts == false)
  if ( !this->m_goto_paused && !this->m_receiveEvts )   {
    std::lock_guard<std::mutex> lock(this->m_bufferLock);
    while( !this->m_todo.empty() )   {
      auto buffer = std::move(this->m_todo.back());
      this->warning("Drop file: %s [%ld MB]", buffer.name.c_str(), buffer.data.size()/MBYTE);
      buffer.name = "";
      this->m_free.emplace_back(std::move(buffer));
      this->m_todo.pop_back();
    }
  }
}

int StorageReader::open_file(Buffer& buffer)   {
  // No threading: synchronous reading
  if ( this->m_num_buffers == 0 || this->m_num_threads == 0 )   {
    int ret = i_open_file(buffer);
    return ret;
  }

  // Asynchronous reading: Wait until some buffer is ready
  {
    std::lock_guard<std::mutex> lock(this->m_bufferLock);
    if ( !buffer.data.empty() )   {
      buffer.name = "";
      this->m_free.emplace_back(std::move(buffer));
    }
  }
  while( 1 )   {{
      std::lock_guard<std::mutex> lock(this->m_bufferLock);
      if ( m_todo.empty() )   {
	if ( 0 == this->m_is_reading && !this->m_receiveEvts )  {
	  return DF_ERROR;
	}
      }
      else   {
	buffer = std::move(this->m_todo.front());
	this->m_todo.pop_front();
	return DF_SUCCESS;
      }
    }
    if ( this->m_goto_paused )   {
      return DF_ERROR;
    }
    if ( this->m_receiveEvts )   {
      ::lib_rtl_sleep(m_poll_tmo);
    }
  }
  return DF_ERROR;
}

/// IRunable implementation : Run the class implementation
int StorageReader::i_run()  {
  int     status;
  Buffer  current_buffer; // Never clear in here. We try to re-use the memory!
  RawFile current_input;

  status = this->waitForGo();
  if ( status != DF_CONTINUE )  {
    this->m_loadEnabled = false;
    return status;
  }
  while (1)  {
    if ( this->m_receiveEvts && GO_PROCESS != this->m_goValue )   {
      ::lib_rtl_sleep(100);
      continue;
    }
    else if ( !this->m_receiveEvts )  {
      break;
    }
    while ( (this->m_receiveEvts && GO_PROCESS == this->m_goValue) ||
	    (0 != current_input.data_size()) ||
	    (0 != m_todo.size()) ||
	    (0 != m_is_reading) )     {

      if ( this->m_muDelay > 0 )  {
	::usleep(this->m_muDelay);
      }
      if ( !(current_input.isMapped() || current_input.isOpen()) )  {
	this->updateRunNumber(0);
	status = this->open_file(current_buffer);
	if ( status == DF_ERROR && this->m_goto_paused )   {
	  // Sleep a bit before goung to pause
	  this->info("Sleeping before going to PAUSE....");
	  ::lib_rtl_sleep(1000*this->m_pauseSleep);
	  this->fireIncident("DAQ_PAUSE");
	  if ( context.task )   {
	    context.task->declareState(Control::ST_PAUSED);
	  }
	  this->m_receiveEvts = false;
	  this->info("Quitting...");
	  break;
	}
	else if ( status == DF_ERROR ) {
	  this->info("Locking event loop. Waiting for work....");
	  ::lib_rtl_lock(m_lock);
	  break;
        }
	this->warning("Opened %s [%ld MB]", current_buffer.name.c_str(), current_buffer.data.size()/MBYTE);
	current_input.map_memory(&current_buffer.data.at(0), current_buffer.data.size());
	current_input.setName(current_buffer.name);
	this->updateRunNumber(current_buffer.runNumber);
      }

      if ( !(current_input.isMapped() || current_input.isOpen()) )  {
	current_input.reset();
	continue;
      }
      else    {
	// Remember data position pointer to possibly recover the operation
	auto pos = current_input.position();
	// Handle request: access data and push to MBM
	status = this->handle_mbm_request(current_input);
	if ( status == DF_SUCCESS )   {
	  // All OK.
	}
	else if ( !(current_input.isOpen() || current_input.isMapped()) )   {
	  continue;  // File is processed: Continue and ask for next file
	}
	else if ( status == DF_CONTINUE && this->m_receiveEvts )   {  // Recoverable: missing consumer etc.
	  if ( (current_buffer.data.size() - pos) > sizeof(EventHeader) )   {
	    // **MSF**: Is this really correct ???
	    current_input.map_memory(&current_buffer.data.at(0), current_buffer.data.size());
	    current_input.position(pos, true);
	  }
	  continue;
	}
	else if ( status == DF_CONTINUE )   {  // Stop requested in addiiton
	  break;    // File is processed: Continue with outer loop
	}
	else if ( status == DF_ERROR )   {     // Unrecoverable failure: goto state ERROR after cleanup
	  this->m_receiveEvts = false;
	  this->updateRunNumber(0);
	  current_input.reset(true);
	  this->fireIncident("DAQ_ERROR");
	  return DF_ERROR;
	}
      }
    }
    current_input.reset(true);
    // Bad file: Cannot read input (m_eventsIN==0)
    this->updateRunNumber(0);
  }
  /// Before actually declaring PAUSED, we wait until no events are pending anymore.
  this->waitForPendingEvents(this->m_maxPauseWait);
  return DF_SUCCESS;
}
