//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  StorageWriter.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "StorageWriter.h"
#include <Dataflow/Plugins.h>
#include <Dataflow/Printout.h>
#include <Storage/fdb_client.h>
#include <Tell1Data/RawFile.h>
#include <Tell1Data/Tell1Decoder.h>
#include <PCIE40Data/pcie40decoder.h>
#include <PCIE40Data/sodin.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

/// ROOT include files
#include <TFile.h>
#include <TSystem.h>
#include <TUrl.h>

/// C/C++ include files
#include <filesystem>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <sstream>
#include <ctime>

namespace {
  static constexpr long MBYTE = (1024e0*1024e0);
}

using namespace Online;
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_StorageWriter,StorageWriter)

namespace  {
  bool _check_nfs_access(const std::string& dir_name)    {
    std::error_code ec;
    struct stat dirfs, rootfs;
    std::filesystem::path check_dir(dir_name);

    /// We need to parent directory which exists to check if the volumes are different:
    while( check_dir.string().find("/",1) != std::string::npos )    {
      check_dir = check_dir.parent_path();
      if ( std::filesystem::exists(check_dir, ec) ) break;
    }
    // Get the device ID of the ROOT directory:
    if ( 0 != ::stat("/", &rootfs) ) {
      errno = ENODEV;
      return false;
    }
    // Get the device ID of the output volume:
    // std::cout << "Checking: " << check_dir.string() << std::endl;
    if ( 0 != ::stat(check_dir.string().c_str(), &dirfs) ) {
      errno = ENODEV;
      return false;
    }
    // The two devices must be different:
    // std::cout << "Checking: " << dirfs.st_dev << " <> " << rootfs.st_dev << std::endl;
    if ( dirfs.st_dev == rootfs.st_dev )    {
      errno = ENODEV;
      return false;
    }
    /// Usable output area.....
    return true;
  }
}

struct StorageWriter::POSIX_FILE   {
  RawFile     file       {   };
  std::mutex  lock;
  long        length     { 0 };
  uint32_t    run        { 0 };
  std::time_t last_write { 0 };
  POSIX_FILE() = default;
  POSIX_FILE(POSIX_FILE&& copy) = delete;
  POSIX_FILE(const POSIX_FILE& copy) = delete;
  POSIX_FILE& operator=(POSIX_FILE&& copy) = delete;
  POSIX_FILE& operator=(const POSIX_FILE& copy) = delete;
  ~POSIX_FILE() = default;

  void close()   {
    if ( this->file.isOpen() ) this->file.close();
    this->length = 0;
    this->run = 0;
  }
  int open(const std::string& fname, int verifyNFS)   {
    std::filesystem::path parent = std::filesystem::path(fname).parent_path();
    if ( verifyNFS )   {
      if ( !_check_nfs_access(parent.string()) )   {
	errno = ENODEV;
	return -1;
      }
    }
    if ( 0 == RawFile::mkdir(parent.c_str(),0777) )   {
      this->file.setName(fname);
      this->length = 0;
      this->run = 0;
      return this->file.openWrite(false);
    }
    return -1;
  }
  const char* name() const   {
    return this->file.cname();
  }
  bool isOpen() const       {
    return this->file.isOpen();
  }
  long write(const void* b, std::size_t l)  {
    return this->file.write(b, l);
  }
};

/// ROOT implementation
struct StorageWriter::ROOT_FILE   {
  std::unique_ptr<TFile> file;
  long                   length = 0;
  uint32_t               run = 0;
  std::time_t            last_write = 0;
  std::mutex             lock;
  ROOT_FILE() = default;
  ROOT_FILE(ROOT_FILE&& copy) = delete;
  ROOT_FILE(const ROOT_FILE& copy) = delete;
  ROOT_FILE& operator=(ROOT_FILE&& copy) = delete;
  ROOT_FILE& operator=(const ROOT_FILE& copy) = delete;
  ~ROOT_FILE() = default;
  void close()   {
    this->length = 0;
    this->run = 0;
    if ( this->file ) this->file->Close();
    this->file.reset();
  }
  int open(const std::string& fname, int verifyNFS)   {
    std::filesystem::path parent = std::filesystem::path(fname).parent_path();
    if ( verifyNFS )    {
      if ( !_check_nfs_access(parent.string()) )   {
	errno = ENODEV;
	return -1;
      }
    }
    void* dir = gSystem->OpenDirectory(parent.c_str());
    if ( dir )   {
      gSystem->FreeDirectory(dir);
    }
    else if ( 0 != gSystem->mkdir(parent.c_str(), kTRUE) )    {
      errno = gSystem->GetErrno();
      return -1;
    }
    TUrl    url(fname.c_str());
    TString opts = "filetype=raw", proto, spec, tmp = url.GetOptions();
    if ( tmp.Length() > 0 ) {
      opts += "&";
      opts += url.GetOptions();
    }
    url.SetOptions( opts );
    proto = url.GetProtocol();
    spec  = (proto == "file" || proto == "http") ? fname + "?filetype=raw" : url.GetUrl();
    this->file.reset(TFile::Open(spec, "RECREATE", "", 0));
    this->length = 0;
    this->run = 0;
    if ( this->file && !this->file->IsZombie() )
      return 1;
    errno = gSystem->GetErrno();
    this->file.reset();
    return -1;
  }
  const char* name() const   {
    return this->file->GetName();
  }
  bool isOpen() const   {
    return this->file.get() ? this->file->IsOpen() : false;
  }
  long write(const void* buff, std::size_t len)  {
    Bool_t ret = this->file->WriteBuffer((const char*)buff, len);
    if ( kTRUE == ret )   {
      errno = gSystem->GetErrno();
      return -1;
    }
    return len;
  }
};

/// Initializing constructor
StorageWriter::StorageWriter(const std::string& nam, Context& ctxt)
  : Component(nam, ctxt)
{
  std::string fname = "/${PARTITION}/${RUN1000}/Run_${RUN}_${NODE}_${TIME}_${PID}_${SEQ}.mdf";
  this->declareProperty("Server",            m_server);
  this->declareProperty("FDBVersion",        m_fdb_version       = 0);
  this->declareProperty("BufferSizeMB",      m_buffer_size       = 1024);
  this->declareProperty("WriteErrorRetry",   m_write_error_retry = 10);
  this->declareProperty("WriteErrorSleep",   m_write_error_sleep = 2000);
  this->declareProperty("PollTimeout",       m_poll_tmo          = 1000);
  this->declareProperty("IdleTimeout",       m_idle_tmo          = 20);
  this->declareProperty("CancelTimeout",     m_cancel_tmo        = 100);
  this->declareProperty("NumBuffers",        m_num_buffers       = 2);
  this->declareProperty("NumThreads",        m_num_threads       = 1);
  this->declareProperty("MinFileSizeMB",     m_min_file_size_MB  = 5);
  this->declareProperty("MaxFileSizeMB",     m_max_file_size_MB  = 5000);
  this->declareProperty("DebugClient",       m_debug_client      = 0);
  this->declareProperty("OutputType",        m_output_type       = "network");
  this->declareProperty("HaveFileDB",        m_have_file_db      = 0);
  this->declareProperty("EnableWriting",     m_enable_writing    = 1);
  this->declareProperty("VerifyNFS",         m_verify_nfs        = 1);
  this->declareProperty("MEP2MDF",           m_mep2mdf           = 1);
  this->declareProperty("MaxEvents",         m_max_events        = std::numeric_limits<int>::max());

  this->declareProperty("Stream",            m_stream            = "RAW");
  this->declareProperty("RunType",           m_run_type          = "");
  this->declareProperty("RunList",           m_run_list);
  this->declareProperty("PartitionName",     m_partition_name     = "LHCb");
  this->declareProperty("FileName",          m_file_name         = fname);
  this->declareProperty("ThreadFileQueues",  m_threadFileQueues  = false);
  storage::error_check_enable(false);
}

/// Default destructor
StorageWriter::~StorageWriter()   {
}

/// Initialize the MBM server
int StorageWriter::initialize()  {
  int sc = this->Component::initialize();
  if ( sc == DF_SUCCESS )  {
    storage::uri_t url(this->m_server);
    this->m_buffer_size *= MBYTE;
    this->m_cancelled   = 0;
    this->m_curr_run = 0;

    if ( this->m_file_name.find(":") != std::string::npos )
      this->m_output_type_id = ROOT_STORAGE, m_output_type = "ROOT";
    else if ( ::strcasecmp(this->m_output_type.c_str(), "NFS") == 0 )
      this->m_output_type_id = POSIX_STORAGE;
    else if ( ::strcasecmp(this->m_output_type.c_str(), "ROOT") == 0 )
      this->m_output_type_id = ROOT_STORAGE;
    else if ( ::strcasecmp(this->m_output_type.c_str(), "POSIX") == 0 )
      this->m_output_type_id = POSIX_STORAGE;
    else if ( ::strcasecmp(this->m_output_type.c_str(), "NETWORK") == 0 )
      this->m_output_type_id = NETWORK_STORAGE;

    for(Buffer& b : this->m_free)
      ::free(b.buffer);
    this->m_free.clear();
    for(std::size_t i=0; i<this->m_num_buffers; ++i)   {
      Buffer b;
      b.buffer  = (uint8_t*)::malloc(m_buffer_size+1024);
      b.pointer = b.buffer;
      this->m_free.push_back(b);
    }
    this->declareMonitor("Events","OUT",     m_events_OUT=0,   "Number of events processed");
    this->declareMonitor("Events","DROPPED", m_events_DROP=0,  "Number of events processed");
    this->declareMonitor("Bursts","OUT",     m_burstsOUT=0,   "Number of bursts processed");
    this->declareMonitor("FilesOpen",        m_filesOpen=0,   "Number of output files currently open");
    this->declareMonitor("FilesOpened",      m_filesOpened=0, "Number of output files opened");
    this->declareMonitor("FilesClosed",      m_filesClosed=0, "Number of output files closed");
    this->declareMonitor("BytesOut",         m_bytesOut=0,    "Number of bytes collected");
    this->declareMonitor("BytesDropped",     m_bytesDropped=0,"Number of bytes dropped");
    this->declareMonitor("BadHeader",        m_badHeader=0,   "Number of MDF events with bad header structure");
    this->m_shutdown = false;
    for(std::size_t i=0; i<this->m_num_threads; ++i)
      this->m_threads.emplace_back(std::make_unique<std::thread>([this]{ this->process_buffers(); }));
    storage::error_check_enable(this->outputLevel < INFO);
    if ( this->decode_run_list() != DF_SUCCESS )    {
      return DF_ERROR;
    }
  }
  return sc;
}

/// Initialize the MBM server
int StorageWriter::start()  {
  int sc = this->Component::start();
  if ( sc != DF_SUCCESS )  {
    return sc;
  }
  if ( this->decode_run_list() != DF_SUCCESS )    {
    return DF_ERROR;
  }
  this->m_last_event_stamp = ::time(0);
  this->m_shutdown = false;
  this->m_cancelled = 0;
  this->m_curr_run = 0;
  return sc;
}

/// Stop the data flow component. Default implementation is empty.
int StorageWriter::stop()  {
  this->m_cancelled = ::time(0);
  if ( !this->m_active.empty() )   {
    std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
    while ( !this->m_active.empty() )   {
      auto buff = std::move(m_active.begin()->second);
      m_active.erase(m_active.begin());
      if ( buff.buffer != nullptr && buff.pointer > buff.buffer )   {
	this->m_todo.push_back(buff);
      }
    }
  }
  ::lib_rtl_usleep(m_poll_tmo);
  this->m_curr_run = 0;
  return this->Component::stop();
}

/// Cancel the data flow component. Default implementation is empty.
int StorageWriter::cancel()  {
  this->m_cancelled = ::time(0);
  return this->Component::cancel();
}

/// Finalize the MBM server
int StorageWriter::finalize()  {
  if ( !this->m_active.empty() )   {
    std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
    while ( !this->m_active.empty() )   {
      auto buff = std::move(m_active.begin()->second);
      m_active.erase(m_active.begin());
      if ( buff.buffer != nullptr && buff.pointer > buff.buffer )   {
	this->m_todo.push_back(buff);
      }
    }
  }
  ::lib_rtl_usleep(this->m_poll_tmo);
  this->m_curr_run = 0;
  this->m_shutdown = true;
  for(size_t i=0; i<this->m_num_threads; ++i)  {
    this->m_threads[i]->join();
    this->m_threads[i].reset();
  }
  this->m_threads.clear();
  for(Buffer& b : this->m_free)
    ::free(b.buffer);
  this->m_free.clear();
  int sc = this->Component::finalize();
  return sc;
}

/// Decode the run-list to determine proper file names for HLT2
int StorageWriter::decode_run_list()  {
  int status = DF_SUCCESS;
  this->m_run_partitions.clear();
  for( const auto& run_part : this->m_run_list )   {
    int run_num = 0;
    auto items = RTL::str_split(run_part,"/");
    if ( items.size() == 2 && 1 == ::sscanf(items[1].c_str(),"%010d",&run_num) )   {
      std::string part = items[0];
      this->m_run_partitions.insert(std::make_pair(run_num, part));
      //
      // We need to update the partition name for runs selected for HLT2 to get the output names correctly.
      // Note:
      // If after a STOP_RUN new runs are selected this will inevitably lead to an inconsistency!
      // This only fixes the problem if runs from the SAME partition are selected!
      //
      this->m_partition_name = part;
      continue;
    }
    this->error("Invalid run-list encountered. Failed to decode. Item: %s", run_part.c_str());
    status = DF_ERROR;
  }
  return status;
}

/// Aquire fresh buffer to save event data
StorageWriter::Buffer& StorageWriter::get_buffer(uint32_t run, int64_t length)   {
  static Buffer empty;
  std::time_t now;
  {
    if ( run > this->m_curr_run )   {
      this->m_curr_run = run;
    }
    std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
    now = ::time(0);
    // Check for outdated buffers from previous runs
    for(auto ib = this->m_active.begin(); ib != this->m_active.end(); ++ib )   {
      const auto& buff = (*ib).second;
      if ( (*ib).first < this->m_curr_run &&
	   (m_active.size() > 2 || (now - buff.last_write) > this->m_idle_tmo) )  {
	this->m_todo.emplace_back(buff);
	this->m_active.erase(ib);
	ib = this->m_active.begin();
      }
      else if ( buff.num_events >= this->m_max_events )  {
	this->m_todo.emplace_back(buff);
	this->m_active.erase(ib);
	ib = this->m_active.begin();
      }
    }
    // Check if there are already active buffers availible with sufficient space
    for(auto ib = this->m_active.begin(); ib != this->m_active.end(); ++ib )   {
      if ( (*ib).first == run )   {
	auto& buff = (*ib).second;
	if ( (buff.pointer - buff.buffer) + length < this->maxBufferSize() )   {
	  buff.last_write = now;
	  return buff;
	}
	this->m_todo.emplace_back(buff);
	this->m_active.erase(ib);
	break;
      }
    }
  }
  while( !this->m_shutdown )   {{
      std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
      if ( !this->m_free.empty() )   {
	this->m_active.emplace(run, m_free.back());
	this->m_free.pop_back();
	for(auto& b : this->m_active)   {
	  if ( b.first == run )   {
	    b.second.pointer   = b.second.buffer;
	    b.second.last_write = ::time(0);
	    b.second.run_number = run;
	    b.second.num_events    = 0;
	    return b.second;
	  }
	}
      }
    }
    if ( this->m_poll_tmo > 0 )  {
      ::lib_rtl_usleep(m_poll_tmo);
    }
    {
      now = ::time(0);
      std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
      for(auto ib = this->m_active.begin(); ib != this->m_active.end(); ++ib )   {
	if ( (*ib).first < this->m_curr_run &&
	     (m_active.size() > 2 || (now - (*ib).second.last_write) > this->m_idle_tmo) )  {
	  this->m_todo.emplace_back((*ib).second);
	  this->m_active.erase(ib);
	  break;  // Should be enough to move them one-by-one
	}
      }
    }
  }
  empty = {nullptr, nullptr};
  return empty;
}

/// Append data to current buffer. If too small allocate a new buffer
int StorageWriter::save_buffer(uint32_t run, const void* data, int64_t length)   {
  if ( this->maxBufferSize() < length )   {
    this->error("Execute: Failed to allocate buffer [%ld bytes]. "
		"BUFFER SIZE TOO SMALL! Change options!", length);
    ++this->m_events_DROP;
    return DF_ERROR;
  }

  Buffer& buff = this->get_buffer(run, length);
  
  if ( this->m_shutdown && buff.buffer == nullptr )   {
    info("Execute: Failed to allocate buffer. Drop event. [Shutdown requested]");
    ++this->m_events_DROP;
    return DF_SUCCESS;
  }
  if ( buff.buffer == nullptr )   {
    info("Execute: Failed to allocate buffer. Drop event. [Internal error ?????]");
    ++this->m_events_DROP;
    return DF_CONTINUE;
  }
  this->m_last_event_stamp = buff.last_write;
  ::memcpy(buff.pointer, data, length);
  buff.pointer += length;
  ++buff.num_events;
  ++this->m_events_OUT;
  return DF_SUCCESS;
}

/// Convert PCIE40 MEP to MDF and save it.
int StorageWriter::save_pcie40_as_mdf(const uint8_t* start, int64_t len)   {
  auto* mep_start = (pcie40::mep_header_t*)start;
  auto* mep_end   = pcie40::add_ptr<pcie40::mep_header_t>(start, len);
  pcie40::decoder_t decoder;
  int64_t nev = 0;

  for(const pcie40::mep_header_t *m, *mep = mep_start; mep < mep_end; )   {
    if ( mep->is_valid() )   {
      const pcie40::multi_fragment_t *mfp = mep->multi_fragment(0);
      std::unique_ptr<pcie40::event_collection_t> ev;
      uint16_t packing = mfp->header.packing;
      nev += packing;
      decoder.decode(ev, mep);
      /// Save the events one by one to the buffer
      for( auto* e=ev->begin(); e != ev->end(); e=ev->next(e) )  {
	uint32_t      hdrvsn = 3;
	std::size_t   hdrlen = EventHeader::sizeOf(hdrvsn);
	std::size_t   length = e->total_length();
	const auto* odin = e->bank_collection(0)->at(0);

	if ( odin && odin->data() )   {
	  const auto* sodin  = odin->begin<pcie40::sodin_t>();
	  int32_t curr_run   = sodin->run_number();
	  int32_t curr_orbit = sodin->orbit_id();
	  int32_t curr_bunch = sodin->bunch_id();
	  Buffer& buff = this->get_buffer(curr_run, length + hdrlen);

	  if ( this->m_shutdown && buff.buffer == nullptr )   {
	    info("Execute: Failed to allocate buffer. Drop event. [Shutdown requested]");
	    ++this->m_events_DROP;
	    return DF_SUCCESS;
	  }
	  if (buff.buffer == nullptr )   {
	    info("Execute: Failed to allocate buffer. Drop event. [Internal error ?????]");
	    ++this->m_events_DROP;
	    return DF_CONTINUE;
	  }

	  const uint8_t* b_beg = buff.pointer;
	  EventHeader*   hdr   = (EventHeader*)buff.pointer;
	  hdr->setChecksum(0);
	  hdr->setCompression(0);
	  hdr->setHeaderVersion(hdrvsn);
	  hdr->setSpare(0);
	  hdr->setDataType(EventHeader::BODY_TYPE_BANKS);
	  hdr->setSubheaderLength(sizeof(EventHeader::Header1));
	  hdr->setSize(length);
	  EventHeader::SubHeader h = hdr->subHeader();
	  h.H1->setTriggerMask(~0u,~0u,~0u,~0u);
	  h.H1->setRunNumber(curr_run);
	  h.H1->setOrbitNumber(curr_orbit);
	  h.H1->setBunchID(curr_bunch);
	  buff.pointer += hdrlen;
	  for( std::size_t i=0, n=e->num_bank_collections(); i<n; ++i)   {
	    buff.pointer = (uint8_t*)e->bank_collection(i)->copy_data(buff.pointer);
	  }
	  if ( buff.pointer-b_beg != hdr->recordSize() )  {
	    this->error("++ Event length inconsistency: %ld <> %ld %ld %ld",
			buff.pointer-b_beg, hdr->recordSize(), length, hdrlen);
	  }
	  ++buff.num_events;
	  ++this->m_events_OUT;
	  continue;
	}
	++this->m_events_DROP;
      }
      /// Move to the next MEP if any
      if ( m = mep->next(); m > mep )  {
	mep = m;
	continue;
      }
    }
    break;
  }
  return nev > 0 ? DF_SUCCESS : DF_ERROR;
}

/// Save PCIE40 MEP as is
int StorageWriter::save_pcie40_as_mep(const uint8_t* start, int64_t len)   {
  auto* mep_start = (pcie40::mep_header_t*)start;
  auto* mep_end   = pcie40::add_ptr<pcie40::mep_header_t>(start, len);

  /// For each MEP in the frame: Search for the odin bank and dump the MEP frame
  for(const pcie40::mep_header_t *m, *mep = mep_start; mep < mep_end; )   {
    if ( !mep->is_valid() )   {
      /// If we do not have a MEP header: stop processing
      ++this->m_badHeader;
      return DF_SUCCESS;
    }
    std::size_t num_fragments = mep->num_source;
    for( uint32_t i = 0; i < num_fragments; ++i )    {
      const pcie40::multi_fragment_t *mfp  = mep->multi_fragment(i);
      const pcie40::frontend_data_t  *curr = mfp->data();
      const std::size_t align = mfp->header.alignment;
      const uint8_t     *typs = mfp->types();
      const uint16_t    *lens = mfp->sizes();

      for (std::size_t cnt=0, n=mfp->packingFactor(); cnt<n; ++cnt, ++typs, ++lens)  {
	auto typx   = *typs;
	auto length = *lens;
	if ( typx == Tell1Bank::ODIN )   {
	  const auto* sodin  = (pcie40::sodin_t*)curr;
	  int32_t run  = sodin->run_number();
	  Buffer& buff = this->get_buffer(run, len);
	  if ( this->m_shutdown && buff.buffer == nullptr )   {
	    this->info("Execute: Failed to allocate buffer. Drop event. [Shutdown requested]");
	    ++this->m_events_DROP;
	    return DF_SUCCESS;
	  }
	  if (buff.buffer == nullptr )   {
	    this->info("Execute: Failed to allocate buffer. Drop event. [Internal error ?????]");
	    ++this->m_events_DROP;
	    return DF_CONTINUE;
	  }
	  int sc = this->save_buffer(run, mep, length);
	  if ( DF_SUCCESS != sc )   {
	    return DF_SUCCESS;
	  }
	  break;
	}
	curr = curr->next(length, align);
      }
    }
    /// Move to the next MEP if any
    if ( m = mep->next(); m > mep )  {
      mep = m;
      continue;
    }
  }
  return DF_SUCCESS;
}

/// Save MDF frame or MDF burst
int StorageWriter::save_mdf_buffer(const uint8_t* start, int64_t len)   {
  long num_bad_headers = 0;
  /// Auto detect data type: now check for MDF data type
  for( const uint8_t *begin=start, *end = start + len; start < end; )   {
    auto* header = (EventHeader*)start;
    if ( !header->is_mdf() )   {
      this->warning("save_mdf: Encountered invalid MDF header: (%d %d %d). Skip %ld bytes of data.",
		    header->size0(), header->size1(), header->size2(), start-begin);
      ++this->m_badHeader;
      return DF_SUCCESS;
    }
    auto*    hdr    = header->subHeader().H1;
    uint32_t length = header->size0();
    uint32_t run    = hdr->runNumber();
    if ( 0 == run )    {
      start += length;
      ++num_bad_headers;
      ++this->m_badHeader;
      ++this->m_events_DROP;
      continue;
    }
    if ( run > this->m_curr_run )   {
      this->m_curr_run = run;
    }
    if ( this->context.manager.currentRunNumber() != run )   {
      this->context.manager.setRunNumber(run);
    }
    int sc = this->save_buffer(run, header, length);
    if ( DF_SUCCESS != sc )   {
      return DF_SUCCESS;
    }
    start += length;
  }
  if ( num_bad_headers > 0 )   {
    this->warning("save_mdf: Dropped %ld MDF frames due to bad header structure.", num_bad_headers);
  }
  return DF_SUCCESS;
}

/// Data processing overload: Write the data record to disk
int StorageWriter::execute(const Context::EventData& event)  {
  int status = DF_SUCCESS;
  /// Extend idle time if there are still events coming
  if ( this->m_cancelled > 0 )  {
    this->m_cancelled = ::time(0);
    ++this->m_events_DROP;
  }
  else if ( this->m_enable_writing )   {
    try  {
      std::size_t len    = event.length;
      auto*       start  = (uint8_t*)event.data;

      /// Extend idle time if there are still events coming
      if ( this->m_cancelled > 0 )  {
	this->m_cancelled = ::time(0);
      }

      /// Auto detect data type: first check for PCIE40 MEP format
      auto* mep_hdr = (pcie40::mep_header_t*)start;
      if( mep_hdr->is_valid() )   {
	if ( this->m_mep2mdf )   {
	  status = this->save_pcie40_as_mdf(start, int64_t(mep_hdr->size*sizeof(uint32_t)));
	  if ( status == DF_SUCCESS ) ++this->m_burstsOUT;
	  return status;
	}
	else   {
	  status = this->save_pcie40_as_mep(start, int64_t(mep_hdr->size*sizeof(uint32_t)));
	  if ( status == DF_SUCCESS ) ++this->m_burstsOUT;
	  return status;
	}
      }

      /// Auto detect data type: first check for MDF/BURST format
      auto* mdf_hdr = (EventHeader*)start;
      if ( mdf_hdr->is_mdf() )   {
	status = this->save_mdf_buffer(start, (int64_t)len);
	if ( status == DF_SUCCESS ) ++this->m_burstsOUT;
	return status;
      }
      this->error("Execute: Cannot determine event type. Drop event buffer.");
      return status;
    }
    catch(const std::exception& e)   {
      this->error(e,"Execute: Error processing event.");
    }
    catch(...)  {
      this->error("Execute: UNKOWN error processing event.");
    }
  }
  else   {
    ++this->m_events_DROP;
  }
  return status;
}

/// Construct file name
std::string StorageWriter::makeFileName(int run)    {
  char text[128];
  struct timeval tv;
  std::string file_name = this->m_file_name;
  std::string part = this->m_partition_name;
  ::gettimeofday(&tv, nullptr);
  struct tm *timeinfo = ::localtime(&tv.tv_sec);

  ++this->m_sequence_number;
  if ( !this->m_run_partitions.empty() )    {
    auto irun = this->m_run_partitions.find(run);
    if ( irun != this->m_run_partitions.end() )
      part = (*irun).second;
  }
  ::strftime(text, sizeof(text), "%Y%m%d-%H%M%S-", timeinfo);
  ::snprintf(text+::strlen(text), sizeof(text)-::strlen(text),"%03ld", tv.tv_usec/1000);
  file_name  = RTL::str_replace(file_name,  "${TIME}",      text);
  file_name  = RTL::str_replace(file_name,  "${NODE}",      RTL::nodeNameShort());
  file_name  = RTL::str_replace(file_name,  "${STREAM}",    this->m_stream);
  file_name  = RTL::str_replace(file_name,  "${RUNTYPE}",   this->m_run_type);
  file_name  = RTL::str_replace(file_name,  "${PARTITION}", part);
  ::snprintf(text, sizeof(text), "%010d", int((run/1000)*1000));
  file_name  = RTL::str_replace(file_name,  "${RUN1000}",   text);
  ::snprintf(text, sizeof(text), "%010d", run);
  file_name  = RTL::str_replace(file_name,  "${RUN}",       text);
  ::snprintf(text, sizeof(text), "%04ld", m_sequence_number);
  file_name  = RTL::str_replace(file_name,  "${SEQ}",       text);
  ::snprintf(text, sizeof(text), "%06d", ::lib_rtl_pid());
  file_name  = RTL::str_replace(file_name,  "${PID}",       text);
  return file_name;
}

/// Thread invocation routine to save assembled buffers to the disk server
template <typename OUT_TYPE>
int StorageWriter::process_posix_buffers(std::mutex& queue_lock, std::map<int,std::unique_ptr<OUT_TYPE> >& open_files)    {
  auto add_db_entry = [this] (size_t len, const std::string& fname)   {
    if ( this->m_have_file_db )   {
      http::HttpReply  reply;
      try   {
	std::string   url;
	client_t cl(this->m_fdb_version);
	storage::uri_t srv(this->m_server);
	storage::client::reqheaders_t hdrs;
	cl.fdbclient =
	  storage::client::create<storage::client::sync>(srv.host, srv.port, 10000, this->m_debug_client);
	if ( this->m_have_file_db == 2 )   {
	  hdrs.emplace_back(http::constants::location, fname);
	}
	reply = cl.save_object_record(fname, url, len, hdrs);
	if ( reply.status == reply.permanent_redirect || reply.status == reply.continuing )  {
	  /// OK. the registration was now successful
	  return true;
	}
	else   {
	  this->error("posix_buffers: FAILED accessing FDB server: %s",
		      http::HttpReply::stock_status(reply.status).c_str());
	  return false;
	}
      }
      catch(const std::exception& e)  {
	this->error("posix_buffers: Exception accessing FDB server: %s", e.what());
	return false;
      }
      catch(...)  {
	this->error("posix_buffers: UNKNOWN Exception accessing FDB server");
	return false;
      }
    }
    return true;
  };
  auto close_output = [this,add_db_entry](std::unique_ptr<OUT_TYPE>& output, const char* reason, int max_retry)   {
    if ( output.get() && output->isOpen() )   {
      std::lock_guard<std::mutex> file_lock(output->lock);
      if ( output->isOpen() )   {
	std::size_t len = output->length;
	std::string nam = output->name();
	output->close();
	this->warning("Closed %s after %ld MB [%s].", nam.c_str(), len/MBYTE, reason);
	++this->m_filesClosed;
	--this->m_filesOpen;
	int  nretry  = max_retry;
	bool status = false;
	do   {
	  status = add_db_entry(len, nam.c_str());
	  if ( !status ) ::lib_rtl_sleep(1000);
	} while ( !status && --nretry > 0 );
	if ( !status )   {
	  this->error("Cannot register %s with %ld MB [%s] (ignored after %d retries).",
		      nam.c_str(), len/MBYTE, reason, max_retry);
	}
	return status;
      }
    }
    output.reset();
    return true;
  };

  std::time_t last_check = ::time(0);
  std::time_t now;
  while( 1 )   {
    // First check buffers: are there any to be retired ?
    Buffer b {nullptr, nullptr}; {
      std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
      now = ::time(0);
      // Check for outdated buffers when being idle:
      for(auto ib = this->m_active.begin(); ib != this->m_active.end(); ++ib )   {
	// Use large timeout before really dropping buffer.
	// Fast changes during saving are handled in this->get_buffer()
	if ( (now - this->m_last_event_stamp) > this->m_idle_tmo )   {
	  this->m_todo.emplace_back((*ib).second);
	  this->m_active.erase(ib);
	  break;
	}
      }
      /// Check for exit condition
      if ( this->m_free.size() == this->m_num_buffers && this->m_shutdown )   {
	std::lock_guard<std::mutex> outputLock(queue_lock);
	/// Shutdown (reset) requested. Close output and register file. If not possible -- nothing we can do!
	for(auto& output : open_files )
	  close_output(output.second, "shutdown", 0);
	this->info("process: Exit condition detected. Leaving submit thread.");
	return DF_SUCCESS;
      }
      else if ( !this->m_todo.empty() )   {
	b = this->m_todo.back();
	this->m_todo.pop_back();
      }
    }
    if ( (now - last_check) >= this->m_idle_tmo || 
	 (now - this->m_last_event_stamp) > this->m_idle_tmo )   {
      std::lock_guard<std::mutex> outputLock(queue_lock);
      std::vector<int> to_erase;
      now = ::time(0);
      for(auto& output : open_files )   {
	if ( this->m_curr_run >= output.second->run )   {
	  if ( (now - output.second->last_write) > this->m_idle_tmo )   {
	    // if the current run is still the active run AND
	    // there are still events (very slowly) flowing, do not close prematurely.
	    if ( (now - this->m_last_event_stamp) > this->m_idle_tmo )   {
	      close_output(output.second, "output-idle", 1);
	      to_erase.emplace_back(output.first);
	    }
	  }
	}
      }
      for(auto run : to_erase)   {
	auto it = open_files.find(run);
	if ( it != open_files.end() ) open_files.erase(it);
      }
      last_check = now;
    }
    if ( !b.buffer )   {
      if ( m_poll_tmo > 0 )  {
	::lib_rtl_usleep(m_poll_tmo);
      }
      continue;
    }
    else if ( b.buffer )   {
      queue_lock.lock();
      auto& output = open_files[b.run_number];
      long  len    = b.pointer - b.buffer;

      if ( output.get() && output->length > 0 && output->length + len > m_max_file_size_MB * MBYTE )
	close_output(output, "size-limit", 1);

      if ( !output.get() )   {
        output = std::make_unique<OUT_TYPE>();
      }

      std::lock_guard<std::mutex> file_lock(output->lock);
      now = ::time(0);
      if ( !output->isOpen() )   {
	std::string fname = this->makeFileName(b.run_number);
	if ( output->open(fname, this->m_verify_nfs) <= 0 )   {
	  this->error("Failed to open output %s. [%s]", fname.c_str(), RTL::errorString(errno).c_str());
	  this->fireIncident("DAQ_ERROR");
	  return DF_ERROR;
	}
	this->warning("Opened %s", output->name());
	output->run = b.run_number;
	++this->m_filesOpened;
	++this->m_filesOpen;
      }
      output->last_write = now;
      queue_lock.unlock();
      long ret = output->write(b.buffer, len);
      if ( ret == long(len) )    {
	output->last_write = now;
	output->length += len;
	std::lock_guard<std::mutex> counterLock(this->m_counter_lock);
	this->m_bytesOut += len;
      }
      else    {
	this->error("Failed to write buffer. Close file %s [%s]", output->name(), RTL::errorString(errno).c_str());
	output->lock.unlock();
	close_output(output, "write-failed", 1);

	std::lock_guard<std::mutex> counterLock(this->m_counter_lock);
	this->m_bytesDropped += len;
	++this->m_writeErrors;
	//this->fireIncident("DAQ_ERROR");
	//return DF_ERROR;
      }
      {
	std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
	b.pointer = b.buffer;
	this->m_free.push_back(b);
      }
      continue;
    }
  }
  {
    std::lock_guard<std::mutex> outputLock(queue_lock);
    for(auto& output : open_files )
      close_output(output.second, "ending", 0);
  }
  return DF_SUCCESS;
}

/// Thread invocation routine to save assembled buffers to the disk server
int StorageWriter::process_network_buffers()    {
  while( 1 )   {
    Buffer b {nullptr, nullptr}; {
      std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
      if ( this->m_free.size() == this->m_num_buffers && this->m_shutdown )   {
	this->info("process: Exit condition detected. Leaving submit thread.");
	return DF_SUCCESS;
      }
      else if ( !this->m_todo.empty() )   {
	b = this->m_todo.back();
	this->m_todo.pop_back();
      }
    }
    if ( b.buffer )   {
      long len = b.pointer - b.buffer;
      /// Skip files with less than a minimum bytes
      if ( len > this->m_min_file_size_MB*MBYTE )  {
	if ( this->write_buffer(b) != DF_SUCCESS )   {
	  /// Set the writer into error state and inhibit all further consumption of events
	  std::lock_guard<std::mutex> counterLock(this->m_counter_lock);
	  this->m_bytesDropped += len;
	  ++this->m_writeErrors;
	  this->fireIncident("DAQ_ERROR");
	  this->error("Execute: Failed to write buffer to file. [%s]. Processing will stop.",
		      std::error_condition(errno,std::system_category()).message().c_str());
	  return DF_ERROR;
	}
	std::lock_guard<std::mutex> counterLock(this->m_counter_lock);
	this->m_bytesOut += len;
      }
      else   {
	this->info("Skip mini-file with only %ld bytes",int64_t(len));
	std::lock_guard<std::mutex> counterLock(this->m_counter_lock);
	this->m_bytesDropped += len;
      }
      {
	std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
	b.pointer = b.buffer;
	this->m_free.push_back(b);
      }
      continue;
    }
    if ( this->m_poll_tmo > 0 )  {
      ::lib_rtl_usleep(this->m_poll_tmo);
    }
  }
  return DF_SUCCESS;
}

/// Thread invocation routine to save assembled buffers to the disk server
int StorageWriter::process_buffers()    {
  switch(this->m_output_type_id)  {
  case NETWORK_STORAGE:
    return this->process_network_buffers();
  case ROOT_STORAGE:
    if ( this->m_threadFileQueues )   {
      std::mutex queue_lock;
      std::map<int,std::unique_ptr<ROOT_FILE> > files;
      return this->process_posix_buffers(queue_lock, files);
    }
    return this->process_posix_buffers(this->m_output_lock,this->m_open_root_files);
  case POSIX_STORAGE:
  default:
    if ( this->m_threadFileQueues )   {
      std::mutex queue_lock;
      std::map<int,std::unique_ptr<POSIX_FILE> > files;
      return this->process_posix_buffers(queue_lock, files);
    }
    return this->process_posix_buffers(this->m_output_lock,this->m_open_posix_files);
  }
}

/// Print server's HttpReply structure
void StorageWriter::print_reply(const char* prefix, const http::HttpReply& reply)   const  {
  std::string line;
#if 0
  std::stringstream str;
  reply.print(str,"");
  do {
    getline(str, line);
    error("write_buffer-reply<%s>: %s", prefix, line.c_str());
  } while( str.good() && !str.eof() );
#endif
  switch(reply.status)   {
  case http::HttpReply::ok:
  case http::HttpReply::accepted:
  case http::HttpReply::permanent_redirect:
    line = http::HttpReply::stock_status(reply.status);
    this->info("write_buffer-reply<%s>: %s", prefix, line.c_str());
    break;
    
  case http::HttpReply::internal_server_error:
  case http::HttpReply::not_implemented:
  case http::HttpReply::bad_gateway:
  case http::HttpReply::service_unavailable:
  case http::HttpReply::gateway_timeout:
  case http::HttpReply::conflict:
  case http::HttpReply::unauthorized:
  case http::HttpReply::bad_request:
  case http::HttpReply::no_content:
  default:
    line = http::HttpReply::stock_status(reply.status);
    this->error("write_buffer-reply<%s>: %s", prefix, line.c_str());
    break;
  }
}

/// Write multi event buffer to file. Eventually open a new file....
int StorageWriter::write_buffer(const Buffer& buff)    {
  std::string fname       = this->makeFileName(buff.run_number);
  int         num_retries = this->m_write_error_retry;
  std::size_t len         = buff.pointer - buff.buffer;
  bool        process     = (this->m_cancelled == 0) || (::time(0) - this->m_cancelled < this->m_cancel_tmo);
  http::HttpReply  reply;

  /// Implement here a retry mechanism starting with registering the DB record
  while ( process )  {
    try   {
      std::string url, err;
      client_t cl(this->m_fdb_version);
      storage::uri_t srv(this->m_server);
      cl.fdbclient =
	storage::client::create<storage::client::sync>(srv.host, srv.port, 10000, this->m_debug_client);

      reply = cl.save_object_record(fname, url, len);

      if ( reply.status == reply.permanent_redirect )  {
	/// OK. the registration was now successful. Send the data.
	/// Implement here a retry mechanism sending the data.
	process = (this->m_cancelled == 0) || (::time(0) - this->m_cancelled < this->m_cancel_tmo);
	++this->m_filesOpen;
	++this->m_filesOpened;
	while ( process )  {
	  try   {
	    reply = cl.save_object_data(url, buff.buffer, len);
	    if ( reply.status == reply.created )  {
	      double kB = double(len)/1024e0;
	      void (Component::*prt)(const char*,...) const = &Component::info;
	      if ( this->m_cancelled > 0 ) prt = &Component::warning;
	      ((*this).*prt)("write_buffer: Saved '%s' %.2f %cB", url.c_str(),
			      kB>1e4 ? kB/1024e0 : kB, kB>1e4 ? 'M' : 'k');
	      ++this->m_filesClosed;
	      return DF_SUCCESS;
	    }
	  }
	  catch(const std::exception& e)  {
	    this->error("write_buffer: Exception while sending data: %s",e.what());
	    reply.status = http::HttpReply::bad_gateway;
	  }
	  catch(...)  {
	    this->error("write_buffer: Exception while sending data: UNKNOWN Exception");
	    reply.status = http::HttpReply::bad_gateway;
	  }
	  switch(reply.status)   {
	    /// These are unrecoverable errors. We have to drop the data
	  case http::HttpReply::conflict:
	  case http::HttpReply::forbidden:
	  case http::HttpReply::bad_request:
	  case http::HttpReply::unauthorized:
	  case http::HttpReply::payment_required:
	  case http::HttpReply::insufficient_storage:
	    err = http::HttpReply::stock_status(reply.status);
	    this->error("HTTP: %s: [%d] %s.  DATA LOSS!!!! (2) Need to throw away data to avoid duplicates!",
			fname.c_str(), int(reply.status), err.substr(0,err.find("\r\n")).c_str());
	    return DF_ERROR;

	    /// These are recoverable errors. We can retry....
	  case http::HttpReply::bad_gateway:
	  case http::HttpReply::gateway_timeout:
	  case http::HttpReply::service_unavailable:
	  default:
	    break;
	  }
	  process  = (m_cancelled == 0) || (::time(0) - m_cancelled < m_cancel_tmo);
	  if ( --num_retries <= 0 ) process = false;
	  err = http::HttpReply::stock_status(reply.status);
	  this->warning("%s: retry(2) %d / %d [%s]", fname.c_str(),
			this->m_write_error_retry-num_retries, this->m_write_error_retry,
			err.substr(0,err.find("\r\n")).c_str());
	  ::lib_rtl_sleep(this->m_write_error_sleep);
	}
	this->error("%s DATA LOSS!!!! Failed to transfer data block!", fname.c_str());
	break;
      }
      else  {
	this->print_reply("dbase-server", reply);
      }
    }
    catch(const std::exception& e)  {
      this->error("write_buffer: Exception while connecting: %s",e.what());
      reply.status = http::HttpReply::bad_gateway;
    }
    catch(...)  {
      this->error("write_buffer: Exception while connecting: UNKNOWN Exception");
      reply.status = http::HttpReply::bad_gateway;
    }

    switch(reply.status)   {
    /// These are unrecoverable errors. We have to drop the data
    case http::HttpReply::conflict:
    case http::HttpReply::bad_request:
    case http::HttpReply::unauthorized:
    case http::HttpReply::forbidden:
    case http::HttpReply::payment_required:
      this->error("HTTP: %s DATA LOSS!!!! [%d] %s. (1) Need to throw away data to avoid duplicates!",
		  fname.c_str(), int(reply.status), http::HttpReply::stock_status(reply.status).c_str());
      return DF_ERROR;

    /// These are recoverable errors. We can retry....
    case http::HttpReply::bad_gateway:
    case http::HttpReply::gateway_timeout:
    case http::HttpReply::service_unavailable:
    case http::HttpReply::insufficient_storage:
    default:
      break;
    }
    process  = (this->m_cancelled == 0);
    process |= (::time(0) - this->m_cancelled < this->m_cancel_tmo);
    if ( --num_retries <= 0 ) process = false;
    warning("%s: retry(1) %d / %d [%s]", fname.c_str(),
	    this->m_write_error_retry-num_retries, this->m_write_error_retry,
	    http::HttpReply::stock_status(reply.status).c_str());
    ::lib_rtl_sleep(this->m_write_error_sleep);
  }
  return DF_ERROR;
}

