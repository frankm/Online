//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  PropertyManip.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "PropertyManip.h"
#include <Dataflow/Plugins.h>
#include <Dataflow/DataflowContext.h>
#include <Dataflow/DataflowManager.h>
#include <Dataflow/ComponentFunctors.h>
#include <Dataflow/Incidents.h>
#include <RPC/JSONRPC.h>
#include <RPC/XMLRPC.h>
#include <RTL/rtl.h>
#include <dim/dis.h>

// ROOT include files
#include <TInterpreter.h>
#include <TSystem.h>

// C/C++ include files
#include <iostream>
#include <sstream>
#include <set>

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_PropertyManip,PropertyManip)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_UI,PropertyManip)

using namespace std;
using namespace Online;

/// DIM callback to publish properties
void PropertyManip::feedProperties(void* tag, void** address, int* size, int*)   {
  static string empty = "";
  if ( tag && address && size )  {
    PropertyManip* m = *(PropertyManip**)tag;
    if ( m )   {
      *address = (void*)&m->propertyData[0];
      *size    = m->propertyData.size();
      return;
    }
  }
  *address = (void*)empty.c_str();
  *size    = 0;
}

/// DIM callback to handle commands
void PropertyManip::processRequest(void* tag, void* address, int* size)   {
  if ( tag && address && size )  {
    int len = *size;
    PropertyManip* m = *(PropertyManip**)tag;
    if ( m && len>0 )   {
      m->handleCommand((char*)address, len);
    }
  }
}

/// Initializing constructor
PropertyManip::PropertyManip(const string& nam, Context& ctxt)
  : Component(nam, ctxt)
{
  declareProperty("When", when="initialize");
  declareProperty("DimMemSize", dimMemSize);
  declareProperty("Interfaces", interfaces = "/JSONRPC" );
}

/// Default destructor
PropertyManip::~PropertyManip()   {
}

/// Start all DIM publishing services
void PropertyManip::startServices()  {
  string svc;
  if ( dimCmdID == 0 )  {
    svc = RTL::processName() + "/properties/set";
    dimCmdID = ::dis_add_cmnd(svc.c_str(),"C",processRequest,(long)this);
  }
  if ( dimSvcID == 0 )  {
    svc = RTL::processName() + "/properties/publish";
    dimSvcID = ::dis_add_service(svc.c_str(),"C",0,0,feedProperties,(long)this);
  }
  void (PropertyManip::*set)(const string&,const string&,const string&) = &PropertyManip::setProperty;
  if ( this->interfaces.find("/RPC2") != std::string::npos && !rpc2 )   {
    rpc2.reset(new rpc::DimServer(RTL::processName(), "/RPC2"));
    rpc2->setDebug(true);
    rpc2->define("clients",            xmlrpc::Call(this).make(&PropertyManip::clients));
    rpc2->define("allProperties",      xmlrpc::Call(this).make(&PropertyManip::allProperties));
    rpc2->define("namedProperties",    xmlrpc::Call(this).make(&PropertyManip::namedProperties));
    rpc2->define("clientProperties",   xmlrpc::Call(this).make(&PropertyManip::clientProperties));
    rpc2->define("property",           xmlrpc::Call(this).make(&PropertyManip::property));
    rpc2->define("setPropertyObject",  xmlrpc::Call(this).make(&PropertyManip::setPropertyObject));
    rpc2->define("setProperty",        xmlrpc::Call(this).make(set));
    rpc2->define("setProperties",      xmlrpc::Call(this).make(&PropertyManip::setProperties));
    rpc2->define("setPropertiesByName",xmlrpc::Call(this).make(&PropertyManip::setPropertiesByName));
    rpc2->define("interpreteCommand",  xmlrpc::Call(this).make(&PropertyManip::interpreteCommand));
    rpc2->define("interpreteCommands", xmlrpc::Call(this).make(&PropertyManip::interpreteCommands));
    rpc2->start(true);
  }
  if ( this->interfaces.find("/XMLRPC") != std::string::npos && !xmlrpc )   {
    xmlrpc.reset(new rpc::DimServer(RTL::processName(), "/XMLRPC"));
    xmlrpc->setDebug(true);
    xmlrpc->define("clients",            xmlrpc::Call(this).make(&PropertyManip::clients));
    xmlrpc->define("allProperties",      xmlrpc::Call(this).make(&PropertyManip::allProperties));
    xmlrpc->define("namedProperties",    xmlrpc::Call(this).make(&PropertyManip::namedProperties));
    xmlrpc->define("clientProperties",   xmlrpc::Call(this).make(&PropertyManip::clientProperties));
    xmlrpc->define("property",           xmlrpc::Call(this).make(&PropertyManip::property));
    xmlrpc->define("setPropertyObject",  xmlrpc::Call(this).make(&PropertyManip::setPropertyObject));
    xmlrpc->define("setProperty",        xmlrpc::Call(this).make(set));
    xmlrpc->define("setProperties",      xmlrpc::Call(this).make(&PropertyManip::setProperties));
    xmlrpc->define("setPropertiesByName",xmlrpc::Call(this).make(&PropertyManip::setPropertiesByName));
    xmlrpc->define("interpreteCommand",  xmlrpc::Call(this).make(&PropertyManip::interpreteCommand));
    xmlrpc->define("interpreteCommands", xmlrpc::Call(this).make(&PropertyManip::interpreteCommands));
    xmlrpc->start(true);
  }
  if ( this->interfaces.find("/JSONRPC") != std::string::npos && !jsonrpc )   {
    jsonrpc.reset(new rpc::DimServer(RTL::processName(), "/JSONRPC"));
    jsonrpc->setDebug(true);
    jsonrpc->define("clients",            jsonrpc::Call(this).make(&PropertyManip::clients));
    jsonrpc->define("allProperties",      jsonrpc::Call(this).make(&PropertyManip::allProperties));
    jsonrpc->define("namedProperties",    jsonrpc::Call(this).make(&PropertyManip::namedProperties));
    jsonrpc->define("clientProperties",   jsonrpc::Call(this).make(&PropertyManip::clientProperties));
    jsonrpc->define("property",           jsonrpc::Call(this).make(&PropertyManip::property));
    jsonrpc->define("setPropertyObject",  jsonrpc::Call(this).make(&PropertyManip::setPropertyObject));
    jsonrpc->define("setProperty",        jsonrpc::Call(this).make(set));
    jsonrpc->define("setProperties",      jsonrpc::Call(this).make(&PropertyManip::setProperties));
    jsonrpc->define("setPropertiesByName",jsonrpc::Call(this).make(&PropertyManip::setPropertiesByName));
    jsonrpc->define("interpreteCommand",  jsonrpc::Call(this).make(&PropertyManip::interpreteCommand));
    jsonrpc->define("interpreteCommands", jsonrpc::Call(this).make(&PropertyManip::interpreteCommands));
    jsonrpc->start(true);
  }
  svc = RTL::processName();
  ::dis_start_serving(svc.c_str());
}

/// Start all DIM publishing services
void PropertyManip::stopServices()  {
  string svc;
  rpc2.reset();
  xmlrpc.reset();
  jsonrpc.reset();
  if ( dimCmdID != 0 )  {
    ::dis_remove_service(dimCmdID);
    dimCmdID = 0;
  }
  if ( dimSvcID != 0 )  {
    ::dis_remove_service(dimSvcID);
    dimSvcID = 0;
  }
}

/// Initialize the property publisher
int PropertyManip::initialize()  {
  int sc = Component::initialize();
  subscribeIncident("DAQ_RUNNING");
  if ( sc == DF_SUCCESS && when == "initialize" )
    startServices();
  return sc;
}

/// Start the property publisher
int PropertyManip::start()  {
  int sc = Component::start();
  if ( sc == DF_SUCCESS && when == "start" )
    startServices();
  return sc;
}

/// Stop the property publisher
int PropertyManip::stop()  {
  unsubscribeIncidents();
  if ( dimSvcID != 0 && when == "start" )
    stopServices();
  return Component::stop();
}

/// Finalize the property publisher
int PropertyManip::finalize()  {
  unsubscribeIncidents();
  if ( dimSvcID != 0 && when == "initialize" )
    stopServices();
  return Component::finalize();
}

/// Incident handler callback: Inform that a new incident has occured
void PropertyManip::handle(const DataflowIncident& inc)   {
  if ( inc.type == "DAQ_RUNNING" ) {
    publishProperties();
  }
  else if ( inc.type == "DAQ_STOPPED" ) {
  }
}

/// Forced update of the properties
int PropertyManip::updateProperties()    {
  vector<Component*> comp = components();
  vector<char> data;
  string value;

  data.reserve(dimMemSize);
  for(const Component* c : comp )   {
    const auto& props = c->properties.properties();
    for(const auto& p : props)  {
      value = c->name + "/" + p.first + " " + p.second.str();
      std::copy(value.begin(),value.end(),back_inserter(data));
      data.push_back(0);
    }
  }
  propertyData = std::move(data);
  return DF_SUCCESS;
}

/// Publish property collection to DIM in WinCCOA format
void PropertyManip::publishProperties()    {
  if ( 0 != dimSvcID )   {
    lock_guard<mutex> lck(lock);
    updateProperties();
    ::dis_update_service(dimSvcID);
  }
}

/// Handle command
void PropertyManip::handleCommand(char* address, size_t len)   {
  string cmd = address;
  if ( cmd == "setProperties" )   {
    size_t cmd_len = cmd.length()+1;
    WinCCOASetProperties(address+cmd_len, len-cmd_len);
    publishProperties();
  }
  else if ( cmd.substr(0,::strlen("publishProperties")) == "publishProperties" )   {
    publishProperties();
  }
  else  {
    error("Received unknown DIM command: %s",cmd.c_str());
  }
}

/// Handle manipulation of properties for WinCCOA
void PropertyManip::WinCCOASetProperties(char* address, size_t len)   {
  lock_guard<mutex> lck(lock);
  char* p = address, *e = p+len, *n, *v, *c;
  while(p < e)  {
    c = p;
    n = ::strchr(c,'/');
    if ( n )  {
      *n = 0;
      ++n;
      v = ::strchr(n,' ');
      if ( v ) {
	*v = 0;
	++v;
	p = v + ::strlen(v) + 1;
	if ( setPropertyEx(c, n, v) == DF_SUCCESS )   {
	  info("Successfully updated option: %s.%s = %s",c,n,v);
	  continue;
	}
	error("Failed to update option: %s.%s = %s",c,n,v);
      }
    }
    else   {
      error("Failed to update option: %s  [Bad formatted command. Unknwon failure]",c);
      return;
    }
  }
}

/// Update a given component property from the string representation
int PropertyManip::setProperty(Property& property, const string& value)   {
  property.str(value);
  return DF_SUCCESS;
}

/// Update a given component property from the string representation
int PropertyManip::setProperty(Component* component, const string& property, const string& value)   {
  auto& props = component->properties.properties();
  for( auto& p : props )  {
    if ( p.first == property )  {
      return setProperty(p.second, value);
    }
  }
  return DF_ERROR;
}

/// Update a given component property from the string representation
int PropertyManip::setPropertyEx(const string& component, const string& property, const string& value)   {
  SelectComponentByName selector(component);
  DataflowManager& mgr = context.manager.manager();
  if ( -1 != mgr.for_each(selector) )
    return setProperty(selector.selected, property, value);
  return DF_ERROR;
}

/// Collect all known components
vector<DataflowComponent*> PropertyManip::components()  const   {
  vector<DataflowComponent*> comp;
  DataflowManager& mgr = context.manager.manager();
  auto sel = selectComponent(comp,DefaultCriterium());
  mgr.for_each(sel);
  return comp;
}

/// Collect all known components, order them by name
map<string,DataflowComponent*> PropertyManip::ordered_components()  const   {
  map<string,DataflowComponent*> ret;
  vector<DataflowComponent*> comp = components();
  info("PropertyManip::ordered_components: Got %ld property clients",comp.size());
  for(auto* c : comp)
    ret.insert(make_pair(c->name,c));
  for(const auto& c : ret)
    info("RET: PropertyManip::ordered_components: %s -- %s",c.first.c_str(),c.second->name.c_str());
  return ret;
}

/// Access the hosted client services
PropertyManip::Clients PropertyManip::clients()  const   {
  map<string,DataflowComponent*> comp = ordered_components();
  Clients ret;
  ret.reserve(comp.size());
  for(const auto& c : comp)
    ret.push_back(c.first);

  for(const auto& c : ret)
    info("RET: PropertyManip::clients: %s",c.c_str());
  return ret;
}

/// Access all properties of an object
PropertyManip::Properties PropertyManip::allProperties()  const   {
  Properties props;
  map<string,DataflowComponent*> comp = ordered_components();
  for(const auto& c : comp)  {
    for(const auto& p : c.second->properties.properties())
      props.push_back(rpc::ObjectProperty(c.first,p.first,p.second.str()));
  }
  return props;
}

/// Access all properties with the same name from all clients
PropertyManip::Properties PropertyManip::namedProperties(const string& nam)  const   {
  Properties props;
  map<string,DataflowComponent*> comp = ordered_components();
  for(const auto& c : comp)  {
    for(const auto& p : c.second->properties.properties())  {
      if ( p.first == nam)  {
	props.push_back(rpc::ObjectProperty(c.first,p.first,p.second.str()));
      }
    }
  }
  return props;
}

/// Access all properties of one remote client (service, ect.)
PropertyManip::Properties PropertyManip::clientProperties(const string& cl)  const   {
  SelectComponentByName selector(cl);
  DataflowManager& mgr = context.manager.manager();
  if ( -1 != mgr.for_each(selector) )  {
    Properties props;
    for(const auto& p : selector.selected->properties.properties())  {
      props.push_back(rpc::ObjectProperty(selector.selected->name,p.first,p.second.str()));
    }
    return props;
  }
  throw runtime_error(RTL::processName()+".clientProperties: Unknown client: "+cl);
}

/// Access a single property of an object
rpc::ObjectProperty PropertyManip::property(const string& cl,
					    const string& nam)  const   {
  SelectComponentByName sel(cl);
  DataflowManager& mgr = context.manager.manager();
  if ( -1 != mgr.for_each(sel) && sel.selected )  {
    for(const auto& p : sel.selected->properties.properties())  {
      if ( p.first == nam)  {
	return rpc::ObjectProperty(sel.selected->name,p.first,p.second.str());
      }
    }
  }
  throw runtime_error(RTL::processName()+".property: Unknown property: "+cl+"."+nam);
}

/// Modify a property
void PropertyManip::setProperty(const string& cl,
				const string& nam,
				const string& value)   {
  SelectComponentByName sel(cl);
  DataflowManager& mgr = context.manager.manager();
  if ( -1 != mgr.for_each(sel) && sel.selected )  {
    setProperty(sel.selected, nam, value);
    publishProperties();
    return;
  }
  throw runtime_error(RTL::processName()+".setProperty: Unknown property: "+cl+"."+nam);
}

/// Modify a property
void PropertyManip::setPropertyObject(const rpc::ObjectProperty& prop)  {
  setProperty(prop.client,prop.name,prop.value);
}

/// Modify all properties in allclients matching the name
int  PropertyManip::setPropertiesByName(const std::string& nam,
					const std::string& val)   {
  vector<Component*> comp = components();
  int count = 0;
  for(auto c : comp)  {
    auto& p = c->properties.properties();
    auto ip = p.find(nam);
    if ( ip != p.end() )  {
      setProperty((*ip).second, val);
      ++count;
    }
  }
  return count;
}

/// Modify a whole bunch of properties. Call returns the number of changes
int PropertyManip::setProperties(const Properties& props)   {
  set<string> miss_clients;
  map<string,Component*> update_clients;
  vector<Component*> comp = components();
  stringstream err;
  int count = 0;

  err << RTL::processName() << ".setProperties: ";
  for(const auto& p : props)  {
    bool found = false;
    for(auto c : comp)  {
      if ( c->name == p.client )  {
	update_clients.insert(make_pair(c->name,c));
	found = true;
	break;
      }
    }
    if ( !found ) miss_clients.insert(p.client);
  }
  for(const auto& c : miss_clients)
    err << "Options for client: " << c << " were not updated." << endl;
  for(const auto& c : update_clients)   {
    for(const auto& p : props)  {
      if ( p.client == c.first )  {
	if ( setProperty(c.second, p.name, p.value) == DF_SUCCESS )  {
	  ++count;
	  continue;
	}
	err << "Option update " << c.first << "." << p.name << " FAILED." << endl;
      }
    }
  }
  if ( count != (long)props.size() )   {
    throw runtime_error(err.str());
  }
  publishProperties();
  return count;
}

/// Invoke the interpreter and interprete the command line
int PropertyManip::interpreteCommand(const string& cmd)   const   {
  if ( !cmd.empty() )   {
  }
  return 1;
}

/// Invoke the interpreter and interprete the command lines
int PropertyManip::interpreteCommands(const vector<string>& cmd)   const  {
  if ( !cmd.empty() )   {
  }
  return 1;
}
