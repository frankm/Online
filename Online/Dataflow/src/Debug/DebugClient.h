//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  Fifo Logger implementation
//--------------------------------------------------------------------------
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ONLINE_DEBUGCLIENT_H
#define ONLINE_DEBUGCLIENT_H

/// C/C++ include files
#include <memory>

/// Debugger namespace declaration
namespace debugger  {

  class DebugClient   {
  public:
    class implementation;
    std::unique_ptr<implementation> imp;
  public:
    /// Default constructor
    DebugClient();
    /// Default destructor
    ~DebugClient();
  };
}
#endif // ONLINE_DEBUGCLIENT_H
