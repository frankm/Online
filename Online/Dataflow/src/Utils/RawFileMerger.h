//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Filemerger.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_RAWFILEMERGER_H
#define ONLINE_DATAFLOW_RAWFILEMERGER_H

// Framework include files

// C/C++ include files
#include <string>
#include <memory>
#include <limits>

///  Online namespace declaration
namespace Online  {

  /// RawFileMerger component to be applied in the manager sequence
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class RawFileMerger  {
  public:
    
    enum OutputType   {
      PCIE40,
      TELL1
    };
    std::string outputName    {       };
    OutputType  outputType    { TELL1 };
    size_t      packing       {     1 };
    size_t      max_file_size { std::numeric_limits<size_t>::max() };
    size_t      max_events    { std::numeric_limits<size_t>::max() };
    bool        write_many    { false };

  protected:
    class event_mixer;
    class event_output;

    typedef std::unique_ptr<event_mixer>  mixer_t;
    typedef std::unique_ptr<event_output> output_t;
    mixer_t     mixer       {       };
    output_t    output      {       };
    int         out_seq_no  { 0     };

    /// Merge a maximum of 'max_events' events to the output file in TELL1 format
    int merge_tell1 (size_t max_events = ~0x0UL);
    /// Merge a maximum of 'max_events' events to the output file in PCIE40 format
    int merge_pcie40(size_t max_events = ~0x0UL);
    /// Open new output file
    int open_output();

  public:
    //// Default constructor
    RawFileMerger() = default;
    /// Initializing constructor
    RawFileMerger(const char* output_name, OutputType output_type);
    /// Default destructor
    virtual ~RawFileMerger();
    /// Start merge cycle: open output file
    int begin();
    /// Start merge cycle: open output file
    int begin(const char* output_name, OutputType output_type);
    /// Add new input source to the merger object
    size_t add(const char* file_name);
    /// Merge a maximum of 'max_events' events to the output file 
    int merge(size_t max_events = ~0x0UL);
    /// End the file merging cycle and close the output file
    int end();
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_RAWFILEMERGER_H
