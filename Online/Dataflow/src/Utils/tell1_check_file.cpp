//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Tell1Data/RawFile.h>
#include <Tell1Data/Tell1Decoder.h>
#include <RZip.h>

// C/C++ include files
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <vector>
#include <set>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

using namespace std;
using namespace Online;

namespace {
  static constexpr int peek_size = int(8*sizeof(int));

  typedef pair<long, unsigned char*> MemBuffer;

  const char* get_arg(const char* arg)   {
    const char* p = ::strchr(arg,'=');
    if ( p ) return p+1;
    throw runtime_error(string("Invalid argument: ")+arg);
  }

  long load_mdf_event(RawFile&     file,
		      pair<long,unsigned char*>  output,
		      pair<long,unsigned char*>& decomp )
  {
    auto*  mdf_hdr   = (EventHeader*)output.second;
    long   status    = 0;
    int    compress  = mdf_hdr->compression();
    long   data_size = mdf_hdr->recordSize();
    if ( compress > 0 )   {
      int  new_size  = 0;
      int  tgt_size  = 0;
      int  src_size  = data_size;
      long hdr_len   = mdf_hdr->sizeOf(mdf_hdr->headerVersion());

      if ( decomp.first < data_size )   {
	decomp.first  = data_size*1.2;
	if ( decomp.second ) delete [] decomp.second;
	decomp.second = new unsigned char[decomp.first];
      }
      ::memcpy(decomp.second, output.second, peek_size);
      status = file.read(decomp.second + peek_size, data_size - peek_size);
      if ( status < data_size - peek_size )   {
	return -1;
      }
      if ( hdr_len-peek_size > 0 )   {
	::memcpy(output.second, decomp.second, hdr_len);
      }
      if ( 0 == ::R__unzip_header(&src_size, decomp.second + hdr_len, &tgt_size) )   {
	if ( output.first < (long)tgt_size )   {
	  return 0;
	}
	::R__unzip( &src_size, decomp.second + hdr_len,
		    &tgt_size, output.second + hdr_len,
		    &new_size);
	if ( new_size > 0 )   {
	  EventHeader* hdr = (EventHeader*)output.second;
	  hdr->setHeaderVersion( 3 );
	  hdr->setDataType(EventHeader::BODY_TYPE_BANKS);
	  hdr->setSubheaderLength(sizeof(EventHeader::Header1));
	  hdr->setSize(new_size);
	  hdr->setCompression(0);
	  hdr->setChecksum(0);
	  hdr->setSpare(0);
	  return hdr_len + new_size;
	}
      }
      return -1;
    }
    if ( output.first < data_size )   {
      return 0;
    }
    status = file.read(output.second + peek_size, data_size - peek_size);
    if ( status < data_size - peek_size )   {
      return -1;
    }
    return data_size;
  }

  void _read_dir(vector<string>& inputs, const char* fn)   {
    struct stat stat;
    if ( 0 == ::stat(fn, &stat) )   {
      switch (stat.st_mode & S_IFMT) {
      case S_IFDIR:  {
	cout << "+++ Use directory content: " << fn << endl;
	auto* dir = ::opendir(fn);
	dirent* dent;
        while((dent=readdir(dir))!=NULL)   {
	  if ( strcmp(dent->d_name,".") == 0 ) continue;
	  if ( strcmp(dent->d_name,"..") == 0 ) continue;
	  string f = fn;
	  f += "/";
	  f +=  dent->d_name;
	  _read_dir(inputs, f.c_str());
	}
	::closedir(dir);
	break;
      }
      default:
	cout << "+++ Use input file:        " << fn << endl;
	inputs.push_back(fn);
	break;
      }
    }
  }
}

extern "C" int tell1_check_file(int argc, char* argv[])    {
  class help  {
  public:
    static void show()  {
      cout <<
	"Usage: tell1_check_file -option [-option]                                \n"
	"  --input=<file>      input file name  (multiple entries allowed)        \n"
	"  -i <file>           dto. If a directory is given all files are scanned \n"
		<< endl;
    }
  };
  vector<string> inputs;
  try   {
    for(int i = 1; i < argc && argv[i]; ++i)  {
      if ( 0 == ::strncasecmp("--input",argv[i],4) )   {
	inputs.push_back(get_arg(argv[i]));
      }
      else if ( 0 == ::strcasecmp("-i",argv[i]) )   {
	inputs.push_back(argv[++i]);
      }
      else   {
	throw runtime_error(string("Invalid argument: ")+argv[i]);
      }
    }
  }
  catch(const exception& e)   {
    cout << "Exception: " << e.what() << endl << endl;
    help::show();
    return EINVAL;
  }
  if ( inputs.empty() )   {
    cout << "No input files given!" << endl << endl;
    help::show();
    return ENOENT;
  }
  auto files = inputs;
  inputs.clear();
  for(size_t i=0; i< files.size(); ++i)
    _read_dir(inputs, files[i].c_str());

  size_t  num_evt_total = 0;
  size_t  num_bytes_total = 0;
  clock_t start, end;

  start = clock();
  for(size_t i=0; i< inputs.size(); ++i)   {
    Online::RawFile input(inputs[i]);
    if ( input.open() < 0 )  {
      cout << "FAILED to open file: " << inputs[i] << endl;
      continue;
    }
    cout << "+++ Opened file       " << inputs[i] << endl;
    cout << "+++ File size on disk " << input.data_size() << " bytes " << endl;

    long num_evt_file = 0;
    long num_bytes_file = 0;
    MemBuffer deCompress;
    while(1)   {
      int      size[peek_size/sizeof(int)];
      uint8_t *data_ptr, *alloc_ptr;
      off_t    position = input.position();
      int      status   = input.read(size, peek_size);

      auto* mdf_hdr = (EventHeader*)size;
      if ( status < int(peek_size) )   {
	goto Done;
      }
      if ( mdf_hdr->is_mdf() )   {
	int  compress   = mdf_hdr->compression();
	long hdr_len    = mdf_hdr->sizeOf(mdf_hdr->headerVersion());
	long alloc_size = mdf_hdr->recordSize();
	++num_evt_file;
	++num_evt_total;
	if ( compress > 0 )   {
	  alloc_size = hdr_len + (mdf_hdr->compression() + 1) * mdf_hdr->size();
	}
	alloc_ptr = data_ptr = new uint8_t[alloc_size];
	::memcpy(data_ptr, size, peek_size);
	long length = load_mdf_event(input, MemBuffer(alloc_size,data_ptr), deCompress);
	if ( length <= 0 )   {
	  delete [] alloc_ptr;
	  goto Done;
	}
	num_bytes_file  += alloc_size;
	num_bytes_total += alloc_size;
	try  {
	  auto* hdr = (EventHeader*)data_ptr;
	  /// Check raw bank record
	  checkRawBanks(hdr->data(), hdr->end(), true, true);
	}
	catch(const exception& e)   {
	  cout << "+++ MDF Exception:  " << e.what() << endl;
	  cout << "    File position:" << position << endl;
	}
	catch(...)   {
	  cout << "+++ UNKNOWN MDF Exception  " << endl;
	  cout << "    File position:" << position << endl;
	}
	delete [] alloc_ptr;
      }
      else   {
	cout << "+++ INVALID MDF Header! " << inputs[i] << ". Read a total of " << num_bytes_file << " bytes" << endl;
	cout << "    File position:" << position << endl;
	break;
      }
      continue;

    Done:
      cout << "+++ End of file. Read " << num_bytes_file << " bytes with a total of " << num_evt_file << " events" << endl;
      break;
    }
  }
  end = clock();
  cout << "+++ " <<  num_evt_total << " Events Processed with " << num_bytes_total << " bytes  "
       << "Time (ticks): " << long(end-start) << " (total) " << long(end-start)/num_evt_total << " (ticks/event)" << endl;
  return 0;
}
