//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <PCIE40Data/pcie40decoder.h>
#include <Tell1Data/RawFile.h>

// C/C++ include files
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <vector>
#include <set>

using namespace std;
namespace pcie40 = Online::pcie40;

using namespace Online;

namespace {
  const char* get_arg(const char* arg)   {
    const char* p = ::strchr(arg,'=');
    if ( p ) return p+1;
    throw runtime_error(string("Invalid argument: ")+arg);
  }

  bool prompt()   {
  re_check:
    cout << "====>  Continue to dump the next event  [q,Q to quit]: " << flush;
  re_prompt:
    char c = std::getchar();
    if ( c == -1  ) goto re_prompt;
    if ( c == 10  ) return true;  // <ENTER>
    if ( c == 'q' ) return false;
    if ( c == 'Q' ) return false;
    goto re_check;
  }
}

extern "C" int pcie_decode_file(int argc, char* argv[])    {
  class help  {
  public:
    static void show()  {
      std::cout <<
	"Usage: pcie_decode_file -option [-option]                         \n"
	"  --input=<file>      input file name  (multiple entries allowed)   \n"
	"  -i <file>           dto.                                          \n"
	"  --dump=meps         Dump MEP headers                              \n"
	"  --dump=events       Dump Event headers                            \n"
	"  --dump=collections  Dump bank collection summaries                \n"
	"  --dump=banks        Dump bank headers                             \n"
	"  -d <item>           dto.                                          \n"
	"  --break             Break before next chunk of printout           \n"
	"  -b                  dto.                                          \n"
		<< std::endl;
    }
  };
  bool do_break = false;
  bool dump_meps = false;
  bool dump_events = false;
  bool dump_bank_headers = false;
  bool dump_bank_collections = false;
  std::vector<std::string> inputs;
  try   {
    for(int i = 1; i < argc && argv[i]; ++i)  {
      if ( 0 == ::strncasecmp("--input",argv[i],4) )   {
	inputs.push_back(get_arg(argv[i]));
      }
      else if ( 0 == ::strcasecmp("-i",argv[i]) )   {
	inputs.push_back(argv[++i]);
      }
      else if ( 0 == ::strncasecmp("--break",argv[i],4) )   {
	do_break = true;
      }
      else if ( 0 == ::strcasecmp("-b",argv[i]) )   {
	do_break = true;
      }
      else if ( 0 == ::strncasecmp("--dump",argv[i],4) )    {
	if ( strncmp(get_arg(argv[i]), "meps", 3) == 0 )   {
	  dump_meps = true;
	}
	else if ( strncmp(get_arg(argv[i]), "events", 3) == 0 )   {
	  dump_events = true;
	  dump_meps = true;
	}
	else if ( strncmp(get_arg(argv[i]), "collections", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_events = true;
	  dump_meps = true;
	}
	else if ( strncmp(get_arg(argv[i]), "banks", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_bank_headers = true;
	  dump_events = true;
	  dump_meps = true;
	}
      }
      else if ( 0 == ::strncasecmp("-d",argv[i],4) )  {
	const char* a2 = argv[++i];
	if ( strncmp(a2, "meps", 3) == 0 )   {
	  dump_meps = true;
	}
	else if ( strncmp(a2, "events", 3) == 0 )   {
	  dump_events = true;
	  dump_meps = true;
	}
	else if ( strncmp(a2, "collections", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_events = true;
	  dump_meps = true;
	}
	else if ( strncmp(a2, "banks", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_bank_headers = true;
	  dump_events = true;
	  dump_meps = true;
	}
      }
      else   {
	throw runtime_error(string("Invalid argument: ")+argv[i]);
      }
    }
  }
  catch(const std::exception& e)   {
    cout << "Exception: " << e.what() << endl << endl;
    help::show();
    return EINVAL;
  }
  if ( inputs.empty() )   {
    cout << "No input files given!" << endl << endl;
    help::show();
    return ENOENT;
  }

  size_t            n_evt = 0;
  clock_t           start, end;
  pcie40::printer_t printer;
  pcie40::decoder_t decoder;
  std::unique_ptr<pcie40::event_collection_t> coll;

  start = clock();
  size_t n_mep = 0;
  size_t bufflen = 0;
  unsigned char* buff = nullptr;
  for(size_t i=0; i< inputs.size(); ++i)   {
    size_t total = 0;
    Online::RawFile input(inputs[i]);
    if ( input.open() < 0 )  {
      cout << "FAILED to open file: " << inputs[i] << endl;
      continue;
    }
    cout << "+++ Successfully opened file: " << inputs[i] << endl;
    while(1)   {
      pcie40::mep_header_t hdr;
      size_t len = input.read(&hdr, sizeof(hdr));
      if ( len == sizeof(hdr) )   {
	size_t rec_len = hdr.size*sizeof(uint32_t);
	total += len;
	if ( bufflen < rec_len )  {
	  bufflen = rec_len;
	  if ( buff ) delete [] buff;
	  buff = new unsigned char[bufflen];
	}
	::memcpy(buff, &hdr, sizeof(hdr));
	len = input.read(buff + sizeof(hdr), rec_len - sizeof(hdr));
	if ( len == rec_len - sizeof(hdr) )   {
	  char text[512];
	  total += len;
	  const pcie40::mep_header_t* mep = (pcie40::mep_header_t*)buff;
	  if ( !mep->is_valid() ) break;
	  const pcie40::multi_fragment_t *mfp = mep->multi_fragment(0);
	  uint16_t packing = mfp->header.packing;
	  n_evt += packing;
	  if ( dump_events )   {
	    cout << "+=========================================================================+" << endl;
	  }
	  if ( dump_meps )   {
	    ::snprintf(text,sizeof(text),
		       "+++ Reading mep[%04ld] at %p length:%7d end: %p  packing:%8d  events:%7ld", 
		       n_mep, (void*)mep, mep->size, (void*)mep->next(), int(packing), n_evt);
	    cout << text << endl;
	  }
	  coll.reset();
	  //   coll_reset ? coll.reset() : (void)decoder.initialize(coll, packing);
	  decoder.decode(coll, mep);
	  if ( dump_events )    {
	    size_t count = 0;
	    if ( dump_meps )   {
	      cout << "+ Got MEP with " << coll->size() << " events." << endl;
	    }
	    for(const auto* e=coll->begin(); e != coll->end(); e = coll->next(e), ++count)   {
	      if ( dump_bank_collections )   {
		cout << "+ ==>  ===================================================================+" << endl;
	      }
	      cout << "+ ==>  Event No. " << count << "  " << e->total_length() << " bytes" << endl;
	      for(size_t ie=0, ne=e->num_bank_collections(); ie<ne; ++ie)   {
		std::map<int,size_t> normal_types, special_types;
		const auto* bc = e->bank_collection(ie);
		size_t normal_data_len  = 0;
		size_t special_data_len = 0;
		for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))  {
		  normal_data_len += b->size();
		  normal_types[b->type()] += 1;
		}
		for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))  {
		  special_data_len += b->size();
		  special_types[b->type()] += 1;
		}
		if ( dump_bank_collections )   {
		  cout << "++====>  Bank collection: "  << ie << "  "
		       << setw(3) << bc->num_banks()    << " / "
		       << setw(3) << bc->num_special()  << " banks  "
		       << setw(5) << bc->total_length() << " bytes  "
		       << endl;
		  if ( bc->num_banks() > 0 )   {
		    cout << "+        Data:   "  << setw(6) << normal_data_len << " bytes  Types: ";
		    for(const auto& in : normal_types) cout << in.first << ":" << in.second << "  ";
		    cout << endl;
		    if ( dump_bank_headers )   {
		      for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))
			cout << "+++       " << *b << endl;
		    }
		  }
		  if ( bc->num_special() > 0 )   {
		    cout << "+        Special:" << setw(6) << special_data_len << " bytes  Types: ";
		    for(const auto& is : special_types) cout << is.first << ":" << is.second << "  ";
		    cout << endl;
		    if ( dump_bank_headers )   {
		      for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))
			cout << "+++====>  " << *b << endl;
		    }
		  }
		}
		if ( dump_bank_headers && do_break )		/// Break in interactive running
		  if ( !prompt() ) goto Done;
	      }
	      if ( dump_bank_collections && do_break )		/// Break in interactive running
		if ( !prompt() ) goto Done;
	    }
	  }
	  if ( dump_events && do_break )		/// Break in interactive running
	    if ( !prompt() ) goto Done;
	  n_mep++;
	  continue;
	}
      }
    Done:
      cout << "+++ End of file " << inputs[i] << ". Read a total of " << total << " bytes" << endl;
      break;
    }
  }
  if ( buff ) delete [] buff;
  end = clock();
  printf(" %ld Events Processed: Fill all banks      %8ld %8ld ticks/event\n",
	 n_evt, long(end-start), long(end-start)/n_evt);
  printer.print_summary(coll);
  return 0;
}
