#!/bin/bash
#*****************************************************************************\
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
#                                                                             *
# This software is distributed under the terms of the GNU General Public      *
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
#                                                                             *
# In applying this licence, CERN does not waive the privileges and immunities *
# granted to it by virtue of its status as an Intergovernmental Organization  *
# or submit itself to any jurisdiction.                                       *
#*****************************************************************************/
. /group/online/dataflow/scripts/preamble.sh

export INFO_OPTIONS=${DYNAMIC_OPTS}/${PARTITION_NAME}_Info.opts;
export SUBFARM_OPTIONS=${DYNAMIC_OPTS}/${PARTITION_NAME}_${SF}_HLT.opts;

get_part_id()  {
pid_cmd=`python <<EOF-OF-PYTHON
import os, sys, socket

match = os.environ['PARTITION_NAME']
match += '_'
match += socket.gethostname().split('.')[0].upper()
lines = os.popen("gentest libOnlineBase.so mbm_summary | grep "+match).readlines()
pid = None
if lines and len(lines):
  for i in lines:
    pid = i.split()[1]
  if pid is not None:
    print "export PARTITION_ID=%s;"%(str(pid),)
    sys.exit(0)
print 'echo "FAILED to determine partition ID from buffer tasks. Match=%s";'%(match,)
for i in lines: print 'echo "'+i[:-1]+'";'
sys.exit(2)
EOF-OF-PYTHON`
#
eval ${pid_cmd}
echo "eval ${pid_cmd}   ${PARTITION_ID}"
}

runit()  {
cat >/tmp/${USER}_feedMBM.opts <<END-OF-OPTS
#pragma print off
#include "$INFO_OPTIONS"
#include "$MBM_SETUP_OPTIONS"
#include "$FARMCONFIGROOT/options/Monitoring.opts"

Manager.Services            = {"Dataflow_MBMClient/MEPManager",
            "Dataflow_HltReader/Reader",
            "Dataflow_RunableWrapper/Wrap"
                    };
Manager.Runable             = "Wrap";
Wrap.Callable               = "Reader";
Reader.Buffer               = "Events";
Reader.BrokenHosts          = "";
Reader.Directories          = {"/localdisk1/hlt1", "/localdisk2/hlt1"};
Reader.AllowedRuns          = { "*" };
Reader.DeleteFiles          = false;
Reader.SaveRest             = false;
Reader.FilePrefix           = "Run_";
Reader.PauseSleep           = 5;  // Optional wait time until 'Output' event queue is empty
Reader.InitialSleep         = 1;
Reader.MaxPauseWait         = 1;
Reader.GoService            = "";
Reader.CheckedBuffers       = {};
//
MEPManager.PartitionBuffers = true;
MEPManager.PartitionName    = ${PARTITION_NAME};
MEPManager.PartitionID      = ${PARTITION_ID};
MEPManager.Buffers          = {"Events"};
Logger.OutputLevel          = 1;
END-OF-OPTS
#
exit 0
#
#
#
#
gentest libDataflow.so dataflow_run_task \
-msg=Dataflow_FmcLogger \
-mon=Dataflow_DIMMonitoring \
-class Class2 -opt=/tmp/${USER}_feedMBM.opts | less;
rm /tmp/${USER}_feedMBM.opts;
}

if test -f /tmp/${USER}_feedMBM.opts;
    then
    rm -f /tmp/${USER}_feedMBM.opts;
fi;
if test -z "${1}";
    then 
    echo "Usage: `basename $0`  <data-directory>";
else
    runit $*
fi;
