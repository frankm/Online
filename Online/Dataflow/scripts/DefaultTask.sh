#!/bin/bash
#*****************************************************************************\
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
#                                                                             *
# This software is distributed under the terms of the GNU General Public      *
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
#                                                                             *
# In applying this licence, CERN does not waive the privileges and immunities *
# granted to it by virtue of its status as an Intergovernmental Organization  *
# or submit itself to any jurisdiction.                                       *
#*****************************************************************************/
#
export CMTCONFIG=x86_64-slc6-gcc49-do0;
cd /home/frankm/cmtuser/OnlineDev_v5r31;
. ./setup.${CMTCONFIG}.vars;
cd ${DATAFLOWROOT}/scripts;
export INFO_OPTIONS=/group/online/dataflow/options/MSF/MSF_Info.opts;
export SUBFARM_OPTIONS=${FARMCONFIGROOT}/options/Empty.opts;
#
# Use the data network for the data transfers....
export TAN_PORT=YES;
export TAN_NODE=`hostname -s`;
export DATAINTEFACE=`hostname -s`;
export DATAINTERFACE=`python /group/online/dataflow/scripts/getDataInterface.py`
export TAN_PORT=YES
export TAN_NODE=${DATAINTERFACE}
#
export MSF_DataSink1="MONA1002::${PARTITION_NAME}_MONA1002_MSFRcv_0";
export MSF_DataSink2="MONA1003::${PARTITION_NAME}_MONA1003_MSFRcv_0";
export MSF_DataSink3="MONA1004::${PARTITION_NAME}_MONA1004_MSFRcv_0";
#
export READER_NODE_OPTIONS=/group/online/dataflow/cmtuser/DQ/DATAQUALITY_OPT/ReaderInput.opts;
#
WORKERS=`echo ${DATAINTEFACE} | cut -b 1-7`;
if test "${PARTITION_NAME}" = "MSF" -a "${DATAINTEFACE}" = "mona1001";then
    export MBM_CONFIG_OPTS=${DATAQUALITYROOT}/options/MSFReaderMBM.opts;
    export MBM_SETUP_OPTIONS=/group/online/dataflow/architectures/lbDataflowArch_MSFReader_MBM_setup.opts;
elif test "${PARTITION_NAME}" = "MSF" -a "${WORKERS}" = "mona100";then
    export MBM_CONFIG_OPTS=${DATAQUALITYROOT}/options/MSFWorkerMBM.opts;
    export MBM_SETUP_OPTIONS=/group/online/dataflow/architectures/lbDataflowArch_MSFWorker_MBM_setup.opts;
fi;
#
export DATAFLOW_task="gentest.exe libDataflow.so dataflow_run_task -msg=Dataflow_FmcLogger -mon=Dataflow_DIMMonitoring"
#
if test -f ./${TASK_TYPE}.sh;
then
    . ./${TASK_TYPE}.sh $*;
else
    exec -a ${UTGID} ${Class1_task} -opts=../options/${TASK_TYPE}.opts;
fi;
