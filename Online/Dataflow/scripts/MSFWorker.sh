#!/bin/bash
#*****************************************************************************\
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
#                                                                             *
# This software is distributed under the terms of the GNU General Public      *
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
#                                                                             *
# In applying this licence, CERN does not waive the privileges and immunities *
# granted to it by virtue of its status as an Intergovernmental Organization  *
# or submit itself to any jurisdiction.                                       *
#*****************************************************************************/
export Checkpoint_task="GaudiCheckpoint.exe libGaudiOnline.so OnlineTask -msgsvc=$msg_svc -tasktype=LHCb::Class1Task -main=../options/Main.opts "
export TASK_TYPE=MSFWorker
. /group/online/dataflow/cmtuser/OnlineRelease/Online/FarmConfig/job/PassThrough.sh
