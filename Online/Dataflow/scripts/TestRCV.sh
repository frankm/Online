#*****************************************************************************\
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
#                                                                             *
# This software is distributed under the terms of the GNU General Public      *
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
#                                                                             *
# In applying this licence, CERN does not waive the privileges and immunities *
# granted to it by virtue of its status as an Intergovernmental Organization  *
# or submit itself to any jurisdiction.                                       *
#*****************************************************************************/
#. setup.x86_64-centos7-gcc62-do0.vars;
export DATAINTERFACE=$HOST
export TAN_NODE=mona0801
export TAN_PORT=YES
export DATAFLOW_task="gentest.exe libDataflow.so dataflow_run_task -class=Class2 -mon=Dataflow_DIMMonitoring -opt=TestRCV.opts -auto"

export UTGID=RCV_$1;
${DATAFLOW_task};

