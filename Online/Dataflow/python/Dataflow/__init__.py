#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#
#  Module initialization script for dataflow
#
#  \author   M.Frank
#  \version  1.0
#  \date     27/03/2018
#
from __future__ import absolute_import, unicode_literals
from builtins import str
from builtins import object
import logging
import cppyy
import imp

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def loaddataflow():
  import os
  import sys
  import platform
  # Add ROOT to the python path in case it is not yet there....
  sys.path.append(os.environ['ROOTSYS'] + os.sep + 'lib')
  from ROOT import gSystem

  logging.info('Loading library: libDataflow')
  result = gSystem.Load("libDataflow")
  if result < 0:
    raise Exception('dataflow.py: Failed to load the dataflow library libDataflow: ' + gSystem.GetErrorStr())
  logging.info('Loading library: libDataflowDict')
  result = gSystem.Load("libDataflowDict")
  if result < 0:
    raise Exception('dataflow.py: Failed to load the dataflow library libDataflowDict: ' + gSystem.GetErrorStr())
  logging.info('Import Online namespace from ROOT')
  from ROOT import Online as module
  return module

# We are nearly there ....
name_space = __import__(__name__)

def import_namespace_item(ns, nam):
  scope = getattr(name_space, ns)
  attr  = getattr(scope, nam)
  setattr(name_space, nam, attr)
  return attr

def import_root(nam):
  setattr(name_space, nam, getattr(ROOT, nam))

# ---------------------------------------------------------------------------
#
try:
  Online = loaddataflow()
  import ROOT
except Exception as X:
  import sys
  logger.error('+--%-100s--+', 100 * '-')
  logger.error('|  %-100s  |', 'Failed to load Online base library:')
  logger.error('|  %-100s  |', str(X))
  logger.error('+--%-100s--+', 100 * '-')
  sys.exit(1)


class _Levels(object):
  def __init__(self):
    self.VERBOSE = 1
    self.DEBUG = 2
    self.INFO = 3
    self.WARNING = 4
    self.ERROR = 5
    self.FATAL = 6
    self.ALWAYS = 7

OutputLevel = _Levels()

# ---------------------------------------------------------------------------
def import_online():
  import_namespace_item('Online', 'ComponentContainer')
  import_namespace_item('Online', 'ControlPlug')
  import_namespace_item('Online', 'Control')
  import_namespace_item('Online', 'DataflowComponent')
  import_namespace_item('Online', 'DataflowComponentProperties')
  import_namespace_item('Online', 'DataflowContext')
  import_namespace_item('Online', 'DataflowTask')
  import_namespace_item('Online', 'MBMClient')
  import_namespace_item('Online', 'OutputLogger')
  import_namespace_item('Online', 'Property')
  import_namespace_item('Online', 'PropertyManager')
  import_namespace_item('Online', 'PythonTask')
  import_namespace_item('Online', 'UnmanagedContainer')

import_online()

# ------------------------Generic STL stuff can be accessed using std:  -----
std        = cppyy.gbl.std
std_vector = std.vector
std_list   = std.list
std_map    = std.map
std_pair   = std.pair
# ------------------------Generic STL stuff can be accessed using std:  -----


