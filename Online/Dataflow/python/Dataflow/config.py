from __future__ import print_function
from builtins import str
from builtins import object
import dataflow
from dataflow import OutputLevel
from dataflow import PythonTask
from dataflow import std
from dataflow import std_vector
from dataflow import std_list
from dataflow import std_pair
from dataflow import std_map
from dataflow import logger

all_components = {}

class iPropertyMgr(object):
  def __init__(self, name):
    self.name = name
  def __repr__(self):
    return self.name
  def print_properties(self):
    for k,v in self.__dict__.items():
      print(str(k),'=',str(v))
  def options(self):
    global std_pair
    opts = []
    cl = std_pair('std::string,std::string')
    for k, v in self.__dict__.items():
      if k == 'name': continue
      if k == 'type': continue
      if isinstance(v,list):
        val = [str(i) for i in v]
      elif isinstance(v,tuple):
        val = [str(i) for i in v]
      else:
        val = v
      o = cl(self.name+'.'+str(k), str(val))
      opts.append(o)
    return opts

class iApplication(iPropertyMgr):
  def __init__(self, clazz):
    iPropertyMgr.__init__(self,'__MAIN__')
    self.Class = clazz

  def run(self):
    global std_vector
    global all_components
    options = std_vector('std::pair<std::string,std::string>')()
    opts    = self.options()
    for o in opts: options.push_back(o)
    for n,c in all_components.items():
      if n.find(self.name) == 0: continue
      opts = c.options()
      for o in opts: options.push_back(o)
    all_components = {}
    opts = None
    return PythonTask().run(options)

class iComponent(iPropertyMgr):
  def __init__(self, type, name):
    iPropertyMgr.__init__(self,name)
    self.type = type
  def __repr__(self):
    return self.type+'/'+self.name

def Application(clazz):
  global all_components
  name = '__MAIN__'
  if name not in all_components:
    all_components[name] = iApplication(clazz)
  return all_components[name]

def Component(type, name=None):
  global all_components
  if not name:
    name = type
    itms = type.split('/')
    if len(itms) > 1:
      type = itms[0]
      name = itms[1]
  if name not in all_components:
    all_components[name] = iComponent(type,name)
  return all_components[name]

