//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_EVENTACCESS_H
#define ONLINE_EVENTDATA_EVENTACCESS_H

/// Framework include files
#include <EventHandling/Macros.h>
#include <EventHandling/EventTraits.h>
#include <CPP/Interactor.h>
#include <RTL/Logger.h>
#include "RTL/Pack.h"

/// C/C++ include files
#include <list>
#include <vector>
#include <mutex>
#include <memory>
#include <atomic>
#include <limits>
#include <climits>
#include <stdexcept>
#include <condition_variable>

/// Online namespace declaration
namespace Online   {

  
  /// Base class to access online event data
  /** @class EventAccess
    *
    * @author  M.Frank
    * @version 1.0
    * @date    25/04/2019
    */
  class EventAccess : public CPP::Interactor  {
  public:
    
    typedef std::lock_guard<std::mutex>        lock_t;
    typedef event_traits::data_t               data_t;
    typedef event_traits::record_t             record_t;
    typedef event_traits::event_collection_t   event_collection_t;
    typedef const event_traits::data_t*        datapointer_t;
    typedef std::vector<Tell1Bank*>            bank_collection_t;

    /// Base class to guard event bursts and protect against premature deletion
    /**
     *
     * \author  M.Frank
     * \version 1.0
     * \date    25/04/2019
     */
    class guard_t   {
    public:
      std::atomic<std::size_t> consumed_len { 0 };
      std::atomic<std::size_t> frame_curr   { 0 };
    protected:
      std::size_t              bx_len       { 0 };
      std::size_t              frame_len    { 0 };
      long                     typ          { event_traits::none::data_type };

    public:
      /// Initializing constructor for multi MDF events
      guard_t(long t) : typ(t)  { }
      /// Default constructor
      guard_t() = delete;
      /// Move constructor
      guard_t(guard_t&& copy) = delete;
      /// Move assignment
      guard_t& operator=(guard_t&& copy) = delete;
      /// Inhibit copy assignment
      guard_t& operator=(const guard_t& copy) = delete;
      /// Default destructor
      virtual ~guard_t() = default;
      /// Careful: unprotected information access
      bool        empty()  const           {  return consumed_len >= bx_len; }
      /// Access the number of Bx in the burst
      std::size_t num_bx()   const         {  return bx_len;                 }
      /// Access the number of event frames in the burst (Num.Bx corrected for TAE)
      std::size_t num_frame()  const       {  return frame_len;              }
      /// Access number of consumer Bx
      std::size_t num_consumed()  const    {  return consumed_len;           }
      /// Access the current event frames in the burst (Num.Bx corrected for TAE)
      std::size_t curr_frame()  const      {  return frame_curr;             }
      /// Gaurd type
      long        type()   const           {  return typ;                    }
      /// Access Bx event record.
      virtual record_t at(std::size_t which) const  = 0;
    };

    typedef std::shared_ptr<guard_t>               shared_guard_t;
    typedef std::list<shared_guard_t>              burst_collection_t;
    typedef std::pair<std::size_t, shared_guard_t> event_t;


    /// Base class to hold basic configuration properties
    /**
     *
     * \author  M.Frank
     * \version 1.0
     * \date    25/04/2019
     */
    struct config_t  {
      /// Default constructor
      config_t() = default;
      /// Move constructor
      config_t(config_t&& copy) = default;
      /// Copy constructor
      config_t(const config_t& copy) = default;
      /// Default destructor
      ~config_t() = default;
      /// Assignment operator
      config_t& operator=(config_t&& copy) = default;
      /// Move assignment operator
      config_t& operator=(const config_t& copy) = default;
      /// Property: Print burst information every burstPrintCount events
      std::size_t  burstPrintCount   = std::numeric_limits<std::size_t>::max();
      /// Property: Maximum number of events to be processed
      std::size_t  maxEventsIn       = std::numeric_limits<std::size_t>::max();
      /// Property: Enable data verification for Tell1 banks
      bool         verifyBanks       = true;
      /// Property: Expand TAE frames. Handle them like normal collisions otherwise
      bool         expandTAE         = true;
    } *base_config = 0;

  public:
    /// Reference to output logger
    std::unique_ptr<RTL::Logger>  m_logger;
    /// Mutex to protect the burst queue
    std::mutex                    m_burstLock;
    /// Mutex to protect the event queue
    std::mutex                    m_eventLock;
    /// Mutex for condition variable when waiting from the frame producer
    std::mutex                    m_dataLock;
    /// Condition variable when waiting from the frame producer
    std::condition_variable       m_haveData;
    /// Container to bank referentes to MDF headers containing event blocks
    burst_collection_t            m_bursts;

    /// Definition of the monitoring data block
    /**
     *
     * \author  M.Frank
     * \version 1.0
     * \date    25/04/2019
     */
    struct monitor_t  {
      /// Monitoring item: Count of bursts which were received from the input source
      std::size_t                 burstsIn       = 0;
      /// Monitoring item: Count of bursts which started or finished processing
      std::size_t                 burstsOut      = 0;
      /// Monitoring item: Count of bursts which were released
      std::size_t                 burstsRelease  = 0;
      /// Monitoring item: Count of bursts which were released
      std::size_t                 burstsRequest  = 0;
      /// Monitoring item: Count of events which were received from the input source
      std::size_t                 eventsIn       = 0;
      /// Monitoring item: Count of events which started or finished processing
      std::size_t                 eventsOut      = 0;
      /// Monitoring item: Count of events which are currently buffered    
      std::size_t                 eventsBuffered = 0;
      /// Monitoring item: Count the events with no data banks
      std::size_t                 eventsNoBanks  = 0;
      /// Monitoring item: Count the events with more banks than allowed
      std::size_t                 eventsMaxBanks = 0;
      /// Monitoring item: Count the number of TAE events encountered
      std::size_t                 framesTAE      = 0;
      /// Monitoring item: Count the number of TAE fragments encountered
      std::size_t                 fragmentsTAE   = 0;
    } monitor;

    /// Property:  poll timeout to check if producers have delivered an event [milliseconds]
    int                           m_eventPollTMO      = 100;
    /// Flag to indicate if event request got cancelled
    bool                          m_cancelled = false;

    std::pair<long, unsigned char*> m_deCompress {0,nullptr};

    /// The event is a MDF with multiple events, which must be decoded
    std::pair<long, std::unique_ptr<event_collection_t> >
    convertMultiMDF(datapointer_t start, std::size_t len);

    /// The event is a PCIE40 MEP structure with multiple events, which must be decoded
    std::pair<long, std::unique_ptr<pcie40::event_collection_t> >
    convertPCIE40MEP(datapointer_t start, std::size_t len);

  public:
    /// Default constructors and destructors / inhibits
    ONLINE_COMPONENT_CTORS(EventAccess);

    /// Default constructor
    EventAccess(std::unique_ptr<RTL::Logger>&& logger);

    /// Standard destructor
    virtual ~EventAccess();

    /// Print burst/event information
    virtual void printInfo()  const;

    /// Set the print frequency
    void setBurstPrintCount(std::size_t value) { base_config->burstPrintCount = value;     }

    /// Set TAE processing flag
    void setExpandTAE(bool value)              { base_config->expandTAE = value;           }

    /// Set flag to verify banks
    void setVerifyBanks(bool value)            { base_config->verifyBanks = value;         }

    /// Queue cancellation
    void queueCancel(bool value = true)        { m_cancelled = value;                      }

    /// Access cancellation flag
    bool           isCancelled()  const        { return m_cancelled;                       }

    /// Any events buffered anymore?
    bool           empty()  const              { return monitor.eventsBuffered == 0;       }

    /// Access number of events buffered (unsafe)
    std::size_t    eventsBuffered()   const    { return monitor.eventsBuffered;            }

    /// Access number of bursts buffered (unsafe)
    std::size_t    burstsBuffered()   const    { return monitor.burstsIn-monitor.burstsOut;}

    /// Insert new entry into burst queue with lock protection
    std::size_t    queueBurst(shared_guard_t&& burst);

    /// Dequeue burst if one is present with lock protection
    shared_guard_t dequeueBurst();

    /// Access thread safe number of queued bursts
    std::size_t    numQueuedBurst();

    /// Dequeue burst if present, otherwise wait for producer to emplace one.
    shared_guard_t waitBurst();

    /// Clear burst queue and update counters
    std::size_t    clear();
    
    /// Dequeue event if one is present with lock protection
    event_t        dequeueEvent(shared_guard_t& context);

    /// Dequeue event if present, otherwise wait for producer to emplace one.
    event_t        waitEvent(shared_guard_t& context);

    /// Fill the event data cache if necessary
    virtual int    fill_cache() = 0;

    /// Cancel all pending I/O requests to the buffer manager
    virtual int    cancel() = 0;

    /// Close connection to event data source
    virtual void   close() = 0;
  };
}        // End namespace Online
#include "RTL/Unpack.h"
#endif   // ONLINE_EVENTDATA_EVENTACCESS_H
