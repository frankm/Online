//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef GAUDIONLINE_EVENTHANDLER_H
#define GAUDIONLINE_EVENTHANDLER_H

/// Framework include files
#include <EventHandling/EventAccess.h>
#include <RTL/Logger.h>

/// Online namespace declaration
namespace Online  {

  /// Forward declarations
  class   EventOutput;
  typedef Tell1Bank BankHeader;

  /// Online algorithm to handle event data
  /** 
    *
    * @author  M.Frank
    * @version 1.0
    * @date    25/04/2019
    */
  class EventHandler   {
  public:

    typedef std::vector<std::pair<const BankHeader*, const void*> > evt_data_t;
    typedef std::pair<size_t, EventAccess::shared_guard_t> evt_desc_t;
    typedef const std::vector<int> mask_t;

    enum dump_flags   {
      DUMP_EVENTS           = 1 << 1,
      DUMP_BANK_COLLECTIONS = 1 << 2,
      DUM_BANK_HEADERS      = 1 << 3
    };
    enum error_flags   {
      NO_MDF_ERROR         = 0,
      INVALID_MDF_FORMAT    = 1 << 1,
      INVALID_MAGIC_PATTERN = 1 << 2,
      INVALID_BANK_SIZE     = 1 << 3,
      TOO_MANY_BANKS        = 1 << 31
    };

  protected:
    /// Reference to output logger
    std::unique_ptr<RTL::Logger> m_logger;

  public:

    /// Default constructor
    EventHandler() = default;

    /// Default destructor
    virtual ~EventHandler() = default;

    /// Put bank container to specified location in the TES
    virtual void put_banks(const std::string& loc, evt_data_t&& event)  const = 0;
    
    std::string _bx_offset(int bx)   const;

    bool         type_found(int required, const std::vector<int>& good_types)  const;

    /// Check if the event is a TAE event
    std::pair<unsigned int, bool> check_tae(const evt_desc_t& event)  const;
    
    /// Access to ODIN banks from the event record
    std::pair<const BankHeader*, const void*> get_odin_bank(const evt_desc_t& event)  const;

    /// Put bank container to specified location in the TES
    void         put_event(const std::string& loc, const evt_desc_t& event, size_t eid)  const;

    /// Extract event record from normal/TAE event frame
    std::pair<int,evt_data_t> create_event_data(const evt_desc_t& event, int bx)   const;

    /// Extract error banks from the event data
    evt_data_t   get_errors(event_traits::record_t event)  const;

    /// Extract error banks from the event data
    evt_data_t   get_errors(evt_desc_t& event)  const;

    /// Extract Tell1 banks from event buffer
    std::pair<int,evt_data_t> extract_tell1_banks(const unsigned char* start, const unsigned char* end)  const;

    /// Dump event structure to output
    void         dump_event(evt_desc_t& e, int flags)  const;

    /// Output MDF frame from the event data
    template <typename traits = void>
    size_t       output_mdf(const evt_data_t& event, mask_t& mask, bool requireODIN, EventOutput& output)  const;

    /// Output MDF frame from the event data
    size_t       output_mdf(const evt_data_t& event, int type, mask_t& mask, bool requireODIN, EventOutput& output)  const;

    /// Output MDF/TAE frame from the event data
    template <typename traits = void>
    size_t       output_mdf(const evt_desc_t& event, mask_t& mask, bool requireODIN, EventOutput& output)  const;

    /// Output MDF/TAE frame from the event data
    size_t       output_mdf(const std::vector<std::pair<int, const evt_data_t*> >& crossings, mask_t& mask, EventOutput& output)  const;

    /// Output TAE frame in MDF format
    template <typename traits = void>
    size_t       output_tae(const evt_desc_t& event, mask_t& mask, EventOutput& output)  const;

    /// Output PCIE40 event in PCIE40-MEP format
    void         output_pcie40(const EventAccess::guard_t* guard, EventOutput& output)  const;

    /// Extract banks from event descriptor
    template <typename traits = void>
      std::pair<int,evt_data_t> extract_banks(const evt_desc_t& event)   const;

    /// On the occurrence of a TAE frame expand data and populate TES locations
    template <typename traits = void>
    size_t       expand_tae(const std::string& prefix, const evt_desc_t& event)  const;

    template <typename T> T mdf_error_report(T return_value, int code)  const;

  };
}       // End namespace Online
#endif  // GAUDIONLINE_EVENTHANDLER_H
