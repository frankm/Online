//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_MBMEVENTOUTPUT_H
#define ONLINE_EVENTDATA_MBMEVENTOUTPUT_H

/// Framework include files
#include <EventHandling/EventOutput.h>
#include <MBM/BufferInfo.h>
#include <MBM/bmdef.h>

/// C/C++ include files
#include <list>
#include <memory>

/// Online namespace declaration
namespace Online   {

  /// Base class to output online event data
  /** @class MBMEventOutput
    *
    * @author  M.Frank
    * @version 1.0
    * @date    25/04/2019
    */
  class MBMEventOutput : public EventOutput  {
  public:
    /// Base class to handle one atomic MBM write operation
    class mbm_transaction_t;

    /// Pointer to mbmplug device
    typedef std::list<mbm_transaction_t*> transaction_collection_t;

    struct mbm_config_t : public config_t  {
      using config_t::config_t;
      /// Partition name
      std::string   partitionName     {};
      /// Property: Partition ID
      unsigned int  partitionID       {0x103};
      /// Property: Flag to discriminate buffers using the partition name
      bool          partitionBuffers  {true};
      /// Property: Set property to positive value to ch3eck for consumers
      int           max_consumer_wait {-1};
      /// Default event type for output declarations
      int           event_type        {EVENT_TYPE_BURST};
      
    } config;

    /// Reference to buffer information if checks are enabled
    std::unique_ptr<MBM::BufferInfo> m_mbmInfo;

  public:
    /// Create proper buffer name depending on partitioning
    std::string bufferName(const std::string& nam)  const;
    /// Check for consumer presence within a given time window
    void checkConsumers(int ev_type, unsigned int partid);

  public:
    /// Default constructors and destructors / inhibits
    ONLINE_COMPONENT_CTORS(MBMEventOutput);

    /// Default constructor
    MBMEventOutput(std::unique_ptr<RTL::Logger>&& logger);

    /// Standard destructor
    virtual ~MBMEventOutput();

    /// EventAccess overload: Connect to event data source
    virtual int connect(const std::string& output, size_t instance);

    /// Cancel all pending I/O requests to the buffer manager
    virtual int cancel()  override;
  };
}        // End namespace Online
#endif   // ONLINE_EVENTDATA_MBMEVENTOUTPUT_H
