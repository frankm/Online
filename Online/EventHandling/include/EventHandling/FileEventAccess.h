//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_FILEEVENTACCESS_H
#define ONLINE_EVENTDATA_FILEEVENTACCESS_H

/// Framework include files
#include "EventHandling/EventAccess.h"
#include "Tell1Data/DirectoryScan.h"
#include "MBM/Requirement.h"

/// Online namespace declaration
namespace Online   {

  /// Generic MDF File I/O class to be used by the OnlineEvtApp
  /**
    *
    * \version 1.0
    * \author  M.Frank
    */
  class FileEventAccess : public EventAccess  {
  public:
    /// The guard type
    template <typename T> class file_guard_t;

    /// Combined request buffer
    MBM::Requirement         m_request;
    /// Directory scan object
    DirectoryScan            m_scan;
    /// Current file name
    RawFile                  m_current;
    /// Monitor the number of file scans
    size_t                   m_num_scans = 0;

    /// Helper class containing the configuration parameters of this object 
    /**
     *
     * \version 1.0
     * \author  M.Frank
     */
    struct file_config_t : public EventAccess::config_t  {
      /// Default constructor
      file_config_t() = default;
      /// Move constructor
      file_config_t(file_config_t&& copy) = default;
      /// Copy constructor
      file_config_t(const file_config_t& copy) = default;
      /// Default destructor
      ~file_config_t() = default;
      /// Assignment operator
      file_config_t& operator=(file_config_t&& copy) = default;
      /// Move assignment operator
      file_config_t& operator=(const file_config_t& copy) = default;

      /// Property: File prefix to select files from data directory
      std::string              prefix {"Run_"};
      /// Property: Path to the file containing broken nodes, where no reading should happen
      std::string              brokenHosts;
      /// Property: File: Requests in string format
      std::vector<std::string> requests;
      /// Property: List of runs to be processed (as strings!)
      std::vector<std::string> allowedRuns;
      /// Property: List of directories or files containing data files
      std::vector<std::string> sources;
      /// Default RawFile event type
      int                      inputType = RawFile::MDF_INPUT_TYPE;
      /// Property: Burst packingfactor
      int                      packingFactor  = 1;
      /// Property: Buffer allocation space for bursts
      size_t                   bufferSize     = 4*1024*1024;
      /// Property: Inhibit rescan of the source directory
      bool                     rescan         = false;
      /// Property: On open failed: delete file
      bool                     openFailDelete = true;
      /// Property: Flag to indicate if files should be deleted
      bool                     deleteFiles    = false;
      /// Property: Flag to indicate if in premature stop/pause the file reminder should be saved.
      bool                     saveRest       = false;
      /// Property: Flag to re-use same file for tests
      bool                     reuseFile      = false;
      /// Property: Memory map files rather than posix open
      bool                     mmapFiles      = false;
    } config;
    enum { FILEACCESS_ERROR = ONLINE_ERROR,
	   FILEACCESS_OK = ONLINE_OK,
	   FILEACCESS_NOMOREFILES = 4,
	   FILEACCESS_ERR_UNLINK = 6
    };

    /// Open a new data file
    std::pair<int,RawFile> openFile();

  public:
    /// Default constructors and destructors / inhibits
    ONLINE_COMPONENT_CTORS(FileEventAccess);

    /// Default constructor
    FileEventAccess(std::unique_ptr<RTL::Logger>&& logger);
    /// Standard destructor
    virtual ~FileEventAccess() = default;
    /// EventAccess overload: Connect to event data source
    virtual int connect(const std::vector<std::string>& requests);
    /// Fill the event data cache if necessary
    virtual int fill_cache()  override;
    /// Cancel all pending I/O requests to the buffer manager
    virtual int cancel()  override;
    /// EventAccess overload: Close connection to event data source
    virtual void close()  override;
  };
}        // End namespace Online
#endif   // ONLINE_EVENTDATA_FILEEVENTACCESS_H
