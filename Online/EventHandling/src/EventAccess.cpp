//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <EventHandling/EventAccess.h>
#include <Tell1Data/EventHeader.h>
#include <Tell1Data/Tell1Decoder.h>
#include <PCIE40Data/pcie40decoder.h>
#include <RTL/rtl.h>

#include <PCIE40Data/pcie40_writer.h>
#define __UNLIKELY( x ) __builtin_expect( ( x ), 0 )


/// C/C++ include files

using namespace std;
using namespace Online;

namespace {
  static size_t MBYTE = 1024UL*1024UL;
#if 0
  inline bool is_hlt_source(unsigned short typ)   {
    switch(typ)   {
    case pcie40::params::sourceid_top5_ODIN: return true;
    case pcie40::params::sourceid_top5_VP:   return true;
    case pcie40::params::sourceid_top5_UT:   return true;
    case pcie40::params::sourceid_top5_FT:   return true;
    case pcie40::params::sourceid_top5_Ecal: return false;
    case pcie40::params::sourceid_top5_Hcal: return false;
    case pcie40::params::sourceid_top5_Muon: return false;
    case pcie40::params::sourceid_top5_Rich: return false;
    default:                                 return false;
    }
  }

  inline bool is_norich_source(unsigned short typ)   {
    switch(typ)   {
    case pcie40::params::sourceid_top5_ODIN: return true;
    case pcie40::params::sourceid_top5_VP:   return true;
    case pcie40::params::sourceid_top5_UT:   return true;
    case pcie40::params::sourceid_top5_FT:   return true;
    case pcie40::params::sourceid_top5_Ecal: return true;
    case pcie40::params::sourceid_top5_Hcal: return true;
    case pcie40::params::sourceid_top5_Muon: return true;
    case pcie40::params::sourceid_top5_Rich: return false;
    default:                                 return false;
    }
  }
#endif
  template <typename BANK_TYPE> inline bool checkBank(const BANK_TYPE* bank)   {
    return
      // Check for Tell1 bank magic word or PCIE40 magic word
      (bank->magic() == BANK_TYPE::MagicPattern || bank->magic() == 0xFACE) &&
      (bank->size() >= 0) &&
      (bank->type() >= 0) && 
      (bank->type() < Tell1Bank::LastType);
  }

  inline bool checkHeader(const EventHeader* header)  {
    return
      header->size0() > 0 &&
      header->size0() < 100UL*MBYTE &&
      header->size0() == header->size1() &&
      header->size0() == header->size2();
  }

  EventAccess::event_t _extract_tae_pcie40(EventAccess* acc, size_t eid, EventAccess::shared_guard_t& guard)   {
    auto*  log   = acc->m_logger.get();
    auto*  evt   = guard->at(eid).tell40_event;
    auto   hw    = evt->tae_half_window();
    /// Remember 1 was already added for first fragment
    if ( hw+eid < guard->num_bx() )   {  // equals to: 2*hw+guard->num_consumed() <= guard->num_bx()
      EventAccess::event_t event(eid, guard);
      size_t len = 2*hw;
      guard->consumed_len += len;
      ++guard->frame_curr;
      {
	EventAccess::lock_t lock(acc->m_eventLock);
	acc->monitor.framesTAE      += 1;
	acc->monitor.fragmentsTAE   += len+1;
	acc->monitor.eventsOut      += len;
	acc->monitor.eventsBuffered -= len;
      }
      return event;
    }
    EventAccess::event_t event(0, EventAccess::shared_guard_t());
    size_t len = guard->num_bx() - guard->num_consumed();
    guard->consumed_len += guard->num_bx();
    log->warning("+++ TAE (window:%d) spanning across MEP boundaries. Skip %ld events", int(hw), len);
    {
      EventAccess::lock_t lock(acc->m_eventLock);
      acc->monitor.eventsOut      += len;
      acc->monitor.eventsBuffered -= len;
    }
    return event;
  }
  EventAccess::event_t _extract_pcie40(EventAccess* /* acc */, size_t eid, EventAccess::shared_guard_t& guard)   {
    ++guard->frame_curr;
    return {eid, guard};
  }
}

/// Standard constructor
EventAccess::EventAccess(unique_ptr<RTL::Logger>&& logger)
  : m_logger(move(logger))
{
}

/// Standard destructor
EventAccess::~EventAccess()    {
  m_logger->info("Event access shutdown.");
}

/// Print burst/event information
void EventAccess::printInfo()  const    {
  if ( (monitor.burstsIn%base_config->burstPrintCount) == 0 )   {
    m_logger->info("+++ Burst-in:%10ld out:%8ld req:%8ld rel:%8ld Events-in:%10ld out:%10ld buffered:%4ld",
		   monitor.burstsIn,      monitor.burstsOut,
		   monitor.burstsRequest, monitor.burstsRelease, 
		   monitor.eventsIn,      monitor.eventsOut,
		   monitor.eventsBuffered);
  }
}

/// Insert new entry into event queue with lock protection
size_t EventAccess::queueBurst(shared_guard_t&& burst)   {
  if ( !burst.get() )   {
    m_logger->warning("+++ queueBurst: Invaliud burst encountered -> skip.");
    return monitor.eventsBuffered;
  }
  {
    lock_t lock(m_eventLock);
    monitor.eventsIn        += burst->num_bx();
    monitor.eventsBuffered  += burst->num_bx();
  }
  {
    lock_t lock(m_burstLock);
    m_bursts.emplace_back(move(burst));
    ++monitor.burstsIn;
  }
  m_haveData.notify_one();
  return monitor.eventsBuffered;
}

/// Dequeue event if one is present with lock protection
EventAccess::shared_guard_t EventAccess::dequeueBurst()   {{
    lock_t lock(m_burstLock);
    if ( !m_bursts.empty() )  {
      shared_guard_t burst(m_bursts.front());
      m_bursts.pop_front();
      ++monitor.burstsOut;
      return burst;
    }
  }
  return shared_guard_t();
}

/// Access thread safe number of queued bursts
size_t EventAccess::numQueuedBurst()   {
  lock_t lock(m_burstLock);
  return m_bursts.size();
}

/// Clear burst queue and update counters
size_t EventAccess::clear()   {
  size_t count = 0;
  pcie40::decoder_t::reset_last_good_run_number();
  for(shared_guard_t burst=dequeueBurst(); burst.get(); burst=dequeueBurst())  {
    lock_t lock(m_eventLock);
    monitor.eventsBuffered -= burst->num_bx();
    count += burst->num_bx();
  }
  return count;
}

/// Dequeue event if present, otherwise wait for producer to emplace one.
EventAccess::shared_guard_t EventAccess::waitBurst()   {
  shared_guard_t burst(dequeueBurst());
  if ( !burst.get() )   {
    while ( m_bursts.empty() )   {
    WaitBurst:
      unique_lock<mutex> lock(m_dataLock);
      cv_status sc = m_haveData.wait_for(lock, chrono::milliseconds(m_eventPollTMO));
      if ( sc == cv_status::no_timeout )   {
	burst = dequeueBurst();
	if ( !burst && !isCancelled() ) continue;
	return burst;
      }
      if ( isCancelled() )   {
	return burst;
      }
    }
    burst = dequeueBurst();
    if ( !burst )  {
      if ( isCancelled() )   {
	m_logger->info("+++ waitBurst: Event request was cancelled.");
	return burst;
      }
      m_logger->warning("+++ waitBurst: Requeue request!");
      goto WaitBurst;
    }
  }
  return burst;
}

/// Dequeue event if one is present
EventAccess::event_t EventAccess::dequeueEvent(shared_guard_t& loop_context)   {
  if( loop_context )  {
    size_t idx = ++loop_context->consumed_len;
    if( idx <= loop_context->num_bx() )  {
      event_t event;
      size_t  eid  = idx-1;
      size_t  nbx  = loop_context->num_bx();
      if( loop_context->type() == event_traits::tell40::data_type )   {
	record_t record(loop_context->at(eid));
	// IF config->ignoreTAE:
	// Handle TAE frames like normal collisions
	//
	// IF TAE:
	// We exclude [-laf-window, +half-window] events from the iteration
	// We need to attach the prevX, nextN fragments
	// later at the proper locations in the TES
	//
	// IF normal cossisions:
	// Simply extract the frame and return it to the user
	//
	if ( !base_config->expandTAE )   {
	  event = ::_extract_pcie40(this, eid, loop_context);
	}
	else if ( record.tell40_event->is_tae_central() )   {
	  event = ::_extract_tae_pcie40(this, eid, loop_context);
	}
	else if ( !record.tell40_event->is_tae() )   {
	  event = ::_extract_pcie40(this, eid, loop_context);
	}
	else   {
	  /// Skip all TAE fragments until we got the central TAE event.
	  /// Return the central TAE event or the next regular event
	  for( uint8_t iw=1, hw = record.tell40_event->tae_half_window(); iw<=2*hw; ++iw )   {
	    std::size_t id = eid+iw;
	    if ( id >= loop_context->num_bx() )   {
	      loop_context.reset();
	      return event_t(0, shared_guard_t());
	    }
	    const auto* ev = record_t(loop_context->at(id)).tell40_event;
	    if ( ev->is_tae_central() )   {
	      event = ::_extract_tae_pcie40(this, id, loop_context);
	      break;
	    }
	    if ( !ev->is_tae() )   {
	      event = ::_extract_pcie40(this, id, loop_context);
	      break;
	    }
	    continue;
	  }
	}
      }
      else if( loop_context->type() == event_traits::tell1::data_type )   {
	// IF TAE:
	// Nothing to do here. The MDF frame is self describing and contained.
	// We only need to attach the prevX, nextN fragments
	// later at the proper locations in the TES
	event = event_t(eid, loop_context);
      }
      //
      // If we used up the burst, reset it.
      if( loop_context->empty() ) {
	loop_context.reset();
      }
      else   {
	++loop_context->frame_curr;
	size_t cons = loop_context->num_consumed();
	size_t curr = loop_context->curr_frame();
	size_t nfrm = loop_context->num_frame();
	m_logger->debug("+++ dequeueEvent: Nbx:%4ld Current:%4ld / %4ld  "
			"eid: [%4ld, %4ld, %4ld] Bx left: %ld",
			nbx, curr, nfrm, eid, event.first, cons-1, nbx-cons);
      }
      // Return the event to the user
      if( event.second.get() )   {
	lock_t lock(m_eventLock);
	++monitor.eventsOut;
	--monitor.eventsBuffered;
      }
      return event;
    }
    loop_context.reset();
  }
  return event_t(0, shared_guard_t());
}

/// Dequeue event if present, otherwise wait for producer to emplace one.
EventAccess::event_t EventAccess::waitEvent(shared_guard_t& loop_context)   {
  event_t event(dequeueEvent(loop_context));
  if( !event.first )   {
    while( !loop_context )   {
      shared_guard_t tmp(waitBurst());
      if( tmp.get() )   {
	loop_context = tmp;
	event = dequeueEvent(loop_context);
	if( !event.first )  {
	  m_logger->error("+++ Encountered empty EVENT!");
	  int halt = 1;
	  while(halt)  {
	    ::lib_rtl_sleep(100); 
	  }
	}
	return event;
      }
      // If cancelled and no bursts queued, we end here and stop processing
      if( m_cancelled ) break;
      m_logger->warning("+++ waitEvent: Encountered empty MDF burst!");
    }
  }
  // This only happens when an "end-marker" is injected and we are already cancelled
  return event;
}

/// The event is a PCIE40 MEP structure with multiple events, which must be decoded
pair<long, unique_ptr<pcie40::event_collection_t> >
EventAccess::convertPCIE40MEP(datapointer_t start, size_t len)    {
  using namespace pcie40;
  struct _printer  {
    RTL::Logger& logger;
    _printer(RTL::Logger& e) : logger(e) {}
    /// Printout operator
    void operator()(int lvl, const char* source, const char* msg)  {
      logger.printmsg(lvl, source, msg);
    }
  } printer(*m_logger);
  pair<long, unique_ptr<pcie40::event_collection_t> > events;
  pcie40::decoder_t::logger_t logger = printer;
  pcie40::decoder_t           decoder(logger);
  datapointer_t end = start + len;
  
  if ( end > start )   {
    static constexpr size_t max_bank    = 20000;
    static constexpr size_t max_source  = 2000;
    static constexpr size_t max_packing = 35000;
    const auto* pmep_hdr = reinterpret_cast<const pcie40::mep_header_t*>(start);
    if ( 0 == pmep_hdr->size )    {
      m_logger->error("convertMEP: MEP with invalid size encountered: %d", pmep_hdr->size);
      decoder.initialize(events.second,0);
      return events;
    }
    if ( 0 == pmep_hdr->num_source || pmep_hdr->num_source > max_source )    {
      m_logger->error("convertMEP: MEP with invalid number of sources encountered: %d",
		      pmep_hdr->num_source);
      decoder.initialize(events.second,0);
      return events;
    }
    uint32_t packing = pmep_hdr->multi_fragment(0)->header.packing;
    if ( 0 == packing || packing > max_packing )    {
      decoder.initialize(events.second,0);
      try {
	throw runtime_error("convertMEP: MEP with invalid packing factor encountered");
      }
      catch(...)   {
	m_logger->error("convertMEP: MEP with invalid packing factor encountered: %d", packing);
      }
      return events;
    }
    /// Header checks done. Now decode the buffer
    decoder.decode(events.second, pmep_hdr);

    /// When decoding the sequence of beam-crossings we need to 
    /// know about TAE crossings.....
    ///
    bool dbg = false;
    events.first = 0;
    for( long iev=0, num_events=events.second->size(); iev < num_events; ++iev )   {
      const pcie40::bank_collection_t* coll = nullptr;
      const pcie40::event_t* ev = events.second->at(iev);
      size_t num_coll = ev->num_bank_collections();
      size_t num_bank = 0;
      for( size_t j=0; j<num_coll; ++j )   {
	coll = ev->bank_collection(j);
	for( const pcie40::bank_t* b=coll->begin(); b != coll->end(); b=coll->next(b) )  {
	  if ( b->magic() != pcie40::bank_t::MagicPattern )  {
	    m_logger->error("convertMEP: Bad magic pattern in raw bank!\n");
	  }
	  ++num_bank;
	}
	for( const pcie40::bank_t* b=coll->special_begin(); b != coll->special_end(); b=coll->next(b) )  {
	  if ( b->magic() != pcie40::bank_t::MagicPattern )  {
	    m_logger->error("convertMEP: Bad magic pattern in raw bank!\n");
	  }
	  ++num_bank;
	}
      }
      if ( num_bank == 0 )   {
	++monitor.eventsNoBanks;
	m_logger->info("convertMEP: Event with ZERO banks encountered");
      }
      else if ( num_bank > max_bank )   {
	++monitor.eventsMaxBanks;
	m_logger->info("convertMEP: Event with TOO MANY banks encountered");
      }
      
      /// If we supposedly igore TAEs, increase event counter and continue
      if ( !base_config->expandTAE )    {
	++events.first;
	continue;
      }

      /// Trame counter with correction for TAE multi-frames
      long window  = ev->tae_half_window();
      bool central = ev->is_tae_central();
      if ( window == 0 )   {
	++events.first;
      }
      else if ( central )    {
	++events.first;
      }
      /// Correction to exclude TAE frames overlapping the boundaries of the MEP
      if ( central )   {
	if ( iev-window < 0 || iev+window >= num_events )   {
	  central = false;
	  window = 0;
	  --events.first; // Subract central (will be added again below)
	  for(long iw = std::max(0L,iev-window); iw < std::min(num_events,iev+window); ++iw)   {
	    auto* e = const_cast<pcie40::event_t*>(events.second->at(iw));
	    e->flags.detail.tae_central = false;
	    e->flags.detail.tae_first   = false;
	    e->flags.detail.tae_window  = 0;
	    ++events.first;
	  }
	}
      }

      /// Run a few basic checks over the resulting TAE frame
      if ( central )   {
	for(long iw = iev-window; iw <= iev+window; ++iw)   {
	  auto* e = events.second->at(iw);
	  if ( e->tae_half_window() != window )   {
	    m_logger->warning("+++ Inconsistent TAE half window %d != %d BX: %ld",
			      int(e->tae_half_window()), int(window), iw);
	  }
	  if ( iw == iev && !e->is_tae_central() )   {
	    m_logger->warning("+++ Detected missing TAE central BX %ld", iw);
	  }
	  else if ( iw != iev && e->is_tae_central() )   {
	    m_logger->warning("+++ Detected false TAE central BX %ld", iw);
	  }
	}
      }
      if ( dbg )   {
	if ( ev->is_tae_central() )   {
	  m_logger->always("convertMEP: TAE at %ld window: %d  [%ld, %ld, %ld] frames:%ld",
			   iev, window, iev-window, iev, iev+window, events.first);
	}
	else if ( window == 0 )   {
	  m_logger->always("convertMEP: Evt at %ld  frames:%ld", iev, events.first);
	}
      }
    }
    if ( dbg )   {
      m_logger->warning("convertMEP: Got MEP with %ld frames", events.first);
    }
    return events;
  }
  m_logger->error("convertMEP: Encountered empty MDF burst!");
  decoder.initialize(events.second,0);
  return events;
}

/// The event is a MDF with multiple events, which must be decoded
pair<long, unique_ptr<EventAccess::event_collection_t> >
EventAccess::convertMultiMDF(datapointer_t startptr, size_t len)   {
  pair<long, unique_ptr<event_collection_t> > events(0,make_unique<event_collection_t>());
  datapointer_t start = startptr, end = startptr + len;

  if ( end > start )   {
    while ( start < end )   {
      const EventHeader* header    = (const EventHeader*)start;
      try   {
	bool               skip      = false;
	bool               stop      = false;
	const char*        err_msg   = nullptr;
	const Tell1Bank*   err_bank  = nullptr;
	auto [evt_start, evt_end]    = header->data_frame();
	// Check bank's magic word and check on bank type and length
	if ( !checkHeader(header) )  {
	  err_msg = "Event with BAD header bank encountered";
	  stop = true;
	}
	else if ( base_config->verifyBanks )  {
	  size_t num_banks = 0;
	  size_t max_banks = 20000;
	  while(evt_start < evt_end && num_banks < (max_banks+1) )  {
	    Tell1Bank* bank = (Tell1Bank*)evt_start;
	    if ( !checkBank(bank) )  {
	      err_msg  = "Event with BAD bank encountered";
	      err_bank = bank;
	      skip     = true;
	      break;
	    }
	    if ( bank->type() == Tell1Bank::TAEHeader )   {
	      max_banks = 2000 * bank->size()/(3*sizeof(int));
	    }
	    evt_start += bank->totalSize();
	    ++num_banks;
	  }
	  if ( num_banks == 0 )   {
	    ++monitor.eventsNoBanks;
	    err_msg = "Event with ZERO banks encountered";
	    skip = true;
	  }
	  else if ( num_banks > max_banks )   {
	    ++monitor.eventsMaxBanks;
	    err_msg = "Event with TOO MANY banks encountered";
	    //stop = true;
	    skip = true;
	  }
	}
	if ( err_msg || err_bank )   {
	  m_logger->error("SKIP EVENT: Buffer:%p Start:%p End:%p Size:(%d,%d,%d) HdrVsn:%u %s",
			  (void*)startptr, (void*)start, (void*)evt_end, header->size0(),
			  header->size1(), header->size2(), header->headerVersion(),
			  stop ? "---> STOP decoding" : "");
	  if ( err_msg )   {
	    m_logger->error("   %s",err_msg);
	  }
	  if ( err_bank )  {
	    m_logger->error("   Bank:     %p  -> %s  SourceID: 0x%04X / %d",
			    (void*)err_bank, Tell1Printout::bankHeader(err_bank).c_str(), 
			    err_bank->sourceID(), err_bank->sourceID());
	  }
	  if ( stop )   {
	    return events;
	  }
	}
	else if ( !skip )   {
	  events.second->emplace_back(header);
	  events.first++;
	}
	start += header->size0();
      }
      catch(...)    {
	m_logger->error("SKIP EVENT: Buffer:%p Start:%p End:%p Size:(%d,%d,%d) HdrVsn:%u",
			(void*)startptr, (void*)start, (void*)end, header->size0(),
			header->size1(), header->size2(), header->headerVersion());
	start = end;
      }
    }
    return events;
  }
  m_logger->error("convertMultiMDF: Encountered empty MDF burst!");
  return events;
}
