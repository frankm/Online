//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <EventHandling/EventHandler.h>
#include <EventHandling/EventTraits.h>
#include <EventHandling/EventOutput.h>

#include <Tell1Data/EventHeader.h>
#include <Tell1Data/Tell1Decoder.h>
#include <Tell1Data/RunInfo.h>

#include <PCIE40Data/pcie40decoder.h>
#include <PCIE40Data/sodin.h>

#include <CPP/Value.h>

/// C/C++ include files
#include <exception>
#include <stdexcept>
#include <vector>

using namespace std;
using namespace Online;

namespace {

  void _update_mask(unsigned int* m, const EventHandler::mask_t& mask)
  {
    if ( mask.size() == 1 )  {
      m[3] = mask[0];
    }
    else if ( mask.size() == 2 )  {
      m[3] = mask[0];
      m[2] = mask[1];
    }
    else if ( mask.size() == 3 )  {
      m[3] = mask[2];
      m[2] = mask[1];
      m[1] = mask[0];
    }
    else if ( mask.size() >= 4 )  {
      m[3] = mask[3];
      m[2] = mask[2];
      m[1] = mask[1];
      m[0] = mask[0];
    }
  }

  template<typename odin_type>
  void _setEventHeader(EventHeader*     ev_hdr,
		       size_t           len,
		       const odin_type& odin,
		       unsigned int*    m)
  {
    ev_hdr->setHeaderVersion(EventHeader::CURRENT_VERSION);
    ev_hdr->setSpare(0);
    ev_hdr->setChecksum(0);
    ev_hdr->setCompression(0);
    ev_hdr->setSize(len);
    ev_hdr->setDataType(EventHeader::BODY_TYPE_BANKS);
    ev_hdr->setSubheaderLength(sizeof(EventHeader::Header1));

    auto* sub_hdr = ev_hdr->subHeader().H1;
    if ( !odin.first )   {
      sub_hdr->setRunNumber(0);
      sub_hdr->setOrbitNumber(0);
      sub_hdr->setBunchID(0);
    }
    else if ( odin.first->version() < 7 )   {
      const auto* o = reinterpret_cast<const RunInfo*>(odin.second);
      sub_hdr->setRunNumber(o->run_number());
      sub_hdr->setOrbitNumber(o->orbit_id());
      sub_hdr->setBunchID(o->bunch_id());
    }
    else    {
      const auto* o = reinterpret_cast<const pcie40::sodin_t*>(odin.second);
      sub_hdr->setRunNumber(o->run_number());
      sub_hdr->setOrbitNumber(o->orbit_id());
      sub_hdr->setBunchID(o->bunch_id());
    }
    sub_hdr->setTriggerMask(m);
  }
  

  std::unique_ptr<unsigned char []> _make_tmp_odin_pcie40()   {
    std::size_t len = sizeof(Tell1Bank)+sizeof(pcie40::sodin_t)+sizeof(uint32_t); // 32 bit padding
    std::unique_ptr<unsigned char[]> tmp(new unsigned char[len]);
    ::memset(tmp.get(), 0, len);
    Tell1Bank* b = (Tell1Bank*)tmp.get();
    b->setMagic();
    b->setVersion(0);
    b->setSourceID(0);
    b->setType(Tell1Bank::ODIN);
    b->setSize(sizeof(pcie40::sodin_t));
    b->begin<pcie40::sodin_t>()->_run_number = pcie40::decoder_t::last_good_run_number();
    return tmp;
  }

  size_t _event_length(const EventHandler::evt_data_t& event)
  {
    size_t len = 0;
    for(const auto& bank : event) len += bank.first->totalSize();
    return len;
  }
  
  event_traits::record_t _record(const EventHandler::evt_desc_t& event)
  {
    return event_traits::record_t(event.second->at(event.first));
  }


  template <typename bank_type> size_t
  output_bank(const unique_ptr<RTL::Logger>& logger,
	      EventOutput::transaction_t*    tr,
	      bank_type*                     header,
	      const void*                    data)
  {
    Tell1Bank* bank = tr->get<Tell1Bank>();
    size_t start = tr->length;
    size_t len   = header->totalSize();
    ::memcpy( (void*)bank, header, bank_type::hdrSize());
    ::memcpy( bank->data(), data, len-bank_type::hdrSize());
    bank->setMagic();
    tr->advance(len);
    if ( tr->length-start != len )   {
      logger->error("output_bank: Failed to commit data!");
    }
    return len;
  }

  template <typename bank_type> size_t
  output_bank(const unique_ptr<RTL::Logger>& logger,
	      EventOutput::transaction_t*    tr,
	      const bank_type&               dsc)
  {
    auto* bank = dsc.first;
    switch (bank->magic())   {
    case pcie40::bank_t::MagicPattern:
      return output_bank<pcie40::bank_t>(logger, tr, (pcie40::bank_t*)bank, dsc.second);
    case Tell1Bank::MagicPattern:
      return output_bank<Tell1Bank>(logger, tr, (Tell1Bank*)bank, dsc.second);
    default:
      logger->warning("Bad bank: %d %ld transaction length:%ld",
		      int(bank->type()), bank->totalSize(), tr->length);
      break;
    }
    return 0;
  }

  const char* _mdf_error_string(int code)   {
    switch(code)    {
    case EventHandler::NO_MDF_ERROR:
      return "Success";
    case EventHandler::INVALID_MDF_FORMAT:
      return "INVALID_MDF_FORMAT";
    case EventHandler::INVALID_MAGIC_PATTERN:
      return "INVALID_MAGIC_PATTERN";
    case EventHandler::TOO_MANY_BANKS:
      return "TOO_MANY_BANKS";
    case EventHandler::INVALID_BANK_SIZE:
      return "INVALID_BANK_SIZE";
    default:
      return "Unknown error";
    }
  }
}

/// Online namespace declaration
namespace Online  {

  template <typename T> T EventHandler::mdf_error_report(T return_value, int code)  const {
    m_logger->error("Bank error: %s -- action aborted.", _mdf_error_string(code));
    return return_value;
  }

  string EventHandler::_bx_offset(int bx)   const   {
    string offset;
    int cr = std::abs(bx);
    if ( cr > 99 ) offset += char('0'+((cr/100)%100));
    if ( cr > 9  ) offset += char('0'+((cr/10)%10));
    offset += char('0'+(cr%10));
    return offset;
  }

  /// Check integer array for the occurrence of an item
  bool EventHandler::type_found(int required_type, const std::vector<int>& good_types)  const
  {
    if ( good_types.empty() )
      return true;
    if ( std::find( good_types.begin(), good_types.end(), required_type) == good_types.end() )
      return false;
    return true;
  }

  /// Access to ODIN banks from the event record
  std::pair<const BankHeader*, const void*> EventHandler::get_odin_bank(const evt_desc_t& event)  const
  {
    const auto record = _record(event);
    switch(event.second->type())   {
    case event_traits::tell1::data_type:  {
      const auto* ev = record.tell1_event;
      if ( ev->is_mdf() )   {
	auto data = ev->data_frame();
	while ( data.start < data.end )   {
	  const Tell1Bank* bank = (Tell1Bank*)data.start;
	  if ( bank->type() == Tell1Bank::ODIN )
	    return {bank, bank->data()};
	  size_t len = bank->totalSize();
	  data.start += len;
	  // Protect against infinite loop in case of data corruption
	  if ( 0 == len ) break;
	}
      }
      return make_pair(nullptr, nullptr);
    }
    case event_traits::tell40::data_type:   {
      const auto* bc = record.tell40_event->bank_collection(pcie40::params::collection_id_ODIN);
      const auto* ba = (bc && bc->num_banks() > 0) ? bc->at(0) : nullptr;
      return ba ? make_pair((const BankHeader*)ba, ba->data()) : make_pair(nullptr, nullptr);
    }
    default:
      m_logger->except("EventHandler","+++ Failed to access ODIN [Invalid event type].");
    }
    return make_pair(nullptr, nullptr);
  }

  /// Check if the event is part of a TAE frame
  std::pair<unsigned int, bool> EventHandler::check_tae(const evt_desc_t& event)  const
  {
    auto record = _record(event);
    switch(event.second->type())   {
    case event_traits::tell1::data_type:  {
      const Tell1Bank* bank = record.tell1_event->data<Tell1Bank>();
      if ( bank->magic() == Tell1Bank::MagicPattern &&
	   bank->type()  == Tell1Bank::TAEHeader )   {
	unsigned int count_bx = bank->size()/sizeof(int)/3;
	unsigned int window   = (count_bx-1)/2;
	return {window, true};
      }
      const auto odin = this->get_odin_bank(event);
      if ( odin.first )    {
	if ( odin.first->version() < 7 )   {
	  const auto* o = reinterpret_cast<const RunInfo*>(odin.second);
	  return {o->tae_window(), o->tae_central()};
	}
	const auto* o = reinterpret_cast<const pcie40::sodin_t*>(odin.second);
	return {o->tae_window(), o->tae_central()};
      }
      m_logger->except("EventHandler","+++ Detected event without ODIN bank! [Missing bank]");
      return make_pair(0,false);
    }
    case event_traits::tell40::data_type:   {
      const auto* e = record.tell40_event;
      return e ? make_pair(e->tae_half_window(),e->is_tae_central()) : make_pair(uint8_t(0),false);
    }
    default:
      m_logger->except("EventHandler","+++ Failed to check for TAE [Invalid event type].");
    }
    return make_pair(0,false);
  }

  /// Create raw event object from banks
  std::pair<int,EventHandler::evt_data_t> 
  EventHandler::create_event_data(const evt_desc_t& event, int bx)  const
  {
    switch(event.second->type())   {
    case event_traits::tell1::data_type:  {
      const auto  rec = _record(event);
      auto frame = rec.tell1_event->data_frame();
      Tell1Bank* bank = (Tell1Bank*)frame.start;
      if ( bank->type() == Tell1Bank::TAEHeader ) {   // Is it the TAE bank?
	const int* data = bank->begin<int>();
	int count_bx = bank->size()/sizeof(int)/3;
	int window   = (count_bx-1)/2;
	for(int i=-window; i<=window; ++i)   {
	  int idx = i+window;
	  int crossing = data[3*idx];
	  if ( crossing == bx )  {
	    int off = data[3*idx+1];
	    int len = data[3*idx+2];
	    frame.start += bank->totalSize() + off;
	    return this->extract_tell1_banks(frame.start, frame.start+len);
	  }
	}
	m_logger->except("create_event_data: Wrong BX requested: %d [%d < bx < %d]",
			 bx, -window, window);
	/// Will never get here: except throws exception!
      }
      return this->extract_tell1_banks(frame.start, frame.end);
    }

    case event_traits::tell40::data_type:   {
      const auto  rec = _record({event.first+bx, event.second});
      const auto& evt = *rec.tell40_event;
      evt_data_t evt_data;
      evt_data.reserve(512);
      for(size_t i=0; i < evt.num_bank_collections(); ++i)  {
	const auto* bc = evt.bank_collection(i);
	for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))
	  evt_data.emplace_back((BankHeader*)b, b->data());
	for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))
	  evt_data.emplace_back((BankHeader*)b, b->data());
      }
      return make_pair(NO_MDF_ERROR, move(evt_data));
    }

    default:
      m_logger->except("EventHandler","+++ Failed to access event structure [Invalid event type].");
    }
    return std::pair<int,evt_data_t>(INVALID_MDF_FORMAT,{});
  }

  /// Put bank container to specified location in the TES
  void EventHandler::put_event(const string& loc, const evt_desc_t& event, size_t eid)  const
  {
    auto evt (this->create_event_data({event.first+eid, event.second}, 0));
    put_banks(loc, move(evt.second));
  }
  
  /// Output TAE frame in MDF format
  template <>
  size_t EventHandler::output_tae<event_traits::tell1>(const evt_desc_t& event,
						       mask_t&           mask,
						       EventOutput&      output)       const
  {
    const event_traits::record_t record = _record(event);
    const event_traits::tell1::event_type* evt = record.tell1_event;
    const Tell1Bank* bank = evt->data<Tell1Bank>();

    /// We have here directly a TAE frame. Just copy it to the output
    if ( bank->type() == Tell1Bank::TAEHeader ) {   // Is it the TAE bank?
      size_t  data_len = evt->size0();
      auto*   tr       = output.getTransaction(data_len);
      auto*   ev_hdr   = tr->get<EventHeader>();
      auto    daq      = ev_hdr->subHeader().H1->triggerMask();
      unsigned int m[] = { daq[0], daq[1], daq[2], daq[3] };

      _update_mask(m, mask);
      ::memcpy(tr->pointer(), record.data, data_len);
      ev_hdr->subHeader().H1->setTriggerMask(m);
      tr->advance(data_len);
      if ( !output.end(tr) )   {
	m_logger->except("output_tae: Failed to commit event output!");
      }
      return data_len;
    }
    m_logger->except("output_tae: This event frame is not a TAE frame. Something is inconsistent!");
    return 0;
  }
  
  /// Output TAE frame in MDF format
  template <>
  size_t EventHandler::output_tae<event_traits::tell40>(const evt_desc_t& event,
							mask_t&           mask,
							EventOutput&      output)       const
  {
    typedef event_traits::tell40 traits;
    if( output.isCancelled() )  {
      return 0;
    }
    else if ( mask.empty() )  {
    }
    bool          dbg = false;
    auto          eid = event.first;
    const auto& guard = event.second;
    const auto   odin = this->get_odin_bank(event);
    if ( odin.first )    {
      auto [half_window, central] = this->check_tae(event);
      if ( central )   {
	if ( eid >= half_window && event.second->num_bx()-eid > half_window )   {
	  size_t total_len = 0, tae_len = 0, checked_len = 0;
	  std::vector<size_t> subevt_len;
	  std::vector<pcie40::event_t*> subevents;
	  const size_t hdr_len = EventHeader::sizeOf(EventHeader::CURRENT_VERSION);
	  for(int hw=half_window, iw=-hw; iw <= hw; ++iw)   {
	    auto   evt = guard->at(eid+iw);
	    size_t len = evt.tell40_event->total_length();
	    subevents.emplace_back(evt.tell40_event);
	    subevt_len.emplace_back(len);
	    total_len += len;
	  }
	  tae_len   = Tell1Bank::hdrSize()+subevents.size()*3*sizeof(int);
	  total_len = total_len + hdr_len + tae_len;

	  auto* tr = output.getTransaction(total_len);
	  if ( !tr )   {
	    m_logger->except("Event output failed [Cannot reserve output space]");
	  }
	  size_t buffer_position = tr->used_bytes();
	  unsigned int m[] = { ~0x0U, ~0x0U, ~0x0U, ~0x0U };
	  _update_mask(m, mask);

	  auto* ev_hdr = tr->get<EventHeader>();
	  _setEventHeader(ev_hdr, total_len-hdr_len, odin, m);
	  tr->advance(hdr_len);
	  checked_len += hdr_len;
	  
	  auto* tae_bnk = tr->get<Tell1Bank>();
	  tae_bnk->setMagic();
	  tae_bnk->setVersion(0);
	  tae_bnk->setType(Tell1Bank::TAEHeader);
	  tae_bnk->setSize(subevents.size()*3*sizeof(int));
	  off_t offset = 0;
	  auto* data   = tae_bnk->begin<int>();
	  for(int hw=half_window, i=-hw; i <= hw; ++i)   {
	    *data++  = i;
	    *data++  = offset;
	    *data++  = subevt_len[i+half_window];
	    offset  += subevt_len[i+half_window];
	  }
	  tr->advance(tae_bnk->totalSize());
	  checked_len += tae_bnk->totalSize();
	  if ( dbg )   {
	    if ( tae_bnk->totalSize() != int(tae_len) )   {
	      m_logger->warning("output_tae: inconsistent TAE bank length: %ld != %ld",
				tae_len, tae_bnk->totalSize());
	    }
	  }
	  for(int hw=half_window, iw=-hw; iw <= hw; ++iw)   {
	    size_t bank_len, sub_len = 0, num_bank = 0;
	    auto evt = this->create_event_data(event, iw);
	    if ( evt.first != NO_MDF_ERROR )    {
	      m_logger->warning("output_tae: Invalid TAE subevent at position %d --> drop",iw);
	      tr->position(buffer_position);
	      return 0;
	    }
	    for( auto& dsc : evt.second )   {
	      auto* bank = dsc.first;
	      if ( bank->magic() != traits::bank_type::MagicPattern )   {
		m_logger->warning("Bad bank: %d %ld transaction length:%ld",
				  int(bank->type()), bank->totalSize(), tr->length);
		continue;
	      }
	      bank_len     = output_bank(m_logger, tr, bank, dsc.second);
	      sub_len     += bank_len;
	      checked_len += bank_len;
	      if ( dbg )   {
		if ( checked_len != tr->length )   {
		  m_logger->warning("output_tae: inconsistent transaction length: %ld != %ld",
				    checked_len, tr->length);
		}
	      }
	      ++num_bank;
	    }
	    if ( dbg )   {
	      if ( sub_len != subevt_len[iw+half_window] )    {
		m_logger->warning("output_tae: inconsistent subevent length: %ld != %ld",
				  sub_len, subevt_len[iw+half_window]);
	      }
	    }
	  }
	  if ( checked_len != ev_hdr->recordSize() )   {
	    m_logger->warning("output_tae: inconsistent frame length: %ld != %ld",
			      checked_len, ev_hdr->recordSize());
	  }
	  if ( !output.end(tr) )   {
	    m_logger->except("Failed to commit event output!");
	  }
	  return total_len;
	}
      }
    }
    return 0;
  }
  
  /// Output TAE frame in MDF format
  template <>
  size_t EventHandler::output_tae<void>(const evt_desc_t& event,
					mask_t&           mask,
					EventOutput&      output)       const
  {
    switch(event.second->type())   {
    case event_traits::tell1::data_type:
      return this->output_tae<event_traits::tell1>(event, mask, output);
    case event_traits::tell40::data_type:
      return this->output_tae<event_traits::tell40>(event, mask, output);
    default:
      m_logger->except("output_tae: Invalid burst type encountered: %ld",event.second->type());
    }
    return 0;
  }

  /// Output MDF frame from the event data
  template <typename traits>
  size_t EventHandler::output_mdf(const evt_data_t& event,
				  mask_t&           routing_mask,
				  bool              /* requireODIN */,
				  EventOutput&      output)  const
  {
    typedef typename traits::bank_type bank_type;
    std::pair<const Tell1Bank*, const void*> odin;
    const EventHeader* evthdr = nullptr;
    const uint32_t*  hlt_mask = nullptr;
    const size_t     hdr_len  = EventHeader::sizeOf(EventHeader::CURRENT_VERSION);
    size_t           data_len = hdr_len;

    for( const auto& dsc : event )   {
      auto* bank = dsc.first;
      if ( bank->type() == bank_type::DAQ && bank->version() == DAQ_STATUS_BANK )  {
	evthdr = reinterpret_cast<const EventHeader*>(dsc.second);
	continue;
      }
      else if ( bank->type() == bank_type::ODIN )  {
	odin.first  = dsc.first;
	odin.second = dsc.second;
      }
      else if ( bank->type() == bank_type::HltRoutingBits )  {
	hlt_mask = reinterpret_cast<const uint32_t*>(dsc.second);
      }
      data_len += bank->totalSize();
    }

    /// Create temorary odin bank for event header if not present.
    /// Worst case: run number is 1234 (default from pcie40 decoder)
    std::unique_ptr<unsigned char []> temp_odin_buffer;
    if ( !odin.first )  {
      temp_odin_buffer = _make_tmp_odin_pcie40();
      Tell1Bank* b = (Tell1Bank*)temp_odin_buffer.get();
      odin = std::make_pair(b, b->data());
    }

    unsigned int m[] = { ~0x0U, ~0x0U, ~0x0U, ~0x0U };
    _update_mask(m, routing_mask);
    if ( hlt_mask )    {
      m[0] = hlt_mask[0];
      m[1] = hlt_mask[1];
      m[2] = hlt_mask[2];
    }
    else if ( evthdr )   {
      auto daq_mask = evthdr->subHeader().H1->triggerMask();
      m[0] = daq_mask[0];
      m[1] = daq_mask[1];
      m[2] = daq_mask[2];
    }

    auto* tr = output.getTransaction(data_len);
    if ( !tr )   {
      m_logger->except("Event output failed [Cannot reserve output space]");
    }
    EventHeader* ev_hdr = tr->get<EventHeader>();
    _setEventHeader(ev_hdr, data_len-hdr_len, odin, m);
    /// Patch event header if no odin bank, but original header....
    if ( temp_odin_buffer.get() && evthdr )   {
      auto* h = ev_hdr->subHeader().H1;
      const auto* dh = evthdr->subHeader().H1;
      h->setRunNumber(dh->runNumber());
      h->setOrbitNumber(dh->orbitNumber());
      h->setBunchID(dh->bunchID());
    }
    tr->advance(hdr_len);
    for(const auto& dsc : event)   {
      if ( dsc.first == odin.first )  {
	output_bank(m_logger, tr, dsc.first, odin.second);
      }
      else if ( dsc.second != (void*)evthdr )   {
	output_bank(m_logger, tr, dsc);
      }
    }
    if ( !output.end(tr) )   {
      m_logger->except("Failed to commit event output!");
    }
    return data_len;
  }
      
  /// Output MDF frame from the event data
  size_t EventHandler::output_mdf(const evt_data_t& event,
				  int               type,
				  mask_t&           mask,
				  bool              requireODIN,
				  EventOutput&      output)  const
  {
    switch(type)   {
    case event_traits::tell1::data_type:
      return this->output_mdf<event_traits::tell1> (event, mask, requireODIN, output);
    case event_traits::tell40::data_type:
      return this->output_mdf<event_traits::tell40>(event, mask, requireODIN, output);
    default:
      m_logger->except("output_mdf: Invalid burst type encountered: %ld", type);
    }
    return 0;
  }

  /// Output PCIE40 event/TAE in MDF format
  template <typename traits> size_t
  EventHandler::output_mdf(const evt_desc_t& event,
			   mask_t&           mask,
			   bool              requireODIN,
			   EventOutput&      output)  const
  {
    auto [half_window, central] = this->check_tae(event);
    if ( central && half_window > 0 )   {
      return this->output_tae<traits>(event, mask, output);
    }
    auto data = this->create_event_data(event, 0);
    if ( data.first != NO_MDF_ERROR )   {
      m_logger->error("Bank error: %s -- action aborted.", _mdf_error_string(data.first));
      return 0;
    }
    return this->output_mdf<traits>(data.second, mask, requireODIN, output);
  }
      
  /// Output PCIE40 event in MDF format
  template <> size_t
  EventHandler::output_mdf<void>(const evt_desc_t& event,
				 mask_t&           mask,
				 bool              requireODIN,
				 EventOutput&      output)  const
  {
    switch(event.second->type())   {
    case event_traits::tell1::data_type:
      return this->output_mdf<event_traits::tell1>(event, mask, requireODIN, output);
    case event_traits::tell40::data_type:
      return this->output_mdf<event_traits::tell40>(event, mask, requireODIN, output);
    default:
      m_logger->except("output_mdf: Invalid data type encountered: %ld",event.second->type());
    }
    return 0;
  }

  /// Output MDF/TAE frame from the event data
  size_t EventHandler::output_mdf(const std::vector<std::pair<int, const evt_data_t*> >& crossings,
				  mask_t& mask, EventOutput& output)  const
  {
    if ( crossings.size() == 1 )  {
      const auto& banks = *crossings.at(0).second;
      if ( !banks.empty() )   {
	if ( banks.at(0).first->magic() == pcie40::bank_t::MagicPattern )
	  return output_mdf<event_traits::tell40>(banks, mask, true, output);
	else if ( banks.at(0).first->magic() == Tell1Bank::MagicPattern )
	  return output_mdf<event_traits::tell1>(banks, mask, true, output);
      }
      return 0;
    }
    std::pair<const Tell1Bank*, const void*> odin(nullptr, nullptr);
    size_t hdr_len = EventHeader::sizeOf(EventHeader::CURRENT_VERSION);
    size_t total_len = hdr_len + Tell1Bank::hdrSize()+crossings.size()*3*sizeof(int);
    for(const auto& cr : crossings)   {
      total_len += _event_length(*cr.second);
      if ( 0 == cr.first )   {
	for(const auto& b : *cr.second)   {
	  if ( b.first->type() == Tell1Bank::ODIN )   {
	    odin = {b.first, b.second};
	    break;
	  }
	}
      }
    }
    /// Create temorary odin bank for event header if not present.
    /// Worst case: run number is 1234 (default from pcie40 decoder)
    std::unique_ptr<unsigned char []> temp_odin_buffer;
    if ( !odin.first )  {
      temp_odin_buffer = _make_tmp_odin_pcie40();
      Tell1Bank* b = (Tell1Bank*)temp_odin_buffer.get();
      odin = std::make_pair(b, b->data());
    }

    auto* tr = output.getTransaction(total_len);
    if ( !tr )   {
      m_logger->except("Event output failed [Cannot reserve output space]");
    }
    unsigned int m[] = { ~0x0U, ~0x0U, ~0x0U, ~0x0U };
    _update_mask(m, mask);

    auto* ev_hdr = tr->get<EventHeader>();
    _setEventHeader(ev_hdr, total_len-hdr_len, odin, m);
    tr->advance(hdr_len);
	  
    auto* tae_bnk = tr->get<Tell1Bank>();
    tae_bnk->setMagic();
    tae_bnk->setVersion(0);
    tae_bnk->setType(Tell1Bank::TAEHeader);
    tae_bnk->setSize(crossings.size()*3*sizeof(int));
    off_t offset = 0;
    auto* data   = tae_bnk->begin<int>();
    tr->advance(tae_bnk->totalSize());
    for(const auto& cr : crossings)   {
      size_t len = 0;
      for( auto& dsc : *cr.second )
	len += output_bank(m_logger, tr, dsc);
      *data++  = cr.first;
      *data++  = offset;
      *data++  = len;
      offset  += len;
    }
    if ( !output.end(tr) )   {
      m_logger->except("Failed to commit event output!");
    }
    return total_len;
  }

  /// Output PCIE40 event in MDF format
  void EventHandler::output_pcie40(const EventAccess::guard_t* guard, EventOutput& output)  const
  {
    if ( guard && !guard->empty() )   {
      EventAccess::record_t  rec = guard->at(0);
      const pcie40::event_collection_t* events =
	pcie40::add_ptr<pcie40::event_collection_t>(rec.data,
						    - sizeof(pcie40::event_collection_t::capacity)
						    - sizeof(pcie40::event_collection_t::length)
						    - sizeof(pcie40::event_collection_t::mep)
						    - sizeof(pcie40::event_collection_t::magic));
      if ( nullptr != events->mep )   {
	auto* tr = output.getTransaction(events->mep->size);
	if ( !tr )   {
	  m_logger->except("+++ Event output failed [Cannot reserve output space]");
	}
	tr->add_data(EventAccess::datapointer_t(events->mep), events->mep->size);
	if ( !output.end(tr) )   {
	  m_logger->except("+++ Failed to commit event output!");
	}
      }
    }
  }

  /// Extract error banks from the event data
  EventHandler::evt_data_t
  EventHandler::get_errors(event_traits::record_t record)  const
  {
    evt_data_t banks;
    if ( record.is_tell40() )   {
      auto& event = *(record.tell40_event);
      for(size_t i=0; i < event.num_bank_collections(); ++i)  {
	const auto* bc = event.bank_collection(i);
	for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))    {
	  if ( b->type() >= b->DaqErrorFragmentThrottled )
	    banks.emplace_back((event_traits::tell1::bank_type*)b, b->data());
	}
	for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))   {
	  if ( b->type() >= b->DaqErrorFragmentThrottled )
	    banks.emplace_back((event_traits::tell1::bank_type*)b, b->data());
	}
      }
    }
    return banks;
  }

  /// Extract error banks from the event data
  EventHandler::evt_data_t
  EventHandler::get_errors(evt_desc_t& event)  const
  {
    const auto record(event.second->at(event.first));
    return this->get_errors(record);
  }

  /// Create raw event bank descriptors from raw buffer
  pair<int,EventHandler::evt_data_t>
  EventHandler::extract_tell1_banks(const unsigned char* start_ptr,
				    const unsigned char* end_ptr)  const
  {
    const auto* start = start_ptr, *end = end_ptr;
    size_t num_banks = 0, max_banks = 3000;
    int error = NO_MDF_ERROR;
    evt_data_t evt_data;
    evt_data.reserve(1024);
    while ( start < end )  {
      Tell1Bank* bank = (Tell1Bank*)start;
      if ( bank->magic() != Tell1Bank::MagicPattern )   {
	m_logger->error("extract_banks: %s [%s] Start:%p (%p) End: %p",
			"Invalid bank", "Bad magic pattern",
			start_ptr, start, end_ptr);
	m_logger->error("extract_banks: %s", Tell1Printout::bankHeader(bank).c_str());
	error = INVALID_MAGIC_PATTERN;
	break;
      }
      evt_data.emplace_back(bank, bank->data());
      start += bank->totalSize();
      if ( bank->type() == Tell1Bank::TAEHeader )   {
	max_banks = 3000 * bank->size()/(3*sizeof(int));
      }
      if ( ++num_banks < max_banks ) continue;
      m_logger->error("Exceeded maximum number of banks: %ld > %ld  [%s]",
		      num_banks, max_banks, "Banks were dropped");
      error = TOO_MANY_BANKS;
      break;
    }
    return make_pair(error,move(evt_data));
  }

  /// Extract banks from event descriptor
  template <> pair<int,EventHandler::evt_data_t>
  EventHandler::extract_banks<event_traits::tell1>(const evt_desc_t& e)  const
  {
    const auto record(e.second->at(e.first));
    if ( record.tell1_event->is_mdf() )   {
      auto frame = record.tell1_event->data_frame();
      return this->extract_tell1_banks(frame.start, frame.end);
    }
    m_logger->except("Attempt to extract Tell1 banks from non-Tell1 frame.");
    return pair<int,evt_data_t>(INVALID_MDF_FORMAT,{});
  }

  /// Extract banks from event descriptor
  template <> pair<int,EventHandler::evt_data_t>
  EventHandler::extract_banks<event_traits::tell40>(const evt_desc_t& event)  const
  {
    const auto record(event.second->at(event.first));
    const event_traits::tell40::event_type* e = record.tell40_event;
    evt_data_t evt_data;
    evt_data.reserve(1024);
    for(size_t i=0; i < e->num_bank_collections(); ++i)  {
      const auto* bc = e->bank_collection(i);
      for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))   {
	evt_data.emplace_back((BankHeader*)b, b->data());
      }
      for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))   {
	evt_data.emplace_back((BankHeader*)b, b->data());
      }
    }
    return make_pair(NO_MDF_ERROR,move(evt_data));
  }

  /// Extract banks from event descriptor
  template <> pair<int,EventHandler::evt_data_t>
  EventHandler::extract_banks<void>(const evt_desc_t& event)  const
  {
    switch(event.second->type())   {
    case event_traits::tell1::data_type:
      return this->extract_banks<event_traits::tell1>(event);
    case event_traits::tell40::data_type:
      return this->extract_banks<event_traits::tell40>(event);
    default:
      m_logger->error("+++ Algorithm failed [Invalid event type].");
    }
    return pair<int,evt_data_t>(INVALID_MDF_FORMAT,{});
  }
  
  /// Dump event structure to output
  void EventHandler::dump_event(evt_desc_t& e, int flags)  const
  {
    auto* log = m_logger.get();
    bool dump_events = (flags&DUMP_EVENTS) != 0;
    bool dump_bank_headers = (flags&DUM_BANK_HEADERS) != 0;
    bool dump_bank_collections = (flags& DUMP_BANK_COLLECTIONS) != 0;
    if ( dump_events )    {
      auto evt_data = extract_banks(e);
      if ( evt_data.second.empty() )  {
	return;
      }
      if ( dump_bank_collections )   {
	log->always("+=====================================================================");
      }
      size_t data_len = 0;
      map<int,size_t> bank_types, bank_data_len;
      for( const auto& b : evt_data.second )   {
	data_len += b.first->size();
	bank_types[b.first->type()] += 1;
	bank_data_len[b.first->type()] += b.first->size();
      }
      log->always("| ++  Event[%s]:   %ld banks  %ld bytes",
		  e.second->type()==event_traits::tell1::data_type ? "TELL1" : "PCIE40",
		  evt_data.second.size(), data_len);
      if ( dump_bank_collections )   {
	for ( const auto& t : bank_types)   {
	  log->always("| ++====>  Bank collection: %3d %-15s Number: %4d  %6ld",
		      t.first, Online::Tell1Printout::bankType(t.first).c_str(),
		      t.second, bank_data_len[t.first]);
	  if ( dump_bank_headers )   {
	    for( const auto& b : evt_data.second )   {
	      if ( b.first->type() == t.first )   {
		log->always("+++  %s", Online::Tell1Printout::bankHeader(b.first).c_str());
	      }
	    }
	  }
	}
      }
    }
  }
  
  /// On the occurrence of a TAE frame expand data and populate TES locations
  template <>
  size_t EventHandler::expand_tae<event_traits::tell1>(const string& prefix,
						       const evt_desc_t&  event)  const
  {
    size_t      eid  = event.first;
    const auto* hdr = event.second->at(eid).tell1_event;
    if ( hdr && hdr->is_mdf() )   {
      const auto* tae = (const Tell1Bank*)hdr->data();
      if ( tae->magic() != Tell1Bank::MagicPattern )   {
	m_logger->except("expand_tae: Invalid TAE bank [Invalid magic pattern] %04X", tae->magic());
	return 0;
      }
      if ( tae->type() != Tell1Bank::TAEHeader )   {
	const uint8_t* start = hdr->data();
	auto banks(this->extract_tell1_banks(start, start+hdr->size()));
	if ( banks.first != NO_MDF_ERROR )   {
	  return 0;
	}
	this->put_banks(prefix + "/Central", move(banks.second));
	return 1;
      }
      const int* data  = tae->begin<int>();
      int    count_bx  = tae->size()/sizeof(int)/3;
      int    half_win  = (count_bx-1)/2;
      string pref_prev = prefix + "/Prev", pref_next = prefix + "/Next";
      const uint8_t* start = ((const uint8_t*)tae)+tae->totalSize();

      for(int i=-half_win; i<=half_win; ++i)   {
	int idx = i + half_win;
	int bx  = data[3*idx];
	int off = data[3*idx+1];
	int len = data[3*idx+2];

	auto banks(this->extract_tell1_banks(start+off, start+off+len));
	if ( banks.first != NO_MDF_ERROR )    {
	  return mdf_error_report(0, banks.first);
	}
	if ( bx < 0 )
	  this->put_banks(pref_prev + _bx_offset(bx), move(banks.second));
	else if ( bx > 0 )
	  this->put_banks(pref_next + _bx_offset(bx), move(banks.second));
	else
	  this->put_banks(prefix + "/Central", move(banks.second));
      }
      return 2*half_win + 1;
    }
    m_logger->except("expand_tae: Invalid Tell1 TAE frame encountered. Skip event");
    return 0;
  }

  /// On the occurrence of a TAE frame expand data and populate TES locations
  template <>
  size_t EventHandler::expand_tae<event_traits::tell40>(const string&      prefix,
							const evt_desc_t&  event)  const
  {
    size_t    eid = event.first;
    auto   record = event.second->at(eid);
    if ( record.is_tell40() )   {
      auto  central = record.tell40_event->is_tae_central();
      auto half_win = record.tell40_event->tae_half_window();
      if ( central )   {
	if ( eid >= half_win && event.second->num_bx()-eid > half_win )   {
	  string pref_prev = prefix + "/Prev", pref_next = prefix + "/Next";
	  for (int bx=-half_win; bx<=half_win; ++bx )  {
	    if ( bx < 0 )
	      this->put_event(pref_prev + _bx_offset(bx), event, -bx);
	    else if ( bx > 0 )
	      this->put_event(pref_next + _bx_offset(bx), event, +bx);
	    else
	      this->put_event(prefix + "/Central", event, 0);
	  }
	  return 2*half_win + 1;
	}
      }
      return 1;
    }
    m_logger->except("expand_tae: Invalid Tell40 TAE frame encountered. Skip event");
    return 0;
  }

  /// On the occurrence of a TAE frame expand data and populate TES locations
  template <>
  size_t EventHandler::expand_tae<void>(const string& prefix,
					const evt_desc_t&   event)   const
  {
    switch(event.second->type())   {
    case event_traits::tell1::data_type:
      return this->expand_tae<event_traits::tell1>(prefix, event);
    case event_traits::tell40::data_type:
      return this->expand_tae<event_traits::tell40>(prefix, event);
    default:
      m_logger->except("expand_tae: Invalid burst type encountered: %ld",event.second->type());
    }
    return 0;
  }
  
}  // End namespace Online
