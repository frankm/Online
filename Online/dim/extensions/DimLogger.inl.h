//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  DimLogger
//--------------------------------------------------------------------------
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

#include "DimLogger.h"
#include <dim/dis.h>

#include <thread>
#include <memory>
#include <mutex>
#include <atomic>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#include <fcntl.h>

using namespace dim;
using namespace std;

class Logger::Implementation    {
public:
  typedef std::unique_ptr<std::thread> thread_t;
  std::mutex  lock;
  std::string utgid;
  std::string real_host;
  std::string physical_host;
  std::string stdout_buffer;
  std::string stderr_buffer;
  std::string output_buffer;
  std::atomic<unsigned long> msg_count;
  thread_t    runner;
  int         service      = -1;
  int         epoll_fd     = -1;
  int         stdout_fd[2] = {-1, -1};
  int         stderr_fd[2] = {-1, -1};
  bool        done = false;
  Implementation() = default;
  ~Implementation() = default;
  /// Object initialization
  void init(long dns_id, const string& utgid);
  /// Read single data frame
  void read_data(int fd, string& data);
  /// Poll data from stdout/stderr pipes
  void poll();
  /// Stop processing and shutdown
  void stop();
};

/// Anonymous namespace for helpers
namespace   {

  /// Dim service callback to serve data
  void feed_logger(void* tag, void** buf, int* size, int* /* first */)   {
    static string empty("");
    if ( tag && buf && size )   {
      Logger::Implementation* h = *(Logger::Implementation**)tag;
      *size = h->output_buffer.length()+1;
      *buf  = (void*)h->output_buffer.c_str();
      return;
    }
    *size = 0;
    *buf  = (void*)empty.c_str();
  }

  /// Translate errno into error string
  string error(int err)   {
    char   text[1024];
    string e = ::strerror_r(err, text, sizeof(text));
    return e;
  }

  std::unique_ptr<Logger>& logger()   {
    static std::unique_ptr<Logger> log;
    if ( !log.get() )    {
      char text1[256], text2[256];
      const char* utgid = ::getenv("UTGID");
      const char* dns   = ::getenv("DIM_HOST_NODE");
      int dns_port      = ::dis_get_dns_port();
      if ( !dns )    {
	::gethostname(text1,sizeof(text1));
	dns = text1;
      }
      if ( !utgid )    {
	::snprintf(text2,sizeof(text2),"P%08d",::getpid());
	utgid = text2;
      }
      ::fprintf(stderr, "Initializing dim logger....utgid:%s dns:%s\n", utgid, dns);
      long dns_id = ::dis_add_dns(dns, dns_port);
      log.reset(new Logger(dns_id, utgid));
    }
    return log;
  }
}

/// Object initialization
void Logger::Implementation::init(long dns_id, const string& utgid)   {
  int    ret = 0, fd;
  string svc = utgid + "_LOGS/log";
  this->utgid     = utgid + "                                                  ";
  this->utgid     = this->utgid.substr(0,32);
  this->msg_count = 0;
  this->service   = ::dis_add_service_dns(dns_id,svc.c_str(),"C",0,0,feed_logger,(long)this);
  this->epoll_fd  = ::epoll_create(10);
  ///
  ret = ::pipe(this->stdout_fd);
  if ( ret != 0 )   {
    ::printf("+++ Failed to create stdout pipe. [%s]\n", error(errno).c_str());
  }
  fd  = ::fileno(stdout);
  ret = ::dup2(this->stdout_fd[1],fd);
  if ( ret == -1 )   {
    ::printf("+++ Failed to dup stdout. [%s]\n", error(errno).c_str());
  }
  ///
  ret = ::pipe(this->stderr_fd);
  if ( ret != 0 )   {
    ::printf("+++ Failed to create stderr pipe. [%s]\n", error(errno).c_str());
  }
  fd  = ::fileno(stderr);
  ret = ::dup2(this->stderr_fd[1],fd);
  if ( ret == -1 )   {
    ::printf("+++ Failed to dup stderr. [%s]\n", error(errno).c_str());
  }
  ///
  struct epoll_event ev;
  ev.events   = EPOLLIN | EPOLLRDHUP | EPOLLERR | EPOLLHUP | EPOLLET;
  ///
  ev.data.fd  = this->stdout_fd[0];
  ::fcntl(this->stdout_fd[0], F_SETFL, O_NONBLOCK);
  ret = ::epoll_ctl(this->epoll_fd, EPOLL_CTL_ADD, this->stdout_fd[0], &ev);
  if ( ret != 0 )    {
    ::printf("+++ Failed to add FD %d to epoll structure. [%s]\n",
	     this->stdout_fd[0], error(errno).c_str());
  }
  ev.data.fd  = this->stderr_fd[0];
  ::fcntl(this->stderr_fd[0], F_SETFL, O_NONBLOCK);
  ret = ::epoll_ctl(this->epoll_fd, EPOLL_CTL_ADD, this->stderr_fd[0], &ev);
  if ( ret != 0 )    {
    ::printf("+++ Failed to add FD %d to epoll structure. [%s]\n",
	     this->stdout_fd[0], error(errno).c_str());
  }
  char host[128];
  ::gethostname(host,sizeof(host));
  this->physical_host = host;
  this->real_host     = host;
  ::setvbuf(stdout, nullptr, _IOLBF, 0);
  ::setvbuf(stderr, nullptr, _IOLBF, 0);
  ::dis_start_serving_dns(dns_id,(utgid+"_LOGS").c_str());
}

/// Read single data frame
void Logger::Implementation::read_data(int fd, string& data)    {
  char    text[512];
  ssize_t nbyt = ::read(fd, text, sizeof(text)-2);
  if ( nbyt <= 0 )    {
    return;
  }
  text[nbyt] = 0;
  for(char* p = text; *p; ++p)   {
    if ( *p == '\n' || *p == '\0' )   {
      struct tm tim;
      char time_str[128];
      time_t now = ::time(0);
      ::localtime_r(&now, &tim);
      ::strftime(time_str,sizeof(time_str),"{\"@timestamp\":\"%Y-%m-%dT%H:%M:%S.000000%z\",",&tim);
      {
	lock_guard<mutex> guard(this->lock);
	this->output_buffer.reserve(512);
	this->output_buffer  = time_str;
	this->output_buffer += "\"message\":\"";
	this->output_buffer += this->utgid;
	this->output_buffer += data;
	this->output_buffer += "\",\"physical_host\":\"";
	this->output_buffer += this->physical_host;
	this->output_buffer += "\",\"host\":\"";
	this->output_buffer += this->real_host;
	this->output_buffer += "\"}";
	++this->msg_count;
	::dis_update_service(this->service);
      }
      data = "";
      data.reserve(512);
      continue;
    }
    data += *p;
  }
}

/// Poll data from stdout/stderr pipes
void Logger::Implementation::poll()    {
  while ( 1 )    {
    struct epoll_event ep[2];
    int nc = ::epoll_wait(this->epoll_fd, ep, sizeof(ep)/sizeof(ep[0]), 100);
    if ( nc > 0 )    {
      for( int i=0; i < nc; ++i )   {
	if ( ep[i].events&EPOLLIN )   {
	  if      ( ep[i].data.fd == this->stdout_fd[0] )
	    this->read_data(this->stdout_fd[0], this->stdout_buffer);
	  else if ( ep[i].data.fd == this->stderr_fd[0] )
	    this->read_data(this->stderr_fd[0], this->stderr_buffer);
	}
      }
    }
    if ( this->done )    {
      break;
    }
  }
}

/// Stop processing and shutdown
void Logger::Implementation::stop()    {
  this->done = true;
  this->runner->join();
  ::dis_remove_service(this->service);
  this->service = 0;
}

/// Initializing constructor
Logger::Logger(long dns_id, const string& utgid)   {
  this->imp = make_unique<Implementation>();
  this->imp->init(dns_id, utgid);
}

/// Run the logger in a separate thread
void Logger::run()    {
  this->imp->runner = make_unique<thread>([this]()  {  this->imp->poll(); });
}

/// Stop processing and shutdown
void Logger::stop()    {
  this->imp->stop();
}

