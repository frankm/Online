#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/dim
----------
#]=======================================================================]

add_compile_definitions(PROTOCOL=1)
online_install_includes(include)

online_library(dim
        src/conn_handler.c
        src/copy_swap.c
        src/dic.c
        src/dim_thr.c
        src/dis.c
        src/dll.c
        src/dna.c
        src/dtq.c
        src/hash.c
        src/open_dns.c
        src/sll.c
        src/swap.c
        src/tcpip.c
        src/utilities.c
        src/diccpp.cxx
        src/dimcpp.cxx
        src/discpp.cxx
        src/tokenstring.cxx)
target_include_directories(dim PRIVATE include/dim)
target_include_directories(dim INTERFACE
                           $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/dim>
                           $<INSTALL_INTERFACE:include/dim>
)
target_compile_definitions(dim PUBLIC MIPSEL)
target_compile_options(dim PRIVATE -Wno-unused  -Wno-format -Wno-format-security)
target_link_libraries( dim PUBLIC Threads::Threads ${CMAKE_DL_LIBS} -lrt)
if(NOT CMAKE_CXX_COMPILER_ID STREQUAL  "Clang")
    target_compile_options(dim
        PRIVATE
            -Wno-misleading-indentation -Wno-stringop-truncation
            $<$<COMPILE_LANGUAGE:C>:    -Wno-discarded-qualifiers>
            $<$<COMPILE_LANGUAGE:CXX>:  -Wno-maybe-uninitialized>
    )
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND CMAKE_CXX_COMPILER_VERSION VERSION_GREATER 10.0)
    target_compile_options(dim
        PRIVATE
	    -Wno-misleading-indentation -Wno-incompatible-pointer-types-discards-qualifiers
    )
else()
    target_compile_options(dim
        PRIVATE
            $<$<COMPILE_LANGUAGE:C>:   -Wno-discarded-qualifiers>
            $<$<COMPILE_LANGUAGE:CXX>: -Wno-sometimes-uninitialized>
    )
endif()

macro(dim_exe_basic name)
    online_executable(${name} ${ARGN})
    target_compile_options(${name} PRIVATE -Wno-format
                                           -Wno-format-security
                                           -Wno-unused
    				   	   -Wno-parentheses
    				   	   -Wno-implicit-fallthrough
                                           -Wno-shadow
	   				   $<$<COMPILE_LANGUAGE:C>: -Wno-int-conversion>
					   $<$<COMPILE_LANGUAGE:CXX>: -Wno-suggest-override>
			   )
    if(CMAKE_CXX_COMPILER_ID STREQUAL  "Clang")
    	   target_compile_options(${name} PRIVATE
					  -Wno-unused-parameter -Wno-misleading-indentation
					  -Wno-incompatible-pointer-types-discards-qualifiers
			   )
    else()
    	   target_compile_options(${name} PRIVATE
	               			  $<$<COMPILE_LANGUAGE:C>: -Wno-discarded-qualifiers>
					  -Wno-cast-function-type
	   				  -Wno-stringop-truncation
           )
    endif()
    target_link_libraries(${name} PRIVATE Online::dim)
endmacro()

macro(dim_exe name)
    dim_exe_basic(${name} ${ARGN})
endmacro()

dim_exe_basic(did.exe src/did/make_did.c)
target_include_directories(did.exe PRIVATE src/did ${MOTIF_INCLUDE_DIR})
target_compile_options(did.exe PRIVATE -Wno-incompatible-pointer-types)
target_link_libraries(did.exe  PRIVATE Online::dim ${MOTIF_LIBRARIES} X11::X11 X11::Xt)

dim_exe(webdid
    src/webDid/webDid.c
    src/webDid/webServer.c
    src/webDid/webSmiD.c
    src/webDid/webTcpip.c
)
target_compile_options(webdid PRIVATE -Wno-long-long)

dim_exe(dns.exe                     src/dns.c)
dim_exe(dim_check_dns               src/util/check_dns.cxx)
dim_exe(dim_get_service_names       src/util/dim_get_service_names.cxx)
dim_exe(dimbridge                   src/util/dimbridge.cxx)
dim_exe(dim_get_service             src/util/dim_get_service.c)
dim_exe(dim_get_services            src/util/dim_get_services.cxx)
dim_exe(dim_send_command            src/util/dim_send_command.c)

dim_exe(dim_test_test_server        src/examples/test_server.c)
dim_exe(dim_test_test_client        src/examples/test_client.c)
dim_exe(dim_test_big_server         src/examples/test_big_server.c)
dim_exe(dim_test_big_client         src/examples/test_big_client.c)
dim_exe(dim_test_demo_server        src/examples/demo_server.c)
dim_exe(dim_test_demo_client        src/examples/demo_client.c)
dim_exe(dim_test_rpc_server         src/examples/rpc_server.cxx)
dim_exe(dim_test_rpc_client         src/examples/rpc_client.cxx)
dim_exe(dim_test_pvss_dim_server    src/examples/pvss_dim_server.cxx)
dim_exe(dim_test_pvss_dim_client    src/examples/pvss_dim_client.cxx)
dim_exe(dim_test_benchServer        src/benchmark/benchServer.cxx)
dim_exe(dim_test_benchClient        src/benchmark/benchClient.cxx)
dim_exe(dim_test_output_server      src/examples/test_output_server.cxx)
# dim_exe(dim_test_bigServer        src/benchmark/bigServer.cxx)
dim_exe(dim_test_ex_dim_fork        src/examples/dim_fork.cxx)
dim_exe(dim_test_ex_dim_fork2       src/examples/dim_fork2.cxx)
dim_exe(dim_test_ex_rpc_server      src/examples/rpc_server.cxx)
dim_exe(dim_test_ex_rshServer       src/examples/rshServer.cxx)


