//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//  ROMon
//-------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================

// Framework includes
#include <GauchoServer/rpc_server_relay.h>
#include <Gaucho/CounterJson.h>
#include <Gaucho/HistJson.h>
#include <Gaucho/TaskRPC.h>
#include <Gaucho/dimhist.h>
#include <Gaucho/RPCdefs.h>
#include <CPP/IocSensor.h>
#include <CPP/TimeSensor.h>
#include <CPP/Interactor.h>
#include <CPP/Event.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <nlohmann/json.hpp>

// C++ include files
#include <iostream>
#include <stdexcept>
#include <fstream>
#include <cerrno>
#include <chrono>


using namespace Online;


namespace my_tests {

  struct my_client : public CPP::Interactor, virtual public rpc_server_relay::client_t {

    enum { CMD_CONNECT = 1, CMD_REMOVE = 2, CMD_GETCOUNTERS = 3 };

    static constexpr const unsigned long HIST_CALL    = 0x1UL;
    static constexpr const unsigned long COUNTER_CALL = 0x2UL;
    typedef nlohmann::json json;

    virtual void on_response(rpc_t* rpc, const void* data, size_t len)  override;

    virtual void on_nolink(rpc_t* rpc_ptr)   override  {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ NoLink from rpc service %-12s :: %-32s",
		       rpc_ptr->server->dns->name.c_str(), rpc_ptr->service.c_str());
    }
    virtual void on_connect_rpc(rpc_t* rpc_ptr)   override   {
      auto* s = rpc_ptr->server;
      ::lib_rtl_output(LIB_RTL_INFO,"%p +++ Add rpc service %-12s :: %-32s -> %s",
		       rpc_ptr, s->dns->name.c_str(), s->name.c_str(), rpc_ptr->service.c_str() );
      if ( rpc_ptr->service.find("/Counter/HistCommand") != std::string::npos )   {
	TimeSensor::instance().add(this, 5, rpc_ptr);
      }
    }

    virtual void on_disconnect_rpc(rpc_t* /* rpc_ptr */)  override  {
    }

    void get_counters(rpc_t* rpc_ptr)    {
      if ( rpc_ptr )   {
	const auto& srv = rpc_ptr->server;
	const auto& dns = srv->dns;
	const auto& con = srv->connections;
	for(const auto& c : con)   {
	  if ( c.second->service.find("/Counter/") != std::string::npos )   {
	    auto tname = RTL::str_split(c.second->service,"/")[0];
	    auto res = this->task_counter_directory(dns->name, tname);
	    std::cout << res << std::endl;
	  }
	}
	//int dat = RPCCDirectory;
	//rpc_ptr->call(&dat, sizeof(int));
      }
    }

    /// Interactor interrupt handler callback
    void handle(const Event& event)  override   {
      switch(event.eventtype) {
      case IocEvent:   {
	switch(event.type) {
	case CMD_GETCOUNTERS:
	  this->get_counters(event.iocPtr<rpc_t>());
	default:
	  break;
	}
	break;
      }
      case TimeEvent:
	IocSensor::instance().send(this, CMD_GETCOUNTERS, event.timer_data);
	break;
      case UpiEvent:
	break;
      default:
	break;
      }
    }


    struct {
      long numRequest     = 0;
      long numSuccess     = 0;
      long numError       = 0;
      long numCacheDrop   = 0;
      long numCacheInsert = 0;
      long numCacheReuse  = 0;
    } m_counters;
    static constexpr unsigned int MAGIC_PATTERN = 0xFEEDBABE;

    struct call_context_t {
      unsigned int             magic { MAGIC_PATTERN };
      int                      ready { 0 };
      std::mutex               lock;
      std::condition_variable  condition;
      json                     answer;
      rpc_server_relay::rpc_t* rpc      { nullptr };
      void*                    request  { nullptr };
      size_t                   len   { 0 };
      call_context_t(rpc_server_relay::rpc_t* r, void* d, size_t l)
	: rpc(r), request(d), len(l)  {}
      ~call_context_t()   {
	len = 0UL;
	rpc = nullptr;
	if ( request ) ::operator delete(request);
      }
      template <typename T=void> T* data()  {  return (T*)this->request; }
      int wait_for(std::chrono::milliseconds timeout)    {
	std::unique_lock<std::mutex> guard(this->lock);
	auto result = this->condition.wait_for(guard, timeout, [this] { return this->ready > 0; });
	this->magic = 0;
	return result;
      }
      template <typename DATA> 
      static void on_ready(call_context_t* context, DATA&& names)   {
	if ( context && context->magic == MAGIC_PATTERN )    {
	  context->answer = names;
	  context->ready  = 1;
	  context->condition.notify_one();
	}
      }
    };

    std::shared_ptr<call_context_t> create_context(rpc_server_relay::rpc_t* rpc, RPCCommType typ, size_t data_length);

    std::vector<std::string> decode_names(const void* beg, const void* end)   const {
      std::vector<std::string> names;
      while ( beg < end )     {
	int titoff = 4;
	const char *tptr = add_ptr<char>(beg, titoff);
	if ( !tptr ) break;
	names.push_back(tptr);
	int recl = 4 + ::strlen(tptr)+1;
	beg = add_ptr(beg,recl);
      }
      return names;
    }

    std::map<std::string, const void*> decode_objects(const void* beg, const void* end)   const {
      std::map<std::string, const void*> objects;
      while ( beg < end )     {
#if 0
	int titoff = 4;
	const char *tptr = add_ptr<char>(beg, titoff);
	if ( !tptr ) break;
	names.push_back(tptr);
	int recl = 4 + ::strlen(tptr)+1;
	beg = add_ptr(beg,recl);
#endif
      }
      return objects;
    }

    bool do_call(std::shared_ptr<rpc_server_relay::rpc_t>& server_rpc, std::shared_ptr<call_context_t>& ctx )   {
      this->_current_context = ctx.get();
      server_rpc->call(ctx->data(), ctx->len);
      int stat = ctx->wait_for(this->rpc_timeout);
      this->_current_context = nullptr;
      if ( stat )   {
	return true;
      }
      return false;
    }

    typedef rpc_server_relay relay;
    std::unique_ptr<relay>  rpc  {};
    std::regex histo_match  {};
    std::regex counter_match  {};
    std::mutex message_id_lock  {};
    std::vector<std::unique_ptr<call_context_t> > calls  {};
    std::chrono::milliseconds rpc_timeout { 1000 };
    int last_message_id {0};

    std::mutex      _context_lock  {}; 
    call_context_t* _current_context { nullptr };

    /// Check dns string for validity
    static bool _check_dns(my_client* rpc, const std::string& dns);
    /// Check dns and task string for validity
    static bool _check_dns_task(my_client* rpc, const std::string& dns, const std::string& task);

    int _new_message_id();
    std::shared_ptr<rpc_server_relay::rpc_t> get_rpc(const std::string& dns, const std::string& task, const std::regex& match);
    
    /// Access matching tasks by regex of a given dns
    json _taskservice_matches(const std::string& dns, const std::regex& match);
    /// Access matching counter tasks of a given dns
    json counter_tasks(const std::string& dns);
    /// Access matching histogram tasks of a given dns
    json histogram_tasks(const std::string& dns);

    /// Emit the directory data of a specified task according to a given match
    json _task_directory(const std::string& dns, const std::string& task, const std::regex& match);
    /// Emit the counter directory of a specified task
    json task_counter_directory(const std::string& dns, const std::string& task);
    /// Access counter list by regular expression
    json task_counter(const std::string& dns, const std::string& task, const std::string& selection);
    /// Access all taskcounter
    json task_counters(const std::string& dns, const std::string& task);
    /// Access counter list by regular expression
    json task_counters_regex(const std::string& dns, const std::string& task, const std::string& selection);

    /// Emit the histogram directory of a specified task
    json task_histogram_directory(const std::string& dns, const std::string& task);

  };

  void my_client::on_response(rpc_t* r, const void* data, size_t len)  {
    if ( data && len )   {
      if ( r->rpc_type == COUNTER_CALL )   {
	int secs = 0, milli = 0;
	r->get_time_stamp(secs, milli);
	const RPCReplyCookie *rep = (const RPCReplyCookie*)data;
	typedef  std::map<const char*, const void*> PTRMAP;
	const void* valin  = data;
	const void* valend = add_ptr(valin, len);
	PTRMAP  hists;

	switch(rep->comm)    {
	case RPCCRead:
	case RPCCReadAll:
	case RPCCReadRegex:
	case RPCCClear:
	case RPCCClearAll:    {
	  const auto *ptr = add_ptr<DimHistbuff1>(valin, sizeof(RPCReply));
	  while (ptr < valend)    {
	    hists.emplace(add_ptr<char>(ptr, ptr->nameoff), ptr);
	    if ( ptr->reclen <= 0 ) break;
	    ptr = add_ptr<DimHistbuff1>(ptr,ptr->reclen);
	  }
	  break;
	}
	case RPCCDirectory:    {
	  auto names = this->decode_names(add_ptr(valin, sizeof(RPCReply)), valend);
	  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Got answer from rpc service %-12s :: %-32s  %08d:%03d  %ld objects",
			   r->server->dns->name.c_str(), r->service.c_str(), secs, milli, names.size());
	  call_context_t::on_ready(this->_current_context, std::move(names));
	  break;
	}
	case RPCCDirectoryCookie:    {
	  auto names = this->decode_names(add_ptr(valin, sizeof(RPCReplyCookie)), valend);
	  call_context_t* context = (call_context_t*)rep->cookie;
	  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Got answer %d from rpc service %-12s :: %-32s  %08d:%03d  %ld objects",
			   rep->id, r->server->dns->name.c_str(), r->service.c_str(), secs, milli, names.size());
	  call_context_t::on_ready(context, std::move(names));
	  break;
	}
	case RPCCReadRegexCookie:   {
	  std::vector<json> objs;
	  auto objects = this->decode_objects(add_ptr(valin, sizeof(RPCReplyCookie)), valend);
	  for ( const auto& k : objects )    {
	    if ( r->rpc_type == COUNTER_CALL )    {
	      json o = JsonCounterDeserialize::de_serialize(k.second);
	      objs.emplace_back(std::move(o));
	    }
	    else if ( r->rpc_type == HIST_CALL )    {
	      json o = JsonHistDeserialize::de_serialize(k.second);
	      o["task"] = r->service;
	      objs.emplace_back(std::move(o));
	    }
	  }
	  call_context_t* context = (call_context_t*)rep->cookie;
	  json result = { {"task", r->service }, {"counters", objs} };
	  call_context_t::on_ready(context, std::move(result));
	  break;
	}
	  /// ERROR:
	case RPCCIllegal:
	default:
	  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ ILLEGAL rpc call %-12s :: %-32s  %08d:%03d",
			   r->server->dns->name.c_str(), r->service.c_str(), secs, milli);
	  std::lock_guard<std::mutex> context_lock(this->_context_lock);
	  if ( this->_current_context )    {
	    call_context_t* context = this->_current_context;
	    context->answer = std::string("");
	    context->ready  = 1;
	    context->condition.notify_one();
	  }
	  return;
	}
	//TimeSensor::instance().add(this, 15, r);
      }
    }
  }

  /// Check dns string for validity
  bool my_client::_check_dns(my_client* r, const std::string& dns)   {
    ++r->m_counters.numRequest;
    if ( dns.empty() )    {
      ++r->m_counters.numError;
      return false;
    }
    return true;
  }

  /// Check dns and task string for validity
  bool my_client::_check_dns_task(my_client* r, const std::string& dns, const std::string& task)   {
    ++r->m_counters.numRequest;
    if ( dns.empty() )    {
      ++r->m_counters.numError;
      return false;
    }
    if ( task.empty() )    {
      ++r->m_counters.numError;
      return false;
    }
    return true;
  }

  int my_client::_new_message_id()     {
    std::lock_guard<std::mutex> lock(this->message_id_lock);
    ++this->last_message_id;
    return this->last_message_id;
  }

  std::shared_ptr<my_client::call_context_t> my_client::create_context(rpc_server_relay::rpc_t* r, RPCCommType comm, size_t data_length)    {
    RPCCommCookie* c = (RPCCommCookie*)::operator new(data_length);
    auto ctx = std::make_unique<call_context_t>(r, c, data_length);
    c->comm = comm;
    switch(comm)    {
    case RPCCReadCookie:
    case RPCCReadAllCookie:
    case RPCCReadRegexCookie:
    case RPCCClearCookie:
    case RPCCClearAllCookie:
    case RPCCDirectoryCookie:
      c->cookie = ctx.get();
      c->id = this->_new_message_id();
      break;
    default:
      break;
    }
    return ctx;
  }

  std::shared_ptr<rpc_server_relay::rpc_t> my_client::get_rpc(const std::string& dns, const std::string& task, const std::regex& match)   {
    if ( _check_dns_task(this, dns, task) )   {
      auto server_rpc = this->rpc->get_server_rpc(dns, task, match);
      return server_rpc;
    }
    return {};
  }

  /// Access matching tasks by regex of a given dns
  my_client::json my_client::_taskservice_matches(const std::string& dns, const std::regex& match)  {
    json answer;
    if ( _check_dns(this, dns) )   {
      auto servers = this->rpc->get_servers(dns, match);
      std::vector<std::string> tasks;
      tasks.reserve(servers.size());
      for( const auto* s : servers )
	tasks.push_back(s->name);
      answer = tasks;
      ++m_counters.numSuccess;
    }
    return answer;
  }

  /// Emit the directory data of a specified task according to a given match
  my_client::json my_client::_task_directory(const std::string& dns, const std::string& task, const std::regex& match)  {
    auto server_rpc = this->get_rpc(dns, task, match);
    if ( server_rpc )   {
      auto ctx = this->create_context(server_rpc.get(), RPCCDirectoryCookie, sizeof(RPCCommCookie));
      if ( this->do_call(server_rpc, ctx) )   {
	++m_counters.numSuccess;
	return ctx->answer;
      }
    }
    ++m_counters.numError;
    return {{"error", EINVAL},
	{"message", "Invalid arguments to call <br>task_directory('"+dns+"','"+task+"')"}};
  }

  /// Access matching counter tasks of a given dns
  my_client::json my_client::counter_tasks(const std::string& dns)  {
    return this->_taskservice_matches(dns, this->counter_match);
  }
  /// Access matching histogram tasks of a given dns
  my_client::json my_client::histogram_tasks(const std::string& dns)  {
    return this->_taskservice_matches(dns, this->histo_match);
  }
  /// Emit the counter directory of a specified task
  my_client::json my_client::task_counter_directory(const std::string& dns, const std::string& task)  {
    return this->_task_directory(dns, task, this->counter_match);
  }
  /// Emit the histogram directory of a specified task
  my_client::json my_client::task_histogram_directory(const std::string& dns, const std::string& task)  {
    return this->_task_directory(dns, task, this->histo_match);
  }

  /// Access histogram list by regular expression
  my_client::json my_client::task_counter(const std::string& dns, const std::string& task, const std::string& selection)   {
    auto result = this->task_counters_regex(dns, task, selection);
    if ( result.is_array() && result.size() > 0 )   {
      return result.at(0);
    }
    return result;
  }

  /// Access all taskcounter
  my_client::json my_client::task_counters(const std::string& dns, const std::string& task)   {
    return this->task_counters_regex(dns, task, ".(.*)");
  }

  /// Access counter list by regular expression
  my_client::json my_client::task_counters_regex(const std::string& dns, const std::string& task, const std::string& selection)   {
    auto server_rpc = this->get_rpc(dns, task, this->counter_match);
    if ( server_rpc )   {
      int cmdlen = selection.length()+1+sizeof(RPCCommRegexCookie);
      auto ctx = this->create_context(server_rpc.get(), RPCCReadRegexCookie, cmdlen);
      auto* cmd = ctx->data<RPCCommRegexCookie>();
      cmd->copy_name(cmd->which, selection);
      if ( this->do_call(server_rpc, ctx) )   {
	if ( ctx->answer.size() > 0 )   {
	  ++m_counters.numSuccess;
	  return ctx->answer;
	}
      }
      ++m_counters.numError;
      return {{"error", EINVAL},
	  {"message", "No counters found: <br>task_counters('"+dns+"','"+task+"')"}};
    }
    ++m_counters.numError;
    return {{"error", EINVAL},
	{"message", "Invalid arguments to call <br>task_counters('"+dns+"','"+task+"')"}};
  }

}

//=========================================================================
static void help_server(const char* msg = 0, const char* argv = 0) {
  std::cout << "run_gaucho_rpc -opt <value> [ -opt <value> ]                                " << std::endl
	    << "     -port    <number>    Listening port (default: 8000)                    " << std::endl
	    << "     -threads <number>    Number of *additional* worker threads (default: 0)" << std::endl
	    << "     -debug               Enable debug flag (default: off)                  " << std::endl
	    << "     -cache <number>      Enable cache with timeout in seconds.             " << std::endl
	    << "     -help                Print this help.                                  " << std::endl;
  if ( msg )
    std::cout << "Error cause:    " << msg << std::endl;
  if ( argv )
    std::cout << "Argument given: " << argv << std::endl;
  std::cout << std::endl;
  ::exit(EINVAL);
}
//=========================================================================
/// Main entry point to start the application
extern "C" int run_gaucho_dns_listener(int argc, char** argv) {
  int port, threads=0, debug;
  std::string host = "0.0.0.0";
  for(int i = 1; i < argc && argv[i]; ++i)  {
    if ( 0 == std::strncmp("-port",argv[i],4) )
      port = ::atol(argv[++i]);
    else if ( 0 == std::strncmp("-host",argv[i],4) )
      host = (++i < argc) ? argv[++i] : "localhost";
    else if ( 0 == std::strncmp("-threads",argv[i],3) )
      threads = (++i < argc) ? ::atol(argv[i]) : 2;
    else if ( 0 == std::strncmp("-debug",argv[i],4) )
      debug = (++i < argc) ? ::atol(argv[i]) : 3;
    else if ( 0 == std::strncmp("-help",argv[i],2) )
      help_server();
    else
      help_server("Invalid argument given", argv[i]);
  }
  if ( debug || threads || port )  {}
  auto flags = std::regex_constants::ECMAScript | std::regex_constants::icase;

  my_tests::my_client client;
  client.counter_match = std::regex("(.*)/Counter/HistCommand", flags);
  client.histo_match   = std::regex("(.*)/Histos/HistCommand", flags);
  client.rpc = std::make_unique<rpc_server_relay>();
  client.rpc->service_matches.insert(std::make_pair(client.HIST_CALL,    client.histo_match));
  client.rpc->service_matches.emplace(std::make_pair(client.COUNTER_CALL, client.counter_match));

  client.rpc->client = &client;
  client.rpc->add_dns("mon01");
  //client.rpc->add_dns("hlt01");
  //client.rpc->add_dns("dataflow01");
  CPP::IocSensor::instance().run();
  return 0;
}
