//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef YAML_INTERFACE_H
#define YAML_INTERFACE_H

/// Framework include files

#include <yaml-cpp/yaml.h>

#endif // YAML_INTERFACE_H
