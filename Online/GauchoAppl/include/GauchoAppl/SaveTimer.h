//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_SAVETIMER_H
#define ONLINE_GAUCHO_SAVETIMER_H

#include "Gaucho/GenTimer.h"
#include "Gaucho/HistSaver.h"
#include "GauchoAppl/MonAdder.h"

/// Online namespace declaration
namespace Online   {

  class SaveTimer : public GenTimer, public HistSaver   {
  public:
    std::shared_ptr<MonAdder>     adder;
    long                          numSaveCycles  { 0 };
  public:
    SaveTimer(std::shared_ptr<MonAdder>& adder, int period = 900);
    ~SaveTimer() = default;
    virtual void timerHandler() override;
  };
}
#endif   // ONLINE_GAUCHO_SAVETIMER_H
