//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_RateService_h
#define ONLINE_GAUCHO_RateService_h
#include "dim/dis.hxx"
#include <string>

class RateService : public DimService   {
public:
#define MAX_C 512
  struct DATA  {
    double d1,d2;
    char c[MAX_C+1];
  };
  DATA m_data;
  RateService (const std::string& name, const std::string& format);
  virtual ~RateService() = default;
  void update(int dsize);
};

#endif  // ONLINE_GAUCHO_RateService_h
