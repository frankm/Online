//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef HISTADDER_H
#define HISTADDER_H
#include "GauchoAppl/MonAdder.h"

/// Online namespace declaration
namespace Online  {

  class HistAdder : public MonAdder  {
  public:
    using MonAdder::MonAdder;
    virtual ~HistAdder();
    virtual void add_item(DimBuffBase* summed, const DimBuffBase* source)  override final;
  };
}
#endif
