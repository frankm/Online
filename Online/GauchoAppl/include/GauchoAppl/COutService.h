//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHOAPPL_COUTSERVICE_H_
#define ONLINE_GAUCHOAPPL_COUTSERVICE_H_

// C/C++ include files
#include <map>
#include <memory>
#include <string>

/// Forward declarations
class DimService;

class COutServiceBase   {
public:
  std::string m_nam;
public:
  COutServiceBase(const std::string& nam) : m_nam(nam)  {  }
  virtual ~COutServiceBase() = default;
};

template <typename T> class COutService : public COutServiceBase   {
public:
  T m_data;
  std::unique_ptr<DimService> m_serv;

public:
  COutService(const std::string& name);
  virtual ~COutService();
  void Update(const T &dat);
};

#endif /* ONLINE_GAUCHOAPPL_COUTSERVICE_H_ */
