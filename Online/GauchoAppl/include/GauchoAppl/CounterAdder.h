//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_COUNTERADDER_H
#define ONLINE_GAUCHO_COUNTERADDER_H

#include "GauchoAppl/MonAdder.h"

/// Online namespace declaration
namespace Online  {

  class CounterAdder : public MonAdder  {
  public:
    using MonAdder::MonAdder;
    virtual ~CounterAdder();
    virtual void add_item(DimBuffBase* summed, const DimBuffBase* source)  override final;
  };
}
#endif //  ONLINE_GAUCHO_COUNTERADDER_H
