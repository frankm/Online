//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "GauchoAppl/HistAdder.h"
#include "Gaucho/MonHist.h"
#include "TH1D.h"
#include "TProfile.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TPad.h"
#include "TCanvas.h"
#include "Gaucho/HistSerDes.h"
TCanvas *canv;
static int nCB=0;

using namespace Online;

void CycleFn(void*, mem_buff&, MonitorItems *hm, MonAdder *)
{
  printf("=====================================Cycle CallBack Function\n");
  if ((nCB % 10) != 0)  {
    nCB++;
    return;
  }
  nCB++;
  int j = 0;
  for ( auto i=hm->begin(); i != hm->end(); ++i )   {
    DimBuffBase *p = i->second;
    char *nam = add_ptr<char>(p,p->nameoff);
    const char *stype=0;
    ++j;
    switch (p->type)      {
    case H_1DIM:
      stype = "H_1DIM";
      break;
    case H_PROFILE:
      stype = "H_PROFILE";
      break;
    case H_RATE:
      stype = "H_RATE";
      break;
    case H_2DIM:
      stype = "H_2DIM";
      break;
    case H_3DIM:
      stype = "H_3DIM";
      break;
    case C_STATENT:
      stype = "C_STATENT";
      break;
    }
    TObject *ro = (TObject*)HistSerDes::de_serialize(i->second);
    bool draw=((j==5) || (j==7) || (j==13));
    switch (p->type)     {
    case H_1DIM:
    case H_PROFILE:
    case H_RATE:        {
      DimHistbuff1 *p=(DimHistbuff1*)i->second;
      printf("\t\t%s %s. Dimension %d #bins %d Nentries %lli\n",stype,nam, p->dim,p->x.nbin,(long long)p->nentries);
      if ((MONTYPE)p->type == H_1DIM)     {
	TH1D *r=(TH1D *)ro;
	if (draw) r->Draw();
      }
      else     {
	if (draw) ro->Draw();
      }
      break;
    }
    case H_2DIM:      {
      DimHistbuff2 *p=(DimHistbuff2*)i->second;
      printf("\t\t%s %s. Dimension %d #x-bins %d #y-bins %d Nentries %lli\n",stype,nam,p->dim,p->x.nbin,p->y.nbin,(long long)p->nentries);
      if (draw) ro->Draw();
      break;
    }
    case H_3DIM:     {
      DimHistbuff3 *p=(DimHistbuff3*)i->second;
      printf("\t\t%s %s. Dimension %d #x-bins %d #y-bins %d Nentries %lli\n",stype,nam,p->dim,p->x.nbin,p->y.nbin,(long long)p->nentries);
      if (draw) ro->Draw();
      break;
    }
    case C_STATENT:     {
      DimStatBuff *p=(DimStatBuff*)i->second;
      printf("\t\t%s %s. Nentries %lli\n",stype,nam, (long long)p->nentries);
      break;
    }
    }
  }
  canv->Update();
}

#include <TROOT.h>
#include <TApplication.h>

int main (int argc, char *argv[])   {
  TH1D::SetDefaultSumw2();
  TH2D::SetDefaultSumw2();
  TApplication *app = new TApplication("Shit",&argc,argv);
  canv = gROOT->MakeDefCanvas();
  gROOT->Draw();
  canv->Show();
  canv->Draw();
  HistAdder *a = new HistAdder("Adder","Histos/Data", ADD_HISTO);
  a->SetCycleFn(&CycleFn,0);
  app->Run();
  sleep(1000000000);
  return 0;
}
