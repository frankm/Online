//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "GauchoAppl/RateService.h"

RateService::RateService (const std::string& name, const std::string& format) 
  : DimService(name.c_str(), (char*)format.c_str(), &m_data, sizeof(m_data))
{
}

void RateService::update(int siz)  {
  updateService(&m_data, siz);
}
