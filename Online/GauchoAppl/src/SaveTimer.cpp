//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <GauchoAppl/SaveTimer.h>
#include <GauchoAppl/ClassDescriptor.h>
#include <Gaucho/SerialHeader.h>
#include <Gaucho/ObjService.h>
#include <Gaucho/HistSerDes.h>
#include <Gaucho/RootHists.h>
#include <Gaucho/Utilities.h>
#include <RTL/strdef.h>

#include <TFile.h>
#include <TROOT.h>
#include <TThread.h>

#include <cstdio>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace Online;

SaveTimer::SaveTimer(std::shared_ptr<MonAdder>& add, int period) 
  : GenTimer(period*1000,TIMER_TYPE_PERIODIC), HistSaver(add), adder(add)
{
  this->m_dueTime     = 0;
  this->m_dontdimlock = true;
  this->EOR           = false;
  this->lockable.reset(); // We take the lock in timerHandler() ourselves!
}

void SaveTimer::timerHandler(void)   {
  static std::mutex _l;
  std::lock_guard<std::mutex> lock(_l);
  ++this->numSaveCycles;
  if ( this->adder )   {
    // Need protection against the adder updating to buffers.
    ObjectLock<MonAdder> adder_lock(this->adder.get());
    auto [m, run] = this->adder->get_class_map("", false);
    if ( m )   {
      std::unique_ptr<TFile> output;
      for (auto& i : *m )   {
	auto& cl = i.second;
	if ( cl->buffer->used() > 0 )    {
	  this->buffer.copy(cl->buffer->begin(), cl->buffer->used());
	  SerialHeader *hd  = this->buffer.begin<SerialHeader>();
	  if ( !output && this->buffer.used() >= sizeof(SerialHeader) )     {
	    this->makeDirs(hd->run_number);
	    output = openFile();
	  }
	  this->savetoFile(this->buffer.begin());
	}
	else if ( cl->outputservice )  {
	  ::printf("Not writing histograms for %s [No data present]", cl->outputservice->getName());
	}
	else   {
	  ::printf("Not writing histograms  [No data present]");
	}
      }
      this->closeFile(std::move(output));
    }
  }
  this->updateSaveSet();
}
