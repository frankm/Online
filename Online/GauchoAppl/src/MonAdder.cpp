//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
// workaround for https://its.cern.ch/jira/browse/GAUDI-1168

#include <GauchoAppl/MonAdder.h>

#include <Gaucho/ObjRPC.h>
#include <Gaucho/ObjService.h>
#include <Gaucho/SerialHeader.h>

#include "Gaucho/GenTimer.h"
#include <GauchoAppl/AdderSys.h>
#include <GauchoAppl/SaveTimer.h>
#include <GauchoAppl/RunManager.h>
#include <GauchoAppl/AddSerializer.h>
#include <GauchoAppl/ClassDescriptor.h>

#include <RTL/strdef.h>

#include <sstream>
#include <exception>

using namespace std;
using namespace Online;

#define offsetinBounds(base,recl,offs) !((add_ptr(base,offs)>add_ptr(base,recl))||(add_ptr(base,offs)<base))


class MonAdder::AddTimer : public GenTimer    {
  MonAdder *owner;

public:
  AddTimer(MonAdder *tis, int period = 5, int typ = 0);
  virtual ~AddTimer(void) = default;
  void timerHandler ( void ) override    {
    int arg = 1;
    if ( !this->isRunning() )  {
      DimClient::sendCommandNB(this->owner->cmdName.c_str(),&arg,sizeof(arg));
    }
  }
};

MonAdder::AddTimer::AddTimer(MonAdder *tis, int period, int typ) : GenTimer(period*1000,typ)   {
  this->owner = tis;
  this->m_dueTime = 0;
  this->m_dontdimlock = true;
}

MonAdder::MonAdder(const string& nam, const string& service_nam, int typ)   {
  this->serviceName = service_nam;
  this->name        = nam;
  this->type        = typ;
  this->runManager  = make_unique<RunManager>();
  AdderSys::instance().add(this);
}

MonAdder::~MonAdder()  {
  if ( this->sourceSvc_id )  {
    ::dis_remove_service(this->sourceSvc_id);
    this->sourceSvc_id = 0;
  }
  this->rpc.reset();
  AdderSys::instance().remove(this);
}

string MonAdder::getClassName(const string &service)   {
  size_t dot = service.rfind(".");
  return (dot != string::npos) ? service.substr(dot+1) : string();
}

void MonAdder::SetCycleFn(void CycleCB(void*, mem_buff&, MonitorItems*, MonAdder *), void *tis)     {
  this->CycleFn  = CycleCB;
  this->CycleArg = tis;
}

void MonAdder::SetPauseFn(void Pause(void*), void *tis)  {
  this->PauseFn  = Pause;
  this->PauseArg = tis;
}

void MonAdder::feedSources(void* tag, void** address, int* size, int* /* first */)   {
  if ( tag && address && size )   {
    stringstream str;
    MonAdder* adder = *(MonAdder**)tag;
    str << "ServiceSources:{";
    for(const auto& i : adder->taskInventory)   {
      str << "'" << i.first << "': {";
      for (auto s = i.second.begin(); s != i.second.end(); )
	str << "'" << *s << "'" << (++s == i.second.end() ? "" : ",");
      str << "}";
    }
    adder->source_buffer = str.str();
    *size    = adder->source_buffer.length()+1;
    *address = (void*)adder->source_buffer.c_str();
    return;
#if 0
    auto [m, run] = adder->get_class_map("",false);
    if ( m )   {
      str << "ServiceSources:" << m->size() << '#';
      for( const auto& cl : *m )  {
	const auto& m = cl.second->inputServices;
	str << '{' << (cl.first.empty() ? "**default**" : cl.first.c_str()) << ":{";
	for (auto s = m.begin(); s != m.end(); )
	  str << s->first << (++s == m.end() ? "" : "|");
	str << '}';
      }
      adder->source_buffer = str.str();
      *size    = adder->source_buffer.length()+1;
      *address = (void*)adder->source_buffer.c_str();
      return;
    }
#endif
  }
  *size = 0;
  *address = (char*)"";
}

void MonAdder::configure()   {
  string dim_svc = RTL::processName() + "/" + this->name + "/Debug/Sources";
  string nam_svc = "MON_" + this->name;
  this->runManager->clear();

  if ( this->type == ADD_COUNTER )   {
    dim_svc += "/Cntr/"+this->serviceName;
    this->serviceName = (this->runAware ? "/Counter/<runno>/" : "/Counter/") + this->serviceName;
  }
  else if ( this->type == ADD_HISTO )   {
    dim_svc += "/Hist/"+this->serviceName;
    this->serviceName = (this->runAware ? "/Histos/<runno>/" : "/Histos/") + this->serviceName;
  }
  if ( this->trackSources && 0 == this->sourceSvc_id )  {
    this->sourceSvc_id = ::dis_add_service(dim_svc.c_str(),"C",0,0,feedSources,(long)this);
  }
  this->invlock.name  = nam_svc + "InventoryLock";
  this->cmdName       = RTL::processName() + "/" + this->name + this->serviceName+"/Timeout";
  this->timer         = make_unique<AddTimer>(this);
  this->service_regex = regex(this->servicePattern.c_str(),regex_constants::icase);
  this->task_regex    = regex(this->taskPattern.c_str(),regex_constants::icase);

  if ( this->isSaver )   {
    this->lockid = make_unique<BRTLLock>();
    this->lockid->name = nam_svc + "AdderLock";
  }

  this->rpc.reset();
  if ( !this->noRPC )  {
    string rpcName;
    if ( this->type == ADD_HISTO )
      rpcName = nam_svc + "/Histos/HistCommand";
    else if ( this->type == ADD_COUNTER )
      rpcName = nam_svc + "/Counter/HistCommand";

    this->maplock.name = rpcName;
    ::lib_rtl_output(AdderSys::instance().debugOn ? LIB_RTL_ALWAYS : LIB_RTL_VERBOSE,
		     "Creating RPC: %s", rpcName.c_str());
    this->rpc = make_unique<ObjRPC>(this->serviceDNS,
				    make_unique<AddSerializer>(this->monitorItems),
				    rpcName,
				    "I:1;C","C", &this->maplock, nullptr);
  }
}

void MonAdder::load_item_map(mem_buff& buffer)    {
  void*    bend   = add_ptr(buffer.begin(), buffer.used());
  void*    hstart = buffer.at<SerialHeader>(0)->endPtr();
  auto*    pp = (DimBuffBase*)hstart;
  const char *nam = "";
  while ( pp < bend )    {
    if ( pp->reclen <= 0 )    {
      ::lib_rtl_output(LIB_RTL_INFO,
		       "Bad Record length (<=0). Last valid Element %s Pointers: (start, end, curr) %p %p %p",
		       nam, hstart, bend, pp);
      break;
    }
    if ( offsetinBounds(pp,pp->reclen,pp->nameoff) )    {
      nam = add_ptr<char>(pp,pp->nameoff); {
	BRTLLock::_Lock lock(&this->maplock);
	this->monitorItems.emplace(nam, pp);
      }
      if ( pp->type == H_RATE )
	this->rateBuff = pp;
    }
    else    {
      ::lib_rtl_output(LIB_RTL_INFO,
		       "+++ MonAdder: Bad value of name offset (%d). Corrupted record?",pp->nameoff);
      break;
    }
    pp = add_ptr<DimBuffBase>(pp,pp->reclen);
  }
}

void MonAdder::load_item_map(MonitorItems& m, mem_buff& buffer)    {
  void*    bend   = add_ptr(buffer.begin(), buffer.used());
  void*    hstart = buffer.at<SerialHeader>(0)->endPtr();
  auto*    pp     = ptr_as<DimBuffBase>(hstart);
  const char *nam = "";
  m.clear();
  while ( pp < bend )    {
    if ( pp->reclen <= 0 )    {
      ::lib_rtl_output(LIB_RTL_INFO,
		       "Bad Record length (<=0). Last valid Element %s Pointers: (start, end, curr) %p %p %p",
		       nam,hstart,bend,pp);
      break;
    }
    if ( offsetinBounds(pp,pp->reclen,pp->nameoff) )    {
      nam = add_ptr<char>(pp,pp->nameoff);
      m.emplace(nam, pp);
    }
    else 
      ::lib_rtl_output(LIB_RTL_INFO,
		       "Problem in reallocating buffer: Name offset in record bad. %d (%0x)",
		       pp->nameoff,pp->nameoff);
    pp = add_ptr<DimBuffBase>(pp,pp->reclen);
  }
}

pair<std::shared_ptr<ClassMap>, unsigned int>
MonAdder::get_class_map(const string& service, bool create)     {
  if ( this->runAware && !service.empty() )   {
    auto         items = RTL::str_split(service,"/");
    string       run   = items.at(items.size()-2);
    unsigned int runno = ::stoi(run);
    auto i = this->runManager->find(runno);
    if (i != this->runManager->end())  {
      return make_pair(i->second, runno);
    }
    if ( create )   {
      auto class_map = make_shared<ClassMap>();
      this->runManager->emplace(runno, class_map);
      return make_pair(class_map, runno);
    }
    return make_pair(std::shared_ptr<ClassMap>(), 0);
  }
  auto i = this->runManager->find(0);
  if (i != this->runManager->end())  {
    return make_pair(i->second, 0);
  }
  if ( create )   {
    auto class_map = make_shared<ClassMap>();
    this->runManager->emplace(0, class_map);
    return make_pair(class_map, 0);
  }
  return make_pair(std::shared_ptr<ClassMap>(), 0);
}

void MonAdder::publishSources()   {
  if ( 0 != this->sourceSvc_id )  {
    ::dis_update_service(this->sourceSvc_id);
  }
}

string MonAdder::taskName(const string &server)   {
  bool status = false;
  cmatch what;
  try  {
    status = regex_search(server.c_str(),what,this->task_regex);
  }
  catch (std::exception &e)  {
    ::lib_rtl_output(LIB_RTL_ERROR,"Caught standard exception in TaskName: what = %s",e.what());
  }
  catch(...)  {
    ::lib_rtl_output(LIB_RTL_ERROR,"Caught unspecific exception in TaskName.");
  }
  return (status) ? server : string();
}

bool MonAdder::addTask(const string &task)  {
  BRTLLock::_Lock lock(&this->invlock);

  auto i = this->taskInventory.find(task);
  if ( i == this->taskInventory.end() )  {
    int level = AdderSys::instance().debugOn ? LIB_RTL_ALWAYS : LIB_RTL_VERBOSE;
    this->taskInventory[task] = set<string>();
    ::lib_rtl_output(level,"Adder[%s] add task: %s", name.c_str(), task.c_str());
    ++this->numTasksConnected;
    return true;
  }
  return false;
}

bool MonAdder::removeTask(const string& task)   {
  int level = AdderSys::instance().debugOn ? LIB_RTL_ALWAYS : LIB_RTL_VERBOSE;
  BRTLLock::_Lock lock(&this->invlock);

  auto tsk = this->taskInventory.find(task);
  if ( tsk == this->taskInventory.end() )   {
    ::lib_rtl_output(level,"%s%s: TaskDied: Task = %s, not found in inventory.",
		     this->name.c_str(), this->serviceName.c_str(), task.c_str());
    return false;
  }
  this->numServicesConnected -= tsk->second.size();
  --this->numTasksConnected;
  ::lib_rtl_output(level,"%s%s: TaskDied: Task = %s, found. Scanning Services...",
		   this->name.c_str(), this->serviceName.c_str(), task.c_str());
  auto class_name = getClassName(task);
  for (const auto& serv : tsk->second )  {
    for ( const auto& m : *this->runManager )   {
      auto& clmap = m.second;
      if ( clmap )    {
	auto clit = clmap->find(class_name);
	if ( clit == clmap->end() )     {
	  if (this->debugOn) ::lib_rtl_output(level,"TaskDied: Class not found");
	  continue;
	}
	auto& cdesc = clit->second;
	::lib_rtl_output(level,"%s%s: TaskDied: Class  found Class Name %s.",
			 this->name.c_str(), this->serviceName.c_str(), cdesc->name.c_str());
	auto j = cdesc->inputServices.find(serv);
	if ( j != cdesc->inputServices.end() )    {
	  cdesc->inputServices.erase(j);
	  --this->numServicesConnected;
	  if ( cdesc->inputServices.size() == 0 )   {
	    cdesc->outputservice.reset();
	    cdesc->tmo_command.reset();
	    --this->numServicesOutput;
	    --this->numServicesCommand;
	  }
	}
      }
    }
  }
  --this->numTasksConnected;
  this->taskInventory.erase(tsk);
  this->publishSources();
  return true;
}

int MonAdder::addService(const string& task, const string& service)   {
  int level  = AdderSys::instance().debugOn ? LIB_RTL_ALWAYS : LIB_RTL_VERBOSE;
  cmatch what;
  int status = 0;
  try  {
    status = regex_search(service.c_str(), what, this->service_regex);
  }
  catch (std::exception &e)  {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s%s: Caught standard exception in addService: what = %s",
		     this->name.c_str(), this->serviceName.c_str(), e.what());
  }
  catch(...)  {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s%s: Caught unspecific exception in addService",
		     this->name.c_str(), this->serviceName.c_str());
  }
  if ( !status )  {
    return SERVICE_NO_MATCH;
  }
  ::lib_rtl_output(level,"MATCHED: %s: %s with Pattern %s...",
		   task.c_str(), service.c_str(),this->servicePattern.c_str());

  BRTLLock::_Lock lock(&this->invlock);
  auto i = this->taskInventory.find(task);
  if ( i == this->taskInventory.end() )    {
    ::lib_rtl_output(level,"%s%s: New Service '%s' called, add task '%s' to inventory \n",
		     this->name.c_str(), this->serviceName.c_str(), service.c_str(),task.c_str());
    i = this->taskInventory.emplace(task, set<string>()).first;
    ++this->numTasksConnected;
  }
  if ( i->second.find(service) != i->second.end() )
    return SERVICE_PRESENT;

  ++this->numServicesConnected;
  i->second.insert(service);

  auto [class_map, run] = this->get_class_map(service, true);
  string classnam = getClassName(service);
  ::lib_rtl_output(level,"%s%s: Adding Service: %s Class Name: %s",
		   this->name.c_str(), this->serviceName.c_str(), service.c_str(), classnam.c_str());

  auto cindx = class_map->find(classnam);
  if (cindx == class_map->end())   {
    cindx = class_map->emplace(classnam, make_shared<ClassDescriptor>(classnam, this)).first;
  }
  int   ret_code = SERVICE_PRESENT;
  auto& cdesc = cindx->second;
  if ( cdesc->addService(service, task) )   {
    ret_code = SERVICE_ADDED;
  }
  if ( !this->disableOutput && !cdesc->outputservice )   {
    cdesc->enableOutput(this->serviceDNS, "MON_" + this->name + this->serviceName, run);
    ++this->numServicesOutput;
  }
  if ( !cdesc->tmo_command )    {
    string tmo_cmd = classnam.empty() ? this->cmdName : this->cmdName + "." + classnam;
    cdesc->tmo_command = make_unique<TimeoutCmd>(tmo_cmd, service, this);
    ++this->numServicesCommand;
  }
  this->serviceDNS ? DimServer::start(this->serviceDNS.get()) : DimServer::start();
  this->publishSources();
  return ret_code;
}

int MonAdder::removeService(const string &task, const string &service)    {
  int ret_code = SERVICE_NO_MATCH;
  BRTLLock::_Lock lock(&this->invlock);

  auto tsk = this->taskInventory.find(task);
  if ( tsk != this->taskInventory.end() )    {
    auto svc = tsk->second.find(service);
    if ( svc != tsk->second.end() )  {
      --this->numServicesConnected;
      tsk->second.erase(svc);
      ret_code = SERVICE_REMOVED;
    }
    auto [class_map, run] = this->get_class_map(service, false);
    if ( class_map )   {
      auto clit = class_map->find(getClassName(service));
      if ( clit != class_map->end() )   {
	auto& cl = clit->second;
	auto   i = cl->inputServices.find(service);
	if ( i != cl->inputServices.end() )  {
	  cl->inputServices.erase(i);
	  if ( cl->inputServices.empty() )    {
	    cl->outputservice.reset();
	    cl->tmo_command.reset();
	    --this->numServicesOutput;
	    --this->numServicesCommand;
	  }
	}
      }
    }
  }
  this->publishSources();
  return ret_code;
}

unsigned long long MonAdder::gettime()   {
#define onesec_nano 1000000000ULL
  struct timeval tv;
  ::gettimeofday(&tv, nullptr);
  unsigned long long timstamp  = tv.tv_sec * onesec_nano + tv.tv_usec*1000ULL;
  return timstamp;
}

void MonAdder::start()   {
  if ( this->timer )  {
   ::lib_rtl_output(LIB_RTL_INFO,"%s%s: MonAdder start called.",
		    this->name.c_str(), this->serviceName.c_str());
    this->timer->startPeriodic(this->rectmo);
  }
}

void MonAdder::stop()   {
  if ( this->timer )  {
    this->timer->stop();
  }
}

void MonAdder::idle_update()  {
  if ( this->runManager )    {
    unsigned long ref = this->gettime();
    for ( const auto& m : *this->runManager )   {
      auto& clmap = m.second;
      if ( clmap )    {
	for ( auto& c : *clmap )   {
	  std::shared_ptr<ClassDescriptor>& cl = c.second;
	  if ( cl )   {
	    if ( 0 == cl->reference && 0 == cl->updateStamp ) {
	      cl->reference = ref;
	    }
	    //this->handleTimeout(cldesc);
	    //this->add_what_we_have();
	    this->finish_cycle(cl, true);
	  }
	}
      }
    }
  }
}

void MonAdder::handleTimeout(std::shared_ptr<ClassDescriptor>& cl)  {
  ::lib_rtl_output(LIB_RTL_VERBOSE,"MonAdder Timeout: service %s for expected time %lli. Sources expected %d received %d",
      this->name.c_str(),cl->reference,cl->expected,cl->received );
  if ( !this->test )  {
    for ( auto& i : this->inputServicemap )    {
      auto& d = i.second;
      if ( d->last_update < cl->reference )      {
        d->m_timeouts++;
        if ( d->buffer.begin() )    {
          if (this->debugOn) ::lib_rtl_output(LIB_RTL_VERBOSE,
              "\t===Adding Buffered data from source %s expected %lli last received %lli\n",
              d->info->getName(), cl->reference, d->last_update);
          this->add_items(cl.get(), d->buffer, d->info.get());
        }
        else    {
          if (this->debugOn) ::lib_rtl_output(LIB_RTL_VERBOSE,
              "\t===No buffered data found from source %s expected %lli last received %lli\n",
              d->info->getName(), cl->reference, d->last_update);
          cl->received++;
        }
      }
    }
    if (this->debugOn) ::lib_rtl_output(LIB_RTL_VERBOSE,"\t==MonAdder Timeout after adding old buffers. Sources expected %d received %d",
        this->inputServicemap.size(),cl->received );
  }
  cl->timeout = true;
  this->finish_cycle(cl, true);
}

void MonAdder::add_what_we_have()   {
  auto [m, run] = this->get_class_map("", false);
  auto& cl = m->begin()->second; // SEGV -- don't call
  cl->buffer->reset();
  cl->monitorItems.clear();
  for ( auto& it : cl->inputServices )  {
    if ( it.second->buffer.used() == 0 )   {
      continue;
    }
    else if ( cl->monitorItems.empty() )    {
      cl->buffer->copy(it.second->buffer.begin(), it.second->m_fragsize);
      this->load_item_map(cl->monitorItems, *cl->buffer);
      this->load_item_map(*cl->buffer);
    }
    else if ( it.second->buffer.begin() )      {
      add_items(cl.get(), it.second->buffer, it.second->info.get());
    }
  }
}

void MonAdder::process_buffer(void *buff, int data_size, MonInfo *info)   {
  SerialHeader* header    = ptr_as<SerialHeader>(buff);
  const string  service   = info->getName();
  const string  classname = getClassName(service);
  auto [class_map, run]   = this->get_class_map(service, true);

  auto itcl = class_map->find(classname);
  if ( itcl == class_map->end() )  {
    ::lib_rtl_output(LIB_RTL_ERROR,"======> process_buffer Called with service %s not in class map",service.c_str());
    return;
  }

  auto& cl = itcl->second;
  if ( data_size == 4 )  {
    ::lib_rtl_output(LIB_RTL_ERROR,"======> No Link from %s. Update counts...",service.c_str());
    cl->received++;
    this->finish_cycle(cl);
    return;
  }
  if ( header->m_magic != SERIAL_MAGIC )  {
    ::lib_rtl_output(LIB_RTL_ERROR,"======> Serial Magic Word Missing  from connection %s\n",service.c_str());
    cl->received++;
    this->finish_cycle(cl);
    return;
  }
  auto it= cl->inputServices.find(service.c_str());
  if ( it == cl->inputServices.end() )  {
    ::lib_rtl_output(LIB_RTL_ERROR,"!!!!!Logic Error Adder add method called with service that's not in the input service map %s\n",service.c_str());
    return;
  }

  int level = this->debugOn ? LIB_RTL_ALWAYS : LIB_RTL_VERBOSE;

  unique_ptr<INServiceDescr>& descr = it->second;
  descr->buffer.copy(buff, data_size);
  descr->m_pid = DimClient::getServerPid();
  descr->m_timeouts = 0;
  if ( descr->info.get() != info ) descr->info.reset(info); // can this ever happen ??
  header = descr->buffer.as<SerialHeader>();
  cl->expected = cl->inputServices.size();

  ++this->numBuffersReceived;
  ++this->numBuffersCurrent;
#if 0
  if ( this->numBuffersCurrent > 100 && cl->expected == cl->received )    {
    this->debugOn = true;
  }
#endif
  long long current  = (this->isEOR) ? header->run_number : header->ser_tim;
  descr->last_update = current;
  this->rateBuff = 0;

  ::lib_rtl_output(level,
		   "Packet from %-48s Expected Number %2d received %2d timestamp %ld current %ld\n",
		   service.c_str(),cl->expected,cl->received+1,cl->reference/10000000,current/10000000);

  if ( (cl->reference < current) || ((cl->reference <= 0) && (cl->received == 0)) )  {
    /// Here either a new update cycle start [ cl->reference < current ]
    /// or there was a "stop" without any prior data [ (cl->reference <= 0) && (cl->received == 0) ]
    if ( (cl->received > 0) && (cl->received < cl->expected) && !cl->timeout )     {
      this->finish_cycle(cl, true);
    }
    ::lib_rtl_output(level,"First Fragment for service %s timestamp %ld",service.c_str(), current);
    cl->timeout = false;
    cl->firstSource = descr->service;
    float tmo = this->rectmo;
    if ( header->version > 1 && this->rectmo > 0 )    {
      header->level++;
      unsigned int uintv = header->updateInterval;
      if (header->level == 1)      {
        tmo = 3.0;
      }
      else if (header->level == 2)     {
        tmo=0.6*uintv+5.0;
      }
      else if (header->level == 3)     {
        tmo=0.6*uintv+8.0;
      }
      else      {
        tmo=0.6*uintv+12.0;
      }
      if ( this->isEOR ) tmo = 500*tmo;
    }
    /// Set timer if enabled by options
    if ( tmo > 0 )   {
      ::lib_rtl_output(level,"Service %s Header Version %d. Update Interval %d: Setting timeout to %d seconds\n",
		       this->servicePattern.c_str(),header->version,header->updateInterval,int(tmo));
      this->timer->startPeriodic(tmo);
    }
    else    {
      ::lib_rtl_output(level,"Service %s Header Version %d. Update Interval %d: No Timout set.\n",
		       this->servicePattern.c_str(),header->version,header->updateInterval);
    }
    cl->added++;
    cl->received = 1;
    cl->time0 = this->gettime();
    if ( this->isSaver && !this->isLocked )   {
      this->lock();
      this->isLocked = true;
    }
    cl->reference = current;
    cl->adoptItems(buff, data_size);
  }
  else if ( cl->reference == current )  {
    cl->received++;
    this->add_items(cl.get(), descr->buffer, info);
  }
  else  {
    ::lib_rtl_output(level,"Late update from %s: m_expected %lli received %lli. Using stored buffer.",
		     service.c_str(), cl->reference, current);
    cl->received++;
    this->add_items(cl.get(), descr->buffer, info);
  }
  this->finish_cycle(cl);
}

void MonAdder::finish_cycle(std::shared_ptr<ClassDescriptor>& clazz, bool force)  {
  int level  = AdderSys::instance().debugOn ? LIB_RTL_ALWAYS : LIB_RTL_VERBOSE;
  auto* cl = clazz.get();
  ::lib_rtl_output(level,"MonAdder::update: %s%s Expected: %2d received: %2d force: %s",
		   this->name.c_str(), this->serviceName.c_str(),
		   cl->inputServices.size(), cl->received, force ? "true" : "false");
  if ( (cl->received >= cl->inputServices.size()) || force )  {
    if ( this->timer && !cl->timeout )    {
      this->timer->stop();
    }
    ///  Normal case,                           forced update after 'stop'
    if ( (cl->reference != cl->updateStamp) )   {
      bool header_corrected = false;
      if ( cl->buffer->used() > sizeof(SerialHeader) )  {
	SerialHeader* header= cl->buffer->begin<SerialHeader>();
	header_corrected = (header && header->run_number != 0);
	header->ser_tim = cl->reference;
	if ( !header_corrected )  {
	  for ( auto& i : cl->inputServices )    {
	    auto& buff = i.second->buffer;
	    if ( buff.used() > sizeof(SerialHeader) )  {
	      SerialHeader* h= i.second->buffer.as<SerialHeader>();
	      if ( h->run_number != 0 )   {
		header->run_number = h->run_number;
		if (this->debugOn) ::lib_rtl_output(LIB_RTL_INFO,"For Adder %s Run Number Fixed to %ld.\n",
						    this->name.c_str(), header->run_number);
		header_corrected = true;
	      }
	    }
	  }
	}
      }
      if ( header_corrected )   {
	::lib_rtl_output(level,"MonAdder::finish_cycle: updating Output Service %s", this->serviceName.c_str());
        this->update(cl);
        cl->updateStamp = cl->reference;
	++this->numUpdateCycles;
        if ( this->CycleFn )   {
	  (*this->CycleFn)(this->CycleArg, *cl->buffer, &cl->monitorItems, this);
	  this->publishSources();
        }
        if (this->doPause && this->PauseFn )   {
	  (*this->PauseFn)(this->PauseArg);
        }
      }
    }
    cl->added     = 0;
    cl->received  = 0;
    cl->reference = 0;
    this->numBuffersCurrent = 0;
  }
}

void MonAdder::update(ClassDescriptor* cl)   {
  if ( this->isSaver )  {
    if ( this->isLocked )    {
      this->isLocked = false;
      this->unlock();
    }
  }
  if ( cl->outputservice )  {
    cl->outputservice->serialize(-1);
    cl->outputservice->update();
    cl->updated = true;
  }
}

/// Callback to sum up all items of a source frame
void MonAdder::add_items(ClassDescriptor* clazz, mem_buff& buffer, MonInfo* info)   {
  int count = 0;
  MonitorItems  monitor_items;
  this->load_item_map(monitor_items, buffer);
  for (const auto& i : monitor_items )  {
    auto j = clazz->monitorItems.find(i.first);
    if ( j != clazz->monitorItems.end() )    {
      auto *summed_item = j->second;
      auto *source_item = i.second;
      if ( !offsetinBounds(summed_item,summed_item->reclen,summed_item->dataoff) )      {
	const string& item_name = j->first;
        ::lib_rtl_output(LIB_RTL_INFO,
			 "Bad Data offset in sum (first source %s) source %s record %s Loop Record number %d",
			 clazz->firstSource.c_str(), info->getName(), item_name.c_str(),count);
        break;
      }
      ++count;
      this->add_item(summed_item, source_item);
    }
    else    {
      clazz->addItem(i.first, i.second);
    }
  }
  clazz->added++;
}

/// Callback to sum up one single item
void MonAdder::add_item(DimBuffBase* /* summed */, const DimBuffBase* /* source */)  {
}
