//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <GauchoAppl/AddSerializer.h>
#include <Gaucho/dimhist.h>
#include <stdexcept>
#include <cstdlib>
#include <regex>

using namespace Online;

AddSerializer::AddSerializer(MonitorItems& itms) : objects(itms)    {
}

std::pair<size_t,void*> AddSerializer::serialize_obj(size_t siz, bool )   {
  void  *pp = nullptr;
  size_t bs = siz;
  printf("AddSerializer: Dump of Object Map\n");
  for( const auto& i : this->objects )   {
    printf("AddSerializer:Histogram %s\n",i.first.c_str());
    DimBuffBase *h = i.second;
    bs += h->reclen;
    if ( pp > h)    {
      pp = h;
    }
  }
  siz = bs;
  return std::make_pair(0,nullptr);
}

std::pair<size_t,void*> AddSerializer::serialize_obj(mem_buff& buff, size_t offset, bool clear)  const  {
  size_t total_size = offset;
  for ( const auto& h : this->objects )
    if ( h.second ) total_size += h.second->reclen;

  void* ptr = buff.allocate(total_size);
  void* obj = add_ptr(ptr, offset);
  for ( const auto& i : this->objects )   {
    DimBuffBase *h = i.second;
    if ( h )    {
      ::memcpy(obj, h, h->reclen);
      if (clear) h->clear();
      obj = add_ptr(obj, h->reclen);
    }
  }
  buff.set_cursor(total_size);
  return std::make_pair(total_size, buff.begin());
}

std::pair<size_t,void*> AddSerializer::serialize_match(const std::string& match, size_t offset, bool clear)  {
  size_t total_size = offset;
  std::vector<DimBuffBase*> items;
  int flags = std::regex_constants::icase | std::regex_constants::ECMAScript;
  std::regex r(match, (std::regex_constants::syntax_option_type)flags);

  items.reserve(this->objects.size());
  for( const auto& i : this->objects )   {
    DimBuffBase *h = i.second;
    if ( h )   {
      std::smatch sm;
      std::string n = add_ptr<char>(h, h->nameoff);
      bool stat = std::regex_match(n, sm, r);
      if ( stat )    {
	items.emplace_back(i.second);
      }
    }
  }
  for ( const auto* h : items )
    total_size += h->reclen;

  void* ptr = this->buffer.allocate(total_size);
  void* obj = add_ptr(ptr, offset);
  for ( auto* h : items )   {
    ::memcpy(obj, h, h->reclen);
    if (clear) h->clear();
    obj = add_ptr(obj, h->reclen);
  }
  return std::make_pair(total_size, ptr);
}

std::pair<size_t,void*> AddSerializer::serialize_obj(const std::vector<std::string>& names, size_t offset, bool clear)  {
  size_t total_size = offset;
  std::vector<DimBuffBase*> items;
  items.reserve(names.size());
  for (unsigned int i=0; i<names.size(); i++)  {
    auto o = this->objects.find(names[i]);
    if ( o != this->objects.end() ) items.emplace_back(o->second);
  }
  for ( const auto* h : items )
    if ( h ) total_size += h->reclen;

  void* ptr = this->buffer.allocate(total_size);
  void* obj = add_ptr(ptr, offset);
  for ( auto* h : items )   {
    if ( h )    {
      ::memcpy(obj, h, h->reclen);
      if (clear) h->clear();
      obj = add_ptr(obj, h->reclen);
    }
  }
  return std::make_pair(total_size, ptr);
}

std::pair<size_t,void*> AddSerializer::serialize_obj(const std::string& name, size_t offset, bool clear)   {
  auto i = this->objects.find(name);
  if ( i != this->objects.end() )   {
    DimBuffBase *h = i->second;
    if ( h != 0 )  {
      void* ptr = this->buffer.allocate(offset + h->reclen);
      ::memcpy(add_ptr(ptr, offset), h, h->reclen);
      if (clear) h->clear();
      this->buffer.set_cursor(offset+h->reclen);
      return std::make_pair(offset+h->reclen, ptr);
    }
  }
  return std::make_pair(offset,this->buffer.allocate(offset,offset));
}

std::pair<size_t,void*> AddSerializer::serialize_dir(size_t offset)   {
  size_t total_size = offset;
  for (auto& i : this->objects )  {
    DimBuffBase *h = i.second;
    total_size += h ? h->namelen + sizeof(int) : 0;
  }
  void*      p = this->buffer.allocate(total_size);
  DimDirEnt* e = add_ptr<DimDirEnt>(p, offset);
  for ( auto& i : this->objects )  {
    DimBuffBase *h = i.second;
    if ( h )   {
      e->type = h->type;
      std::strncpy(e->name,h->name(),h->namelen);
      e = add_ptr<DimDirEnt>(e,sizeof(e->type)+h->namelen);
    }
  }
  this->buffer.set_cursor(total_size);
  return std::make_pair(total_size, p);
}

std::pair<size_t,const void*> AddSerializer::data()   const    {
  return std::make_pair(this->buffer.used(), this->buffer.begin());
}

void AddSerializer::updateExpansions()     {
}

