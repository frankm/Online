//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "GenStatSvc.h"
#include <Gaucho/MonCounter.h>
#include <RTL/strdef.h>

DECLARE_COMPONENT( GenStatSvc )
using namespace Online;


namespace   {

  template <typename T> COutService<T>* _get_service(DimBuffBase *buff, const std::string& pref, GenStatSvc::COUTServiceMap& m)   {
    const char  *name = add_ptr<char>(buff, buff->nameoff);
    std::string  nam  = pref+"/"+name;
    auto  it = m.find(nam);
    auto* os = (COutService<T>*)(it == m.end() ? nullptr : it->second);
    if (os == 0)    {
      m[nam] = os = new COutService<T>(nam);
    }
    return os;
  }

  template <typename PUB,typename SRC=PUB> void _pub_number(DimBuffBase *buff, const std::string& pref, GenStatSvc::COUTServiceMap& m)   {
    const SRC *value = add_ptr<SRC>(buff, buff->dataoff);
    auto* os = _get_service<PUB>(buff, pref, m);
    os->Update(*value);
  }

  template <typename T> void _pub_avg(DimBuffBase *buff, const std::string& pref, GenStatSvc::COUTServiceMap& m)   {
    auto   *os    = _get_service<double>(buff, pref, m);
    long   *ents  = add_ptr<long>(buff,buff->dataoff);
    double *mean  = add_ptr<double>(ents,sizeof(long));
    double  value = (double(*ents) * (*mean));
    os->Update(value);
  }
#define _pub_stat  _pub_avg
#define _pub_sigma _pub_avg

  void _pub_effi(DimBuffBase *buff, const std::string& pref, GenStatSvc::COUTServiceMap& m)   {
    auto *os = _get_service<double>(buff, pref, m);
    long *trueEntries  = add_ptr<long>(buff,buff->dataoff);
    long *falseEntries = add_ptr<long>(trueEntries,sizeof(long));
    double denom = std::max(1e0, double(*trueEntries + *falseEntries));
    os->Update(double(*trueEntries) / denom);
  }
}

StatusCode GenStatSvc::start()  {
  auto sc = PubSvc::start();
  m_prefix = RTL::str_replace(m_prefix,"<part>",m_PartitionName);
  startCycling();
  DimServer::autoStartOn();
  return sc;
}

void GenStatSvc::analyze(mem_buff& /* buf */, MonitorItems* mmap)  {
  for ( const auto& it : *mmap )  {
    DimBuffBase *buff = (DimBuffBase*)it.second;
    switch(buff->type)    {
    case C_INT:
    case C_UINT:
    case C_ATOMICINT:         _pub_number<int,int>(buff, m_prefix, m_outmap);       break;

    case C_ULONG:
    case C_ATOMICLONG:        _pub_number<long long,long>(buff, m_prefix, m_outmap);break;
    case C_LONGLONG:          _pub_number<long long,long long>(buff, m_prefix, m_outmap);break;

    case C_FLOAT:
    case C_RATEFLOAT:
    case C_ATOMICFLOAT:       _pub_number<float,float>(buff, m_prefix, m_outmap);   break;

    case C_DOUBLE:
    case C_RATEDOUBLE:
    case C_ATOMICDOUBLE:      _pub_number<float,double>(buff, m_prefix, m_outmap);  break;

      /// ==========================================================================================
    case C_GAUDIACCCHAR:
    case C_GAUDIACCuCHAR:     _pub_number<int,char>(buff, m_prefix, m_outmap);      break;
    case C_GAUDIACCSHORT:
    case C_GAUDIACCuSHORT:    _pub_number<int,short>(buff, m_prefix, m_outmap);     break;
    case C_GAUDIACCINT:
    case C_GAUDIACCuINT:      _pub_number<long long,int>(buff, m_prefix, m_outmap); break;
    case C_GAUDIACCLONG:
    case C_GAUDIACCuLONG:     _pub_number<long long,long>(buff, m_prefix, m_outmap);break;
    case C_GAUDIACCFLOAT:     _pub_number<float,double>(buff, m_prefix, m_outmap);  break;
    case C_GAUDIACCDOUBLE:    _pub_number<double,double>(buff, m_prefix, m_outmap); break;
      /// ==========================================================================================
    case C_GAUDIAVGACCc:      _pub_avg<signed char>(buff, m_prefix, m_outmap);      break;
    case C_GAUDIAVGACCcu:     _pub_avg<unsigned char>(buff, m_prefix, m_outmap);    break;
    case C_GAUDIAVGACCs:      _pub_avg<short>(buff, m_prefix, m_outmap);            break;
    case C_GAUDIAVGACCsu:     _pub_avg<unsigned short>(buff, m_prefix, m_outmap);   break;
    case C_GAUDIAVGACCi:      _pub_avg<int>(buff, m_prefix, m_outmap);              break;
    case C_GAUDIAVGACCiu:     _pub_avg<unsigned int>(buff, m_prefix, m_outmap);     break;
    case C_GAUDIAVGACCl:      _pub_avg<long>(buff, m_prefix, m_outmap);             break;
    case C_GAUDIAVGACClu:     _pub_avg<unsigned long>(buff, m_prefix, m_outmap);    break;
    case C_GAUDIAVGACCf:      _pub_avg<float>(buff, m_prefix, m_outmap);            break;
    case C_GAUDIAVGACCd:      _pub_avg<double>(buff, m_prefix, m_outmap);           break;
      /// ==========================================================================================
    case C_GAUDISTATACCc:     _pub_stat<signed char>(buff, m_prefix, m_outmap);     break;
    case C_GAUDISTATACCcu:    _pub_stat<unsigned char>(buff, m_prefix, m_outmap);   break;
    case C_GAUDISTATACCs:     _pub_stat<short>(buff, m_prefix, m_outmap);           break;
    case C_GAUDISTATACCsu:    _pub_stat<unsigned short>(buff, m_prefix, m_outmap);  break;
    case C_GAUDISTATACCi:     _pub_stat<int>(buff, m_prefix, m_outmap);             break;
    case C_GAUDISTATACCiu:    _pub_stat<unsigned int>(buff, m_prefix, m_outmap);    break;
    case C_GAUDISTATACCl:     _pub_stat<long>(buff, m_prefix, m_outmap);            break;
    case C_GAUDISTATACClu:    _pub_stat<unsigned long>(buff, m_prefix, m_outmap);   break;
    case C_GAUDISTATACCf:     _pub_stat<float>(buff, m_prefix, m_outmap);           break;
    case C_GAUDISTATACCd:     _pub_stat<double>(buff, m_prefix, m_outmap);          break;

      /// ==========================================================================================
    case C_GAUDISIGMAACCc:    _pub_sigma<signed char>(buff, m_prefix, m_outmap);    break;
    case C_GAUDISIGMAACCcu:   _pub_sigma<unsigned char>(buff, m_prefix, m_outmap);  break;
    case C_GAUDISIGMAACCs:    _pub_sigma<short>(buff, m_prefix, m_outmap);          break;
    case C_GAUDISIGMAACCsu:   _pub_sigma<unsigned short>(buff, m_prefix, m_outmap); break;
    case C_GAUDISIGMAACCi:    _pub_sigma<int>(buff, m_prefix, m_outmap);            break;
    case C_GAUDISIGMAACCiu:   _pub_sigma<unsigned int>(buff, m_prefix, m_outmap);   break;
    case C_GAUDISIGMAACCl:    _pub_sigma<long>(buff, m_prefix, m_outmap);           break;
    case C_GAUDISIGMAACClu:   _pub_sigma<unsigned long>(buff, m_prefix, m_outmap);  break;
    case C_GAUDISIGMAACCf:    _pub_sigma<float>(buff, m_prefix, m_outmap);          break;
    case C_GAUDISIGMAACCd:    _pub_sigma<double>(buff, m_prefix, m_outmap);         break;

    case C_GAUDIBINACCc:
    case C_GAUDIBINACCcu:
    case C_GAUDIBINACCs:
    case C_GAUDIBINACCsu:
    case C_GAUDIBINACCi:
    case C_GAUDIBINACCiu:
    case C_GAUDIBINACCl:
    case C_GAUDIBINACClu:
    case C_GAUDIBINACCf:
    case C_GAUDIBINACCd:      _pub_effi(buff, m_prefix, m_outmap);                  break;
    default:
      break;
    }
  }
}

GenStatSvc::GenStatSvc(const std::string& name, ISvcLocator* svc)
  : PubSvc(name, svc)
{
  declareProperty("ServicePrefix", m_prefix = "");
}

GenStatSvc::~GenStatSvc()  {
  for (const auto& i : m_outmap)
    delete i.second;
  m_outmap.clear();
}
