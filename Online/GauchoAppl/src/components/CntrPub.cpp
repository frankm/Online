//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "CntrPub.h"
#include <AIDA/IHistogram.h>
#include <Gaucho/MonCounter.h>

DECLARE_COMPONENT( CntrPub )

CntrPub::CntrPub(const std::string& name, ISvcLocator* sl) :  PubSvc(name, sl)
{
  this->declareProperty("ServiceInfix",   this->m_SvcInfix);
  this->declareProperty("CounterPattern", this->m_counterPattern);
}

StatusCode CntrPub::start()  {
  StatusCode sc = this->PubSvc::start();
  this->startCycling();
  return sc;
}

StatusCode CntrPub::initialize()   {
  StatusCode sc = this->PubSvc::initialize();
  this->m_counterPatternRegex = std::regex(this->m_counterPattern,std::regex_constants::icase);
  return sc;
}

StatusCode CntrPub::finalize()   {
  for (auto& mit : this->m_cntrSvcMap )
    delete mit.second;
  this->m_cntrSvcMap.clear();
  return this->PubSvc::finalize();
}

void CntrPub::analyze(mem_buff&, Online::MonitorItems* mmap)
{
  for ( auto& it : *mmap )  {
    bool status = std::regex_search(it.first, m_counterPatternRegex);
    if (status)    {
      SVCDescr *sdes { nullptr };
      auto mit = m_cntrSvcMap.find(it.first);
      if (mit == m_cntrSvcMap.end())      {
        std::string svcnam = it.first;
        sdes = new SVCDescr;

        if (svcnam.find("R_",0,2) != std::string::npos)        {
          sdes->name = "stat/"+m_PartitionName+"_x_"+svcnam.substr(2,std::string::npos)+"/Rate";
          sdes->svc = new DimService(this->dns(), (char*) sdes->name.c_str(),sdes->rdata);
          sdes->type = 1;
        }
        else        {
          sdes->name = "stat/"+m_PartitionName+"_x_"+svcnam+"/Count";
          sdes->svc = new DimService(this->dns(), (char*) sdes->name.c_str(),sdes->idata);
          sdes->type = 0;
        }
        m_cntrSvcMap.insert(std::make_pair(svcnam,sdes));
      }
      else
      {
        sdes = (*mit).second;
      }
      auto *d = Online::CounterSerDes::de_serialize(it.second);
      if (sdes->type == 1)
      {
        sdes->rdata = d->d_data;
      }
      else
      {
        if (d->type == C_INT)
        {
          sdes->idata = d->i_data;
        }
        else if (d->type == C_LONGLONG)
        {
          sdes->idata = int(d->l_data);
        }
      }
      sdes->svc->updateService();
    }
  }
}

