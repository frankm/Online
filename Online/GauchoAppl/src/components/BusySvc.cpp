//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "BusySvc.h"

#include <GaudiKernel/IMonitorSvc.h>
#include <Gaucho/GenTimer.h>
#include <RTL/strdef.h>

using namespace Online;
DECLARE_COMPONENT( BusySvc )

namespace {
  inline long mem_unit(const std::string& unit)   {
    long fact=1;
    if (unit == "kB")
      fact = 1000;
    else if (unit == "B")
      fact = 1;
    else if (unit == "MB")
      fact = 1000000;
    return fact;
  }
  long mem_in_bytes(const std::vector<std::string>& toks)   {
    return std::stol(toks.at(1)) * ((toks.size() > 2) ? mem_unit(toks.at(2)) : 1);
  }
}

class BusySvc::IdleTimer : public GenTimer   {
public:
  BusySvc *m_bsysvc;
  IdleTimer(BusySvc *master, int period = 5000) : GenTimer(period), m_bsysvc(master)  {
  }
  ~IdleTimer() = default;
  void timerHandler(void) override    {
    m_bsysvc->calcIdle();
  }
};

BusySvc::BusySvc(const std::string& nam, ISvcLocator* sl) : Service(nam,sl)  {
  declareProperty("BogusMips",          m_bogus = 0.0);
  declareProperty("ExtendedMonitoring", m_extendedMonitoring = false);
  ::memset(m_times_old, 0, sizeof(m_times_old));
  m_first = true;
}

StatusCode BusySvc::initialize()   {
  StatusCode sc = Service::initialize();
  if( !sc.isSuccess() )  {
    return StatusCode::FAILURE;
  }
  m_monitorSvc = this->service("MonitorSvc", true);
  if( m_monitorSvc )  {
    return StatusCode::FAILURE;
  }
  if (m_bogus == 0.0)  {
    m_mybogus = this->getBogus();
  }
  auto node_name = RTL::str_upper(RTL::nodeNameShort());
  auto node_class = this->nodeClass(node_name);
  node_class = "PLUS";
  m_bogus     = m_mybogus;
  calcIdle();
  m_timer = std::make_unique<IdleTimer>(this);
  m_monitorSvc->declareInfo(node_name+"/NodeBusy",m_busyFraction,"",this);
  if ( m_extendedMonitoring )   {
    m_monitorSvc->declareInfo("IdleFraction",m_idlebogus,"",this);
    m_monitorSvc->declareInfo("BogoMIPS",m_bogus,"",this);
    m_monitorSvc->declareInfo("busyFraction",m_busybogus,"",this);
    m_monitorSvc->declareInfo("NumCores",m_numCores,"",this);

    m_monitorSvc->declareInfo("TotMemory",m_memtot,"",this);
    m_monitorSvc->declareInfo("FreeMemory",m_memfree,"",this);
    m_monitorSvc->declareInfo("MemBuffers",m_membuff,"",this);
    m_monitorSvc->declareInfo("SwapSpaceTot",m_memSwaptot,"",this);
    m_monitorSvc->declareInfo("SwapSpaceFree",m_memSwapfree,"",this);
    m_monitorSvc->declareInfo("MemAvail",m_memAvail,"",this);

    m_monitorSvc->declareInfo(node_class+"/IdleFraction",m_idlebogus,"",this);
    m_monitorSvc->declareInfo(node_class+"/BogoMIPS",m_bogus,"",this);
    m_monitorSvc->declareInfo(node_class+"/busyFraction",m_busybogus,"",this);
    m_monitorSvc->declareInfo(node_class+"/NumCores",m_numCores,"",this);

    m_monitorSvc->declareInfo(node_class+"/TotMemory",m_memtot,"",this);
    m_monitorSvc->declareInfo(node_class+"/FreeMemory",m_memfree,"",this);
    m_monitorSvc->declareInfo(node_class+"/MemBuffers",m_membuff,"",this);
    m_monitorSvc->declareInfo(node_class+"/SwapSpaceTot",m_memSwaptot,"",this);
    m_monitorSvc->declareInfo(node_class+"/SwapSpaceFree",m_memSwapfree,"",this);
    m_monitorSvc->declareInfo(node_class+"/MemAvail",m_memAvail,"",this);
  }
  return StatusCode::SUCCESS;
}

StatusCode BusySvc::finalize()  {
  this->m_timer->stop();
  this->m_timer.reset();
  return this->Service::finalize();
}

StatusCode BusySvc::start()  {
  StatusCode sc = this->Service::start();
  if ( sc.isSuccess() )   {
    this->m_bogus = m_mybogus;
    this->m_timer->start();
  }
  return sc;
}

std::string BusySvc::nodeClass(const std::string& node_name)  const  {
  FILE *f = std::fopen("/group/online/ecs/FarmNodes.txt","r");
  std::string c = "";
  if ( f != nullptr )  {
    char  line[256];
    char *stat = line;
    while (stat != 0)  {
      stat = std::fgets(line,sizeof(line),f);
      if ( stat )   {
	auto l = RTL::str_split(line," ");
	if ( l.at(0) == node_name)    {
	  c = l.at(1);
	  break;
	}
      }
    }
    fclose(f);
  }
  return c;
}

double BusySvc::getBogus()   {
  char   line[256];
  float  bogo  = 0e0;
  int    model = 0;
  int    proc  = 0;
  double bogus = 0e0;
  FILE *input = std::fopen("/proc/cpuinfo", "r");
  char *stat = line;
  m_numCores = 0;
  while (stat != 0)  {
    stat = std::fgets(line, sizeof(line), input);
    if (stat == 0)
      break;
    if (strstr(line, "processor\t") != 0)    {
      auto l = RTL::str_split(line, ":");
      std::sscanf(l.at(1).c_str(), "%d", &proc);
      m_numCores++;
    }
    else if (strstr(line, "bogomips\t") != 0)    {
      auto l = RTL::str_split(line, ":");
      std::sscanf(l.at(1).c_str(), "%f", &bogo);
      bogus += bogo;
    }
    else if (strstr(line, "model\t") != 0)    {
      auto l = RTL::str_split(line, ":");
      std::sscanf(l.at(1).c_str(), "%d", &model);
    }
  }
  std::fclose(input);
  ::lib_rtl_output(LIB_RTL_INFO,"Number of Cores: %d.\n",m_numCores);
//  if (model == 23)
//  {
//    bogus = 270.0;
//  }
//  else if (model == 44)
//  {
//    bogus = 960.0;
//  }
//  else if (model == 1)
//  {
//    bogus = 960.0;
//  }
//  bogus = bogo;
  //  printf ("Bogomips %f %f\n",bogo,bogus);
  return bogus;
}

void BusySvc::calcIdle()   {
  char   line[256];
  double p_id = 0.0;
  long   dtimes[7], times[7];
  FILE  *input = std::fopen("/proc/stat","r");
  char  *stat = line;
  while ( stat != 0 )  {
    stat = std::fgets(line,sizeof(line),input);
    if ( stat != 0 )   {
      if ( std::strstr(line,"cpu ") != 0 )    {
	auto l = RTL::str_split(line," ");
	long t = 0;
	for (int i=1;i<=7;i++)   {
	  std::sscanf(l.at(i).c_str(), "%ld",&times[i-1]);
	}
	if (!m_first)    {
	  for (int i=0;i<7;i++)    {
	    dtimes[i] = times[i]-m_times_old[i];
	    t += dtimes[i];
	  }
	  p_id = double(dtimes[3])/t;
	}
	::memcpy(m_times_old, times, sizeof(m_times_old));
	m_first = false;
	break;
      }
    }
  }
  std::fclose(input);
  m_bogus        = m_mybogus;
  m_idlebogus    = p_id*m_bogus;
  m_busyFraction = (1.0-p_id);
  m_busybogus    = (1.0-p_id)*m_bogus;

  input = std::fopen("/proc/meminfo","r");
  stat = line;
  while (stat != 0)  {
    stat = std::fgets(line, sizeof(line), input);
    if ( stat != 0 )    {
      auto toks = RTL::str_split(line," ");
      const auto& tok = toks.at(0);

      if ( tok.find("MemTotal") != std::string::npos )
	m_memtot = mem_in_bytes(toks);
      else if ( tok.find("MemFree") != std::string::npos )
	m_memfree = mem_in_bytes(toks);
      else if ( tok.find("Buffers") != std::string::npos )
	m_membuff = mem_in_bytes(toks);
      else if ( tok.find("SwapTotal") != std::string::npos )
	m_memSwaptot = mem_in_bytes(toks);
      else if ( tok.find("SwapFree") != std::string::npos )
	m_memSwapfree = mem_in_bytes(toks);
      else if ( tok.find("MemAvailable") != std::string::npos )
	m_memAvail = mem_in_bytes(toks);
    }
  }
  std::fclose(input);
}
