//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef _PASSDEF_H
#define _PASSDEF_H
#define PA_NOMORE  2 //....... Used by PubArea::GetSlotofType to scan over slots
#define PA_SUCCESS 1 //...................................... Everything went OK
#define PA_FAILURE 0 //..................................... Something was wrong
#endif
