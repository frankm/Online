
#ifndef ONLINEBASE_CPP_MEM_BUF_H
#define ONLINEBASE_CPP_MEM_BUF_H


#include <memory>
#include <cstring>


/// Online namespace declaration
namespace Online  {

  /// Memory buffer class
  class mem_buff  {
  private:
    std::unique_ptr<unsigned char[]> data  {   };
    unsigned char* pointer                 { nullptr };
    size_t size                            { 0 };
  public:
    /// Default constructor
    mem_buff() = default;
    /// Initializing constructor with memory allocation
    mem_buff(size_t len);
    /// Initializing constructor with memory allocation and cursor at offset
    mem_buff(size_t len, size_t offset);
    /// Initializing constructor with memory copy
    mem_buff(const void* pointer, size_t len);
    /// Default destructor
    ~mem_buff() = default;
    /// Access the start-pointer of the buffer memory.
    unsigned char*       begin()        {   return this->data.get();               }
    /// Access the start-pointer of the buffer memory.
    const unsigned char* begin() const  {   return this->data.get();               }
    /// Access the end-pointer of the buffer memory.
    unsigned char*       end()          {   return this->data.get() + this->size;  }
    /// Access the end-pointer of the buffer memory.
    const unsigned char* end() const    {   return this->data.get() + this->size;  }
    /// Access the current cursor of the buffer object
    unsigned char* ptr()                {   return this->pointer;                  }
    /// Access the current cursor of the buffer object
    const unsigned char* ptr() const    {   return this->pointer;                  }
    /// Access the total buffer length
    size_t length()  const              {   return this->size;                     }
    /// Access the number of used bytes in the buffer
    size_t used()    const              {   return this->pointer - this->begin();  }

    /// Swap two buffers
    void swap(mem_buff& other);

    /// Copy data to the buffer at a specified offset. If insufficient memory reallocation takes place
    unsigned char* copy(size_t offset, const void* pointer, size_t len);
    /// Copy data to the buffer at a specified offset. If insufficient memory reallocation takes place
    unsigned char* copy(size_t offset, const mem_buff& copy);

    /// Copy data to the buffer at the start. If insufficient memory reallocation takes place
    unsigned char* copy(const void* pointer, size_t len);
    /// Copy data to the buffer at the start. If insufficient memory reallocation takes place
    unsigned char* copy(const mem_buff& copy);

    /// Append data to the buffer at the cursor. If insufficient memory reallocation takes place
    unsigned char* append(const void* pointer, size_t len);
    /// Append data to the buffer at the cursor. If insufficient memory reallocation takes place
    unsigned char* append(const mem_buff& buffer);

    /// Allocate a new chunk of memory of the specified size.
    unsigned char* allocate(size_t len, bool reset=false);
    /// Reallocate a new chunk of memory of the specified size. Old data are preserved.
    unsigned char* reallocate(size_t len);

    /// Reset the buffer memory (aka memset)
    void reset(unsigned char val = 0);
    /// Set the buffer cursor to a specified location
    void set_cursor(size_t offset);

    template <typename T = unsigned char> T* as();
    template <typename T = unsigned char> const T* as()  const;
    template <typename T = unsigned char> T* begin();
    template <typename T = unsigned char> const T* begin()  const;
    template <typename T = unsigned char> T* at(size_t offset=0);
    template <typename T = unsigned char> const T* at(size_t offset=0)  const;
    //template <typename T = unsigned char> T* advance(size_t offset);
  };


  inline mem_buff::mem_buff(size_t len) : size(len)  {
    this->data.reset(pointer = new unsigned char[this->size]);
  }

  inline mem_buff::mem_buff(size_t len, size_t offset) : size(len)  {
    this->data.reset(this->pointer = new unsigned char[this->size]);
    this->pointer += offset;
  }

  inline mem_buff::mem_buff(const void* source, size_t len) : size(len)  {
    this->data.reset(this->pointer = new unsigned char[this->size]);
    ::memcpy(this->pointer, source, this->size);
  }

  /// Swap two buffers
  inline void mem_buff::swap(mem_buff& other)    {
    auto tmp_data     = std::move(this->data);
    auto tmp_pointer  = this->pointer;
    auto tmp_size     = this->size;
    this->data    = std::move(other.data);
    this->pointer = other.pointer;
    this->size    = other.size;
    other.data    = std::move(tmp_data);
    other.pointer = tmp_pointer;
    other.size    = tmp_size;
  }

  inline unsigned char* mem_buff::allocate(size_t len, bool reset)    {
    if ( len > this->size )   {
      this->data.reset(this->pointer = new unsigned char[this->size = len]);
    }
    if ( reset )   {
      ::memset(this->data.get(), 0, this->size);
    }
    return this->pointer = this->data.get();
  }

  inline unsigned char* mem_buff::reallocate(size_t len)    {
    if ( len > this->size )   {
      size_t old_len = this->size;
      size_t offset  = this->pointer - this->data.get();
      std::unique_ptr<unsigned char[]> old(this->data.release());
      this->data.reset(this->pointer = new unsigned char[this->size = len]);
      if ( old_len )   {
	::memcpy(this->pointer, old.get(), old_len);
	this->pointer += offset;
      }
    }
    // Advance pointer to old position or not?
    return this->pointer;
  }

  inline void mem_buff::reset(unsigned char val)   {
    if ( this->data.get() )   {
      ::memset(this->data.get(), val, this->size);
      this->pointer = this->data.get();
    }
  }

  inline unsigned char* mem_buff::copy(size_t offset, const void* source, size_t len)   {
    if ( !this->data.get() || (offset+len) > this->size )   {
      this->allocate(offset+len);
    }
    this->pointer = this->begin()+offset;
    ::memcpy(this->pointer, source, len);
    this->pointer += len;
    return this->pointer;
  }

  inline unsigned char* mem_buff::copy(const void* source, size_t len)   {
    return this->copy(0, source, len);
  }

  inline unsigned char* mem_buff::copy(size_t offset, const mem_buff& copy)   {
    return this->copy(offset, copy.data.get(), copy.used());
  }

  inline unsigned char* mem_buff::copy(const mem_buff& copy)   {
    return this->copy(0, copy.data.get(), copy.used());
  }

  inline unsigned char* mem_buff::append(const void* source, size_t len)   {
    if ( !this->data.get() )
      this->allocate(len);
    else if ( this->pointer + len > this->data.get() + this->size )
      this->reallocate(1.5*(this->size + len));
    ::memcpy(this->pointer, source, len);
    this->pointer += len;
    return this->pointer;
  }

  inline unsigned char* mem_buff::append(const mem_buff& buff)   {
    return this->append(buff.data.get(), buff.used());
  }

  inline void mem_buff::set_cursor(size_t offset)   {
    this->pointer = this->data.get() + offset;
  }

#if 0
  template <typename T> inline T* mem_buff::advance(size_t offset)   {
    auto* p = (T*)this->pointer;
    this->pointer += offset;
    return p;
  }
#endif

  template <typename T> inline T* mem_buff::as()   {
    return (T*)this->data.get();
  }

  template <typename T> inline const T* mem_buff::as()  const {
    return (const T*)this->data.get();
  }

  template <typename T> inline T* mem_buff::begin()   {
    return (T*)this->data.get();
  }

  template <typename T> inline const T* mem_buff::begin()  const {
    return (const T*)this->data.get();
  }

  template <typename T> inline T* mem_buff::at(size_t offset)   {
    return (T*)(this->data.get() + offset);
  }

  template <typename T> inline const T* mem_buff::at(size_t offset)  const {
    return (const T*)(this->data.get() + offset);
  }
}

#endif // ONLINEBASE_CPP_MEM_BUF_H
