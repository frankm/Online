//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : OnlineBase
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINEBASE_NET_TcpNetworkChannel_H__
#define ONLINEBASE_NET_TcpNetworkChannel_H__

#include "CPP/EventHandler.h"
#include "NET/NetworkChannel.h"

/// Definition of the TCP NetworkChannel data structure
/**
 *  {\Large{\bf Class TcpNetworkChannel}}
 *
 *  Networking object based on TCP sockets, which allows sending and receiving with timeouts
 *
 *
 *  \author  M.Frank
 *  \version 1.0
 */
class TcpNetworkChannel : public NetworkChannel {
public:
  struct IOSB  {
    unsigned short condition;          /* I/O status code           */
    unsigned short count;              /* Number of Bytes I/O'ed    */
    unsigned int   information;        /* Device specific info      */
  };
protected:
  //@Man: protected variables
  /// Accept IOSB:
  IOSB m_iosb;
  /// Dummy receive buffer
  int  m_recvBuff;
  /// Default ACTION after AST callback -> Call handler callback....
  static int _defaultAction ( void* par );
public:
  //@Man: public member functions
  /// Standard Constructor to initialize the connection for CONNECTOR
  TcpNetworkChannel();
  /// Constructor to initialize the connection for ACCEPTOR
  explicit TcpNetworkChannel(Channel channel);
  /// Destructor: closes channel
  ~TcpNetworkChannel();
  /// Connect to network partner (Connector)
  int connect ( const Address&  addr, int tmo = 0) override;
  /// Bind Address + listen to specified connection (Acceptor)
  int bind( const Address& addr, int con_pend = 5 ) override;
  /// Accept connection on socket (Acceptor)
  virtual Channel accept( Address& addr, int tmo = 0 );
  /// Set send buffer size
  int setSendBufferSize(int len);
  /// Set receive buffer size
  int setReceiveBufferSize(int len);
  /// send data to network partner.
  int send  (const void* buff, int len, int tmo = 0, int flags = 0, const Address* addr = 0) override;
  /// receive data from network partner.
  int recv  (void* buff, int len, int tmo = 0, int flags = 0, Address* addr = 0) override;
  /// Queue Accept call
  int queueAccept  ( Port port, CPP::EventHandler *handler );
  /// Queue receive call
  int queueReceive ( Port port, CPP::EventHandler *handler );
  /// Queue receive call
  int _unqueueIO ( Port port );
  /// Cancel eventually pending I/O requests
  int cancel() override;
  /// Set socket option to reuse address in case of re-creation
  int reuseAddress()  const override;
};
#endif  /* ONLINEBASE_NET_TcpNetworkChannel_H__  */
