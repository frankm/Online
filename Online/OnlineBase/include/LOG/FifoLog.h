//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  Fifo based output logger
//--------------------------------------------------------------------------
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ONLINE_FIFLOLOG_LOGGER_H
#define ONLINE_FIFLOLOG_LOGGER_H

/// C/C++ include files
#include <string>
#include <memory>

/// FIFLOLOG namespace declaration
namespace fifolog  {

  /// Logger class capturing stdout and stderr.
  /**
   *  \author  M.Frank
   *  \version 1.0
   */
  class Logger   {
  public:
    class Implementation;
    /// Place holder for implementation details
    std::unique_ptr<Implementation> imp;

  public:
    /// Initializing constructor
    Logger(const std::string& fifo, const std::string& utgid, const std::string& tag);
    /// Default destructor
    ~Logger();
    /// Run the logger in a separate thread
    void run();
    /// Stop processing and shutdown
    void stop();
  };
}

extern "C"  {
  /// Initialize the logger unit
  void fifolog_initialize_logger();
  /// Set fifo value (Only possible BEFORE startup (return=1), otherwise returns NULL)
  int  fifolog_set_fifo(const char* fifo);
  /// Set new utgid value
  void fifolog_set_utgid(const char* new_utgid);
  /// Set new tag value
  void fifolog_set_tag(const char* new_tag);
  /// Print a message
  void fifolog_print(int level, const char* source, const char* message);
  /// Shutdown the logger unit
  void fifolog_finalize_logger();
}

#endif // ONLINE_FIFLOLOG_LOGGER_H
