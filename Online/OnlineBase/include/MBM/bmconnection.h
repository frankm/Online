//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef _MBMLIB_MBMCONNCTION_H
#define _MBMLIB_MBMCONNCTION_H



union MBMConnection  {
  char           name[128];       // Name of answer connection
  struct ServerConnection {
    char         name[128];       // Name of answer connection
    int          request;
    int          poll;
  } master;
  struct FifoServerConnection {
    char         name[128];       // Name of answer connection
    int          request;
    int          response;
  } client;
  struct FifoClientConnection {
    char         name[128];       // Name of answer connection
    int          request;
    int          response;
  } server;
  struct ShmConnection {
    char         name[128];       // Name of answer connection
    int          slot;
    int          response;
  } shm;
  struct AnyConnection {
    char         name[128];       // Name of answer connection
    void*        channel;
  } any;
  void init()  {
    name[0] = 0;
    client.request  = -1;
    client.response = -1;
  }
  bool hasResponse()  {  return client.response > 0; }
};

#endif // _MBMLIB_MBMCONNCTION_H
