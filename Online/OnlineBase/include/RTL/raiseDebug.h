//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
/*
 * raiseDebug.h
 *
 *  Created on: Aug 26, 2017
 *      Author: beat
 */

#ifndef ONLINE_ONLINEBASE_ONLINEBASE_RTL_RAISEDEBUG_H_
#define ONLINE_ONLINEBASE_ONLINEBASE_RTL_RAISEDEBUG_H_

namespace RTL   {
  void raiseDebug(void);
}
#endif /* ONLINE_ONLINEBASE_ONLINEBASE_RTL_RAISEDEBUG_H_ */
