//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  Fifo Logger implementation
//--------------------------------------------------------------------------
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

#include "LOG/FifoLog.inl.h"
