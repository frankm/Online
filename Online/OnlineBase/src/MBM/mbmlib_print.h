//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef _MBM_MBMLIB_PRINT_H
#define _MBM_MBMLIB_PRINT_H


#ifndef MBM_PRINT
inline int _mbm_printf(const char* , ...)  {  return 1;   }
#else
inline int _mbm_printf(const char* fmt, ...)  {
  char buff[1024];
  va_list args;
  va_start(args, fmt);
  int len = ::vsnprintf(buff, sizeof(buff), fmt, args);
  ::lib_rtl_output(LIB_RTL_ERROR,buff);
  return len;
}
#endif

#endif // _MBM_MBMLIB_PRINT_H
