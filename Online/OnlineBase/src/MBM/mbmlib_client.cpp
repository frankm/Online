/*-----------------------------------------------------------------------*/
/*
 *   OS-9 BUFFER MANAGER
 *   TRAP MODULE  (C ROUTINES)
 *
 * Edition History
 *
 *  #   Date     Comments                                              By
 * -- -------- ------------------------------------------------------ ---
 *  0  28/09/88  Initial version                                       PM
 *  1  11/11/88  Released version 1.0                                  PM
 *  2  29/11/88  Multibuffer and spy introduced                        PM
 *  3  15/12/88  Released version 2.0                                  PM
 *  4  14/03/89  Minor corrections                                     PM
 *  5  24/09/91  Allow waiting space and event at the same time        PM
 *  6  06/10/92  Add update request for reformatting tasks          PM/BJ
 * ---
 *  7  ??/12/92  Multi-Buffer Manager calls                            BJ
 *  8  25/03/93  Basic clean-up                                      AMi8
 *  9  10/10/06  Major cleanup, move to C++ and implementation
 *               on linux and WIN32                                   MSF
 * 10  12/12/12  Major reimplementation using a client server         MSF
 *               approach.
 * 11  17/05/19  Add possibility for shared memory communication.
 *               Now it screams: up to 300 kHz!                       MSF
 *-----------------------------------------------------------------------*/

#define MBM_IMPLEMENTATION

/// Framework include files
#include <MBM/bmdef.h>
#include <MBM/bmstruct.h>
#include <MBM/bmserver.h>

#include "mbmlib_print.h"
#include "mbmlib_client.h"
#include "mbmlib_message.h"

#ifndef MBM_CLIENT_STANDALONE
#include "mbmlib_comm_shm.h"
#endif

/// C/C++ include files
#include <memory>
#include <cerrno>
#include <string>

using std::string;

#define MBM_CHECK_CONS(bm) if(bm==0 || bm==MBM_INV_DESC || bm->user==0) {return MBM_ILL_CONS;}
#define MBM_CHECK_BMID(bm) if(bm==0 || bm==MBM_INV_DESC) {return MBM_ERROR;}

#if 0
static inline int mbm_error(const char* fn, int line)  {  
  ::lib_rtl_output(LIB_RTL_ERROR,"MBM Error at %s Line:%d.\n",fn,line);
  return MBM_ERROR;  
}
//#undef  MBM_ERROR
//#define MBM_ERROR mbm_error(__FILE__,__LINE__);
#endif

static  qentry_t *desc_head = 0;
static  int reference_count = 0;
typedef MBMMessage MSG;
typedef std::lock_guard<std::mutex> LOCK;


/** MBM Client communication functions   */
/// Open connection to server process
int MBMClientCommunication::open_server(const char* bm_name, const char* name)  const  {
  return m_open_server(bm, bm_name, name);
}

/// Close connection to server process
int MBMClientCommunication::close_server()  const  {
  return m_close_server(bm);
}

/// Move server connection to worker thread
int MBMClientCommunication::move_server(const char* bm_name, USER* u, int serverid)  const  {
  return m_move_server ? m_move_server(bm, bm_name, u, serverid) : MBM_NORMAL;
}

/// Exchange MBM message with server
int MBMClientCommunication::communicate_server(MBMMessage& msg, int* cancelation_flag)  const  {
  return m_communicate_server(bm, msg, cancelation_flag);
}

/// Interrupt MBM message exchange with server
int MBMClientCommunication::interrupt_server()  const  {
  return m_interrupt_server ? m_interrupt_server(bm) : MBM_NORMAL;
}

/// Clear possibly pending data from communication channel
int MBMClientCommunication::clear_server()  const  {
  return m_clear_server ? m_clear_server(bm) : MBM_NORMAL;
}

/// Send request to server
int MBMClientCommunication::send_request_server(MBMMessage& msg, bool clear_before)  const   {
  return m_send_request_server(bm, msg, clear_before);
}

/// Read server response
int MBMClientCommunication::read_response_server(MBMMessage& msg, int* cancelled)   const  {
  return m_read_response_server(bm, msg, cancelled);
}

void _mbm_start_debugger() {
  char text[256];
  ::snprintf(text,sizeof(text),"/usr/bin/gdb --pid %d",::lib_rtl_pid());
  ::system(text);
}

int _mbm_comm_message(BMID bm, int code) {
  MBM_CHECK_BMID(bm);
  MSG msg(code,bm->user);
  LOCK lck (bm->lock);
  return bm->communication.communicate_server(msg,0);
}

int _mbm_cons_message(BMID bm, int code) {
  MBM_CHECK_CONS(bm);
  MSG msg(code,bm->user);
  LOCK lck (bm->lock);
  return bm->communication.communicate_server(msg,0);
}

// clean-up this user in all buffers
int _mbm_shutdown (void* /* param */) {
  qentry_t *q, *bmq = desc_head;
  BMID bm, ids[32];
  MSG msg(MSG::FORCE_SHUTDOWN);
  int cnt = 0, sc = 1, len=sizeof(ids)/sizeof(ids[0]);

  if ( 0 == reference_count )   {
    return MBM_NORMAL;
  }
  else if (bmq == 0)  {
    return MBM_NORMAL;
  }
  reference_count = 0;
  while( lib_rtl_queue_success(sc) ) {
    for(cnt=0; cnt<len; ++cnt) ids[cnt]=0;
    for(sc=::remqhi(bmq,&q),cnt=0; cnt<len && lib_rtl_queue_success(sc); sc=::remqhi(bmq,&q)) {
      bm = (BMID)q;
      if ( !(bm == 0 || bm == MBM_INV_DESC) ) {      
        ids[cnt++] = bm;
      }
    }
    for(int i=0; i<cnt; ++i)  {
      bm = ids[i];
      if ( bm->connection.server.request>0 ) {    // Send EXCLUDE message to server 
        msg.user = bm->user;                      // if this is still possible.
        msg.data.exclude.pid = bm->pid;
        bm->communication.send_request_server(msg,false); // Otherwise it could also not become worse.
      }
    }
    ::lib_rtl_sleep(200);
    for(int i=0; i<cnt; ++i)  { // Now close communication channels and
      bm = ids[i];              // unmape the global sections if owned.
      bm->communication.close_server();
      if ( bm->buff_add && bm->own_buffer )   {
        ::lib_rtl_unmap_section(bm->buff_add);
      }
      bm->buff_add = 0;
      ::lib_rtl_output(LIB_RTL_INFO,
		       "[INFO] mbm client %s CRASHED - Shutdown of buffer %s [pid:%d, part:%d]\n",
		       bm->name, bm->bm_name, bm->pid, bm->partID);
    }
  }
  desc_head = 0;
  return MBM_NORMAL;
}

int mbm_clear(BMID bm)  {
  MBM_CHECK_CONS(bm);
  bm->cancelled = false;
  return bm->communication.clear_server();
}

int mbm_unmap_memory(BMID bm)  {
  std::unique_ptr<BMDESCRIPT> mbm{bm};
  if ( mbm.get() && bm->buffer_add )  {
    ::lib_rtl_unmap_section(bm->buff_add);
    bm->buffer_add = 0;
  }
  return MBM_ERROR;
}

/// Map monitoring shared memory section
BufferMemory* mbm_map_mon_memory(const char* bm_name) {
  auto bmid = std::make_unique<BufferMemory>();
  if ( mbmsrv_map_mon_memory(bm_name,bmid.get()) == MBM_NORMAL ) {
    return bmid.release();
  }
  return MBM_INV_MEMORY;
}

/// Map monitoring shared memory section
BufferMemory* mbm_map_mon_memory_quiet(const char* bm_name) {
  auto bmid = std::make_unique<BufferMemory>();
  if ( mbmsrv_map_mon_memory_quiet(bm_name,bmid.get()) == MBM_NORMAL ) {
    return bmid.release();
  }
  return MBM_INV_MEMORY;
}

/// Unmap monitoring shared memory section
int mbm_unmap_mon_memory(BufferMemory* bmid) {
  if ( bmid && bmid != MBM_INV_MEMORY ) {
    std::unique_ptr<BufferMemory> ptr{bmid};
    return mbmsrv_unmap_memory(ptr.get());
  }
  return MBM_ERROR;
}

/// Access control table in monitoring section
CONTROL* mbm_get_control_table(BufferMemory* bmid) {
  if ( bmid && bmid != MBM_INV_MEMORY ) {
    return bmid->ctrl;
  }
  return nullptr;
}

/// Access user table in monitoring section
USER* mbm_get_user_table(BufferMemory* bmid) {
  if ( bmid && bmid != MBM_INV_MEMORY ) {
    return bmid->user;
  }
  return nullptr;
}

/// Access event table in monitoring section
EVENT* mbm_get_event_table(BufferMemory* bmid) {
  if ( bmid && bmid != MBM_INV_MEMORY ) {
    return bmid->event;
  }
  return nullptr;
}

/// Access buffer name from BMID. Returns NULL on error/invalid BMID
const char* mbm_buffer_name(BMID bmid)   {
  if ( bmid && bmid != MBM_INV_DESC ) {
    return bmid->bm_name;
  }
  return nullptr;
}

/// Map global buffer information on this machine
int mbm_map_global_buffer_info(lib_rtl_gbl_t* handle, bool create)  {
  return mbmsrv_map_global_buffer_info(handle,create);
}

/// Unmap global buffer information on this machine
int mbm_unmap_global_buffer_info(lib_rtl_gbl_t handle, bool remove) {
  return mbmsrv_unmap_global_buffer_info(handle,remove);
}

static int mbm_setup_communication(BMID bm, int com)   {
  int status;
  bm->communication.bm = bm;
  auto MBM_COMMUNICATION = ::getenv("MBM_COMMUNICATION");
  string mbm_comm = MBM_COMMUNICATION ? MBM_COMMUNICATION : "";
  if ( 0 == MBM_COMMUNICATION )                            ;
  else if ( string::npos != mbm_comm.find("BM_COM_SHM1") ) com = BM_COM_SHM1;
  else if ( string::npos != mbm_comm.find("BM_COM_SHM2") ) com = BM_COM_SHM2;
  else if ( string::npos != mbm_comm.find("BM_COM_ASIO") ) com = BM_COM_ASIO;
  else if ( string::npos != mbm_comm.find("BM_COM_UNIX") ) com = BM_COM_UNIX;
  else if ( string::npos != mbm_comm.find("BM_COM_FIFO") ) com = BM_COM_FIFO;

#ifdef MBM_CLIENT_STANDALONE
  if ( com == BM_COM_FIFO )  {
    ::_mbm_connections_use_fifos(bm->communication);
  }
  else   {
    ::lib_rtl_output(LIB_RTL_FATAL,
		     "++bm_client++ Invalid communication type %s "
		     " Only FIFO is allowed!", com);
    return 0;
  }
#else
  if ( com == BM_COM_SHM1 || com == BM_COM_SHM2 )  {
    char text[256];
    ::snprintf(text,sizeof(text),"bm_comm_%s",bm->bm_name);
    status = ::lib_rtl_map_section(text,0,&bm->comm_add);
    if (!lib_rtl_is_success(status))    {
      ::lib_rtl_output(LIB_RTL_OS,"++bm_client++ Error mapping SHM "
		       "communication section for MBM buffer %s\n",
		       bm->bm_name);
      return 0;
    }
    bm->comm = (SHMCOMM_gbl*)bm->comm_add->address;
    status = ::lib_rtl_create_lock2(&bm->comm->table_lock, &bm->comm_lock);
    if (!lib_rtl_is_success(status))    {
      ::lib_rtl_output(LIB_RTL_OS,"++bm_client++ Error initializing "
		       "communication lock for MBM buffer %s\n",
		       bm->bm_name);
      ::lib_rtl_unmap_section(bm->comm_add);
      return 0;
    }
    bm->connection.shm.slot = -1;
    ::lib_rtl_lock(bm->comm_lock);
    for(int i = 0; i < bm->comm->num_user; ++i)   {
      SHMCOMM& c = bm->comm->user[i];
      if ( c.pid <= 0 )   {
	c.pid = ::lib_rtl_pid();
	bm->connection.shm.slot = i;
	bm->connection.shm.response = 1;
	break;
      }
    }
    ::lib_rtl_unlock(bm->comm_lock);
    if ( com == BM_COM_SHM1 )
      ::_mbm_connections_use_shm1(bm->communication);
    else
      ::_mbm_connections_use_shm2(bm->communication);
    ::lib_rtl_output(LIB_RTL_INFO,"++bm_client++ Included in %s "
		     "communication: %s [Slot:%d]\n",
		     bm->bm_name, com == BM_COM_SHM1 
		     ? "Shared memory [unblocking semaphores]"
		     : "Shared Memory [blocking semaphores]",
		     bm->connection.shm.slot);
  }
  else if ( com == BM_COM_ASIO )
    ::_mbm_connections_use_asio(bm->communication);
  else if ( com == BM_COM_UNIX )
    ::_mbm_connections_use_unix(bm->communication);
  else
    ::_mbm_connections_use_fifos(bm->communication);
#endif

  if ( bm->communication.open_server(bm->bm_name, bm->name) != MBM_NORMAL )  {
    bm->communication.close_server();
    ::lib_rtl_unmap_section(bm->comm_add);
    return 0;
  }

  // Exchange inclusion message with server instance
  MSG msg(MSG::INCLUDE);
  MSG::include_t& inc = msg.data.include;
  ::strncpy(msg.data.include.name,bm->name,sizeof(msg.data.include.name)-1);
  inc.name[sizeof(inc.name)-1] = 0;
  inc.serverid = 0;
  inc.pid      = bm->pid;
  inc.partid   = bm->partID;
  if ( bm->communication.communicate_server(msg,0) != MBM_NORMAL ) {
    ::lib_rtl_output(LIB_RTL_OS,
		     "++bm_client++ %s: INCLUDE message to server '%s': Failed to communicate\n",
		     bm->name, bm->bm_name);
    bm->communication.close_server();
    return 0;
  }

  // Communication now OK. Let's check if the actual request failed or not:
  if ( msg.status != MBM_NORMAL ) {
    ::lib_rtl_output(LIB_RTL_OS,
		     "++bm_client++ %s: Failed to include into MBM buffer %s. Status=%d\n",
		     bm->name,bm->bm_name,msg.status);
    bm->communication.close_server();
    return 0;
  }

  // Switch to "our" server's input connection
  if ( inc.serverid != 0 )   {
    status = bm->communication.move_server(bm->bm_name,msg.user,inc.serverid);
    if ( status != MBM_NORMAL )  {
      bm->communication.close_server();
      return 0;
    }
  }
  // All fine: Store the server cookie for further requests
  bm->user      = msg.user;
  bm->comm_type = com;
  return 1;
}

/// Include as client in buffer manager
BMID mbm_include(const char* bm_name, const char* name, int partid, int com, int flags) {
  int status;
  char text[256];
  auto bm = std::make_unique<BMDESCRIPT>();
  ::strncpy(bm->bm_name,bm_name,sizeof(bm->bm_name)-1);
  bm->bm_name[sizeof(bm->bm_name)-1] = 0;
  ::strncpy(bm->name,name,sizeof(bm->name)-1);
  bm->name[sizeof(bm->name)-1] = 0;
  bm->pid    = ::lib_rtl_pid();
  bm->partID = partid;
  bm->cancelled  = false;
  bm->own_buffer = 0;
  ::strncpy(bm->connection.name,name,sizeof(bm->connection.name)-1);
  status = ::mbm_setup_communication(bm.get(), com);
  if ( 0 == status )   {
    return MBM_INV_DESC;
  }

  int map_flags = LIB_RTL_GBL_SHARED;
  if ( flags&BM_INC_READ  ) map_flags |= LIB_RTL_GBL_READ;
  if ( flags&BM_INC_WRITE ) map_flags |= LIB_RTL_GBL_WRITE;

  ::snprintf(text,sizeof(text),"bm_buff_%s",bm_name);  
  status  = ::lib_rtl_map_section(text, 0, &bm->buff_add, map_flags);
  if (!lib_rtl_is_success(status))  {
    ::lib_rtl_output(LIB_RTL_OS,
		     "++bm_client++ %s: Error mapping buffer section for MBM buffer %s. Status=%d\n",
		     bm->name,bm->bm_name,status);
    bm->communication.close_server();
    ::lib_rtl_unmap_section(bm->comm_add);
    return MBM_INV_DESC;
  }
  bm->own_buffer = 1;
  bm->buffer_add = (char*)bm->buff_add->address;

  // Now setup exit and rundown handler
  if ( reference_count == 0 )  {
    desc_head  = new qentry_t(0,0);
#ifndef MBM_CLIENT_STANDALONE
    ::lib_rtl_declare_exit(_mbm_shutdown, 0);
    ::lib_rtl_declare_rundown(_mbm_shutdown,0);
#endif
  }
  ::insqhi(bm.get(), desc_head);
  reference_count++;
  return bm.release();
}

/// Include as client in buffer manager reusing the masters global section
BMID mbm_connect(BMID master, const char* name, int partid) {
  int status;
  auto bm = std::make_unique<BMDESCRIPT>();
  ::strncpy(bm->bm_name,master->bm_name,sizeof(bm->bm_name)-1);
  bm->bm_name[sizeof(bm->bm_name)-1] = 0;
  ::strncpy(bm->name,name,sizeof(bm->name)-1);
  bm->name[sizeof(bm->name)-1] = 0;
  bm->pid        = ::lib_rtl_pid();
  bm->partID     = partid;
  bm->cancelled  = false;
  bm->own_buffer = 0;
  ::strncpy(bm->connection.name,name,sizeof(bm->connection.name)-1);

  status = ::mbm_setup_communication(bm.get(), master->comm_type);
  if ( 0 == status )   {
    return MBM_INV_DESC;
  }
  bm->buff_add   = master->buff_add;
  bm->buffer_add = (char*)master->buff_add->address;

  // Now setup exit and rundown handler
  if ( reference_count == 0 )  {
    desc_head  = new qentry_t(0,0);
#ifndef MBM_CLIENT_STANDALONE
    ::lib_rtl_declare_exit(_mbm_shutdown, 0);
    ::lib_rtl_declare_rundown(_mbm_shutdown,0);
#endif
  }
  ::insqhi(bm.get(), desc_head);
  reference_count++;
  return bm.release();
}

/// Include as client in buffer manager in read-only mode
BMID mbm_include_read (const char* bm_name, const char* name, int partid, int com_type)   {
  return ::mbm_include(bm_name, name, partid, com_type, BM_INC_READ);
}

/// Include as client in buffer manager in write-only mode
BMID mbm_include_write (const char* bm_name, const char* name, int partid, int com_type)   {
  return ::mbm_include(bm_name, name, partid, com_type, BM_INC_WRITE);
}

/// Exclude client from buffer manager
int mbm_exclude (BMID bm)  {
  MBM_CHECK_CONS(bm);
  MSG msg(MSG::EXCLUDE,bm->user);
  MSG::exclude_t& exclude = msg.data.exclude;
  exclude.pid = bm->pid;
  if ( bm->communication.communicate_server(msg,0) != MBM_NORMAL ) {
    ::_mbm_printf("++bm_client++ communication problem while unlinking the connection");
  }
  if ( bm->communication.close_server() != MBM_NORMAL ) {
    ::_mbm_printf("++bm_client++ Problem while closing the connections");
  }
#ifndef MBM_CLIENT_STANDALONE
  if ( bm->comm_lock && bm->connection.shm.slot >= 0 )  {
    SHMCOMM& c = bm->comm->user[bm->connection.shm.slot];
    c.pid = 0;
    ::lib_rtl_delete_lock(bm->comm_lock);
    ::lib_rtl_unmap_section(bm->comm_add);
    bm->connection.shm.slot = -1;
    bm->connection.shm.response = 0;
  }
#endif
  if ( bm->own_buffer )   {
    ::lib_rtl_unmap_section(bm->buff_add);
  }
  qentry_t *entry = ::remqent(bm);
  if ( entry ) delete entry;
  reference_count--;
  if (reference_count == 0)  {
#ifndef MBM_CLIENT_STANDALONE
    ::lib_rtl_remove_rundown(_mbm_shutdown,0);
    ::lib_rtl_remove_exit(_mbm_shutdown,0);
#endif
    delete desc_head;
    desc_head = 0;
  }
  return MBM_NORMAL;
}

int mbm_cancel_request (BMID bm)   {
  MBM_CHECK_BMID(bm);
  MSG msg(MSG::CANCEL_REQUEST,bm->user);
  bm->cancelled = true;
  return bm->communication.send_request_server(msg,false);
}

int mbm_set_cancelled (BMID bm, int value)    {
  MBM_CHECK_BMID(bm);
  bm->cancelled = (value != 0);
  return MBM_NORMAL;
}

int mbm_is_cancelled (BMID bm)    {
  MBM_CHECK_BMID(bm);
  return bm->cancelled;
}

// Consumer routines
int mbm_add_req (BMID bm, int evtype, 
                 const unsigned int* trg_mask, 
                 const unsigned int* veto_mask, 
                 int masktype, int usertype, int freqmode, float freq)
{
  MBM_CHECK_CONS(bm);
  MSG msg(MSG::ADD_REQUEST,bm->user);
  MSG::requirement_t& r = msg.data.requirement;
  LOCK lck (bm->lock);
  r.evtype    = evtype;
  r.masktype  = masktype;
  r.freqmode  = freqmode;
  r.usertype  = usertype;
  r.frequency = freq;
  ::memcpy(r.mask,trg_mask,sizeof(r.mask));
  ::memcpy(r.veto,veto_mask,sizeof(r.veto));
  int status = bm->communication.communicate_server(msg, &bm->cancelled);
  if ( MBM_REQ_CANCEL == status ) {
    //bm->cancelled = false;
    //return MBM_NORMAL;
  }
  return status;
}

int mbm_del_req (BMID bm, int evtype, 
                 const unsigned int* trmask,
                 const unsigned int* veto, 
                 int masktype, int usertype)
{
  MBM_CHECK_CONS(bm);
  MSG msg(MSG::DEL_REQUEST,bm->user);
  MSG::requirement_t& r = msg.data.requirement;
  r.evtype    = evtype;
  r.masktype  = masktype;
  r.usertype  = usertype;
  r.freqmode  = 0;
  r.frequency = 0;
  ::memcpy(r.mask,trmask,sizeof(r.mask));
  ::memcpy(r.veto,veto,sizeof(r.veto));
  LOCK lck (bm->lock);
  int status = bm->communication.communicate_server(msg, &bm->cancelled);
  if ( MBM_REQ_CANCEL == status ) {
    //bm->cancelled = false;
    //return MBM_NORMAL;
  }
  return status;
}

int mbm_get_event_a (BMID bm, int** ptr, long* size, int* evtype, unsigned int* trmask,
                     int /* part_id */, RTL_ast_t astadd, void* astpar) {
  MBM_CHECK_CONS(bm);
  MSG msg(MSG::GET_EVENT,bm->user);
  bm->evt_ptr   = ptr;
  bm->evt_size  = size;
  bm->evt_type  = evtype;
  bm->trmask    = trmask;
  bm->ast_addr  = astadd;
  bm->ast_param = astpar;
  ::memset(trmask,0x0,sizeof(TriggerMask));
  return bm->communication.send_request_server(msg,true);
}

int mbm_wait_event (BMID bm) {
  MBM_CHECK_CONS(bm);
  MSG msg(0);
  LOCK lck (bm->lock);
  if ( bm->cancelled )    {
    *bm->evt_ptr  = 0;
    *bm->evt_size = 0;
    *bm->evt_type = 0;
    return MBM_REQ_CANCEL;
  }
  int status = bm->communication.read_response_server(msg, &bm->cancelled);
  if ( MBM_NORMAL == status ) {
    MSG::get_event_t& evt = msg.data.get_event;
    char* ptr = bm->buffer_add + evt.offset;
    *bm->evt_ptr  = (int*)ptr;
    *bm->evt_size = evt.size;
    *bm->evt_type = evt.type;
    ::memcpy(bm->trmask,evt.trmask,sizeof(evt.trmask));
    if ( bm->ast_addr != 0 ) {
      ::lib_rtl_run_ast(bm->ast_addr, bm->ast_param, 3);
    }
    bm->ast_addr = 0;
    bm->ast_param = 0;
  }
  else if ( MBM_REQ_CANCEL == status ) {
    *bm->evt_ptr  = 0;
    *bm->evt_size = 0;
    *bm->evt_type = 0;
  }
  else if ( MBM_NO_EVENT == status ) {
    *bm->evt_ptr  = 0;
    *bm->evt_size = 0;
    *bm->evt_type = 0;
  }
  return status;
}

int mbm_wait_event_a (BMID bm)    {
  return ::_mbm_cons_message(bm,MSG::WAIT_EVENT);
}

int mbm_get_event (BMID bm, int** ptr, long* size, int* evtype, unsigned int* trmask, int part_id) {
  int sc = ::mbm_get_event_a(bm,ptr,size,evtype,trmask,part_id,NULL,bm);
  if ( sc == MBM_NORMAL || sc == MBM_NO_EVENT ) {
    return ::mbm_wait_event(bm);
  }
  return sc;
}

int mbm_get_event_try (BMID bm, int** ptr, long* size, int* evtype, unsigned int* trmask,
		       int /* part_id */, RTL_ast_t astadd, void* astpar) {
  MBM_CHECK_CONS(bm);
  MSG msg(MSG::GET_EVENT_TRY,bm->user);
  bm->evt_ptr   = ptr;
  bm->evt_size  = size;
  bm->evt_type  = evtype;
  bm->trmask    = trmask;
  bm->ast_addr  = astadd;
  bm->ast_param = astpar;
  ::memset(trmask,0x0,sizeof(TriggerMask));
  int sc = bm->communication.send_request_server(msg,true);
  if ( sc == MBM_NORMAL ) {
    return ::mbm_wait_event(bm);
  }
  return MBM_ERROR;
}


const char* mbm_buffer_address(BMID bm) {
  if ( bm != MBM_INV_DESC && bm->buff_add ) {
    return bm->buffer_add;
  }
  return 0;
}

int mbm_buffer_size(BMID bm, size_t* size) {
  if ( bm != MBM_INV_DESC && bm->buff_add && size )   {
    *size = bm->buff_add->size;
    return MBM_NORMAL;
  }
  return MBM_ERROR;
}

int mbm_buffer_memory(BMID bm, const char** addr, size_t* size) {
  if ( bm != MBM_INV_DESC && bm->buff_add && size && addr )   {
    *addr = bm->buffer_add;
    *size = bm->buff_add->size;
    return MBM_NORMAL;
  }
  return MBM_ERROR;
}

int mbm_stop_consumer(BMID bm)   {
  return ::_mbm_comm_message(bm, MSG::STOP_CONSUMER);
}

int mbm_free_event (BMID bm) {
  return ::_mbm_cons_message(bm,MSG::FREE_EVENT);
}

int mbm_pause (BMID bm)  {
  return ::_mbm_cons_message(bm,MSG::PAUSE);
}

/*
 * Producer Routines
 */
int mbm_get_space_try(BMID bm, long size, int** ptr, RTL_ast_t astadd, void* astpar)  {
  MBM_CHECK_BMID(bm);
  MSG msg(MSG::GET_SPACE_TRY,bm->user);
  MSG::get_space_t& sp = msg.data.get_space;
  sp.size = size;
  {
    LOCK lck (bm->lock);
    if ( bm->cancelled )    {
      *bm->evt_ptr  = 0;
      bm->ast_addr  = nullptr;
      bm->ast_param = nullptr;
      return MBM_REQ_CANCEL;
    }
  }
  int status = bm->communication.communicate_server(msg, &bm->cancelled);
  bm->evt_ptr = ptr;
  if ( status == MBM_NORMAL ) {
    bm->ast_addr  = astadd;
    bm->ast_param = astpar;
    *bm->evt_ptr  = (int*)(bm->buffer_add + sp.offset);
    if ( bm->ast_addr )   {
      ::lib_rtl_run_ast(bm->ast_addr, bm->ast_param, 3);
    }
    bm->ast_addr  = 0;
    bm->ast_param = 0;
  }
  else if ( MBM_NO_ROOM == status )  {
    *bm->evt_ptr  = 0;
    bm->ast_addr  = nullptr;
    bm->ast_param = nullptr;
    //bm->cancelled = false;
  }
  else if ( MBM_REQ_CANCEL == status )  {
    *bm->evt_ptr  = 0;
    bm->ast_addr  = nullptr;
    bm->ast_param = nullptr;
    //bm->cancelled = false;
  }
  else if ( MBM_ILL_LEN == status )  {
    *bm->evt_ptr  = 0;
    bm->ast_addr  = nullptr;
    bm->ast_param = nullptr;
    //bm->cancelled = false;
  }
  else {
    ::lib_rtl_output(LIB_RTL_FATAL,"MBM Error: Got message with bad status:%d.\n",msg.status);
  } 
  return status;
}

int mbm_get_space_a (BMID bm, long size, int** ptr, RTL_ast_t astadd, void* astpar)  {
  MBM_CHECK_BMID(bm);
  MSG msg(MSG::GET_SPACE,bm->user);
  MSG::get_space_t& sp = msg.data.get_space;
  bm->evt_ptr   = ptr;
  bm->ast_addr  = astadd;
  bm->ast_param = astpar;
  sp.size       = size;
  {
    LOCK lck (bm->lock);
    if ( bm->cancelled )    {
      return MBM_REQ_CANCEL;
    }
  }
  return bm->communication.send_request_server(msg, true);
}

int mbm_wait_space(BMID bm)    {
  MBM_CHECK_BMID(bm);
  MSG msg(MBM_ERROR,bm->user);
  {
    LOCK lck (bm->lock);
    if ( bm->cancelled )    {
      *bm->evt_ptr  = 0;
      return MBM_REQ_CANCEL;
    }
  }
  int status = bm->communication.read_response_server(msg, &bm->cancelled);
  if ( status == MBM_NORMAL )  {
    if ( msg.type != MSG::GET_SPACE ) {
      ::lib_rtl_output(LIB_RTL_FATAL,
		       "MBM Error: Got message of type:%s instead of expected GET_SPACE [%d]\n",
                       msg.typeStr(msg.type), bm->connection.shm.slot);
    }
    MSG::get_space_t& sp = msg.data.get_space;
    char* ptr = bm->buffer_add + sp.offset;
    *bm->evt_ptr  = (int*)ptr;
    ::lib_rtl_run_ast(bm->ast_addr, bm->ast_param, 3);
    bm->ast_addr  = 0;
    bm->ast_param = 0;
  }
  else if ( MBM_REQ_CANCEL == status )  {
    *bm->evt_ptr  = 0;
    //bm->cancelled = false;
  }
  else if ( MBM_ILL_LEN == status )  {
    *bm->evt_ptr  = 0;
    //bm->cancelled = false;
  }
  else if ( MBM_NO_ROOM == status )  {
    *bm->evt_ptr  = 0;
    //bm->cancelled = false;
  }
  else {
    ::lib_rtl_output(LIB_RTL_FATAL,"MBM Error: Got message with bad status:%d.\n",msg.status);
  }
  return status;
}

static int _mbm_declare_event (BMID bm, long len, int evtype, const unsigned int* trmask,
                               const char* dst, void** free_add, long* free_size, int /* part_id */, int wait)
{
  MBM_CHECK_BMID(bm);
  MSG msg(MSG::DECLARE_EVENT,bm->user);
  MSG::declare_event_t& evt = msg.data.declare_event;
  evt.size = len;
  evt.wait = wait;
  evt.type = evtype;
  ::strncpy(evt.dest,dst ? dst : "", sizeof(evt.dest)-1);
  ::memcpy(evt.trmask,trmask,sizeof(evt.trmask));
  *free_add = 0;
  *free_size = 0;
  int status = bm->communication.send_request_server(msg, false);
  if ( status == MBM_NORMAL )   {
    if ( bm->cancelled )  {
      *free_add  = 0;
      *free_size = 0;
      //bm->cancelled = false;
      return MBM_REQ_CANCEL;
    }
    status = bm->communication.read_response_server(msg, &bm->cancelled);
    if ( status == MBM_NORMAL ) {
      *free_add  = (bm->buffer_add + evt.freeAddr);
      *free_size = evt.freeSize;
      if ( msg.type != MSG::DECLARE_EVENT ) {
        ::lib_rtl_output(LIB_RTL_FATAL,
			 "MBM: Got message of type:%s instead of expected DECLARE_EVENT [%d]\n",
                         msg.typeStr(msg.type), bm->connection.shm.slot);
      }
    }
    else if ( MBM_NO_ROOM == status )  {
      *free_add  = 0;
      *free_size = 0;
      //bm->cancelled = false;
    }
    else if ( MBM_REQ_CANCEL == status )  {
      *free_add  = 0;
      *free_size = 0;
      //bm->cancelled = false;
    }
    else {
      ::lib_rtl_output(LIB_RTL_FATAL,
		       "MBM: _mbm_declare_event got message with bad status:%d [%d].\n",
		       msg.status, status);
    }
  }
  return status;
}

int mbm_declare_event(BMID bm, long len, int evtype, const unsigned int* trmask,
                      const char* dst, void** free_add, long* free_size, int part_id)
{
  return ::_mbm_declare_event(bm,len,evtype,trmask,dst,free_add,free_size,part_id,1);
}

int mbm_declare_event_try(BMID bm, long len, int evtype, const unsigned int* trmask,
                          const char* dst, void** free_add, long* free_size, int part_id)
{
  return ::_mbm_declare_event(bm,len,evtype,trmask,dst,free_add,free_size,part_id,0);
}

int mbm_declare_event_and_send(BMID bm, long len, int evtype, const unsigned int* trmask,
                               const char* dst, void** free_add, long* free_size, int part_id)
{
  return ::_mbm_declare_event(bm,len,evtype,trmask,dst,free_add,free_size,part_id,-1);
#if 0
  int sc = ::_mbm_declare_event(bm,len,evtype,trmask,dst,free_add,free_size,part_id,1);
  if( MBM_NORMAL == sc )  {
    sc = mbm_send_space(bm);
    if (lib_rtl_is_success(sc))      {
      *free_add  = 0;
      *free_size = 0;
    }
  }
  return sc;
#endif
}

int mbm_free_space (BMID bm)   {
  return ::_mbm_comm_message(bm, MSG::FREE_SPACE);
}

int mbm_send_space (BMID bm)    {
  MBM_CHECK_BMID(bm);
  MSG msg(MSG::SEND_SPACE,bm->user);
  int sc = bm->communication.communicate_server(msg,&bm->cancelled);
  if ( MBM_REQ_CANCEL == sc )  {
    // bm->cancelled = false;
  }
  else if ( msg.type != MSG::SEND_SPACE ) {
    ::lib_rtl_output(LIB_RTL_FATAL,
		     "MBM: Got message of type:%s instead of expected SEND_SPACE [%d]\n",
                     msg.typeStr(msg.type), bm->connection.shm.slot);
  }
  return sc;
}

int mbm_wait_space_a(BMID bm)    {
  return ::_mbm_comm_message(bm, MSG::WAIT_SPACE_A);
}

// Statistics routines
int mbm_events_actual (BMID bm, long *events)   {
  MBM_CHECK_BMID(bm);
  MSG msg(MSG::STAT_EVENTS_ACTUAL,bm->user);
  if ( bm->communication.communicate_server(msg,0) == MBM_NORMAL ) {
    *events = msg.data.statistics;
    return MBM_NORMAL;
  }
  return MBM_ERROR;
}

int mbm_events_produced (BMID bm, long *events)   {
  MBM_CHECK_BMID(bm);
  MSG msg(MSG::STAT_EVENTS_PRODUCED,bm->user);
  if ( bm->communication.communicate_server(msg,0) == MBM_NORMAL ) {
    *events = msg.data.statistics;
    return MBM_NORMAL;
  }
  return MBM_ERROR;
}

int mbm_events_seen (BMID bm, long *events)   {
  MBM_CHECK_BMID(bm);
  MSG msg(MSG::STAT_EVENTS_SEEN,bm->user);
  if ( bm->communication.communicate_server(msg,0) == MBM_NORMAL ) {
    *events = msg.data.statistics;
    return MBM_NORMAL;
  }
  return MBM_ERROR;
}

int mbm_stop_producer(BMID bm)   {
  return ::_mbm_comm_message(bm, MSG::STOP_PRODUCER);
}

int mbm_reset_statistics (BMID bm)   {
  return ::_mbm_comm_message(bm, MSG::STAT_RESET);
}

int mbm_grant_update (BMID bm)   {
  return ::_mbm_comm_message(bm, MSG::GRANT_UPDATE);
}

int mbm_min_alloc(BMID bm, long* size) {
  MBM_CHECK_BMID(bm);
  MSG msg(MSG::STAT_MIN_ALLOC,bm->user);
  if( bm->communication.communicate_server(msg,0) == MBM_NORMAL ) {
    *size = msg.data.statistics;
    return MBM_NORMAL;
  }
  return MBM_ERROR;
}

int mbm_events_in_buffer (BMID bm, int* events)  {
  MBM_CHECK_BMID(bm);
  MSG msg(MSG::STAT_EVENTS_IN_BUFFER,bm->user);
  if( bm->communication.communicate_server(msg,0) == MBM_NORMAL ) {
    *events = msg.data.statistics;
    return MBM_NORMAL;
  }
  return MBM_ERROR;
}

int mbm_space_in_buffer (BMID bm, long* total, long* large)  {
  MBM_CHECK_BMID(bm);
  MSG msg(MSG::STAT_SPACE_IN_BUFFER,bm->user);
  if( bm->communication.communicate_server(msg,0) == MBM_NORMAL ) {
    *total = msg.data.space_in_buffer.total;
    *large = msg.data.space_in_buffer.large;
    return MBM_NORMAL;
  }
  return MBM_ERROR;
}

int mbm_process_exists(BMID bm, const char* name, int* exists)  {
  MBM_CHECK_BMID(bm);
  MSG msg(MSG::PROCESS_EXISTS,bm->user);
  ::strncpy(msg.data.process_exists.name,name,sizeof(msg.data.process_exists.name)-1);
  msg.data.process_exists.name[sizeof(msg.data.process_exists.name)-1] = 0;
  if( bm->communication.communicate_server(msg,0) == MBM_NORMAL ) {
    *exists = msg.data.statistics;
    return MBM_NORMAL;
  }
  return MBM_ERROR;
}
