#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================

"""
     Fifo log initializer
     Required ENVIRONMENT:

     PARTITION_NAME or SYSTAG ===> Fills sys_tag field on log message
     PROCESS_NAME   or UTGID  ===> utgid field in log message
     LOGFIFO                  ===> Path to the fifo to write the log messages to

     \author   M.Frank
     \version  1.0
"""
from __future__ import print_function
from builtins import object

logger = None

class _init_logs(object):
  def __init__(self):
    import fifo_log._fifo_log
    self.fifo = fifo_log._fifo_log

  def stop(self):
    self.fifo.logger_stop();
    print('Stopped fifo logger output.')
    return self

  def start(self):
    self.fifo.logger_start();
    return self
    
  def set_tag(self, tag):
    self.fifo.logger_set_tag(tag)
    return self
    
  def set_utgid(self, utgid):
    self.fifo.logger_set_utgid(utgid)
    return self

logger = _init_logs()
from ._fifo_log import *
