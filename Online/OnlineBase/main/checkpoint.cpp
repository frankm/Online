//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#define main checkpointing_main
#include "gentest.cpp"
#undef main
extern void CheckpointRestoreWrapper__init_instance(int argc, char** argv);

int main (int argc, char** argv)  {
  CheckpointRestoreWrapper__init_instance(argc, argv);
  return checkpointing_main(argc,argv);
}
