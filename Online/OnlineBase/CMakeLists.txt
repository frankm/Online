#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/OnlineBase
-----------------
#]=======================================================================]
#
online_library(OnlineBase
        src/AMS/ams_bounce.cpp
        src/AMS/ams_transfer.cpp
        src/AMS/amslib.cpp
        src/AMS/amsu.cpp
        src/ASIO/NameServer.cpp
        src/ASIO/Socket.cpp
        src/ASIO/TanInterface.cpp
        src/ASIO/TcpConnection.cpp
        src/ASIO/TcpRequestHandler.cpp
        src/ASIO/TcpServer.cpp
        src/ASIO/Transfer.cpp
        src/ASIO/UnixConnection.cpp
        src/ASIO/UnixRequestHandler.cpp
        src/ASIO/UnixServer.cpp
        src/ASIO/UnixTransfer.cpp
        src/CPP/AmsSensor.cpp
        src/CPP/AsciiDisplay.cpp
        src/CPP/CheckpointRestoreWrapper.cpp
        src/CPP/FSM.cpp
        src/CPP/FormatCnv.cpp
        src/CPP/IocLogDevice.cpp
        src/CPP/IocSensor.cpp
        src/CPP/Sensor.cpp
        src/CPP/Table.cpp
        src/CPP/TimeSensor.cpp
        src/MBM/BufferInfo.cpp
        src/MBM/DumpBits.cpp
        src/MBM/Dumper.cpp
        src/MBM/Installer.cpp
        src/MBM/Manager.cpp
        src/MBM/mbm_extract.cpp
        src/MBM/mbmlib_asio.cpp
        src/MBM/mbmlib_asio_unix.cpp
        src/MBM/mbmlib_client.cpp
        src/MBM/mbmlib_comm_shm.cpp
        src/MBM/mbmlib_fifo.cpp
	src/MBM/mbmlib_memory.cpp
        src/MBM/mbmlib_server.cpp
        src/MBM/mbmlib_msg.cpp
        src/MBM/mbmmon.cpp
        src/MBM/mbmmon_scr.cpp
	src/MBM/Monitor.cpp
        src/MBM/RemoteMon.cpp
        src/MBM/Requirement.cpp
        src/MBM/Summary.cpp
        src/NET/DataTransfer.cpp
        src/NET/IOPortManager.cpp
        src/NET/NetworkChannel.cpp
        src/NET/TcpConnection.cpp
        src/NET/TcpNetworkChannel.cpp
        src/NET/UdpConnection.cpp
        src/NET/UdpNetworkChannel.cpp
        src/PUBAREA/PA.cpp
        src/PUBAREA/PubArea.cpp
        src/PUBAREA/PubArea_AllocateSlot.cpp
        src/PUBAREA/PubArea_Clean.cpp
        src/PUBAREA/PubArea_CleanProcessArea.cpp
        src/PUBAREA/PubArea_Create.cpp
        src/PUBAREA/PubArea_Delete.cpp
        src/PUBAREA/PubArea_Display.cpp
        src/PUBAREA/PubArea_Dump.cpp
        src/PUBAREA/PubArea_FormatSlot.cpp
        src/PUBAREA/PubArea_FreeSlot.cpp
        src/PUBAREA/PubArea_GetSlotOfType.cpp
        src/PUBAREA/PubArea_Lock.cpp
        src/PUBAREA/PubArea_Unlock.cpp
        src/RTL/bits.cpp
        src/RTL/bits2.cpp
        src/RTL/conioex.cpp
	src/RTL/ConsoleDisplay.cpp
        src/RTL/FmcLogDevice.cpp
        src/RTL/GlobalSection.cpp
        src/RTL/Process.cpp
        src/RTL/ProcessGroup.cpp
        src/RTL/TimerManager.cpp
        src/RTL/graphics.cpp
	src/RTL/Logger.cpp
        src/RTL/que.cpp
        src/RTL/raiseDebug.cpp
        src/RTL/rtl.cpp
        src/RTL/rtl_CLI.cpp
        src/RTL/rtl_events.cpp
        src/RTL/rtl_globsec.cpp
        src/RTL/rtl_locks.cpp
        src/RTL/rtl_memory.cpp
        src/RTL/rtl_process.cpp
        src/RTL/rtl_qio.cpp
        src/RTL/rtl_strdef.cpp
        src/RTL/rtl_sys.cpp
        src/RTL/rtl_systime.cpp
        src/RTL/rtl_threads.cpp
        src/RTL/rtl_time.cpp
        src/RTL/rtl_timers.cpp
	src/RTL/rtl_utils.cpp
	src/SCR/MouseSensor.cpp
        src/SCR/ScrDisplay.cpp
        src/SCR/listr.cpp
        src/SCR/lists.cpp
        src/SCR/scr.cpp
        src/SCR/scr_ansi.cpp
        src/SCR/scr_recorder.cpp
        src/SCR/scr_window.cpp
        src/SCR/scrcc.cpp
        src/TAN/NameServer.cpp
        src/TAN/TanDB.cpp
        src/TAN/TanDB_Dump.cpp
        src/TAN/TanInterface.cpp
        src/TAN/TanMon.cpp
        src/WT/wtlib.cpp)
if(TARGET PkgConfig::numa)
    target_compile_definitions(OnlineBase PRIVATE ONLINE_HAVE_NUMA)
    target_link_libraries(OnlineBase PRIVATE PkgConfig::numa)
endif()
target_compile_options(OnlineBase PRIVATE -Wno-format -Wno-format-security)
if(NOT CMAKE_CXX_COMPILER_ID STREQUAL  "Clang")
    target_compile_options(OnlineBase PRIVATE -Wno-stringop-truncation -Wno-class-memaccess)
endif()
target_link_libraries(OnlineBase PUBLIC Boost::headers Threads::Threads ${CMAKE_DL_LIBS} -lrt)
#
#===============================================================================
online_library(MBM
       src/RTL/que.cpp
       src/RTL/rtl_time.cpp
       src/RTL/rtl_memory.cpp
       src/RTL/rtl_utils.cpp
       src/RTL/rtl_globsec.cpp
       src/MBM/mbmlib_client.cpp
       src/MBM/mbmlib_memory.cpp
       src/MBM/mbmlib_msg.cpp
       src/MBM/mbmlib_fifo.cpp
       src/MBM/Requirement.cpp)
if(TARGET PkgConfig::numa)
    target_compile_definitions(MBM PRIVATE ONLINE_HAVE_NUMA)
    target_link_libraries(MBM PRIVATE PkgConfig::numa)
endif()
target_compile_definitions(MBM PRIVATE -DMBM_CLIENT_STANDALONE)
target_link_libraries(MBM PRIVATE Boost::headers Threads::Threads -lrt)
#
#===============================================================================
online_library(FifoLog
    src/LOG/FifoLog_preload.cpp
    src/LOG/FifoLog.cpp
)
target_link_libraries(FifoLog Threads::Threads ${CMAKE_DL_LIBS} -lrt)
#
online_python_module(_fifo_log fifo_log src/LOG/FifoLog_module.cpp)
#
foreach(name IN ITEMS gentest genRunner genPython mbm_remove checkpoint onlinekernel_test)
    online_executable(${name}.exe main/${name}.cpp)
    target_link_libraries(${name}.exe PRIVATE Threads::Threads ${CMAKE_DL_LIBS} -lrt)

    online_executable(${name} main/${name}.cpp)
    target_link_libraries(${name} PRIVATE Threads::Threads ${CMAKE_DL_LIBS} -lrt)
endforeach()
#
target_link_libraries(genPython      PRIVATE Python::Python)
target_link_libraries(genPython.exe  PRIVATE Python::Python)
target_link_libraries(checkpoint     PRIVATE Online::OnlineBase)
target_link_libraries(checkpoint.exe PRIVATE Online::OnlineBase)
#
#===============================================================================
online_install_includes(include)
online_install_python(python)
