//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//  ========================================================================
//
//  Author    : Markus Frank
//
//  ========================================================================

/// Framework include files
#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiOnline/IRawEventCreator.h>
#include <Event/RawEvent.h>

// C/C++ include files
#include <string>
#include <memory>

namespace Online  {

  class RawEventRegister :  virtual public IRawEventCreator  {
  protected:
    IDataProviderSvc* eventSvc  {nullptr};
    typedef LHCb::RawBank  bank_t;
    typedef LHCb::RawEvent event_t;

    StatusCode put(const std::string& location, std::unique_ptr<event_t>&& raw)   {
      return this->eventSvc->registerObject(location, raw.release());
    }

  public:
    RawEventRegister(IDataProviderSvc* svc) : eventSvc(svc) {}
    virtual ~RawEventRegister() = default;

    virtual StatusCode put(const std::string& location, const std::vector<void*>& banks)  override final  {
      auto raw = std::make_unique<event_t>();
      for( const auto* b : banks )   {
	const bank_t* bank = (const bank_t*)b;
	raw->adoptBank(bank, false);
      }
      return put(location, std::move(raw));
    }

    virtual StatusCode put(const std::string& location, const unsigned char* start, const unsigned char* end)  override final  {
      auto raw = std::make_unique<event_t>();
      while(start < end)    {
	bank_t* bank = (bank_t*)start;
	size_t len = bank->totalSize();
	raw->adoptBank(bank, false);
	start += len;
	if ( 0 == len ) break;
      }
      return put(location, std::move(raw));
    }
  };
}

using namespace Online;

/// Factory instantiation
DECLARE_COMPONENT( RawEventRegister )
