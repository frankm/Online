//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <RawBankSizes/Structure.h>
#include <iostream>
#include <string>

static void test(const std::string& input)    {
  HParam::HistParams result;
  auto ret = Gaudi::Parsers::parse(result, input);
  std::cout << "Tried to parse: " << input << std::endl;
  std::cout << ret.getCode() << "  --> ";
  Gaudi::Utils::toStream(result,std::cout);
  std::cout <<  std::endl;
}

int main(int, char**)   {
  test("(50, 0.0, 1000.0, 0, 5, 'L0')");
  test("(50, 0.0, 1000.0, 0, 5, \"L0\")");
  test("(50,0.0,1000.0,0,5,\"L0\")");
  test("(50,0.0,1000.0,0,5,'L0')");
  return 0;
}
