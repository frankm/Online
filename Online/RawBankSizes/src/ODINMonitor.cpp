//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : GaudiOnline
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/IHistogramSvc.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>
#include <Tell1Data/Tell1Bank.h>
#include <Tell1Data/RunInfo.h>
#include <PCIE40Data/sodin.h>

/// Forward declarations
namespace LHCb { class RawBank; }

///  Online namespace declaration
namespace Online {

  /**@class ODINMonitor ODINMonitor.cpp
   *
   * ODIN monitoring
   *
   */
  class GAUDI_API ODINMonitor: public Gaudi::Algorithm   {

  private:
    typedef std::vector<std::pair<const LHCb::RawBank*, const void*> > lb_evt_data_t;

    AIDA::IHistogram1D* m_bcidsHisto = 0;
    AIDA::IHistogram1D* m_bxtypeHisto = 0;
    AIDA::IHistogram1D* m_trgtypeHisto = 0;
    AIDA::IHistogram2D* m_bxtypeHistovsBxid = 0;
    AIDA::IHistogram2D* m_trgtypeHistovsBxid = 0;
    DataObjectReadHandle<lb_evt_data_t> m_rawData {this,"RawData","Banks/RawData"};

  public:
    /// Standard Algorithm Constructor(s)
    using Algorithm::Algorithm;

    /// Algorithm overload: Initialize the algorithm
    StatusCode initialize() override {
      if ( !histoSvc()->findObject( "ODIN/Bcids", m_bcidsHisto).isSuccess() )  {
        m_bcidsHisto = histoSvc()->book( "ODIN/Bcids", "BCIds", 3564, -0.5, 3563.5);
        declareInfo("ODIN/Bcids", m_bcidsHisto, m_bcidsHisto->title());
      }
      if ( !histoSvc()->findObject( "ODIN/BXType", m_bxtypeHisto).isSuccess() )  {
        m_bxtypeHisto = histoSvc()->book( "ODIN/BXType", "BXType", 4, -0.5, 3.5);
        declareInfo("ODIN/BXType", m_bxtypeHisto, m_bxtypeHisto->title());
      }
      if ( !histoSvc()->findObject( "ODIN/TrgType", m_trgtypeHisto).isSuccess() )  {
        m_trgtypeHisto = histoSvc()->book( "ODIN/TrgType", "TrgType", 16, -0.5, 15.5);
        declareInfo("ODIN/TrgType", m_trgtypeHisto, m_trgtypeHisto->title());
      }
      if ( !histoSvc()->findObject( "ODIN/BXTypeVsBXID", m_bxtypeHistovsBxid).isSuccess() )  {
        m_bxtypeHistovsBxid = histoSvc()->book( "ODIN/BXTypeVsBXID", "BXTypeVsBXID", 3564, -0.5, 3563.5, 4, -0.5, 3.5);
        declareInfo("ODIN/BXTypeVsBXID", m_bxtypeHistovsBxid, m_bxtypeHistovsBxid->title());
      }
      if ( !histoSvc()->findObject( "ODIN/TrgTypeVsBXID", m_trgtypeHistovsBxid).isSuccess() )  {
        m_trgtypeHistovsBxid = histoSvc()->book( "ODIN/TrgTypeVsBXID", "TrgTypeVsBXID", 3564, -0.5, 3563.5, 16, -0.5, 15.5);
        declareInfo("ODIN/TrgTypeVsBXID", m_trgtypeHistovsBxid, m_trgtypeHistovsBxid->title());
      }
      return StatusCode::SUCCESS;
    }

    /// Algorithm overload: Start the algorithm
    StatusCode start() override {
      return StatusCode::SUCCESS;
    }


    /// Algorithm overload: Finalize the algorithm
    StatusCode finalize() override {
      m_bcidsHisto = 0;
      m_bxtypeHisto = 0;
      m_trgtypeHisto = 0;
      m_bxtypeHistovsBxid = 0;
      m_trgtypeHistovsBxid = 0;
      return StatusCode::SUCCESS;
    }

    /// Algorithm overload: Event execution routine
    StatusCode execute(const EventContext& /* context */ ) const override final {
      typedef std::vector<std::pair<const Tell1Bank*, const void*> > evt_data_t;
      if ( !m_bcidsHisto )   {
	error() << "Where the heck did my histogram go?" << endmsg;
	return StatusCode::FAILURE;
      }
      if ( !m_bxtypeHisto )   {
	error() << "Where the heck did my histogram go?" << endmsg;
	return StatusCode::FAILURE;
      }
      if ( !m_trgtypeHisto )   {
	error() << "Where the heck did my histogram go?" << endmsg;
	return StatusCode::FAILURE;
      }
      if ( !m_bxtypeHistovsBxid )   {
	error() << "Where the heck did my histogram go?" << endmsg;
	return StatusCode::FAILURE;
      }
      if ( !m_trgtypeHistovsBxid )   {
	error() << "Where the heck did my histogram go?" << endmsg;
	return StatusCode::FAILURE;
      }
      const evt_data_t* event = reinterpret_cast<evt_data_t*>(m_rawData.get());
      if ( event )   {
	std::for_each(begin(*event),end(*event),[this](const auto& bank) {
	    if ( bank.first->type() == Tell1Bank::ODIN )   {
	      if ( bank.first->version() >= 7 )  {
		auto* sodin = (pcie40::sodin_t*)bank.second;
		this->m_bcidsHisto->fill( sodin->bunch_id() );
		this->m_bxtypeHisto->fill( sodin->bx_type() );
		this->m_trgtypeHisto->fill( sodin->trigger_type() );
		this->m_bxtypeHistovsBxid->fill( sodin->bunch_id(), sodin->bx_type() );
		this->m_trgtypeHistovsBxid->fill( sodin->bunch_id(), sodin->trigger_type() );
	      }
	      else   {
		auto* odin = (RunInfo*)bank.second;
		this->m_bcidsHisto->fill( odin->bunch_id() );
		this->m_bxtypeHisto->fill( odin->bx_type() );
		this->m_bxtypeHistovsBxid->fill( odin->bunch_id(), odin->bx_type() );
	      }
	    }
	  });
      }
      return StatusCode::SUCCESS;
    }
  };
}

DECLARE_COMPONENT( Online::ODINMonitor )
