#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/RawBankSizes
-------------------
#]=======================================================================]

online_interface_library(RawBankSizesLib)
target_link_libraries(RawBankSizesLib INTERFACE Gaudi::GaudiKernel)
#
#===============================================================================
online_gaudi_module(RawBankSizes
        src/RawBankSizes.cpp
        src/Structure.cpp
        src/EventSize.cpp
	src/ODINMonitor.cpp
        src/RawEventRegister.cpp)
target_link_libraries(RawBankSizes PRIVATE
        AIDA::aida
	Boost::filesystem
        Gaudi::GaudiKernel
        LHCb::DAQEventLib
        Online::GaudiOnline
        Online::EventData
	Online::RawBankSizesLib
)
#
#===============================================================================
#online_executable(MakePages src/MakePages.cpp)
#target_link_libraries(MakePages PRIVATE Online::OnlineHistDB)
#
#online_executable(ParseHistParams src/ParseHistParams.cpp src/Structure.cpp)
#target_link_libraries(ParseHistParams PRIVATE Gaudi::GaudiKernel)
#
#===============================================================================
## FIXME: Do not know how to fix this -- Markus
##add_custom_target(WriteOptions ALL
##                  COMMAND ${env_cmd} --xml ${env_xml}
##                    ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/src/WriteOptions.py)
