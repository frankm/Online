#!/bin/bash
#
. /group/online/dataflow/scripts/preamble.sh;
. ${FARMCONFIGROOT}/job/createEnvironment.sh  $*;
#
setup_options_path MONITORING;
#
if test "$PARTITION" = "FESTxxx"; then
exec -a ${UTGID} \
    /home/frankm/bin/valgrind --tool=memcheck --leak-check=yes --track-origins=yes \
    `which genPython.exe` `which gaudirun.py` ${RAWBANKSIZESROOT}/options/ODINMon.py --application=OnlineEvents;
else
exec -a ${UTGID} \
    `which genPython.exe` `which gaudirun.py` ${RAWBANKSIZESROOT}/options/ODINMon.py --application=OnlineEvents;
fi;
