#ifdef RAWSIZE_BANKDESCR_H
#else
#define RAWSIZE_BANKDESCR_H


namespace Online   {

#define HISTPERHIST 3
  std::string titqual[HISTPERHIST] = {" (all Triggers)", " (HLTAccept)", " (noBias)"};
  std::string namqual[HISTPERHIST] = {"(all)", "(HLTAccept)", "(HLT-Pass)"};
  // titqual[0] = " (all Triggers)";
  // titqual[1] = " (nonLumi)";
  // titqual[2] = " (Lumi)";
  // namqual[0] = "(all)";
  // namqual[1] = "(nonLumi)";
  // namqual[2] = "(Lumi)";

  class BankDescr
  {
  public:
    bool isError;
    std::string name;
    int rootbankidx;
    double sum;
    double siz;
    double xmin,xmax,binw;
    int nbin;
    int nentries;
    AIDA::IHistogram1D *h[HISTPERHIST];
    std::string h_name[HISTPERHIST];
    AIDA::IProfile1D *p[HISTPERHIST];
    std::string p_name[HISTPERHIST];
    bool noprofile;
    bool nohist;

    BankDescr()
      {
	nentries = 0;
	sum=0;
	isError = false;
      }
    void init(const std::string& nam, bool noprof)
    {
      int i;
      name = nam;
      noprofile = noprof;
      for (i=0;i<HISTPERHIST;i++)
	{
	  h_name[i] = "h"+name+namqual[i];
	  p_name[i] = "p"+name+namqual[i];
	}

    };
    void init(int bn, bool noprof)
    {
      noprofile = noprof;
      switch (bn)
	{
	case Tell1Bank::ITError:
	  {
	    isError = true;
	    rootbankidx=Tell1Bank::IT;
	    break;
	  }
	case Tell1Bank::TTError:
	  {
	    isError = true;
	    rootbankidx=Tell1Bank::TT;
	    break;
	  }
	case Tell1Bank::VeloError:
	  {
	    isError = true;
	    rootbankidx=Tell1Bank::Velo;
	    break;
	  }
	case Tell1Bank::OTError:
	  {
	    isError = true;
	    rootbankidx=Tell1Bank::OT;
	    break;
	  }
	case Tell1Bank::EcalPackedError:
	  {
	    isError = true;
	    rootbankidx=Tell1Bank::EcalPacked;
	    break;
	  }
	case Tell1Bank::HcalPackedError:
	  {
	    isError = true;
	    rootbankidx=Tell1Bank::HcalPacked;
	    break;
	  }
	case Tell1Bank::PrsPackedError:
	  {
	    isError = true;
	    rootbankidx=Tell1Bank::PrsPacked;
	    break;
	  }
	case Tell1Bank::L0CaloError:
	  {
	    isError = true;
	    rootbankidx=Tell1Bank::L0Calo;
	    break;
	  }
	case Tell1Bank::L0MuonError:
	  {
	    isError = true;
	    rootbankidx=Tell1Bank::L0Muon;
	    break;
	  }
	case Tell1Bank::MuonError:
	  {
	    isError = true;
	    rootbankidx=Tell1Bank::Muon;
	    break;
	  }
	case Tell1Bank::L0DUError:
	  {
	    isError = true;
	    rootbankidx=Tell1Bank::L0DU;
	    break;
	  }
	case Tell1Bank::L0PUError:
	  {
	    isError = true;
	    rootbankidx=Tell1Bank::L0PU;
	    break;
	  }
	default:
	  {
	    isError=false;
	    rootbankidx = -1;
	    break;
	  }
	}
      name = Tell1Printout::bankType( Tell1Bank::BankType(bn) );
      int i;
      for (i=0;i<HISTPERHIST;i++)
	{
	  h_name[i] = "h"+name+namqual[i];
	  p_name[i] = "p"+name+namqual[i];
	}
    }
  };
}
#endif
