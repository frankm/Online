#ifndef BEAT_STRUCTURE_H 
#define BEAT_STRUCTURE_H 1

// Include files
#include "GaudiKernel/StatusCode.h"
#include <string>

namespace HParam   {
  class HistParams   	{
  public:
    int   n_bin  { -2  };
    float xmin   { 0e0 };
    float xmax   { 0e0 };
    int   s_min  { 0   };
    int   s_max  { -2  };
    std::string det { "Unknown" };
    HistParams() = default;
  };
}
// ============================================================================
namespace Gaudi  {
  namespace Utils   {
    std::ostream& toStream (const HParam::HistParams& o, std::ostream& os);
  }
  namespace Parsers   {
    StatusCode parse(HParam::HistParams& o, const std::string& input);
  }
}
// ============================================================================
// The END 
// ============================================================================
#endif // BEAT_STRUCTURE_H
// ============================================================================
