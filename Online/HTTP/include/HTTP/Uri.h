//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef HTTP_HTTP_URI_H
#define HTTP_HTTP_URI_H

// C/C++ include files
#include <string>

/// Namespace for the http server and client apps
namespace http   {
  
  /// Simple uri parser
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    10.01.2021
   */
  struct Uri {
  public:
    std::string protocol, host, port, path, query;
  public:
    Uri(const std::string& url);
    Uri(Uri&& copy) = default;
    Uri(const Uri& copy) = default;
    Uri& operator=(Uri&& copy) = default;
    Uri& operator=(const Uri& copy) = default;
  };
}      // End namespace http
#endif // HTTP_HTTP_URI_H
