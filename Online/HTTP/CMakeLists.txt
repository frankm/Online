#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/HTTP
-----------
#]=======================================================================]
#
online_library(HTTP
        src/Cache.cpp
        src/Compress.cpp
        src/HttpReply.cpp
        src/HttpHeader.cpp
        src/HttpServer.cpp
        src/HttpClient.cpp
        src/HttpRequest.cpp
        src/HttpFileHandler.cpp
        src/HttpCacheCheck.cpp
        src/HttpCacheHandler.cpp
        src/Uri.cpp
)
target_link_libraries(HTTP PUBLIC Online::OnlineBase Boost::headers Threads::Threads ZLIB::ZLIB -lrt)
#
online_install_includes(include)
