//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : HTTP
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <HTTP/HttpCacheCheck.h>
#include <HTTP/HttpCacheHandler.h>

// C/C++ include files
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/bind.hpp>

/// Initializing constructor. Start timer
http::HttpCacheCheck::HttpCacheCheck(HttpCacheHandler& hdlr, boost::asio::io_service& io, int tmo)
  : timer(io), handler(hdlr), timeout(tmo)
{
  this->start();
}

/// Initializing constructor. Does not start timer.
http::HttpCacheCheck::HttpCacheCheck(HttpCacheHandler& hdlr, boost::asio::io_service& io)
  : timer(io), handler(hdlr), timeout(10)
{
}

/// Default destructor
http::HttpCacheCheck::~HttpCacheCheck()    {
}

/// Rearm timer after single shot
void http::HttpCacheCheck::start()   {
  timer.expires_from_now(boost::posix_time::seconds(this->timeout));
  timer.async_wait(boost::bind(&HttpCacheCheck::check, this));
}

/// Rearm timer after single shot with new timeout
void http::HttpCacheCheck::start(int tmo)   {
  this->timeout = tmo;
  this->start();
}

/// Default cleanup handler: drop expired items
void http::HttpCacheCheck::check()   {
  if ( this->handler.cache.debug )     {
    std::cout << std::setw(14) << std::left << this->handler.cache.name+":" 
	      << "Timer callback: Flush old cache entries...." << std::endl;
  }
  this->handler.drop_expired();
  this->start();
}
