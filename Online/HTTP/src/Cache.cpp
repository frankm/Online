//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <HTTP/Cache.h>
#include <HTTP/Cache.inl.h>

// C/C++ include files
#include <cstring>

template <>
void http::Cache<http::HttpReply>::_set(http::Cache<http::HttpReply>::entry_type* e,
					http::Cache<http::HttpReply>::key_type    key,
					std::time_t            tmo,
					const std::string&     path,
					const std::string&     encoding,
					const http::HttpReply& reply)
{
  e->data     = reply.clone();
  e->timeout  = ::time(0) + tmo;
  e->encoding = encoding;
  e->path     = path;
  e->hash     = key;
}

template class http::Cache<http::HttpReply>;
