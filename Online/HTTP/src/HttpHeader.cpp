//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#include "HTTP/HttpHeader.h"
#include <vector>
#include <sstream>
#include <cstring>
#include <stdexcept>

using namespace std;

namespace http {

  /// Initializing constructor
  template <typename T> HttpHeader::HttpHeader(const string& n, const T& v) : name(n)   {
    stringstream str;
    str << v;
    this->value = str.str();
  }

  /// Initializing constructor
  template <typename T> HttpHeader::HttpHeader(const string& n, const T* v) : name(n)   {
    stringstream str;
    str << *v;
    this->value = str.str();
  }

  template <> HttpHeader::HttpHeader(const string& n, struct tm*const& v) : name(n)   {
    char text[64];
    ::strftime(text,sizeof(text),constants::DATE_TIME_FORMAT,v);
    this->value = text;
  }

  template <> HttpHeader::HttpHeader(const string& n, const struct tm& v) : name(n)   {
    char text[64];
    ::strftime(text,sizeof(text),constants::DATE_TIME_FORMAT,&v);
    this->value = text;
  }

  /// Initializing constructor (specializations)
  template HttpHeader::HttpHeader(const string& n, const char& v);
  template HttpHeader::HttpHeader(const string& n, const unsigned char& v);
  template HttpHeader::HttpHeader(const string& n, const short& v);
  template HttpHeader::HttpHeader(const string& n, const unsigned short& v);
  template HttpHeader::HttpHeader(const string& n, const int& v);
  template HttpHeader::HttpHeader(const string& n, const unsigned int& v);
  template HttpHeader::HttpHeader(const string& n, const long& v);
  template HttpHeader::HttpHeader(const string& n, const unsigned long& v);
  template HttpHeader::HttpHeader(const string& n, const float& v);
  template HttpHeader::HttpHeader(const string& n, const double& v);

  /// Convert header value to generic primitive type
  template<typename T> T HttpHeader::as()   const   {
    stringstream str(this->value);
    T tim = 0;
    str >> tim;
    return tim;
  }

  /// Convert header value to generic primitive type (specializations)
  template char           HttpHeader::as<char>()            const;
  template unsigned char  HttpHeader::as<unsigned char>()   const;
  template short          HttpHeader::as<short>()           const;
  template unsigned short HttpHeader::as<unsigned short>()  const;
  template int            HttpHeader::as<int>()             const;
  template unsigned int   HttpHeader::as<unsigned int>()    const;
  template long           HttpHeader::as<long>()            const;
  template unsigned long  HttpHeader::as<unsigned long>()   const;
  template float          HttpHeader::as<float>()           const;
  template double         HttpHeader::as<double>()          const;
  template string         HttpHeader::as<string>()          const;

  /// Convert header value to time stamp in seconds since epoch
  template <> struct tm HttpHeader::as<struct tm>()  const   {
    struct tm tim;
    if ( 0 != ::strptime(value.c_str(),constants::DATE_TIME_FORMAT,&tim) )
      return tim;
    throw runtime_error("HttpHeader: Failed to convert header['"+name+"']: '"+this->value+" to date-time");
  }

  /// Convert header value to time counter (time_t)
  time_t HttpHeader::as_time()  const   {
    struct tm tm = this->as<struct tm>();
    return ::mktime(&tm);
  }

  /// Access date-time string
  std::string HttpHeader::now()    {
    return HttpHeader::date_time(::time(nullptr));
  }
  
  /// Access date-time string
  std::string HttpHeader::date_time(time_t time)    {
    struct tm tim;
    char text[64];
    ::gmtime_r(&time, &tim);
    ::strftime(text,sizeof(text),constants::DATE_TIME_FORMAT,&tim);
    return text;
  }
  
  /// Formatted printout with prefix
  std::ostream& HttpHeader::print(std::ostream& os, const std::string& prefix)  const   {
    os << prefix << "HTTP header:  '" << this->name << "' = '" << this->value << "'" << std::endl;
    return os;
  }

  vector<unsigned char> to_vector(const char* data)   {
    vector<unsigned char> r;
    if ( data )   {
      size_t len = ::strlen(data);
      r.reserve(len+1);
      for(const char *c=data; *c; ++c) r.push_back((unsigned char)(*c));
    }
    return r;
  }

  vector<unsigned char> to_vector(const string& data)   {
    vector<unsigned char> r;
    if ( !data.empty() )   {
      r.reserve(data.length()+1);
      for(const char *c=data.c_str(); *c; ++c) r.push_back((unsigned char)(*c));
    }
    return r;
  }

  std::string to_string(const unsigned char* data, size_t len)    {
    std::string res;
    if ( data )   {
      const char* p = reinterpret_cast<const char*>(data);
      res.assign(p, p+len);
    }
    return res;
  }
}
