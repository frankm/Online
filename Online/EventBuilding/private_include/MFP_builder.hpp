#ifndef MFP_BUILDER_H
#define MFP_BUILDER_H 1

#include "EventBuilding/raw_tools.hpp"
#include "EventBuilding/mdf_tools.hpp"
#include <vector>
#include <cstring>
#include <cassert>
#include <memory>
#include "EventBuilding/MFP_tools.hpp"

namespace EB {

  constexpr uint8_t base_MFP_align = 2;
  class MFP_builder {
  private:
    EB::source_header_map_type<std::vector<EB::raw_header*>> _raw_map;
    EB::source_header_map_type<uint64_t> _ev_id_map;

    uint64_t _ev_id;
    uint64_t _temporary_ev_id;

  public:
    MFP_builder();
    // append the raw blocks of the given event to the internal structure
    int append_event(const EB::MDF_block& event);
    // append the raw blocks of the given events to the internal structure
    int append_events(const std::vector<EB::MDF_block>& events);

    // returns the mapping between the various sources and the buffers
    std::vector<EB::type_id_pair_type> get_source_mapping();
    // returns the number of sources
    size_t get_source_number();
    // generates an MFP with the given packing fraction
    int build_MFP(size_t packing_fraction, char* buffer, size_t buffer_size, EB::type_id_pair_type key);
    // generates all the MFPs with the given packing fraction
    int build_MFPs(size_t packing_fraction, const std::vector<char*>& buffers, const std::vector<size_t>& buffer_sizes);
    int build_MFPs(
      size_t packing_fraction,
      const std::vector<std::shared_ptr<char>>& buffers,
      const std::vector<size_t>& buffer_sizes);
    // generates multiple MFPs with the given packing fraction the MFPs are generated using the provided source mapping
    // a partial mapping of the detector is possible
    int build_MFPs(
      size_t packing_fraction,
      const std::vector<char*>& buffers,
      const std::vector<size_t>& buffer_sizes,
      const std::vector<EB::type_id_pair_type> source_mapping);
    int build_MFPs(
      size_t packing_fraction,
      const std::vector<std::shared_ptr<char>>& buffers,
      const std::vector<size_t>& buffer_sizes,
      const std::vector<EB::type_id_pair_type> source_mapping);
    // all the events are deleted from the internal structure
    void reset_data();
    // resets the ev_id
    void reset_ev_id();
    // resets everything
    void reset();
  };
} // namespace EB

#endif // MFP_BUILDER_H