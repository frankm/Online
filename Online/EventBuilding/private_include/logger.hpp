#ifndef EB_LOGGER_H
#define EB_LOGGER_H 1
#include <iostream>
#include <sstream>

#include "DD4hep/Printout.h"

namespace EB {
  constexpr Online::PrintLevel base_level = Online::PrintLevel::INFO;

  template<Online::PrintLevel print_level>
  class Log_stream_buff : public std::stringbuf {
  public:
    Log_stream_buff(Online::PrintLevel set_level = base_level, const std::string& name = "") :
      std::stringbuf(), _set_level(set_level), _name(name)
    {}

    void set_level(Online::PrintLevel level) { _set_level = level; }
    void set_name(const std::string& name) { _name = name; }

    virtual int sync() override
    {
      if (print_level >= _set_level) {
        Online::printout(print_level, _name.c_str(), this->str().c_str());
      }

      this->str("");

      return 0;
    }

  private:
    Online::PrintLevel _set_level;
    std::string _name;
  };

  template<Online::PrintLevel print_level>
  class Log_stream : public std::ostream {
  public:
    Log_stream() {}
    Log_stream(std::stringbuf* buff, Online::PrintLevel level = base_level) : std::ostream(buff), _set_level(level) {}

    void set_level(Online::PrintLevel level) { _set_level = level; }

    // TODO check if this is the right way
    Log_stream<print_level>& operator<<(std::ostream& (*pf)(std::ostream&) )
    {
      if (print_level >= _set_level) {
        pf(*(dynamic_cast<std::ostream*>(this)));
      }
      return *this;
    }

    // TODO check if this is the right way
    template<typename T>
    Log_stream<print_level>& operator<<(const T& data)
    {
      if (print_level >= _set_level) {
        *(dynamic_cast<std::ostream*>(this)) << data;
      }

      return *this;
    }

  private:
    Online::PrintLevel _set_level;
  };

  class Logger {
  public:
    Logger() :
      _nolog(), _verbose(), _debug(), _info(), _warning(), _error(), _fatal(), _always(), nolog(&_nolog),
      verbose(&_verbose), debug(&_debug), info(&_info), warning(&_warning), error(&_error), fatal(&_fatal),
      always(&_always), _set_level(base_level)
    {}

    Logger(const std::string name, Online::PrintLevel level) : Logger()
    {
      set_level(level);
      set_name(name);
    }

    void set_level(Online::PrintLevel level)
    {
      nolog.set_level(level);
      verbose.set_level(level);
      debug.set_level(level);
      info.set_level(level);
      warning.set_level(level);
      error.set_level(level);
      fatal.set_level(level);
      always.set_level(level);

      _nolog.set_level(level);
      _verbose.set_level(level);
      _debug.set_level(level);
      _info.set_level(level);
      _warning.set_level(level);
      _error.set_level(level);
      _fatal.set_level(level);
      _always.set_level(level);

      _set_level = level;
    }

    void set_name(const std::string& name)
    {
      _nolog.set_name(name);
      _verbose.set_name(name);
      _debug.set_name(name);
      _info.set_name(name);
      _warning.set_name(name);
      _error.set_name(name);
      _fatal.set_name(name);
      _always.set_name(name);

      _name = name;
    }

    std::string get_name() const { return _name; }
    Online::PrintLevel get_level() const { return _set_level; }

    bool is_active(Online::PrintLevel level) const { return level >= _set_level; }

  private:
    Log_stream_buff<Online::PrintLevel::NOLOG> _nolog;
    Log_stream_buff<Online::PrintLevel::VERBOSE> _verbose;
    Log_stream_buff<Online::PrintLevel::DEBUG> _debug;
    Log_stream_buff<Online::PrintLevel::INFO> _info;
    Log_stream_buff<Online::PrintLevel::WARNING> _warning;
    Log_stream_buff<Online::PrintLevel::ERROR> _error;
    Log_stream_buff<Online::PrintLevel::FATAL> _fatal;
    Log_stream_buff<Online::PrintLevel::ALWAYS> _always;

  public:
    Log_stream<Online::PrintLevel::NOLOG> nolog;
    Log_stream<Online::PrintLevel::VERBOSE> verbose;
    Log_stream<Online::PrintLevel::DEBUG> debug;
    Log_stream<Online::PrintLevel::INFO> info;
    Log_stream<Online::PrintLevel::WARNING> warning;
    Log_stream<Online::PrintLevel::ERROR> error;
    Log_stream<Online::PrintLevel::FATAL> fatal;
    Log_stream<Online::PrintLevel::ALWAYS> always;

  private:
    Online::PrintLevel _set_level;
    std::string _name;
  };

} // namespace EB

#endif // EB_LOGGER_H
