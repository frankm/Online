#ifndef MFP_PRELOADER_H
#define MFP_PRELOADER_H 1

#include "MFP_builder.hpp"
#include "mdf_reader.hpp"
#include <vector>
#include <memory>
#include <functional>
#include <tuple>
#include <cassert>
#include <cstdint>

namespace EB {
  class MFP_preloader {
  public:
    typedef std::shared_ptr<char> buffer_ptr_type;

    MFP_preloader() = default;

    bool set_MDF_filename(const char* filename);

    void set_packing_factor(size_t packing_factor);
    size_t get_packing_factor() const;

    void set_n_MFPs(int n_MFPs);
    int get_n_MFPs() const;

    int set_buffers(std::vector<buffer_ptr_type>& buffers, std::vector<size_t>& buffer_sizes);
    std::tuple<std::vector<buffer_ptr_type>, std::vector<size_t>, std::vector<size_t>> get_buffers();

    void set_detector_map(const std::vector<EB::type_id_pair_type>& src_map);

    int preload_MFPs();

  private:
    // based fragment size used for self buffer allocation
    // const size_t frag_size = 256;
    const size_t _frag_size = 1024;
    size_t _packing_factor;
    size_t _n_MFPs;

    std::vector<buffer_ptr_type> _buffers;
    std::vector<size_t> _buffer_sizes;
    std::vector<size_t> _buffer_occupancy;

    std::vector<EB::type_id_pair_type> _source_mapping;

    MDF_reader _reader;

    EB::MFP_builder _builder;
  };
} // namespace EB

#endif // MFP_PRELOADER_H
