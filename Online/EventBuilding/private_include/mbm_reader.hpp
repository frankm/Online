#ifndef MBM_READER_H
#define MBM_READER_H 1

#include "buffer_interface.hpp"
#include "Dataflow/MBMClient.h"
#include "Dataflow/DataflowComponent.h"
#include "MBM/Producer.h"
#include "MBM/Consumer.h"
#include <cstddef>
#include <cstdint>
#include <deque>
#include <iostream>
#include <string>
#include <tuple>

#include "MBM/Requirement.h"

namespace EB {
  template<class T>
  class Mbm_reader : public EB::Buffer_reader<T> {
  public:
    // alignment is explesser in 2^alignment min value 1
    Mbm_reader() : EB::Buffer_reader<T>() {}
    // Mbm_writer(MBM::Producer* producer, int facility = 0);
    Mbm_reader(
      Online::DataflowComponent::Context& context,
      const std::string& instance,
      const std::string& name,
      const std::string& req);
    virtual ~Mbm_reader();
    std::vector<std::tuple<void*, size_t>> get_full_buffer() override;
    T* try_get_element() override;
    void read_complete() override;
    void flush() override;
    void cancel();

    int get_src_id() const override { return 0; }

  protected:
    Online::DataflowComponent::Context* _context;
    std::unique_ptr<MBM::Consumer> _consumer;
    int _total_size = 0;
    std::string _m_req;
    MBM::Requirement _m_requirement;
    bool _ack_pending = false;
  };
} // namespace EB

#include "mbm_reader_impl.hpp"

#endif // MBM_WRITER_H
