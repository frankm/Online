#ifndef BUFFER_INTERFACE_H
#define BUFFER_INTERFACE_H 1

#include <cstdint>
#include <cstddef>
#include <unistd.h>
#include <vector>
#include <tuple>
#include <atomic>

#include "Dataflow/DataflowContext.h"

namespace EB {
  // DEBUG test
  static int BUFFER_STATUS_IGNORE = 0;

  class Buffer_element {
  public:
    Buffer_element(int payload, size_t size);
    Buffer_element();
    ~Buffer_element() {}

    size_t bytes();
    bool is_valid();
    bool is_wrap();
    void set_wrap();
    void init(int payload, size_t size);

    // protected:
    int _status; // 0 non init 1 is valid 2 is wrap

    size_t _size;

    int _payload;
  };

  template<class T>
  class Buffer_reader {
  private:
    bool _cancel = false;

  public:
    // alignment is expressed as 2^alignment
    // Buffer_reader(uint8_t alignment = 12) : Aligned_buffer(alignment) {}
    virtual ~Buffer_reader() {}

    virtual std::vector<std::tuple<void*, size_t>> get_full_buffer() = 0;

    // non blocking ask for next data
    virtual T* try_get_element() = 0;
    // blocking ask for next data
    T* get_element(int& status = BUFFER_STATUS_IGNORE)
    {
      status = Online::DataflowStatus::DF_SUCCESS;
      T* ret_val = NULL;
      bool cancel_copy;
      while ((ret_val == NULL) && !(cancel_copy = _cancel)) {
        ret_val = this->try_get_element();
        if (ret_val == NULL) {
          usleep(1);
        }
      }

      if ((ret_val == NULL) && cancel_copy) {
        status = Online::DataflowStatus::DF_CANCELLED;
      }

      return ret_val;
    }

    // cancel all pending operations
    void cancel() { _cancel = true; }
    // resume normal operation after cancel is issues
    void reset_cancel() { _cancel = false; }
    // ack all the pending reads
    virtual void read_complete() = 0;
    // remove any data from the buffer
    virtual void flush() = 0;
    // The src id should be set by an external device or in the constructor of the derived class
    virtual int get_src_id() const = 0;
  };

  template<class T>
  class Buffer_writer {
  private:
    bool _cancel = false;

  public:
    virtual ~Buffer_writer() {}

    virtual std::vector<std::tuple<void*, size_t>> get_full_buffer() = 0;

    // non blocking ask for space
    virtual T* try_write_next_element(size_t size) = 0;
    // blocking ask for space
    T* write_next_element(size_t size, int& status = BUFFER_STATUS_IGNORE)
    {
      status = Online::DataflowStatus::DF_SUCCESS;
      T* ret_val = NULL;
      bool cancel_copy;
      while ((ret_val == NULL) && !(cancel_copy = _cancel)) {
        ret_val = this->try_write_next_element(size);
        if (ret_val == NULL) {
          usleep(1);
        }
      }

      if ((ret_val == NULL) && cancel_copy) {
        status = Online::DataflowStatus::DF_CANCELLED;
      }

      return ret_val;
    }

    // cancel all pending operations
    void cancel()
    {
      _cancel = true;
      write_discard();
    }
    // resume normal operation after cancel is issues
    void reset_cancel() { _cancel = false; }
    // ack all the pending writes
    virtual void write_complete() = 0;
    // discard all the pending writes
    virtual void write_discard() = 0;
    // The src id should be set by an external device or in the constructor of the derived class
    virtual int get_src_id() const = 0;
  };

} // namespace EB

#endif // BUFFER_INTERFACE_H
