#ifndef TIMER_H
#define TIMER_H 1
#include <ctime>

namespace EB {
  class Timer {
  public:
    Timer(clockid_t clk_id = CLOCK_MONOTONIC);

    // start stop and reset of a stop watch timer reset does not stop the timer it clears only the elasped time
    void start();
    void stop();
    void reset();

    // get elasped time is seconds or nano seconds without stopping the timer
    double get_elapsed_time_s() const;
    clock_t get_elapsed_time_ns() const;

    // setter and getter for the clockid used the setter resets the time to avoid inconsistency
    clockid_t get_clk_id() const;
    void set_clk_id(clockid_t clk_id);

  private:
    bool _is_running;
    timespec _start_time;
    clock_t _elapsed_time;
    clockid_t _clock_id;
  };
} // namespace EB
#endif // TIMER_H