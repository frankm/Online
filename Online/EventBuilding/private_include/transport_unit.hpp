#ifndef TRANSPORT_UNIT_H
#define TRANSPORT_UNIT_H 1

#include <memory>
#include <vector>
#include <cassert>
#include <string>
#include "logger.hpp"
#include "Parallel_Comm.hpp"
#include "EventBuilding/MEP_tools.hpp"
#include "Dataflow/DataflowComponent.h"

namespace EB {
  using namespace Online;
  // static configuration
  bool is_bu(int rank, const std::vector<int>& n_rus_nic);
  bool is_ru(int rank, const std::vector<int>& n_rus_nic);

  class Transport_unit : public DataflowComponent {
  public:
    Transport_unit(const std::string& name, Context& framework);
    virtual ~Transport_unit();

    // FOR DEBUG ONLY
    void set_rank(int rank);

    virtual int initialize() override;
    virtual int finalize() override;
    virtual int cancel() override;
    virtual int start() override;
    virtual int stop() override;

  protected:
    int _my_rank;
    int _world_size;
    int _numa_node;
    // Properties
    std::vector<int> _n_rus_per_nic;
    int _n_par_mess;
    std::vector<int> _n_sources_per_ru;
    // _ru_ranks and _bu_ranks can either be self assigned or set via an opts file
    std::vector<int> _ru_ranks;
    std::vector<int> _bu_ranks;
    std::vector<int> _shift_pattern;
    bool _MPI_errors_return;

    // DEBUG counters
    uint32_t _barrier_count;
    uint32_t _pre_barrier_count;

    // prefix sum of the number of sources up to the give idx, the last element is the total number of sources
    std::vector<int> _prefix_n_sources_per_ru;

    Logger logger;
    std::unique_ptr<IB_verbs::Parser> _ibParser;
    std::unique_ptr<Parallel_comm> _ibComm;
    std::string _ib_config_file = "";
    int sync();
    int init_pattern();
    int init_ranks();
    int check_ranks();
    int init_n_sources();
    virtual void reset_counters();
  };
} // namespace EB

#endif // TRANSPORT_UNIT_H
