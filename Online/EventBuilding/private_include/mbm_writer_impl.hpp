#include "mbm_writer.hpp"
#include "RTL/rtl.h"
#include <stdexcept>
#include "EventBuilding/tools.hpp"
#include <unistd.h>
#include <sys/stat.h> /* For mode constants */
#include <fcntl.h>    /* For O_* constants */
#include <sys/mman.h>
#include <cstring>
#include <sstream>
#include <string>
#include <memory>
#include <iostream>
#include <tuple>

template<class T>
EB::Mbm_writer<T>::Mbm_writer(
  Online::DataflowComponent::Context& context,
  const std::string& instance,
  const std::string& name) :
  EB::Buffer_writer<T>(),
  _context(&context), _producer(context.mbm->createProducer(name, instance)), _status(IDLE)
{}

template<class T>
EB::Mbm_writer<T>::~Mbm_writer()
{
  if (this->is_set()) {
    _context->mbm->cancelBuffers();
  }
}

template<class T>
std::vector<std::tuple<void*, size_t>> EB::Mbm_writer<T>::get_full_buffer()
{
  // TODO check for errors
  void* mbm_data = (void*) mbm_buffer_address(_producer->id());
  size_t mbm_size;
  mbm_buffer_size(_producer->id(), &mbm_size);

  std::vector<std::tuple<void*, size_t>> ret_val;
  ret_val.emplace_back(std::tuple<void*, size_t>(mbm_data, mbm_size));
  return ret_val;
}

template<class T>
T* EB::Mbm_writer<T>::try_write_next_element(size_t size)
{
  T* ret_val = NULL;
  int status;

  if (!this->is_set()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " on an invalid buffer";
    throw std::logic_error(err_mess.str());
  }

  if (_status != IDLE) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " used buffer use write_complete, write_discard or cancel.";
    throw std::logic_error(err_mess.str());
  }

  status = _producer->getSpaceTry(size);

  if (status == MBM_NORMAL) {
    MBM::EventDesc& dsc = _producer->event();
    ret_val = reinterpret_cast<T*>(dsc.data);
    dsc.len = size;
    dsc.type = EVENT_TYPE_MEP;
    // If MEP, we emulate a trigger mask with the partition ID
    // TODO check what should be put in here
    dsc.mask[0] = _producer->partitionID();
    dsc.mask[1] = ~0x0;
    dsc.mask[2] = ~0x0;
    dsc.mask[3] = ~0x0;
    _status = ACK_PENDING;
  } else if (status == MBM_NO_ROOM) {
    ret_val = NULL;
  } else {
    std::ostringstream err_mess;
    err_mess << "Mbm_writer: try_write_next_element getSpaceTry error " << status;
    throw std::runtime_error(err_mess.str());
  }

  return ret_val;
}

template<class T>
void EB::Mbm_writer<T>::write_discard()
{
  _status = IDLE;
}

template<class T>
void EB::Mbm_writer<T>::write_complete()
{
  // TODO status and ret_Val should consistent
  int status;
  if (!this->is_set()) {
    std::ostringstream err_mess;
    err_mess << "Mbm_writer: write_complete on an invalid buffer";
    throw std::logic_error(err_mess.str());
  }

  if (_status == ACK_PENDING) {

    status = _producer->declareEvent();
    if (status != MBM_NORMAL) {
      // TODO add proper error handling, like cancelling pending requests
      std::ostringstream err_mess;
      err_mess << "Mbm_writer: write_complete declareEvent error " << status;
      throw std::runtime_error(err_mess.str());
    }

    status = _producer->sendSpace();
    if (status != MBM_NORMAL) {
      // TODO add proper error handling, like cancelli pending requests
      std::ostringstream err_mess;
      err_mess << "Mbm_writer: write_complete sendSpace error " << status;
      throw std::runtime_error(err_mess.str());
    }
    _status = IDLE;
  }
}

template<class T>
void EB::Mbm_writer<T>::cancel()
{
  EB::Buffer_writer<T>::cancel();

  _producer->cancel();
  _status = IDLE;
}

template<class T>
size_t EB::Mbm_writer<T>::free_space()
{
  return 0;
}

template<class T>
bool EB::Mbm_writer<T>::is_set()
{
  return (_producer.get() != nullptr);
}

template<class T>
void EB::Mbm_writer<T>::reset(MBM::Producer* producer)
{
  _producer.reset(producer);
  // if the instance is different we assume non pending acks
  _status = IDLE;
}
