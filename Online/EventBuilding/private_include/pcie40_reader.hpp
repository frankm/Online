#ifndef PCIE40_READER_H
#define PCIE40_READER_H 1
#include <memory>
#include <vector>
#include <deque>
#include <lhcb/pcie40/id.h>
#include <lhcb/pcie40/daq.h>
#include <lhcb/pcie40/pcie40.hpp>
#include "EventBuilding/MFP_tools.hpp"
#include "buffer_interface.hpp"
#include "circular_buffer_reader.hpp"
#include "circular_buffer_writer.hpp"
#include "shared_ptr_buffer_backend.hpp"
#include "file_writer.hpp"

namespace EB {
  // alignments are expressed as a power of 2
  constexpr int default_PCIe40_alignment = 12;     // 4096 B
  constexpr int default_PCIe40_frag_alignment = 5; // 32 B

  class PCIe40_reader {
  protected:
    static constexpr int src_version_mask = 0xFF0000;
    static constexpr int src_version_shift = 16;
    static constexpr int src_subsystem_mask = 0xF800;
    static constexpr int src_subsystem_shift = 11;
    static constexpr int src_num_mask = 0x7FF;
    static constexpr int src_num_shift = 0;

    void* _buffer;
    ssize_t _buffer_size;

    ssize_t _internal_read_off;
    ssize_t _device_read_off;
    ssize_t _available_data;
    size_t _requested_size;

    int _stream;
    int _id_fd;
    int _stream_fd;
    int _ctrl_fd;

    std::string _name;

    // TODO this hould be controlled by the ECS
    // alignment between MFPs into the PCIe40 buffer as a power of 2
    int PCIe40_alignment = default_PCIe40_alignment;

    int update_device_ptr();

    virtual void init_internal_status();

  public:
    PCIe40_reader();
    PCIe40_reader(int id, int stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN);
    PCIe40_reader(const std::string& name, int stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN);
    PCIe40_reader(const PCIe40_reader& other) = delete;
    PCIe40_reader(PCIe40_reader&& other);
    virtual ~PCIe40_reader();

    PCIe40_reader& operator=(const PCIe40_reader& other) = delete;
    PCIe40_reader& operator=(PCIe40_reader&& other);

    int open(int id);
    int open(const std::string& name);

    // Resets the the cards and flush the buffer
    int reset();

    bool is_open();

    void close();

    template<class T>
    T* extract_element();
    void ack_read();
    void update_usage();
    void cancel_pending();

    const std::string get_name() const;

    uint16_t _src_id;
    uint8_t _block_version;
  };

  class PCIe40_MFP_reader : public PCIe40_reader, public EB::Buffer_reader<EB::MFP> {
  public:
    PCIe40_MFP_reader();
    PCIe40_MFP_reader(int id, int stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN);
    PCIe40_MFP_reader(const std::string& name, int stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN);

    EB::MFP* try_get_element() override;
    void read_complete() override;
    void flush() override;

    int get_src_id() const override;

    std::vector<std::tuple<void*, size_t>> get_full_buffer() override;
  };

  class PCIe40_frag_reader : public EB::Buffer_reader<EB::MFP> {
  public:
    PCIe40_frag_reader();
    PCIe40_frag_reader(
      int id,
      size_t buffer_size,
      int n_frags = 1,
      size_t buffer_alignment = default_PCIe40_alignment,
      int stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN);
    PCIe40_frag_reader(
      const std::string& name,
      size_t buffer_size,
      int n_frags = 1,
      size_t buffer_alignment = default_PCIe40_alignment,
      int stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN);

    EB::MFP* try_get_element() override;
    void read_complete() override;
    void flush() override;

    int get_src_id() const override;

    void set_n_frags(int n_MFPs);
    int get_n_frags() const;

    void set_align(int n_MFPs);
    int get_align() const;

    std::vector<std::tuple<void*, size_t>> get_full_buffer() override;

  protected:
    PCIe40_reader _pcie40_reader;
    Circular_buffer_reader<EB::MFP, Shared_ptr_buffer_backend> _internal_buffer_reader;
    Circular_buffer_writer<EB::MFP, Shared_ptr_buffer_backend> _internal_buffer_writer;

    uint16_t _n_frags;
    // TODO we may want to modify this
    uint8_t _align = default_PCIe40_frag_alignment;
    // add more vectors for sizes and offsets
    // add a function that builds all the MFPs in the buffer
    std::vector<void*> _frag_list;
    std::vector<uint8_t> _type_list;
    std::vector<uint16_t> _size_list;
    uint64_t _ev_id;
    uint8_t _version;
    uint32_t _total_size = 0;

    // Set to true when a flush is received, deasserted when the consecutive sequenque of flushes ends
    bool _flush = false;
    // Set to true if a flush MFP is pending because the internal buffer is full, deasserted when the flush MFP is sent
    bool _flush_pending = false;

    void scan_frag_list();
    int build_MFP();
  };
} // namespace EB

#endif // PCIE40_READER_H
