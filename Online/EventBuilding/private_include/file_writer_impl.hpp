#include "file_writer.hpp"

#include <sys/stat.h>
#include <fcntl.h>
#include <sstream>
#include <stdexcept>

template<class T>
EB::File_writer<T>::File_writer(const std::string& file_name) :
  _file_name(file_name), _file(fopen(file_name.c_str(), "wb"), [](FILE* file) { fclose(file); })
{
  if (_file == NULL) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " " << file_name << ": " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }
}

template<class T>
int EB::File_writer<T>::write(const T* data)
{
  int ret_val = 0;
  if (data != NULL) {
    ret_val = fwrite(data, 1, data->bytes(), _file.get());
  }

  fflush(_file.get());

  return ret_val;
}