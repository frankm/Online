#ifndef CIRCULAR_BUFFER_STATUS_H
#define CIRCULAR_BUFFER_STATUS_H 1

#include <cstddef>
#include <cstdint>

namespace EB {
  constexpr uint64_t ptr_mask = 0x7fffffffffffffff;
  constexpr uint64_t wrap_mask = ~ptr_mask;

  struct Circular_buffer_status {
    Circular_buffer_status() : write_stat(0), read_stat(0), size(0), alignment(0), id(0) {}
    Circular_buffer_status(size_t size, size_t alignment, int id) :
      write_stat(0), read_stat(0), size(size), alignment(alignment), id(id)
    {}

    uint64_t write_stat;
    uint64_t read_stat;

    // size of the usable allocated area
    size_t size;
    size_t alignment;
    int id;

    bool is_empty() const;
    bool is_full() const;
    bool same_page() const;

    size_t tail_free_space() const;
    size_t head_free_space() const;

    uintptr_t write_ptr() const;
    uintptr_t read_ptr() const;

    void set_write_ptr(uintptr_t write_ptr);
    void set_read_ptr(uintptr_t read_ptr);
    void toggle_write_wrap();
    void toggle_read_wrap();
  };
} // namespace EB

#endif // CIRCULAR_BUFFER_STATUS_H
