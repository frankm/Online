#ifndef PARALLEL_COMM_H
#define PARALLEL_COMM_H 1

#include <vector>
#include <queue>

#include "timer.hpp"
#include "logger.hpp"
#include "infiniband_net/ib.hpp"
#include "infiniband_net/sock.hpp"
#include "infiniband_net/parser.hpp"
#include "Dataflow/DataflowContext.h"

namespace EB {

  enum comm_op_status { INIT = 0, PENDING, COMPLETED, ERROR };

  struct comm_op {
    int count;
    size_t datatype;

    virtual ~comm_op() = default;

    virtual std::ostream& print(std::ostream& os) const;
    friend std::ostream& operator<<(std::ostream& os, const comm_op& op);
  };

  struct comm_send : public comm_op {
    const void* snd_buff;
    int destination;

    std::ostream& print(std::ostream& os) const override;
    friend std::ostream& operator<<(std::ostream& os, const comm_send& send);
  };

  struct comm_recv : public comm_op {
    void* recv_buff;
    int source;

    std::ostream& print(std::ostream& os) const override;
    friend std::ostream& operator<<(std::ostream& os, const comm_recv& recv);
  };

  class Parallel_comm {
  public:
    Parallel_comm(IB_verbs::Parser* prs, std::string logName, Online::PrintLevel logLevel);
    Parallel_comm(const Parallel_comm& src) = delete;
    Parallel_comm& operator=(const Parallel_comm& src) = delete;
    Parallel_comm(Parallel_comm&& src);
    Parallel_comm& operator=(Parallel_comm&& src);
    ~Parallel_comm();
    void ibKillBlocking();
    int ibInit(IB_verbs::Parser* ibprs);
    int getTotalProcesses();
    int getProcessID();
    int ibDeregMRs();
    int send(const std::vector<comm_send>& sends);
    int receive(const std::vector<comm_recv>& recvs);
    int addMR(char* bufPtr, size_t bufSize);
    int ibBarrier();
    int ibScatterV(char* data, size_t n_elem, size_t elem_size, std::vector<int>& dsts);
    int ibBroadcastV(char* data, size_t n_elem, size_t elem_size, std::vector<int>& dsts);
    int ibGatherBlockV(char* data, int* data_sizes, int* data_idxs, size_t elem_size, std::vector<int>& remotes);
    std::vector<uint32_t>
    ibGatherV(char* data, int* data_sizes, int* data_idxs, size_t elem_size, std::vector<int>& remotes, int& ret_val);
    int ibTestRecvs(std::vector<uint32_t>& recvs);
    EB::Logger logger;

  private:
    IB_verbs::IB _ibObj;
    int _ibInit(IB_verbs::Parser* ibprs);
    int _ibDestroy();
    int _sendV(const std::vector<comm_send>& sends);
    std::vector<uint32_t> _receiveV(const std::vector<comm_recv>& recvs);
    int _waitSends(std::vector<uint32_t>& sends);
    int _waitRecvs(std::vector<uint32_t>& recvs);
    int _syncSend(int dest);
    uint32_t _syncRecv(int src);
  };
} // namespace EB

#endif // PARALLEL_COMM_H
