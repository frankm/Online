#ifndef UNIT_HPP
#define UNIT_HPP

namespace Online {

  class Unit {
  public:
    Unit() = default;

  protected:
    int rank;
    int worldSize;
  };
} // namespace Online
#endif
