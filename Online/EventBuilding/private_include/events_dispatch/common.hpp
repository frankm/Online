#ifndef COMMON_HPP
#define COMMON_HPP

#include <assert.h>
#include <ctime>
#include <malloc.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/timerfd.h>
#include <unistd.h>
#include <utility>
#include <sstream>
#include <string>

/// CONSTANTS

namespace DISPATCH {
  const int BITS_PER_BYTE = 8;
  const bool DO_WARMUP = false;

  // init variables
  // const size_t M_SIZE = (4 * 1024 * 1024);
  //	const size_t INIT_AMOUNT = 222220288;
  //	const size_t INIT_TRANS = 2155872256;
  //	const size_t FULL_TRANS = INIT_AMOUNT / M_SIZE;
  //	const size_t REMAINDER = INIT_AMOUNT % M_SIZE;

  const unsigned int BU_RU_DATA_TAG = 10000;
  const unsigned int PRINT_TAG = 10001;
  const unsigned int BU_RU_SIZE_TAG = 10002;
  const unsigned int BU_RU_DATA_REMAINDER_TAG = 10003;
  const unsigned int MU_TAG = 10004;
  const unsigned int MU_MEPS_TAG = 10005;
  const unsigned int OU_FU_WARMUP_TAG = 10006;
  const unsigned int MU_FU_ALLOC_TAG = 10008;
  // maximal  actual number of records of data blobs to be stored in builder units
  // (data producers)
  // These buffers hold blobs of data must be transmitted to the data consumers
  // Typically equals 1 and is cyclically used
  const unsigned int MAX_BUFFER_COUNT_BU = 1;

  // BUFFER SIZE
  const size_t BUFFER_RECORD_SIZE_MB = 12 * 1024;

  // MPI message size for warmup
  const int WARMUP_MPI_SIZE = 512 * 1024 * 1024;

  // sending period in microseconds
  const unsigned int PERIOD = 10000;

  // how many times repeat the transmissions
  const unsigned int REPEAT_FOR_EVAL = 4096;

  // time lapse in seconds to display BUs / FUs throughput in ms
  const unsigned int PROBE_TP_LAPSE = 1000;

  // MAX parallel BU credits
  // defines how many data consumers can receive in parallel from a given data
  // producer
  const int MAX_BU_CREDITS = 1;

  // TODO. Not yet implemented
  // For now, FUs receive from a single BU at once
  const int MAX_FU_CREDITS = 2;

  // MICROSECONDS IN A SECOND
  const long int US_S = 1e6;

  // MILISECONDS IN A SECOND
  const long int MS_S = 1000;

  // THRESHOLD BUFFER OCCUPANCY FOR BU
  // MAXIMAL NUMBER OF OCCUPIED RECORDS IN A DATA PRODUCER BUFFER
  // UPON GENERTAION OF NEW EVENT, IF THIS THRESOLD EXCEEDED, WARNING IS SIGNALLED
  // THIS THRESHOLD MUST NOT BE EXCEEDED THROUGHOUT THE WHOLE OPERATION
  const int THRESHOLD_BU_QUEUE_BUFFER_OCCUPANCY = 1;

  // once in how many iterations probe time on BU
  // tuned if system calls impact performance or event generation interval jitters
  const unsigned int PROBE_EVERY_IT = 1000;

  // TIME IN US defining interval how often data producer generates a blob of data
  const int64_t TIME_ELAPSED_FOR_GENERATION = 1000000;

  // value used to convert GiB to GB
  const double GIB_TO_GB_RATIO = (1024.0 / 1000.0) * (1024.0 / 1000.0) * (1024.0 / 1000.0);

  const unsigned int MB_IN_GRANT = 128;
  const size_t PAGE_SIZE = 4096;
  const int MAX_MEPS = 518144;

  // meps constants
  const int PWORDS_FIELD_OFFSET = 4;
  const int BYTES_IN_WORD_32 = 4;
  const int MAX_MEPS_IN_ONE_RUN = 1;

  /// PARAMETERS

  // 80 Gbit/s was the value for the 14 BUs and 18 FUs
  // How manyEvents
  // For 128 MB (1 Gbit) transmisison grants this value also means events
  // generation throughput in Gbit/s.
  // Throughput is defined as:
  // Generated_data =
  // Generation_period = TIME_ELAPSED_FOR_GENERATION
  // Throughput = Generated_data / Generation_period
  extern unsigned int gibit_events_in_one_quantum;

  // Events Manager holds rank 0. THEN THERE ARE BUS - DATA PRODUCERS, THEN THERE
  // ARE FUS - DATA CONSUMERS
  // How Many Filter Units - DATA CONSUMERS
  extern int how_many_fus;

  // How Many Builder Units - DATA PRODUCERS
  extern int how_many_ous;

  // Total Number of MPI processes, Including Builder Units, Filter Units and one
  // Events Manager
  //	extern int how_many_processes;

  // Total number of MPI processes
  //	extern int total_processes_no;

  // transmitting window length
  // a single transmission from a data producer to data consumer is implemented by
  // multiple MPI_ISends
  // WINDOW LENGTH defines a number of parallel MPI_ISends
  extern int window_length;

  // total number of MPI_ISends invoked per single BU->FU transmission grant
  // ENTRY_SIZE_MB * BUNDLED_TRANSMISSIONS  defines the granted BU->FU
  // transmission size
  // (PER_BUFFER_ENTRIES) / (BUNDLED_TRANSMISSIONS) defines the grant number
  // needed for a single generated BU data blob
  extern size_t bundled_transmissions;

  // buffer size for single data portion in readout-to-builder units transmission
  // 128 MB transmission grant is a reference
  extern size_t entry_size_kb;

  extern size_t entry_size_b;

  // defines ratio of amount of generated data in data producer versus amount of
  // consumed data in producers
  // typically defined by link  links speed ratio
  extern unsigned int fu_to_ou_buffer_and_throughput_ratio;

  // to set the generation type
  extern char distr_type;

  // maximal simulated data consumer capacity for records of a blobs of data
  // used to evaluate impact of processing time and consumers blocking on the
  // overall transmisisons
  extern unsigned int max_simulated_buffer_count_fu;

  extern bool perform_data_check;

  // generate constant or variable size data
  extern bool constant_size;

  // use MPI barriers when not running via WinCC
  extern bool use_debug_mpi_barriers;

  /// STRUCTURES

  enum fu_status { TRANSMISSION_DONE, FREE_DATA_UPDATE };

  struct mpi_fu_ready_msg {
    int rank;
    fu_status status;
  };

  // Message sent from Builder Unit to Event Manager to signal readiness of data
  struct mpi_bu_ready_msg {
    int rank;
    size_t untransmitted_meps_number;
  };

  // Message sent from Event Manager to Builder Unit to signal grant to a Filter
  // Unit
  struct mpi_bu_transmission_grant_msg {
    int send_to_which_fu;
  };

  // Message to exchange data size transmitted from BU to FU
  struct mpi_bu_fu_mep_size_msg {
    int sender_rank;
    size_t size;
  };

  struct current_transmission_metadata {
    int full_transmissions_no;
    int remainder_size;
  };

  struct fu_processing_entry {
    size_t processing_time;
    size_t mep_size;
  };

  struct mu_fu_alloc_msg {
    bool success;
    size_t size;
  };

  // meps structure
  typedef std::pair<char*, size_t> mep_entry;

  // get shmem name
  std::stringstream get_shmem_name(std::string shmem_name, int rank);
} // namespace DISPATCH

#endif
