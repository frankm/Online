#include "circular_buffer_writer.hpp"
#include <stdexcept>
#include "EventBuilding/tools.hpp"
#include <unistd.h>
#include <sys/stat.h> /* For mode constants */
#include <fcntl.h>    /* For O_* constants */
#include <sys/mman.h>
#include <cstring>
#include <sstream>
#include <string>
#include <memory>
#include <iostream>
#include <tuple>

template<class T, class B>
EB::Circular_buffer_writer<T, B>::~Circular_buffer_writer()
{}

template<class T, class B>
std::vector<std::tuple<void*, size_t>> EB::Circular_buffer_writer<T, B>::get_full_buffer()
{
  std::vector<std::tuple<void*, size_t>> ret_val;
  ret_val.emplace_back(this->_backend.get_buffer());
  return ret_val;
}

template<class T, class B>
T* EB::Circular_buffer_writer<T, B>::try_write_next_element(size_t size)
{
  if (!this->is_set()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " on an invalid buffer";
    throw std::logic_error(err_mess.str());
  }

  T* ret_val = NULL;

  bool fit = false;

  update_local_status();

  uintptr_t write_ptr = this->_backend._local_buffer_status.write_ptr();
  uintptr_t buffer_ptr = reinterpret_cast<uintptr_t>(this->_backend._buffer);

  size_t padded_size = size + get_padding(size, 1 << this->_backend._local_buffer_status.alignment);

  if ((padded_size + sizeof(T)) <= this->_backend._local_buffer_status.tail_free_space()) {
    fit = true;
  } else if (padded_size <= this->_backend._local_buffer_status.head_free_space()) {
    reinterpret_cast<T*>(write_ptr + buffer_ptr)->set_wrap();
    this->_backend._local_buffer_status.toggle_write_wrap();
    write_ptr = 0;
    fit = true;
  }

  if (fit) {
    ret_val = reinterpret_cast<T*>(write_ptr + buffer_ptr);

    write_ptr += padded_size;
    this->_backend._local_buffer_status.set_write_ptr(write_ptr);
    ret_val = new (ret_val) T();
  }

  return ret_val;
}

template<class T, class B>
void EB::Circular_buffer_writer<T, B>::write_complete()
{
  if (!this->is_set()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " on an invalid buffer";
    throw std::logic_error(err_mess.str());
  }

  asm volatile("mfence" ::: "memory");
  this->_backend._remote_buffer_status->write_stat = this->_backend._local_buffer_status.write_stat;
}

template<class T, class B>
void EB::Circular_buffer_writer<T, B>::write_discard()
{
  if (!this->is_set()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " on an invalid buffer";
    throw std::logic_error(err_mess.str());
  }

  update_local_status();
}

template<class T, class B>
void EB::Circular_buffer_writer<T, B>::update_local_status()
{
  if (!this->is_set()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " on an invalid buffer";
    throw std::logic_error(err_mess.str());
  }
  this->_backend._local_buffer_status.read_stat = this->_backend._remote_buffer_status->read_stat;
}

template<class T, class B>
int EB::Circular_buffer_writer<T, B>::get_src_id() const
{
  return this->_backend._local_buffer_status.id;
}

template<class T, class B>
size_t EB::Circular_buffer_writer<T, B>::free_space()
{
  if (!this->is_set()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " on an invalid buffer";
    throw std::logic_error(err_mess.str());
  }

  update_local_status();

  return std::max(
    this->_backend._local_buffer_status.tail_free_space(), this->_backend._local_buffer_status.head_free_space());
}
