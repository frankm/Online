/**
 * @file parser.hpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief Parser for InfiniBand Library
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */
#pragma once
#if !defined(PARSER_H)
#define PARSER_H
#include <fstream>
#include <vector>
#include <string>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <infiniband/verbs.h>
#include <iostream>
#include "proc.hpp"
#include "ArduinoJson.h"
#include <future>
#include <stdexcept>

namespace IB_verbs {
  class Parser {
  private:
    int _globalProc = 0;
    int _ipPort = 0;
    int _totalProcs = 0;
    int _ibNumaNode = -1;
    int _ibDevNum = -1;
    std::string _ibDevString = "";
    std::vector<sockaddr_in> _hostvec;
    std::string _myHostName = "";
    std::string _myIP = "";
    int _getMyNetInfo();
    std::string _getIpByHostname(std::string hostname);
    void _readHostFile(const char* filename, const int nProc);
    void _readHostFile(const char* filename);
    void _getMyIbDevInfo();

  public:
    Parser();
    Parser(const char* filename);
    Parser(const char* filename, const int rankid);
    ~Parser();
    std::string getMyHostname() const;
    std::string getMyIp() const;
    int getTotalProcesses() const;
    int getIpPort() const;
    std::string getIbDevString() const;
    // TODO getters should be const
    int getIbNumaNode() const;
    int getIbDevNum() const;
    int getProcessId() const;
    std::vector<sockaddr_in> getHostList();
  };
} // namespace IB_verbs

#endif // PARSER_H
