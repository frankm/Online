/**
 * @file proc.hpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief Process Class
 * @version 0.1.1
 * @date 16/11/2020
 *
 * @copyright Copyright (c) 2020
 *
 */

#pragma once
#if !defined(PROC_H)
#define PROC_H
#include <fstream>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <infiniband/verbs.h>
#include <iostream>
#include <memory>
#include <string>

namespace IB_verbs {
  class Proc {
  private:
    /* data */
  public:
    struct MRInfo {
      uint32_t rkey;
      uintptr_t addr;
    };
    int sockFd;
    std::string ibDevString;
    sockaddr_in sockAddr;
    int ibDev;
    uint16_t local_lid;
    uint16_t remote_lid;
    uint32_t remote_qp_num;      // must be 32 bit
    uint32_t remote_sync_qp_num; // must be 32 bit
    uint8_t qp_rdma;
    uint8_t next_qp;
    ibv_qp* qp;
    ibv_qp* sync_qp;
    MRInfo rdma;
    Proc(/* args */);
    Proc(Proc&&) = default;
    Proc& operator=(Proc&&) = default;
    Proc(const Proc&) = delete;
    Proc& operator=(const Proc&) = delete;
    ~Proc();
  };

  inline Proc::Proc(/* args */)
  {
    this->sockFd = 0;
    memset(&this->sockAddr, 0, sizeof(sockaddr_in));
    this->ibDevString = "";
    this->ibDev = 0;
    this->local_lid = 0;
    this->remote_lid = 0;
    this->next_qp = 0;
    this->qp = NULL;
    this->sync_qp = NULL;
  }

  inline Proc::~Proc() {}
} // namespace IB_verbs
#endif // PROC_H
