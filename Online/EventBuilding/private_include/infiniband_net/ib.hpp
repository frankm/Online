/**
 * @file ib.hpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief InfiniBand Communication Library
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */

#ifndef IB_H
#define IB_H

constexpr int PARALLEL_WRS = 10;

#define VERSION "0.1.7"
#include <infiniband/verbs.h>
#include <vector>
#include <array>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include "sock.hpp"
#include "parser.hpp"
#include <thread>
#include <mutex>
#include <atomic>
#include <unordered_map>
#include <future>
#include <stdexcept>
#include <errno.h>

// #define IB_DUMP_FILE 1

/**
 * @class ib ib.hpp
 * @brief InfiniBand Communication Library
 *
 * This library implements an high-level API to
 * enable InfiniBand communication between machines.
 * It is written in InfiniBand verbs in order to
 * have maximum customization and achieve best performance.
 *
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @date 24/03/2021
 */

namespace IB_verbs {
  class IB {
  public:
    /**
     * @brief Construct a new empty ib object
     *
     */
    IB();
    /**
     * @brief Default move constructor
     *
     * @param src The ib object to move
     */
    IB(IB&& src);
    /**
     * @brief Construct a new ib object with host file and process number
     * Construct a new ib object and call ibParseHosts method in
     * order to configure the object.
     * @param filename Host file formatted in JSON.
     * @param nProc Number of the global process, start from 0.
     */
    IB(const char* filename, const int nProc);
    /**
     * @brief Construct a new ib object with host file
     * Construct a new ib object and call ibParseHosts method in
     * order to configure the object.
     * Host id is determined by reading the environment variable UTGID.
     * @param filename Host file formatted in JSON.
     */
    IB(const char* filename);
    /**
     * @brief Host file parser
     * Parse a Host file formatted in JSON where the hostname, TCP port
     * and IB device are assigned to every process.
     *
     * @param filename Host file formatted in JSON.
     * @param nProc Number of the global process, start from 0.
     * @return int
     */
    int ibParseHosts(const char* filename, const int nProc);
    /**
     * @brief Host file parser
     * Parse a Host file formatted in JSON where the hostname, TCP port
     * and IB device are assigned to every process.
     * Localhost is determined by reading the environment variable UTGID
     * @param filename Host file formatted in JSON.
     * @return int
     */
    int ibParseHosts(const char* filename);
    /**
     * @brief Initialization of the ib object
     * Open IB device, set protection domain, set completion queues,
     * set TCP server for receiving queue pairs on a separate thread.
     * Send QP information to all the hosts, wait to receive all QP info,
     * set the QPs to Ready-To-Send, initialize sync requests, start sync thread,
     * compute barrier host LUT.
     */
    void ibInit();
    /**
     * @brief Register a Memory Region
     * Register a Memory Region to the IB device.
     * The MR is defined by a pointer and a size.
     * MRs are saved in a list, so this method
     * has to be called only once.
     * The memory must be aligned in sizes of 4096 B.
     * @param bufPtr Pointer to the memory.
     * @param bufSize Size of the memory.
     * @return ibv_mr* Pointer to the MR object. NULL if error.
     */
    ibv_mr* ibAddMR(char* bufPtr, size_t bufSize);
    /**
     * @brief Deregister a Memory Region
     * Deregister a Memory Region from the IB device.
     * To indentify the MR, the pointer and size must be passed.
     * It also removes the MR from the list.
     * ibAddMr must be called in order to use this memory again.
     * @param bufPtr Pointer to the memory.
     * @param bufSize Size of the memory.
     * @return int 0 for success, -1 for error.
     */
    int ibRemoveMR(char* bufPtr, size_t bufSize);
    /**
     * @brief Receive data from a remote source
     * Post a Receive Request (RR) and retreive a Work ID associated
     * to this request.
     * Polling of the Receive Completion Queue must be done in
     * order to know the status of the request.
     * This method is non-blocking.
     * @param bufPtr Pointer to the buffer where incoming data will be written.
     * @param bufSize Size of the buffer for the incoming data.
     * @param sender Source ID, corresponds to the position in the host file.
     * @return uint32_t Work ID associated with this RR.
     */
    uint32_t ibRecv(char* bufPtr, uint32_t bufSize, uint32_t sender);
    /**
     * @brief Send data to a remote destination
     * Post a Send Request (SR) and retreive a Work ID associated
     * to this request.
     * Polling of the Send Completion Queue must be done in
     * order to know the status of the request.
     * This method is non-blocking.
     * @param bufPtr Pointer to the buffer to be sent.
     * @param bufSize Size of the buffer.
     * @param dest Desitination ID, corresponds to the position in the host file.
     * @return uint32_t Work ID associated with this SR.
     */
    uint32_t ibSend(char* bufPtr, uint32_t bufSize, uint32_t dest);
    /**
     * @brief Receive data from a remote source (blocking)
     * Post a Receive Request (RR), poll the Receive Completion Queue
     * order to retreive the status of the request.
     * This method is blocking.
     * @param bufPtr Pointer to the buffer where incoming data will be written.
     * @param bufSize Size of the buffer for the incoming data.
     * @param sender Source ID, corresponds to the position in the host file.
     * @return int 0 for success, -1 for error.
     */
    int ibBlockRecv(char* bufPtr, uint32_t bufSize, uint32_t sender);
    /**
     * @brief Send data to a remote destination (blocking)
     * Post a Send Request (SR), poll the Send Completion Queue
     * order to retreive the status of the request.
     * This method is blocking.
     * @param bufPtr Pointer to the buffer to be sent.
     * @param bufSize Size of the buffer.
     * @param dest Desitination ID, corresponds to the position in the host file.
     * @return int 0 for success, -1 for error.
     */
    int ibBlockSend(char* bufPtr, uint32_t bufSize, uint32_t dest);
    /**
     * @brief Blocking write to a remote destitation via RDMA
     * Write to a remote destination memory region via RDMA opcode.
     * Prior to this, mr info must be exchanged using the appropriate method.
     * The transfer completion can be signaled by a SR by setting the sendComplete to true.
     * Transfer completion will be transmitted using the sync QP, to retrieve the result
     * use ibWaitSyncRecv(RDMA_OP_COMPLETE, src).
     * @param bufPtr pointer to the send buffer.
     * @param bufSize size of the region.
     * @param dest Destination ID.
     * @param sendComplete true if completion must be signaled to the remote, false otherwise.
     * @return int 0 for success, -1 for error.
     */
    int ibBlockRDMAWrite(char* bufPtr, uint32_t bufSize, uint32_t dest, bool sendComplete);
    /**
     * @brief Write to a remote destitation via RDMA
     * Write to a remote destination memory region via RDMA opcode.
     * Prior to this, mr info must be exchanged using the appropriate method.
     * @param bufPtr pointer to the send buffer.
     * @param bufSize size of the region.
     * @param dest Destination ID.
     * @return uint32_t RDMA operation wrId.
     */
    uint32_t ibRDMAWrite(char* bufPtr, uint32_t bufSize, uint32_t dest);
    /**
     * @brief Blocking read from a remote destitation via RDMA
     * Read from a remote destination memory region via RDMA opcode.
     * Prior to this, mr info must be exchanged using the appropriate method.
     * The transfer completion can be signaled by a SR by setting the sendComplete to true.
     * Transfer completion will be transmitted using the sync QP, to retrieve the result
     * use ibWaitSyncRecv(RDMA_OP_COMPLETE, src).
     * @param bufPtr pointer to the region where incoming data is written to.
     * @param bufSize size of the region.
     * @param dest Destination ID.
     * @param sendComplete true if completion must be signaled to the remote, false otherwise.
     * @return int 0 for success, -1 for error.
     */
    int ibBlockRDMARead(char* bufPtr, uint32_t bufSize, uint32_t dest, bool sendComplete);
    /**
     * @brief Read to a remote destitation via RDMA
     * Read from a remote destination memory region via RDMA opcode.
     * Prior to this, mr info must be exchanged using the appropriate method.
     * @param bufPtr pointer to the region where incoming data is written to.
     * @param bufSize size of the region.
     * @param dest Destination ID.
     * @return uint32_t RDMA operation wrId.
     */
    uint32_t ibRDMARead(char* bufPtr, uint32_t bufSize, uint32_t dest);
    /**
     * @brief Receive RDMA MR info
     * Exchange Memory Region address and key with a remote source
     * to initialize the RDMA transfer.
     * @param bufPtr pointer to the region where RDMA is executed.
     * @param bufSize size of the region where RDMA is executed.
     * @param src Source ID
     * @return int 0 for success, -1 for error.
     */
    int ibRecvRDMAInfo(char* bufPtr, uint32_t bufSize, uint32_t src);
    /**
     * @brief Send RDMA MR info
     * Exchange Memory Region address and key with a remote source
     * to initialize the RDMA transfer.
     * @param bufPtr pointer to the region where RDMA is executed.
     * @param bufSize size of the region where RDMA is executed.
     * @param src Source ID
     * @return int 0 for success, -1 for error.
     */

    int ibSendRDMAInfo(char* bufPtr, uint32_t bufSize, uint32_t dest);
    /**
     * @brief Check the status of a Send Request (blocking)
     * Poll the Send Completion Queue and wait
     * the completion of the SR.
     * This method is blocking.
     * @param wrId The work ID of the Send Request.
     * @return int 0 for success, -1 for error.
     */
    int ibWaitSendCQ(uint32_t wrId);
    /**
     * @brief Check the status of a Receive Request (blocking)
     * Poll the Receive Completion Queue and wait
     * the completion of the RR.
     * This method is blocking.
     * @param wrId The work ID of the Receive Request.
     * @return int 0 for success, -1 for error.
     */
    int ibWaitRecvCQ(uint32_t wrId);
    /**
     * @brief Check the status of a Send Request
     * Poll the Send Completion Queue and check if
     * the SR has been completed with success.
     * This method is non blocking.
     * @param wrId The work ID of the Send Request.
     * @return int 1 for not found, 0 for success, -1 for error.
     */
    int ibTestSendCQ(uint32_t wrId);
    /**
     * @brief Check the status of a Receive Request
     * Poll the Receive Completion Queue and check if
     * the RR has been completed with success.
     * This method is non blocking.
     * @param wrId The work ID of the Receive Request.
     * @return int 1 for not found, 0 for success, -1 for error.
     */
    int ibTestRecvCQ(uint32_t wrId);
    /**
     * @brief Wait a vector of send requests to be completed
     * Poll the Send Completion Queue until all the SR in wrs
     * have been completed.
     * @param wrs vector of work ids.
     * @return int 0 for success, -1 for error.
     */
    int ibWaitAllSends(std::vector<uint32_t>& wrs);
    /**
     * @brief Wait a vector of receive requests to be completed
     * Poll the Receive Completion Queue until all the RR in wrs
     * have been completed.
     * @param wrs vector of work ids.
     * @return int 0 for success, -1 for error.
     */
    int ibWaitAllRecvs(std::vector<uint32_t>& wrs);
    /**
     * @brief Test a vector of Send Requests for completion
     * Poll the Send Completion Queue and returns a vector reporting
     * the status of the SRs present in wrs.
     * @param wrs vector of work ids.
     * @return std::vector<int> vector reporting the status of
     * SRs, 1 for not found, 0 for success, -1 for error.
     */
    std::vector<int> ibTestAnySends(std::vector<uint32_t>& wrs);
    /**
     * @brief Test a vector of Receive Requests for completion
     * Poll the Receive Completion Queue and returns a vector reporting
     * the status of the RRs present in wrs.
     * @param wrs vector of work ids.
     * @return std::vector<int> vector reporting the status of
     * RRs, 1 for not found, 0 for success, -1 for error.
     */
    std::vector<int> ibTestAnyRecvs(std::vector<uint32_t>& wrs);
    /**
     * @brief Test a vector of receive requests to be completed
     * Poll the Receive Completion Queue until all the RR in wrs
     * have been completed. It deletes completed WRs from the
     * given vector.
     * @param wrs vector of work ids.
     * @return int 1 for not found, 0 for success, -1 for error.
     */
    int ibTestAllRecvs(std::vector<uint32_t>& wrs);
    /**
     * @brief Test a vector of Send Requests for completion (at least one)
     * Poll the Send Completion Queue and returns a vector reporting
     * the status of the SRs present in wrs.
     * This method wait for at least one requesto to be successful.
     * @param wrs vector of work ids.
     * @return std::vector<int> vector reporting the status of
     * SRs, 1 for not found, 0 for success, -1 for error.
     */
    std::vector<int> ibWaitAnySends(std::vector<uint32_t>& wrs);
    /**
     * @brief Test a vector of Receive Requests for completion (at least one)
     * Poll the Receive Completion Queue and returns a vector reporting
     * the status of the RRs present in wrs.
     * This method wait for at least one requesto to be successful.
     * @param wrs vector of work ids.
     * @return std::vector<int> vector reporting the status of
     * RRs, 1 for not found, 0 for success, -1 for error.
     */
    std::vector<int> ibWaitAnyRecvs(std::vector<uint32_t>& wrs);
    /**
     * @brief Immediate codes for synchronization
     *
     */
    enum immediate_t : uint32_t { NO_IMM_DATA, BARRIER, ONE_TO_ONE, RDMA_OP_COMPLETE };
    /**
     * @brief Send a sync message
     * Post a zero-byte with immediate SR to the sync QP.
     * Used to synchronize between processes.
     * @param imm_code immediate code to send (immediate_t)
     * @param dest remote host process id.
     * @return uint32_t work id associated with this SR.
     */
    uint32_t ibSendSync(immediate_t imm_code, uint32_t dest);
    /**
     * @brief Retrieve the status of a Sync SR
     * Poll the sync send CQ to retrieve the status
     * of a specified SR.
     * @param wrId The work id of the SR to test.
     * @return int 1 for not found, 0 for success, -1 for error.
     */
    int ibTestSyncSend(uint32_t wrId);
    /**
     * @brief Wait the completion of a Sync SR
     * Poll the sync send CQ and wait the specified SR to be
     * completed.
     * @param wrId The work id of the SR to check.
     * @return int 0 for success, -1 for error.
     */
    int ibWaitSyncSend(uint32_t wrId);
    /**
     * @brief Wait the completion of a vector of Sync SRs
     * Poll the sync send CQ and wait the specified SRs to be
     * completed.
     * @param wrs The work ids vector of the SR to check.
     * @return int 0 for success, -1 for error.
     */
    int ibWaitAllSyncSends(std::vector<uint32_t>& wrs);
    /**
     * @brief Post a RR for a sync message
     * Post a zero-byte with immediate RR to the sync QP.
     * Used to synchronize between processes.
     * @param src remote host process id.
     * @return uint32_t work id associated with this SR.
     */
    uint32_t ibRecvSync(uint32_t src);
    /**
     * @brief Check for a sync message
     * Check if a sync message from src has arrived and
     * the immediate code corresponds.
     * @param imm_code immediate_t code to check.
     * @param src remote host process id.
     * @return int 1 for not found, 0 for success, -1 for error.
     */
    int ibTestSyncRecv(immediate_t imm_code, uint32_t src);
    /**
     * @brief Wait for a sync message
     * Wait a sync message from src with the immediate code specified.
     * @param imm_code immediate_t code to check.
     * @param src remote host process id.
     * @return int 0 for success, -1 for error.
     */
    int ibWaitSyncRecv(immediate_t imm_code, uint32_t src);
    /**
     * @brief Wait a sync message vector to be completed
     * Wait until all the sync messages in the vector are completed.
     * @param wrs vector of a 2 uint32_t array.
     * [0] is immediate code, [1] is remote host id.
     * @return int 0 for success, -1 for error.
     */
    int ibWaitAllSyncRecvs(std::vector<std::array<uint32_t, 2>>& wrs);
    /**
     * @brief types of barriers
     * TOURNAMENT barrier is an highly parallel implementation
     * for high scalability.
     * CENTRAL barrier is a simple All-to-One, One-to-All implementation.
     * does not scale well, useful for debugging.
     */
    enum barrier_t : int { TOURNAMENT, CENTRAL };
    /**
     * @brief Synchronize all processes.
     * The barrier waits for all processes to synchronize.
     * Every process must call the barrier in order to synchronize.
     * @param type Default is TOURNAMENT, CENTRAL is also available. (barrier_t)
     * @return int 0 for success, -1 for error.
     */
    int ibBarrier(barrier_t type = TOURNAMENT);
    /**
     * @brief Deregisters the allocated Memory Regions
     * deregisters the memory regions from the device and erase the LUT.
     * @return int 0 for success, -1 for error.
     */
    int ibDeregMRs();
    /**
     * @brief Destroys the ib object
     * Join open threads, eallocate MRs, destroy Queue Pairs, Protection Domain, Completion Queues.
     * Close the IB device.
     */
    void ibDestroy();
    /**
     * @brief Get the Process ID
     * Return the process ID, determined by the position in the host file.
     * @return int process ID.
     */
    int getProcessID() const;
    /**
     * @brief Get the total number of processes
     * Correspond to the host file array size.
     * @return int total number of processes.
     */
    int getTotalProcesses() const;
    /**
     * @brief Returns the InfiniBand device number
     * Get the infiniBand device number which correspond to the system name (e.g. 0 -> ib0, mlx5_0.)
     * @return int device number.
     */
    int ibGetDevNum() const;
    /**
     * @brief Returns the NUMA node associated with the device
     * Get the NUMA node associate with the InfiniBand device.
     * @return int NUMA node.
     */
    int ibGetNumaNode() const;
    /**
     * @brief Destroy the ib object
     * Join open threads, eallocate MRs, destroy Queue Pairs, Protection Domain, Completion Queues.
     * Close the IB device.
     */
    ~IB() noexcept;
    /**
     * @brief Kill all blocking methods
     * This command will signal all the blocking calls to die.
     */
    void ibKillBlocking();
    IB& operator=(IB&& src) noexcept;
    // delete copy constructor and copy assignment
    IB(const IB& src) = delete;
    IB& operator=(const IB& src) = delete; // copy assignment
    int ibSetHostList(Parser& hostParser);

  private:
    int _ibSetQP(Proc& host);
    int _getDevInfo();
    void _destroyQP(ibv_qp* qp);
    uint32_t _ibGetNextWrId();
    std::thread _QPrecv;
    std::promise<int> _ret;
    std::future<int> _rtf;
    // data methods
    uint32_t _ibRecv(ibv_mr* mr, char* bufPtr, uint32_t bufSize, uint32_t sender);
    uint32_t _ibSend(ibv_mr* mr, char* bufPtr, uint32_t bufSize, uint32_t dest);
    int _ibSendCQ(uint32_t wrId);
    int _ibRecvCQ(immediate_t imm_code, uint32_t wrId);
    int _pollSCQ();
    int _pollRCQ();
    // sync methods
    int _pollSyncRCQ();
    int _ibSync(uint64_t key);
    void _syncFunc(std::atomic<bool>& run);
    int _syncInit();
    // barrier methods
    int _runCentral();
    int _initTournament();
    int _runTournament();
    // for tournament barrier
    std::vector<uint32_t> _tournament_hlist;
    std::vector<std::array<uint32_t, 2>> _tournament_lut;
    bool _tournament_lonely = false;
    int _tournament_dst = -1;
    // global stuff
    int _devNum;
    std::string _devName = "";
    int _numaNode;
    uint8_t _portNum = 1;
    size_t _procNum;
    std::atomic<uint32_t> _wrId = 0;
    int _totalProcs;
    ibv_context* _ctx = NULL;
    ibv_pd* _pd = NULL;
    ibv_port_attr _port_attr;
    ibv_device_attr _dev_attr;
    ibv_device** _dev_list = NULL;
    std::vector<Proc> _hosts;
    std::atomic<bool> _kill_blocking{false};
    // DATA CQ objects
    ibv_cq* _sendcq = NULL;
    ibv_cq* _recvcq = NULL;
    std::unordered_map<uint32_t, ibv_wc> _wcsend;
    std::unordered_map<uint32_t, ibv_wc> _wcrecv;
    // SYNC CQ objects
    ibv_cq* _syncRcq = NULL;
    ibv_cq* _syncScq = NULL;
    std::unordered_map<uint64_t, ibv_wc> _wcsyncr;
    std::unordered_map<uint32_t, ibv_wc> _wcsyncs;
    std::unordered_map<uint32_t, size_t> _reverse_hosts;
    std::thread _syncThread;
    std::atomic<bool> _runsync{true};
    // Out-of-Band comms
    Sock _sockObj;
    // mutexes for inter-thread synchronization
    std::mutex _mtx_poll_r;
    std::mutex _mtx_poll_s;
    std::mutex _mtx_host;
    std::mutex _mtx_mr;
    std::mutex _mtx_sync;
    // memory region registers
    struct memreg {
      ibv_mr* _mr = NULL;
      char* _bufPtr = NULL;
      size_t _bufSize = 0;
    };
    std::vector<memreg> _memMap;
    // rdma operations
    uint32_t _RDMAop(char* bufPtr, uint32_t bufSize, uint32_t dest, bool opType /*true read, false write*/);

#ifdef IB_DUMP_FILE
    std::string _dump_file_path = "/scratch/eb_testing";
    std::ofstream _wc_dump_file;
    std::mutex _wc_dump_file_mutex;
    void _dump_wc(const struct ibv_wc& wc);
    std::ofstream _wr_dump_file;
    std::mutex _wr_dump_file_mutex;
    struct ib_wr {
      uint32_t qp_num;
      uint32_t src_qp;
      ibv_wr_opcode opcode;
      bool receive;
    };
    void _dump_wr(const ib_wr& wr);
#endif
  };
} // namespace IB_verbs

#endif // !IB_H