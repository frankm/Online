#ifndef SHMEM_BUFFER_READER_H
#define SHMEM_BUFFER_READER_H 1

#include "circular_buffer_reader.hpp"
#include "shared_mem_buffer_backend.hpp"
#include <cstddef>
#include <cstdint>
#include <deque>
#include <iostream>
#include <string>
#include <tuple>

namespace EB {
  template<class T>
  class Shmem_buffer_reader : public Circular_buffer_reader<T, Shared_mem_buffer_backend> {
  public:
    // alignment is explesser in 2^alignment min value 1
    Shmem_buffer_reader() {}
    Shmem_buffer_reader(const std::string& shm_name) :
      Circular_buffer_reader<T, Shared_mem_buffer_backend>(
        std::move(Shared_mem_buffer_backend(shm_name, false, false, false)))
    {}

    virtual ~Shmem_buffer_reader() {}
  };
} // namespace EB
#endif // SHMEM_BUFFER_READER_H
