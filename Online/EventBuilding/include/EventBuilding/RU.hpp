#ifndef RU_H
#define RU_H 1

#include <memory>
#include <string>
#include <list>
#include <mutex>
#include <atomic>

#include "Dataflow/DataflowComponent.h"
#include "MFP_tools.hpp"
#include "transport_unit.hpp"
#include "dummy_mfp_buffer.hpp"
#include "Parallel_Comm.hpp"
#include "bw_mon.hpp"
#include "timer.hpp"
#include "buffer_interface.hpp"
#include "pcie40_reader.hpp"
#include "file_writer.hpp"

namespace EB {
  using namespace Online;
  enum RU_buffer_types { dummy_MFP_buffer, shmem_MFP_buffer, PCIe40_MFP_buffer, PCIe40_frag_buffer };

  constexpr std::initializer_list<RU_buffer_types> all_RU_buffer_types =
    {dummy_MFP_buffer, shmem_MFP_buffer, PCIe40_MFP_buffer, PCIe40_frag_buffer};

  constexpr auto default_RU_buffer_type = RU_buffer_types::shmem_MFP_buffer;
  constexpr auto default_RU_buffer_size = 1;

  const std::string RU_buffer_type_to_string(RU_buffer_types type);

  std::ostream& operator<<(std::ostream& os, RU_buffer_types type);

  class RU : public Transport_unit {
  public:
    RU(const std::string& name, Context& framework);
    virtual ~RU();

    /// Service implementation: initialize the service
    virtual int initialize() override;
    /// Service implementation: start of the service
    virtual int start() override;
    /// Service implementation: stop the service
    virtual int stop() override;
    /// Service implementation: finalize the service
    virtual int finalize() override;

    /// Cancel I/O operations of the dataflow component
    virtual int cancel() override;
    /// Stop gracefully execution of the dataflow component
    virtual int pause() override;
    /// Incident handler implementation: Inform that a new incident has occurred
    virtual void handle(const DataflowIncident& inc) override;
    /// IRunable implementation : Run the class implementation
    virtual int run() override;

  private:
    // there is one buffer per associated data source
    std::vector<std::unique_ptr<EB::Buffer_reader<EB::MFP>>> _buffers;
    int _my_idx;
    std::atomic<bool> _send_MEPs = false;
    bool _end_of_run = true;
    bool _send_empty = false;
    // true if we received at least one MFP since start
    std::vector<bool> _received_data;

    // mpfs and sizes of every source for the full linear shift rotation
    std::vector<const char*> selected_MFPs;
    // number of loaded fragments per source
    std::vector<int> _n_frags_per_dst;
    std::vector<uint32_t> sizes;
    // std::vector<int>::const_iterator curr_dst;

    // properties
    // per RU parameters
    // TODO check if signed int is enough size_t is not supported by the framework
    // well it is not the buffer sizes property is now in GB
    std::vector<size_t> _buffer_sizes;
    std::vector<int> _prop_buffer_sizes;
    std::vector<bool> _write_to_file;
    std::vector<int> _PCIe40_ids;
    std::vector<std::string> _PCIe40_names;
    std::vector<int> _buffer_type;
    std::vector<int> _dummy_src_ids;
    int _SODIN_ID;
    std::string _SODIN_name;
    int _SODIN_stream;
    // global parameters
    // global parameters
    int _n_fragment;
    std::string _shmem_prefix;
    // TODO check if the MDF_filename can be global to all the RUs
    std::string _MDF_filename;
    std::string _out_file_prefix;
    int _n_MFPs;
    int _n_MFPs_to_file;
    int _stop_timeout;

    std::vector<int> _shift_offset;

    // internal states
    int check_buffers();
    int stream_select(int id);
    int stream_select(const std::string& name);
    int config_buffers();
    void config_dummy();
    void config_shmem();
    void config_PCIe40();
    void config_PCIe40_frag();
    int init_shift();
    int send_MFPs(int shift);
    int linear_shift();
    int send_sizes();
    int init_dummy_src_ids();
    int send_src_ids();
    int load_mfps();
    // preload a set on 0 sized NULL MFPs
    int load_empty();

    void reset_counters();

    // monitoring counter
    int _MFP_count = 0;
    // DF monitoring counters
    // TODO remove duplicates
    int _DF_events_out;
    int _DF_events_in;
    int _DF_events_err;

    // DEBUG counters
    int _run_loop_iteration;

    Bw_mon bw_monitor;

    Timer load_mpfs_timer;
    Timer send_sizes_timer;
    Timer linear_shift_timer;
    Timer send_timer;
    Timer sync_timer;
    Timer total_timer;

    std::timed_mutex _loop_lock;
    std::timed_mutex _start_lock;

    // file writer
    File_writer<EB::MFP> _file_writer;
    int _n_MFPs_written_to_file;
  };
} // namespace EB

#endif // RU_H
