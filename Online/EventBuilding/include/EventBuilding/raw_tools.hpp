#ifndef RAW_TOOLS_H
#define RAW_TOOLS_H 1

#include <unistd.h>
#include <stdint.h>
#include <iostream>
#include "EventBuilding/generic_block.hpp"
#include <unordered_map>
#include <map>
#include <iostream>

namespace EB {
  // raw block alignment in bytes
  constexpr size_t block_alignment = 4;
  constexpr uint16_t block_magic = 0xCBCB;

  constexpr uint16_t src_id_det_mask = 0x1;
  constexpr uint16_t src_id_fe_mask = 0xCBCB;

  // TODO this should come from LHCb
  enum BankType {
    L0Calo = 0,       //  0
    L0DU,             //  1
    PrsE,             //  2
    EcalE,            //  3
    HcalE,            //  4
    PrsTrig,          //  5
    EcalTrig,         //  6
    HcalTrig,         //  7
    Velo,             //  8
    Rich,             //  9
    TT,               // 10
    IT,               // 11
    OT,               // 12
    Muon,             // 13
    L0PU,             // 14
    DAQ,              // 15
    ODIN,             // 16
    HltDecReports,    // 17
    VeloFull,         // 18
    TTFull,           // 19
    ITFull,           // 20
    EcalPacked,       // 21
    HcalPacked,       // 22
    PrsPacked,        // 23
    L0Muon,           // 24
    ITError,          // 25
    TTError,          // 26
    ITPedestal,       // 27
    TTPedestal,       // 28
    VeloError,        // 29
    VeloPedestal,     // 30
    VeloProcFull,     // 31
    OTRaw,            // 32
    OTError,          // 33
    EcalPackedError,  // 34
    HcalPackedError,  // 35
    PrsPackedError,   // 36
    L0CaloFull,       // 37
    L0CaloError,      // 38
    L0MuonCtrlAll,    // 39
    L0MuonProcCand,   // 40
    L0MuonProcData,   // 41
    L0MuonRaw,        // 42
    L0MuonError,      // 43
    GaudiSerialize,   // 44
    GaudiHeader,      // 45
    TTProcFull,       // 46
    ITProcFull,       // 47
    TAEHeader,        // 48
    MuonFull,         // 49
    MuonError,        // 50
    TestDet,          // 51
    L0DUError,        // 52
    HltRoutingBits,   // 53
    HltSelReports,    // 54
    HltVertexReports, // 55
    HltLumiSummary,   // 56
    L0PUFull,         // 57
    L0PUError,        // 58
    DstBank,          // 59
    DstData,          // 60
    DstAddress,       // 61
    FileID,           // 62
    VP,               // 63
    FTCluster,        // 64
    VL,               // 65
    UT,               // 66
    UTFull,           // 67
    UTError,          // 68
    UTPedestal,       // 69
    HC,               // 70
    HltTrackReports,  // 71
    HCError,          // 72
    VPRetinaCluster,  // 73
    // Add new types here. Don't forget to update also RawBank.cpp
    LastType // LOOP Marker; add new bank types ONLY before!
  };

  const std::string ToString(const BankType);

  typedef std::pair<uint8_t, uint16_t> type_id_pair_type;

  #ifdef HAVE_PCIE40
  // TODO implement a proper conversion
  type_id_pair_type new_src_id_to_old(uint16_t src_id);

  uint16_t old_to_new(EB::type_id_pair_type src_id);

  uint16_t type_to_partition_id(const BankType b_type);
  #endif

  std::ostream& operator<<(std::ostream& os, const BankType);

  struct __attribute__((__packed__)) raw_header {
    uint16_t magic;
    uint16_t size;
    uint8_t type;
    uint8_t version;
    uint16_t src_id;

    size_t mem_size() const;
    size_t payload_mem_size() const;
    size_t payload_size() const;
    // this may be dangerous
    void* get_payload() { return reinterpret_cast<uint8_t*>(this) + sizeof(this); }

    bool is_valid() const { return (magic == block_magic); }

    #ifdef HAVE_PCIE40
    int get_sys_src_id() const;
    int get_src_id_num() const;

    void set_sys_src_id(uint16_t sys_src_id);
    void set_src_id_num(uint16_t src_id_num);
    #endif

    std::ostream& print(std::ostream& os) const;
    friend std::ostream& operator<<(std::ostream& os, const raw_header& header);
  };

  std::ostream& operator<<(std::ostream& os, const raw_header& header);

  class raw_block : public Generic_block {
  public:
    raw_header header;

    raw_block();
    raw_block(std::shared_ptr<char> payload, size_t size);

    // TODO data may be const void *
    int read_block(void* data);
    int read_block(void* data, void* buffer, size_t buffer_size);
  };


  struct type_src_id_hash {
    std::size_t operator()(const type_id_pair_type& p) const { return std::hash<int>{}(p.first << 16 | p.second); }
  };

  template<class F, class S>
  struct pair_less {
    std::size_t operator()(const std::pair<F, S>& lhs, const std::pair<F, S>& rhs) const
    {

      return (lhs.first != rhs.first) ? lhs.first < rhs.first : lhs.second < rhs.second;
    }
  };

  // typedef std::unordered_map<type_id_pair_type, raw_header*, type_src_id_hash> source_header_map_type;
  template<class value>
  class source_header_unordered_map_type : public std::unordered_map<type_id_pair_type, value, type_src_id_hash> {
  };

  template<class value>
  class source_header_map_type : public std::map<type_id_pair_type, value, pair_less<uint8_t, uint16_t>> {
  public:
    typedef type_id_pair_type key_type;
  };

} // namespace EB

#endif // RAW_TOOLS_H
