#include "options.hpp"
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <cstdlib>

EB::Options::Options(int argc, char** argv)
{
  _argc = argc;
  _argv = argv;
  add_option("help", 'h', "Print this help and exit", false, [this](const char* arg) {
    this->print_usage();
    exit(0);
  });
}

EB::Options::~Options()
{
  for (auto& elem : _options) {
    delete elem.name;
  }
}

// TODO add check for duplicated options

void EB::Options::add_option(
  const std::string& long_name,
  char short_name,
  const std::string& help,
  bool is_required,
  std::function<void(const char*)> user_funct,
  bool has_args)
{
  // we need deep copy
  char* long_name_copy = new char[long_name.size() + 1];
  long_name.copy(long_name_copy, long_name.size(), 0);
  long_name_copy[long_name.size()] = '\0';
  _options.emplace_back(option{long_name_copy, has_args, NULL, short_name});
  _lambdas.emplace_back(user_funct);

  _short_names.emplace_back(short_name);
  _required.emplace_back(is_required);
  _help.emplace_back(help);

  _max_text_aling = std::max(_max_text_aling, strlen(long_name_copy));
}

template<>
void EB::Options::add_option<int>(
  const std::string& long_name,
  char short_name,
  const std::string& help,
  bool is_required,
  int& out_var)
{
  auto funct = [&out_var](const char* arg) { out_var = atoi(arg); };

  add_option(long_name, short_name, help, is_required, funct, true);
}

template<>
void EB::Options::add_option<unsigned long int>(
  const std::string& long_name,
  char short_name,
  const std::string& help,
  bool is_required,
  unsigned long int& out_var)
{
  // TODO add error checking
  auto funct = [&out_var](const char* arg) { out_var = strtoul(arg, NULL, 0); };

  add_option(long_name, short_name, help, is_required, funct, true);
}

template<>
void EB::Options::add_option<double>(
  const std::string& long_name,
  char short_name,
  const std::string& help,
  bool is_required,
  double& out_var)
{
  auto funct = [&out_var](const char* arg) { out_var = atof(arg); };

  add_option(long_name, short_name, help, is_required, funct, true);
}

template<>
void EB::Options::add_option<std::string>(
  const std::string& long_name,
  char short_name,
  const std::string& help,
  bool is_required,
  std::string& out_var)
{
  auto funct = [&out_var](const char* arg) { out_var.assign(arg); };

  add_option(long_name, short_name, help, is_required, funct, true);
}

void EB::Options::add_flag(
  const std::string& long_name,
  char short_name,
  const std::string& help,
  bool is_required,
  bool& out_flag)
{
  out_flag = false;

  auto funct = [&out_flag](const char* arg) { out_flag = true; };

  add_option(long_name, short_name, help, is_required, funct, false);
}

void EB::Options::add_counter(
  const std::string& long_name,
  char short_name,
  const std::string& help,
  bool is_required,
  int& counter)
{
  counter = 0;

  auto funct = [&counter](const char* arg) { counter++; };

  add_option(long_name, short_name, help, is_required, funct, false);
}

bool EB::Options::parse_args()
{
  bool ret_val = true;
  // the list must be null terminated
  _options.emplace_back(option{0, 0, 0, 0});

  auto short_options = build_short_options();

  int index;
  int c;

  std::vector<bool> required_not_set(_required);

  while ((c = getopt_long(_argc, _argv, short_options.c_str(), _options.data(), &index)) != -1) {
    if (c == '?') {
      // error in option parsing
      ret_val = false;
    } else {
      if (c != 0) {
        auto it = std::find(_short_names.begin(), _short_names.end(), c);
        index = std::distance(_short_names.begin(), it);
      }
      apply_lambda(index);

      required_not_set[index] = false;
    }
  }

  // check if any required option is not set
  for (size_t k = 0; k < required_not_set.size(); k++) {
    if (required_not_set[k]) {
      ret_val = false;
      std::cerr << "error missing mandatory option" << std::endl;
      print_option(k);
    }
  }

  return ret_val;
}

std::string EB::Options::build_short_options()
{
  std::string ret_val;

  for (size_t k = 0; k < _short_names.size(); k++) {
    ret_val += _short_names[k];
    if (_options[k].has_arg != 0) {
      ret_val += ':';
    }
  }

  return ret_val;
}

void EB::Options::apply_lambda(int idx) { _lambdas[idx](optarg); }

void EB::Options::print_usage() const
{
  std::cerr << "USAGE:" << std::endl;
  std::cerr << _argv[0];
  for (size_t k = 0; k < _short_names.size(); k++) {
    if (!_required[k]) {
      std::cerr << " [";
    }
    std::cerr << " -" << _short_names[k];
    if (_options[k].has_arg) {
      std::cerr << " <" << _options[k].name << ">";
    }
    if (!_required[k]) {
      std::cerr << " ]";
    }
  }

  std::cerr << std::endl;
  for (size_t k = 0; k < _help.size(); k++) {
    print_option(k);
  }
}

void EB::Options::print_option(int idx) const
{
  std::cerr << " -" << _short_names[idx] << " --" << _options[idx].name
            << std::setw(_max_text_aling - strlen(_options[idx].name) + 2);
  if (_options[idx].has_arg) {
    // TODO align print properly
    std::cerr << "<" << _options[idx].name << ">";
  } else {
    std::cerr << " " << std::setw(_max_text_aling - strlen(_options[idx].name) + 2) << " ";
  }
  std::cerr << std::setw(_max_text_aling - strlen(_options[idx].name) + 2) << " " << _help[idx] << std::endl;
}