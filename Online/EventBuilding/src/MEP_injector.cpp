#include "MEP_injector.hpp"
#include "MFP_builder.hpp"
#include "mdf_reader.hpp"
#include "EventBuilding/tools.hpp"
#include "EventBuilding/tools.hpp"
#include <numa.h>
#include <stdexcept>
#include <sstream>
#include <algorithm>
#include <utility>
#include <unistd.h>

EB::MEP_injector::MEP_injector(
  const char* MDF_filename,
  size_t buffer_size,
  int numa_node,
  int packing_factor,
  int n_meps,
  Online::DataflowComponent::Context& context,
  const std::string& instance,
  const std::string& name,
  bool write_to_file,
  const std::string out_file_name,
  int n_meps_to_file) :
  _MDF_filename(MDF_filename),
  _internal_buffer_writer(buffer_size, numa_node), _external_buffer_writer(context, instance, name),
  _packing_factor(packing_factor), _n_MEPs(n_meps), _numa_node(numa_node), _copy_status(status_t::READY),
  _thread_exit(false)
{
  if (write_to_file) {
    _file_writer = std::move(File_writer<EB::MEP>(out_file_name));
    _n_meps_to_file = n_meps_to_file;
  } else {
    _n_meps_to_file = 0;
  }

  int err_check;
  err_check = preload_MEPs();
  if (err_check != 0) {
    std::stringstream err_mess;
    err_mess << "Unable to preload MEPs error code: " << err_check;
    throw std::runtime_error(err_mess.str());
  }
  _t_copy = std::thread(thread_copy, &_thread_exit, &_copy_status, &_t_write_ptr, &_t_mep_ptr);
}

EB::MEP_injector::~MEP_injector()
{
  _thread_exit = true;
  if (_t_copy.joinable()) {
    _t_copy.join();
  }
}

std::vector<std::tuple<void*, size_t>> EB::MEP_injector::get_full_buffer()
{
  return _internal_buffer_writer.get_full_buffer();
}

EB::MEP* EB::MEP_injector::try_write_next_element(size_t size)
{
  EB::MEP* ret_val = NULL;
  // this should be always re to READY at this point extra safety
  if (_copy_status.load() == status_t::READY) {
    ret_val = _external_buffer_writer.try_write_next_element(_MEPs[_curr_mep_idx]->bytes());
    if (ret_val != NULL) {

      _t_write_ptr = ret_val;
      _t_mep_ptr = _MEPs[_curr_mep_idx].get();
      _copy_status.store(status_t::PTR_READY, std::memory_order_release);

      if (_n_meps_to_file > 0) {
        _file_writer.write(_MEPs[_curr_mep_idx].get());
        _n_meps_to_file--;
      }

      _curr_mep_idx = (_curr_mep_idx + 1) % _MEPs.size();
      // The dummy buffer should always have space
      ret_val = _internal_buffer_writer.try_write_next_element(size);
    }
  }
  return ret_val;
}

void EB::MEP_injector::thread_copy(bool* exit, std::atomic<int>* status, void** write_ptr, EB::MEP** mep_ptr)
{
  while (!*exit) {
    while (status->load(std::memory_order_acquire) != status_t::PTR_READY) {
      // we try not to block
      if (*exit) {
        return;
      }
    }
    memcpy(*write_ptr, *mep_ptr, (*mep_ptr)->bytes());
    status->store(status_t::COPY_READY, std::memory_order_release);
  }
}

void EB::MEP_injector::write_complete()
{
  // If we are in ready the thread is not doing anything
  if (_copy_status.load() != status_t::READY) {
    while (_copy_status.load(std::memory_order_acquire) != status_t::COPY_READY) {
      // If the copy joins we try not to block
      // This is in principle not necessary with the current implementation
      if (_thread_exit) {
        return;
      }
    }
    _external_buffer_writer.write_complete();
    _copy_status.store(status_t::READY, std::memory_order_release);
  }
  _internal_buffer_writer.write_complete();
}

void EB::MEP_injector::write_discard()
{
  if (_copy_status.load() != status_t::READY) {
    while (_copy_status.load(std::memory_order_acquire) != status_t::COPY_READY) {
      // If the copy joins we try not to block
      // This is in principle not necessary with the current implementation
      if (_thread_exit) {
        return;
      }
    }
    _external_buffer_writer.write_discard();
    _copy_status.store(status_t::READY, std::memory_order_release);
  }
  _internal_buffer_writer.write_discard();
}

// no src id
int EB::MEP_injector::get_src_id() const { return 0; }

int EB::MEP_injector::preload_MEPs()
{
  int ret_val = 0;
  std::vector<EB::MDF_block> blocks;
  std::vector<std::shared_ptr<char>> MFPs;
  std::vector<size_t> buffer_sizes;

  std::vector<EB::offset_type> offsets;
  std::vector<EB::src_id_type> src_ids;
  size_t MFP_size = 300 * 1024 * _packing_factor;
  MDF_reader reader(_MDF_filename.c_str());
  EB::MFP_builder builder;

  while (!_write_ptrs.empty()) {
    _write_ptrs.pop();
  }
  _curr_mep_idx = 0;

  _MEPs.clear();
  _MEPs.reserve(_n_MEPs);
  for (size_t k = 0; k < _n_MEPs; k++) {
    blocks.clear();
    ret_val = reader.extract_events(blocks, _packing_factor);
    if (ret_val != 0) {
      break;
    }

    ret_val = builder.append_events(blocks);
    if (ret_val != 0) {
      break;
    }

    size_t n_sources = builder.get_source_number();
    buffer_sizes.resize(n_sources, MFP_size);
    // resize the temporary MFP buffers
    if (MFPs.size() != n_sources) {
      int old_size = MFPs.size();
      MFPs.resize(n_sources);
      for (size_t k = old_size; k < MFPs.size(); k++) {
        MFPs[k] = std::shared_ptr<char>(new char[MFP_size]);
      }
    }
    offsets.resize(n_sources, 0);
    src_ids.resize(n_sources, 0);

    ret_val = builder.build_MFPs(_packing_factor, MFPs, buffer_sizes);
    if (ret_val != 0) {
      break;
    }

    std::sort(MFPs.begin(), MFPs.end(), [](const auto a, const auto b) {
      return reinterpret_cast<EB::MFP*>(a.get())->header.src_id < reinterpret_cast<EB::MFP*>(b.get())->header.src_id;
    });

    auto current_mfp = reinterpret_cast<EB::MFP*>(MFPs[0].get());
    size_t header_size = EB::mep_header_mem_size(n_sources);
    size_t padded_size = current_mfp->bytes() + get_padding(current_mfp->bytes(), 4096);
    size_t total_size = EB::mep_header_mem_size(n_sources) + padded_size;
    offsets[0] = header_size / EB::MEP_WORD_SIZE;
    src_ids[0] = current_mfp->header.src_id;
    for (int k = 1; k < n_sources; k++) {
      current_mfp = reinterpret_cast<EB::MFP*>(MFPs[k].get());
      // For the offset calculation we use the size of the previous MFP
      offsets[k] = offsets[k - 1] + padded_size / EB::MEP_WORD_SIZE;
      src_ids[k] = current_mfp->header.src_id;
      padded_size = current_mfp->bytes() + get_padding(current_mfp->bytes(), 4096);
      total_size += padded_size;
    }

    _MEPs.emplace_back(std::shared_ptr<EB::MEP>(reinterpret_cast<EB::MEP*>(new char[total_size])));
    EB::MEP* curr_mep = _MEPs.back().get();
    if (curr_mep == NULL) {
      ret_val = ENOMEM;
      break;
    }

    if (_numa_node != -1) {
      numa_tonode_memory(_MEPs.back().get(), total_size, _numa_node);
    }

    curr_mep->set_magic_valid();
    curr_mep->header.n_MFPs = n_sources;
    curr_mep->header.p_words = total_size / EB::MEP_WORD_SIZE;
    memcpy(curr_mep->header.offsets(), offsets.data(), offsets.size() * sizeof(EB::offset_type));
    memcpy(curr_mep->header.src_ids(), src_ids.data(), src_ids.size() * sizeof(EB::src_id_type));

    for (size_t k = 0; k < n_sources; k++) {
      size_t size = (reinterpret_cast<EB::MFP*>(MFPs[k].get()))->bytes();
      memcpy(reinterpret_cast<void*>(curr_mep->at(k)), MFPs[k].get(), size);
    }
  }

  return ret_val;
}