#include "circular_buffer_status.hpp"

bool EB::Circular_buffer_status::is_empty() const { return (write_stat == read_stat); }

bool EB::Circular_buffer_status::is_full() const { return (write_ptr() == read_ptr()) && (read_stat != write_stat); }

bool EB::Circular_buffer_status::same_page() const { return (write_stat & wrap_mask) == (read_stat & wrap_mask); }

uintptr_t EB::Circular_buffer_status::write_ptr() const { return (write_stat & ptr_mask) << 1; }

uintptr_t EB::Circular_buffer_status::read_ptr() const { return (read_stat & ptr_mask) << 1; }

void EB::Circular_buffer_status::set_write_ptr(uintptr_t write_ptr)
{
  write_stat = (write_stat & wrap_mask) | ((write_ptr >> 1) & ptr_mask);
}

void EB::Circular_buffer_status::set_read_ptr(uintptr_t read_ptr)
{
  read_stat = (read_stat & wrap_mask) | ((read_ptr >> 1) & ptr_mask);
}

void EB::Circular_buffer_status::toggle_write_wrap()
{
  write_stat = write_stat ^ wrap_mask; // flip MSB
}

void EB::Circular_buffer_status::toggle_read_wrap()
{
  read_stat = read_stat ^ wrap_mask; // flip MSB
}

size_t EB::Circular_buffer_status::tail_free_space() const
{
  size_t ret_val;
  if (same_page()) {
    ret_val = size - write_ptr();
  } else {
    ret_val = read_ptr() - write_ptr();
  }

  return ret_val;
}

size_t EB::Circular_buffer_status::head_free_space() const
{
  size_t ret_val;
  if (same_page()) {
    ret_val = read_ptr();
  } else {
    // in this case there is no head space because the write ptr is chasing the read ptr
    ret_val = 0;
  }

  return ret_val;
}