#include "mdf_reader.hpp"
#include <system_error>

EB::MDF_reader::MDF_reader(const char* file_name)
{
  _input_fd = open(file_name, O_RDONLY);
  if (_input_fd < 0) {
    throw std::system_error(errno, std::generic_category(), file_name);
  }
}

bool EB::MDF_reader::set_file(const char* file_name)
{
  close(_input_fd);

  _input_fd = open(file_name, O_RDONLY);

  return (_input_fd >= 0);
}

int EB::MDF_reader::extract_event(EB::MDF_block& block) { return block.read_block(_input_fd); }

int EB::MDF_reader::extract_event(EB::MDF_block& block, void* buffer, size_t size)
{
  // return block.read_block(input_fd, buffer, size);
  // TODO implement this
  return 0;
}

int EB::MDF_reader::extract_events(std::vector<EB::MDF_block>& blocks, int n_events)
{
  int ret_val = 0;
  blocks.reserve(blocks.size() + n_events);
  for (int k = 0; (k < n_events) && (ret_val >= 0); k++) {
    blocks.emplace_back();
    ret_val = extract_event(blocks.back());
    if (ret_val != 0) {
      // if there is an error the last element of the blocks array is not valid so we remove it
      blocks.pop_back();
    }
  }
  return ret_val;
}

int EB::MDF_reader::extract_events(std::vector<EB::MDF_block>& blocks, int n_events, void* buffer, size_t size)
{
  int ret_val = 0;
  blocks.reserve(blocks.size() + n_events);
  for (int k = 0; (k < n_events) && (ret_val >= 0); k++) {
    blocks.emplace_back();
    ret_val = extract_event(blocks.back(), buffer, size);
    if (ret_val >= 0) {
      // if the external buffer was provided the pointers must be moved and the size adjusted
      size_t last_payload_size = blocks.back().payload_size();
      buffer = reinterpret_cast<char*>(buffer) + last_payload_size;
      size -= last_payload_size;
      ret_val += last_payload_size;
    } else {
      // if there is an error the last element of the blocks array is not valid so we remove it
      blocks.pop_back();
    }
  }
  return ret_val;
}

int EB::MDF_reader::extract_events(std::vector<EB::MDF_block>& blocks)
{
  int ret_val = 0;
  bool stop = false;
  while (!stop) {
    blocks.emplace_back();
    ret_val = extract_event(blocks.back());
    if (ret_val >= 0) {
      // if the external buffer was provided the pointers must be moved and the size adjusted
      ret_val += blocks.back().payload_size();
    } else {
      // if there is an error the last element of the blocks array is not valid so we remove it
      blocks.pop_back();
      stop = true;
    }
  }
  return ret_val;
}

int EB::MDF_reader::extract_events(std::vector<EB::MDF_block>& blocks, void* buffer, size_t size)
{
  int ret_val = 0;
  bool stop = false;
  while (!stop) {
    blocks.emplace_back();
    ret_val = extract_event(blocks.back(), buffer, size);
    if (ret_val >= 0) {
      // if the external buffer was provided the pointers must be moved and the size adjusted
      size_t last_payload_size = blocks.back().payload_size();
      buffer = reinterpret_cast<char*>(buffer) + last_payload_size;
      size -= last_payload_size;
      ret_val += last_payload_size;
    } else {
      // if there is an error the last element of the blocks array is not valid so we remove it
      blocks.pop_back();
      stop = true;
    }
  }
  return ret_val;
}