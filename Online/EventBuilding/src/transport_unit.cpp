#include "transport_unit.hpp"
#include "EventBuilding/tools.hpp"
#include <algorithm>
#include <functional>
#include <numeric>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <type_traits>
#include "infiniband_net/sock.hpp"
#include "infiniband_net/parser.hpp"

bool EB::is_bu(int rank, const std::vector<int>& n_rus_nic)
{
  bool ret_val = false;
  int idx = 0;
  int k = 0;
  while (idx <= rank) {
    ret_val = (rank - idx) == n_rus_nic[k];
    idx += n_rus_nic[k] + 1; // +1 for the BU
    k++;
  }

  return ret_val;
}

bool EB::is_ru(int rank, const std::vector<int>& n_rus_nic)
{
  bool ret_val = false;
  int idx = 0;
  int k = 0;
  while (idx <= rank) {
    ret_val = (rank - idx) < n_rus_nic[k];
    idx += n_rus_nic[k] + 1; // +1 for the BU
    k++;
  }

  return ret_val;
}

EB::Transport_unit::Transport_unit(const std::string& name, Context& framework) : DataflowComponent(name, framework)
{
  declareProperty("RUs_per_nic", _n_rus_per_nic);
  // TODO check if this property is needed
  declareProperty("n_par_mess", _n_par_mess = 1);
  declareProperty("n_sources_per_ru", _n_sources_per_ru);
  // the default value is the self assignment
  declareProperty("RU_ranks", _ru_ranks);
  declareProperty("BU_ranks", _bu_ranks);
  declareProperty("shift_pattern", _shift_pattern);
  declareProperty("ib_config_file", _ib_config_file);
  // This property is deprecated
  declareProperty("MPI_errors_return", _MPI_errors_return = false);
  // DEBUG counters
  declareMonitor("barrier_count", _barrier_count, "Number of barriers completed in the RUN");
  declareMonitor("pre_barrier_count", _pre_barrier_count, "Number of barriers reached in the RUN");
}

EB::Transport_unit::~Transport_unit() {}

int EB::Transport_unit::init_ranks()
{

  std::stringstream logger_name;
  logger_name << logger.get_name() << " rank " << _my_rank;
  logger.set_name(logger_name.str());

  if (_n_rus_per_nic.size() == 0) {
    // if 0 size self assign 1 RU per NIC
    _n_rus_per_nic.resize(_world_size / 2, 1);
    logger.warning << __FUNCTION__ << " no number of RUs per NIC provided setting to default value " << 1 << std::flush;
  } else if (_n_rus_per_nic.size() == 1) {
    _n_rus_per_nic.resize(_world_size / 2, _n_rus_per_nic[0]);
    logger.warning << __FUNCTION__ << " Single number of RUs per NIC provided setting to default value "
                   << _n_rus_per_nic[0] << std::flush;
  }

  if ((_bu_ranks.size() == 0) && (_ru_ranks.size() == 0)) {
    // if 0 size self assign
    logger.info << "Self assigning RU and BU ranks" << std::flush;
    _ru_ranks.reserve(_world_size);
    _bu_ranks.reserve(_world_size);
    for (int k = 0; k < _world_size; k++) {
      // TODO this is not implemented in the fastest way, in any case this should not be used in the real system
      if (is_bu(k, _n_rus_per_nic)) {
        _bu_ranks.push_back(k);
      } else if (is_ru(k, _n_rus_per_nic)) {
        _ru_ranks.push_back(k);
      } else {
        logger.error << "rank " << k << " cannot be assigned to any unit" << std::flush;
      }
    }
  }

  if (logger.is_active(PrintLevel::DEBUG)) {
    logger.debug << "RU ranks: ";
    for (const auto& elem : _ru_ranks) {
      logger.debug << elem << " ";
    }
    logger.debug << std::flush;

    logger.debug << "BU ranks: ";
    for (const auto& elem : _bu_ranks) {
      logger.debug << elem << " ";
    }
    logger.debug << std::flush;
  }

  return check_ranks();
}

int EB::Transport_unit::check_ranks()
{
  std::vector<int> all_ranks(_world_size);
  std::iota(all_ranks.begin(), all_ranks.end(), 0);
  logger.debug << "all ranks: ";
  for (const auto& elem : all_ranks) {
    logger.debug << elem << " ";
  }
  logger.debug << std::flush;

  std::vector<int> configured_ranks;
  configured_ranks.reserve(_world_size);
  configured_ranks.insert(configured_ranks.end(), _ru_ranks.begin(), _ru_ranks.end());
  configured_ranks.insert(configured_ranks.end(), _bu_ranks.begin(), _bu_ranks.end());
  std::sort(configured_ranks.begin(), configured_ranks.end());

  // if this condition is met all the ranks are properly set
  if (all_ranks != configured_ranks) {
    // if there is a mismatch we need to go deeper
    std::vector<int> error_ranks(std::max(configured_ranks.size(), all_ranks.size()));
    std::vector<int>::iterator rank_error_it;

    // search for duplicated ranks
    rank_error_it = find_all_rep(configured_ranks.begin(), configured_ranks.end(), error_ranks.begin());
    if (rank_error_it != error_ranks.begin()) {
      logger.error << __FUNCTION__ << " Invalid RU & BU rank configuration, repeated ranks: ";
      for (auto it = error_ranks.begin(); it != rank_error_it; it++) {
        logger.error << *it << " ";
      }
      logger.error << std::flush;
    }

    // after reporting duplicates uniq on the configured ranks
    rank_error_it = std::unique(configured_ranks.begin(), configured_ranks.end());
    configured_ranks.erase(rank_error_it, configured_ranks.end());

    // search for missing ranks
    rank_error_it = std::set_difference(
      all_ranks.begin(), all_ranks.end(), configured_ranks.begin(), configured_ranks.end(), error_ranks.begin());

    if (rank_error_it != error_ranks.begin()) {
      logger.error << __FUNCTION__ << " Invalid RU & BU rank configuration, missing ranks: ";
      for (auto it = error_ranks.begin(); it != rank_error_it; it++) {
        logger.error << *it << " ";
      }
      logger.error << std::flush;
    }

    // search for non existing ranks
    rank_error_it = std::set_difference(
      configured_ranks.begin(), configured_ranks.end(), all_ranks.begin(), all_ranks.end(), error_ranks.begin());

    if (rank_error_it != error_ranks.begin()) {
      logger.error << __FUNCTION__ << " Invalid RU & BU rank configuration, non existing ranks: ";
      for (auto it = error_ranks.begin(); it != rank_error_it; it++) {
        logger.error << *it << " ";
      }
      logger.error << std::flush;
    }
    return DF_ERROR;
  }

  // It is enough to check this because the total number of ranks has already been tested

  if (_n_rus_per_nic.size() != _bu_ranks.size()) {
    logger.error << __FUNCTION__
                 << " Invalid configuration, the number of RUs per NICs has to be provided for every RU/BU group: "
                 << _bu_ranks.size() << " BUs provided with " << _n_rus_per_nic.size() << " RUs groups" << std::flush;
    return DF_ERROR;
  }

  int num_rus = std::accumulate(_n_rus_per_nic.begin(), _n_rus_per_nic.end(), 0);

  if (_ru_ranks.size() != num_rus) {
    logger.error << __FUNCTION__ << " Invalid configuration: the number of processes and units does not match. Got "
                 << _bu_ranks.size() << " BUs instead of " << _world_size - num_rus << " and " << _ru_ranks.size()
                 << " RUs instead of " << num_rus << std::flush;
    return DF_ERROR;
  }

  // Check if the configuration is the same on all the nodes

  return DF_SUCCESS;
}

void EB::Transport_unit::set_rank(int rank)
{
  // TODO this may not be the best sanity check
  if (!logger.is_active(Online::PrintLevel::DEBUG)) {
    logger.always << __FILE__ << ":" << __LINE__ << ": WARNING this function is for DEBUG only" << std::endl;
  }
  _my_rank = rank;
}

int EB::Transport_unit::initialize()
{
  int ret_val;
  ret_val = DataflowComponent::initialize();
  if (ret_val != DF_SUCCESS) {
    return error("Failed to initialize service base class.");
  }
  logger.set_level(Online::PrintLevel(outputLevel));

  try {
    _ibParser = std::make_unique<IB_verbs::Parser>(_ib_config_file.c_str());
  } catch (std::exception& e) {
    logger.error << "IB file parser: " << e.what() << std::flush;
    return 0;
  }
  _my_rank = _ibParser->getProcessId();
  logger.debug << "my rank is " << _my_rank << std::flush;

  _world_size = _ibParser->getTotalProcesses();
  logger.debug << "world size is " << _world_size << std::flush;
  if (_world_size == 0) return DF_ERROR;
  ret_val = init_ranks();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  _numa_node = _ibParser->getIbNumaNode();

  ret_val = init_n_sources();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  ret_val = init_pattern();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  return ret_val;
}

int EB::Transport_unit::start()
{
  try {
    _ibComm = std::make_unique<Parallel_comm>(_ibParser.get(), logger.get_name(), logger.get_level());
  } catch (std::exception& e) {
    logger.error << "Parallel Comm Constructor: " << e.what() << std::flush;
    return 0;
  }
  logger.debug << "verbs started" << std::flush;
  return 1;
}

int EB::Transport_unit::cancel()
{
  _ibComm->ibKillBlocking();
  return DF_SUCCESS;
}

int EB::Transport_unit::stop()
{
  _ibComm.reset(nullptr);
  logger.debug << "verbs stopped" << std::flush;
  return 1;
}

int EB::Transport_unit::finalize()
{
  // TODO maybe we want a barrier here
  _ibParser.reset(nullptr);
  return DataflowComponent::finalize();
}

// TODO ibBarrier should return DF_CANCELLED or cancel
int EB::Transport_unit::sync()
{
  int ret_val = DF_SUCCESS;
  _pre_barrier_count++;
  ret_val = _ibComm->ibBarrier();
  _barrier_count++;
  return ret_val;
}

int EB::Transport_unit::init_pattern()
{
  int ret_val = DF_SUCCESS;
  std::vector<int> base_pattern(_bu_ranks.size());
  std::iota(base_pattern.begin(), base_pattern.end(), 0);
  if (_shift_pattern.size() == 0) {
    _shift_pattern = base_pattern;
    logger.warning << "No shift pattern provided, enabling all the nodes with default shift." << std::flush;
  }

  // FIXME input checking should be done only for non default values

  std::vector<int> nodes;

  // copy of the vector without the ghost nodes
  std::copy_if(
    _shift_pattern.begin(), _shift_pattern.end(), std::back_inserter(nodes), [](int val) { return (val >= 0); });

  int num_ghost = _shift_pattern.size() - nodes.size();
  int num_nodes = nodes.size();

  std::sort(nodes.begin(), nodes.end());

  if (_shift_pattern.size() < _bu_ranks.size()) {
    logger.error << "Invalid shift pattern: the shift pattern should have at least " << _bu_ranks.size()
                 << " nodes, got only " << _shift_pattern.size() << " nodes" << std::flush;
    ret_val = DF_ERROR;
  } else if (_shift_pattern.size() - num_ghost != _bu_ranks.size()) {
    logger.error << "Invalid shift pattern: expecting " << _shift_pattern.size() - _bu_ranks.size()
                 << " ghost nodes, got " << num_ghost << " ghost nodes" << std::flush;
    ret_val = DF_ERROR;
  } else if (nodes != base_pattern) {
    logger.error << "Invalid shift pattern:";
    for (const auto& elem : _shift_pattern) {
      logger.error << " " << elem;
    }
    logger.error << std::flush;
    ret_val = DF_ERROR;
  }

  return ret_val;
}

int EB::Transport_unit::init_n_sources()
{
  int ret_val = DF_SUCCESS;
  if (_n_sources_per_ru.size() == 0) {
    _n_sources_per_ru.resize(_ru_ranks.size(), 1);
    logger.warning << __FUNCTION__ << " no number of sources per RU provided setting to default value: " << 1
                   << std::flush;
  } else if (_n_sources_per_ru.size() == 1) {
    _n_sources_per_ru.resize(_ru_ranks.size(), _n_sources_per_ru[0]);
    logger.warning << __FUNCTION__ << " Single number of sources per RU provided: " << _n_sources_per_ru[0]
                   << std::flush;
  }

  else if (_n_sources_per_ru.size() != _ru_ranks.size()) {
    logger.error << __FUNCTION__ << " configuration error: incorrect number of sources per RU. Got "
                 << _n_sources_per_ru.size() << " values for " << _ru_ranks.size() << " RUs" << std::flush;
    ret_val = DF_ERROR;
  }

  auto zero_src_it = std::find(_n_sources_per_ru.begin(), _n_sources_per_ru.end(), 0);
  if (zero_src_it != _n_sources_per_ru.end()) {
    auto ru_idx = std::distance(_n_sources_per_ru.begin(), zero_src_it);
    logger.error << __FUNCTION__ << " configuration error: RU id " << ru_idx << " got 0 sources." << std::flush;
    ret_val = DF_ERROR;
  }

  // the last element is the used for the MPI gather and represents the number of sources of the BU so it is set to 0
  _n_sources_per_ru.emplace_back(0);

  // n RU + 2 elements the first element is 0 and the last two the number total number of sources (the last element is
  // the used for the MPI gather and represents the number of sources of the BU)
  _prefix_n_sources_per_ru.resize(_n_sources_per_ru.size() + 1);
  _prefix_n_sources_per_ru[0] = 0;
  std::partial_sum(_n_sources_per_ru.begin(), _n_sources_per_ru.end(), _prefix_n_sources_per_ru.begin() + 1);

  return ret_val;
}

void EB::Transport_unit::reset_counters()
{
  _pre_barrier_count = 0;
  _barrier_count = 0;
}