#include "MFP_preloader.hpp"
bool EB::MFP_preloader::set_MDF_filename(const char* filename) { return _reader.set_file(filename); }

void EB::MFP_preloader::set_packing_factor(size_t packing_factor) { _packing_factor = packing_factor; }
size_t EB::MFP_preloader::get_packing_factor() const { return _packing_factor; }

void EB::MFP_preloader::set_n_MFPs(int n_MFPs) { _n_MFPs = n_MFPs; }
int EB::MFP_preloader::get_n_MFPs() const { return _n_MFPs; }

int EB::MFP_preloader::set_buffers(std::vector<buffer_ptr_type>& buffers, std::vector<size_t>& buffer_sizes)
{
  int ret_val = 0;
  if (buffers.size() == buffer_sizes.size()) {
    _buffers = std::move(buffers);
    _buffer_sizes = std::move(buffer_sizes);
    // reset buffer occupancy
    _buffer_occupancy = std::vector<size_t>(_buffers.size(), 0);
  } else {
    ret_val = -1;
  }

  return ret_val;
}

void EB::MFP_preloader::set_detector_map(const std::vector<EB::type_id_pair_type>& src_map)
{
  _source_mapping = src_map;
}

std::tuple<std::vector<EB::MFP_preloader::buffer_ptr_type>, std::vector<size_t>, std::vector<size_t>>
EB::MFP_preloader::get_buffers()
{
  return {std::move(_buffers), std::move(_buffer_sizes), std::move(_buffer_occupancy)};
}

int EB::MFP_preloader::preload_MFPs()
{
  int ret_val = 0;
  std::vector<EB::MDF_block> events;
  ret_val = _reader.extract_events(events, _packing_factor * _n_MFPs);

  if (ret_val == 0) {
    _builder.append_events(events);
    int n_sources;
    // no src mapping provided
    if (_source_mapping.size() == 0) {
      n_sources = _builder.get_source_number();
    } else {
      n_sources = _source_mapping.size();
    }

    assert(_buffers.size() == _buffer_sizes.size());
    // no buffers have been provided we allocate some
    if (_buffers.size() == 0) {
      size_t buff_size = (EB::MFP_header_size(_packing_factor) + _frag_size * _packing_factor) * _n_MFPs;

      assert((_buffer_sizes.size() == 0) && (_buffer_occupancy.size() == 0));

      _buffers.resize(n_sources);
      _buffer_sizes.resize(n_sources, buff_size);
      _buffer_occupancy.resize(n_sources, 0);

      for (auto& elem : _buffers) {
        // elem = EB::MFP_preloader::buffer_ptr_type(new char[buff_size], [](auto p) {delete;});
        elem = EB::MFP_preloader::buffer_ptr_type(new char[buff_size]);
      }
    }

    std::vector<char*> write_ptr(n_sources);
    // free space in the buffer for convenience
    std::vector<size_t> buffer_free = _buffer_sizes;

    for (size_t idx = 0; idx < write_ptr.size(); idx++) {
      write_ptr[idx] = _buffers[idx].get();
    }

    for (size_t k = 0; (k < _n_MFPs) && (ret_val >= 0); k++) {
      if (_source_mapping.size() == 0) {
        ret_val = _builder.build_MFPs(_packing_factor, write_ptr, buffer_free);
      } else {
        ret_val = _builder.build_MFPs(_packing_factor, write_ptr, buffer_free, _source_mapping);
      }

      if (ret_val == 0) {
        for (size_t idx = 0; idx < write_ptr.size(); idx++) {
          // updateing wrt pointe and buffer occupancy
          // TODO this will overlap because the MFP_header does not include the per fragment data
          size_t total_block_size = reinterpret_cast<EB::MFP_header*>(write_ptr[idx])->bytes();
          // page aligned MFPs
          total_block_size += get_padding(total_block_size, 4096);
          write_ptr[idx] += total_block_size;
          _buffer_occupancy[idx] += total_block_size;
          buffer_free[idx] -= total_block_size;
          // this is needed becuase build_MFPs is not aware of padding
          if (_buffer_occupancy[idx] >= _buffer_sizes[idx]) {
            buffer_free[idx] = 0;
          }
        }
      }
    }
  }
  return ret_val;
}