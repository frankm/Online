#include "EventBuilding/raw_tools.hpp"
#include "EventBuilding/tools.hpp"
// TODO check why this file breaks linking if include in header file
#ifdef HAVE_PCIE40
#include <daq40/sourceid.h>
#endif

EB::raw_block::raw_block() : Generic_block() { header = {}; }

EB::raw_block::raw_block(std::shared_ptr<char> payload, size_t size) : Generic_block(payload, size) { header = {}; }

std::ostream& EB::raw_header::print(std::ostream& os) const
{
  os << "------RAW HEADER------" << std::endl;
  os << "magic: " << (magic == EB::block_magic ? "OK" : "NOT OK") << std::endl;
  os << "size: " << size << std::endl;
  os << "type: " << BankType(type) << std::endl;
  os << "version: " << (uint) version << std::endl;
  os << "source id: " << src_id << std::endl;
  return os;
}

std::ostream& EB::operator<<(std::ostream& os, const EB::raw_header& header) { return header.print(os); }

// size_t EB::raw_header::real_size() const { return size + size % raw::block_alignment; }
size_t EB::raw_header::mem_size() const { return size + get_padding(size, EB::block_alignment); }

size_t EB::raw_header::payload_mem_size() const { return mem_size() - sizeof(this); }

size_t EB::raw_header::payload_size() const { return size - sizeof(this); }

const std::string EB::ToString(const BankType b_type)
{
  switch (b_type) {
  case L0Calo: return "L0Calo";
  case L0DU: return "L0DU";
  case PrsE: return "PrsE";
  case EcalE: return "EcalE";
  case HcalE: return "HcalE";
  case PrsTrig: return "PrsTrig";
  case EcalTrig: return "EcalTrig";
  case HcalTrig: return "HcalTrig";
  case Velo: return "Velo";
  case Rich: return "Rich";
  case TT: return "TT";
  case IT: return "IT";
  case OT: return "OT";
  case Muon: return "Muon";
  case L0PU: return "L0PU";
  case DAQ: return "DAQ";
  case ODIN: return "ODIN";
  case HltDecReports: return "HltDecReports";
  case VeloFull: return "VeloFull";
  case TTFull: return "TTFull";
  case ITFull: return "ITFull";
  case EcalPacked: return "EcalPacked";
  case HcalPacked: return "HcalPacked";
  case PrsPacked: return "PrsPacked";
  case L0Muon: return "L0Muon";
  case ITError: return "ITError";
  case TTError: return "TTError";
  case ITPedestal: return "ITPedestal";
  case TTPedestal: return "TTPedestal";
  case VeloError: return "VeloError";
  case VeloPedestal: return "VeloPedestal";
  case VeloProcFull: return "VeloProcFull";
  case OTRaw: return "OTRaw";
  case OTError: return "OTError";
  case EcalPackedError: return "EcalPackedError";
  case HcalPackedError: return "HcalPackedError";
  case PrsPackedError: return "PrsPackedError";
  case L0CaloFull: return "L0CaloFull";
  case L0CaloError: return "L0CaloError";
  case L0MuonCtrlAll: return "L0MuonCtrlAll";
  case L0MuonProcCand: return "L0MuonProcCand";
  case L0MuonProcData: return "L0MuonProcData";
  case L0MuonRaw: return "L0MuonRaw";
  case L0MuonError: return "L0MuonError";
  case GaudiSerialize: return "GaudiSerialize";
  case GaudiHeader: return "GaudiHeader";
  case TTProcFull: return "TTProcFull";
  case ITProcFull: return "ITProcFull";
  case TAEHeader: return "TAEHeader";
  case MuonFull: return "MuonFull";
  case MuonError: return "MuonError";
  case TestDet: return "TestDet";
  case L0DUError: return "L0DUError";
  case HltRoutingBits: return "HltRoutingBits";
  case HltSelReports: return "HltSelReports";
  case HltVertexReports: return "HltVertexReports";
  case HltLumiSummary: return "HltLumiSummary";
  case L0PUFull: return "L0PUFull";
  case L0PUError: return "L0PUError";
  case DstBank: return "DstBank";
  case DstData: return "DstData";
  case DstAddress: return "DstAddress";
  case FileID: return "FileID";
  case VP: return "VP";
  case FTCluster: return "FTCluster";
  case VL: return "VL";
  case UT: return "UT";
  case UTFull: return "UTFull";
  case UTError: return "UTError";
  case UTPedestal: return "UTPedestal";
  case HC: return "HC";
  case HltTrackReports: return "HltTrackReports";
  case HCError: return "HCError";
  case VPRetinaCluster: return "VPRetinaCluster";
  case LastType: return "LastType";
  default: return "Undefined Type";
  }
}

std::ostream& EB::operator<<(std::ostream& os, const BankType b_type) { return os << ToString(b_type); }

#ifdef HAVE_PCIE40
int EB::raw_header::get_sys_src_id() const { return SourceId_sys(src_id); }
int EB::raw_header::get_src_id_num() const { return SourceId_num(src_id); }

void EB::raw_header::set_sys_src_id(uint16_t sys_src_id) { src_id = (sys_src_id << 11) | (src_id & 0x1F); }
void EB::raw_header::set_src_id_num(uint16_t src_id_num) { src_id = (src_id & 0xF800) | (src_id_num & 0x7FF); }

uint16_t EB::type_to_partition_id(const BankType b_type)
{
  switch (b_type) {
  case VP: return SourceIdSys_VELO_A;
  case FTCluster: return SourceIdSys_SCIFI_A;
  case UT: return SourceIdSys_UT_A;
  case Muon: return SourceIdSys_MUON_A;
  case EcalPacked: return SourceIdSys_ECAL;
  case HcalPacked: return SourceIdSys_HCAL;
  case Rich: return SourceIdSys_RICH_1;
  case ODIN: return SourceIdSys_ODIN;
  // TODO probably TDET is not a good default value
  default: return SourceIdSys_TDET;
  }
}

EB::type_id_pair_type EB::new_src_id_to_old(uint16_t src_id) { return std::make_pair(EB::BankType::UT, src_id); }

uint16_t EB::old_to_new(EB::type_id_pair_type src_id_type)
{
  return (type_to_partition_id(static_cast<BankType>(src_id_type.first)) << 11) | (src_id_type.second & 0x7FF);
}
#endif
