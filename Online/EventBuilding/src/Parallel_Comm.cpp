#include "Parallel_Comm.hpp"
#include <exception>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <stdexcept>
#include <sstream>
#include <numa.h>

using namespace Online;

std::ostream& EB::comm_op::print(std::ostream& os) const
{
  os << "size " << count * datatype << "\n";
  return os;
}

std::ostream& EB::comm_send::print(std::ostream& os) const
{
  os << "COMM SEND"
     << "\n";
  os << "destination " << destination << "\n";
  os << "buffer " << std::hex << snd_buff << std::dec << "\n";
  return comm_op::print(os);
}

std::ostream& EB::comm_recv::print(std::ostream& os) const
{
  os << "COMM RECV"
     << "\n";
  os << "source " << source << "\n";
  os << "buffer " << std::hex << recv_buff << std::dec << "\n";
  return comm_op::print(os);
}

EB::Parallel_comm::Parallel_comm(Parallel_comm&& src) : _ibObj(std::move(src._ibObj)) {}

EB::Parallel_comm& EB::Parallel_comm::operator=(Parallel_comm&& src)
{
  if (this != &src) {
    this->_ibDestroy();
    this->_ibObj = std::move(src._ibObj);
  }
  return *this;
}

EB::Parallel_comm::Parallel_comm(IB_verbs::Parser* prs, std::string logName, Online::PrintLevel logLevel)
{
  logger.set_name(logName);
  logger.set_level(logLevel);
  if (!_ibInit(prs)) throw std::runtime_error("cannot start verbs!!!");
}

EB::Parallel_comm::~Parallel_comm() { _ibDestroy(); }

int EB::Parallel_comm::_ibInit(IB_verbs::Parser* ibprs)
{
  try {
    int ret = _ibObj.ibSetHostList(*ibprs);
  } catch (std::exception& e) {
    logger.error << "IB config file parser: " << e.what() << std::flush;
    return DataflowStatus::DF_ERROR;
  }
  int devNuma = _ibObj.ibGetNumaNode();
  int numa_err = numa_run_on_node(devNuma);
  if (numa_err != 0) {
    logger.error << __FUNCTION__ << " numa_run_on_node " << devNuma << ":" << strerror(errno) << std::flush;
    return Online::DF_ERROR;
  }

  logger.debug << "running on NUMA node " << devNuma << std::flush;

  numa_set_preferred(devNuma);
  try {
    _ibObj.ibInit();
  } catch (std::exception& e) {
    logger.error << "IB init: " << e.what() << std::flush;
    return DataflowStatus::DF_ERROR;
  }
  return DataflowStatus::DF_SUCCESS;
}

int EB::Parallel_comm::_ibDestroy()
{
  try {
    _ibObj.ibDestroy();
  } catch (std::exception& e) {
    logger.error << "IB destroy: " << e.what() << std::flush;
    return DataflowStatus::DF_ERROR;
  }
  return DataflowStatus::DF_SUCCESS;
}

int EB::Parallel_comm::ibDeregMRs()
{
  try {
    _ibObj.ibDeregMRs();
  } catch (std::exception& e) {
    logger.error << "IB dereg MRs: " << e.what() << std::flush;
    return DataflowStatus::DF_ERROR;
  }
  return DataflowStatus::DF_SUCCESS;
}

int EB::Parallel_comm::getTotalProcesses() { return _ibObj.getTotalProcesses(); }

int EB::Parallel_comm::getProcessID() { return _ibObj.getProcessID(); }

int EB::Parallel_comm::addMR(char* bufPtr, size_t bufSize)
{
  if (_ibObj.ibAddMR(bufPtr, bufSize) == NULL) {
    return DataflowStatus::DF_ERROR;
  }
  return DataflowStatus::DF_SUCCESS;
}

void EB::Parallel_comm::ibKillBlocking() { _ibObj.ibKillBlocking(); }

int EB::Parallel_comm::send(const std::vector<comm_send>& sends)
{
  int ret;
  logger.debug << "waiting sync: " << sends.front().destination << std::flush;
  ret = _syncRecv(sends.front().destination);
  if (ret != DataflowStatus::DF_SUCCESS) {
    return ret;
  }

  logger.debug << "Posting SRs" << std::flush;
  ret = _sendV(sends);
  if (ret == -5) {
    return DataflowStatus::DF_CANCELLED;
  } else if (ret != 0) {
    return DataflowStatus::DF_ERROR;
  }

  return DataflowStatus::DF_SUCCESS;
}

int EB::Parallel_comm::receive(const std::vector<comm_recv>& recvs)
{
  int ret = DataflowStatus::DF_SUCCESS;
  logger.debug << "Posting RRs" << std::flush;
  auto recvsid = _receiveV(recvs);
  logger.debug << "send sync: " << recvs.front().source << std::flush;
  ret = _syncSend(recvs.front().source);
  if (ret != DataflowStatus::DF_SUCCESS) {
    return ret;
  }

  logger.debug << "waiting recv completion" << std::flush;
  ret = _waitRecvs(recvsid);
  logger.debug << "recv completed" << std::flush;
  return ret;
}

int EB::Parallel_comm::_waitRecvs(std::vector<uint32_t>& recvs)
{
  int ret = _ibObj.ibWaitAllRecvs(recvs);
  if (ret == 0) {
    return DataflowStatus::DF_SUCCESS;
  } else if (ret == -5) {
    return DataflowStatus::DF_CANCELLED;
  }
  return DataflowStatus::DF_ERROR;
}

int EB::Parallel_comm::_waitSends(std::vector<uint32_t>& sends)
{
  int ret = _ibObj.ibWaitAllSends(sends);
  if (ret == 0) {
    return DataflowStatus::DF_SUCCESS;
  } else if (ret == -5) {
    return DataflowStatus::DF_CANCELLED;
  }
  return DataflowStatus::DF_ERROR;
}

int EB::Parallel_comm::ibTestRecvs(std::vector<uint32_t>& recvs) { return _ibObj.ibTestAllRecvs(recvs); }

std::vector<uint32_t> EB::Parallel_comm::_receiveV(const std::vector<comm_recv>& recvs)
{
  std::vector<uint32_t> req_vec;
  for (auto& recv_it : recvs) {
    size_t size = recv_it.count * recv_it.datatype;
    uint32_t ret = _ibObj.ibRecv((char*) recv_it.recv_buff, size, recv_it.source);
    if (ret != 0) {
      req_vec.push_back(ret);
    } else {
      logger.error << "IB recv FAILED with err: " << ret << std::flush;
    }
  }
  return req_vec;
}

int EB::Parallel_comm::_sendV(const std::vector<comm_send>& sends)
{
  std::vector<uint32_t> req_vec;
  for (auto& send_it : sends) {
    size_t size = send_it.count * send_it.datatype;
    uint32_t ret = _ibObj.ibSend((char*) send_it.snd_buff, size, send_it.destination);

    if (ret != 0) {
      req_vec.push_back(ret);
    } else {
      logger.error << "IB send FAILED with err: " << ret << std::flush;
      return -1;
    }
  }

  return _ibObj.ibWaitAllSends(req_vec);
}

int EB::Parallel_comm::_syncSend(int dest)
{
  int rank = _ibObj.getProcessID();
  logger.debug << rank << " S SYNC to " << dest << std::flush;
  uint32_t wid = _ibObj.ibSendSync(IB_verbs::IB::immediate_t::ONE_TO_ONE, dest);
  if (wid == 0) return DataflowStatus::DF_ERROR;
  int ret = _ibObj.ibWaitSyncSend(wid);
  if (ret == 0) {
    return DataflowStatus::DF_SUCCESS;
  } else if (ret == -5) {
    return DataflowStatus::DF_CANCELLED;
  }
  return DataflowStatus::DF_ERROR;
}

uint32_t EB::Parallel_comm::_syncRecv(int src)
{
  int rank = _ibObj.getProcessID();
  logger.debug << rank << " R SYNC from " << src << std::flush;
  int ret = _ibObj.ibWaitSyncRecv(IB_verbs::IB::immediate_t::ONE_TO_ONE, src);
  if (ret == 0) {
    return DataflowStatus::DF_SUCCESS;
  } else if (ret == -5) {
    return DataflowStatus::DF_CANCELLED;
  }
  return DataflowStatus::DF_ERROR;
}

int EB::Parallel_comm::ibBarrier()
{
  int ret = _ibObj.ibBarrier();
  if (ret == -5) {
    return DataflowStatus::DF_CANCELLED;
  } else if (ret < 0) {
    logger.error << "Parallel_Comm: Barrier Failed" << std::flush;
    return DataflowStatus::DF_ERROR;
  }
  return DataflowStatus::DF_SUCCESS;
}

int EB::Parallel_comm::ibScatterV(char* data, size_t n_elem, size_t elem_size, std::vector<int>& dsts)
{
  std::vector<uint32_t> wrids;
  int ret_val = 0;
  uint32_t data_wrid = 0;
  for (int j = 0; j < dsts.size(); j++) {
    logger.debug << "waiting sync from " << dsts.at(j) << std::flush;
    ret_val = _syncRecv(dsts.at(j));
    if (ret_val != DataflowStatus::DF_SUCCESS) {
      return ret_val;
    }
    size_t idx = j * n_elem * elem_size;
    logger.debug << "sending sizes to " << dsts.at(j) << ": " << n_elem << std::flush;
    data_wrid = _ibObj.ibSend(data + idx, n_elem * elem_size, dsts.at(j));
    if (data_wrid == 0) return DataflowStatus::DF_ERROR;
    wrids.push_back(data_wrid);
    ret_val = _waitSends(wrids);
    if (ret_val != DataflowStatus::DF_SUCCESS) {
      return ret_val;
    }
  }
  return DataflowStatus::DF_SUCCESS;
}

int EB::Parallel_comm::ibBroadcastV(char* data, size_t n_elem, size_t elem_size, std::vector<int>& dsts)
{
  std::vector<uint32_t> wrids;
  int ret_val = 0;
  uint32_t data_wrid = 0;
  for (int j = 0; j < dsts.size(); j++) {
    logger.debug << "waiting sync from " << dsts.at(j) << std::flush;
    ret_val = _syncRecv(dsts.at(j));
    if (ret_val != DataflowStatus::DF_SUCCESS) {
      return ret_val;
    }
    logger.debug << "sending sizes to " << dsts.at(j) << ": " << n_elem << std::flush;
    data_wrid = _ibObj.ibSend(data, n_elem * elem_size, dsts.at(j));
    if (data_wrid == 0) return DataflowStatus::DF_ERROR;
    wrids.push_back(data_wrid);
    ret_val = _waitSends(wrids);
    if (ret_val != DataflowStatus::DF_SUCCESS) {
      return ret_val;
    }
  }
  return DataflowStatus::DF_SUCCESS;
}

int EB::Parallel_comm::ibGatherBlockV(
  char* data,
  int* data_sizes,
  int* data_idxs,
  size_t elem_size,
  std::vector<int>& remotes)
{
  int ret_val = 0;
  auto wrids = ibGatherV(data, data_sizes, data_idxs, elem_size, remotes, ret_val);
  logger.debug << "sync done" << std::flush;
  ret_val = _ibObj.ibWaitAllRecvs(wrids);
  if (ret_val == -5) {
    return DataflowStatus::DF_CANCELLED;
  } else if (ret_val < 0) {
    return DataflowStatus::DF_ERROR;
  }
  return DataflowStatus::DF_SUCCESS;
}

std::vector<uint32_t> EB::Parallel_comm::ibGatherV(
  char* data,
  int* data_sizes,
  int* data_idxs,
  size_t elem_size,
  std::vector<int>& remotes,
  int& ret_val)
{
  ret_val = DataflowStatus::DF_SUCCESS;
  std::vector<uint32_t> wrids;
  std::vector<uint32_t> syncids;
  uint32_t data_wrid, sync_wrid;
  for (int i = 0; i < remotes.size(); i++) {
    logger.debug << "posting gather recv from " << remotes.at(i) << ": " << data_sizes[i] << ", "
                 << (uintptr_t) (data) + data_idxs[i] * elem_size << std::flush;
    data_wrid = _ibObj.ibRecv(data + data_idxs[i] * elem_size, data_sizes[i] * elem_size, remotes.at(i));
    if (data_wrid == 0) {
      ret_val = DataflowStatus::DF_ERROR;
      // TODO check if break is a better option
      return wrids;
    }
    wrids.push_back(data_wrid);
    logger.debug << "sending sync to " << remotes.at(i) << std::flush;
    sync_wrid = _ibObj.ibSendSync(IB_verbs::IB::immediate_t::ONE_TO_ONE, remotes.at(i));
    if (sync_wrid == 0) {
      ret_val = DataflowStatus::DF_ERROR;
      // TODO check if break is a better option
      return wrids;
    }
    syncids.push_back(sync_wrid);
  }
  logger.debug << "waiting for sync" << std::flush;
  int ret = _ibObj.ibWaitAllSyncSends(syncids);
  if (ret == -5) {
    ret_val = DataflowStatus::DF_CANCELLED;
  } else if (ret != 0) {
    ret_val = DataflowStatus::DF_ERROR;
  }
  logger.debug << "sync done" << std::flush;
  return wrids;
}