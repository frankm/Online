#include "EventBuilding/RU.hpp"
#include "shmem_buffer_reader.hpp"
#include <algorithm>
#include <numeric>
#include <stdexcept>
#include <sstream>
#include <chrono>
#include "MFP_preloader.hpp"
#include "RTL/rtl.h"
#include "EventBuilding/tools.hpp"
#include "EventBuilding/raw_tools.hpp"

const std::string EB::RU_buffer_type_to_string(RU_buffer_types type)
{
  switch (type) {
  case dummy_MFP_buffer: return "dummy_MFP_buffer";
  case shmem_MFP_buffer: return "shmem_MFP_buffer";
  case PCIe40_MFP_buffer: return "PCIe40_MFP_buffer";
  case PCIe40_frag_buffer: return "PCIe40_frag_reader";
  default: return "Error unkown buffer type";
  }
}

std::ostream& EB::operator<<(std::ostream& os, RU_buffer_types type) { return os << RU_buffer_type_to_string(type); }

EB::RU::RU(const std::string& nam, DataflowContext& ctxt) : Transport_unit(nam, ctxt)
{
  // TODO declare all the properties
  info("RU constructor");
  declareProperty("n_fragment", _n_fragment = 1);
  declareProperty("shmem_prefix", _shmem_prefix = "RU");
  declareProperty("buffer_type", _buffer_type);

  declareProperty("write_to_file", _write_to_file);
  declareProperty("out_file_prefix", _out_file_prefix = "RU");
  declareProperty("n_MFPs_to_file", _n_MFPs_to_file = 1);
  // TODO find a good default value
  declareProperty("PCIe40_ids", _PCIe40_ids);
  declareProperty("PCIe40_names", _PCIe40_names);
  declareProperty("buffer_sizes", _prop_buffer_sizes);
  declareProperty("MDF_filename", _MDF_filename);
  declareProperty("n_MFPs", _n_MFPs = 1);
  declareProperty("stop_timeout", _stop_timeout = 10);
  declareProperty("dummy_src_ids", _dummy_src_ids);
  declareProperty("SODIN_ID", _SODIN_ID = -1);
  declareProperty("SODIN_name", _SODIN_name = "");
  declareProperty("SODIN_stream", _SODIN_stream = 0);

  // DF monitoring counters
  declareMonitor("Events/IN", _DF_events_in, "Number events received in the RUN");
  declareMonitor("Events/OUT", _DF_events_out, "Number events processed and sent in RUN");
  declareMonitor("Events/ERROR", _DF_events_err, "Number events with errors in the RUN");

  // DF monitoring counters
  declareMonitor("Events/IN", _DF_events_in, "Number events received in the RUN");
  declareMonitor("Events/OUT", _DF_events_out, "Number events processed and sent in RUN");
  declareMonitor("Events/ERROR", _DF_events_err, "Number events with errors in the RUN");
  // DEBUG counters
  declareMonitor("run_loop_iteration", _run_loop_iteration, "Iteration fo the run loop in the RUN");

  logger.set_name(name);
}

EB::RU::~RU() {}

int EB::RU::initialize()
{
  int sc = Transport_unit::initialize();
  if (sc != DF_SUCCESS) {
    return error("Failed to initialize Transport unit base class.");
  }

  _my_idx = get_idx(_ru_ranks.begin(), _ru_ranks.end(), _my_rank);
  if (_my_idx < 0) {
    logger.error << " this process should not be a RU. Aborting." << std::flush;
    return DF_ERROR;
  }

  // Addding MPI information to the log name
  std::stringstream logger_name;
  logger_name << logger.get_name() << " idx " << _my_idx;
  logger.set_name(logger_name.str());

  sc = init_shift();
  if (sc != DF_SUCCESS) {
    logger.error << __FUNCTION__ << " init shift failed" << std::flush;
    return sc;
  }

  sc = init_dummy_src_ids();
  if (sc != DF_SUCCESS) {
    logger.error << __FUNCTION__ << " init dummy src ids failed" << std::flush;
    return sc;
  }

  return DF_SUCCESS;
}

int EB::RU::check_buffers()
{
  int ret_val = DF_SUCCESS;

  if (_buffer_type.size() == 0) {
    _buffer_type.resize(_ru_ranks.size(), default_RU_buffer_type);
    logger.warning << __FUNCTION__ << " no buffer types provided setting to default value "
                   << EB::default_RU_buffer_type << std::flush;
  } else if (_buffer_type.size() == 1) {
    _buffer_type.resize(_ru_ranks.size(), _buffer_type[0]);
    logger.warning << __FUNCTION__ << " Single buffer type provided: " << _buffer_type[0] << std::flush;
  }

  if (_buffer_type.size() != _ru_ranks.size()) {
    logger.error << __FUNCTION__ << " Configuration error: not enough buffer types provided " << _buffer_type.size()
                 << "/" << _ru_ranks.size() << std::flush;

    ret_val = DF_ERROR;
  }

  if (_prop_buffer_sizes.size() == 0) {
    _prop_buffer_sizes.resize(_prefix_n_sources_per_ru[_ru_ranks.size()], default_RU_buffer_size);
    logger.warning << __FUNCTION__ << " no buffer sizes provided setting to default value "
                   << EB::default_RU_buffer_size << std::flush;
  } else if (_prop_buffer_sizes.size() == 1) {
    _prop_buffer_sizes.resize(_prefix_n_sources_per_ru[_ru_ranks.size()], _prop_buffer_sizes[0]);
    logger.warning << __FUNCTION__ << " Single buffer size provided: " << _prop_buffer_sizes[0] << std::flush;
  }

  if (_prop_buffer_sizes.size() != _prefix_n_sources_per_ru[_ru_ranks.size()]) {
    logger.error << __FUNCTION__ << " Configuration error: not enough buffer sizes provided "
                 << _prop_buffer_sizes.size() << "/" << _prefix_n_sources_per_ru[_ru_ranks.size()] << std::flush;

    ret_val = DF_ERROR;
  }

  _buffer_sizes.resize(_prop_buffer_sizes.size());
  std::transform(_prop_buffer_sizes.begin(), _prop_buffer_sizes.end(), _buffer_sizes.begin(), [](auto val) {
    return val * 1024 * 1024 * 1024UL;
  });

  if (_write_to_file.size() == 0) {
    _write_to_file.resize(_ru_ranks.size(), false);
    logger.warning << __FUNCTION__ << " write to file was not set, setting it to the default value " << false
                   << std::flush;
  } else if (_write_to_file.size() == 1) {
    _write_to_file.resize(_ru_ranks.size(), _write_to_file[0]);
    logger.warning << __FUNCTION__ << " Single write to file provided: " << _write_to_file[0] << std::flush;
  }

  if (_write_to_file.size() != _ru_ranks.size()) {
    logger.error << __FUNCTION__ << " Configuration error: not enough write to file provided " << _write_to_file.size()
                 << "/" << _bu_ranks.size() << std::flush;

    ret_val = DF_ERROR;
  }

  _buffer_sizes.resize(_prop_buffer_sizes.size());
  std::transform(_prop_buffer_sizes.begin(), _prop_buffer_sizes.end(), _buffer_sizes.begin(), [](auto val) {
    return val * 1024 * 1024 * 1024UL;
  });

  return ret_val;
}

int EB::RU::config_buffers()
{
  int ret_val = DF_SUCCESS;
  _buffers.reserve(_n_sources_per_ru[_my_idx]);

  ret_val = check_buffers();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  try {
    if (_buffer_type[_my_idx] == dummy_MFP_buffer) {
      config_dummy();
    } else if (_buffer_type[_my_idx] == shmem_MFP_buffer) {
      config_shmem();
    } else if (_buffer_type[_my_idx] == PCIe40_frag_buffer) {
      config_PCIe40_frag();
    } else if (_buffer_type[_my_idx] == PCIe40_MFP_buffer) {
      config_PCIe40();
    } else {
      logger.error << __FUNCTION__ << " unsupported buffer type " << _buffer_type[_my_idx] << std::flush;
      ret_val = DF_ERROR;
      return ret_val;
    }

    if (_write_to_file[_my_idx]) {
      std::stringstream file_name;
      file_name << _out_file_prefix << "_" << _my_idx << ".mfp";
      _file_writer = std::move(File_writer<EB::MFP>(file_name.str()));
    }

  } catch (const std::exception& e) {
    logger.error << __FUNCTION__ << " unable to configure the output buffer " << RU_buffer_types(_buffer_type[_my_idx])
                 << " " << e.what() << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  } catch (...) {
    logger.error << __FUNCTION__ << " unexpected exception: output buffer type "
                 << RU_buffer_types(_buffer_type[_my_idx]) << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  }

  return ret_val;
}

int EB::RU::start()
{
  int ret_val = DF_SUCCESS;
  info("RU start");
  int sc = Transport_unit::start();
  if (sc != DF_SUCCESS) {
    return error("Failed to start Transport_unit base class.");
  }
  ret_val = config_buffers();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }
  // exchange srcids
  ret_val = send_src_ids();
  if (ret_val != DF_SUCCESS) return ret_val;
  _ibComm->ibDeregMRs(); // clear memory regions

  for (auto& buf : _buffers) {
    auto buflist = buf->get_full_buffer();
    for (std::tuple<void*, size_t>& list_it : buflist) {
      ret_val = _ibComm->addMR((char*) std::get<0>(list_it), std::get<1>(list_it));
      if (ret_val != DF_SUCCESS) {
        return ret_val;
      }
    }
  }
  logger.debug << "data MRs allocated" << std::flush;
  // mpfs and sizes per destination per src
  size_t mfp_vec_size = _bu_ranks.size() * _buffers.size();
  selected_MFPs.resize(mfp_vec_size);
  sizes.resize(mfp_vec_size);
  _n_frags_per_dst.resize(_bu_ranks.size());
  //  TO DO
  ret_val = _ibComm->addMR((char*) &sizes, sizes.size() * sizeof(uint32_t));
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }
  // END TO DO
  logger.debug << "size MRs allocated dim: " << mfp_vec_size << std::flush;
  std::lock_guard<decltype(_start_lock)> loop_guard(_start_lock);
  _send_MEPs = true;
  reset_counters();
  _end_of_run = true;
  return DF_SUCCESS;
}

int EB::RU::stop()
{
  int ret_val = DF_SUCCESS;
  logger.info << "stop" << std::flush;
  std::unique_lock<std::timed_mutex> loop_guard(_loop_lock, std::chrono::duration<int>(_stop_timeout));
  if (loop_guard) {
    logger.info << "stop executed properly" << std::flush;
  } else {
    logger.error << "Unable to execute a clean stop. Aborting" << std::flush;
    ret_val = DF_ERROR;
  }
  int sc = Transport_unit::stop();
  if (sc != DF_SUCCESS) {
    return error("Failed to stop Transport_unit base class.");
  }
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }
  // undo the buffer allocation done in start
  // TODO check if the buffers should be cleaned if stop aborts
  _buffers.clear();

  return ret_val;
}

int EB::RU::cancel()
{
  // TODO implement this
  std::lock_guard<decltype(_start_lock)> guard(_start_lock);
  logger.info << "cancel" << std::flush;
  _send_MEPs = false;
  Transport_unit::cancel();
  for (auto& buffer : _buffers) {
    buffer->cancel();
  }

  return DF_SUCCESS;
}

int EB::RU::pause()
{
  // _send_MEPs = false;
  // the EB does not go into pause
  return DF_SUCCESS;
}

int EB::RU::finalize() { return Transport_unit::finalize(); }

void EB::RU::handle(const DataflowIncident& inc)
{
  info("RU incident");
  //  TODO implement this
}

void EB::RU::config_dummy()
{
  // fast access to my config
  auto my_buffer_sizes_begin = _buffer_sizes.begin() + _prefix_n_sources_per_ru[_my_idx];
  auto my_buffer_sizes_end = _buffer_sizes.begin() + _prefix_n_sources_per_ru[_my_idx + 1];
  // config already checked

  // loads data from the MDF file
  MFP_preloader preloader;
  if (!preloader.set_MDF_filename(_MDF_filename.c_str())) {
    std::stringstream error_mess;
    error_mess << __FUNCTION__ << ": unable to open MDF file " << _MDF_filename << " " << strerror(errno);
    throw std::runtime_error(error_mess.str());
  }

  preloader.set_n_MFPs(_n_MFPs);

  std::vector<MFP_preloader::buffer_ptr_type> buffers(_n_sources_per_ru[_my_idx]);
  std::vector<EB::type_id_pair_type> source_mapping(_n_sources_per_ru[_my_idx]);
  for (int k = 0; k < _n_sources_per_ru[_my_idx]; k++) {
    void* tmp_ptr;
    posix_memalign(&tmp_ptr, sysconf(_SC_PAGE_SIZE), *(my_buffer_sizes_begin + k));
    if (tmp_ptr == NULL) {
      std::stringstream error_mess;
      error_mess << __FUNCTION__ << ": unable to allocate buffer " << k << " " << strerror(errno);
      throw std::runtime_error(error_mess.str());
    }

    buffers[k] = MFP_preloader::buffer_ptr_type(reinterpret_cast<char*>(tmp_ptr), free);
    source_mapping[k] = EB::new_src_id_to_old(_dummy_src_ids[_prefix_n_sources_per_ru[_my_idx] + k]);
  }

  preloader.set_detector_map(source_mapping);
  std::vector<size_t> tmp_sizes(my_buffer_sizes_begin, my_buffer_sizes_end);
  preloader.set_buffers(buffers, tmp_sizes);

  preloader.set_packing_factor(_n_fragment);

  if (preloader.preload_MFPs() < 0) {
    std::stringstream error_mess;
    error_mess << __FUNCTION__ << ": unable to preload MFPs from file " << _MDF_filename;
    throw std::runtime_error(error_mess.str());
  }

  auto buffs = preloader.get_buffers();

  for (int k = 0; k < _n_sources_per_ru[_my_idx]; k++) {
    _buffers.emplace_back(new Dummy_MFP_buffer(
      std::get<0>(buffs)[k], std::get<2>(buffs)[k], _dummy_src_ids[_prefix_n_sources_per_ru[_my_idx] + k]));
  }
}

void EB::RU::config_shmem()
{
  int local_idx;

  local_idx = get_idx_UTGID(RTL::processName(), "RU");

  int idx_offset = 0;
  while (local_idx > 0) {
    idx_offset += _n_sources_per_ru[_my_idx - local_idx];
    local_idx--;
  }

  for (int k = 0; k < _n_sources_per_ru[_my_idx]; k++) {
    std::stringstream shmem_name;
    shmem_name << _shmem_prefix << "_" << k + idx_offset;
    _buffers.emplace_back(new Shmem_buffer_reader<EB::MFP>(shmem_name.str().c_str()));
  }
}

int EB::RU::stream_select(int id)
{
  int stream;
  if (_SODIN_ID != id) {
    stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN;
  } else {
    stream = static_cast<int>(P40_DAQ_STREAM::P40_DAQ_STREAM_ODIN0) + _SODIN_stream;
  }

  return stream;
}

int EB::RU::stream_select(const std::string& name)
{
  int stream;
  if (_SODIN_name != name) {
    stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN;
  } else {
    // TODO add multi stream support based on the _SODIN_stream variable
    stream = static_cast<int>(P40_DAQ_STREAM::P40_DAQ_STREAM_ODIN0) + _SODIN_stream;
  }

  return stream;
}
void EB::RU::config_PCIe40()
{
  if (_PCIe40_ids.size() == _prefix_n_sources_per_ru[_ru_ranks.size()]) {
    logger.info << "PCIe40 selection by ID" << std::flush;
    // fast access to my config
    auto my_PCIe40_ids_begin = _PCIe40_ids.begin() + _prefix_n_sources_per_ru[_my_idx];
    auto my_PCIe40_ids_end = _PCIe40_ids.begin() + _prefix_n_sources_per_ru[_my_idx + 1];

    for (auto it = my_PCIe40_ids_begin; it != my_PCIe40_ids_end; it++) {
      _buffers.emplace_back(new PCIe40_MFP_reader(*it, stream_select(*it)));
    }
  } else if (_PCIe40_names.size() == _prefix_n_sources_per_ru[_ru_ranks.size()]) {
    logger.info << "PCIe40 selection by name" << std::flush;
    // fast access to my config
    auto my_PCIe40_names_begin = _PCIe40_names.begin() + _prefix_n_sources_per_ru[_my_idx];
    auto my_PCIe40_names_end = _PCIe40_names.begin() + _prefix_n_sources_per_ru[_my_idx + 1];

    for (auto it = my_PCIe40_names_begin; it != my_PCIe40_names_end; it++) {
      _buffers.emplace_back(new PCIe40_MFP_reader(*it, stream_select(*it)));
    }
  } else {
    // In this set of functions (config_buffers) the errors are handled via expections
    std::ostringstream err_mess;
    err_mess << "RU " << _my_idx << " " << __FUNCTION__
             << ": Invalid PCIe40 configuration: not enough IDs or names provided. Got " << _PCIe40_names.size()
             << " names and " << _PCIe40_ids.size() << " ids for " << _prefix_n_sources_per_ru[_ru_ranks.size()]
             << " data sources";
    throw std::runtime_error(err_mess.str());
  }
}

void EB::RU::config_PCIe40_frag()
{
  // fast access to my config
  // buffer_sizes_are checked in the buffer_check method
  auto my_buffer_sizes_it = _buffer_sizes.begin() + _prefix_n_sources_per_ru[_my_idx];
  auto my_buffer_sizes_end = _buffer_sizes.begin() + _prefix_n_sources_per_ru[_my_idx + 1];
  auto my_buffer_sizes_size = std::distance(my_buffer_sizes_it, my_buffer_sizes_end);

  if (_PCIe40_ids.size() == _prefix_n_sources_per_ru[_ru_ranks.size()]) {
    auto my_PCIe40_ids_it = _PCIe40_ids.begin() + _prefix_n_sources_per_ru[_my_idx];
    for (int k = 0; k < my_buffer_sizes_size; k++) {
      _buffers.emplace_back(new PCIe40_frag_reader(
        *(my_PCIe40_ids_it),
        *(my_buffer_sizes_it),
        _n_fragment,
        EB::default_PCIe40_alignment,
        stream_select(*my_PCIe40_ids_it)));
      my_PCIe40_ids_it++;
      my_buffer_sizes_it++;
    }
  } else if (_PCIe40_names.size() == _prefix_n_sources_per_ru[_ru_ranks.size()]) {
    auto my_PCIe40_names_it = _PCIe40_names.begin() + _prefix_n_sources_per_ru[_my_idx];
    for (int k = 0; k < my_buffer_sizes_size; k++) {
      _buffers.emplace_back(new PCIe40_frag_reader(
        *(my_PCIe40_names_it),
        *(my_buffer_sizes_it),
        _n_fragment,
        EB::default_PCIe40_alignment,
        stream_select(*my_PCIe40_names_it)));
      my_PCIe40_names_it++;
      my_buffer_sizes_it++;
    }
  } else {
    // In this set of functions (config_buffers) the errors are handled via expections
    std::ostringstream err_mess;
    err_mess << "RU " << _my_idx << " " << __FUNCTION__
             << ": Invalid PCIe40 configuration: not enough IDs or names provided. Got " << _PCIe40_names.size()
             << " names and " << _PCIe40_ids.size() << " ids for " << _prefix_n_sources_per_ru[_ru_ranks.size()]
             << " data sources";
    throw std::runtime_error(err_mess.str());
  }
}

int EB::RU::init_shift()
{
  // precomputes the linear shift pattern
  // this is the shift phase of the current unit i.e. the RU block index plus the number of ghost before the unit
  // itself
  int ru_idx = 0;
  int ru_block_idx = 0;
  while (ru_idx + _n_rus_per_nic[ru_block_idx] <= _my_idx) {
    ru_idx += _n_rus_per_nic[ru_block_idx];
    ru_block_idx++;
  }

  _shift_offset = _shift_pattern;
  int shift_idx = get_idx(_shift_offset.begin(), _shift_offset.end(), ru_block_idx);

  logger.debug << "shift idx " << shift_idx << std::flush;

  std::rotate(_shift_offset.begin(), _shift_offset.begin() + shift_idx, _shift_offset.end());

  if (logger.is_active(Online::PrintLevel::DEBUG)) {
    logger.debug << "shift vectors: ";
    for (auto elem : _shift_offset) {
      logger.debug << elem << " ";
    }

    logger.debug << std::flush;
  }

  return DF_SUCCESS;
}

int EB::RU::run()
{
  // Transport_unit::run();
  info("RU run");
  int ret_val = DF_SUCCESS;
  // TODO check if this is a good idea we may don't want to flush the buffers async
  if ((_buffer_type[_my_idx] == PCIe40_frag_buffer) || (_buffer_type[_my_idx] == PCIe40_MFP_buffer)) {
    for (const auto& buffer : _buffers) {
      buffer->flush();
    }
  }
  // reset the number of MFPs written to file
  _n_MFPs_written_to_file = 0;
  _received_data = std::vector<bool>(_n_sources_per_ru[_my_idx], false);
  _send_empty = false;
  bw_monitor.reset();
  total_timer.start();
  while (_send_MEPs.load()) {
    // TODO add monitoring counters reset
    std::lock_guard<decltype(_loop_lock)> loop_guard(_loop_lock);
    // TODO add counter rest on end of run

    logger.debug << "load" << std::flush;
    load_mpfs_timer.start();
    // TODO check for race conditions when start is executed
    if (_send_empty) {
      ret_val = load_empty();
      _send_empty = false;
    } else {
      ret_val = load_mfps();
    }
    load_mpfs_timer.stop();

    // DF_CANCELLED means that the STOP signal has been received
    // DF_ERROR will trigger a transition into ERROR
    if (ret_val != DF_SUCCESS) {
      break;
    }
    // init the barrier

    logger.debug << "send size" << std::flush;
    send_sizes_timer.start();
    ret_val = send_sizes();
    if (ret_val != DF_SUCCESS) {
      break;
    }
    send_sizes_timer.stop();

    logger.debug << "shift" << std::flush;
    linear_shift_timer.start();
    ret_val = linear_shift();
    if (ret_val != DF_SUCCESS) {
      break;
    }
    // DEBUG duplicated events at stop
    load_empty();
    linear_shift_timer.stop();

    if (logger.is_active(Online::PrintLevel::DEBUG)) {
      logger.debug << "sleep" << std::flush;
      sleep(1);
    }

    total_timer.stop();
    if (bw_monitor.get_elapsed_time() > 5) {
      // TODO remove the reset from the print
      double bw = bw_monitor.get_bw_and_reset();

      double total_time = total_timer.get_elapsed_time_s();
      double load_mpfs_time = load_mpfs_timer.get_elapsed_time_s();
      double send_sizes_time = send_sizes_timer.get_elapsed_time_s();
      double linear_shift_time = linear_shift_timer.get_elapsed_time_s();
      double send_time = send_timer.get_elapsed_time_s();
      double sync_time = sync_timer.get_elapsed_time_s();

      if (logger.is_active(Online::PrintLevel::INFO)) {
        logger.info << "Status report" << std::flush;
        logger.info << " BW " << bw << " Gb/s" << std::flush;
        logger.info << " total " << total_time << " s " << total_time / total_time * 100 << " %" << std::flush;
        logger.info << " load " << load_mpfs_time << " s " << load_mpfs_time / total_time * 100 << " %" << std::flush;
        logger.info << " send size " << send_sizes_time << " s " << send_sizes_time / total_time * 100 << " %"
                    << std::flush;
        logger.info << " linear shift " << linear_shift_time << " s " << linear_shift_time / total_time * 100 << " %"
                    << std::flush;
        logger.info << " send data " << send_time << " s " << send_time / total_time * 100 << " %" << std::flush;
        logger.info << " sync " << sync_time << " s " << sync_time / total_time * 100 << " %" << std::flush;
      }

      total_timer.reset();
      load_mpfs_timer.reset();
      send_sizes_timer.reset();
      linear_shift_timer.reset();
      send_timer.reset();
      sync_timer.reset();
    }
    total_timer.start();
    _run_loop_iteration++;
  }
  total_timer.stop();

  // this will trigger the transition of the FSM into error
  if (ret_val == DF_ERROR) {
    fireIncident("DAQ_ERROR");
  }

  return ret_val;
}

int EB::RU::send_sizes()
{
  return _ibComm->ibScatterV(reinterpret_cast<char*>(sizes.data()), _buffers.size(), sizeof(uint32_t), _bu_ranks);
}

int EB::RU::send_src_ids()
{
  int ret_val = DF_SUCCESS;
  std::vector<EB::src_id_type> temp;
  temp.reserve(_n_sources_per_ru[_my_idx]);
  for (const auto& buff : _buffers) {
    temp.push_back(buff->get_src_id());
  }
  logger.debug << "send src ids " << temp.size() << std::flush;
  // register mr
  ret_val = _ibComm->addMR(reinterpret_cast<char*>(temp.data()), temp.size() * sizeof(EB::src_id_type));
  if (ret_val != DF_SUCCESS) {
    logger.error << __FUNCTION__ << " unable to add memory region ptr " << temp.data() << " size " << temp.size()
                 << std::flush;
    return ret_val;
  }

  ret_val =
    _ibComm->ibBroadcastV(reinterpret_cast<char*>(temp.data()), temp.size(), sizeof(EB::src_id_type), _bu_ranks);
  if (ret_val != DF_SUCCESS) {
    logger.error << __FUNCTION__ << " src id broadcast failed" << std::flush;
    return ret_val;
  }
  return ret_val;
}

int EB::RU::linear_shift()
{
  int ret_val = DF_SUCCESS;

  for (const auto& shift : _shift_offset) {
    // sync mpi barrier
    sync_timer.start();
    logger.debug << "sync" << std::flush;
    ret_val = sync();
    if (ret_val != DF_SUCCESS) {
      return ret_val;
    }
    sync_timer.stop();
    logger.debug << "on shift " << shift << std::flush;
    send_timer.start();
    // skip the ghost nodes
    if (shift != -1) {
      ret_val = send_MFPs(shift);
      if (ret_val != DF_SUCCESS) {
        return ret_val;
      }
    }
    send_timer.stop();
  }
  logger.debug << "send complete" << std::flush;
  // TODO check if this is the best place for this piece of code
  for (int src = 0; src < _buffers.size(); src++) {
    _buffers[src]->read_complete();
  }
  return ret_val;
}

int EB::RU::send_MFPs(int shift_off)
{
  int ret_val = DF_SUCCESS;
  int comm_err;
  int MFPs_size = _buffers.size();
  // this may be done by shift_offset[shift]
  int dst = _bu_ranks[shift_off];
  int shift_idx = shift_off * _buffers.size();
  std::vector<comm_send> send_vec(MFPs_size);
  size_t total_size = 0;
  for (int k = 0; (k < MFPs_size); k++) {
    if (logger.is_active(Online::PrintLevel::DEBUG)) {
      logger.debug << "Comm_Send MFPs " << k << " size " << sizes[shift_idx + k] << " dst " << dst << "\n";
      if (selected_MFPs[shift_idx + k]) {
        if (logger.is_active(Online::PrintLevel::VERBOSE)) {
          logger.verbose << reinterpret_cast<const EB::MFP*>(selected_MFPs[shift_idx + k])->print(false);
          logger.verbose << std::flush;
        } else {
          logger.debug << reinterpret_cast<const EB::MFP*>(selected_MFPs[shift_idx + k])->print(true);
        }
      }
      logger.debug << std::flush;
    }
    auto& send = send_vec.at(k);
    send.snd_buff = reinterpret_cast<const void*>(selected_MFPs[shift_idx + k]);
    send.count = sizes[shift_idx + k];
    send.datatype = sizeof(char);
    send.destination = dst;
    total_size += sizes[shift_idx + k];
  }
  comm_err = _ibComm->send(send_vec);
  if (comm_err == DF_CANCELLED) {
    ret_val = comm_err;
  } else if (comm_err != DF_SUCCESS) {
    logger.error << __FUNCTION__ << " send " << comm_err << std::flush;
    ret_val = comm_err;
  }
  // k=0 first source is taken into account to the event flow
  if (selected_MFPs[shift_idx] != 0) {
    _DF_events_out += reinterpret_cast<const EB::MFP*>(selected_MFPs[shift_idx])->header.n_banks;
  }

  _MFP_count += _buffers.size();
  bw_monitor.add_sent_bytes(total_size);
  return ret_val;
}

int EB::RU::load_mfps()
{
  int ret_val = DF_SUCCESS;
  std::vector<EB::MFP*> ptrs(_bu_ranks.size());
  for (int src = 0; src < _buffers.size(); src++) {
    int dst;
    for (dst = 0; dst < _bu_ranks.size(); dst++) {
      EB::MFP* next_MFP = NULL;
      EB::Timer idle_timer;
      idle_timer.start();
      while ((next_MFP == NULL) && _send_MEPs) {
        try {
          next_MFP = _buffers[src]->try_get_element();
        } catch (const std::exception& e) {
          logger.error << __FUNCTION__ << " Exception while reading buffer " << src << " src ID "
                       << _buffers[src]->get_src_id() << " " << e.what() << std::flush;
          ret_val = DF_ERROR;
          return ret_val;
        } catch (...) {
          logger.error << __FUNCTION__ << " unexpected exception while reading buffer " << src << " src ID "
                       << _buffers[src]->get_src_id() << std::flush;
          ret_val = DF_ERROR;
          return ret_val;
        }

        if (next_MFP == NULL) {
          usleep(1);
          if (idle_timer.get_elapsed_time_s() > 5) {
            logger.warning << __FUNCTION__ << " no data from buffer " << src << " src ID "
                           << _buffers[src]->get_src_id() << std::flush;
            idle_timer.reset();
          }
        } else if (next_MFP->is_end_run() && !_received_data[src]) {
          // We skip all the END of RUNs received before any valid data
          next_MFP = NULL;
        }
      }

      if (!_send_MEPs) {
        logger.info << "STOP received cancelling pending IO" << std::flush;
        return DF_CANCELLED;
      }

      if (!next_MFP->is_header_valid()) {
        // TODO check with Paolo the severity of this error
        logger.warning << "invalid MFP from src " << _buffers[src]->get_src_id() << std::flush;
        _DF_events_err += 1;
      } else {
        if (next_MFP->is_end_run()) {
          // the end of run will trigger a round of zero-sized MFPs
          _end_of_run = true;
          _send_empty = true;
          logger.warning << "end of run" << std::flush;
          break;
        } else {
          // TODO check if this keeps coherency in case of truncated events
          // fragments from multiple data sources are counted only once
          if (src == 0) {
            _DF_events_in += next_MFP->header.n_banks;
          }

          // assert the flag we received at least one MFP
          _received_data[src] = true;

          if (_end_of_run) {
            // reset performance counters when a new run starts
            reset_counters();
          }
          _end_of_run = false;
        }
      }

      if (logger.is_active(Online::PrintLevel::DEBUG)) {
        logger.debug << "NEXT MFP\n";
        if (logger.is_active(Online::PrintLevel::VERBOSE)) {
          logger.verbose << next_MFP->print(true);
          logger.verbose << std::flush;
        } else {
          logger.debug << next_MFP->print(false);
        }
        logger.debug << std::flush;
      }

      // Write MFPs to file for debug

      if (_write_to_file[_my_idx] && (_n_MFPs_written_to_file < _n_MFPs_to_file)) {
        // TODO add error checking
        _file_writer.write(next_MFP);
        _n_MFPs_written_to_file++;
      }

      int idx = dst * _buffers.size() + src;
      selected_MFPs[idx] = reinterpret_cast<const char*>(next_MFP);
      sizes[idx] = next_MFP->bytes();
    }

    // All the missing slots are filled up with zero-sized MFPs
    for (int k = dst; k < _bu_ranks.size(); k++) {
      int idx = dst * _buffers.size() + src;
      selected_MFPs[idx] = NULL;
      sizes[idx] = 0;
    }
  }

  return ret_val;
}

// Placeholder no performance counters implemented yet
void EB::RU::reset_counters()
{
  // reset counters of the base class
  Transport_unit::reset_counters();
  logger.info << "resetting counters" << std::flush;
  _DF_events_out = 0;
  _DF_events_in = 0;
  _DF_events_err = 0;
  _run_loop_iteration = 0;
}

int EB::RU::load_empty()
{
  for (int src = 0; src < _buffers.size(); src++) {
    for (int dst = 0; dst < _bu_ranks.size(); dst++) {
      int idx = dst * _buffers.size() + src;
      selected_MFPs[idx] = NULL;
      sizes[idx] = 0;
    }
  }
  return DF_SUCCESS;
}

int EB::RU::init_dummy_src_ids()
{
  int ret_val = DF_SUCCESS;

  if (_dummy_src_ids.size() == 0) {
    _dummy_src_ids.resize(_prefix_n_sources_per_ru[_ru_ranks.size()]);
    // if src_ids are not assigned values from 0 to n-1 are used
    std::iota(_dummy_src_ids.begin(), _dummy_src_ids.end(), 0);
  }

  if (logger.is_active(PrintLevel::DEBUG)) {
    logger.debug << "src ids: ";
    for (const auto& elem : _dummy_src_ids) {
      logger.debug << elem << " ";
    }
    logger.debug << std::flush;
  }

  // check config

  if (_dummy_src_ids.size() != _prefix_n_sources_per_ru.back()) {
    logger.error << __FUNCTION__ << " configuration error: incorrect number of src IDs. Got " << _dummy_src_ids.size()
                 << " IDs for " << _prefix_n_sources_per_ru.back() << " sources" << std::flush;
    return DF_ERROR;
  }

  return ret_val;
}
