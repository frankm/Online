#include "timer.hpp"
#include "EventBuilding/tools.hpp"
#include <stdexcept>
#include <cstring>

EB::Timer::Timer(clockid_t clk_id)
{
  _clock_id = clk_id;
  reset();
  _is_running = false;
}

clockid_t EB::Timer::get_clk_id() const { return _clock_id; }

void EB::Timer::set_clk_id(clockid_t clk_id)
{
  _clock_id = clk_id;
  reset();
}

void EB::Timer::start()
{
  _is_running = true;
  int error = clock_gettime(_clock_id, &_start_time);
  if (error != 0) {
    throw std::runtime_error(strerror(error));
  }
}

void EB::Timer::stop()
{
  _elapsed_time = get_elapsed_time_ns();
  _is_running = false;
}

void EB::Timer::reset()
{
  int error = clock_gettime(_clock_id, &_start_time);
  if (error != 0) {
    throw std::runtime_error(strerror(error));
  }

  _elapsed_time = 0;
}

clock_t EB::Timer::get_elapsed_time_ns() const
{
  clock_t ret_val = _elapsed_time;
  if (_is_running) {
    timespec stop_time;
    int error = clock_gettime(_clock_id, &stop_time);
    if (error != 0) {
      throw std::runtime_error(strerror(error));
    }

    ret_val += timespec_diff_ns(stop_time, _start_time);
  }

  return ret_val;
}

double EB::Timer::get_elapsed_time_s() const { return get_elapsed_time_ns() / 1.e9; }