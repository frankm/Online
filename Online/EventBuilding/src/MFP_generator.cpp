#include <sstream>
#include <stdexcept>
#include <random>
#include <iostream>
#include <string>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <unistd.h>
#include <cassert>
#include "EventBuilding/tools.hpp"
#include "EventBuilding/MFP_generator.hpp"
#include "RTL/rtl.h"
#include "numa.h"
#include "PCIE40Data/RawBank40.h"
#include "PCIE40Data/sodin.h"

EB::MFP_generator::MFP_generator(const std::string& nam, Context& ctxt) : DataflowComponent(nam, ctxt)
{
  logger.set_level(Online::PrintLevel(outputLevel));
  logger.set_name(name);
  declareProperty("n_banks", _prop_n_banks = 3000);
  declareProperty("src_id", _prop_src_id = "0");
  declareProperty("align", _prop_align = 2); // log2 of base mfp alignment
  declareProperty("block_version", _prop_block_version = 0);
  declareProperty("event_rate", _event_rate = 30e6);
  declareProperty("bank_type", _prop_bank_type = 0);
  declareProperty("bank_size", _prop_bank_size = 100);
  declareProperty("random_sizes", _random_sizes = false);
  declareProperty("random_seed", _random_seed = 0);
  declareProperty("shmem_prefix", _shmem_name = "RU");
  declareProperty("numa_layout", _numa_layout);
  declareProperty("buffer_size", _prop_buffer_size = 1);
  declareProperty("reconnect", _reconnect = true);
  declareProperty("stop_timeout", _stop_timeout = 10);
  declareProperty("n_preloaded_sizes", _n_preloaded_sizes = 1000000);
  declareProperty("enable_test_pattern", _enable_test_pattern = false);

  declareProperty("SODIN_UTGUID", _sodin_utguid = "");

  // SD specific props
  declareProperty("sd_specific_run", _sub_detector_specific_run = false);
  declareProperty("velo_UTGUID_override", _velo_utigid_override);
  declareProperty("scifi_UTGUID_override", _scifi_utigid_override);
  declareProperty("ut_UTGUID_override", _ut_utigid_override);
  declareProperty("muon_UTGUID_override", _muon_utigid_override);
  declareProperty("hcal_UTGUID_override", _hcal_utigid_override);
  declareProperty("ecal_UTGUID_override", _ecal_utigid_override);
  declareProperty("rich1_UTGUID_override", _rich1_utigid_override);
  declareProperty("rich2_UTGUID_override", _rich2_utigid_override);

  // generic sd mean 150
  declareProperty("rand_lambda", _lambda = 2);
  declareProperty("rand_offset", _offset = 150 - 20 * 2);
  declareProperty("rand_multi", _multi = 20);

  // velo mean 156
  declareProperty("velo_rand_lambda", _velo_lambda = 2);
  declareProperty("velo_rand_offset", _velo_offset = 156 - 20 * 2);
  declareProperty("velo_rand_multi", _velo_multi = 20);

  // scifi mean 100
  declareProperty("scifi_rand_lambda", _scifi_lambda = 2);
  declareProperty("scifi_rand_offset", _scifi_offset = 100 - 20 * 2);
  declareProperty("scifi_rand_multi", _scifi_multi = 20);

  // ut mean 100
  declareProperty("ut_rand_lambda", _ut_lambda = 2);
  declareProperty("ut_rand_offset", _ut_offset = 100 - 20 * 2);
  declareProperty("ut_rand_multi", _ut_multi = 20);

  // muon mean 156
  declareProperty("muon_rand_lambda", _muon_lambda = 2);
  declareProperty("muon_rand_offset", _muon_offset = 156 - 20 * 2);
  declareProperty("muon_rand_multi", _muon_multi = 20);

  // hcal mean 156
  declareProperty("hcal_rand_lambda", _hcal_lambda = 2);
  declareProperty("hcal_rand_offset", _hcal_offset = 156 - 20 * 2);
  declareProperty("hcal_rand_multi", _hcal_multi = 20);

  // hcal mean 156
  declareProperty("ecal_rand_lambda", _ecal_lambda = 2);
  declareProperty("ecal_rand_offset", _ecal_offset = 156 - 20 * 2);
  declareProperty("ecal_rand_multi", _ecal_multi = 20);

  // rich1 mean 166
  declareProperty("rich1_rand_lambda", _rich1_lambda = 2);
  declareProperty("rich1_rand_offset", _rich1_offset = 166 - 20 * 2);
  declareProperty("rich1_rand_multi", _rich1_multi = 20);

  // rich2 mean 166
  declareProperty("rich2_rand_lambda", _rich2_lambda = 2);
  declareProperty("rich2_rand_offset", _rich2_offset = 166 - 20 * 2);
  declareProperty("rich2_rand_multi", _rich2_multi = 20);

  // DF monitoring counters
  declareMonitor("Events/IN", _DF_events_in, "Number events received in the RUN");
  declareMonitor("Events/OUT", _DF_events_out, "Number events processed and sent in RUN");
  declareMonitor("Events/ERROR", _DF_events_err, "Number events with errors in the RUN");
}

EB::MFP_generator::~MFP_generator() {}

int EB::MFP_generator::initialize()
{
  int sc = Component::initialize();
  _generator.seed(_random_seed + 2 * getpid());
  if (sc != Online::DF_SUCCESS) {
    return error("Failed to initialize service base class.");
  }

  logger.set_level(Online::PrintLevel(outputLevel));

  try {
    _local_id = get_idx_UTGID(RTL::processName(), "MFPGen");
  } catch (const std::runtime_error& e) {
    logger.error << "Unable to set local ID from UTGID: " << e.what() << std::flush;
    return Online::DF_ERROR;
  }

  std::stringstream shmem_name_sstream;
  shmem_name_sstream << _shmem_name << "_" << _local_id;
  _shmem_name = shmem_name_sstream.str();

  std::stringstream logger_name;
  logger_name << logger.get_name() << " " << _shmem_name;
  logger.set_name(logger_name.str());

  _generate = false;

  sc = config_numa();
  if (sc != Online::DF_SUCCESS) {
    return sc;
  }

  // TODO add overflow/underflow detection
  _n_banks = _prop_n_banks;
  _src_id = std::stoi(_prop_src_id);
  _align = _prop_align;
  _bank_size = _prop_bank_size;
  _block_version = _prop_block_version;
  _bank_type = _prop_bank_type;
  _buffer_size = _prop_buffer_size * 1024 * 1024 * 1024UL;
  // interval between the generation of one block and the next one in ns
  _sleep_interval_ns = _n_banks / (_event_rate * 1e-9);
  logger.info << "sleep interval " << _sleep_interval_ns << " ns" << std::flush;
  if (!std::isfinite(_sleep_interval_ns) || (_sleep_interval_ns <= 0.)) {
    logger.warning << "invalid event rate events will be generated at maximum rate" << std::flush;
    _sleep_interval_ns = 0;
  }

  logger.info << "event_rate " << _event_rate << std::flush;

  try {
    logger.debug << "Opening shmem buffer " << _shmem_name << std::flush;
    _buffer.reset_backend(std::move(
      Shared_mem_buffer_backend(_shmem_name.c_str(), true, _reconnect, true, _buffer_size, 12, _src_id, _numa_node)));
  } catch (const std::exception& e) {
    logger.error << __FUNCTION__ << " unable to configure the output buffer " << _shmem_name << " " << e.what()
                 << std::flush;
    return Online::DF_ERROR;
  } catch (...) {
    logger.error << __FUNCTION__ << " unexpected exception: output buffer " << _shmem_name << std::flush;
    return Online::DF_ERROR;
  }

  _is_sodin = string_icompare(RTL::processName(), _sodin_utguid) == 0;

  if (_is_sodin) {
    init_sodin();
  } else {
    // SODIN doesn't behave like the other SDs

    sc = init_sd();
    if (sc != Online::DF_SUCCESS) {
      return sc;
    }

    if (_sub_detector_specific_run) {
      init_sd_run();
    }

    init_sizes();
  }

  return Online::DF_SUCCESS;
}

void EB::MFP_generator::init_sodin()
{
  _bank_size = sizeof(Online::pcie40::sodin_t);
  _bank_sizes.resize(_n_banks);

  std::fill(_bank_sizes.begin(), _bank_sizes.end(), _bank_size);

  _bank_type = Online::RawBank40::ODIN;

  _run_number = 0;
}

int EB::MFP_generator::init_sd()
{
  int ret_val = Online::DF_SUCCESS;
  if (
    std::find(_velo_utigid_override.begin(), _velo_utigid_override.end(), RTL::processName()) !=
    _velo_utigid_override.end()) {
    _sub_detector = "VA";
    logger.info << "Overriding default SD with Velo" << std::flush;
  } else if (
    std::find(_scifi_utigid_override.begin(), _scifi_utigid_override.end(), RTL::processName()) !=
    _scifi_utigid_override.end()) {
    _sub_detector = "SA";
    logger.info << "Overriding default SD with SciFi" << std::flush;
  } else if (
    std::find(_ut_utigid_override.begin(), _ut_utigid_override.end(), RTL::processName()) !=
    _ut_utigid_override.end()) {
    _sub_detector = "UA";
    logger.info << "Overriding default SD with UT" << std::flush;
  } else if (
    std::find(_muon_utigid_override.begin(), _muon_utigid_override.end(), RTL::processName()) !=
    _muon_utigid_override.end()) {
    _sub_detector = "MA";
    logger.info << "Overriding default SD with Muon" << std::flush;
  } else if (
    std::find(_hcal_utigid_override.begin(), _hcal_utigid_override.end(), RTL::processName()) !=
    _hcal_utigid_override.end()) {
    _sub_detector = "HC";
    logger.info << "Overriding default SD with HCal" << std::flush;
  } else if (
    std::find(_ecal_utigid_override.begin(), _ecal_utigid_override.end(), RTL::processName()) !=
    _ecal_utigid_override.end()) {
    _sub_detector = "EC";
    logger.info << "Overriding default SD with ECal" << std::flush;
  } else if (
    std::find(_rich1_utigid_override.begin(), _rich1_utigid_override.end(), RTL::processName()) !=
    _rich1_utigid_override.end()) {
    _sub_detector = "R1";
    logger.info << "Overriding default SD with Rich1" << std::flush;
  } else if (
    std::find(_rich2_utigid_override.begin(), _rich2_utigid_override.end(), RTL::processName()) !=
    _rich2_utigid_override.end()) {
    _sub_detector = "R2";
    logger.info << "Overriding default SD with Rich2" << std::flush;
  } else {
    try {
      _sub_detector = get_sub_detector_UTGID(RTL::processName(), "MFPGen");
    } catch (const std::runtime_error& e) {
      logger.error << "Unable to set sub detector from UTGID: " << e.what() << std::flush;
      ret_val = Online::DF_ERROR;
    }
  }

  return ret_val;
}

void EB::MFP_generator::init_sd_run()
{
  // currently the TD machines are replacing VC ones
  if (_sub_detector == "VA" || _sub_detector == "VC" || _sub_detector == "TD") {
    logger.info << "SD selected: Velo" << std::flush;
    _lambda = _velo_lambda;
    _offset = _velo_offset;
    _multi = _velo_multi;
    // TODO double check the bank types with Roel
    _bank_type = Online::RawBank40::VP;
  } else if (_sub_detector == "SA" || _sub_detector == "SC") {
    logger.info << "SD selected: SciFi" << std::flush;
    _lambda = _scifi_lambda;
    _offset = _scifi_offset;
    _multi = _scifi_multi;
    // TODO double check the bank types with Roel
    _bank_type = Online::RawBank40::FTCluster;
  } else if (_sub_detector == "UA" || _sub_detector == "UC") {
    logger.info << "SD selected: UT" << std::flush;
    _lambda = _ut_lambda;
    _offset = _ut_offset;
    _multi = _ut_multi;
    // TODO double check the bank types with Roel
    _bank_type = Online::RawBank40::UT;
  } else if (_sub_detector == "MA" || _sub_detector == "MC") {
    logger.info << "SD selected: Muon" << std::flush;
    _lambda = _muon_lambda;
    _offset = _muon_offset;
    _multi = _muon_multi;
    // TODO double check the bank types with Roel
    _bank_type = Online::RawBank40::Muon;
  } else if (_sub_detector == "HC") {
    logger.info << "SD selected: HCal" << std::flush;
    _lambda = _hcal_lambda;
    _offset = _hcal_offset;
    _multi = _hcal_multi;
    // TODO double check the bank types with Roel
    _bank_type = Online::RawBank40::HcalPacked;
  } else if (_sub_detector == "EC") {
    logger.info << "SD selected: ECal" << std::flush;
    _lambda = _ecal_lambda;
    _offset = _ecal_offset;
    _multi = _ecal_multi;
    // TODO double check the bank types with Roel
    _bank_type = Online::RawBank40::EcalPacked;
  } else if (_sub_detector == "R1") {
    logger.info << "SD selected: Rich1" << std::flush;
    _lambda = _rich1_lambda;
    _offset = _rich1_offset;
    _multi = _rich1_multi;
    // TODO double check the bank types with Roel
    _bank_type = Online::RawBank40::Rich;
  } else if (_sub_detector == "R2") {
    logger.info << "SD selected: Rich2" << std::flush;
    _lambda = _rich2_lambda;
    _offset = _rich2_offset;
    _multi = _rich2_multi;
    // TODO double check the bank types with Roel
    _bank_type = Online::RawBank40::Rich;
  } else {
    logger.warning << "Unknown sub detector " << _sub_detector << std::flush;
  }

  logger.info << "rand distr params: lambda=" << _lambda << " offset=" << _offset << " multi=" << _multi << std::flush;
}

void EB::MFP_generator::init_sizes()
{
  _bank_sizes.resize(_n_preloaded_sizes);

  if (_random_sizes) {
    // padding data properly
    // TODO add variable distribution
    // auto random_distribution = std::uniform_real_distribution<float>(80, 210);
    auto random_distribution = std::poisson_distribution<>(_lambda);

    for (auto& elem : _bank_sizes) {
      size_t random_size = random_distribution(_generator) * _multi + _offset;
      if (random_size <= 0) {
        // 1 ensures that at least one aligned word
        random_size = 1;
      }
      elem = random_size + get_padding(random_size, 1 << _align);
    }
  } else {
    // auto padded_size = _bank_size + get_padding(_bank_size, 1 << _align);
    std::fill(_bank_sizes.begin(), _bank_sizes.end(), _bank_size);
  }

  // if (logger.is_active(Online::PrintLevel::DEBUG)) {
  logger.info << "Generated sizes: ";
  for (int k = _bank_sizes.size() - 10; k < _bank_sizes.size(); k++) {
    if (k >= 0) {
      logger.info << _bank_sizes[k] << ", ";
    }
  }
  logger.info << std::flush;
}

int EB::MFP_generator::start()
{
  // TODO check if we need to flush the buffer
  _ev_id = 0;
  _last_report_ev_id = 0;
  _total_timer.reset();
  _random_gen_timer.reset();
  _MFP_write_timer.reset();
  _dead_time_timer.reset();

  std::lock_guard<decltype(_start_lock)> loop_guard(_start_lock);

  // reset MON counters
  _DF_events_out = 0;
  _DF_events_in = 0;
  _DF_events_err = 0;

  _generate = true;

  return Online::DF_SUCCESS;
}

int EB::MFP_generator::run()
{
  _ev_id = 0;
  std::lock_guard<decltype(_loop_lock)> loop_guard(_loop_lock);
  _total_timer.start();
  _MFP_write_timer.start();
  while (_generate) {
    _MFP_write_timer.reset();
    write_MFP();
    // Active wait
    while (_MFP_write_timer.get_elapsed_time_ns() <= _sleep_interval_ns)
      ;

    if (_total_timer.get_elapsed_time_s() > 5) {
      _total_timer.stop();
      auto total_time = _total_timer.get_elapsed_time_s();
      auto dead_time = _dead_time_timer.get_elapsed_time_s();
      auto random_gen_time = _random_gen_timer.get_elapsed_time_s();
      auto ev_rate = (double) (_ev_id - _last_report_ev_id) / total_time;
      logger.info << "EV rate " << ev_rate << std::flush;
      logger.info << "dead time " << dead_time << " s " << dead_time / total_time * 100 << "%" << std::flush;
      logger.info << "random gen time " << random_gen_time << " s " << random_gen_time / total_time * 100 << "%"
                  << std::flush;
      _total_timer.reset();
      _random_gen_timer.reset();
      _dead_time_timer.reset();
      _last_report_ev_id = _ev_id;
      _total_timer.start();
    }
  }
  _total_timer.stop();
  _MFP_write_timer.stop();

  write_end_run();

  return Online::DF_SUCCESS;
}

void EB::MFP_generator::write_MFP()
{
  EB::MFP* mfp;

  size_t payload_size = 0; // _bank_size * _n_banks;

  std::vector<EB::bank_size_type> local_sizes(_n_banks);

  _random_gen_timer.start();
  if (!_is_sodin) {
    auto rand_distribution = std::uniform_int_distribution<int>(0, _bank_sizes.size() - 1);
    for (auto& elem : local_sizes) {
      elem = _bank_sizes[rand_distribution(_generator)];
      payload_size += elem + get_padding(elem, 1 << _align);
    }
  } else {
    local_sizes = _bank_sizes;
    payload_size = (_bank_sizes[0] + get_padding(_bank_sizes[0], 1 << _align)) * _n_banks;
  }
  _random_gen_timer.stop();

  size_t header_size = EB::MFP_header_size(_n_banks);

  _dead_time_timer.start();
  mfp = _buffer.write_next_element(payload_size + header_size);
  _dead_time_timer.stop();
  if (mfp != NULL) {
    if (_enable_test_pattern) {
      memset(mfp, 0xdd, payload_size + header_size);
    }
    mfp->header.magic = EB::MFP_magic;
    mfp->header.n_banks = _n_banks;
    mfp->header.packet_size = payload_size + header_size;
    mfp->header.ev_id = _ev_id;
    mfp->header.src_id = _src_id;
    mfp->header.align = _align;
    mfp->header.block_version = _block_version;
    auto types = mfp->header.bank_types();
    auto sizes = mfp->header.bank_sizes();
    std::copy(local_sizes.begin(), local_sizes.end(), sizes);
    std::fill(types, types + _n_banks, _bank_type);

    // test pattern has higher priority
    if (_enable_test_pattern) {
      write_test_pattern(mfp);
    } else if (_is_sodin) {
      write_ODIN_banks(mfp);
    }

    _DF_events_in += _n_banks;

    logger.debug << "MFP size " << mfp->bytes() << std::flush;

    _buffer.write_complete();
    _DF_events_out += _n_banks;
    _ev_id += _n_banks;
  }
}

void EB::MFP_generator::write_test_pattern(EB::MFP* mfp)
{
  // The global ev id is modified in the write mfp method
  uint64_t ev_id = _ev_id;
  auto sizes = mfp->header.bank_sizes();
  // for (int k = 0; k < _n_banks; k++) {
  int k = 0;
  for (auto it = mfp->begin(); it != mfp->end(); it++) {
    auto size = sizes[k];
    memset(&(*it), 0xFF & ev_id, size);
    memset(&(*it) + size - 1, 0x00, 1);
    ev_id++;
    k++;
  }
}

void EB::MFP_generator::write_ODIN_banks(EB::MFP* mfp)
{
  // The global ev id is modified in the write mfp method
  uint64_t ev_id = _ev_id;
  for (auto it = mfp->begin(); it != mfp->end(); it++) {
    Online::pcie40::sodin_t* bank = reinterpret_cast<Online::pcie40::sodin_t*>(&(*it));
    memset(bank, 0x0, sizeof(Online::pcie40::sodin_t));
    bank->_run_number = _run_number;
    bank->_event_id = ev_id;
    ev_id++;
  }
}

void EB::MFP_generator::write_end_run()
{
  EB::MFP* mfp;
  // the end of run MFP will have 0 banks a 0 payload, this is not necessary but the MFP needs to be coherent
  size_t size = EB::MFP_header_size(0);
  mfp = _buffer.write_next_element(size);
  // if header si NULL cancel has been issued, no end of run is needed
  if (mfp != NULL) {
    memset(mfp, 0, size);
    mfp->set_end_run();
    mfp->header.packet_size = size;

    _buffer.write_complete();
  }
}

int EB::MFP_generator::stop()
{
  int ret_val = Online::DF_SUCCESS;
  std::unique_lock<std::timed_mutex> loop_guard(_loop_lock, std::chrono::duration<int>(_stop_timeout));
  if (loop_guard) {
    logger.info << "stop executed properly" << std::flush;
  } else {
    logger.error << "Unable to execute a clean stop. Aborting" << std::flush;
    ret_val = Online::DF_ERROR;
  }
  _total_timer.stop();
  _random_gen_timer.stop();
  _MFP_write_timer.stop();
  _dead_time_timer.stop();
  _buffer.reset_cancel();
  return ret_val;
}

int EB::MFP_generator::cancel()
{
  std::lock_guard<decltype(_start_lock)> guard(_start_lock);
  _generate = false;
  _buffer.cancel();
  return Online::DF_SUCCESS;
}

int EB::MFP_generator::pause() { return Online::DF_SUCCESS; }

int EB::MFP_generator::finalize()
{
  _buffer.reset_backend();
  return Online::DF_SUCCESS;
}

void EB::MFP_generator::handle(const Online::DataflowIncident& inc)
{
  logger.info << "incident" << std::flush;
  //  TODO implement this
}

int EB::MFP_generator::config_numa()
{
  int ret_val = Online::DF_SUCCESS;

  numa_set_strict(1);

  if (_numa_layout.size() == 0) {
    _numa_node = -1;
    logger.warning << "No numa topology provided enabling all the nodes" << std::flush;
  } else {
    _numa_node = 0;
    int idx_count = _numa_layout[_numa_node];
    while ((_local_id >= idx_count) && (_numa_node < _numa_layout.size())) {
      _numa_node++;
      idx_count += _numa_layout[_numa_node];
    }

    if (_numa_node >= _numa_layout.size()) {
      _numa_node = -1;
      logger.warning << "Invalid numa topology enabling all the nodes" << std::flush;
    }
  }
  logger.debug << "Running on NUMA node " << _numa_node << std::flush;

  int numa_err;
  numa_err = numa_run_on_node(_numa_node);
  if (numa_err != 0) {
    logger.error << __FUNCTION__ << " numa_run_on_node " << _numa_node << ":" << strerror(errno) << std::flush;
    ret_val = Online::DF_ERROR;
    return ret_val;
  }

  numa_set_preferred(_numa_node);

  return ret_val;
}
