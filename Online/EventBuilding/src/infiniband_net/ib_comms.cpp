/**
 * @file ib_comms.cpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief InfiniBand Communication Library - Communication Methods
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */
#include "ib.hpp"
#include <cstdio>
#include <ctime>
#include <random>
#include <chrono>
#include <cmath>

using namespace std;

#ifndef P_INFO
#define P_INFO "Rank: " << this->_procNum << ":   "
#endif

uint32_t IB_verbs::IB::ibRecv(char* bufPtr, uint32_t bufSize, uint32_t sender)
{
  ibv_mr* mr = this->ibAddMR(bufPtr, bufSize);
  // TODO fix error condition
  // if (mr == NULL) {
  // return 0;
  // }
  uint32_t wrId = this->_ibRecv(mr, (char*) bufPtr, bufSize, sender);
  return wrId;
}

uint32_t IB_verbs::IB::ibSend(char* bufPtr, uint32_t bufSize, uint32_t dest)
{
  ibv_mr* mr = this->ibAddMR(bufPtr, bufSize);
  // TODO fix error condition
  // if (mr == NULL) {
  // return 0;
  // }
  uint32_t wrId = this->_ibSend(mr, (char*) bufPtr, bufSize, dest);
  return wrId;
}

uint32_t IB_verbs::IB::_ibRecv(ibv_mr* mr, char* bufPtr, uint32_t bufSize, uint32_t sender)
{
  Proc& host = _hosts.at(sender);
  ibv_recv_wr* bad_recv_wr;
  ibv_sge list;
  memset(&list, 0, sizeof(list));
  list.addr = (uintptr_t) bufPtr;
  list.length = bufSize;
  uint32_t wrId = _ibGetNextWrId(); // get random wr_id
  int nsge = 1;
  if (bufPtr == NULL && bufSize == 0) {
    nsge = 0; // zero-byte msg
    list.lkey = 0;
  } else {
    list.lkey = mr->lkey;
  }
  ibv_recv_wr recv_wr;
  memset(&recv_wr, 0, sizeof(recv_wr));
  recv_wr.wr_id = wrId;
  recv_wr.sg_list = &list;
  recv_wr.num_sge = nsge;
// //cout << P_INFO << "receiving on " << host.remote_qp_num[idx] << "-" << host.qp[idx]->qp_num << "\r";
#ifdef IB_DUMP_FILE
  ib_wr wr;
  wr.qp_num = host.qp->qp_num;
  wr.src_qp = host.remote_qp_num;
  wr.receive = true;
  _dump_wr(wr);
#endif
  int ret = ibv_post_recv(host.qp, &recv_wr, &bad_recv_wr);
  if (ret < 0) return 0;
  return wrId;
}

uint32_t IB_verbs::IB::_ibSend(ibv_mr* mr, char* bufPtr, uint32_t bufSize, uint32_t dest)
{
  Proc& host = _hosts.at(dest);
  ibv_send_wr* bad_send_wr;
  ibv_sge list;
  memset(&list, 0, sizeof(list));
  list.addr = (uintptr_t) bufPtr;
  list.length = bufSize;
  uint32_t wrId = _ibGetNextWrId(); // get random wr_id
  int nsge = 1;
  if (bufPtr == NULL && bufSize == 0) {
    nsge = 0; // zero-byte msg
    list.lkey = 0;
  } else {
    list.lkey = mr->lkey;
  }
  ibv_send_wr send_wr;
  memset(&send_wr, 0, sizeof(send_wr));
  send_wr.wr_id = wrId;
  send_wr.sg_list = &list;
  send_wr.num_sge = nsge;
  send_wr.opcode = IBV_WR_SEND;
  send_wr.send_flags = IBV_SEND_SIGNALED;
// //cout << P_INFO << "sending on " << host.remote_qp_num[idx] << "-" << host.qp[idx]->qp_num << "\r";
#ifdef IB_DUMP_FILE
  ib_wr wr;
  wr.src_qp = host.qp->qp_num;
  wr.qp_num = host.remote_qp_num;
  wr.receive = false;
  wr.opcode = send_wr.opcode;
  _dump_wr(wr);
#endif
  int ret = ibv_post_send(host.qp, &send_wr, &bad_send_wr);
  if (ret < 0) return 0;
  return wrId;
}

int IB_verbs::IB::ibBlockRecv(char* bufPtr, uint32_t bufSize, uint32_t sender)
{
  uint32_t wrId = this->ibRecv(bufPtr, bufSize, sender);
  return this->ibWaitRecvCQ(wrId);
}

int IB_verbs::IB::ibBlockSend(char* bufPtr, uint32_t bufSize, uint32_t dest)
{
  uint32_t wrId = this->ibSend(bufPtr, bufSize, dest);
  return this->ibWaitSendCQ(wrId);
}
