/**
 * @file ib_rdma_ops.cpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief InfiniBand Communication Library - RDMA Methods
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */
#include "ib.hpp"
#include <cstdio>
#include <ctime>
#include <random>
#include <chrono>
#include <cmath>

using namespace std;

#ifndef P_INFO
#define P_INFO "Rank " << this->_procNum << ":   "
#endif

uint32_t IB_verbs::IB::_RDMAop(char* bufPtr, uint32_t bufSize, uint32_t dest, bool opType /*true read, false write*/)
{
  Proc& host = _hosts.at(dest);
  ibv_mr* mr = this->ibAddMR(bufPtr, bufSize);
  ibv_send_wr* bad_send_wr;
  ibv_sge list = {};
  list.addr = (uintptr_t) bufPtr;
  list.length = bufSize;
  list.lkey = mr->lkey;
  ibv_send_wr send_wr;
  memset(&send_wr, 0, sizeof(send_wr));
  uint32_t wrId = _ibGetNextWrId(); // get random wr_id

  send_wr.wr_id = wrId, send_wr.sg_list = &list, send_wr.num_sge = 1,
  send_wr.opcode = (opType ? IBV_WR_RDMA_READ : IBV_WR_RDMA_WRITE), send_wr.send_flags = IBV_SEND_SIGNALED,
  send_wr.wr.rdma.remote_addr = host.rdma.addr;
  send_wr.wr.rdma.rkey = host.rdma.rkey;
#ifdef IB_DUMP_FILE
  ib_wr wr;
  wr.src_qp = host.qp->qp_num;
  wr.qp_num = host.remote_qp_num;
  wr.receive = false;
  wr.opcode = send_wr.opcode;
  _dump_wr(wr);
#endif
  int ret = ibv_post_send(host.qp, &send_wr, &bad_send_wr);
  if (ret < 0) return 0;

  return wrId;
}

int IB_verbs::IB::ibRecvRDMAInfo(char* bufPtr, uint32_t bufSize, uint32_t src)
{
  Proc& host = _hosts.at(src);
  ibv_mr* mr = this->ibAddMR(bufPtr, bufSize); // MR where we want RDMA on
  if (mr == NULL) return -1;
  // post RR to get remote MR info
  int ret = this->ibBlockRecv((char*) &host.rdma, sizeof(Proc::MRInfo), src);
  return ret;
}

int IB_verbs::IB::ibSendRDMAInfo(char* bufPtr, uint32_t bufSize, uint32_t dest)
{
  // post SR to send remote MR info
  ibv_mr* mr = this->ibAddMR(bufPtr, bufSize); // MR where we want RDMA on
  if (mr == NULL) return -1;
  _hosts.at(this->_procNum).rdma.rkey = mr->rkey;
  _hosts.at(this->_procNum).rdma.rkey = (uintptr_t) mr->addr;
  int ret = this->ibBlockSend((char*) &_hosts.at(this->_procNum).rdma, sizeof(Proc::MRInfo), dest);
  return ret;
}

uint32_t IB_verbs::IB::ibRDMAWrite(char* bufPtr, uint32_t bufSize, uint32_t dest)
{
  uint32_t wrId = _RDMAop(bufPtr, bufSize, dest, false);
  return wrId;
}

uint32_t IB_verbs::IB::ibRDMARead(char* bufPtr, uint32_t bufSize, uint32_t dest)
{
  uint32_t wrId = _RDMAop(bufPtr, bufSize, dest, true);
  return wrId;
}

int IB_verbs::IB::ibBlockRDMAWrite(char* bufPtr, uint32_t bufSize, uint32_t dest, bool sendComplete)
{
  uint32_t wrId = _RDMAop(bufPtr, bufSize, dest, false);
  int ret = ibWaitSendCQ(wrId);
  if (ret != 0) return ret;
  if (sendComplete) {
    wrId = ibSendSync(RDMA_OP_COMPLETE, dest);
    return ibWaitSyncSend(wrId);
  }
  return 0;
}

int IB_verbs::IB::ibBlockRDMARead(char* bufPtr, uint32_t bufSize, uint32_t dest, bool sendComplete)
{
  uint32_t wrId = _RDMAop(bufPtr, bufSize, dest, true);
  int ret = ibWaitSendCQ(wrId);
  if (ret != 0) return ret;
  if (sendComplete) {
    wrId = ibSendSync(RDMA_OP_COMPLETE, dest);
    return ibWaitSyncSend(wrId);
  }
  return 0;
}
