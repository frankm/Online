/**
 * @file parser.hpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief Parser for InfiniBand Library
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */
#include "parser.hpp"
#include <cerrno>
#include <cstring>

using namespace std;

IB_verbs::Parser::Parser(const char* filename)
{
  _getMyNetInfo();
  _readHostFile(filename);
  _getMyIbDevInfo();
}

IB_verbs::Parser::Parser(const char* filename, const int rankid)
{
  _getMyNetInfo();
  _readHostFile(filename, rankid);
  _getMyIbDevInfo();
}

IB_verbs::Parser::~Parser() {}

int IB_verbs::Parser::getProcessId() const { return this->_globalProc; }

int IB_verbs::Parser::getTotalProcesses() const { return this->_totalProcs; }

int IB_verbs::Parser::getIbDevNum() const { return this->_ibDevNum; }

int IB_verbs::Parser::getIbNumaNode() const { return this->_ibNumaNode; }

std::string IB_verbs::Parser::getMyHostname() const { return this->_myHostName; }

std::string IB_verbs::Parser::getMyIp() const { return this->_myIP; }

int IB_verbs::Parser::_getMyNetInfo()
{
  char buf[256];
  char* ip;
  hostent* host;
  int ret = gethostname(buf, sizeof(buf));
  if (ret == -1) return ret;
  this->_myHostName = string(buf);
  host = gethostbyname(buf);
  if (host == NULL) return -1;
  ip = inet_ntoa(*((in_addr*) host->h_addr_list[0]));
  this->_myIP = string(ip);
  return 0;
}

void IB_verbs::Parser::_getMyIbDevInfo()
{
  int num_devs = 0;
  auto dev_list = ibv_get_device_list(&num_devs);
  if (dev_list == NULL) {
    string err_mess("Error while opening IB devices: ");
    err_mess += strerror(errno);
    throw runtime_error(err_mess);
  } else if (num_devs == 0) {
    throw runtime_error("cannot find IB devices");
  }
  this->_ibDevNum = -1;
  for (int i = 0; i < num_devs; i++) {
    string tmp(ibv_get_device_name(dev_list[i]));
    if (this->_ibDevString == tmp) {
      _ibDevNum = i;
      break;
    }
  }
  if (_ibDevNum == -1) {
    throw runtime_error("cannot find the device");
  }
  string path(dev_list[this->_ibDevNum]->ibdev_path);
  string numastr;
  ifstream numafile(path + "/device/numa_node");
  if (numafile.is_open()) {
    numafile >> numastr;
    this->_ibNumaNode = stoi(numastr);
    numafile.close();
  } else {
    this->_ibNumaNode = -1;
  }

  ibv_free_device_list(dev_list);
}

std::string IB_verbs::Parser::_getIpByHostname(std::string hostname)
{
  char* ip;
  hostent* host;
  host = gethostbyname(hostname.c_str());
  ip = inet_ntoa(*((in_addr*) host->h_addr_list[0]));
  return string(ip);
}

int IB_verbs::Parser::getIpPort() const { return this->_ipPort; }

std::string IB_verbs::Parser::getIbDevString() const { return this->_ibDevString; }

void IB_verbs::Parser::_readHostFile(const char* filename, const int nProc)
{
  DynamicJsonDocument doc(1e6);
  ifstream hostFile(filename);
  deserializeJson(doc, hostFile);
  JsonArray hostsArray = doc["hosts"];
  // get hosts size
  int procs = hostsArray.size();
  // resize lut
  _hostvec.resize(procs);
  sockaddr_in tmp = {};
  std::fill(_hostvec.begin(), _hostvec.end(), tmp);
  this->_totalProcs = procs;
  string curr_ip = "";
  int hostn = 0;
  // REMOTES
  for (int i = 0; i < procs; i++) {
    sockaddr_in temp;
    int rankid = hostsArray[i]["rankid"].as<int>();
    if (rankid >= procs) {
      throw runtime_error("rank ID out of range.");
    }
    if (hostsArray[i].containsKey("ip"))
      curr_ip = hostsArray[i]["ip"].as<std::string>();
    else
      curr_ip = this->_getIpByHostname(hostsArray[i]["hostname"].as<std::string>());
    hostn = hostsArray[i]["port"].as<int>();
    // cout << curr_ip << ":" << hostn << endl;
    temp.sin_family = AF_INET;
    inet_pton(AF_INET, curr_ip.c_str(), &temp.sin_addr);
    temp.sin_port = htons(hostn);
    // cout << i << endl;
    if (rankid == nProc) {
      if (curr_ip.compare(this->_myIP) == 0) {
        this->_globalProc = nProc;
        this->_ibDevString = hostsArray[i]["ibdev"].as<std::string>();
      } else {
        throw runtime_error("Hostname and rank id mismatch.");
      }
    }
    if (_hostvec[rankid].sin_port != 0) {
      throw runtime_error("duplicate rank id.");
    }
    _hostvec[rankid] = temp;
  }
  doc.clear();
  hostFile.close();
}

void IB_verbs::Parser::_readHostFile(const char* filename)
{
  DynamicJsonDocument doc(1e6);
  ifstream hostFile(filename);
  deserializeJson(doc, hostFile);
  JsonArray hostsArray = doc["hosts"];
  // get hosts size
  int procs = hostsArray.size();
  // resize lut
  _hostvec.resize(procs);
  sockaddr_in tmp = {};
  std::fill(_hostvec.begin(), _hostvec.end(), tmp);
  this->_totalProcs = procs;
  string curr_ip = "";
  int hostn;
  // REMOTES
  bool is_this_process = false;
  for (int i = 0; i < procs; i++) {
    sockaddr_in temp;
    int rankid = hostsArray[i]["rankid"].as<int>();
    string utgid = hostsArray[i]["utgid"].as<std::string>();
    if (rankid >= procs) {
      throw runtime_error(utgid + ": rank ID out of range.");
    }
    // look for ip
    if (hostsArray[i].containsKey("ip"))
      curr_ip = hostsArray[i]["ip"].as<std::string>();
    else
      curr_ip = this->_getIpByHostname(hostsArray[i]["hostname"].as<std::string>());
    // look for port
    if (hostsArray[i].containsKey("port"))
      hostn = hostsArray[i]["port"].as<int>();
    else
      hostn = 10000 + rankid;

    temp.sin_family = AF_INET;
    inet_pton(AF_INET, curr_ip.c_str(), &temp.sin_addr);
    temp.sin_port = htons(hostn);
    // cout << i << endl;
    char* env = getenv("UTGID");
    if (env == NULL) {
      doc.clear();
      hostFile.close();
      throw runtime_error("Cannot find UTGID env variable.");
    }
    string env_utgid(env);

    // compare utgid
    if (env_utgid.compare(utgid) == 0) {
      if (curr_ip.compare(this->_myIP) == 0) {
        this->_globalProc = rankid;
        this->_ibDevString = hostsArray[i]["ibdev"].as<std::string>();
        is_this_process = true;
      } else {
        doc.clear();
        hostFile.close();
        throw runtime_error("Hostname and UTGID mismatch.");
      }
    }

    if (_hostvec[rankid].sin_port != 0) {
      throw runtime_error(utgid + ": duplicate rank id.");
    }
    _hostvec[rankid] = temp;
  }
  if (!is_this_process) {
    doc.clear();
    hostFile.close();
    throw runtime_error("Haven't found this process in the config file, UTGID mismatch.");
  }
  doc.clear();
  hostFile.close();
}

std::vector<sockaddr_in> IB_verbs::Parser::getHostList() { return _hostvec; }
