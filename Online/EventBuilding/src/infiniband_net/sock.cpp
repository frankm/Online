/**
 * @file sock.hpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief Socket Communication for InfiniBand Library
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */
#include "sock.hpp"

using namespace std;

IB_verbs::Sock::Sock() {}
IB_verbs::Sock::~Sock() {}

void IB_verbs::Sock::recvQPs(const int rankid, const std::vector<Proc>& k, std::atomic<int>& ret)
{
  ret = 0;
  vector<Proc>& hosts = const_cast<vector<Proc>&>(k);
  int connfd = socket(AF_INET, SOCK_STREAM, 0);
  // Preventing the OS from keeping the socket open after close()
  struct linger sl;
  sl.l_onoff = 1;  /* non-zero value enables linger option in kernel */
  sl.l_linger = 0; /* timeout interval in seconds */
  if (setsockopt(connfd, SOL_SOCKET, SO_LINGER, &sl, sizeof(sl)) < 0) {
    // TODO find a better err code
    ret = -20;
    return;
  }
  int reuse_addr = 1;
  if (setsockopt(connfd, SOL_SOCKET, SO_REUSEADDR, &reuse_addr, sizeof(reuse_addr)) < 0) {
    // TODO find a better err code
    ret = -21;
    return;
  }

  sockaddr_in server;
  server.sin_family = AF_INET;
  server.sin_port = hosts.at(rankid).sockAddr.sin_port;
  server.sin_addr.s_addr = INADDR_ANY;
  if (bind(connfd, (struct sockaddr*) &server, sizeof(server)) < 0) {
    ret = -1;
    return;
  }
  if (listen(connfd, 50) < 0) {
    ret = -2;
    return;
  }
  QPInfo tmp_qp_info;
  int host_size = hosts.size();
  for (int i = 0; i < host_size; i++) {
    int fd = accept(connfd, (struct sockaddr*) NULL, NULL);
    if (fd < 0) {
      ret = -3;
      return;
    }
    int rcv = recv(fd, (char*) &tmp_qp_info, sizeof(QPInfo), 0);
    if (rcv < 0) {
      ret = -4;
      return;
    }
    int idx = ntohl(tmp_qp_info.procNum);
    hosts.at(idx).remote_lid = ntohs(tmp_qp_info.lid);
    hosts.at(idx).remote_sync_qp_num = ntohl(tmp_qp_info.sync_qp_num);
    hosts.at(idx).remote_qp_num = ntohl(tmp_qp_info.qp_num);
    if (shutdown(fd, SHUT_RDWR) != 0) {
      ret = -5;
      return;
    }
    if (close(fd) != 0) {
      ret = -6;
      return;
    }
  }
  if (close(connfd) != 0) {
    ret = -7;
    return;
  }
}

int IB_verbs::Sock::sendQP(const int rankid, Proc& rem)
{ // send local qp infos
  QPInfo tmp_qp_info;
  tmp_qp_info.lid = htons(rem.local_lid);
  tmp_qp_info.qp_num = htonl(rem.qp->qp_num);
  tmp_qp_info.sync_qp_num = htonl(rem.sync_qp->qp_num);
  tmp_qp_info.procNum = htonl(rankid);
  int connfd = socket(AF_INET, SOCK_STREAM, 0);
  if (connfd < 0) return -1;
  if (connect(connfd, (sockaddr*) &rem.sockAddr, sizeof(rem.sockAddr)) < 0) return -1;
  send(connfd, (char*) &tmp_qp_info, sizeof(QPInfo), 0);
  close(connfd);
  return 0;
}
