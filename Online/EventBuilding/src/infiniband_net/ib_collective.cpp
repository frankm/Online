/**
 * @file ib_collective.cpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief InfiniBand Communication Library - Collective Methods
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */
#include "ib.hpp"
#include <cstdio>
#include <ctime>
#include <random>
#include <chrono>
#include <cmath>

using namespace std;

#ifndef P_INFO
#define P_INFO "Rank " << procNum << ":   "
#endif

int IB_verbs::IB::ibBarrier(barrier_t type)
{
  switch (type) {
  case TOURNAMENT: return this->_runTournament(); break;
  case CENTRAL: return this->_runCentral(); break;
  default: return this->_runTournament(); break;
  }
}

int IB_verbs::IB::_initTournament()
{
  // all of this can be put in init!
  array<uint32_t, 2> req;
  req[0] = BARRIER;
  int rounds = static_cast<int>(log2(this->_totalProcs));
  int sz = this->_totalProcs;
  int procNum = this->_procNum;
  for (int i = 1; i <= rounds; i++) {
    if (procNum % (1 << i) == 0) {
      if (procNum <= (this->_totalProcs - (1 << i))) {
        int src = (procNum + (1 << (i - 1)));

        // cout << P_INFO << "S recv " << src << endl;
        req[1] = src;
        _tournament_lut.push_back(req);
        _tournament_hlist.push_back(src);
        int mod = sz % ((1 << i));
        if (mod != 0) { // add receive for _tournament_lonely processes
          if (procNum == (sz - 3 * (1 << (i - 1)))) {
            int src = (sz - (1 << (i - 1)));

            // cout << P_INFO << "D recv " << src << endl;
            req[1] = src;
            _tournament_lut.push_back(req);
            _tournament_hlist.push_back(src);
            _tournament_lonely = true; // for release sync
          }
          sz -= mod;
        }
      } else {
        int src = (procNum - (1 << i));

        // cout << P_INFO << "RD recv " << src << endl;
        req[1] = src;
        _tournament_lut.push_back(req); // recv release
        _tournament_dst = src;
        break;
      }
    } else {
      int src = (procNum - (1 << (i - 1)));

      // cout << P_INFO << "R recv " << src << endl;
      req[1] = src;
      _tournament_lut.push_back(req); // recv release
      _tournament_dst = src;
      break;
    }
  }
  return 0;
}
int IB_verbs::IB::_runTournament()
{
  int res;
  if (_tournament_dst > -1) {
    uint32_t wr = this->ibSendSync(BARRIER, _tournament_dst); // blocking to mantain sync
    if (wr == 0) return -1;
    res = this->ibWaitSyncSend(wr);
    if (res != 0) return res;
  }
  res = this->ibWaitAllSyncRecvs(_tournament_lut);
  if (res != 0) return res;

  int idx = _tournament_hlist.size() - 1;
  uint32_t _tournament_lonely_send = 0;
  if (_tournament_lonely) {
    _tournament_lonely_send =
      this->ibSendSync(BARRIER, _tournament_hlist.at(idx)); // syncing with _tournament_lonely processes simultaneously
    if (_tournament_lonely_send == 0) return -1;
    idx--;
  }
  while (idx >= 0) {

    uint32_t temp_send = this->ibSendSync(BARRIER, _tournament_hlist.at(idx));
    if (temp_send == 0) return -1;
    if (_tournament_lonely) {
      res = this->ibWaitSyncSend(_tournament_lonely_send);
      if (res != 0) return res;
      _tournament_lonely = false;
    }
    res = this->ibWaitSyncSend(temp_send);
    if (res != 0) return res;
    idx--;
  }
  return 0;
}

int IB_verbs::IB::_runCentral()
{
  int res;
  int procNum = this->_procNum;
  if (procNum == 0) {
    vector<array<uint32_t, 2>> recvs_ids;
    for (int i = 1; i < this->_totalProcs; i++) {
      array<uint32_t, 2> req;
      req[0] = BARRIER;
      req[1] = i;
      recvs_ids.push_back(req);
    }
    res = this->ibWaitAllSyncRecvs(recvs_ids);
    if (res != 0) return res;
    vector<uint32_t> send_ids;
    uint32_t temp_wr;
    for (int i = 1; i < this->_totalProcs; i++) {
      temp_wr = this->ibSendSync(BARRIER, i);
      if (temp_wr == 0) return -1;
      send_ids.push_back(temp_wr);
    }
    res = this->ibWaitAllSyncSends(send_ids);
    if (res != 0) return res;
  } else {
    uint32_t wrid = this->ibSendSync(BARRIER, 0);
    if (wrid == 0) return -1;
    res = this->ibWaitSyncSend(wrid);
    if (res != 0) return res;
    res = this->ibWaitSyncRecv(BARRIER, 0);
    if (res != 0) return res;
  }
  return 0;
}