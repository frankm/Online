/**
 * @file IB.cpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief InfiniBand Communication Library
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */

#include "ib.hpp"
#include <cstdio>
#include <ctime>
#include <chrono>
#include <cmath>
#include <cerrno>
#include <cstring>
#include <sstream>
#include <iostream>
using namespace std;
#ifndef P_INFO
#define P_INFO "Rank " << this->_procNum << ":   "
#endif

IB_verbs::IB::IB()
{
  memset(&this->_port_attr, 0, sizeof(_port_attr));
  memset(&this->_dev_attr, 0, sizeof(_dev_attr));
}

IB_verbs::IB::IB(IB&& src)
{
  this->_hosts = move(src._hosts);
  this->_memMap = move(src._memMap);
  this->_devNum = src._devNum;
  this->_procNum = src._procNum;
  this->_totalProcs = src._totalProcs;
  this->_ctx = src._ctx;
  this->_pd = src._pd;
  this->_sendcq = src._sendcq;
  this->_recvcq = src._recvcq;
  this->_wcsend = move(src._wcsend);
  this->_wcrecv = move(src._wcrecv);
  this->_wcsyncr = move(src._wcsyncr);
  this->_wcsyncs = move(src._wcsyncs);
  this->_port_attr = src._port_attr;
  this->_dev_attr = src._dev_attr;
  this->_wrId = src._wrId.load();
  this->_sockObj = move(src._sockObj);
  src._ctx = NULL;
  src._pd = NULL;
  src._sendcq = NULL;
  src._recvcq = NULL;
  src._wrId = 0;
}

IB_verbs::IB& IB_verbs::IB::operator=(IB&& src) noexcept
{
  if (this != &src) {
    try {
      this->ibDestroy();
    } catch (...) {
    }
    this->_hosts = move(src._hosts);
    this->_memMap = move(src._memMap);
    this->_devNum = src._devNum;
    this->_procNum = src._procNum;
    this->_totalProcs = src._totalProcs;
    this->_ctx = src._ctx;
    this->_pd = src._pd;
    this->_sendcq = src._sendcq;
    this->_recvcq = src._recvcq;
    this->_wcsend = move(src._wcsend);
    this->_wcrecv = move(src._wcrecv);
    this->_wcsyncr = move(src._wcsyncr);
    this->_wcsyncs = move(src._wcsyncs);
    this->_port_attr = src._port_attr;
    this->_dev_attr = src._dev_attr;
    this->_wrId = src._wrId.load();
    this->_sockObj = move(src._sockObj);
    src._ctx = NULL;
    src._pd = NULL;
    src._sendcq = NULL;
    src._recvcq = NULL;
    src._wrId = 0;
  }
  return *this;
}

IB_verbs::IB::IB(const char* filename, const int nProc)
{
  memset(&this->_port_attr, 0, sizeof(_port_attr));
  memset(&this->_dev_attr, 0, sizeof(_dev_attr));
  this->ibParseHosts(filename, nProc);
  this->ibInit();
}

IB_verbs::IB::IB(const char* filename)
{
  memset(&this->_port_attr, 0, sizeof(_port_attr));
  memset(&this->_dev_attr, 0, sizeof(_dev_attr));
  this->ibParseHosts(filename);
  this->ibInit();
}

IB_verbs::IB::~IB() noexcept
{
  try {
    this->ibDestroy();
  } catch (...) {
  }
}

int IB_verbs::IB::getProcessID() const { return this->_procNum; }

int IB_verbs::IB::ibGetDevNum() const { return this->_devNum; }

int IB_verbs::IB::ibGetNumaNode() const { return this->_numaNode; }

int IB_verbs::IB::getTotalProcesses() const { return this->_totalProcs; }

void IB_verbs::IB::_destroyQP(ibv_qp* qp)
{

  if (qp) {
    // transition qp to ERROR
    ibv_qp_attr qp_attr;
    memset(&qp_attr, 0, sizeof(qp_attr));
    qp_attr.qp_state = IBV_QPS_ERR;
    int ret = ibv_modify_qp(qp, &qp_attr, IBV_QP_STATE);
    if (ret != 0) {
      throw runtime_error("Failed to modify qp to ERROR");
    }
    // destroy qp
    ret = ibv_destroy_qp(qp);
    if (ret != 0) {
      throw runtime_error("Failed to destroy qp");
    }
    qp = NULL;
  }
}

int IB_verbs::IB::_ibSetQP(Proc& host)
{
  // DATA QPs
  {
    int ret = 0;
    {

      ibv_qp_attr qp_attr = {};
      qp_attr.qp_state = IBV_QPS_INIT;
      qp_attr.qp_access_flags =
        IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_ATOMIC;
      qp_attr.pkey_index = 0;
      qp_attr.port_num = this->_portNum;
      ret = ibv_modify_qp(host.qp, &qp_attr, IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS);
      if (ret != 0) {
        throw runtime_error("Failed to modify qp to INIT");
      }
    }
    {
      ibv_ah_attr ah_attr = {};
      ah_attr.dlid = host.remote_lid;
      ah_attr.sl = 0;
      ah_attr.src_path_bits = 0;
      ah_attr.is_global = 0;
      ah_attr.port_num = this->_portNum;

      ibv_qp_attr qp_attr = {};
      qp_attr.qp_state = IBV_QPS_RTR;
      qp_attr.path_mtu = IBV_MTU_4096;
      qp_attr.rq_psn = 0;
      qp_attr.dest_qp_num = host.remote_qp_num;
      qp_attr.ah_attr = ah_attr;
      qp_attr.max_dest_rd_atomic = 2;
      qp_attr.min_rnr_timer = 12;

      ret = ibv_modify_qp(
        host.qp,
        &qp_attr,
        IBV_QP_STATE | IBV_QP_AV | IBV_QP_PATH_MTU | IBV_QP_DEST_QPN | IBV_QP_RQ_PSN | IBV_QP_MAX_DEST_RD_ATOMIC |
          IBV_QP_MIN_RNR_TIMER);
      if (ret != 0) {
        throw runtime_error("Failed to modify qp to RTR\n");
      }
    }
    {
      ibv_qp_attr qp_attr = {};
      qp_attr.qp_state = IBV_QPS_RTS;
      qp_attr.sq_psn = 0;
      qp_attr.max_rd_atomic = 2;
      qp_attr.timeout = 14;
      qp_attr.retry_cnt = 7;
      qp_attr.rnr_retry = 7;

      ret = ibv_modify_qp(
        host.qp,
        &qp_attr,
        IBV_QP_STATE | IBV_QP_TIMEOUT | IBV_QP_RETRY_CNT | IBV_QP_RNR_RETRY | IBV_QP_SQ_PSN | IBV_QP_MAX_QP_RD_ATOMIC);
      if (ret != 0) {
        throw runtime_error("Failed to modify qp to RTS\n");
      }
    }
  }
  // SYNC QP
  int ret = 0;
  {

    ibv_qp_attr qp_attr = {};
    qp_attr.qp_state = IBV_QPS_INIT;
    qp_attr.qp_access_flags =
      IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_ATOMIC;
    qp_attr.pkey_index = 0;
    qp_attr.port_num = this->_portNum;

    ret = ibv_modify_qp(host.sync_qp, &qp_attr, IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS);
    if (ret != 0) {
      throw runtime_error("Failed to modify qp to INIT\n");
    }
  }
  {
    ibv_ah_attr ah_attr = {};
    ah_attr.dlid = host.remote_lid;
    ah_attr.sl = 0;
    ah_attr.src_path_bits = 0;
    ah_attr.is_global = 0;
    ah_attr.port_num = this->_portNum;

    ibv_qp_attr qp_attr = {};
    qp_attr.qp_state = IBV_QPS_RTR;
    qp_attr.path_mtu = IBV_MTU_4096;
    qp_attr.rq_psn = 0;
    qp_attr.dest_qp_num = host.remote_sync_qp_num;
    qp_attr.ah_attr = ah_attr;
    qp_attr.max_dest_rd_atomic = 2;
    qp_attr.min_rnr_timer = 12;

    ret = ibv_modify_qp(
      host.sync_qp,
      &qp_attr,
      IBV_QP_STATE | IBV_QP_AV | IBV_QP_PATH_MTU | IBV_QP_DEST_QPN | IBV_QP_RQ_PSN | IBV_QP_MAX_DEST_RD_ATOMIC |
        IBV_QP_MIN_RNR_TIMER);
    if (ret != 0) {
      throw runtime_error("Failed to modify qp to RTR\n");
    }
  }
  {
    ibv_qp_attr qp_attr = {};
    qp_attr.qp_state = IBV_QPS_RTS;
    qp_attr.sq_psn = 0;
    qp_attr.max_rd_atomic = 2;
    qp_attr.timeout = 14;
    qp_attr.retry_cnt = 7;
    qp_attr.rnr_retry = 7;

    ret = ibv_modify_qp(
      host.sync_qp,
      &qp_attr,
      IBV_QP_STATE | IBV_QP_TIMEOUT | IBV_QP_RETRY_CNT | IBV_QP_RNR_RETRY | IBV_QP_SQ_PSN | IBV_QP_MAX_QP_RD_ATOMIC);
    if (ret != 0) {
      throw runtime_error("Failed to modify qp to RTS\n");
    }
  }
  return 0;
}

ibv_mr* IB_verbs::IB::ibAddMR(char* bufPtr, size_t bufSize)
{
  ibv_mr* memr = NULL;
  // FIXME uninitialized return value
  if (bufPtr == NULL && bufSize == 0) return memr;
  if (_memMap.size() > 0) {
    for (size_t i = 0; i < _memMap.size(); i++) {
      if (_memMap.at(i)._bufPtr == bufPtr) {
        // same pointer
        if (_memMap.at(i)._bufSize >= bufSize) {
          return _memMap.at(i)._mr;
        }
      } else if (
        bufPtr > _memMap.at(i)._bufPtr && bufPtr < (_memMap.at(i)._bufPtr + _memMap.at(i)._bufSize) &&
        (_memMap.at(i)._bufPtr + _memMap.at(i)._bufSize) >= (bufSize + bufPtr)) {
        return _memMap.at(i)._mr;
      }
    }
  }
  memr = ibv_reg_mr(
    this->_pd,
    (void*) bufPtr,
    bufSize,
    IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_RELAXED_ORDERING);
  if (memr == NULL) {
    return NULL;
  }
  memreg item = {};
  item._mr = memr;
  item._bufPtr = bufPtr;
  item._bufSize = bufSize;

  _memMap.push_back(item);
  return memr;
}

int IB_verbs::IB::ibRemoveMR(char* bufPtr, size_t bufSize)
{
  if (_memMap.size() > 0) {
    for (size_t i = 0; i < _memMap.size(); i++) {
      if (_memMap.at(i)._bufPtr == bufPtr && _memMap.at(i)._bufSize == bufSize) {
        if (ibv_dereg_mr(_memMap.at(i)._mr) == 0) {
          _memMap.at(i) = _memMap.back();
          _memMap.pop_back();
          return 0;
        }
      }
    }
  }
  return -1;
}

int IB_verbs::IB::ibParseHosts(const char* filename, const int nProc)
{
  try {
    Parser prs(filename, nProc);
    this->_totalProcs = prs.getTotalProcesses();
    this->_procNum = prs.getProcessId();
    this->_devName = prs.getIbDevString();
    this->_devNum = prs.getIbDevNum();
    this->_numaNode = prs.getIbNumaNode();
    auto list = prs.getHostList();
    _hosts.resize(list.size());
    for (size_t i = 0; i < list.size(); i++) {
      _hosts[i].sockAddr = list[i];
    }
  } catch (const std::exception& e) {
    throw;
  }
  return 0;
}

int IB_verbs::IB::ibParseHosts(const char* filename)
{
  try {
    Parser prs(filename);
    this->_totalProcs = prs.getTotalProcesses();
    this->_procNum = prs.getProcessId();
    this->_devName = prs.getIbDevString();
    this->_devNum = prs.getIbDevNum();
    this->_numaNode = prs.getIbNumaNode();
    auto list = prs.getHostList();
    _hosts.resize(list.size());
    for (size_t i = 0; i < list.size(); i++) {
      _hosts[i].sockAddr = list[i];
    }
  } catch (const std::exception& e) {
    throw;
  }
  return 0;
}

int IB_verbs::IB::ibSetHostList(Parser& hostParser)
{
  try {
    auto list = hostParser.getHostList();
    this->_totalProcs = hostParser.getTotalProcesses();
    this->_procNum = hostParser.getProcessId();
    this->_devName = hostParser.getIbDevString();
    this->_devNum = hostParser.getIbDevNum();
    this->_numaNode = hostParser.getIbNumaNode();
    _hosts.resize(list.size());
    for (size_t i = 0; i < list.size(); i++) {
      _hosts[i].sockAddr = list[i];
    }
  } catch (const std::exception& e) {
    throw;
  }
  return 0;
}

int IB_verbs::IB::_getDevInfo()
{
  int num_devs = 0;
  this->_dev_list = ibv_get_device_list(&num_devs);
  if (this->_dev_list == NULL) {
    throw runtime_error("cannot find IB devices");
  }
  this->_devNum = -1;
  for (int i = 0; i < num_devs; i++) {
    string tmp(ibv_get_device_name(this->_dev_list[i]));
    if (this->_devName == tmp) {
      _devNum = i;
      break;
    }
  }
  if (_devNum == -1) {
    throw runtime_error("cannot find the device");
  }
  string path(this->_dev_list[this->_devNum]->ibdev_path);
  string numastr;
  ifstream numafile(path + "/device/numa_node");
  if (numafile.is_open()) {
    numafile >> numastr;
    this->_numaNode = stoi(numastr);
    numafile.close();
  } else {
    this->_numaNode = -1;
  }
  return 0;
}

void IB_verbs::IB::ibInit()
{
#ifdef IB_DUMP_FILE
  std::stringstream base_dump_filename;
  char* utgid = getenv("UTGID");
  base_dump_filename << _dump_file_path << "/" << utgid;
  _wc_dump_file = std::ofstream(base_dump_filename.str() + "_wc_dump.txt");
  _wr_dump_file = std::ofstream(base_dump_filename.str() + "_wr_dump.txt");
  if (!_wc_dump_file.is_open() or !_wr_dump_file.is_open()) {
    throw runtime_error("cannot open IB dump files " + base_dump_filename.str());
  }
#endif

  // if no dev list (can happen if ibInit is recalled)
  int num_devs;
  if (this->_dev_list == NULL) {
    this->_dev_list = ibv_get_device_list(&num_devs);
    if (this->_dev_list == NULL) {
      string err_mess("Error while opening IB devices: ");
      err_mess += strerror(errno);
      throw runtime_error(err_mess);
    } else if (num_devs == 0) {
      throw runtime_error("cannot find IB devices");
    }
  }
  // open the device
  this->_ctx = ibv_open_device(this->_dev_list[this->_devNum]);
  if (this->_ctx == NULL) {
    throw runtime_error("cannot open IB device");
  }
  ibv_free_device_list(_dev_list);
  // allocate protection domain
  this->_pd = ibv_alloc_pd(this->_ctx);
  if (this->_pd == NULL) {
    throw runtime_error("cannot allocate protection domain");
  }
  // query dev port
  int ret = ibv_query_port(this->_ctx, this->_portNum, &this->_port_attr);
  if (ret < 0) {
    throw runtime_error("cannot query IB port on device");
  }
  if (this->_port_attr.state != IBV_PORT_ACTIVE) {
    throw runtime_error("IB port is not active, check cable");
  }
  if (this->_port_attr.phys_state != 5 /*LINKUP*/) {
    throw runtime_error("IB port physical link error, check SM");
  }
  // get dev info
  ret = ibv_query_device(this->_ctx, &(this->_dev_attr));
  if (ret < 0) {
    throw runtime_error("cannot query device for attributes");
  }
  // create completion queues
  this->_sendcq = ibv_create_cq(this->_ctx, (this->_dev_attr.max_cqe) / 3, NULL, NULL, 0);
  if (this->_sendcq == NULL) {
    throw runtime_error("cannot create data send completion queue");
  }
  this->_recvcq = ibv_create_cq(this->_ctx, (this->_dev_attr.max_cqe) / 3, NULL, NULL, 0);
  if (this->_recvcq == NULL) {
    throw runtime_error("cannot create data receive completion queue");
  }
  this->_syncScq = ibv_create_cq(this->_ctx, (this->_dev_attr.max_cqe) / 3, NULL, NULL, 0);
  if (this->_syncScq == NULL) {
    throw runtime_error("cannot create sync send completion queue");
  }
  this->_syncRcq = ibv_create_cq(this->_ctx, (this->_dev_attr.max_cqe) / 3, NULL, NULL, 0);
  if (this->_syncRcq == NULL) {
    throw runtime_error("cannot create sync receive completion queue");
  }

  // CREATING QUEUE PAIRS FOR EVERY REMOTE
  ibv_qp_cap cap = {};
  cap.max_send_wr = 8192;
  cap.max_recv_wr = (unsigned int) this->_dev_attr.max_qp_wr;
  cap.max_send_sge = 1;
  cap.max_recv_sge = 1;

  ibv_qp_init_attr qp_init_attr = {};
  qp_init_attr.send_cq = this->_sendcq;
  qp_init_attr.recv_cq = this->_recvcq;
  qp_init_attr.cap = cap;
  qp_init_attr.qp_type = IBV_QPT_RC;

  ibv_qp_init_attr qp_sync_init_attr = {};
  qp_sync_init_attr.send_cq = this->_syncScq;
  qp_sync_init_attr.recv_cq = this->_syncRcq;
  qp_sync_init_attr.cap = cap;
  qp_sync_init_attr.qp_type = IBV_QPT_RC;

  // creating queue pairs
  for (size_t i = 0; i < _hosts.size(); i++) {
    // data qp
    _hosts.at(i).qp = ibv_create_qp(this->_pd, &qp_init_attr);
    if (_hosts.at(i).qp == NULL) {
      throw runtime_error("cannot create data queue pair");
    }
    // sync qp
    _hosts.at(i).sync_qp = ibv_create_qp(this->_pd, &qp_sync_init_attr);
    if (_hosts.at(i).sync_qp == NULL) {
      throw runtime_error("cannot query sync queue pair");
    }
  }
  // adding thread for recv qp data over tcp
  std::atomic<int> _retsocket = {0};
  _QPrecv = std::thread(&Sock::recvQPs, &_sockObj, this->_procNum, std::ref(this->_hosts), std::ref(_retsocket));
  ret = 0;
  int timeout = 0;
  // sending qp data over tcp
  for (size_t i = 0; i < _hosts.size(); i++) {
    timeout = 0;
    _hosts.at(i).local_lid = this->_port_attr.lid;
    while (_sockObj.sendQP(this->_procNum, _hosts.at(i)) < 0) {
      sleep(1);
      timeout++;
      if (timeout > 120) {
        ret = -1;
      }
      int rtc = _retsocket;
      if (rtc != 0) {
        if (!_QPrecv.joinable()) throw runtime_error("cannot join socket thread");
        _QPrecv.join();
      }
      switch (rtc) {
      case -1: throw runtime_error("socket bind error"); break;
      case -2: throw runtime_error("socket listen error"); break;
      case -3: throw runtime_error("exchange accept error"); break;
      case -4: throw runtime_error("exchange receive error"); break;
      case -5: throw runtime_error("exchange shutdown error"); break;
      case -6: throw runtime_error("exchange close error"); break;
      case -7: throw runtime_error("socket close error"); break;
      }
    }
  }
  // waiting for all qp data to be received
  if (!_QPrecv.joinable()) throw runtime_error("cannot join socket thread");
  _QPrecv.join();
  if (ret == -1) {
    throw runtime_error("timeout sending qp info to remote");
  }
  // setting up the qps
  for (size_t i = 0; i < _hosts.size(); i++) {
    try {
      this->_ibSetQP(_hosts.at(i));
    } catch (...) {
      throw;
    }
  }
  // FILLING REVERSE LUT
  for (size_t i = 0; i < _hosts.size(); i++) {
    uint32_t qp_n = _hosts.at(i).sync_qp->qp_num;
    _reverse_hosts[qp_n] = i;
  }
  // PRIMING SYNC RECVS
  if (_syncInit() != 0) {
    throw runtime_error("sync init failed");
  }
  // STARTING SYNC THREAD
  _syncThread = std::thread(&IB_verbs::IB::_syncFunc, this, std::ref(_runsync));
  // COMPUTE LUT FOR BARRIER
  _initTournament();
}

int IB_verbs::IB::ibDeregMRs()
{
  int ret = 0;
  for (size_t i = 0; i < this->_memMap.size(); i++) {
    ret += ibv_dereg_mr(_memMap.at(i)._mr);
    _memMap.at(i)._mr = NULL;
  }
  if (ret != 0) throw runtime_error(string("cannot dereg memory region. Err: ").append(strerror(errno)));
  _memMap.clear();
  return ret;
}

void IB_verbs::IB::ibKillBlocking() { _kill_blocking = true; }

void IB_verbs::IB::ibDestroy()
{
  int ret = 0;
  _runsync = false;
  if (_syncThread.joinable()) {
    _syncThread.join();
  }
  for (size_t i = 0; i < this->_hosts.size(); i++) {
    try {
      _destroyQP(this->_hosts.at(i).qp);
    } catch (...) {
      throw;
    }
    try {
      _destroyQP(this->_hosts.at(i).sync_qp);
    } catch (...) {
      throw;
    }
  }
  _hosts.clear();
  if (this->_sendcq) {
    ret = ibv_destroy_cq(this->_sendcq);
    if (ret != 0) throw runtime_error(string("cannot destroy send cq. Err: ").append(strerror(errno)));
    this->_sendcq = NULL;
  }
  if (this->_recvcq) {
    ret = ibv_destroy_cq(this->_recvcq);
    if (ret != 0) throw runtime_error(string("cannot destroy recv cq. Err: ").append(strerror(errno)));
    this->_recvcq = NULL;
  }
  if (this->_syncScq) {
    ret = ibv_destroy_cq(this->_syncScq);
    if (ret != 0) throw runtime_error(string("cannot destroy sync send cq. Err: ").append(strerror(errno)));
    this->_syncScq = NULL;
  }
  if (this->_syncRcq) {
    ret = ibv_destroy_cq(this->_syncRcq);
    if (ret != 0) throw runtime_error(string("cannot destroy sync recv cq. Err: ").append(strerror(errno)));
    this->_syncRcq = NULL;
  }
  try {
    ibDeregMRs();
  } catch (...) {
    throw;
  }
  if (this->_pd) {
    ret = ibv_dealloc_pd(this->_pd);
    if (ret != 0) throw runtime_error(string("cannot destroy protection domain. Err: ").append(strerror(errno)));
    this->_pd = NULL;
  }
  if (this->_ctx) {
    ret = ibv_close_device(this->_ctx);
    if (ret != 0) throw runtime_error(string("cannot close device. Err: ").append(strerror(errno)));
    this->_ctx = NULL;
  }
#ifdef IB_DUMP_FILE
  _wc_dump_file.close();
  _wr_dump_file.close();
#endif
}

uint32_t IB_verbs::IB::_ibGetNextWrId()
{
  uint32_t ret_val = _wrId.fetch_add(1);
  // 0 is not a valid ID
  if (ret_val == 0) {
    return _ibGetNextWrId();
  }
  return ret_val;
}

#ifdef IB_DUMP_FILE
void IB_verbs::IB::_dump_wc(const struct ibv_wc& wc)
{
  std::lock_guard<std::mutex> file_guard(_wc_dump_file_mutex);
  _wc_dump_file << wc.qp_num;
  _wc_dump_file << "," << wc.status << "," << wc.opcode << "," << wc.wr_id << std::endl;
}

void IB_verbs::IB::_dump_wr(const IB_verbs::IB::ib_wr& wr)
{
  std::lock_guard<std::mutex> file_guard(_wr_dump_file_mutex);
  _wr_dump_file << wr.qp_num << "," << wr.src_qp;
  if (!wr.receive) {
    _wr_dump_file << "," << wr.opcode;
  } else {
    _wr_dump_file << ",recv";
  }
  _wr_dump_file << std::endl;
}
#endif
