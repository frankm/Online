#include "EventBuilding/MFP_tools.hpp"
#include "EventBuilding/tools.hpp"
#include <iostream>
#include <sstream>

size_t EB::MFP_header_size(int packing_fraction)
{
  return sizeof(MFP_header) +
         aligned_array_end(reinterpret_cast<bank_type_type*>(sizeof(MFP_header)), packing_fraction, MFP_alignment) +
         aligned_array_size<bank_size_type>(packing_fraction, MFP_alignment);
}

EB::bank_type_type* EB::MFP_header::bank_types()
{
  return reinterpret_cast<uint8_t*>(reinterpret_cast<uintptr_t>(this) + sizeof(MFP_header));
}

const EB::bank_type_type* EB::MFP_header::bank_types() const { return const_cast<MFP_header*>(this)->bank_types(); }

EB::bank_size_type* EB::MFP_header::bank_sizes()
{
  return reinterpret_cast<bank_size_type*>(
    reinterpret_cast<uintptr_t>(bank_types()) + aligned_array_size<bank_type_type>(n_banks, MFP_alignment));
  // this should be used if the ftype array is not aligned
  // bank_types() + aligned_array_end(reinterpret_cast<bank_type_type*>(&align), n_banks, MFP_alignment)));
}

const EB::bank_size_type* EB::MFP_header::bank_sizes() const { return const_cast<MFP_header*>(this)->bank_sizes(); }

size_t EB::MFP_header::header_size() const { return MFP_header_size(n_banks); }

size_t EB::MFP_header::bytes() const { return packet_size; }

void* EB::MFP::payload()
{
  return reinterpret_cast<void*>(
    reinterpret_cast<uintptr_t>(header.bank_sizes()) +
    aligned_array_size<bank_size_type>(header.n_banks, MFP_alignment));
}

const void* EB::MFP::payload() const { return const_cast<MFP*>(this)->payload(); }

std::string EB::MFP_header::print(bool verbose) const
{
  std::stringstream os;
  os << "\n------MFP HEADER------\n";
  os << "Magic " << std::hex << magic << std::dec << "\n";
  os << "event ID " << ev_id << "\n";
  os << "source ID " << src_id << "\n";
  os << "n banks " << n_banks << "\n";
  os << "packet size " << packet_size << "\n";
  if (verbose) {
    auto types = bank_types();
    auto sizes = bank_sizes();
    for (int k = 0; k < n_banks; k++) {
      os << "bank type " << EB::BankType(types[k]) << " bank size " << sizes[k] << "\n";
    }
  }

  return os.str();
}

std::ostream& EB::operator<<(std::ostream& os, const MFP_header& header) { return os << header.print(); }

bool EB::MFP::is_header_valid() const { return (header.magic == MFP_magic); }
void EB::MFP::set_header_valid() { header.magic = MFP_magic; }

bool EB::MFP::is_end_run() const { return is_header_valid() && (header.ev_id == MFP_end_run); }
void EB::MFP::set_end_run()
{
  // TODO set the correct size
  set_header_valid();
  header.ev_id = MFP_end_run;
  header.n_banks = 0;
}

void* EB::MFP::get_bank_n(int n)
{
  void* ret_val;
  if (n >= header.n_banks) {
    ret_val = nullptr;
  } else {
    auto it = this->begin();
    for (int k = 0; k < n; k++) {
      it++;
    }
    ret_val = &(*it);
  }
  return ret_val;
}

const void* EB::MFP::get_bank_n(int n) const
{
  const void* ret_val;
  if (n >= header.n_banks) {
    ret_val = nullptr;
  } else {
    auto it = this->cbegin();
    for (int k = 0; k < n; k++) {
      it++;
    }
    ret_val = &(*it);
  }
  return ret_val;
}

void* EB::MFP::operator[](int n)
{
  auto it = this->begin();
  for (int k = 0; k < n; k++) {
    it++;
  }
  return &(*it);
}

const void* EB::MFP::operator[](int n) const
{
  auto it = this->cbegin();
  for (int k = 0; k < n; k++) {
    it++;
  }
  return &(*it);
}

bool EB::MFP::is_wrap() const { return header.magic == MFP_wrap; }
void EB::MFP::set_wrap() { header.magic = MFP_wrap; }

std::string EB::MFP::print(bool verbose) const
{
  std::stringstream os;
  os << header.print(verbose);
  os << "Payload " << payload() << '\n';

  if (verbose) {
    // TODO add something for the banks
  }

  return os.str();
}
std::ostream& EB::operator<<(std::ostream& os, const MFP& header) { return os << header.print(); }

// iterator implementation

EB::MFP::iterator::reference EB::MFP::iterator::operator*() const { return *_bank; }
EB::MFP::iterator::pointer EB::MFP::iterator::operator->() { return _bank; }
EB::MFP::iterator& EB::MFP::iterator::operator++()
{
  auto padded_size = *_size + get_padding(*_size, 1 << _align);
  _bank += padded_size;
  _size++;
  return *this;
}

EB::MFP::iterator EB::MFP::iterator::operator++(int)
{
  iterator tmp = *this;
  ++(*this);
  return tmp;
}
bool EB::operator==(const EB::MFP::iterator& a, const EB::MFP::iterator& b) { return a._size == b._size; }
bool EB::operator!=(const EB::MFP::iterator& a, const EB::MFP::iterator& b) { return !(a == b); }

// const iterator implementation
EB::MFP::const_iterator::reference EB::MFP::const_iterator::operator*() const { return *_bank; }
EB::MFP::const_iterator::pointer EB::MFP::const_iterator::operator->() { return _bank; }
EB::MFP::const_iterator& EB::MFP::const_iterator::operator++()
{
  auto padded_size = *_size + get_padding(*_size, 1 << _align);
  _bank += padded_size;
  _size++;
  return *this;
}

EB::MFP::const_iterator EB::MFP::const_iterator::operator++(int)
{
  const_iterator tmp = *this;
  ++(*this);
  return tmp;
}
bool EB::operator==(const EB::MFP::const_iterator& a, const EB::MFP::const_iterator& b) { return a._size == b._size; }
bool EB::operator!=(const EB::MFP::const_iterator& a, const EB::MFP::const_iterator& b) { return a._size != b._size; }
