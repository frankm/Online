#include "pcie40_reader.hpp"
#include "EventBuilding/tools.hpp"
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <limits>
#include <cstdlib>

EB::PCIe40_reader::PCIe40_reader() { init_internal_status(); }

EB::PCIe40_reader::PCIe40_reader(int id, int stream) : PCIe40_reader()
{
  _stream = stream;
  int err_val = open(id);
  if (err_val != 0) {
    std::ostringstream err_mess;
    err_mess << "EB::PCIe40_reader: Unable to open device " << id << " err code " << err_val << " " << strerror(errno);
    throw std::invalid_argument(err_mess.str());
  }
}

EB::PCIe40_reader::PCIe40_reader(const std::string& name, int stream) : PCIe40_reader()
{
  _stream = stream;
  int err_val = open(name);
  if (err_val != 0) {
    std::ostringstream err_mess;
    err_mess << "EB::PCIe40_reader: Unable to open device " << name << " err code " << err_val << " "
             << strerror(errno);
    throw std::invalid_argument(err_mess.str());
  }
}

EB::PCIe40_reader::~PCIe40_reader() { close(); }

void EB::PCIe40_reader::init_internal_status()
{
  _stream = P40_DAQ_STREAM::P40_DAQ_STREAM_NULL;
  _stream_fd = -1;
  _id_fd = -1;
  _ctrl_fd = -1;
  _buffer = NULL;
  _buffer_size = 0;
  _requested_size = 0;
  _name = "undefined_device";
}

EB::PCIe40_reader::PCIe40_reader(PCIe40_reader&& other) :
  _buffer(other._buffer), _buffer_size(other._buffer_size), _internal_read_off(other._internal_read_off),
  _device_read_off(other._device_read_off), _available_data(other._available_data),
  _requested_size(other._requested_size), _stream(other._stream), _id_fd(other._id_fd), _stream_fd(other._stream_fd),
  _name(std::move(other._name)), PCIe40_alignment(other.PCIe40_alignment)
{
  // Reset internal status of the moved object
  other.init_internal_status();
}

EB::PCIe40_reader& EB::PCIe40_reader::operator=(PCIe40_reader&& other)
{
  // protection against self move
  if (this == &other) {
    // Close currently open stream
    // TODO this should be called only if is_open()
    close();

    _buffer = other._buffer;
    _buffer_size = other._buffer_size;
    _internal_read_off = other._internal_read_off;
    _device_read_off = other._device_read_off;
    _available_data = other._available_data;
    _requested_size = other._requested_size;
    _stream = other._stream;
    _id_fd = other._id_fd;
    _stream_fd = other._stream_fd;
    _name = std::move(other._name);
    PCIe40_alignment = other.PCIe40_alignment;

    // Reset internal status of the moved object
    other.init_internal_status();
  }

  return *this;
}

int EB::PCIe40_reader::open(int id)
{
  int ret_val = 0;

  if (is_open()) {
    close();
  }

  _id_fd = p40_id_open(id);

  if (_id_fd < 0) {
    ret_val = -1;
    close();
    return ret_val;
  }

  _stream_fd = p40_stream_open(id, static_cast<P40_DAQ_STREAM>(_stream));
  if (_stream_fd < 0) {
    ret_val = -2;
    close();
    return ret_val;
  }

  int enabled = p40_stream_enabled(_stream_fd);
  if (enabled < 0) {
    close();
    return -3;
  } else if (enabled == 0) {
    close();
    return -4;
  }

  if (p40_stream_lock(_stream_fd) < 0) {
    close();
    return -5;
  }

  ssize_t size = p40_stream_get_host_buf_bytes(_stream_fd);
  if (size < 0) {
    close();
    return -6;
  }

  _buffer_size = size;
  _buffer = p40_stream_map(_stream_fd);
  if (_buffer == NULL) {
    close();
    return -7;
  }

  _ctrl_fd = p40_ctrl_open(id);
  if (_ctrl_fd < 0) {
    ret_val = -8;
    close();
    return ret_val;
  }

  if (update_device_ptr() != 0) {
    close();
    return -10;
  }

  _internal_read_off = _device_read_off;
  _requested_size = 0;

  // TODO check with Paolo if there is a constant defined somewhere
  char name[16];

  if (p40_id_get_name(_id_fd, name, sizeof(name)) != 0) {
    close();
    return -9;
  }

  _name = name;

  int vers_id = p40_id_get_source(_id_fd);

  _src_id = (vers_id & (src_num_mask | src_subsystem_mask)) >> src_num_shift;
  _block_version = (vers_id & src_version_mask) >> src_version_shift;

  return ret_val;
}

int EB::PCIe40_reader::reset()
{
  int ret_val = 0;

  if (!is_open()) {
    ret_val = -1;
    return ret_val;
  }

  // ret_val = p40_ctrl_reset_logic(_ctrl_fd);
  ret_val = p40_stream_reset_logic(_stream_fd);
  if (ret_val != 0) {
    return ret_val;
  }

  ret_val = update_device_ptr();
  if (ret_val != 0) {
    return ret_val;
  }

  _internal_read_off = _device_read_off;
  _requested_size = 0;
  update_usage();

  return ret_val;
}

int EB::PCIe40_reader::open(const std::string& name)
{
  int ret_val = 1;
  int id = p40_id_find(name.c_str());
  if (id >= 0) {
    ret_val = open(id);
  }

  return ret_val;
}

const std::string EB::PCIe40_reader::get_name() const { return _name; }

void EB::PCIe40_reader::close()
{
  // Make sure that this function does not change errno, so it's easier to use in cleanups
  int saved_errno = errno;

  if (_stream_fd >= 0) {
    p40_stream_unlock(_stream_fd);
    // No need to call p40_stream_unmap: p40_stream_close does it for us.
    p40_stream_close(_stream_fd, _buffer);
  }
  if (_id_fd >= 0) {
    p40_id_close(_id_fd);
  }
  if (_ctrl_fd >= 0) {
    p40_ctrl_close(_ctrl_fd);
  }

  init_internal_status();

  errno = saved_errno;
}

bool EB::PCIe40_reader::is_open() { return (_id_fd >= 0) && (_stream_fd >= 0) && (_buffer != NULL); }

int EB::PCIe40_reader::update_device_ptr()
{
  int ret_val = 0;
  _device_read_off = p40_stream_get_host_buf_read_off(_stream_fd);

  if (_device_read_off < 0) {
    ret_val = -1;
  }

  return ret_val;
}

std::vector<std::tuple<void*, size_t>> EB::PCIe40_MFP_reader::get_full_buffer()
{
  std::vector<std::tuple<void*, size_t>> ret_val;
  if (is_open()) {
    // double the size because of the double mapping
    ret_val.emplace_back(std::make_pair(_buffer, 2 * _buffer_size));
  }

  return ret_val;
}

// TODO check if something special is needed for fragments
template<>
pcie40_frg_hdr* EB::PCIe40_reader::extract_element()
{
  pcie40_frg_hdr* ret_val = NULL;
  if (!is_open()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << ": extract_element() device not open.";
    throw std::logic_error(err_mess.str());
  }
  size_t size;
  if (_available_data >= sizeof(pcie40_frg_hdr)) {
    ret_val = reinterpret_cast<pcie40_frg_hdr*>(reinterpret_cast<uintptr_t>(_buffer) + _internal_read_off);
    if (ret_val->ghdr.is_invalid()) {
      std::ostringstream err_mess;
      err_mess << "EB::PCIe40_reader: corrupted data stream on " << _name << " invalid fragment";
      throw std::runtime_error(err_mess.str());
    }

    size = ret_val->bytes();
    size += ret_val->padding();
    // TODO check if this statemnt is still valid
    if (_available_data < size) {
      // The current version of the PCIe40 API can have incompete MFPs in the stream this check is mandatory and a
      // failure should not trigger an error
      ret_val = NULL;
    } else {
      _internal_read_off += size;
      _requested_size += size;
      _available_data -= size;
      // wrap around (the memory region is mapped two times contiguosly)
      if (_internal_read_off > _buffer_size) {
        _internal_read_off -= _buffer_size;
      }
    }
  }

  return ret_val;
}

template<>
EB::MFP* EB::PCIe40_reader::extract_element()
{
  EB::MFP* ret_val = NULL;
  if (!is_open()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << ": extract_element() device not open.";
    throw std::logic_error(err_mess.str());
  }
  size_t size;
  if (_available_data >= sizeof(EB::MFP)) {
    ret_val = reinterpret_cast<EB::MFP*>(reinterpret_cast<uintptr_t>(_buffer) + _internal_read_off);
    if (!ret_val->is_header_valid()) {
      std::ostringstream err_mess;
      err_mess << "EB::PCIe40_reader: corrupted data stream on " << _name << " invalid MFP";
      throw std::runtime_error(err_mess.str());
    }

    size = ret_val->bytes();
    size += get_padding(size, 1 << PCIe40_alignment);
    if (_available_data < size) {
      // The current version of the PCIe40 API can have incompete MFPs in the stream this check is mandatory and a
      // failure should not trigger an error
      ret_val = NULL;
    } else {
      _internal_read_off += size;
      _requested_size += size;
      // wrap around (the memory region is mapped two times contiguosly)
      if (_internal_read_off > _buffer_size) {
        _internal_read_off -= _buffer_size;
      }
    }
  }

  return ret_val;
}

void EB::PCIe40_reader::update_usage()
{
  _available_data = p40_stream_get_host_buf_bytes_used(_stream_fd);
  _available_data -= _requested_size;
}

void EB::PCIe40_reader::ack_read()
{
  if (!is_open()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << ": ack_read() device not open.";
    throw std::logic_error(err_mess.str());
  }

  if (p40_stream_free_host_buf_bytes(_stream_fd, _requested_size) < 0) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << ": ack_read() unable to free buffer space.";
    throw std::runtime_error(err_mess.str());
  }

  if (update_device_ptr() != 0) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << ": ack_read() unable to update device ptr.";
    throw std::runtime_error(err_mess.str());
  }

  _internal_read_off = _device_read_off;
  _requested_size = 0;
  update_usage();
}

void EB::PCIe40_reader::cancel_pending()
{
  // All the calls in this method are local and do not query the device
  _internal_read_off = _device_read_off;
  _available_data += _requested_size;
  _requested_size = 0;
}

EB::MFP* EB::PCIe40_MFP_reader::try_get_element()
{
  update_usage();
  return extract_element<EB::MFP>();
}

void EB::PCIe40_MFP_reader::read_complete() { ack_read(); }

void EB::PCIe40_MFP_reader::flush() { reset(); }

int EB::PCIe40_MFP_reader::get_src_id() const { return _src_id; }

EB::PCIe40_MFP_reader::PCIe40_MFP_reader() : PCIe40_reader() {}

EB::PCIe40_MFP_reader::PCIe40_MFP_reader(int id, int stream) : PCIe40_reader(id, stream) {}

EB::PCIe40_MFP_reader::PCIe40_MFP_reader(const std::string& name, int stream) : PCIe40_reader(name, stream) {}

EB::PCIe40_frag_reader::PCIe40_frag_reader() : _pcie40_reader(), _n_frags(1) {}

EB::PCIe40_frag_reader::PCIe40_frag_reader(
  int id,
  size_t buffer_size,
  int n_frags,
  size_t buffer_alignment,
  int stream) :
  _pcie40_reader(id, stream)
{
  set_n_frags(n_frags);
  Shared_ptr_buffer_backend buffer_reader_backend(buffer_size, buffer_alignment);
  // TODO maybe there is a better way to copy this
  Shared_ptr_buffer_backend buffer_writer_backend(buffer_reader_backend);
  _internal_buffer_reader.reset_backend(std::move(buffer_reader_backend));
  _internal_buffer_writer.reset_backend(std::move(buffer_writer_backend));
}

EB::PCIe40_frag_reader::PCIe40_frag_reader(
  const std::string& name,
  size_t buffer_size,
  int n_frags,
  size_t buffer_alignment,
  int stream) :
  _pcie40_reader(name, stream)
{
  set_n_frags(n_frags);
  Shared_ptr_buffer_backend buffer_reader_backend(buffer_size, buffer_alignment);
  // TODO maybe there is a better way to copy this
  Shared_ptr_buffer_backend buffer_writer_backend(buffer_reader_backend);
  _internal_buffer_reader.reset_backend(std::move(buffer_reader_backend));
  _internal_buffer_writer.reset_backend(std::move(buffer_writer_backend));
}

void EB::PCIe40_frag_reader::set_n_frags(int n_frags)
{
  if ((n_frags > 0) && (n_frags <= std::numeric_limits<decltype(_n_frags)>::max())) {
    _n_frags = n_frags;
    _frag_list.reserve(_n_frags);
    _type_list.reserve(_n_frags);
    _size_list.reserve(_n_frags);
  } else {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << ": set_n_frags() invalid n_frags value: " << n_frags
             << ". Expected value > 0  and <= " << std::numeric_limits<decltype(_n_frags)>::max();
    throw std::runtime_error(err_mess.str());
  }
}

int EB::PCIe40_frag_reader::get_n_frags() const { return _n_frags; }

EB::MFP* EB::PCIe40_frag_reader::try_get_element()
{
  if (!_pcie40_reader.is_open()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << ": try_get_element() device " << _pcie40_reader.get_name()
             << " is not open.";
    throw std::logic_error(err_mess.str());
  }

  _pcie40_reader.update_usage();

  // all the fragments from the buffer are read and the MFPs are generated
  // TODO check if the FPGA is no overwhelming us
  if (!_flush_pending) {
    scan_frag_list();
  }

  int mfp_err = build_MFP();
  if (mfp_err < 0) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << ": build_MFP() device " << _pcie40_reader.get_name() << ": ";
    if (mfp_err == -1) {
      err_mess << "Internal buffer full unable to copy the next MFP";
    } else if (mfp_err == -2) {
      err_mess << "Internal buffer full unable to copy the flush MFP";
    } else {
      err_mess << "Unknown error";
    }

    throw std::runtime_error(err_mess.str());
  }

  // _pcie40_reader.ack_read();
  // _internal_buffer_writer.write_complete();

  return _internal_buffer_reader.try_get_element();
}

void EB::PCIe40_frag_reader::scan_frag_list()
{
  if (!_pcie40_reader.is_open()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << ": scan_frag_list() device " << _pcie40_reader.get_name()
             << " is not open.";
    throw std::logic_error(err_mess.str());
  }

  while (_frag_list.size() < _n_frags) {
    pcie40_frg_hdr* next_frag = _pcie40_reader.extract_element<pcie40_frg_hdr>();
    // no more data into the device buffer
    if (next_frag == NULL) {
      _flush = false;
      break;
    } else if (next_frag->flushed()) {
      // The flush fragment is discarted
      // The first flush ends this loop and triggers the generation of an end of run MFP, all the subsequent consecutive
      // flushes are simply ignored
      if (!_flush) {
        _flush = true;
        break;
      } else {
        continue;
      }
    } else {
      // Valid data deassert the flush flag
      _flush = false;
    }

    // we strip off the fragment header
    size_t header_offset = sizeof(next_frag->le_evid) + sizeof(next_frag->ghdr);
    _frag_list.emplace_back(reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(next_frag) + header_offset));
    _size_list.emplace_back(next_frag->bytes() - header_offset);
    _type_list.emplace_back(next_frag->ghdr.type());
    _total_size += _size_list.back() + get_padding(_size_list.back(), 1 << _align);
    // we store only the first ev_id
    if (_frag_list.size() == 1) {
      _ev_id = next_frag->evid();
      _version = next_frag->ghdr.version();
    }
  }
}

int EB::PCIe40_frag_reader::build_MFP()
{
  int ret_val = 0;

  // TODO check how the flushed events should be handled
  if ((_frag_list.size() == _n_frags) || _flush) {
    // If the buffer was full when writing flush the frag list is empty and no MFP should be generated
    if (_frag_list.size() > 0) {
      // TODO remove the padding for the last element
      size_t MFP_size =
        // EB::MFP_header_size(_frag_list.size(), 1 << _align) + _total_size - get_padding(_size_list.back(), 1 <<
        // _align);
        EB::MFP_header_size(_frag_list.size()) + _total_size;
      EB::MFP* new_MFP = _internal_buffer_writer.try_write_next_element(MFP_size);
      if (new_MFP != NULL) {
        new_MFP->set_header_valid();
        new_MFP->header.n_banks = _frag_list.size();
        new_MFP->header.packet_size = MFP_size;
        new_MFP->header.ev_id = _ev_id;
        new_MFP->header.src_id = _pcie40_reader._src_id;
        new_MFP->header.align = _align;
        new_MFP->header.block_version = _version;

        std::memcpy(new_MFP->header.bank_types(), _type_list.data(), _type_list.size() * sizeof(_type_list.front()));
        std::memcpy(new_MFP->header.bank_sizes(), _size_list.data(), _size_list.size() * sizeof(_size_list.front()));
        // copy the list of fragments
        EB::MFP::iterator MFP_it = new_MFP->begin();
        for (int index = 0; index < _frag_list.size(); index++) {
          std::memcpy(reinterpret_cast<void*>(&(*MFP_it)), _frag_list[index], _size_list[index]);
          MFP_it++;
        }

        // the last fragment has no padding
        // std::memcpy(reinterpret_cast<void*>(&(*MFP_it)), _frag_list.back(), _size_list.back());

        _internal_buffer_writer.write_complete();
        _pcie40_reader.ack_read();
        _frag_list.clear();
        _type_list.clear();
        _size_list.clear();
        _total_size = 0;
      } else {
        return -1;
      }
    }

    if (_flush) {
      size_t MFP_size = EB::MFP_header_size(0);
      EB::MFP* new_MFP = _internal_buffer_writer.try_write_next_element(MFP_size);
      if (new_MFP != NULL) {
        _flush_pending = false;
        new_MFP->set_end_run();
        new_MFP->header.packet_size = MFP_size;
        _internal_buffer_writer.write_complete();
      } else {
        _flush_pending = true;
        ret_val = -2;
      }
    }
  }

  return ret_val;
}

void EB::PCIe40_frag_reader::read_complete() { _internal_buffer_reader.read_complete(); }

void EB::PCIe40_frag_reader::flush()
{
  // TODO check retrun value of reset
  _pcie40_reader.reset();
  _internal_buffer_reader.flush();
}

int EB::PCIe40_frag_reader::get_src_id() const { return _pcie40_reader._src_id; }

std::vector<std::tuple<void*, size_t>> EB::PCIe40_frag_reader::get_full_buffer()
{
  return _internal_buffer_reader.get_full_buffer();
}
