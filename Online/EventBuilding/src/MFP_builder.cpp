#include "MFP_builder.hpp"
#include "EventBuilding/raw_tools.hpp"
#include <memory>

EB::MFP_builder::MFP_builder() {}

int EB::MFP_builder::append_event(const EB::MDF_block& event)
{
  char* tmp_ptr = event.payload().get();
  char* buffer_end = event.payload().get() + event.payload_size();
  EB::raw_header* tmp_header = reinterpret_cast<EB::raw_header*>(tmp_ptr);
  do {
    uint8_t key = tmp_header->type;
    uint16_t value = tmp_header->src_id;
    auto map_it_emplace = _raw_map.emplace(std::make_pair(key, value), std::vector<EB::raw_header*>(1, tmp_header));
    // check if the insertion of the new element succeded
    if (!map_it_emplace.second) {
      map_it_emplace.first->second.push_back(tmp_header);
    } else {
      // if the source is a new one the ev_id is set to 0
      _ev_id_map.emplace(std::make_pair(key, value), 0);
    }

    tmp_ptr += tmp_header->mem_size();
    tmp_header = reinterpret_cast<EB::raw_header*>(tmp_ptr);
  } while (tmp_header->is_valid() && (tmp_ptr < buffer_end));
  // TODO add error checking
  return 0;
}

int EB::MFP_builder::append_events(const std::vector<EB::MDF_block>& events)
{
  for (const auto& event : events) {
    append_event(event);
  }
  // TODO add error checking
  return 0;
}

std::vector<EB::type_id_pair_type> EB::MFP_builder::get_source_mapping()
{
  std::vector<EB::type_id_pair_type> key_list;
  key_list.reserve(_raw_map.size());
  for (const auto& map_elem : _raw_map) {
    key_list.emplace_back(map_elem.first);
  }

  return key_list;
}

// returns the number of sources
size_t EB::MFP_builder::get_source_number() { return _raw_map.size(); }

int EB::MFP_builder::build_MFP(size_t packing_fraction, char* buffer, size_t buffer_size, EB::type_id_pair_type key)
{
  int ret_val = 0;
  auto map_it = _raw_map.find(key);
  auto ev_id_it = _ev_id_map.find(key);
  if (map_it != _raw_map.end()) {
    if (map_it->second.size() < packing_fraction) {
      ret_val = -2;
      std::cerr << "ERROR not enough events provided for type " << (uint) map_it->first.first << " id "
                << map_it->first.second << " " << map_it->second.size() << "/" << packing_fraction << std::endl;
    } else {
      uint32_t block_size = 0;
      if (buffer_size < MFP_header_size(packing_fraction)) {
        // std::cerr << "ERROR insufficient buffer size" << std::endl;
        ret_val = -3;
      } else {
        MFP* next_MFP = reinterpret_cast<MFP*>(buffer);
        next_MFP->set_header_valid();
        next_MFP->header.block_version = 0;
        next_MFP->header.ev_id = ev_id_it->second;
        next_MFP->header.n_banks = packing_fraction;
        // the src_id should be the same for all the fragments from the same source
        // with the new src_id this can be taken from the map key
        // next_MFP->src_id = EB::old_to_new(map_it->second[0]->src_id);
        next_MFP->header.src_id = EB::old_to_new(key);
        next_MFP->header.align = base_MFP_align;
        uint16_t* hdr_sizes = next_MFP->header.bank_sizes();
        uint8_t* hdr_types = next_MFP->header.bank_types();
        // the block size includes the header
        // TODO check if we can use an iterator
        // char* hdr_payload = reinterpret_cast<char*>(next_MFP->payload());
        auto MFP_it = next_MFP->begin();
        size_t header_size = next_MFP->header.header_size();
        for (size_t k = 0; k < packing_fraction; k++) {
          hdr_types[k] = map_it->second[k]->type;
          hdr_sizes[k] = map_it->second[k]->payload_size();
          // check if the buffer has enough space for the next fragment
          size_t payload_mem_size = map_it->second[k]->payload_mem_size();
          size_t full_bank_size = payload_mem_size + get_padding(payload_mem_size, 1 << base_MFP_align);
          if (block_size + header_size + full_bank_size <= buffer_size) {
            memcpy(&(*MFP_it), map_it->second[k]->get_payload(), payload_mem_size);
            MFP_it++;
            // this line inserts a full raw bank uncluding the header it can be used for debugging
            //   memcpy(hdr_payload + block_size, map_it->second[k], map_it->second[k]->mem_size());
          } else {
            // std::cerr << "ERROR insufficient buffer size" << std::endl;
            ret_val = -3;
            break;
          }
          block_size += full_bank_size;
#ifdef DEBUG
          std::cout << "raw size " << hdr_sizes[k] << " aligned size " << full_bank_size << std::endl;
#endif
          // this line should be used when inserting a full raw bank
          // block_size += map_it->second[k]->mem_size();
        }
        // deleting the processed events from the internal structure
        map_it->second.erase(map_it->second.begin(), map_it->second.begin() + packing_fraction);
        next_MFP->header.packet_size = block_size + header_size;
        (ev_id_it->second) += packing_fraction;
      }
    }
  } else {
    // std::cerr << "ERROR key not found type " << (uint) key.first << " id " << key.second << std::endl;
    ret_val = -5;
  }
  return ret_val;
}

int EB::MFP_builder::build_MFPs(
  size_t packing_fraction,
  const std::vector<std::shared_ptr<char>>& buffers,
  const std::vector<size_t>& buffer_sizes)
{
  int ret_val = 0;
  // TODO implement this properly
  if ((_raw_map.size() != buffers.size()) || (_raw_map.size() != buffer_sizes.size())) {
    ret_val = -1;
    std::cerr << "ERROR not enough bufers provided" << std::endl;
  }

  if (ret_val == 0) {
    int source_idx = 0;
    for (const auto map_it : _raw_map) {
      // TODO better error code handling
      ret_val = build_MFP(packing_fraction, buffers[source_idx].get(), buffer_sizes[source_idx], map_it.first);
      // TODO find a better way of breking the loop
      if (ret_val != 0) {
        break;
      }
      source_idx++;
    }
  }
  return ret_val;
}

int EB::MFP_builder::build_MFPs(
  size_t packing_fraction,
  const std::vector<std::shared_ptr<char>>& buffers,
  const std::vector<size_t>& buffer_sizes,
  const std::vector<EB::type_id_pair_type> source_mapping)
{
  int ret_val = 0;
  if ((source_mapping.size() != buffers.size()) || (source_mapping.size() != buffer_sizes.size())) {
    ret_val = -1;
    std::cerr << "ERROR not enough bufers provided" << std::endl;
  }

  if (ret_val == 0) {
    int source_idx = 0;
    for (const auto src : source_mapping) {
      // TODO better error code handling
      ret_val = build_MFP(packing_fraction, buffers[source_idx].get(), buffer_sizes[source_idx], src);
      if (ret_val != 0) {
        break;
      }
      source_idx++;
    }
  }
  return ret_val;
}

int EB::MFP_builder::build_MFPs(
  size_t packing_fraction,
  const std::vector<char*>& buffers,
  const std::vector<size_t>& buffer_sizes)
{
  int ret_val = 0;
  // TODO implement this properly
  if ((_raw_map.size() != buffers.size()) || (_raw_map.size() != buffer_sizes.size())) {
    ret_val = -1;
    std::cerr << "ERROR not enough bufers provided" << std::endl;
  }

  if (ret_val == 0) {
    size_t source_idx = 0;
    for (const auto map_it : _raw_map) {
      // TODO better error code handling
      ret_val = build_MFP(packing_fraction, buffers[source_idx], buffer_sizes[source_idx], map_it.first);
      // TODO find a better way of breking the loop
      if (ret_val != 0) {
        break;
      }
      source_idx++;
    }
  }
  return ret_val;
}

int EB::MFP_builder::build_MFPs(
  size_t packing_fraction,
  const std::vector<char*>& buffers,
  const std::vector<size_t>& buffer_sizes,
  const std::vector<EB::type_id_pair_type> source_mapping)
{
  int ret_val = 0;
  if ((source_mapping.size() != buffers.size()) || (source_mapping.size() != buffer_sizes.size())) {
    ret_val = -1;
    std::cerr << "ERROR not enough bufers provided" << std::endl;
  }

  if (ret_val == 0) {
    size_t source_idx = 0;
    for (const auto src : source_mapping) {
      // TODO better error code handling
      ret_val = build_MFP(packing_fraction, buffers[source_idx], buffer_sizes[source_idx], src);
      if (ret_val != 0) {
        break;
      }
      source_idx++;
    }
  }
  return ret_val;
}

void EB::MFP_builder::reset_data() { _raw_map.clear(); }

void EB::MFP_builder::reset_ev_id()
{
  for (auto& map_elem : _ev_id_map) {
    map_elem.second = 0;
  }
}
void EB::MFP_builder::reset()
{
  reset_data();
  reset_ev_id();
}
