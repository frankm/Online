#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include <time.h>
#include "string.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>

#include "Dataflow/Incidents.h"
#include "Dataflow/MBMClient.h"
#include "RTL/rtl.h"

#include "events_dispatch/Unit.hpp"
#include "events_dispatch/Filter_unit.hpp"
#include "events_dispatch/common.hpp"

using namespace std;
using namespace Online;
using namespace DISPATCH;
using namespace EB;

Filter_unit::Filter_unit(const std::string& nam, DataflowContext& ctxt) : DataflowComponent(nam, ctxt)
{
  info("FU: IN CONSTRUCTOR");
  declareProperty("how_many_ous", how_many_ous = 1);
  declareProperty("how_many_fus", how_many_fus = 1);
  declareProperty("Buffer", _mbm_name = "Events");
}

int Filter_unit::initialize()
{

  info("FU: IN INTIALIZE");
  info("OUs NUMBER: %d, FUs NUMBER: %d", how_many_ous, how_many_fus);
  int sc = Component::initialize();
  if (sc != DF_SUCCESS)
    return error("Failed to initialize base-class");
  else if (!context.mbm) {
    info("mbm in context : %ld", context.mbm);
    return error("Failed to access MBM client.");
  }

  info("MBM Producer connecting to %s\n", _mbm_name.c_str());

  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &(this->rank));
  MPI_Comm_size(MPI_COMM_WORLD, &(this->worldSize));

  info("FU: MY RANK: %d, OUs NUMBER: %d, FUs NUMBER: %d \n", this->rank, how_many_ous, how_many_fus);

  this->_next_probing = 1;
  this->_all_finished_granted_transmission = 0;
  this->_mpi_fu_ready_request = MPI_REQUEST_NULL;

  this->_mpi_data_sent_requests = new MPI_Request[window_length];

  this->_mpi_mu_fu_alloc_request = MPI_REQUEST_NULL;
  for (int x = 0; x != MAX_FU_CREDITS; x++) {
    for (int y = 0; y != window_length; y++) {
      _mpi_data_sent_requests[y] = MPI_REQUEST_NULL;
    }
  }

  this->_mpi_acq_request = MPI_REQUEST_NULL;
  this->_data_buffer_current_ptr = 0;
  // counter
  this->_current_probe_completed_bytes = 0;
  // counter
  this->_all_completed_bytes = 0;
  _fu_ready_message.rank = rank;

  _write_buff = new Mbm_writer<EB::MEP>(context, RTL::processName(), _mbm_name);

  if (use_debug_mpi_barriers) MPI_Barrier(MPI_COMM_WORLD);

  return DF_SUCCESS;
}

int Filter_unit::finalize()
{
  info("FU: IN INTIALIZE");
  return DF_SUCCESS;
}

int Filter_unit::start()
{
  info("FU: IN START");
  signal_readiness();
  if (DO_WARMUP) mpi_warmup_run();

  // disabled for the WinCC runs
  if (use_debug_mpi_barriers) MPI_Barrier(MPI_COMM_WORLD);
  return DF_SUCCESS;
}

int Filter_unit::stop()
{
  // TODO implement this
  return DF_SUCCESS;
}

int Filter_unit::cancel()
{
  // TODO implement this
  return DF_SUCCESS;
}

void Filter_unit::handle(const DataflowIncident& inc)
{
  info("FU Got incident: %s of type %s", inc.name.c_str(), inc.type.c_str());
}

int Filter_unit::run()
{
  info("FU: IN RUN");

  _probe_start = g_get_real_time();
  _global_start = _probe_start;

  // asynchronous receive of message with data size
  run_recv();
  MPI_Irecv(
    &_alloc_msg, sizeof(mu_fu_alloc_msg), MPI_CHAR, 0, MU_FU_ALLOC_TAG, MPI_COMM_WORLD, &_mpi_mu_fu_alloc_request);

  info("FU rank %d IN RUN. Starting main loop \n", this->rank);
  while (true) {

    while (service_mu_alloc_call() == false) {
    }

    while (exchange_message_size() == false) {
    }

    int total_completions = 0;

    if (_current_transmission_meta.full_transmissions_no > window_length) {
      while (true) {

        int indx;
        bool data_transmission_complete = probe_for_completion(&indx);

        probe_print_throughput();
        if (data_transmission_complete == true) {
          total_completions++;
          // info("FU RANK %d: COMPLETION. MAKING NEW REQUEST AT INDEX %d. TOTAL COMPLETIONS %d out of %d \n",
          // e rank,indx,total_completions,current_transmission_meta.full_transmissions_no);

          size_t size = entry_size_kb * 1024;
          recv_from_bu(size, indx, _ou_fu_mep_size_msg.sender_rank);
          _acquired_data_ptr += size;

          _current_probe_completed_bytes += size;
          if (total_completions == _current_transmission_meta.full_transmissions_no - window_length) break;
        }
      }
    }
    // allBufferEntries
    if (_current_transmission_meta.full_transmissions_no != 0) {
      // info("FU RANK %d: FINALIZING WINDOWED TRANSMISSIONS \n", rank);
      int data_received = false;
      while (data_received == false) {
        MPI_Testall(window_length, _mpi_data_sent_requests /*[0]*/, &data_received, MPI_STATUS_IGNORE);
        probe_print_throughput();
      }
      int min = std::min(_current_transmission_meta.full_transmissions_no, window_length);
      size_t size = entry_size_kb * 1024;
      _current_probe_completed_bytes += min * size;
    }
    ////info("FU: FINALIZING REMAINDER size %ld \n", currentTransmissionMetadata.remainderSize );
    if (_current_transmission_meta.remainder_size != 0) {

      MPI_Recv(
        _acquired_data_ptr,
        _current_transmission_meta.remainder_size,
        MPI_CHAR,
        _ou_fu_mep_size_msg.sender_rank,
        BU_RU_DATA_REMAINDER_TAG,
        MPI_COMM_WORLD,
        MPI_STATUS_IGNORE);

      ////info("FU: REMAINDER SIZE %ld FINALIZED \n", currentTransmissionMetadata.remainderSize);
      _current_probe_completed_bytes += _current_transmission_meta.remainder_size;
    }
    // int64_t now = g_get_real_time();
    // info("[ %.2lf s ]  FU RANK %d: DONE RECEIVING DATA \n", ((double) now - (double) global_start) / (double)
    //  US_S,rank);
    _write_buff->write_complete();

    probe_print_throughput();
    signal_readiness();
  }
  return DF_SUCCESS;
}

void Filter_unit::signal_readiness(fu_status stat)
{
  MPI_Wait(&(_mpi_fu_ready_request), MPI_STATUS_IGNORE);
  _fu_ready_message.status = stat;
  MPI_Isend(
    &(_fu_ready_message), sizeof(mpi_fu_ready_msg), MPI_CHAR, 0, MU_TAG, MPI_COMM_WORLD, &(_mpi_fu_ready_request));
  // info("FU done signalling readiness. My free space: %ld \n", fu_ready_message.free_space);
}

int Filter_unit::pause()
{
  // TODO implement this
  info("FU pause\n");
  return DF_SUCCESS;
}

bool Filter_unit::probe_for_completion(int* index)
{
  int data_received = false;
  for (int a = 0; a != window_length; a++) {
    int test;
    // TODO - Remove dummy
    MPI_Test(&(_mpi_data_sent_requests /*[0]*/[a]), &test, MPI_STATUS_IGNORE);
    if (test == true) {
      data_received = true;
      *index = a;
      break;
    }
  }
  return data_received;
}

void Filter_unit::recv_from_bu(size_t size, int index, int senderRank, int tag)
{
  char* data = _acquired_data_ptr;
  MPI_Wait(&(_mpi_data_sent_requests /*[0]*/[index]), MPI_STATUS_IGNORE);
  MPI_Irecv(
    data, static_cast<int>(size), MPI_CHAR, senderRank, tag, MPI_COMM_WORLD, &(_mpi_data_sent_requests /*[0]*/[index]));
}

long unsigned int Filter_unit::get_entry_address(int entryIndex)
{
  long unsigned int addr = entryIndex * entry_size_kb * 1024;
  return addr;
}

void Filter_unit::probe_print_throughput()
{
  int64_t now = g_get_real_time();

  if (now - _global_start > _next_probing * US_S) {

    double tput_bytes_per_sec = static_cast<double>(_current_probe_completed_bytes) * static_cast<double>(US_S) /
                                static_cast<double>(now - _probe_start);
    double tput_gbits_per_sec = tput_bytes_per_sec * 8.0 / 1000.0 / 1000.0 / 1000.0;

    // info("BYTES : %ld COMPLETED IRECSs %ld time %lf \n",bytes ,currentProbeCompletedIrecvs ,((double)(now -
    // probeStart) ));

    _all_completed_bytes += _current_probe_completed_bytes;
    double total_tput_bytes_per_sec =
      static_cast<double>(_all_completed_bytes) * static_cast<double>(US_S) / static_cast<double>(now - _global_start);
    double total_tput_gbits_per_sec = total_tput_bytes_per_sec * 8.0 / 1000.0 / 1000.0 / 1000.0;

    info(
      "[ %.1lf s ] FU RANK %d TP %.1lf Gbits/s AVG TP %.1lf Gbits/s\n",
      (static_cast<double>(now) - static_cast<double>(_global_start)) / static_cast<double>(US_S),
      rank,
      tput_gbits_per_sec,
      total_tput_gbits_per_sec);

    this->_current_probe_completed_bytes = 0;
    _next_probing++;
    _probe_start = now;
  }
}

void Filter_unit::mpi_warmup_run()
{

  /*
      const char* mbm_data =  mbm_buffer_address(m_producer->id());
      long mbm_size;
      mbm_buffer_size(m_producer->id(), &mbm_size);
  */

  std::vector<std::tuple<void*, size_t>> buff_data = _write_buff->get_full_buffer();
  long mbm_size = std::get<1>(buff_data[0]);
  const char* mbm_data = (const char*) std::get<0>(buff_data[0]);

  info("FU: mbm size: %ld \n", mbm_size);
  int total_iterations = (int) (mbm_size / (size_t) WARMUP_MPI_SIZE);
  size_t remainder_size = mbm_size % (size_t) WARMUP_MPI_SIZE;

  info("STARTUP SIZE: %ld AND PTR: %ld . TOTAL ITERATIONS %d\n", mbm_size, mbm_data, total_iterations);

  uint8_t* dummy_buffer = (uint8_t*) memalign(2UL * 1024UL * 1024UL, WARMUP_MPI_SIZE);
  mlock(dummy_buffer, WARMUP_MPI_SIZE);
  memset(dummy_buffer, 0xFF, WARMUP_MPI_SIZE);

  size_t index = 0;
  MPI_Request s_request;
  MPI_Request r_request;
  for (int it = 0; it != total_iterations; it++) {
    info("FU WARMUP PHASE 1 SEND: %d\n", it);
    MPI_Irecv(dummy_buffer, WARMUP_MPI_SIZE, MPI_CHAR, rank, MPI_ANY_TAG, MPI_COMM_WORLD, &r_request);
    MPI_Isend(&(mbm_data[index]), WARMUP_MPI_SIZE, MPI_CHAR, rank, rank, MPI_COMM_WORLD, &s_request);
    MPI_Wait(&s_request, MPI_STATUS_IGNORE);
    MPI_Wait(&r_request, MPI_STATUS_IGNORE);
    index += WARMUP_MPI_SIZE;
  }

  MPI_Irecv(dummy_buffer, remainder_size, MPI_CHAR, rank, MPI_ANY_TAG, MPI_COMM_WORLD, &r_request);
  MPI_Isend(&(mbm_data[index]), remainder_size, MPI_CHAR, rank, rank, MPI_COMM_WORLD, &s_request);
  MPI_Wait(&s_request, MPI_STATUS_IGNORE);
  MPI_Wait(&r_request, MPI_STATUS_IGNORE);

  index = 0;
  for (int it = 0; it != total_iterations - 1; it++) {
    info("FU WARMUP PHASE 2 SEND: %d\n", it);
    MPI_Irecv(dummy_buffer, WARMUP_MPI_SIZE, MPI_CHAR, rank, MPI_ANY_TAG, MPI_COMM_WORLD, &r_request);
    MPI_Isend(
      &(mbm_data[index + WARMUP_MPI_SIZE / 2]), WARMUP_MPI_SIZE, MPI_CHAR, rank, rank, MPI_COMM_WORLD, &s_request);
    MPI_Wait(&s_request, MPI_STATUS_IGNORE);
    MPI_Wait(&r_request, MPI_STATUS_IGNORE);

    index += WARMUP_MPI_SIZE;
  }

  free(dummy_buffer);
}

bool Filter_unit::exchange_message_size()
{

  int flag = false;
  MPI_Test(&(_mpi_acq_request), &flag, MPI_STATUS_IGNORE);

  if (flag == true) {
    // info("FU RECEIVED MESSAGE SIZE %ld fromi OU RANK %d\n", ou_fu_mep_size_msg.size, ou_fu_mep_size_msg.sender_rank);
    int buRank = _ou_fu_mep_size_msg.sender_rank;

    if (_last_allocated_message_size != _ou_fu_mep_size_msg.size) {
      info("ERROR - EXCHANGED SIZE DOESN'T EQUAL THE ALLOCATED ONE");
    }

    get_meta(_ou_fu_mep_size_msg.size);

    init_new_transmission(_ou_fu_mep_size_msg.sender_rank);
    MPI_Ssend(&(_ou_fu_mep_size_msg), sizeof(_ou_fu_mep_size_msg), MPI_CHAR, buRank, BU_RU_SIZE_TAG, MPI_COMM_WORLD);
    // info("FU RANK %d EXCHANGED MESSAGE SIZE: %ld \n", rank, ou_fu_mep_size_msg.size);
    run_recv();

    return true;
  }
  return false;
}

void Filter_unit::run_recv()
{
  MPI_Irecv(
    &(_ou_fu_mep_size_msg),
    sizeof(_ou_fu_mep_size_msg),
    MPI_CHAR,
    MPI_ANY_SOURCE,
    BU_RU_SIZE_TAG,
    MPI_COMM_WORLD,
    (&_mpi_acq_request));
}

void Filter_unit::get_meta(size_t size)
{
  _current_transmission_meta.full_transmissions_no = size / entry_size_b;
  _current_transmission_meta.remainder_size = size % entry_size_b;
}

void Filter_unit::init_new_transmission(int senderRank)
{

  unsigned int to_send = std::min(window_length, _current_transmission_meta.full_transmissions_no);
  for (unsigned int i = 0; i != to_send; i++) {
    size_t size = entry_size_kb * 1024;
    recv_from_bu(size, i, senderRank);
    _acquired_data_ptr += size;
  }
}

bool Filter_unit::service_mu_alloc_call()
{
  int flag;
  MPI_Test(&_mpi_mu_fu_alloc_request, &flag, MPI_STATUS_IGNORE);
  if (flag) {
    EB::MEP* alloc_header = _write_buff->try_write_next_element(_alloc_msg.size);
    bool alloc_success = (alloc_header != NULL);
    _alloc_msg.success = alloc_success;
    if (alloc_success) {
      _acquired_data_ptr = (char*) alloc_header;
      _last_allocated_message_size = _alloc_msg.size;
    }
    MPI_Ssend(&_alloc_msg, sizeof(mu_fu_alloc_msg), MPI_CHAR, 0, MU_FU_ALLOC_TAG, MPI_COMM_WORLD);
    MPI_Irecv(
      &_alloc_msg, sizeof(mu_fu_alloc_msg), MPI_CHAR, 0, MU_FU_ALLOC_TAG, MPI_COMM_WORLD, &_mpi_mu_fu_alloc_request);
    return alloc_success;
  }
  return false;
}
