#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include <time.h>
#include <sys/timerfd.h>
#include <glib.h>
#include <sys/mman.h>
#include <algorithm>

#include "events_dispatch/Output_unit.hpp"
#include "events_dispatch/common.hpp"

using namespace Online;
using namespace DISPATCH;
using namespace EB;

/// Standard Constructor
Output_unit::Output_unit(const std::string& nam, DataflowContext& ctxt) : DataflowComponent(nam, ctxt), Unit()
{
  info("OU: IN CONSTRUCTOR");
  declareProperty("how_many_ous", how_many_ous = 1);
  declareProperty("how_many_fus", how_many_fus = 1);
  declareProperty("buffer_type", _buffer_type = 1);
  declareProperty("Buffer", _mbm_name = "Buffer");
  declareProperty("Requirements", _m_req);
}

/// IService implementation: initialize the service
int Output_unit::initialize()
{
  info("OU: IN INTIALIZE");

  int sc = Component::initialize();
  if (sc != DF_SUCCESS) return error("Failed to initialize base-class");
  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &(this->rank));
  MPI_Comm_size(MPI_COMM_WORLD, &(this->worldSize));

  info("OU: MY RANK: %d, OUs NUMBER: %d, FUs NUMBER: %d", this->rank, how_many_ous, how_many_fus);

  this->_next_probing = 1;
  this->_delay_timer = 0;
  this->_total_serviced_events = 0;

  this->_started_credited_transmissions = new int[MAX_BU_CREDITS];
  for (auto i = 0; i != MAX_BU_CREDITS; i++) {
    this->_started_credited_transmissions[i] = 0;
  }

  this->_dataTransmissionRequests = new MPI_Request*[MAX_BU_CREDITS];
  for (auto i = 0; i != MAX_BU_CREDITS; i++) {
    this->_dataTransmissionRequests[i] = new MPI_Request[window_length];
  }

  this->_current_transmission_meta = new current_transmission_metadata[MAX_BU_CREDITS];

  this->_credit_free_array = new bool[MAX_BU_CREDITS];
  for (auto x = 0; x != MAX_BU_CREDITS; x++) {
    this->_credit_free_array[x] = true;
  }

  for (auto x = 0; x != MAX_BU_CREDITS; x++) {
    for (auto y = 0; y != window_length; y++) {
      this->_dataTransmissionRequests[x][y] = MPI_REQUEST_NULL;
    }
  }

  this->_bu_ready_requests = MPI_REQUEST_NULL;
  this->_trans_granted_requests = MPI_REQUEST_NULL;
  this->_pending_credits_no = 0;
  this->_credits_to_rank_array = new int[MAX_BU_CREDITS];
  this->_pending_credits_array = new int[MAX_BU_CREDITS];

  this->_ready_mep_array = new size_t[MAX_MEPS];

  this->_all_completed_bytes = 0;
  this->_current_probe_completed_bytes = 0;

  // disabled for the WinCC runs
  if (use_debug_mpi_barriers) MPI_Barrier(MPI_COMM_WORLD);

  return sc;
}

/// IService implementation: finalize the service
int Output_unit::finalize()
{
  MPI_Finalize();
  return DF_SUCCESS;
}

/// IService implementation: finalize the service
/// TODO
int Output_unit::stop()
{
  // TODO implement this
  return DF_SUCCESS;
}

/// Cancel I/O operations of the dataflow component
int Output_unit::cancel()
{
  // TODO implement this
  return DF_SUCCESS;
}

/// Stop gracefully execution of the dataflow component
int Output_unit::pause()
{
  // TODO implement this
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void Output_unit::handle(const DataflowIncident& inc)
{
  info("OU Got incident: %s of type %s", inc.name.c_str(), inc.type.c_str());
}

/// Service implementation: start of the service
// TODO
int Output_unit::start()
{
  info("OU: IN START");

  if (_buffer_type == 1) {
    info("OU: SHMEM INIT");
    std::stringstream shmem_name = get_shmem_name("BU", (this->rank) - 1);
    info("In config_shmem. Shmem name: %s", shmem_name.str().c_str());
    _recv_buff = new Shmem_buffer_reader<EB::MEP>(shmem_name.str());
  } else if (_buffer_type == 2) {
    info("OU: MBM INIT. MBM Name %s", _mbm_name.c_str());
    _recv_buff = new Mbm_reader<EB::MEP>(context, RTL::processName(), _mbm_name, _m_req);
    info("OU: PAST MBM INIT");
  }

  if (DO_WARMUP) mpi_warmup_run();

  // disabled for the WinCC runs
  if (use_debug_mpi_barriers) MPI_Barrier(MPI_COMM_WORLD);

  return DF_SUCCESS;
}

/// IRunable implementation : Run the class implementation
int Output_unit::run()
{
  info("OU: IN RUN");

  int64_t probe_start;
  this->probe_global_start = g_get_real_time();
  probe_start = probe_global_start;
  this->_now = probe_start;
  this->_start_timer = probe_start;

  start_receive_from_mu();

  info("OU rank %d IN RUN: Warmup done.  Starting main loop \n", this->rank);

  while (true) {
    if (_ready_mep_vector.empty()) {
      std::vector<mep_entry> new_mep_vector = get_meps_vector();
      if (!new_mep_vector.empty()) {
        signal_new_meps_ready(new_mep_vector);
        _ready_mep_vector.insert(_ready_mep_vector.end(), new_mep_vector.begin(), new_mep_vector.end());
      }
    }

    update_granted_transmissions();

    if (_pending_credits_no > 0 && _ready_mep_vector.size() != 0) {

      _current_transmission = _ready_mep_vector[0];
      int ready_rank = _pending_credits_array[_pending_credits_no - 1];
      _pending_credits_no--;
      int slot = get_free_slot();
      get_meta(std::get<1>(_current_transmission), slot);
      exchange_message_size(ready_rank, std::get<1>(_current_transmission));

      this->_credit_free_array[slot] = false;
      this->_started_credited_transmissions[slot] = 0;
      this->_credits_to_rank_array[slot] = ready_rank;
      start_send_data_to_fu(ready_rank, slot);
    }

    int ready_rank = 0;
    if (check_if_granted_transmission(&ready_rank) == true) {
      // int64_t	probe_time = g_get_real_time();
      ////printf("[ %.4lf s ]  OU RANK %d WAS GRANTED TRANSMISSION to RANK %d
      ///\n",((double)probe_time-(double)probe_global_start)/(double)US_S,this->rank,ready_rank);
      _pending_credits_array[_pending_credits_no] = ready_rank;
      _pending_credits_no++;
      start_receive_from_mu();
    }

    int64_t probe_now = g_get_real_time();
    if (probe_now - probe_global_start > _next_probing * US_S) {
      double tput_bytes_per_sec = static_cast<double>(_current_probe_completed_bytes) * static_cast<double>(US_S) /
                                  (static_cast<double>(probe_now - probe_start));
      double tput_gbits_per_sec = tput_bytes_per_sec * 8.0 / 1000.0 / 1000.0 / 1000.0;

      _all_completed_bytes += _current_probe_completed_bytes;
      double total_tput_bytes_per_sec = static_cast<double>(_all_completed_bytes) * static_cast<double>(US_S) /
                                        (static_cast<double>(probe_now - probe_global_start));
      double total_tput_gbits_per_sec = total_tput_bytes_per_sec * 8.0 / 1000.0 / 1000.0 / 1000.0;
      info(
        "[ %.1lf s ] OU RANK %d TP %.4lf Gbits/s AVG TP Gbits/s %.1lf\n",
        static_cast<double>((probe_now) - (probe_global_start)) / static_cast<double>(US_S),
        rank,
        tput_gbits_per_sec,
        total_tput_gbits_per_sec);

      probe_start = probe_now;
      _next_probing++;
      _current_probe_completed_bytes = 0;
    }
  }
  return DF_SUCCESS;
}

void Output_unit::start_send_data_to_fu(int rank, int slot)
{
  // int64_t now = g_get_real_time();
  // printf("[ %.1lf s ] OU RANK: %d FIRST SENDING TO FU %d. ADDR %ld size %ld  \n",(((double) now - (double)
  // probe_global_start) / (double) US_S), this->rank,rank, std::get<0>(currentTransmission) - dataBufferPtr->data,
  // std::get<1>(currentTransmission));
  unsigned int to_send = std::min(window_length, _current_transmission_meta[slot].full_transmissions_no);

  // size_t off =(size_t)std::get<0>(currentTransmission);
  // printf("offset for RANK %ld: %ld\n",off-(size_t)this->dataBufferPtr->data, rank);
  // printf("OU : initializing with %d transmissions out of %d \n", to_send,
  // currentTransmissionMetadata[slot].fullTransmissionsNo);
  for (unsigned int i = 0; i != to_send; i++) {
    size_t addr =
      static_cast<size_t>(_started_credited_transmissions[slot]) * static_cast<size_t>(entry_size_kb) * 1024UL;
    char* offset = std::get<0>(_current_transmission) + addr;

    MPI_Wait(&(_dataTransmissionRequests[slot][i]), MPI_STATUS_IGNORE);
    MPI_Isend(
      offset,
      entry_size_kb * 1024,
      MPI_CHAR,
      rank,
      BU_RU_DATA_TAG,
      MPI_COMM_WORLD,
      &(_dataTransmissionRequests[slot][i]));

    _started_credited_transmissions[slot]++;
  }
}

void Output_unit::signal_new_meps_ready(std::vector<mep_entry> new_mep_vector)
{
  // printf("""SENDING EVENTS READY TO MU \n");
  _bu_ready_message.rank = this->rank;
  _bu_ready_message.untransmitted_meps_number = new_mep_vector.size();

  get_array_of_meps(new_mep_vector);
  MPI_Send(&(_bu_ready_message), sizeof(mpi_bu_ready_msg), MPI_CHAR, 0, MU_TAG, MPI_COMM_WORLD);
  MPI_Send(
    _ready_mep_array,
    sizeof(size_t) * _bu_ready_message.untransmitted_meps_number,
    MPI_CHAR,
    0,
    MU_MEPS_TAG,
    MPI_COMM_WORLD);

  // printf("DONE SENDING EVENTS READY TO MU \n");
}

bool Output_unit::time_elapsed_check()
{
  _delay_timer++;
  if (_delay_timer >= PROBE_EVERY_IT) {
    _delay_timer = 0;
    _now = g_get_real_time();
    if (_now - _start_timer >= TIME_ELAPSED_FOR_GENERATION) {
      _start_timer += TIME_ELAPSED_FOR_GENERATION;
      return true;
    }
  }
  return false;
}

bool Output_unit::check_if_granted_transmission(int* readyRank)
{
  int dataReceived;

  MPI_Test(&_trans_granted_requests, &dataReceived, MPI_STATUS_IGNORE);

  if (dataReceived == true) *readyRank = _trans_granted_message.send_to_which_fu;

  return dataReceived;
}

void Output_unit::start_receive_from_mu()
{
  MPI_Irecv(
    &_trans_granted_message,
    sizeof(mpi_bu_transmission_grant_msg),
    MPI_CHAR,
    0,
    MU_TAG,
    MPI_COMM_WORLD,
    &(_trans_granted_requests));
}

void Output_unit::update_granted_transmissions()
{
  for (int i = 0; i != MAX_BU_CREDITS; i++) {
    if (this->_credit_free_array[i] == false) {
      if (_started_credited_transmissions[i] < _current_transmission_meta[i].full_transmissions_no) {
        int indx;
        int dataReceived;
        MPI_Testany(window_length, _dataTransmissionRequests[i], &indx, &dataReceived, MPI_STATUS_IGNORE);
        // int64_t now = g_get_real_time();
        // printf("[ %.1lf s ] OU RANK %d: CHECKING \n",(((double) now - (double) probe_global_start) / (double)
        // US_S),this->rank);

        if (dataReceived == true) {
          _current_probe_completed_bytes += entry_size_kb * 1024;

          char* addr = std::get<0>(_current_transmission);
          addr += _started_credited_transmissions[i] * entry_size_kb * 1024;

          // printf("OFFSET : %ld DATA SIZE %ld \n", addr-this->dataBufferPtr->data, entry_size_kb * 1024);
          // int64_t now = g_get_real_time();
          // printf("[ %.3lf s ] OU RANK %d: RESENDING TO %d SERVICED TRANSMISSIONS %d \n",(((double) now - (double)
          // probe_global_start) / (double) US_S),this->rank,creditsToRankArray[i],
          // startedCreditedTransmissions[i]);

          MPI_Wait(&(_dataTransmissionRequests[i][indx]), MPI_STATUS_IGNORE);
          MPI_Isend(
            addr,
            entry_size_kb * 1024,
            MPI_CHAR,
            _credits_to_rank_array[i],
            BU_RU_DATA_TAG,
            MPI_COMM_WORLD,
            &(_dataTransmissionRequests[i][indx]));

          _now = g_get_real_time();
          // info("[ %.3lf s ] OU RANK %d: AFTER RESENDING TO %d SERVICED TRANSMISSIONS %d \n",(((double) now -
          // (double) probe_global_start) / (double) US_S),this->rank,creditsToRankArray[i],
          // startedCreditedTransmissions[i]);

          _started_credited_transmissions[i]++;
        }
      } else {
        unsigned int to_send = std::min(window_length, _current_transmission_meta[i].full_transmissions_no);
        // info("OU RANK %d: TESTING WINDOW. TO SEND %d \n", rank, to_send);
        MPI_Waitall(window_length, _dataTransmissionRequests[i], MPI_STATUSES_IGNORE);
        _current_probe_completed_bytes += entry_size_kb * 1024 * to_send;

        if (_current_transmission_meta[i].remainder_size != 0) {
          ////info("OU SENDING REMAINDER size %ld  \n", currentTransmissionMetadata[i].remainderSize);
          size_t addr = _started_credited_transmissions[i] * entry_size_kb * 1024;
          // info("OFFSET : %ld DATA SIZE %ld \n", std::get<0>(currentTransmission) + addr -
          // this->dataBufferPtr->data, currentTransmissionMetadata[i].remainderSize);
          MPI_Send(
            std::get<0>(_current_transmission) + addr,
            _current_transmission_meta[i].remainder_size,
            MPI_CHAR,
            _credits_to_rank_array[i],
            BU_RU_DATA_REMAINDER_TAG,
            MPI_COMM_WORLD);

          _current_probe_completed_bytes += _current_transmission_meta[i].remainder_size;
        }

        // int64_t now = g_get_real_time();
        // info("[ %.1lf s ] OU RANK %d : REMAINDER size %d AND ALL %d FULL TRANSMISSIONS FINALIZED. REMAINING
        // TRANSMISISONS %ld \n",
        //(((double) now - (double) probe_global_start) / (double) US_S),
        // rank,
        // current_transmission_meta[i].remainder_size,
        // current_transmission_meta[i].full_transmissions_no,
        // ready_mep_vector.size());

        // info("OU SETTING TO TRUE \n");
        this->_credit_free_array[i] = true;

        // int64_t	probe_time = g_get_real_time();
        // info(" [ %.2lf s ] OU RANK %d GRANTS AVAILABLE: %lu OCCUPANCY %lu \n",
        // ((double)probe_time-(double)probe_global_start)/(double)US_S, rank, readyMepVector.size(),
        // FBUFF::getBufferOccupancy( *(this->dataBufferPtr)));

        this->_total_serviced_events++;
        // sync after transmitting all data from vector
        _ready_mep_vector.erase(_ready_mep_vector.begin());
        if (_ready_mep_vector.empty()) {
          // info("OU RANK %d VECTOR EMPTIED\n",rank);
          _recv_buff->read_complete();
        }
        // all serviced and eventual remainder
      }
    }
  }
}
int Output_unit::get_free_slot()
{

  for (auto i = 0; i != MAX_BU_CREDITS; i++) {
    if (_credit_free_array[i] == true) return i;
  }

  info("OU ERROR - SHOULD NOT BE HERE ! \n");
  return -1;
}

void Output_unit::mpi_warmup_run()
{
  /*
    int64_t t_init_start = g_get_real_time();
    // send
    // INITIALIZE OU->EM messages path (EVENTS READY)
    MPI_Isend(&(bu_ready_message), sizeof(mpi_bu_ready_msg), MPI_CHAR, 0, MU_TAG, MPI_COMM_WORLD, &(bu_ready_requests));
    MPI_Wait(&(bu_ready_requests), MPI_STATUS_IGNORE);

    MPI_Irecv(
      &trans_granted_message,
      sizeof(mpi_bu_transmission_grant_msg),
      MPI_CHAR,
      0,
      MU_TAG,
      MPI_COMM_WORLD,
      &(trans_granted_requests));
    MPI_Wait(&(bu_ready_requests), MPI_STATUS_IGNORE);
  */
  // TODO
  // TODO//std::vector<std::tuple<void*, size_t>> buff_out = buff_reader->get_full_buffer();
  // TODO//

  std::vector<std::tuple<void*, size_t>> buff_data = _recv_buff->get_full_buffer();
  long size = std::get<1>(buff_data[0]);
  const char* ptr = (const char*) std::get<0>(buff_data[0]);

  // size_t size = std::get<1>(buff_out[0]);
  // char* ptr = reinterpret_cast<char*>(std::get<0>(buff_out[0]));
  int total_iterations = (int) (size / (size_t) WARMUP_MPI_SIZE);
  size_t remainder_size = size % (size_t) WARMUP_MPI_SIZE;

  info("STARTUP SIZE: %ld AND PTR: %ld . TOTAL ITERATIONS %d\n", size, ptr, total_iterations);

  uint8_t* dummy_buffer = (uint8_t*) memalign(2UL * 1024UL * 1024UL, WARMUP_MPI_SIZE);
  mlock(dummy_buffer, WARMUP_MPI_SIZE);
  memset(dummy_buffer, 0xFF, WARMUP_MPI_SIZE);

  size_t index = 0;
  MPI_Request s_request;
  MPI_Request r_request;
  for (int it = 0; it != total_iterations; it++) {
    info("OU WARMUP PHASE 1 SEND: %d\n", it);
    MPI_Irecv(dummy_buffer, WARMUP_MPI_SIZE, MPI_CHAR, rank, MPI_ANY_TAG, MPI_COMM_WORLD, &r_request);
    MPI_Isend(&(ptr[index]), WARMUP_MPI_SIZE, MPI_CHAR, rank, rank, MPI_COMM_WORLD, &s_request);
    MPI_Wait(&s_request, MPI_STATUS_IGNORE);
    MPI_Wait(&r_request, MPI_STATUS_IGNORE);

    index += WARMUP_MPI_SIZE;
  }

  /// remainder
  MPI_Irecv(dummy_buffer, remainder_size, MPI_CHAR, rank, MPI_ANY_TAG, MPI_COMM_WORLD, &r_request);
  MPI_Isend(&(ptr[index]), remainder_size, MPI_CHAR, rank, rank, MPI_COMM_WORLD, &s_request);
  MPI_Wait(&s_request, MPI_STATUS_IGNORE);
  MPI_Wait(&r_request, MPI_STATUS_IGNORE);

  /// phase two
  index = 0;
  for (int it = 0; it != total_iterations - 1; it++) {
    info("OU WARMUP PHASE 2 SEND: %d\n", it);
    MPI_Irecv(dummy_buffer, WARMUP_MPI_SIZE, MPI_CHAR, rank, MPI_ANY_TAG, MPI_COMM_WORLD, &r_request);
    MPI_Isend(&(ptr[index + WARMUP_MPI_SIZE / 2]), WARMUP_MPI_SIZE, MPI_CHAR, rank, rank, MPI_COMM_WORLD, &s_request);
    MPI_Wait(&s_request, MPI_STATUS_IGNORE);
    MPI_Wait(&r_request, MPI_STATUS_IGNORE);

    index += WARMUP_MPI_SIZE;
  }

  free(dummy_buffer);
}

std::vector<mep_entry> Output_unit::get_meps_vector()
{
  return FBUFF::get_mep<EB::MEP>(_recv_buff); // return FBUFF::get_meps(*(this->buff_reader));
}

void Output_unit::exchange_message_size(int fu_rank, size_t size)
{
  _bu_fu_mep_size_msg.size = size;
  _bu_fu_mep_size_msg.sender_rank = this->rank;

  // info("OU RANK %d IN EXCHANGE: MEP SIZE: %ld AT ADDR  %ld\n", rank,size ,
  // std::get<0>(currentTransmission)-dataBufferPtr->data);

  MPI_Ssend(&(_bu_fu_mep_size_msg), sizeof(_bu_fu_mep_size_msg), MPI_CHAR, fu_rank, BU_RU_SIZE_TAG, MPI_COMM_WORLD);
  MPI_Recv(
    &(_bu_fu_mep_size_msg),
    sizeof(_bu_fu_mep_size_msg),
    MPI_CHAR,
    fu_rank,
    BU_RU_SIZE_TAG,
    MPI_COMM_WORLD,
    MPI_STATUS_IGNORE);

  // info("OU RANK %d SUCCESFULLY EXCHANGED MESSAGE SIZE: %ld \n", rank,size);
}

void Output_unit::get_meta(size_t size, int ind)
{
  _current_transmission_meta[ind].full_transmissions_no = size / entry_size_b;
  _current_transmission_meta[ind].remainder_size = size % entry_size_b;
}

void Output_unit::get_array_of_meps(std::vector<mep_entry> new_mep_vector)
{
  for (unsigned int a = 0; a != new_mep_vector.size(); a++) {
    _ready_mep_array[a] = std::get<1>(new_mep_vector[a]);
  }
}
