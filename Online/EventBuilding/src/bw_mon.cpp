#include "bw_mon.hpp"
#include <stdexcept>
#include <cstring>
#include "EventBuilding/tools.hpp"

EB::Bw_mon::Bw_mon(clockid_t clk_id) : _bytes_sent(0), _timer(clk_id) { _timer.start(); }

void EB::Bw_mon::reset()
{
  _bytes_sent = 0;
  _timer.reset();
}

double EB::Bw_mon::get_bw() const { return (double) (_bytes_sent * 8) / _timer.get_elapsed_time_ns(); }

double EB::Bw_mon::get_bw_and_reset()
{
  double ret_val = get_bw();
  reset();
  return ret_val;
}

size_t EB::Bw_mon::get_bytes() const { return _bytes_sent; }

size_t EB::Bw_mon::get_bytes_and_reset()
{
  size_t ret_val = get_bytes();
  reset();
  return ret_val;
}

void EB::Bw_mon::add_sent_bytes(size_t n_bytes) { _bytes_sent += n_bytes; }

void EB::Bw_mon::set_clock_id(clockid_t clk_id)
{
  _timer.set_clk_id(clk_id);
  reset();
}

clockid_t EB::Bw_mon::get_clock_id() const { return _timer.get_clk_id(); }

double EB::Bw_mon::get_elapsed_time() { return _timer.get_elapsed_time_s(); }
