#include "dummy_mep_buffer_writer.hpp"
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <cstdlib>
#include <cerrno>
#include <cstring>
#include "EventBuilding/tools.hpp"
#include <numa.h>

EB::Dummy_mep_buffer_writer::Dummy_mep_buffer_writer() {}

EB::Dummy_mep_buffer_writer::Dummy_mep_buffer_writer(size_t buffer_size, int numa_node) : Dummy_mep_buffer_writer()
{
  if (reset_buffer(buffer_size, numa_node) != 0) {
    std::stringstream err_mess;
    err_mess << "Unable to allocate buffer " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }
}

EB::Dummy_mep_buffer_writer::~Dummy_mep_buffer_writer() {}

int EB::Dummy_mep_buffer_writer::reset_buffer(size_t buffer_size, int numa_node)
{
  int ret_val = 0;
  void* ptr;

  ret_val = posix_memalign(&ptr, sysconf(_SC_PAGE_SIZE), buffer_size);
  if (ret_val == 0) {
    if (numa_node != -1) {
      numa_set_strict(1);
      numa_tonode_memory(ptr, buffer_size, numa_node);
    }
    _buffer = buffer_type(ptr, free);
    _buffer_size = buffer_size;
    _status = EMPTY;
  }

  return ret_val;
}

EB::MEP* EB::Dummy_mep_buffer_writer::try_write_next_element(size_t size)
{
  EB::MEP* ret_val = NULL;
  if (_buffer) {
    if ((size <= _buffer_size) && (_status == EMPTY)) {
      ret_val = reinterpret_cast<EB::MEP*>(_buffer.get());
      _status = FULL;
    } else if (_status == FULL) {
      std::stringstream err_mess;
      err_mess << __FILE__ << ": " << __FUNCTION__ << "Requesting write to a used buffer, call write_complete() first";
      throw std::logic_error(err_mess.str());
    } else {
      std::stringstream err_mess;
      err_mess << __FILE__ << ": " << __FUNCTION__ << "Buffer overflow! Requested size " << size << " buffer size "
               << _buffer_size;
      throw std::runtime_error(err_mess.str());
    }
  } else {
    std::stringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " non initialized buffer";
    throw std::logic_error(err_mess.str());
  }

  return ret_val;
}

std::vector<std::tuple<void*, size_t>> EB::Dummy_mep_buffer_writer::get_full_buffer()
{
  std::vector<std::tuple<void*, size_t>> ret_val;

  ret_val.push_back(std::make_tuple(_buffer.get(), _buffer_size));

  return ret_val;
}

void EB::Dummy_mep_buffer_writer::write_complete()
{
  if (!_buffer) {
    std::stringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " non initialized buffer";
    throw std::logic_error(err_mess.str());
  }

  _status = EMPTY;
}

void EB::Dummy_mep_buffer_writer::write_discard()
{
  if (!_buffer) {
    std::stringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " non initialized buffer";
    throw std::logic_error(err_mess.str());
  }

  _status = EMPTY;
}