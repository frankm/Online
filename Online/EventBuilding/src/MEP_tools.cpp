#include "EventBuilding/MEP_tools.hpp"
#include "EventBuilding/MFP_tools.hpp"
#include "EventBuilding/tools.hpp"
#include <sstream>
#include <stdexcept>

size_t EB::MEP_header::size() const { return mep_header_size(n_MFPs); }

size_t EB::MEP_header::mem_size() const { return mep_header_mem_size(n_MFPs); }

EB::src_id_type* EB::MEP_header::src_ids()
{
  return reinterpret_cast<src_id_type*>(reinterpret_cast<uintptr_t>(this) + sizeof(MEP_header));
}

const EB::src_id_type* EB::MEP_header::src_ids() const { return const_cast<MEP_header*>(this)->src_ids(); }

EB::offset_type* EB::MEP_header::offsets()
{
  return reinterpret_cast<offset_type*>(
    reinterpret_cast<uintptr_t>(src_ids()) + aligned_array_size<src_id_type>(n_MFPs, MEP_alignment));
}

const EB::offset_type* EB::MEP_header::offsets() const { return const_cast<MEP_header*>(this)->offsets(); }

void* EB::MEP::payload() { return reinterpret_cast<void*>(at(0)); }

const void* EB::MEP::payload() const { return const_cast<MEP*>(this)->payload(); }

EB::MFP* EB::MEP::at(int n)
{
  if (n >= header.n_MFPs) {
    std::stringstream error_mess;
    error_mess << __FUNCTION__ << ": requested MFP of idx " << n << " in a MEP with " << header.n_MFPs << " MFPs";
    throw std::runtime_error(error_mess.str());
  }
  return reinterpret_cast<EB::MFP*>(reinterpret_cast<uintptr_t>(this) + header.offsets()[n] * MEP_WORD_SIZE);
}

const EB::MFP* EB::MEP::at(int n) const { return const_cast<MEP*>(this)->at(n); }

EB::MFP* EB::MEP::operator[](int n) { return at(n); }
const EB::MFP* EB::MEP::operator[](int n) const { return at(n); }

bool EB::MEP::is_magic_valid() const { return header.magic == MEP_magic; }
void EB::MEP::set_magic_valid() { header.magic = MEP_magic; }

bool EB::MEP::is_valid() const
{
  bool ret_val = is_magic_valid();
  if (ret_val) {
    uint64_t ev_id = at(0)->header.ev_id;
    // We exit the loop as soon as there is one invalid ev_id
    for (auto it = this->cbegin(); (it != this->cend()) && (ret_val); it++) {
      // for (int k = 1; (k < header.n_MFPs) && (ret_val); k++) {
      ret_val &= (ev_id == it->header.ev_id);
    }
  }

  return ret_val;
}

bool EB::MEP::is_wrap() const { return header.magic == MEP_wrap; }
void EB::MEP::set_wrap() { header.magic = MEP_wrap; }

size_t EB::MEP::bytes() const
{
  return header.p_words * MEP_WORD_SIZE; // p_words is the size in 32 bits words
}

size_t EB::MEP::header_size() const { return header.size(); }

size_t EB::MEP::header_mem_size() const { return header.mem_size(); }

std::string EB::MEP_header::print() const
{
  std::stringstream os;
  os << "\n------MEP HEADER------\n";
  os << "Magic " << std::hex << magic << std::dec << "\n";
  os << "n MFPs " << n_MFPs << "\n";
  os << "p words " << p_words << "\n";
  auto src = src_ids();
  auto off = offsets();
  for (int k = 0; k < n_MFPs; k++) {
    os << "MFP " << k << ": source ID " << src[k] << " offset " << off[k] << "\n";
  }

  return os.str();
}

std::ostream& EB::operator<<(std::ostream& os, const MEP_header& header) { return os << header.print(); }

std::ostream& EB::operator<<(std::ostream& os, const MEP& mep) { return os << mep.print(); }

std::string EB::MEP::print(bool MFP_header_only) const
{
  std::stringstream os;

  os << header;

  if (!MFP_header_only) {
    // for (int k = 0; k < header.n_MFPs; k++) {
    // os << *(this->at(k));
    for (auto it = this->cbegin(); it != this->cend(); it++) {
      // TODO implement print for full MFPs
      os << *it;
    }
  }

  return os.str();
}

size_t EB::mep_header_size(int n_MFPs)
{
  return sizeof(MEP_header) + aligned_array_size<src_id_type>(n_MFPs, MEP_alignment) + n_MFPs * sizeof(offset_type);
}

size_t EB::mep_header_mem_size(int n_MFPs)
{
  size_t base_size = mep_header_size(n_MFPs);
  return base_size + get_padding(base_size, 4096);
}

// ITERATOR
EB::MEP::iterator& EB::MEP::iterator::operator++()
{
  _offset++;
  return *this;
}

EB::MEP::iterator EB::MEP::iterator::operator++(int)
{
  iterator tmp = *this;
  ++(*this);
  return tmp;
}

EB::MEP::iterator::reference EB::MEP::iterator::operator*() const { return *_get_ptr(); }
EB::MEP::iterator::pointer EB::MEP::iterator::operator->() { return _get_ptr(); }

EB::MEP::iterator::pointer EB::MEP::iterator::_get_ptr() const
{
  return reinterpret_cast<pointer>(_base_ptr + (*_offset) * MEP_WORD_SIZE);
}

bool EB::operator==(const EB::MEP::iterator& a, const EB::MEP::iterator& b) { return a._offset == b._offset; }
bool EB::operator!=(const EB::MEP::iterator& a, const EB::MEP::iterator& b) { return !(a == b); }

// CONST ITERATOR
EB::MEP::const_iterator& EB::MEP::const_iterator::operator++()
{
  _offset++;
  return *this;
}

EB::MEP::const_iterator EB::MEP::const_iterator::operator++(int)
{
  const_iterator tmp = *this;
  ++(*this);
  return tmp;
}

EB::MEP::const_iterator::reference EB::MEP::const_iterator::operator*() const { return *_get_ptr(); }
EB::MEP::const_iterator::pointer EB::MEP::const_iterator::operator->() { return _get_ptr(); }

EB::MEP::const_iterator::pointer EB::MEP::const_iterator::_get_ptr() const
{
  return reinterpret_cast<pointer>(_base_ptr + (*_offset) * MEP_WORD_SIZE);
}

bool EB::operator==(const EB::MEP::const_iterator& a, const EB::MEP::const_iterator& b)
{
  return a._offset == b._offset;
}

bool EB::operator!=(const EB::MEP::const_iterator& a, const EB::MEP::const_iterator& b) { return !(a == b); }