import argparse
import os
import re
import time
import pydim

def monitor_counters(bu_metrics, ru_metrics, hosts, partition, time_interval):
    services = []
    for host in hosts:
        host_metrics = []
        for metric in ru_metrics:
            for idx in range(2):
                host_metrics.append(f'{host.upper()}_RU_{idx}/RU/{metric}')
        for metric in bu_metrics:
            for idx in range(2):
                host_metrics.append(f'{host.upper()}_BU_{idx}/BU/{metric}')
        services.append(host_metrics)

    print('host\t' + '\t'.join([service[(service.find('/'))+1:] for service in services[0]]))


    while True:
        for host, host_services in zip(hosts,services):
            results = [monitor_counter(service, partition) for service in host_services]
            print(f'{host}\t\t' + '\t\t'.join([str(result[0]) if len(result)>=1 else '-' for result in results]))
        

        time.sleep(time_interval)
        print('host\t' + '\t'.join([service[(service.find('/'))+1:] for service in services[0]]))


def monitor_counter(service, partition):
    service_name = f'{partition}_{service}'
    return pydim.dic_sync_info_service(service_name)


def main():
    parser = argparse.ArgumentParser(
        description='EB counters reader script',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    host_group = parser.add_mutually_exclusive_group(required=True)

    host_group.add_argument(
        "-H",
        "--hosts",
        help=
        'Comma separated list of hosts to monitor.',
        type=str)

    host_group.add_argument(
        "-f",
        "--host_file",
        help=
        'Host file, one hostname per row. # comment',
        type=str)

    parser.add_argument("-D",
                        "--dim_dns",
                        help='Override the DIM_DNS_NODE env variable value.',
                        default=None,
                        type=str)

    parser.add_argument(
        "-p",
        "--partition",
        help=
        'Partition to monitor.',
        type=str,
        default='LHCb',
        required=False)

    parser.add_argument(
        "-b",
        "--bu_counters",
        help=
        'Comma separated list of BU counters to monitor.',
        type=str,
        default='snd_bytes,rcv_bytes,event_counter,discarted_MEP_counter',
        required=False)

    parser.add_argument(
        "-r",
        "--ru_counters",
        help=
        'Comma separated list of BU counters to monitor.',
        type=str,
        default='Events/OUT',
        required=False)
    
    parser.add_argument(
        "-t",
        "--time",
        help= 'Time interval in seconds between prints.',
        type=float,
        required=False,
		default=1)

    args = parser.parse_args()

    env = os.environ.copy()

    if (args.dim_dns):
        env['DIM_DNS_NODE'] = args.dim_dns
        pydim.dic_set_dns_node(args.dim_dns)

    if (args.hosts):
        host_list = args.hosts.split(',')
    else :
        with open(args.host_file, 'r') as host_file:
            no_comment_match = re.compile('([^#]*)(?:#)?.*')
            host_list = [match.group(1) for match in 
             [no_comment_match.match(line.strip()) for line in host_file.readlines()]
              if match.group(1) != '']

    host_list = list(set(host_list))

    bu_metrics = list(set(args.bu_counters.split(',')))
    ru_metrics = list(set(args.ru_counters.split(',')))

    bu_metrics.sort()
    ru_metrics.sort()
    host_list.sort()

    services = []
    for metric in ru_metrics:
        for host in host_list:
            for idx in range(2):
                services.append(f'{host.upper()}_RU_{idx}/RU/{metric}')

    for metric in bu_metrics:
        for host in host_list:
            for idx in range(2):
                services.append(f'{host.upper()}_BU_{idx}/BU/{metric}')

    monitor_counters(bu_metrics, ru_metrics, host_list, args.partition, args.time)



    
if __name__ == "__main__":
    main()
