import run_eb_scan
import argparse
import os
import re
import pydim
import csv

def main():
    parser = argparse.ArgumentParser(
        description='EB counters reader script',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    host_group = parser.add_mutually_exclusive_group(required=True)

    host_group.add_argument(
        "-H",
        "--hosts",
        help=
        'Comma separated list of hosts to monitor.',
        type=str)

    host_group.add_argument(
        "-f",
        "--host_file",
        help=
        'Host file, one hostname per row. # comment',
        type=str)

    parser.add_argument(
        "-o",
        "--out",
        help=
        'Output file.',
        default='eb_counters.csv',
        type=str)
    
    parser.add_argument("-D",
                        "--dim_dns",
                        help='Override the DIM_DNS_NODE env variable value.',
                        default=None,
                        type=str)

    parser.add_argument(
        "-b",
        "--bu_counters",
        help=
        'Comma separated list of BU counters to monitor.',
        type=str,
        default='snd_bytes,rcv_bytes,event_counter,discarted_MEP_counter',
        required=False)

    parser.add_argument(
        "-r",
        "--ru_counters",
        help=
        'Comma separated list of BU counters to monitor.',
        type=str,
        default='Events/OUT',
        required=False)
    
    parser.add_argument(
        "-t",
        "--time",
        help= 'Time interval in seconds.',
        type=float,
        required=True)

    args = parser.parse_args()

    env = os.environ.copy()

    if (args.dim_dns):
        env['DIM_DNS_NODE'] = args.dim_dns
        pydim.dic_set_dns_node(args.dim_dns)

    if (args.hosts):
        host_list = args.hosts.split(',')
    else :
        with open(args.host_file, 'r') as host_file:
            no_comment_match = re.compile('([^#]*)(?:#)?.*')
            host_list = [match.group(1) for match in 
             [no_comment_match.match(line.strip()) for line in host_file.readlines()]
              if match.group(1) != '']

    bu_metrics = list(set(args.bu_counters.split(',')))
    ru_metrics = list(set(args.ru_counters.split(',')))

    services = []
    for metric in ru_metrics:
        for host in host_list:
            for idx in range(2):
                services.append(f'{host.upper()}_RU_{idx}/R_RU/{metric}')

    for metric in bu_metrics:
        for host in host_list:
            for idx in range(2):
                services.append(f'{host.upper()}_BU_{idx}/R_BU/{metric}')

    res = run_eb_scan.check_data_df(services, args.time,'TDET')

    # transpose_res = [dict()]*len(list(res.values())[0])
    transpose_res = [dict() for k in range(len(list(res.values())[0]))]
    for key, values in res.items():
        for k, value in enumerate(values):
            transpose_res[k][key] = value

    with open(args.out, 'w') as out_file:
        writer = csv.DictWriter(out_file, fieldnames=res.keys())
        writer.writeheader()
        for data in transpose_res:
            writer.writerow(data)


    
if __name__ == "__main__":
    main()