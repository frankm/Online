//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "GaudiKernel/Service.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IUpdateable.h"
#include "Gaucho/IGauchoMonitorSvc.h"
#include "GaudiKernel/IHistogramSvc.h"
#include "dim/dis.hxx"
#include "RTL/rtl.h"

#include "TH1D.h"
#include "TProfile.h"
#include "AIDA/IProfile1D.h"
#include "AIDA/IHistogram1D.h"

#include <typeinfo>
#include <string>
#include <map>
#include <set>
#include <thread>
#include <atomic>

using namespace std;
using namespace Gaudi::Accumulators;
class MonSvcTest;
class MyCommand;

void runng(MonSvcTest *tis);
class MonSvcTest : virtual public Service, virtual public IIncidentListener   {
  public:
    int m_count=0;
    int m_count1=0;
    double m_double=0;
    double m_darray[100];
    int m_iarray[100];
    unsigned int m_uiarray[100];
    unsigned long m_ularray[100];
    IGauchoMonitorSvc *m_mons;
    IHistogramSvc *m_hsvc;
    AIDA::IHistogram1D *m_h1;
    AIDA::IHistogram1D *m_h2;
    bool m_stop;
    float m_float;
    thread *m_thread=0;
    int m_One;
    string m_SaveDir;
    bool m_SaveHists;
    int m_savePeriode;
    int m_count2=0;
    atomic<int> m_aint1;
    atomic<int> m_aint2;
    atomic<long> m_along1;
    atomic<long> m_along2;
    Gaudi::Accumulators::AveragingCounter<long> *m_GaudiCounter;
    MyCommand *m_chRun=0;
    bool m_changeRun = false;
    Histogram<2u, atomicity::full,double> *m_G2DH1;
    ProfileHistogram<1u,atomicity::full,double> *m_G1DP1;
    AIDA::IProfile2D *m_prof2;
    AIDA::IProfile1D *m_prof1;

    MonSvcTest(const string& name, ISvcLocator* sl): Service(name, sl)
    {
      m_count=0;
      m_double=0.0;
      m_mons=0;
      m_h1=m_h2=0;m_hsvc=0;
      m_stop = false;
      m_float=0.0;
      for (int i=0;i<100;i++)      {
        m_darray[i]= 0.0;
        m_iarray[i]= 0;
        m_uiarray[i] = 0;
        m_ularray[i] = 0;
      }
      m_One = 1;
      declareProperty("SaveDir",m_SaveDir="/home/beat/hists");
      declareProperty("Save",m_SaveHists=false);
      declareProperty("SaveIntval",m_savePeriode=60);
    }
    StatusCode initialize() override;
    StatusCode start() override    {

      m_stop =false;
      m_thread = new thread(runng,this);
      if (m_SaveHists)      {
        string part="Beat";
        string proc = RTL::processName();
	shared_ptr<DimService> svc;
        m_mons->StartSaving(m_SaveDir,part,proc,m_savePeriode,svc);
      }
      m_thread->detach();
      StatusCode status=Service::start();
      return status;
    }
    StatusCode stop() override    {
      StatusCode status =Service::stop();
      m_stop =true;
      return status;
    }
    StatusCode finalize() override    {
      StatusCode status = Service::finalize();
      return status;
    }
    StatusCode running ()    {
      int runno = 1234;
      m_mons->setRunNo(runno);
      int j=0;
      while(1)
      {
        m_double++;
        m_count++;
        for (int i=0;i<100;i++)
        {
          m_darray[i]= m_darray[i]+i;
          m_iarray[i]+= i;
        }
        m_aint1++;
        m_along1++;
        if (m_GaudiCounter != 0)*m_GaudiCounter += 1;
        (*m_G1DP1)[0.5] += 0.5;
        m_prof1->fill(-0.5,-0.5);
        for (int i = 0;i<10;i++)
        {
          (*m_G1DP1)[10.0*double(i)+0.5] += double(i);
          m_prof1->fill(10.0*double(i)+0.5,double(i),1.0);
        }
        (*m_G1DP1)[120.0] += 120.0;
        m_prof1->fill(120.0,120.0);
        nlohmann::json json = m_G1DP1->toJSON();
        usleep(500000);
        if (m_stop) return StatusCode::SUCCESS;
        j++;
        if (m_changeRun)
        {
          m_changeRun = false;
          m_mons->update(runno).ignore();
          m_mons->resetHistos(this);
          runno++;
          m_mons->setRunNo(runno);
        }
      }
      return StatusCode::SUCCESS;
    }
    void handle(const Incident& inc) override    {
      MsgStream log(msgSvc(),name());
      log << MSG::INFO << "MonSvcTest: Got incident from:" << inc.source() << ": " << inc.type() << endmsg;
      if ( inc.type() == "APP_RUNNING" ) {
      }
      else if ( inc.type() == "APP_STOPPED" )  {
      }
    }
};

class MyCommand : public DimCommand
{
  public:
    MonSvcTest *m_Parent;
    MyCommand (const char *name,MonSvcTest *p) : DimCommand(name,"C")
    {
      m_Parent = p;
    }
    void commandHandler() override
    {
      m_Parent->m_changeRun = true;
    }
};

StatusCode MonSvcTest::initialize()
{
  m_chRun = new MyCommand("ChangeRun", this);
  IIncidentSvc *m_incidentSvc { nullptr };
  StatusCode sc = Service::initialize();
  sc = serviceLocator()->service("MonitorSvc", m_mons, false);
  sc = serviceLocator()->service("HistogramDataSvc", m_hsvc, true);
  sc = serviceLocator()->service("IncidentSvc",m_incidentSvc,true);
  if( !sc.isSuccess() ) {
    return sc;
  }
  m_count1 = -5;
  m_count2 = 6;
  m_incidentSvc->addListener(this,"APP_RUNNING");
  m_incidentSvc->addListener(this,"APP_STOPPED");
  m_h1 = m_hsvc->book("Hist1","a Histogram1",100,0.0,100.0);
  m_h2 = m_hsvc->book("Hist2","a Histogram2",100,0.0,100.0);
  m_prof1 = m_hsvc->bookProf("Prof1","a 1D Profile",10,0.0,100.0);
  m_prof2 = m_hsvc->bookProf("Prof2","a 2D Profile",10,0.0,100.0,10,0.0,100.0);
  m_mons->declareInfo(string("NumTask"), m_One,string("Number of Tasks"),this);
  m_mons->declareInfo(string("Integer1"), m_count,string("An Integer"),this);
  m_mons->declareInfo(string("Integer2"), m_count,string("An Integer"),this);
  m_mons->declareInfo(string("Integer3"), m_count,string("An Integer"),this);
  m_mons->declareInfo(string("Integer4"), m_count,string("An Integer"),this);
  m_mons->declareInfo("Float1", m_float,"A Float", this);
  m_mons->declareInfo("Double1", m_double,"A Double", this);
  m_mons->declareInfo("Double2", m_double,"A Double", this);
  m_mons->declareInfo("Double3", m_double,"A Double", this);
  m_mons->declareInfo("Double4", m_double,"A Double", this);
  m_mons->declareInfo("DArray", "d",m_darray,sizeof(m_darray),"A Double Array", this);
  m_mons->declareInfo("IArray", "i",m_iarray,sizeof(m_iarray),"An Integer Array ", this);
  m_mons->declareInfo("UIArray", "i",m_uiarray,sizeof(m_uiarray),"An Unsigned Integer Array ", this);
  m_mons->declareInfo("ULArray", "i",m_ularray,sizeof(m_ularray),"An Unsigned Long Array ", this);
  m_mons->declareInfo( string("Hist1"),(const AIDA::IBaseHistogram*)m_h1, string("A Histogram1"),this);
  m_mons->declareInfo( string("Hist2"),(const AIDA::IBaseHistogram*)m_h2, string("A Histogram1"),this);
  m_mons->declareInfo("Pair1",m_count1,m_count2,"A Pair1",this);
  m_mons->declareInfo("AtomicInt1",m_aint1,"A atomic int 1",this);
  m_mons->declareInfo("AtomicInt2",m_aint2,"A atomic int 2",this);
  m_mons->declareInfo("AtomicLong1",m_along1,"A atomic long 1",this);
  m_mons->declareInfo("AtomicLong",m_along2,"A atomic long 2",this);
  m_GaudiCounter = new Gaudi::Accumulators::AveragingCounter<long>(this,"GaudiAcc");
//  m_mons->declareInfo("GaudiAcc",*m_GaudiCounter,"A Gaudi Counter",this);
  unsigned int nBins1, nBins2;
  double min1, min2, max1,max2, val1,val2;
  nBins1 = 10;
  nBins2 = 10;
  min1 = min2 =0.0;
  max1 = 100.0;
  max2 = 50.0;
  val1 = 20.0;
  val2 = 30.0;
  m_G2DH1 = new Histogram<2, atomicity::full,double> {this, "GaudiH2D", "A Gaudi 2D histogram",{{nBins1, min1, max1}, {nBins2, min2, max2}}};
  m_G1DP1 = new ProfileHistogram<1u, atomicity::full,double> {this, "GaudiP1D", "A Gaudi 1D Profile",{nBins1, min1, max1}};
  ++(*m_G2DH1)[{val1, val2}];    // prefered syntax
//  m_mons->declareInfo("Gaudi2DHistogram",m_G2DH1,"A Gaudi Histogram",this);
//  m_mons->declareInfo("Gaudi1DProfile",m_G1DP1,"A Gaudi 1D Profile",this);
  m_mons->declareInfo("Prof1",(const AIDA::IBaseHistogram*)m_prof1,"A 1D Profile",this);
  m_mons->declareInfo("Prof2",(const AIDA::IBaseHistogram*)m_prof2,"A 2D Profile",this);
  return StatusCode::SUCCESS;
}

void runng(MonSvcTest *tis)
{
//  tis->m_mons->setRunNo(1234);
  StatusCode status = tis->running();
  if ( !status.isSuccess() ) {}
}

DECLARE_COMPONENT(MonSvcTest)
