//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
//      UpdateAndReset.cpp
//      --------------------------------------------------------------------
//
//      Package   : Gaucho
//
//      Author    : Eric van Herwijnen
//      Should be run as first algorithm in TopAlg
//      Checks:
//      1) If the run number has changed. If so, updateAll(true) and resetHistos
//      2) If timerelapsed flag is true. If so, updateAll(false)
//
//==========================================================================

#include "UpdateAndReset.h"

// Include files
#include <Gaudi/MonitoringHub.h>
#include <GaudiKernel/IRegistry.h>
#include <GaudiKernel/HistogramBase.h>
#include <GaudiKernel/IDataManagerSvc.h>

#include <PCIE40Data/sodin.h>
#include <Tell1Data/RunInfo.h>
#include <Tell1Data/Tell1Bank.h>
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include <dim/dis.hxx>

#include <AIDA/IHistogram.h>
#include <AIDA/IProfile1D.h>

#include <TFile.h>
#include <TROOT.h>
#include <TDirectory.h>

#include <TH1.h>
#include <TProfile.h>
#include <TSystem.h>

#include <sstream>

using namespace Online;

// Static Factory declaration
DECLARE_COMPONENT( UpdateAndReset )

typedef std::vector<std::pair<const Tell1Bank*, const void*> > evt_data_t;

namespace {
  static const std::string s_statusNoUpdated( "NO_UPDATED" );
  static const std::string s_statusProcessingUpdate( "PROCESSINGUPDATE" );
  static const std::string s_statusUpdated( "UPDATED" );

  template <typename Out, typename In>
  Out* a2r_cast( In* aida ) {
    using Base = std::conditional_t<std::is_const_v<Out>, const Gaudi::HistogramBase, Gaudi::HistogramBase>;
    auto base  = dynamic_cast<Base*>( aida );
    return base ? dynamic_cast<Out*>( base->representation() ) : nullptr;
  }

  template <typename T>
  void save_histogram( T* profile, bool save, bool reset ) {
    if ( save ) {
      // TObject* hRoot         = Gaudi::Utils::Aida2ROOT::aida2root( profile );
      TObject* hRoot         = a2r_cast<TObject,T>( profile );
      auto     HistoFullName = RTL::str_split( hRoot->GetName(), "/" );
      gDirectory->Cd( "/" );
      for ( unsigned int i = 0; i < HistoFullName.size() - 1; i++ ) {
        TKey* k = gDirectory->GetKey( HistoFullName.at( i ).c_str() );
        if ( k == 0 ) { gDirectory->mkdir( HistoFullName.at( i ).c_str() ); }
        gDirectory->Cd( HistoFullName.at( i ).c_str() );
      }
      hRoot->Write( HistoFullName.at( HistoFullName.size() - 1 ).c_str() );
      // should we reset at the root level?
      // if (reset) hRoot->Reset();
    }
    if ( reset ) { profile->reset(); }
  }
} // namespace

// Constructor
//------------------------------------------------------------------------------
UpdateAndReset::UpdateAndReset( const std::string& name, ISvcLocator* ploc ) : Gaudi::Algorithm( name, ploc ) {
  declareProperty( "desiredDeltaTCycle", m_desiredDeltaTCycle = 20 );
  declareProperty( "disableReadOdin", m_disableReadOdin = 0 );

  declareProperty( "saveHistograms", m_saveHistograms = 0 );
  declareProperty( "saveSetDir", m_saveSetDir = "/hist/Savesets" );
  declareProperty( "saverCycle", m_saverCycle = 900 );
  declareProperty( "resetHistosAfterSave", m_resetHistosAfterSave = 0 );
  declareProperty( "publishSaveSetLocation", m_publishSavesetLoc = true );
  declareProperty( "MyName", m_MyName = "" ); // partition_TaskName
  declareProperty( "resetOnStart", m_resetOnStart = false );

  m_runNumber        = 0;
  m_dimSvcSaveSetLoc = 0;
  m_eorNumber        = 0;
  m_one              = 1;
}

//------------------------------------------------------------------------------
StatusCode UpdateAndReset::initialize() {
  m_rootSinkSvc = service( "Gaudi::Histograming::Sink::Root" );
  if ( !m_rootSinkSvc ) {
    error() << "Could not get RootHistogramSink service" << endmsg;
    return StatusCode::FAILURE;
  }
  m_rootSinkSvc.as<IProperty>()->setProperty( "FileName", "/dev/null" ).ignore();
  return StatusCode::SUCCESS;
}

StatusCode UpdateAndReset::start() {
  MsgStream   msg( msgSvc(), name() );
  std::string utgid = RTL::processName();

  m_one              = 1;
  m_gauchoMonitorSvc = monitorSvc();
  if ( !m_gauchoMonitorSvc ) {
    fatal() << "Unable to locate the IGauchoMonitorSvc interface." << endmsg;
    return StatusCode::FAILURE;
  }

  m_taskName = "unknownTask";
  m_partName = "unknownPartition";

  std::vector<std::string> serviceParts;
  if ( m_MyName.empty() ) {
    serviceParts = RTL::str_split( utgid, "_" );
    if ( 3 == serviceParts.size() ) {
      m_partName = serviceParts.at( 0 );
      m_taskName = serviceParts.at( 1 );
    } else if ( 4 == serviceParts.size() ) {
      m_partName = serviceParts.at( 0 );
      m_taskName = serviceParts.at( 2 );
    }
  } else {
    serviceParts = RTL::str_split( utgid, "_" );
    m_partName   = serviceParts.at( 0 );
    m_taskName   = m_MyName;
  }

  this->m_infoFileStatus = "SAVESETLOCATION/......................................................";
  if ( this->m_saveHistograms ) {
    if ( ( this->m_dimSvcSaveSetLoc == 0 ) && this->m_publishSavesetLoc ) {
      std::string infoName = this->m_partName + "/" + this->m_taskName + "/SAVESETLOCATION";
      this->m_dimSvcSaveSetLoc =
          std::make_shared<DimService>( infoName.c_str(), (char*)this->m_infoFileStatus.c_str() );
    }
  }

  this->declareInfo( "Tasks", m_one, "NumberOfTasks" );
  if ( 1 == m_saveHistograms ) {
    div_t divresult = div( this->m_saverCycle, this->m_desiredDeltaTCycle );
    if ( 0 != divresult.rem ) { this->m_saverCycle = divresult.quot * this->m_desiredDeltaTCycle; }
  }

  // The below part is for test
  this->m_runNumber   = 0;
  this->m_runStatus   = s_statusNoUpdated;
  this->m_cycleStatus = s_statusNoUpdated;

  if ( this->m_saveHistograms != 0 ) {
    m_gauchoMonitorSvc->StartSaving( this->m_saveSetDir, this->m_partName, this->m_taskName, this->m_saverCycle,
                                     this->m_dimSvcSaveSetLoc );
  }
  if ( m_resetOnStart ) { m_gauchoMonitorSvc->resetHistos( this ); }
  this->declareInfo( "CurrentRun", m_runNumber, "Current run number" );

  return StatusCode::SUCCESS;
}

//------------------------------------------------------------------------------
StatusCode UpdateAndReset::execute( EventContext const& /* ctxt */ ) const {
  UpdateAndReset* This = const_cast<UpdateAndReset*>( this );
  /// Otherwise normal execution: retrieve the run number from ODIN
  const evt_data_t* event = (evt_data_t*)m_rawData.get();
  int               runno = 100;
  if ( event ) {
    for ( const auto& b : *event ) {
      const auto* hdr = b.first;
      if ( hdr->type() == Tell1Bank::ODIN ) {
        if ( hdr->version() < 7 ) {
          const RunInfo* odin = (const RunInfo*)b.second;
          runno               = odin->run_number();
        } else {
          const pcie40::sodin_t* odin = (const pcie40::sodin_t*)b.second;
          runno                       = odin->run_number();
        }
        return This->do_execute( runno );
      }
    }
    if ( m_disableReadOdin ) { return This->do_execute( m_runNumber ); }
    debug() << "ODIN bank not found at location " << m_rawData << endmsg;
    return StatusCode::FAILURE;
  }
  MsgStream msg( msgSvc(), name() );
  msg << MSG::DEBUG << "rawEvent not found at location " << m_rawData << endmsg;
  return StatusCode::FAILURE;
}

StatusCode UpdateAndReset::do_execute( int runno ) {
  if ( m_runNumber == 0 ) {
    std::lock_guard<std::mutex> lock( m_mutex );
    if ( m_runNumber == 0 ) {
      m_runNumber = runno;
      m_gauchoMonitorSvc->setRunNo( runno );
    }
  }
  if ( runno > m_runNumber ) {
    std::lock_guard<std::mutex> lock( m_mutex );
    if ( runno > m_runNumber ) {
      m_gauchoMonitorSvc->update( m_runNumber ).ignore();
      if ( m_saveHistograms > 0 ) {
        m_eorNumber = m_runNumber;
        manageTESHistos( false, true, true, true );
      }
      m_gauchoMonitorSvc->resetHistos( this );
      m_gauchoMonitorSvc->setRunNo( runno );
      m_runNumber = runno;
    }
  }
  m_one = 1;
  return StatusCode::SUCCESS;
}

//------------------------------------------------------------------------------
StatusCode UpdateAndReset::stop() {
  if ( m_saveHistograms > 0 ) {
    m_eorNumber = m_runNumber;
    manageTESHistos( false, true, true, true );
    if ( m_gauchoMonitorSvc ) { m_gauchoMonitorSvc->StopSaving(); }
  } else if ( m_gauchoMonitorSvc ) {
    m_gauchoMonitorSvc->update( m_runNumber ).ignore();
    if ( !m_resetOnStart ) { m_gauchoMonitorSvc->resetHistos( 0 ); }
    m_gauchoMonitorSvc.reset();
  }
  m_one = 0;
  return StatusCode::SUCCESS;
}

//------------------------------------------------------------------------------
StatusCode UpdateAndReset::restart() { return StatusCode::SUCCESS; }

//------------------------------------------------------------------------------
StatusCode UpdateAndReset::finalize() {
  if ( m_dimSvcSaveSetLoc ) { m_dimSvcSaveSetLoc.reset(); }
  return StatusCode::SUCCESS;
}

void UpdateAndReset::manageTESHistos( bool list, bool reset, bool save, bool isFromEndOfRun ) {
  MsgStream                msg( msgSvc(), name() );
  char                     timestr[64], year[5], month[3], day[3];
  IRegistry*               object   = rootObject();
  time_t                   rawTime  = ::time( NULL );
  struct tm*               timeInfo = ::localtime( &rawTime );
  std::vector<std::string> idList;
  int                      level = 0;

  msg << MSG::DEBUG << "managing histos list " << list << " reset " << reset << " save " << save << " endofrun "
      << isFromEndOfRun << endmsg;
  m_infoFileStatus = "......this is the file name were we will save histograms...........";
  ::strftime( timestr, sizeof( timestr ), "%Y%m%dT%H%M%S", timeInfo );
  ::strftime( year, sizeof( year ), "%Y", timeInfo );
  ::strftime( month, sizeof( month ), "%m", timeInfo );
  ::strftime( day, sizeof( day ), "%d", timeInfo );

  if ( save ) {
    std::unique_ptr<TFile> hist_file;
    std::string dirName = m_saveSetDir + "/" + year + "/" + m_partName + "/" + m_taskName + "/" + month + "/" + day;
    void*       dir     = gSystem->OpenDirectory( dirName.c_str() );
    if ( !dir ) { gSystem->mkdir( dirName.c_str(), true ); }
    // add runnumber to saveset name
    std::stringstream str;
    str << dirName << "/" << m_taskName << "-";
    if ( m_runNumber != 0 )
      str << ( isFromEndOfRun ? m_eorNumber : m_runNumber );
    else
      str << "0";
    str << "-" << timestr << ( isFromEndOfRun ? "-EOR.root" : ".root" );

    std::string tmpfile = str.str();
    m_infoFileStatus.replace( 0, m_infoFileStatus.length(), tmpfile );

    // Use the Gaudi::Histograming::Sink::Root service as we don't want to copy the JSON->ROOT logic
    // FIXME: put this into a library in Gaudi and use that instead in the dedicated Online service.
    // An annoying side-effect is that the selection of histograms in OnlMonitorSink has no effect.
    m_rootSinkSvc.as<IProperty>()
        ->setProperty( "FileName", m_infoFileStatus )
        .andThen( [&] { return m_rootSinkSvc->stop(); } )
        .orElse( [&] { error() << "Could not save histograms with Gaudi::Histograming::Sink::Root" << endmsg; } )
        .ignore();
    // Reset the property such that on the proper stop transition of the service, the saveset (modified by us below to
    // add old histograms) is not overwritten.
    m_rootSinkSvc.as<IProperty>()->setProperty( "FileName", "/dev/null" ).ignore();
    // use our own sink to reset as Gaudi::Histograming::Sink::Root doesn't do it
    if ( reset ) {
      for ( auto& entity : m_sinkSvc->entities() ) { entity.reset(); }
    }

    hist_file.reset( new TFile( ( m_infoFileStatus ).c_str(), "UPDATE" ) );
    if ( !hist_file->IsZombie() ) {
      histogramIdentifier( object, idList, reset, save, level, hist_file.get() );
      hist_file->Close();
      hist_file.reset();
    } else {
      std::string errorTmpfile = "Error Saving Data => Zombie File..!!!!!";
      m_infoFileStatus.replace( 0, m_infoFileStatus.length(), errorTmpfile );
      msg << MSG::DEBUG << "error opening file " << m_infoFileStatus << endmsg;
      if ( hist_file->IsOpen() ) hist_file->Close();
      hist_file.reset();
    }
    if ( m_publishSavesetLoc ) { m_dimSvcSaveSetLoc->updateService( (char*)m_infoFileStatus.c_str() ); }
  } else { // f=0 because should also be able to reset without saving
    histogramIdentifier( object, idList, reset, save, level, nullptr );
  }
}

void UpdateAndReset::histogramIdentifier( const IRegistry* object, std::vector<std::string>& idList, bool reset,
                                          bool save, int& level, TDirectory* rootdir ) {
  Bool_t dirstat = TH1::AddDirectoryStatus();
  TH1::AddDirectory( kFALSE );
  try {
    std::vector<IRegistry*>  leaves;
    SmartIF<IDataManagerSvc> dataManagerSvc( this->histoSvc() );
    if ( !dataManagerSvc ) {
      TH1::AddDirectory( dirstat );
      return;
    }
    StatusCode sc = dataManagerSvc->objectLeaves( object, leaves );
    if ( sc.isFailure() ) {
      TH1::AddDirectory( dirstat );
      return;
    }
    for ( const auto* l : leaves ) {
      const std::string& id = l->identifier();
      if ( rootdir != 0 ) rootdir->cd();

      DataObject* dataObject;
      sc = histoSvc()->retrieveObject( id, dataObject );
      if ( sc.isFailure() ) { continue; }
      if ( IHistogram* histogram = dynamic_cast<AIDA::IHistogram*>( dataObject ) ) {
        save_histogram( histogram, save, reset );
        idList.push_back( id );
        continue;
      }
      if ( IProfile1D* profile = dynamic_cast<AIDA::IProfile1D*>( dataObject ) ) {
        save_histogram( profile, save, reset );
        idList.push_back( id );
        continue;
      }
      // not an histogram: must be a directory: create corresponding TDirectory
      auto        rootDirs = RTL::str_split( id, "/" );
      TDirectory* newdir   = rootdir;
      if ( nullptr != newdir ) {
        newdir = rootdir->mkdir( rootDirs.at( rootDirs.size() - 1 ).c_str(), "", true );
        newdir->cd();
      }
      int newLevel = level + 1;
      if ( newLevel >= 10 ) continue;
      histogramIdentifier( l, idList, reset, save, newLevel, newdir );
    }
  } catch ( const std::exception& ex ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::WARNING << "histogramidentifier std::exception: " << ex.what() << endmsg;
  }
  TH1::AddDirectory( dirstat );
}

IRegistry* UpdateAndReset::rootObject() {
  DataObject* pObj{nullptr};
  if ( histoSvc()->retrieveObject( std::string(), pObj ).isSuccess() ) return pObj->registry();
  return nullptr;
}
