/***********************************************************************************\
* (c) Copyright 1998-2022 CERN for the benefit of the LHCb and ATLAS collaborations *
 *                                                                                   *
 * This software is distributed under the terms of the Apache version 2 licence,     *
 * copied verbatim in the file "LICENSE".                                            *
 *                                                                                   *
 * In applying this licence, CERN does not waive the privileges and immunities       *
 * granted to it by virtue of its status as an Intergovernmental Organization        *
 * or submit itself to any jurisdiction.                                             *
 \***********************************************************************************/

#include "Gaudi/MonitoringHub.h"
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/Service.h"

#include <deque>
#include <string>
#include <vector>

class IGauchoMonitorSvc;

// namespace Gaudi::Parsers {
//   StatusCode parse( std::vector<std::pair<std::string, std::string>>& result, const std::string& input );
// }

struct IOnlMonitorSink : extend_interfaces<INamedInterface> {
  DeclareInterfaceID( IOnlMonitorSink, 1, 0 );

  virtual std::deque<Gaudi::Monitoring::Hub::Entity>& entities() = 0;
};

class OnlMonitorSink : public extends<Service, IOnlMonitorSink>, public Gaudi::Monitoring::Hub::Sink {
public:
  using extends::extends;
  IGauchoMonitorSvc* m_mons = nullptr;
  //   bool               m_delayedProcessing = false;

  StatusCode initialize() override;
  StatusCode finalize() override;

  StatusCode start() override;
  StatusCode stop() override;

  void registerEntity( Gaudi::Monitoring::Hub::Entity ent ) override;

  void removeEntity( Gaudi::Monitoring::Hub::Entity const& /*ent*/ ) override;

  std::deque<Gaudi::Monitoring::Hub::Entity>& entities() override { return m_monitoringEntities; }

private:
  std::deque<Gaudi::Monitoring::Hub::Entity>                        m_monitoringEntities;
  Gaudi::Property<std::vector<std::pair<std::string, std::string>>> m_CountersToPublish{
      this, "CountersToPublish", {}, "Algorithms and counters to publish Online"};
  Gaudi::Property<std::vector<std::pair<std::string, std::string>>> m_HistogramsToPublish{
      this, "HistogramsToPublish", {}, "Histograms to publish Online"};
  std::vector<std::pair<std::string, std::string>> m_AlgoCounters;
  std::vector<std::pair<std::string, std::string>> m_Histograms;
};
