//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
/*
 * GaudiHistograms.h
 *
 *  Created on: Nov 19, 2020
 *      Author: beat
 */
#ifndef ONLINE_GAUCHO_SRC_COMPONENTS_GAUDIHISTOGRAMS_H_
#define ONLINE_GAUCHO_SRC_COMPONENTS_GAUDIHISTOGRAMS_H_

#include <Gaucho/MonHist.h>
#include <RTL/strdef.h>
#include <atomic>
#include <string>
#include <tuple>

/// specialization of MonCounter class for AveragingCounters
using namespace std;
using namespace Gaudi::Accumulators;
using namespace Gaudi::Monitoring;
using Entity = Hub::Entity;

/// Online namespace declaration
namespace Online {

  template <>
  MONTYPE MonHist<Entity>::analyzeType( const Entity& var ) {
    this->object               = (Entity*)new Entity( var );
    nlohmann::json j           = this->object->toJSON();
    string         typstr      = j.at( "type" ).get<string>();
    auto           splt        = RTL::str_split( typstr, ":" );
    this->descriptor.clazz     = HT_Unknown;
    this->descriptor.bintyp    = BT_Unknown;
    this->descriptor.dimension = j.at( "dimension" ).get<unsigned int>();
    if ( splt.at( 0 ) != "histogram" ) { return this->descriptor.type(); }
    this->descriptor.clazz   = HT_Gaudi;
    this->descriptor.profile = splt.at( 1 ).find( "Profile" ) != string::npos;
    this->descriptor.bintyp  = this->descriptor.bin_type( splt.at( 2 ) );
    return this->descriptor.type();
  }

  template <>
  void MonHist<Entity>::reset() {
    auto* r = const_cast<Entity*>( this->object );
    r->reset();
  }

  template <>
  int MonHist<Entity>::setup() {
    switch ( this->descriptor.clazz ) {
    case HT_Gaudi: {
      const Entity*  rhist = this->object;
      nlohmann::json j     = rhist->toJSON();
      auto   jsonAxis      = j.at( "axis" );
      size_t totbin        = 1;
      this->title          = j.at( "title" ).get<string>();
      this->axes.resize( this->descriptor.dimension );
      for ( int i = 0; i < this->descriptor.dimension; i++ ) {
        this->axes[i].nbin   = jsonAxis[i].at( "nBins" ).get<int>();
        this->axes[i].min    = jsonAxis[i].at( "minValue" ).get<double>();
        this->axes[i].max    = jsonAxis[i].at( "maxValue" ).get<double>();
        this->axes[i].lablen = 0;
        totbin *= ( this->axes[i].nbin + 2 );
      }
      this->headerLen =
          DimHistbuff1::buffer_name_offset( this->descriptor.dimension ) + name_length() + 1 + this->title_length() + 1;
      this->headerLen        = ( this->headerLen + 7 ) & ~7;
      this->m_xmitbuffersize = this->headerLen + ( ( this->descriptor.profile ) ? 4 : 2 ) * totbin * sizeof( double );
      break;
    }
    default:
      break;
    }
    return this->m_xmitbuffersize;
  }

  namespace {
    template <typename T> struct Helper   {
      static void cpy_bins(void* ptr, nlohmann::json& j)   {
	DimHistbuff1* pp = (DimHistbuff1*)ptr;
	if      ( pp->dim == 1 ) pp->type = H_1DIM;
	else if ( pp->dim == 2 ) pp->type = H_2DIM;
	else if ( pp->dim == 3 ) pp->type = H_3DIM;
	auto bincont = j.at( "bins" ).get<std::vector<T> >();
	auto blocksize = bincont.size() * sizeof( double );
	double* content = add_ptr<double>( ptr, pp->dataoff );
	double* weights = add_ptr<double>( ptr, pp->dataoff + blocksize );
	for(size_t i=0; i < bincont.size(); ++i, ++content, ++weights)    {
	  *content = bincont[i];
	  *weights = 0e0;  // // TODO fill weights when actually available
	}
      }
    };
  }

  template <>
  int MonHist<Entity>::serialize( void* ptr ) {

    DimHistbuff1* pp = (DimHistbuff1*)ptr;
    setup_buffer( pp, *this );
    setup_labels( pp, this->axes );

    nlohmann::json j        = this->object->toJSON();
    string         typstr   = j.at( "type" ).get<string>();
    auto           splt     = RTL::str_split( typstr, ":" );
    auto           NEntries = j.at( "nEntries" ).get<unsigned long>();
    auto           btyp     = ::tolower(splt.at(2)[0]);
    pp->nentries            = double( NEntries );
    pp->dataoff             = this->hdrlen(); // TODO removed?
    if ( this->descriptor.profile ) {
      auto bincont          = j.at("bins").get<std::vector<std::tuple<std::tuple<unsigned int, double>, double>>>();
      GaudiProfileBin* dest = static_cast<GaudiProfileBin*>( add_ptr( ptr, pp->dataoff ) );
      for ( size_t i = 0; i < bincont.size(); i++ ) {
	std::tuple<unsigned int, double> tmp;
	std::tie( tmp, dest[i].sumw2 )         = bincont[i];
	std::tie( dest[i].nent, dest[i].sumw ) = tmp;
      }
    }
    else if ( btyp == 'd' )
      Helper<double>::cpy_bins(ptr, j);
    else if ( btyp == 'f' )
      Helper<float>::cpy_bins(ptr, j);
    else if ( btyp == 'l' )
      Helper<long>::cpy_bins(ptr, j);
    else if ( btyp == 'm' )
      Helper<long unsigned>::cpy_bins(ptr, j);
    else if ( btyp == 'i' )
      Helper<int>::cpy_bins(ptr, j);
    else if ( btyp == 'j' )
      Helper<unsigned int>::cpy_bins(ptr, j);
    else if ( btyp == 's' )
      Helper<short>::cpy_bins(ptr, j);
    else if ( btyp == 't' )
      Helper<unsigned short>::cpy_bins(ptr, j);
    else if ( btyp == 'c' )
      Helper<char>::cpy_bins(ptr, j);
    else if ( btyp == 'h' )
      Helper<unsigned char>::cpy_bins(ptr, j);
    else
      throw std::runtime_error("Unknown Gaudi Histogram type: "+typstr);
    return pp->reclen;
  }
} // namespace Online
#endif /* ONLINE_GAUCHO_SRC_COMPONENTS_GAUDIHISTOGRAMS_H_ */
