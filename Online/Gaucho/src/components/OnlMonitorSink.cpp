/***********************************************************************************\
* (c) Copyright 1998-2022 CERN for the benefit of the LHCb and ATLAS collaborations *
 *                                                                                   *
 * This software is distributed under the terms of the Apache version 2 licence,     *
 * copied verbatim in the file "LICENSE".                                            *
 *                                                                                   *
 * In applying this licence, CERN does not waive the privileges and immunities       *
 * granted to it by virtue of its status as an Intergovernmental Organization        *
 * or submit itself to any jurisdiction.                                             *
 \***********************************************************************************/

#include "OnlMonitorSink.h"

#include "Gaucho/IGauchoMonitorSvc.h"
#include "GaudiKernel/Service.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/Accumulators/Histogram.h"

#include "RTL/strdef.h"
#include <nlohmann/json.hpp>

#include <algorithm>
#include <regex>

using namespace std::string_literals;
using namespace Gaudi::Accumulators;
using namespace std;

namespace Gaudi::Parsers {
  StatusCode parse( std::vector<std::pair<std::string, std::string>>& result, const std::string& input );
}

StatusCode Gaudi::Parsers::parse( std::vector<std::pair<std::string, std::string>>& result, const std::string& input ) {
  return Gaudi::Parsers::parse_( result, input );
}

StatusCode OnlMonitorSink::initialize() {
  StatusCode sc = Service::initialize();
  if ( !sc.isSuccess() )  {
    error() << "Failed to initialize the base class." << endmsg;
    return sc;
  }
  sc = serviceLocator()->service( "MonitorSvc", m_mons, false );
  if ( !sc.isSuccess() )  {
    error() << "Failed to access the MonitorSvc" << endmsg;
    return sc;
  }
  for ( const auto& p : m_CountersToPublish ) { m_AlgoCounters.emplace_back( p.first, p.second ); }
  for ( const auto& p : m_HistogramsToPublish ) { m_Histograms.emplace_back( p.first, p.second ); }

  serviceLocator()->monitoringHub().addSink( this );
  return sc;
}

StatusCode OnlMonitorSink::start() {
  auto ok = Service::start();
  if ( !ok ) return ok;

  std::sort( begin( m_monitoringEntities ), end( m_monitoringEntities ), []( const auto& a, const auto& b ) {
    return std::tie( a.name, a.component ) < std::tie( b.name, b.component );
  } );

  for ( auto& entity : m_monitoringEntities ) {
    m_mons->declareInfo( entity.component + "/" + entity.name, entity, "" );
  }
  m_monitoringEntities.clear();
  return StatusCode::SUCCESS;
}

StatusCode OnlMonitorSink::stop() {
  return Service::stop();
}

StatusCode OnlMonitorSink::finalize() {
  if ( m_mons )  {
    m_mons->undeclareAll(nullptr);
    m_mons->release();
    m_mons = nullptr;
  }
  return Service::finalize();
}

void OnlMonitorSink::registerEntity( Gaudi::Monitoring::Hub::Entity ent ) {

  for ( long unsigned int i = 0; i < m_CountersToPublish.size(); i++ ) {

    auto type = std::string( ent.type );
    if ( type.find( "counter" ) != std::string::npos ) {
      std::smatch m;
      std::regex  regex_algo( m_AlgoCounters.at( i ).first, std::regex::icase );
      std::regex  regex_counter( m_AlgoCounters.at( i ).second, std::regex::icase );
      auto        algorithm = std::string( ent.component );
      auto        counter   = std::string( ent.name );
      std::regex  regex_type( type, std::regex::icase );

      if ( std::regex_search( algorithm, m, regex_algo ) && std::regex_search( counter, m, regex_counter ) ) {
	std::vector<std::string> item = RTL::str_split(type,":");
	if ( item.size() == 3 && item[0] == "counter" )   {
	  if ( item[1] == "BinomialCounter" ||
	       item[1] == "AveragingCounter" ||
	       item[1] == "StatCounter" ||
	       item[1] == "Counter" ||
	       item[1] == "SigmaCounter" ||
	       item[1] == "MsgCounter" )   {
	    const std::string& dtyp = item[2];
	    switch(dtyp[0])   {
	    case 'd':  // double
	    case 'f':  // float
	    case 'c':  // char
	    case 'h':  // uchar
	    case 's':  // short
	    case 't':  // ushort
	    case 'i':  // int
	    case 'j':  // uint
	    case 'l':  // long
	    case 'm':  // ulong
	      m_monitoringEntities.emplace_back( std::move( ent ) );
	      continue;
	    default:
	      break;
	    }
	  }
	}
        else if ( item[1] == "MsgCounter" )  {
	  m_monitoringEntities.emplace_back( std::move(ent) );
	  continue;
	}
	throw std::runtime_error( "OnlMonitorSink :: Counter " + algorithm + "/" + counter + " has type " + type +
				  ", which is currently unsupported" );
      }
    }
  }
  
  for ( long unsigned int i = 0; i < m_HistogramsToPublish.size(); i++ ) {
    auto type = std::string( ent.type );
    if ( type.find( "histogram" ) != std::string::npos ) {
      std::smatch m;
      std::regex  regex_algo( m_Histograms.at( i ).first, std::regex::icase );
      std::regex  regex_counter( m_Histograms.at( i ).second, std::regex::icase );
      auto        algorithm = std::string( ent.component );
      auto        counter   = std::string( ent.name );
      std::regex  regex_type( type, std::regex::icase );

      if ( std::regex_search( algorithm, m, regex_algo ) && std::regex_search( counter, m, regex_counter ) )
        m_monitoringEntities.emplace_back( std::move( ent ) );
    }
  }
}

void OnlMonitorSink::removeEntity( Gaudi::Monitoring::Hub::Entity const& entity) {
  if ( m_mons )   {
    m_mons->undeclareInfo(entity.component + "/" + entity.name);
  }
  auto it = std::find( begin( m_monitoringEntities ), end( m_monitoringEntities ), entity );
  if ( it != m_monitoringEntities.end() ) { m_monitoringEntities.erase( it ); }
}

DECLARE_COMPONENT( OnlMonitorSink )
