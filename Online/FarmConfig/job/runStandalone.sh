#!/bin/bash
# =========================================================================
#
#  Generic farm task startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
#
utgid="P_$$";
logfifo=;
partition=;
runinfo=;
task_type=;
task_info=;
task_count=;
task_auto=;
task_debug=;
start_logger=;
startup_script="/group/online/dataflow/scripts/runFarmTask.sh";
#
usage()  {
    echo " Usage: $0 -arg [-arg]";
    echo " Passed arguments: $*";
    echo " ";
    echo " Standalone script to mimic the online envirnent for HLT-farm tasks.";
    echo " ";
    echo " ";
    echo "    At least one of these 2 arguments must be supplied:";
    echo "        -partition <name>   Partition name to detemina the run-info data file";
    echo "        -runinfo   <path>   Path to run-info file to determine the partition.";
    echo "    Process steering arguments:";
    echo "     *  -type      <name>   Set Task type (selects job file and options from FarmConfig.";
    echo "        -auto               Set Task to auto startup. Must be supported by script!";
    echo "        -debug              Add -debug flag to image startup.";
    echo "    Optional arguments to be passed to the process instance:";
    echo "     *  -logfifo   <path>   Path to output- and error logger fifo.";
    echo "     *  -utgid     <name>   Process UTGID. Default=${utgid}.";
    echo "        -taskinfo  <path>   Path to the architecture files.";
    echo "        -count     <number> Set NBOFSLAVES.";
    echo " ";
    echo "    (*)  => These options are mandatory for default dataflow tasks.";
    #echo "  partition:'$partition' runinfo:'$runinfo'  logfifo:'$logfifo' ";
    exit 13;    # EACCES
}
#
#  Parse script arguments
#
parse_args()  {
    all_args="$*";
    while [[ "$1" == -* ]]; do
	a1=`echo $1 | tr A-Z a-z`;
	case ${a1} in
	    -auto)
		task_auto="-auto";
		;;
	    -debug)
		task_debug="-debug";
		;;
	    -partition)
		partition="$2";
		shift;
		;;
	    -runinfo)
		runinfo="$2";
		shift;
		;;
	    -logfifo)
		logfifo="$2";
		shift;
		;;
	    -logger)
		start_logger="YES";
		;;
	    -utgid)
		utgid="$2";
		shift;
		;;
	    -type)
		task_type="$2";
		shift;
		;;
	    -count)
		task_count="$2";
		shift;
		;;
	    -taskinfo)
		task_info="$2";
		shift;
		;;
	    -script)
		startup_script="$2";
		shift;
		;;
	    *)
		usage $all_args;
		;;
	esac
	shift;
    done;
    if test -z "$partition" -a -z "$runinfo"; then
	echo "Neither partition name nor runinfo file specified!";
	usage $*;
    fi;
    if test -z "$logfifo"; then
	echo "No -logfifo argument given!";
	usage $*;
    fi;
    if test -z "$task_type"; then
	echo "No -type argument given!";
	usage $*;
    fi;
}
#
run_it()  {
    alias errlog='xterm -sl 20000 -ls -132 -geometry 230x50 -title logViewer@${DIM_DNS_NODE} -e /opt/FMC/bin/logViewer -l 2 -S '
    echo "+ ------------------------------------------------------------------+";
    echo "|   Starting LHCb online task executor....                          |";
    echo "+ ------------------------------------------------------------------+";
    echo "| UTGID:          $utgid";
    echo "| Runinfo:        $runinfo";
    echo "| Output logger:  $logfifo";
    echo "| Task type:      $task_type";
    echo "| Number of instances: $task_count";
    echo "| Start error logger to see output from task:";
    echo "| xterm -sl 10000 -ls -132 -geometry 230x50 -e /opt/FMC/bin/logViewer -l 2 -S -m `hostname -s` -N ${DIM_DNS_NODE} &";
    echo "+ ------------------------------------------------------------------+";
    echo "| Command string: UTGID=$utgid ${startup_script} $* ";
    echo "+ ------------------------------------------------------------------+";
    if test -n "${start_logger}"; then
	xterm -sl 20000 -ls -132 \
	    -geometry 250x50 \
	    -title logViewer@${DIM_DNS_NODE} \
	    -e /opt/FMC/bin/logViewer \
	    -l 2 -S -m `hostname -s` -N ${DIM_DNS_NODE} &
    fi;
    export UTGID=$utgid;
    source ${startup_script} -logfifo=${logfifo} $*;
}
# Parse argument list
parse_args $*;
#
if test ! -f $runinfo; then
    echo "The runinfo file for partition $partition : $runinfo does not exist.";
    usage $*;
fi;
if test ! -e $logfifo; then
    echo "The logfifo '$logfifo' for partition $partition : $runinfo does not exist.";
    usage $*;
fi;
#
other_args="";
if test -n "$task_count"; then 
    other_args="$other_args -count=$task_count";
fi;
if test -n "$task_info"; then 
    other_args="$other_args -taskinfo=$task_info";
fi;
if test -n "$task_auto"; then 
    other_args="$other_args -auto=YES";
fi;
if test -n "$task_debug"; then
    other_args="$other_args -debug=YES";
fi;
#
#
#
if test -n "$partition"; then
    runinfo=/group/online/dataflow/options/${partition}/HLT/OnlineEnvBase.py;
    run_it -type=$task_type -runinfo=$runinfo $other_args;
elif test -n "$runinfo"; then
    run_it -type=$task_type -runinfo=$runinfo $other_args;
fi;
