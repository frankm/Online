#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
# . /group/online/dataflow/EventBuilder/EventBuilderRelease/setup.x86_64-centos7-gcc9-opt.vars;
#. /group/online/dataflow/EventBuilder/EventBuilderRelease/setup.x86_64_v2-centos7-gcc10-do0.vars;
. /group/online/dataflow/EventBuilder/EventBuilderRelease/setup.x86_64_v2-centos7-gcc10-opt.vars;
#. /group/online/dataflow/EventBuilder/EventBuilderRelease/setup.${CMTCONFIG}.vars;
cd ${FARMCONFIGROOT}/job;
#
`dataflow_task Class1` -opts=../../EventBuilding/options/${TASK_TYPE}.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
