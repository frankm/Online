#!/bin/bash
# =========================================================================
#
#  Default script to start the Allen on a farm node.
#
#  Author   R. Aaij, M. Fontanta, D. vom Bruch, C. Agapopoulou, D. Campora
#  Version: 1.0
#  Date:    05/2022
#
# =========================================================================

source /cvmfs/lhcb.cern.ch/lib/LbEnv

if [ -z "${MOORE_VERSION}" ]; then
  echo "ERROR: HLT1 Stack not set"
  return 3
elif [ -z "${HLT_ARCHITECTURE}" ]; then
  echo "ERROR: HLT1 Architecture not set"
  return 4
fi

echo "HLT1 Stack = ${MOORE_VERSION}"
echo "HLT1 Architecture = ${ALLEN_ARCHITECTURE}"

NUMA_DOMAIN=0

if [[ "${HLT_ARCHITECTURE}" == *"cuda"* ]]; then
  has_gpu="`lspci | grep -i NVIDIA | grep 2231`"; # 2231 is the A5000
  if [ -z "$has_gpu" ]; then
    echo "GPU not found"
    echo "ERROR: HLT1 Architecture contains substring cuda and GPU not found"
    return 1
  else
    echo "GPU found"
    nvidia-smi
    if [ $? -ne 0 ]; then
      echo "WARNING: nvidia-smi returned an error code"
    fi
    if [[ `nvidia-smi -i 0` != *"No running processes found"* ]]; then
      echo "ERROR: GPU is under use by another process"
      return 5
    fi
    NUMA_DOMAIN=`nvidia-smi topo -m | grep GPU0 | tail -1 | awk '{ print $NF; }'`
    unset CUDA_VISIBLE_DEVICES
  fi
fi

source /group/hlt/commissioning/${MOORE_VERSION}/MooreOnline/build.${HLT_ARCHITECTURE}/mooreonlineenv.sh
export PYTHONPATH=${DYNAMIC_OPTS}:${PYTHONPATH}

numactl --cpunodebind=${NUMA_DOMAIN} --membind=${NUMA_DOMAIN} sh -c "exec -a ${UTGID} genPython.exe `which gaudirun.py` ${ALLENONLINEROOT}/options/AllenConfig.py --application=AllenApplication;"
