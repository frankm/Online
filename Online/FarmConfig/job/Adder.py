"""
     Online Passthrough application configuration

     @author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import Gaudi.Configuration as Gaudi
from   GaudiOnline.OnlineApplication import *
import Configurables
from Configurables import MonitorSvc
from Configurables import AdderSvc 
from xml.dom import minidom
import os

def TaskListfromArch(archfile, tasklist):
    xmldoc = minidom.parse(archfile)
    itemlist = xmldoc.getElementsByTagName('task')
#    tasklist = []
    for s in itemlist:
        nam = s.attributes['name']
#        if not nam.find('MEPR')>=0:
        tasklist.append(s.attributes['name'].value)


# BusySvc = Configurables.BusySvc
HistPers = Configurables.HistogramPersistencySvc
print 'MSG_INFO: ',MSG_INFO

def SetupNodeAdder(svc,nam,type,partitionName):
  svc.PartitionName = partitionName
  if type == "Histo":
    s = nam#+"HistAdder"
    snam = "Histos"
    svc.AdderClass = "hists"
  else:
    s = nam#+"CounterAdder"
    snam = "Counter"
    svc.AdderClass = "Counter"
  svc.MyName  = "<part>_<node>_"+nam
  svc.TaskPattern = "<part>_<node>_"+nam+"_(.*)"
  svc.ServicePattern  = "MON_<part>_<node>_"+nam+"_(.*)/"+snam+"/"
  svc.ReceiveTimeout = 5
#   if dohostdns:
#       AddS.InDNS = InDns
#       AddS.OutDNS = OutDns
  if partitionName == "LHCbA": # overwrite certain options for the Alignment...
    svc.SaveInterval = -1
    svc.SaveonUpdate = False
    svc.SaveSetTaskName = s;
    svc.ReceiveTimeout = 1
    svc.EoRTmoFactor = 2
    svc.GotoPause = False

def SetupSFAdder(svc,nam,type,partitionName):
  svc.PartitionName = partitionName
  svc.AdderClass = "hists"
  if type == "Histo":
    s = nam+"HistAdder"
    snam = "Histos"
    svc.AdderClass = "hists"
  else:
    s = nam+"CounterAdder"
    snam = "Counter"
    svc.AdderClass = "Counter"
  svc.MyName  = "<part>_<node>_"+nam
  svc.TaskPattern = "<part>_<node>[0-9][0-9]_NodeAdder_0"
  svc.ServicePattern  = "MON_<part>_<node>[0-9][0-9]_"+nam+"/"+snam+"/"
  svc.ReceiveTimeout = 8
#   if dohostdns:
#       AddS.InDNS = InDns
#       AddS.OutDNS = OutDns
  if partitionName == "LHCbA": # overwrite certain options for the Alignment...
    svc.SaveInterval = -1
    svc.SaveonUpdate = False
    svc.SaveSetTaskName = s;
    svc.ReceiveTimeout = 1
    svc.EoRTmoFactor = 8
    svc.GotoPause = False

def SetupTopAdder(svc,nam,type,partitionName):
  svc.PartitionName = partitionName
  svc.AdderClass = "hists"
  if type == "Histo":
    s = nam+"HistAdder"
    snam = "Histos"
    svc.AdderClass = "hists"
  else:
    s = nam+"CounterAdder"
    snam = "Counter"
    svc.AdderClass = "Counter"
  svc.MyName  = "<part>_"+nam+"_00"
  svc.TaskPattern = "<part>_HLT[a-z][0-9][0-9]_SubFarmAdder_(.*)"
  svc.ServicePattern  = "MON_<part>_hlt[a-z][0-9][0-9]_"+nam+"/"+snam+"/"
  svc.ReceiveTimeout = 5
#   if dohostdns:
#       AddS.InDNS = InDns
#       AddS.OutDNS = OutDns
  if partitionName == "LHCbA": # overwrite certain options for the Alignment...
    svc.SaveInterval = -1
    svc.SaveonUpdate = False
    svc.SaveSetTaskName = s;
    svc.ReceiveTimeout = 1
    svc.EoRTmoFactor = 2
    svc.GotoPause = False

def SetupSaver(svc,nam,type,partitionName):
  svc.PartitionName = partitionName
  svc.AdderClass = "hists"
  if type == "Histo":
    s = nam+"HistAdder"
    snam = "Histos"
    svc.AdderClass = "hists"
  else:
    return
  svc.MyName  = "<part>_Saverhlt01_"+nam+"_00"
  svc.TaskPattern = "MON_<part>_HLT02_PartAdder_0"
  svc.ServicePattern  = "MON_<part>_hlt01_"+nam+"_00/Histos/"
  svc.ReceiveTimeout = 12
  svc.SaveRootDir = "/hist/Savesets"
  svc.IsSaver = True
  svc.SaveInterval = 900
  svc.SaveonUpdate = False
  svc.SaveSetTaskName= s
#   if dohostdns:
#       AddS.InDNS = InDns
#       AddS.OutDNS = OutDns
  if partitionName == "LHCbA": # overwrite certain options for the Alignment...
    svc.SaveInterval = -1
    svc.SaveonUpdate = False
    svc.SaveSetTaskName = s;
    svc.ReceiveTimeout = 1
    svc.EoRTmoFactor = 2
    svc.GotoPause = False

  
class AdderApp(Application):
  def __init__(self, outputLevel, partitionName='SF', partitionID=0xFFFF, level="1", archname=""):
    Application.__init__(self,
                         outputLevel=outputLevel,
                         partitionName=partitionName,
                         partitionID=partitionID,
                         classType=Class1)
    if level=="1":
      InDns = os.getenv("InDns","<dns>")
      OutDns = os.getenv("OutDns","<dns>")
      SetupFunc = "SetupNodeAdder"
    if level =="2":
      InDns = os.getenv("InDns","<node>")
      OutDns = os.getenv("OutDns","hlt01")
      SetupFunc = "SetupSFAdder"
    elif level == "3":
      InDns = os.getenv("InDns","hlt01")
      OutDns = os.getenv("OutDns","mona08")
      SetupFunc = "SetupTopAdder"
    elif level == "3.1":
      InDns = os.getenv("InDns","mona08")
      OutDns = os.getenv("OutDns","mona08")
      SetupFunc = "SetupSaver"
    elif level == "4":
      InDns = os.getenv("InDns","mona08")
      OutDns = os.getenv("OutDns","mona08")
      
    self.config.autoStart = False
    self.app.HistogramPersistency  = 'NONE'
    HistPers.Warnings       = False;
    msv = MonitorSvc()
    msv.OutputLevel = outputLevel
    msv.CounterUpdateInterval     = 5;
    msv.PartitionName             = partitionName;
    msv.ExpandCounterServices = True
#     print msv
#     msv.OutDns = "ecs03"
    self.app.ExtSvc.append(msv)
    tasklist = []
#     arch = os.getenv("ARCH",arch)
    dohostdns = (archname == "Calib")
    archfile = "/group/online/dataflow/architectures/lbDataflowArch_"+archname+".xml"
    TaskListfromArch(archfile, tasklist)
    histsvc = []
    cntsvc = []
    for s in tasklist:
      if 'NodeAdder' in s:
        continue
      if 'AligAdder' in s:
        continue
      if 'SubFarmAdder' in s:
        continue
      tsk = s.encode()#+"HistAdder"
      AddS = AdderSvc(tsk+"HistAdder")
      globals()[SetupFunc](AddS,tsk,"Histo",partitionName)
      AddS.InDNS = InDns
      AddS.OutDNS = OutDns;
      print AddS.MyName, AddS.InDNS, AddS.OutDNS,AddS.TaskPattern, AddS.ServicePattern
      self.app.ExtSvc.append(AddS)
      AddS = AdderSvc(tsk+"CountAdder")
      globals()[SetupFunc](AddS,tsk,"Counter",partitionName)
      AddS.InDNS = InDns
      AddS.OutDNS = OutDns;
      print AddS.MyName, AddS.InDNS, AddS.OutDNS,AddS.TaskPattern, AddS.ServicePattern
      self.app.ExtSvc.append(AddS)
#       histsvc.append(hsvc)
#       cntsvc.append(csvc)

Level = os.getenv("AdderLevel","")
Arch = os.getenv("ARCH_FILE","")    
app = AdderApp(MSG_DEBUG, partitionName='LHCb',level=Level,archname=Arch)
print 'Setup complete....'
# print dir(app)


