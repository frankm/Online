#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
unset PYTHONPATH;
unset PYTHONHOME;
eval `/usr/bin/python2 -c "import os;s=os.environ['UTGID'];print 'export BU_OPTIONS='+s[s.find('BU'):]+'.opts'"`;
if test "${PARTITION}" = "TDET"; then
	. /group/online/dataflow/EventBuilder/EventBuilderDev/setup.x86_64_v2-centos7-gcc10-opt.vars;
elif test "${PARTITION}" = "FEST"; then
	. /group/online/dataflow/EventBuilder/EventBuilderDev/setup.x86_64_v2-centos7-gcc10-opt.vars;
else
	. /group/online/dataflow/EventBuilder/EventBuilderRelease/setup.x86_64_v2-centos7-gcc10-opt.vars;
fi;

cd ${FARMCONFIGROOT}/job;
`dataflow_task Class1` -opts=../../EventBuilding/options/${BU_OPTIONS} ${AUTO_STARTUP} ${DEBUG_STARTUP};
