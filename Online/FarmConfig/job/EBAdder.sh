#!/bin/bash
# =========================================================================
#
#  Default script to start the event reader task on the HLT farm
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
export LC_ALL=C;
export ADDER_TYPE=EventBuilder;
exec -a ${UTGID} genPython.exe `which gaudirun.py` ./AddersFromArchitecture.py --application=Online::OnlineApplication;
