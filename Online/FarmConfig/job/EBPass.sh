#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
if [[ "$UTGID" == *"${TASK_TYPE}_0"* ]]; then
  export MBM_INPUT_BUFFER=Events_0;
elif [[ "$UTGID" == *"${TASK_TYPE}_1"* ]]; then
  export MBM_INPUT_BUFFER=Events_1;
elif [[ "$UTGID" == *"${TASK_TYPE}_2"* ]]; then
  export MBM_INPUT_BUFFER=Events_0;
elif [[ "$UTGID" == *"${TASK_TYPE}_3"* ]]; then
  export MBM_INPUT_BUFFER=Events_1;
elif [[ "$UTGID" == *"${TASK_TYPE}_4"* ]]; then
  export MBM_INPUT_BUFFER=Events_0;
else
  export MBM_INPUT_BUFFER=Events_1;
fi;
#
execute `gaudi_event_task ${FARMCONFIGROOT}/job/Passthrough.py`;
