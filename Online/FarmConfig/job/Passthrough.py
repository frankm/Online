"""
     Minimal Gaudi task in the online environment

     @author M.Frank
"""
from __future__ import print_function
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import sys
import Configurables
import Gaudi.Configuration as Gaudi

import GaudiOnline
import OnlineEnvBase as OnlineEnv

from Configurables import Online__FlowManager as FlowManager
application = GaudiOnline.Passthrough(outputLevel=OnlineEnv.OutputLevel,
                                      partitionName=OnlineEnv.PartitionName,
                                      partitionID=OnlineEnv.PartitionID,
                                      classType=GaudiOnline.Class1)
application.setup_fifolog()
application.config.debug       = False
buffer_name = os.getenv("MBM_INPUT_BUFFER","Events")
application.setup_mbm_access(buffer_name, True)
writer = application.setup_mbm_output('EventOutput')
writer.MBM_maxConsumerWait     = 10
writer.MBM_allocationSize      = 1024*1024*10
writer.MBM_burstPrintCount     = 25000
writer.MaxEventsPerTransaction = 10
writer.UseRawData              = False
writer.RequireODIN             = False

writer.UseRawData              = True

ev_size = Configurables.Online__EventSize('EventSize')

explorer = Configurables.StoreExplorerAlg('Explorer')
explorer.Load                  = 1
explorer.PrintFreq             = 0.0000001
#explorer.OutputLevel           = 1

##have_odin = False
part = OnlineEnv.PartitionName
###if part == 'FEST' or part == 'LHCb2' or part == 'TDET':
have_odin = True

application.setup_hive(FlowManager("EventLoop"), 40)
application.setup_monitoring(have_odin=have_odin)
#application.setup_algorithms(writer, 0.05)
application.setup_algorithms([ev_size,explorer,writer], 1.0)
if os.getenv("BURN_CPU"):
  application.passThrough.BurnCPU   = 1
  application.passThrough.MicroDelayTime = 1000.0*float(os.getenv("BURN_CPU"))

application.monSvc.DimUpdateInterval   = 5
application.monSvc.CounterClasses = [
  "AuxCounters 10 (.*)EventInput/daqCounter.(.*)",
  "DAQErrors    3 (.*)EventInput/daqError.(.*)"
]; 

application.config.expandTAE           = False
application.config.burstPrintCount     = 30000

# Mode slection::  synch: 0 async_queued: 1 sync_queued: 2
application.config.execMode            = 1
application.config.numEventThreads     = 15
application.config.MBM_numConnections  = 8
application.config.MBM_numEventThreads = 5
#
application.config.numEventThreads     = 15
application.config.MBM_numConnections  = 3
application.config.MBM_numEventThreads = 4
#
application.config.numEventThreads     = 4
application.config.MBM_numConnections  = 2
application.config.MBM_numEventThreads = 2
#
if os.getenv("numEventThreads"):
  application.config.numEventThreads     = int(os.getenv("numEventThreads"))
if os.getenv("MBM_numConnections"):
  application.config.MBM_numConnections  = int(os.getenv("MBM_numConnections"))
if os.getenv("MBM_numEventThreads"):
  application.config.MBM_numEventThreads = int(os.getenv("MBM_numEventThreads"))

application.enableUI()  
#
# Enable this for debugging
#
_dbg = 0
if os.getenv("DEBUG"):
  _dbg = str(os.getenv("DEBUG"))

if _dbg:
  application.config.execMode            = 0
  application.config.numEventThreads     = 1
  application.config.MBM_numConnections  = 1
  application.config.MBM_numEventThreads = 1
#
application.config.MBM_requests = [
    'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0',
    'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'
]
#
print('Setup complete.... Have ODIN: '+str(have_odin))
