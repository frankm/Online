# =========================================================================
#
#  Setup script to start various types of adders
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
from xml.dom import minidom
import os
import sys
import socket
import Gaudi
import Configurables
from   GaudiOnline import Class0
from   GaudiOnline import Class1
from   GaudiOnline import Class2
from   GaudiOnline import Application
from   GauchoAppl  import GauchoApplConf

#SAVESET_DIRECTORY = '/group/online/dataflow/cmtuser/Savesets'
SAVESET_DIRECTORY = '/hist/Savesets'
HLT_TOP_NODE = "hlt01"
MON_TOP_NODE = "mon01"

EventLoopMgr     = Gaudi.Configurables.EventLoopMgr
ApplicationMgr   = Gaudi.Configuration.ApplicationMgr
vnode = False

global adder_debug_flag
adder_debug_flag = False

def VictimNode():
  import re
  victimnodes_re = []
  vnodes=""
  vnodes=os.getenv("victimnodes","")
  if vnodes!="":
    victimnodes = vnodes.split(",")
    for i in victimnodes:
      victimnodes_re.append(re.compile(i,re.IGNORECASE))
  hname = socket.gethostname().split('.')[0]
  for i in victimnodes_re:
    if i.match(hname) != None:
      return True

def TaskListfromArch(arch):
    tasklist = []
    xmldoc   = minidom.parse(arch)
    itemlist = xmldoc.getElementsByTagName('task')
    for s in itemlist:
      nam = s.attributes['name']
      tasklist.append(s.attributes['name'].value)
    return tasklist

# ===========================================================================================
#
# Class to simplyfy the building of adder services
#
# ===========================================================================================
class Adder:
  def __init__(self, task, nodes, tasks, partition, typ, in_dns=None, out_dns=None, ext=None, debug=False, run_aware=False):
    task_name = str(task)
    tname = task_name
    if ext: tname = task_name + ext
    if typ == "counter":
      adder = GauchoApplConf.AdderSvc("C_"+task_name)
      adder.ServicePattern = "MON_<part>_"+nodes+"_"+tname+"/Counter/"
    else:
      adder = GauchoApplConf.AdderSvc("H_"+task_name)
      adder.ServicePattern = "MON_<part>_"+nodes+"_"+tname+"/Histos/"

    adder.MyName         = "<part>_<node>_"+task_name
    adder.PartitionName  = partition
    if tasks:
      adder.TaskPattern  = "<part>_"+nodes+"_"+str(tasks)+"_(.*)"
    else:
      adder.TaskPattern  = "<part>_"+nodes+"_"+task_name

    if adder_debug_flag:
      print("%-8s Service pattern: %s Task pattern: %s Debug: %s"\
            %("INFO", adder.ServicePattern, adder.TaskPattern, str(debug), ))

    adder.AdderClass     = typ
    adder.RunAware       = run_aware
    adder.DebugOn        = debug
    if in_dns:
      adder.InDNS        = in_dns
    if out_dns:
      adder.OutDNS       = out_dns
    self.task_name = task_name
    self.obj = adder
    #if partition == 'FEST':
    #  adder.DebugOn = True

  def setTaskPattern(self, pattern):
    self.obj.TaskPattern = pattern
    return self

  def setServicePattern(self, pattern):
    self.obj.ServicePattern = pattern
    return self

  def enableSaving(self, recv_tmo, interval, saveset_dir):
    self.obj.ReceiveTimeout  = recv_tmo
    self.obj.IsSaver         = True
    self.obj.SaveInterval    = interval
    self.obj.SaveOnUpdate    = False
    self.obj.SaveRootDir     = saveset_dir
    self.obj.SaveSetTaskName = self.task_name
    return self


# ===========================================================================================
#
# Function to determine the adders from the architecture file
#
# ===========================================================================================
def AddersfromTasks(tasklist, adder_type, partition, dohostdns):
  global adder_debug_flag
  service_list = []
  adder_task_class = Class1
  adder_debug_flag = True
  tasks_str = str(tasklist)[1:-1]
  
  if adder_type=='HltNode' or adder_type == 'EventBuilder' or adder_type == 'AlignWorker':
    adder_task_class = Class1
    print('INFO    Configure adder of type %s Tasks: %s'%(adder_type, tasks_str, ) )
    for task_name in tasklist:
      if 'EBAdder'    in task_name:        continue
      if 'HLT2Adder'  in task_name:        continue
      if 'NodeAdder'  in task_name:        continue
      if 'AligAdder'  in task_name:        continue
      adder = Adder(task_name, '<node>', None, partition, 'counter', None, '<dns>', ext='_(.*)')
      #adder.obj.DebugOn  = True
      adder.obj.HaveTimer = 0
      service_list.append(adder.obj)

      adder = Adder(task_name, '<node>', None, partition, 'hists',   None, '<dns>', ext='_(.*)')
      #adder.obj.DebugOn  = True
      service_list.append(adder.obj)       
      # ===========================================================================================

  elif adder_type == 'HltSubfarm':
    adder_task_class = Class1
    adder_debug_flag = True
    print('INFO    Configure adder of type %s Tasks: %s'%(adder_type, tasks_str, ) )
    for task_name in tasklist:
      adder = Adder(task_name, '<node>[0-9][0-9]', 'HLT2Adder', partition, 'counter', '<node>', HLT_TOP_NODE)
      adder.obj.TrackSources = 1
      #adder.obj.DebugOn  = True
      service_list.append(adder.obj)
      adder = Adder(task_name, '<node>[0-9][0-9]', 'HLT2Adder', partition, 'hists',   '<node>', HLT_TOP_NODE)
      adder.obj.TrackSources = 1
      #adder.obj.DebugOn  = True
      service_list.append(adder.obj)
      # ===========================================================================================

  elif adder_type=='HltTop':
    adder_task_class = Class1
    adder_debug_flag = True
    for task_name in tasklist:
      adder = Adder(task_name, 'hlt[0-2][0-9][0-9]', 'HLT2SFAdder', partition, 'counter', HLT_TOP_NODE, MON_TOP_NODE)
      adder.obj.TrackSources = 1
      #adder.obj.DebugOn  = True
      service_list.append(adder.obj)
      adder = Adder(task_name, 'hlt[0-2][0-9][0-9]', 'HLT2SFAdder', partition, 'hists',   HLT_TOP_NODE, MON_TOP_NODE)
      adder.obj.TrackSources = 1
      #adder.obj.DebugOn  = True
      service_list.append(adder.obj)
      # ===========================================================================================

  elif adder_type == 'HltSaver':
    adder_debug_flag = True
    adder_task_class = Class1
    for task_name in tasklist:
      top = 'hlt[0-2][0-9][0-9]' # HLT_TOP_NODE
      adder = Adder(task_name, top, 'HLT2TopAdder', partition, 'hists', MON_TOP_NODE, MON_TOP_NODE) \
        .enableSaving(12, 900, SAVESET_DIRECTORY)
      adder.obj
      service_list.append(adder.obj)
      # ===========================================================================================

  elif adder_type=='HltPublisher':
    adder_task_class = Class0
    for task in tasklist:
      task_name = str(task)
      publisher = GauchoApplConf.GenStatSvc('Pub'+task_name)
      top = 'hlt0[0-9]' # HLT_TOP_NODE
      publisher.ServicePattern = 'MON_<part>_'+top+'_'+task_name+'/Counter/'
      publisher.TaskPattern    = '<part>_HLT0[0-9]_HLT2TopAdder'
      publisher.MyName         = '<part>_<node>_'+task_name
      publisher.PartitionName  = partition
      publisher.InDNS          = MON_TOP_NODE
      publisher.OutDNS         = HLT_TOP_NODE
      publisher.AdderClass     = 'counter'
      publisher.ServicePrefix  = '/Stat/<part>/'+task_name;
      if adder_debug_flag or True:
        print('%-8s Service pattern: %s Task pattern: %s'\
              %('INFO', publisher.ServicePattern, publisher.TaskPattern, ))
      service_list.append(publisher)
      # ===========================================================================================
  elif adder_type=='EBPartAdder':
    adder_task_class = Class1
    for task_name in tasklist:
      # SODIN is missing here !
      regex = '[a-z].eb[0-9][0-9]'
      regex = '(([a-z].eb[0-9][0-9])|(sodin01))'
      adder = Adder(task_name, regex, 'EBAdder', partition, 'counter', 'dataflow01', MON_TOP_NODE)
      adder.obj.ExpandRate   = 1
      adder.obj.TrackSources = 1
      adder.obj.HaveTimer    = 0
      #adder.obj.DebugOn     = 1
      service_list.append(adder.obj)
      adder = Adder(task_name, regex, 'EBAdder', partition, 'hists',   'dataflow01', MON_TOP_NODE)
      adder.obj.TrackSources = 1
      #adder.obj.DebugOn     = 1
      service_list.append(adder.obj)
      # ===========================================================================================
  elif adder_type=='EBPartSaver':
    adder_task_class = Class1
    for task_name in tasklist:
      adder = Adder(task_name, 'dataflow02', 'EBPartAdder', partition, 'hists', MON_TOP_NODE, MON_TOP_NODE) \
        .enableSaving(12, 900, SAVESET_DIRECTORY)
      adder.obj.TrackSources = 1
      #adder.obj.DebugOn     = 1
      service_list.append(adder.obj)
      # ===========================================================================================
  elif adder_type=='EBPartPublisher':
    adder_task_class = Class1
    for task in tasklist:
      task_name = str(task)
      publisher = GauchoApplConf.GenStatSvc('Pub'+task_name)
      publisher.ServicePattern = 'MON_<part>_<node>_'+task_name+'/Counter/'
      publisher.TaskPattern    = '<part>_<node>_EBPartAdder_(.*)'
      publisher.MyName         = '<part>_<node>_'+task_name
      publisher.PartitionName  = partition
      publisher.InDNS          = MON_TOP_NODE
      publisher.OutDNS         = MON_TOP_NODE
      publisher.AdderClass     = 'counter'
      publisher.ServicePrefix  = '/Stat/<part>/'+task_name;
      publisher.TrackSources   = 1
      #publisher.DebugOn       = True
      if adder_debug_flag:
        print('%-8s Service pattern: %-40s Task pattern: %s'\
              %('INFO', publisher.ServicePattern, publisher.TaskPattern, ))
      service_list.append(publisher)
      # ===========================================================================================
  elif adder_type == 'AlignSubfarm':  # Use this one if you ONLY have a SUBFARM ADDER
    adder_task_class = Class1
    adder_debug_flag = True
    print('INFO    Configure adder of type %s Tasks: %s'%(adder_type, tasks_str, ) )
    for task_name in tasklist:
      in_dns = '<node>'
      in_dns = None
      adder = Adder(task_name, '<node>[0-9][0-9]', None, partition, 'counter', in_dns, HLT_TOP_NODE, ext='_(.*)')
      adder.obj.TrackSources = 1
      service_list.append(adder.obj)
      adder = Adder(task_name, '<node>[0-9][0-9]', None, partition, 'hists',   in_dns, HLT_TOP_NODE, ext='_(.*)')
      adder.obj.TrackSources = 1
      service_list.append(adder.obj)
      # ===========================================================================================

  elif adder_type == 'AlignTop':  # Use this one if you ONLY have a SUBFARM ADDER
    adder_task_class = Class1
    adder_debug_flag = True
    print('INFO    Configure adder of type %s Tasks: %s'%(adder_type, tasks_str, ) )
    for task_name in tasklist:
      adder = Adder(task_name, 'hlt[0-2][0-9][0-9]', 'AligSFAdder', partition, 'counter', None, '') #HLT_TOP_NODE, HLT_TOP_NODE)
      adder.obj.TrackSources = 1
      service_list.append(adder.obj)
      adder = Adder(task_name, 'hlt[0-2][0-9][0-9]', 'AligSFAdder', partition, 'hists', None, '') #HLT_TOP_NODE, HLT_TOP_NODE)
      adder.obj.TrackSources = 1
      service_list.append(adder.obj)
      # ===========================================================================================

  elif adder_type=='AlignAdder':
    adder_task_class = Class1
    for task_name in tasklist:
      if task_name.find('Worker') == -1:
        continue
      dns     = '<dns>'
      node    = 'hlt[0-2][0-9][0-9]'
      dbg     = False
      in_dns  = None
      out_dns = 'hlt01'
      node    = socket.gethostname().upper()
      adder   = Adder(task_name, node, None, partition, 'counter', in_dns, out_dns, ext='_(.*)', debug=dbg)
      adder.obj.ExpandRate   = 1
      adder.obj.TrackSources = 1
      print('%-8s Service pattern: %-40s Task pattern: %s'%('INFO', adder.obj.ServicePattern, adder.obj.TaskPattern, ))
      service_list.append(adder.obj)
      adder   = Adder(task_name, node, None, partition, 'hists',   in_dns, out_dns, ext='_(.*)', debug=dbg)
      adder.obj.TrackSources = 1
      print('%-8s Service pattern: %-40s Task pattern: %s'%('INFO', adder.obj.ServicePattern, adder.obj.TaskPattern, ))
      service_list.append(adder.obj)
      # ===========================================================================================

  return (adder_task_class, service_list)

class AdderApp(Application):
  def __init__(self, outputLevel, partitionName='OFFLINE', partitionID=0xFFFF, adders=[]):
    import fifo_log
    Application.__init__(self, 
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID)
    # First setup printing device:
    self.config.logDeviceType    = 'RTL::Logger::LogDevice'
    self.config.logDeviceFormat  = '%-8LEVEL %-24SOURCE'
    self.config.logDeviceType    = 'fifo'
    fifo_log.logger_set_tag(partitionName)
    fifo_log.logger_start()
    # Now the rest:
    self.config.numEventThreads   = 0
    self.config.numStatusThreads  = 0
    self.config.autoStart         = False
    self.app                      = ApplicationMgr()
    self.app.MessageSvcType       = 'MessageSvc'
    self.app.EvtSel               = 'NONE'
    self.app.EvtMax               = -1
    self.app.AppName              = ''    # utgid
    self.app.HistogramPersistency = 'NONE'
    self.app.EvtSel               = 'NONE'
    self.setup_monitoring_service()
    self.monSvc.HaveRates         = False
    services = [self.monSvc]
    for i in adders: services.append(i)
    self.app.ExtSvc               = services
    EventLoopMgr().Warnings       = False
    self.enableUI()

def run_adder():
  import errno
  import OnlineEnvBase
  vnode = VictimNode()
  part = OnlineEnvBase.PartitionName
  try:
    #  arch = OnlineEnvBase.WorkerArchitecture
    arch = os.getenv('ARCHITECTURE')
    if not arch:
      arch = '/group/online/dataflow/options/'+part+'/Architecture.xml'
    os.stat(arch)
  except:
    print('[FATAL] Adder: Failed to access the architecture file: %s for partition: %s'%(arch, part,))
    sys.exit(errno.ENOENT)

  hostdns = False
  adder_type = os.getenv('ADDER_TYPE','1')
  print('[INFO] Adder: AdderType: %s Partition: %s Architecture: %s'%(adder_type, part, arch))

  tasklist      = TaskListfromArch(arch)
  cls,adderlist = AddersfromTasks(tasklist,adder_type,part,hostdns)
  outputLevel   = OnlineEnvBase.OutputLevel
  #if adder_type == 'HltPublisher': outputLevel = 2
  app = AdderApp(outputLevel=outputLevel,partitionName=part,adders=adderlist)
  app.config.classType = cls
# Invoke the adder application:
run_adder()
