#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
if test "${PARTITION}" = "TDET"; then
	. /group/online/dataflow/EventBuilder/EventBuilderDev/setup.x86_64_v2-centos7-gcc10-opt.vars;
elif test "${PARTITION}" = "FEST"; then
	. /group/online/dataflow/EventBuilder/EventBuilderDev/setup.x86_64_v2-centos7-gcc10-opt.vars;
else
	. /group/online/dataflow/EventBuilder/EventBuilderRelease/setup.x86_64_v2-centos7-gcc10-opt.vars;
fi;

cd ${EVENTBUILDINGROOT}/options;
`dataflow_task Class1` -opts=EB_RU.opts ${AUTO_STARTUP} ${DEBUG_STARTUP}
