#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
if [[ "$UTGID" == *"${TASK_TYPE}_0"* ]]; then
  export MBM_OUTPUT_BUFFER=Events_0;
elif [[ "$UTGID" == *"${TASK_TYPE}_1"* ]]; then
  export MBM_OUTPUT_BUFFER=Events_1;
elif [[ "$UTGID" == *"${TASK_TYPE}_2"* ]]; then
  export MBM_OUTPUT_BUFFER=Events_0;
elif [[ "$UTGID" == *"${TASK_TYPE}_3"* ]]; then
  export MBM_OUTPUT_BUFFER=Events_1;
elif [[ "$UTGID" == *"${TASK_TYPE}_4"* ]]; then
  export MBM_OUTPUT_BUFFER=Events_0;
else
  export MBM_OUTPUT_BUFFER=Events_1;
fi;
export RUN_NUMBER_SERVICE=${PARTITION}/RunInfo/RunNumber;
#
execute `dataflow_task Class2` -opts=${STATIC_OPTS}/${TASK_TYPE}.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
