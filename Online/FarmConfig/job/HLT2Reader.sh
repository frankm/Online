#!/bin/bash
# =========================================================================
#
#  Generic farm task startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#`dataflow_task Class2` -opts=/group/online/dataflow/cmtuser/OnlineRelease/TestBeam/options/MDFProd.opts
`dataflow_task Class2` -opts=../options/HLT2Reader.opts
