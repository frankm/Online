#!/bin/bash
# =========================================================================
#
#  Farm worker node controller startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
HOST=`hostname -s | tr a-z A-Z`;
HOST_LONG=`hostname -f | tr a-z A-Z`;
#
#  Check the existence of various arguments.
#  Otherwise use default values.
#
#
DEBUG_ARGS=;
#
if test -z "${LOGFIFO}"; then
    export LOGFIFO=/run/fmc/logSrv.fifo;
fi;
if test "${LOGFIFO}" = "/run/fmc/logSrv.fifo" -a -e "/dev/shm/logs.dev"; then
  export LOGFIFO=/dev/shm/logs.dev;
fi;
if test -z "${TMS_DNS}"; then
    export TMS_DNS=${HOST_LONG};
fi;
if test -z "${SMI_DNS}"; then
    export SMI_DNS=${HOST_LONG};
fi;
EB_NODE="`hostname -s | tr a-z A-Z | cut -b 3-4`";
HLT_NODE="`echo ${DIM_HOST_NODE} | tr a-z A-Z | cut -b 1-3`";
if test "${EB_NODE}" = "EB" -o "${HLT_NODE}" = "HLT"; then
    echo "ERROR:   `date` Killing left-overs..... " > /tmp/Controller.startup;
    for i in  `ps -ef | grep ${PARTITION}_${HOST}_ | grep -v grep | awk '{ print $2 }'`;
      do kill -9 $i;
    done;
    for i in  `ps -ef | grep ${UTGID}_SMI | grep -v grep | awk '{ print $2 }'`;
      do kill -9 $i;
    done;
fi;
#
if test -z "${SMI_FILE}"; then
    if test "${HLT_NODE}" = "MON"; then
        export SMI_FILE=${SMICONTROLLERROOT}/options/MonNode;
    elif test "${PARTITION}" = "LHCbA"; then
	export SMI_FILE=${SMICONTROLLERROOT}/options/AligNodeFunc;
    elif test "${PARTITION}" = "LHCb2"; then
        export SMI_FILE=${SMICONTROLLERROOT}/options/StdNode;
    elif test "${PARTITION}" = "FEST"; then
	export SMI_FILE=${SMICONTROLLERROOT}/options/StdNode;
	# DEBUG_ARGS="-debug";
    else
        export SMI_FILE=${SMICONTROLLERROOT}/options/StdNode;
    fi;
fi;
if test -z "${SMI_DOMAIN}"; then
    export SMI_DOMAIN=${PARTITION_NAME}_${HOST}_SMI;
fi;
if test -z "${DIM_DNS_NODE}"; then
    export DIM_DNS_NODE=${HOST_LONG};
fi;
SMI_DEBUG=0;
SMI_STANDALONE=1;
if test "`echo ${DIM_HOST_NODE} | tr a-z A-Z | cut -b 1-4`" = "HLT2"; then
    SMI_DEBUG=1;
    SMI_STANDALONE="3 -smiscript=${FARMCONFIGROOT}/job/Controller_SMI.sh";
    echo ${SMI_STANDALONE} > /dev/shm/logs.fifo;
fi;
#
#  DEBUG_ARGS="-debug";
#  echo "${UTGID} [ERROR] ${HOST}";
if test "`echo ${HHH} | cut -b 1-5`" = "MON01"; then
#    cd /group/online/dataflow/cmtuser/OnlineDev_v7r11;
#    . setup.x86_64_v2-centos7-gcc10-do0.vars;
    SMI_DEBUG=0;
fi;
#
make_gdb_input()  {
    echo "run"                  > /tmp/gdb_commands.txt;
    echo "thread apply all bt" >> /tmp/gdb_commands.txt;
    echo "quit"                >> /tmp/gdb_commands.txt;
    echo "y"                   >> /tmp/gdb_commands.txt;
    echo "y"                   >> /tmp/gdb_commands.txt;
    echo "y"                   >> /tmp/gdb_commands.txt;
}
#
exec -a ${UTGID} `which genRunner.exe` libSmiController.so smi_controller \
    -print=4 		           \
    -logger=fifo 	           \
    -part=${PARTITION_NAME}  	   \
    -dns=${DIM_DNS_NODE}           \
    -tmsdns=${TMS_DNS} 	           \
    -smidns=${SMI_DNS} 	           \
    -smidomain=${SMI_DOMAIN}       \
    -smidebug=${SMI_DEBUG}         \
    -smifile=${SMI_FILE}           \
    -count=${NBOFSLAVES}           \
    -service=none    	           \
    -runinfo=${RUNINFO}            \
    -taskconfig=${ARCH_FILE}       \
    "${CONTROLLER_REPLACEMENTS}"   \
    -standalone=${SMI_STANDALONE}  \
    -bindcpus=0  ${DEBUG_ARGS}
##
##
## < /tmp/gdb_commands.txt 2>&1 > /dev/shm/logs.dev;
