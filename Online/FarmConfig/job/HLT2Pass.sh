#!/bin/bash
## =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
#export BURN_CPU=150;
#export BURN_CPU=0.15;
unset BURN_CPU;
unset DEBUG;
export numEventThreads=15;
export MBM_numConnections=4;
export MBM_numEventThreads=3;
#
exec -a ${UTGID} genPython.exe `which gaudirun.py` ${FARMCONFIGROOT}/job/Passthrough.py --application=Online::OnlineEventApp;
