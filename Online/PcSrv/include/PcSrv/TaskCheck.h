//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_TASKCHECK_H
#define RPC_TASKCHECK_H

// Framework include files
#include "PcSrv/TaskDBApi.h"
#include "CPP/Interactor.h"
#include "CPP/Event.h"

// C/C++ include files
#include <mutex>
#include <chrono>
#include <memory>
#include <map>

/// Namespace for the dimrpc based implementation
namespace taskdb {

  struct TmSrvTask   {
  public:
    int pid = 0;
    std::string exe, utgid, startup, state;
  public:
    /// Default constructor
    TmSrvTask() = default;
    /// Move constructor
    TmSrvTask(TmSrvTask&& copy) = default;
    /// Copy constructor
    TmSrvTask(const TmSrvTask& copy) = default;
    /// Default destructor
    ~TmSrvTask() = default;
    /// Move assignment
    TmSrvTask& operator=(TmSrvTask&& copy) = default;
    /// Copy assignment
    TmSrvTask& operator=(const TmSrvTask& copy) = default;
  };

  ///  Task checker class
  /**
   *  Synchronize DB content with the information from the task manager
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class TaskCheck : public CPP::Interactor {
  public:
    /// Definition of the task collection type
    typedef std::vector<TaskDBApi>                 servers_t;
    typedef std::chrono::duration<int>             duration_t;
    typedef std::chrono::system_clock::time_point  time_point_t;
    typedef std::map<std::string, Task>            db_tasks_t;
    typedef std::map<std::string, TmSrvTask>       tm_tasks_t;
    struct StartInfo   {
      time_point_t last_submit = std::chrono::system_clock::from_time_t(0);
      duration_t   restart_interval       {5};
      duration_t   pause_interval       {120};
      size_t       max_restart_attempts   {5};
      size_t       restart_attempts       {0};
      size_t       hash                   {0};
      int          print                  {0};
    };

    servers_t    servers;
    db_tasks_t   m_db_tasks;
    tm_tasks_t   m_tm_tasks;
    time_point_t m_allow_tm_updates;
    time_point_t m_connectTime;
    int          m_dbReconnectTmo   = 15;
    int          m_dbCheckUpdateTmo = 30;
    int          m_tmCheckUpdateTmo = 10;
    std::mutex   m_lock;
    std::map<std::string, std::string> m_replacements;
    std::map<std::string, StartInfo>   m_start_info;
    std::string  m_dns;
    std::string  m_node;
    std::string  m_tmName;
    int          m_tmInfo       = -1;
    bool         m_inited       = false;
    bool         m_info_changed = false;
    bool         m_strict_match = false;

    /// Dim callback to retrieve the task content
    static void tmSrvInfoHandler(void* tag, void* address, int* size);
    /// Disentable the task content in the task manager
    virtual void load_tm_info(const std::vector<char>& data);
    /// Load all relevant information from the database server
    virtual void load_db_info();
    virtual void check_db_for_update();
    /// Check which tasks have to be killed to to changes in the database
    int synchronize_tm_to_db(const db_tasks_t& processes);
    /// Check which tasks have to be killed to to changes in the database
    int synchronize_tm(const db_tasks_t& processes);
    /// Kill task instance using the tmSrv
    bool kill_task(const std::string& utgid);
    /// Start task instance using the tmSrv
    bool start_task(const Task& task);

  public:
    /// Default constructor
    TaskCheck(const std::string& dns, const std::string& node);
    /// Default destructor
    virtual ~TaskCheck();
    /// Access the task container from the tmSrv service
    tm_tasks_t& tm_tasks()                {   return m_tm_tasks;  }
    /// Access the task container from the tmSrv service
    const tm_tasks_t& tm_tasks()  const   {   return m_tm_tasks;  }
    /// Access the task container from the database
    db_tasks_t& db_tasks()                {   return m_db_tasks;  }
    /// Access the task container from the database
    const db_tasks_t& db_tasks()  const   {   return m_db_tasks;  }
    /// Set strict matching
    bool setStrictMatching(bool new_value);
    /// Initalization callback
    virtual bool initialize();
    /// Shutdown callback
    virtual bool shutdown();
    /// Add submit command string replacement 
    void replaceString(const std::string& from, const std::string& to);
    /// Interactor overload: interrupt handler callback
    virtual void handle(const Event& event)  override;
    /// Handle IOV event
    virtual void handleIoc(const Event& event);
    /// Handle Time event
    virtual void handleTimer(const Event& event);
  };
}       // End namespace taskdb
#endif  // RPC_TASKCHECK_H
