//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_TASKDBOBJECTS_H
#define RPC_TASKDBOBJECTS_H

// C/C++ include files
#include <string>
#include <vector>
#include <map>

/// Namespace for the taskdb implementation
namespace taskdb {

#define DEFAULT_CTORS(X)			\
  X() = default;				\
  X(const X& copy) = default;			\
  X(X&& copy) = default;			\
  ~X() = default;				\
  X& operator= (const X& copy) = default;	\
  X& operator= (X&& copy) = default

  ///  Structure to represent a Task structure
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class Task  final   {
  public:
    /// Property name
    std::string name;
    /// Property value in its string representation
    std::string utgid;
    /// Startup command
    std::string command;
    /// Startup command parameters
    std::string command_parameters;
    /// Submit parameters
    std::string submit_parameters;
    /// Restart parameters
    std::string restart_parameters;
    /// Task description
    std::string description;
    /// Default constructors etc.
    DEFAULT_CTORS(Task);
    /// Initializing constructor
    Task(const std::string& nam, const std::string& utgid, const std::string& command);
    /// operator less to support maps/sets
    bool operator<(const Task& copy) const;
  };
  /// Initializing constructor
  inline Task::Task(const std::string& n, const std::string& u, const std::string& c)
    : name(n), utgid(u), command(c) {}
  
  ///  Structure to represent a TaskSet
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class TaskSet  final   {
  public:
    /// Property name
    std::string name;
    /// TaskSet description
    std::string description;
    /// Default constructors etc.
    DEFAULT_CTORS(TaskSet);
    /// Initializing constructor
    TaskSet(const std::string& nam, const std::string& description="");
    /// operator less to support maps/sets
    bool operator<(const TaskSet& copy) const;
  };
  /// Initializing constructor
  inline TaskSet::TaskSet(const std::string& n, const std::string& d)
    : name(n), description(d) {}
  
  ///  Structure to represent a NodeClass
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class NodeClass  final   {
  public:
    /// Property name
    std::string name;
    /// NodeClass description
    std::string description;
    /// Default constructors etc.
    DEFAULT_CTORS(NodeClass);
    /// Initializing constructor
    NodeClass(const std::string& nam, const std::string& description="");
    /// operator less to support maps/sets
    bool operator<(const NodeClass& copy) const;
  };
  /// Initializing constructor
  inline NodeClass::NodeClass(const std::string& n, const std::string& d)
    : name(n), description(d) {}

  ///  Structure to represent a NodeType
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class NodeType  final   {
  public:
    /// Property name
    std::string name;
    /// NodeType priority
    std::string priority;
    /// NodeType description
    std::string description;
    /// Default constructors etc.
    DEFAULT_CTORS(NodeType);
    /// Initializing constructor
    NodeType(const std::string& name, const std::string& description);
    /// operator less to support maps/sets
    bool operator<(const NodeType& copy) const;
  };
  /// Initializing constructor
  inline NodeType::NodeType(const std::string& n, const std::string& d)
    : name(n), description(d) {}
  
  ///  Structure to represent a TasksInSet link
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class TasksInSet  final   {
  public:
    /// Task name
    std::string name;
    /// Default constructors etc.
    DEFAULT_CTORS(TasksInSet);
    /// Initializing constructor
    TasksInSet(const std::string& name);
    /// operator less to support maps/sets
    bool operator < (const TasksInSet& copy) const;
  };
  /// Initializing constructor
  inline TasksInSet::TasksInSet(const std::string& n) : name(n) {}

  ///  Structure to represent a SetsInClass link
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class SetsInClass  final   {
  public:
    /// Task name
    std::string name;
    /// Default constructors etc.
    DEFAULT_CTORS(SetsInClass);
    /// Initializing constructor
    SetsInClass(const std::string& name);
    /// operator less to support maps/sets
    bool operator < (const SetsInClass& copy) const;
  };
  /// Initializing constructor
  inline SetsInClass::SetsInClass(const std::string& n) : name(n) {}

  ///  Structure to represent a ClassesInNode link
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class ClassesInNode  final   {
  public:
    /// Task name
    std::string name;
    /// Default constructors etc.
    DEFAULT_CTORS(ClassesInNode);
    /// Initializing constructor
    ClassesInNode(const std::string& name);
    /// operator less to support maps/sets
    bool operator < (const ClassesInNode& copy) const;
  };
  /// Initializing constructor
  inline ClassesInNode::ClassesInNode(const std::string& n) : name(n) {}

  ///  Structure holding a database snapshot
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class Snapshot  final   {
  public:
    int timeStamp;
    typedef std::pair<Task,      std::string>  Tsk;
    typedef std::pair<TaskSet,   std::vector<TasksInSet> > Set;
    typedef std::pair<NodeClass, std::vector<SetsInClass> > Class;
    typedef std::pair<NodeType,  std::vector<ClassesInNode> >  Type;
    std::map<std::string, Tsk>  tasks;
    std::map<std::string, Set>   sets;
    std::map<std::string, Class> classes;
    std::map<std::string, Type>  types;

    /// Default constructor
    Snapshot() = default;
    /// Move constructor
    Snapshot(Snapshot&& c) = default;
    /// Copy constructor
    Snapshot(const Snapshot& c) = default;
    /// Move assignment
    Snapshot& operator=(Snapshot&& c) = default;
    /// Copy assignment
    Snapshot& operator=(const Snapshot& c) = default;
  };
}       // End namespace taskdb

std::ostream& operator<< (std::ostream& os, const taskdb::Task& tasks);
std::ostream& operator<< (std::ostream& os, const taskdb::TaskSet& obj);
std::ostream& operator<< (std::ostream& os, const taskdb::NodeClass& obj);
std::ostream& operator<< (std::ostream& os, const taskdb::NodeType& obj);

std::ostream& operator<< (std::ostream& os, const std::vector<taskdb::Task>& tasks);
std::ostream& operator<< (std::ostream& os, const std::vector<taskdb::TaskSet>& obj);
std::ostream& operator<< (std::ostream& os, const std::vector<taskdb::NodeClass>& obj);
std::ostream& operator<< (std::ostream& os, const std::vector<taskdb::NodeType>& obj);

std::ostream& operator<< (std::ostream& os, const std::vector<taskdb::TasksInSet>& obj);
std::ostream& operator<< (std::ostream& os, const std::vector<taskdb::SetsInClass>& obj);
std::ostream& operator<< (std::ostream& os, const std::vector<taskdb::ClassesInNode>& obj);
std::ostream& operator<< (std::ostream& os, const taskdb::Snapshot& snapshot);

#endif  // RPC_TASKDBOBJECTS_H
