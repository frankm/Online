#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/PcSrv
------------
#]=======================================================================]

online_library(PcSrvLib
        src/CLI.cpp
        src/CacheDBApi.cpp
        src/CacheDBSrv.cpp
        src/Task.cpp
        src/TaskCheck.cpp
        src/TaskDBApi.cpp
        src/Utils.cpp
        src/tests.cpp)
# Hide some warnings
target_compile_options(PcSrvLib PRIVATE -Wno-keyword-macro)
target_link_libraries(PcSrvLib
 PUBLIC     Online::OnlineBase Online::RPCServer
 PRIVATE    Online::dim Online::HTTP
)
#
#===============================================================================
online_install_includes(include)
online_install_scripts(scripts)
