#!/bin/bash
export UTGID
#
export HOST=`hostname -s`
#
export PATH=/group/online/dataflow/cmtuser/libraries/Miniconda/bin:$PATH;
export PYTHONHOME=/group/online/dataflow/cmtuser/libraries/Miniconda;
/group/online/dataflow/cmtuser/libraries/Miniconda/bin/activate;
#
cd /group/online/dataflow/cmtuser/TaskDB/Online/TaskDB/python;
eval `python TaskDB/Setup.py`
#
exec -a ${UTGID} python TaskDB/Server.py \
    --hostname=${HOST} --port=3501 \
    --source=http://ecs03.lbdaq.cern.ch:3500/TDBDATA/XMLRPC \
    --cache --multi-threaded=5 \
    --type=rpc --run
