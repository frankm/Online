#!/bin/bash
export UTGID
#
#
export HOST=`hostname -s`
#
export PATH=/group/online/dataflow/cmtuser/libraries/Miniconda/bin:$PATH;
export PYTHONHOME=/group/online/dataflow/cmtuser/libraries/Miniconda;
/group/online/dataflow/cmtuser/libraries/Miniconda/bin/activate;
#
cd /group/online/dataflow/cmtuser/TaskDB/Online/TaskDB;
eval `python3.6 python/TaskDB/Setup.py`
#
exec -a ${UTGID} python3.6 python/TaskDB/Server.py \
    --hostname=${HOST} --port=3500 --source=sqlite:///LHCbOnline.db --run --type=json
