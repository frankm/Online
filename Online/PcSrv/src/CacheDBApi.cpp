//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : PcSrv
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <PcSrv/CacheDBApi.h>
#include <HTTP/Compress.h>
#include <RPC/XMLRPC.h>

/// C/C++ include files
#include <iostream>
#include <sstream>

using namespace std;
using namespace xmlrpc;
using namespace taskdb;

typedef lock_guard<mutex> Lock;

/// Initializing constructor
CacheDBApi::CacheDBApi()
{
}

/// Default destructor
CacheDBApi::~CacheDBApi()   {
}

void CacheDBApi::check()   const   {
  if ( !m_snapshot.get() || m_blob.empty() )   {
    ++m_num_errs;
    throw runtime_error("Could not yet contact the server for database copy. Try later again.");
  }
}
#include <iomanip>
/// Attach snapshot object
void CacheDBApi::setSnapShot(string&& data)   {
  try {
    vector<unsigned char> xml(http::compress::decompress("gzip", std::move(http::compress::base64_decode(data))));
    dd4hep::xml::DocumentHolder doc(dd4hep::xml::DocumentHandler().parse((char*)&xml[0],xml.size()));
    xml_h root = doc.root().child(_rpcU(param)).child(_rpcU(value));
    unique_ptr<Snapshot> s(new Snapshot(xmlrpc::XmlCoder(root).to<taskdb::Snapshot>()));
    Lock lock(m_lock);
    m_regexCache.clear();
    m_blob = std::move(data);
    m_snapshot.reset(s.release());
    m_connectTime = chrono::system_clock::now();
  }
  catch(const exception& e)    {
    vector<unsigned char> xml(http::compress::decompress("gzip", std::move(http::compress::base64_decode(data))));
    cout << "XML Record [exception]:" << e.what() << endl;
    cout << (char*)&xml[0] << flush << endl << flush;
  }
}

/// Access the decompressed snapshot
const Snapshot& CacheDBApi::snapshot()   const    {
  Lock lock(m_lock);
  check();
  return *m_snapshot;
}


/// Retrieve the database cache (compressed and base64 encoded)
string CacheDBApi::getCache(long /* stamp */)  const   {
  Lock lock(m_lock);
  check();
  ++m_num_calls;
  return m_blob;
}

/// Access TaskDB to retrieve the tasks for this node
CacheDBApi::tasks_t CacheDBApi::getTasksByNode(const string& node)  const  {
  tasks_t result;
  if ( !node.empty() )   {
    map<long, string> matches;
    Lock lock(m_lock);
    check();
    ++m_num_calls;
    for(const auto& t : m_snapshot->types)   {
      regex_cache_t::iterator i = m_regexCache.find(t.first);
      if ( i == m_regexCache.end() )  {
	i = m_regexCache.insert(make_pair(t.first,make_unique<regex>(t.first))).first;
      }
      if ( regex_match (node, *((*i).second)))
	matches.insert(make_pair(::atol(t.second.first.priority.c_str()), t.second.first.name));
    }
    if ( !matches.empty() )  {
      const string& n = (*matches.rbegin()).second;
      for(const auto& t : m_snapshot->types)   {
	if ( t.first == n )   {
	  for(const auto& c : t.second.second)   {
	    const auto& cls_sets = m_snapshot->classes[c.name].second;
	    for(const auto& cs : cls_sets)   {
	      const auto& tsk_sets = m_snapshot->sets[cs.name].second;
	      for(const auto& ts : tsk_sets)    {
		result.push_back(m_snapshot->tasks[ts.name].first);
	      }
	    }
	  }
	}
      }
    }
  }
  return result;
}

/// Access TaskDB to add a new task
int CacheDBApi::addTask(const string& /* name */ ,
			const string& /* utgid */ ,
			const string& /* command */ ,
			const string& /* task_parameters */ ,
			const string& /* command_parameters */ ,
			const string& /* restart_parameters */ ,
			const string& /* description */)   const   
{
  ++m_num_errs;
  throw runtime_error("CacheDBApi::addTask(...) is not supported for a readonly taskdb cache service!");
}

/// Access TaskDB to retrieve matching tasks
CacheDBApi::tasks_t CacheDBApi::getTask(const string& match)  const  {
  tasks_t result;
  if ( !match.empty() )   {
    bool star = (match == "*");
    Lock lock(m_lock);
    check();
    ++m_num_calls;
    if ( star )   {
      for(const auto& t : m_snapshot->tasks)
	result.push_back(t.second.first);
      return result;
    }
    for(const auto& t : m_snapshot->tasks)   {
      if ( t.first == match )  {
	result.push_back(t.second.first);
	break;
      }
    }
  }
  return result;
}

/// Access TaskDB to delete matching tasks
int CacheDBApi::deleteTask(const string& /* match */)  const  {
  ++m_num_errs;
  throw runtime_error("CacheDBApi::deleteTask(...) is not supported for a readonly taskdb cache service!");
}

/// Add new task set to the database
int CacheDBApi::addSet(const string& /* name */,
		       const string& /* description */)   const   {
  ++m_num_errs;
  throw runtime_error("CacheDBApi::addSet(...) is not supported for a readonly taskdb cache service!");
}

/// Delete task set from the database
int CacheDBApi::deleteSet(const string& /* name */)   const   {
  ++m_num_errs;
  throw runtime_error("CacheDBApi::deleteSet(...) is not supported for a readonly taskdb cache service!");
}

/// Access TaskDB to retrieve matching task sets
CacheDBApi::sets_t CacheDBApi::getSet(const string& match)   const   {
  sets_t  result;
  if ( !match.empty() )   {
    bool star = (match == "*");
    Lock lock(m_lock);
    check();
    ++m_num_calls;
    if ( star )   {
      for(const auto& s : m_snapshot->sets)
	result.push_back(s.second.first);
      return result;
    }
    for(const auto& s : m_snapshot->sets)   {
      if ( s.first == match )  {
	result.push_back(s.second.first);
	break;
      }
    }
  }
  return result;
}

/// Access TaskDB to retrieve matching task in a given task set
CacheDBApi::tasks_in_set_t CacheDBApi::tasksInSet(const string& match)   const   {
  if ( !match.empty() )   {
    Lock lock(m_lock);
    check();
    auto i = m_snapshot->sets.find(match);
    if ( i != m_snapshot->sets.end() )
      return (*i).second.second;
  }
  ++m_num_errs;
  throw runtime_error("Taskset '"+match+"' is not present in the database.");
}

/// Add new node class to the database
int CacheDBApi::addClass(const string& /* name */,
			 const string& /* description */)   const   {
  ++m_num_errs;
  throw runtime_error("CacheDBApi::addClass(...) is not supported for a readonly taskdb cache service!");
}

/// Delete node class from the database
int CacheDBApi::deleteClass(const string& /* name */)   const   {
  ++m_num_errs;
  throw runtime_error("CacheDBApi::deleteClass(...) is not supported for a readonly taskdb cache service!");
}

/// Access TaskDB to retrieve matching node classes
CacheDBApi::classes_t CacheDBApi::getClass(const string& match)   const   {
  classes_t result;
  if ( !match.empty() )   {
    bool star = (match == "*");
    Lock lock(m_lock);
    check();
    ++m_num_calls;
    if ( star )   {
      for(const auto& c : m_snapshot->classes)
	result.push_back(c.second.first);
      return result;
    }
    for(const auto& c : m_snapshot->classes)   {
      if ( c.first == match )  {
	result.push_back(c.second.first);
	break;
      }
    }
  }
  return result;
}

/// Access TaskDB to retrieve matching task in a given task set
CacheDBApi::sets_in_class_t CacheDBApi::taskSetsInClass(const string& match)   const  {
  if ( !match.empty() )   {
    Lock lock(m_lock);
    check();
    auto i = m_snapshot->classes.find(match);
    if ( i != m_snapshot->classes.end() )
      return (*i).second.second;
  }
  ++m_num_errs;
  throw runtime_error("NodeClass '"+match+"' is not present in the database.");
}

/// Add new node type to the database
int CacheDBApi::addNode(const string&    name,
			int           /* priority */,
			const string& /* description */)   const   {
  ++m_num_errs;
  throw runtime_error("CacheDBApi::addNode("+name+",...) is not supported for a readonly taskdb cache service!");
}

/// Delete node type from the database
int CacheDBApi::deleteNode(const string& name)   const   {
  ++m_num_errs;
  throw runtime_error("CacheDBApi::deleteNode("+name+") is not supported for a readonly taskdb cache service!");
}

/// Access TaskDB to retrieve matching node types
CacheDBApi::nodes_t  CacheDBApi::getNode(const string& match)   const   {
  nodes_t result;
  if ( !match.empty() )   {
    bool star = (match == "*");
    Lock lock(m_lock);
    check();
    ++m_num_calls;
    if ( star )   {
      for(const auto& n : m_snapshot->types)
	result.push_back(n.second.first);
      return result;
    }
    for(const auto& n : m_snapshot->types)   {
      if ( n.first == match )  {
	result.push_back(n.second.first);
	break;
      }
    }
  }
  return result;
}

/// Access TaskDB to retrieve matching task in a given task set
CacheDBApi::classes_in_node_t CacheDBApi::classesInNode(const string& match)   const  {
  if ( !match.empty() )   {
    Lock lock(m_lock);
    check();
    ++m_num_calls;
    auto i = m_snapshot->types.find(match);
    if ( i != m_snapshot->types.end() )
      return (*i).second.second;
  }
  ++m_num_errs;
  throw runtime_error("NodeType '"+match+"' is not present in the database.");
}

/// Access TaskDB to check if a database update is required
bool CacheDBApi::needDbUpdate(const string& host, int port, long stamp)   const  {
  Lock lock(m_lock);
  ++m_num_calls;
  if ( !host.empty() && port && stamp )   {
    time_t last  = chrono::system_clock::to_time_t(m_connectTime);
    if ( stamp < last ) return true;
  }
  return false;
}
