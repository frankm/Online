//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// C/C++ include files
#include <sstream>
#include <iostream>
#include <stdexcept>

// Framework include files
#include "PcSrv/Task.h"
#include "PcSrv/TaskDBApi.h"
#include "HTTP/HttpClient.h"
#include "RPC/XMLRPC.h"
#include "RTL/rtl.h"

using namespace std;

namespace {
  ostream& logger(const string& prefix="")  {
    return cout << prefix;
  }

  unique_ptr<rpc::RpcClientBase> get_connection(int argc, char** argv)   {
    string port  = "3500";
    string mount = "/TDBDATA/XMLRPC";
    string host  = "ecs03.lbdaq.cern.ch";
    RTL::CLI cli(argc, argv, [] (int, char**)   {
	cout << "    -host=<host-name>   TaskDB server host name   \n"
	     << "    -port=<port-number> TaskDB server port        \n"
	     << "    -mount=<mount-name> Web server directory name \n"
	     << endl;
      });
    cli.getopt("port",  3, port);
    cli.getopt("host",  3, host);
    cli.getopt("mount", 3, mount);
    return make_unique<rpc::RpcClient<http::HttpClient> >(host, mount, port);
  }

  size_t dump_objects(const taskdb::TaskDBApi::tasks_t& tasks)   {
    for( const auto& t : tasks )    {
      logger() << "Task: name:        " << t.name << endl;
      logger() << "      utgid:       " << t.utgid << endl;
      logger() << "      command:     " << t.command << endl;
      logger() << "      params:      " << t.command_parameters << endl;
      logger() << "      submit:      " << t.submit_parameters << endl;
      logger() << "      restart:     " << t.restart_parameters << endl;
      logger() << "      description: " << t.description << endl;
    }
    logger()   << "Got a total of " << tasks.size() << " tasks." << endl;
    return tasks.size();
  }
  size_t dump_objects(const vector<taskdb::TaskSet>& obj)   {
    for( const auto& o : obj )    {
      logger() << "TaskSet: name:     " << o.name << endl;
      logger() << "      description: " << o.description << endl;
    }
    logger()   << "Got a total of " << obj.size() << " task sets." << endl;
    return obj.size();
  }
  size_t dump_objects(const vector<taskdb::NodeClass>& obj)   {
    for( const auto& o : obj )    {
      logger() << "Node class: name:  " << o.name << endl;
      logger() << "      description: " << o.description << endl;
    }
    logger()   << "Got a total of " << obj.size() << " node classes." << endl;
    return obj.size();
  }
  size_t dump_objects(const vector<taskdb::NodeType>& obj)   {
    for( const auto& o : obj )    {
      logger() << "Node type: name:   " << o.name << endl;
      logger() << "      description: " << o.description << endl;
    }
    logger()   << "Got a total of " << obj.size() << " node types." << endl;
    return obj.size();
  }
  template <typename T> size_t dump_links(const vector<T>& obj)   {
    for( const auto& o : obj )    {
      logger() << "Link name:     " << o.name << endl;
    }
    logger()   << "Got a total of " << obj.size() << " links." << endl;
    return obj.size();
  }
  int check_result(int res)   {
    logger() << ((res == 1) ? "SUCCESS" : "FAILED") << endl;
    return res;
  }
}


extern "C" int taskdb_test(int argc, char** argv)   {
  RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
      cout << "  taskdb_classes_in_node -opt [-opt]                    \n";
      get_connection(ac, av);
      ::exit(0);
    });
  taskdb::TaskDBApi api(get_connection(argc,argv));
  auto result = api.test();
  return result;
}

extern "C" int taskdb_tasks_by_node(int argc, char** argv)   {
  string node  = "hlta1120";
  RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
      cout << "  taskdb_tasks_by_node -opt [-opt]                \n";
      cout << "    -node=<node-name>   Node name to query tasks  \n";
      get_connection(ac, av);
      ::exit(0);
    });
  cli.getopt("node",   3, node);
  taskdb::TaskDBApi api(get_connection(argc,argv));
  auto tasks = api.getTasksByNode(node);
  dump_objects(tasks);
  return 1;
}

extern "C" int taskdb_task(int argc, char** argv)   {
  string match  = "*";
  RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
      cout << "  taskdb_task -opt [-opt]                               \n";
      cout << "    -match=<match-name>   Match task name to query tasks \n";
      get_connection(ac, av);
      ::exit(0);
    });
  cli.getopt("match",  3, match);
  taskdb::TaskDBApi api(get_connection(argc,argv));
  auto tasks = api.getTask(match);
  dump_objects(tasks);
  return 1;
}

extern "C" int taskdb_delete_task(int argc, char** argv)   {
  string match  = "*";
  RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
      cout << "  taskdb_delete_task -opt [-opt]                               \n";
      cout << "    -match=<match-name>   Match task name to query tasks \n";
      get_connection(ac, av);
      ::exit(0);
    });
  cli.getopt("match",  3, match);
  taskdb::TaskDBApi api(get_connection(argc,argv));
  auto result = api.deleteTask(match);
  return check_result(result);
}

extern "C" int taskdb_set(int argc, char** argv)   {
  string match  = "*";
  RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
      cout << "  taskdb_set -opt [-opt]                               \n";
      cout << "    -match=<match-name>   Match name to query sets \n";
      get_connection(ac, av);
      ::exit(0);
    });
  cli.getopt("match",  3, match);
  taskdb::TaskDBApi api(get_connection(argc,argv));
  auto result = api.getSet(match);
  dump_objects(result);
  return 1;
}

extern "C" int taskdb_tasks_in_set(int argc, char** argv)   {
  string match  = "*";
  RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
      cout << "  taskdb_tasks_in_set -opt [-opt]                  \n";
      cout << "    -match=<match-name>   Match name to query sets \n";
      get_connection(ac, av);
      ::exit(0);
    });
  cli.getopt("match",  3, match);
  taskdb::TaskDBApi api(get_connection(argc,argv));
  auto result = api.tasksInSet(match);
  dump_links(result);
  return 1;
}

extern "C" int taskdb_class(int argc, char** argv)   {
  string match  = "*";
  RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
      cout << "  taskdb_class -opt [-opt]                               \n";
      cout << "    -match=<match-name>   Match name to query classes.   \n";
      get_connection(ac, av);
      ::exit(0);
    });
  cli.getopt("match",  3, match);
  taskdb::TaskDBApi api(get_connection(argc,argv));
  auto result = api.getClass(match);
  dump_objects(result);
  return 1;
}

extern "C" int taskdb_tasksets_in_class(int argc, char** argv)   {
  string match  = "*";
  RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
      cout << "  taskdb_tasksets_in_class -opt [-opt]                      \n";
      cout << "    -match=<match-name>   Match name to query classes   \n";
      get_connection(ac, av);
      ::exit(0);
    });
  cli.getopt("match",  3, match);
  taskdb::TaskDBApi api(get_connection(argc,argv));
  auto result = api.taskSetsInClass(match);
  dump_links(result);
  return 1;
}

extern "C" int taskdb_node(int argc, char** argv)   {
  string match  = "*";
  RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
      cout << "  taskdb_node -opt [-opt]                               \n";
      cout << "    -match=<match-name>   Match name to query nodes.   \n";
      get_connection(ac, av);
      ::exit(0);
    });
  cli.getopt("match",  3, match);
  taskdb::TaskDBApi api(get_connection(argc,argv));
  auto result = api.getNode(match);
  dump_objects(result);
  return 1;
}

extern "C" int taskdb_classes_in_node(int argc, char** argv)   {
  string match  = "*";
  RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
      cout << "  taskdb_classes_in_node -opt [-opt]                    \n";
      cout << "    -match=<match-name>   Match name to query classes   \n";
      get_connection(ac, av);
      ::exit(0);
    });
  cli.getopt("match",  3, match);
  taskdb::TaskDBApi api(get_connection(argc,argv));
  auto result = api.classesInNode(match);
  dump_links(result);
  return 1;
}

extern "C" int taskdb_need_db_update(int argc, char** argv)   {
  string match  = "*", host;
  long time_stamp = 1;
  int port = 0;
  RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
      cout << "  taskdb_need_db_update -opt [-opt]                    \n";
      get_connection(ac, av);
      ::exit(0);
    });
  
  cli.getopt("port",  3, port);
  cli.getopt("node",  3, host);
  if ( cli.getopt("now", 3) != 0 ) time_stamp = ::time(0);
  taskdb::TaskDBApi api(get_connection(argc,argv));
  auto result = api.needDbUpdate(host, port, time_stamp);
  cout << "need_db_update: " << result << endl;
  return 1;
}
