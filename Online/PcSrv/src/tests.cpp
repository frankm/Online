//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// C/C++ include files
#include <sstream>
#include <iostream>
#include <memory>

// Framework include files
#include <PcSrv/Task.h>
#include <PcSrv/TaskDBApi.h>
#include <HTTP/HttpClient.h>
#include <RPC/RpcClientHandle.h>
#include <RPC/XMLRPC.h>
#include <RTL/rtl.h>

using namespace std;

namespace {
  int s_test_count = 0;
  void decodeArray(const string& prefix, xml_elt_t e);
  void decodeStruct(const string& prefix, xml_elt_t e);

  ostream& logger(const string& prefix="")  {
    time_t t = ::time(0);
    struct tm tm;
    ::localtime_r(&t, &tm);
    char buff[124];
    ::strftime(buff,sizeof(buff),"%H:%M:%S %d-%m-%Y ",&tm);
    return cout << buff << prefix;
  }

  /// Decode xmlrpc-data iteratively to primitives
  void decodeItem(const string& prefix, xml_elt_t elt)  {
    string tag = elt.tag();
    if ( tag == "int" || tag == "i4" )  {
      logger(prefix) << "(int)    " << elt.text() << endl;
      ++s_test_count;
    }
    else if ( tag == "double" )   {
      logger(prefix) << "(double) " << elt.text() << endl;
      ++s_test_count;
    }
    else if ( tag == "string" )   {
      logger(prefix) << "(string) " << elt.text() << endl;
      ++s_test_count;
    }
    else if ( tag == "boolean" )   {
      logger(prefix) << "(bool)   " << elt.text() << endl;
      ++s_test_count;
    }
    else if ( tag == "struct" )   {
      decodeStruct(prefix+"(struct)",elt);
      ++s_test_count;
    }
    else if ( tag == "array" )   {
      decodeArray(prefix+"(array)",elt);
      ++s_test_count;
    }
    else   {
      logger(prefix) << "UNKNOWN data type: " << tag << endl;
    }
  }

  /// Decode xmlrpc-array to values
  void decodeArray(const string& prefix, xml_elt_t e)  {
    for(xml_coll_t data(e,_rpcU(data)); data; ++data)   {
      int count = 0;
      stringstream str;
      for(xml_coll_t value(data,_rpcU(value)); value; ++value)   {
	for(xml_coll_t type(value,_U(star)); type; ++type)   {
	  str.str("");
	  str << prefix << "[" << count << "]";
	  decodeItem(str.str(),type);
	  ++count;
	}
      }
      return; // According to the standard exactly 1 data element is allowed!
    }
  }

  /// Decode xmlrpc-structure to values
  void decodeStruct(const string& prefix, xml_elt_t structure)  {
    for(xml_coll_t m(structure,_rpcU(member)); m; ++m)   {
      xml_elt_t n = xml_elt_t(m).child(_U(name));
      xml_elt_t v = xml_elt_t(m).child(_U(value));
      // According to the standard exactly 1 data element is allowed!
      decodeItem(prefix+"."+n.text(),xml_coll_t(v,_U(star)));
    }
  }
}

extern "C" int test_response_decoding(int, char**)   {
  const char* response = 
    "<?xml version='1.0'?>\n"
    "<methodResponse>\n"
    "  <params>\n"
    "    <param>\n"
    "      <value>\n"
    "        <array>\n"
    "          <data>\n"

    "            <value>\n"
    "              <struct>\n"
    "                <member>\n"
    "                  <name>task</name>\n"
    "                  <value><string>TorrentLoader</string></value>\n"
    "                </member>\n"
    "                <member>\n"
    "                  <name>utgid</name>\n"
    "                  <value><string>TorrentLoader</string></value>\n"
    "                </member>\n"
    "                <member>\n"
    "                  <name>command</name>\n"
    "                  <value><string>&lt;SCRIPTS&gt;/BitTorrentLoader.sh</string></value>\n"
    "                </member>\n"
    "                <member>\n"
    "                  <name>task_parameters</name>\n"
    "                  <value><string>--no-authentication -K 120 -M 5 -g onliners -p 0 -n online</string></value>\n"
    "                </member>\n"
    "                <member>\n"
    "                  <name>command_parameters</name>\n"
    "                  <value><string></string></value>\n"
    "                </member>\n"
    "                <member>\n"
    "                  <name>description</name>\n"
    "                  <value><string></string></value>\n"
    "                </member>\n"
    "              </struct>\n"
    "            </value>\n"

    "            <value>\n"
    "              <struct>\n"
    "                <member>\n"
    "                  <name>task</name>\n"
    "                  <value><string>Test_Task</string></value>\n"
    "                </member>\n"
    "                <member>\n"
    "                  <name>utgid</name>\n"
    "                  <value><string>Test_Task</string></value>\n"
    "                </member>\n"
    "                <member>\n"
    "                  <name>command</name>\n"
    "                  <value><string>&lt;SCRIPTS&gt;/Test_Task.sh</string></value>\n"
    "                </member>\n"
    "                <member>\n"
    "                  <name>task_parameters</name>\n"
    "                  <value><string>--no-authentication -K 120 -M 5 -g onliners -p 0 -n online</string></value>\n"
    "                </member>\n"
    "                <member>\n"
    "                  <name>command_parameters</name>\n"
    "                  <value><string></string></value>\n"
    "                </member>\n"
    "                <member>\n"
    "                  <name>description</name>\n"
    "                  <value><string></string></value>\n"
    "                </member>\n"
    "              </struct>\n"
    "            </value>\n"

    "          </data>\n"
    "        </array>\n"
    "      </value>\n"
    "    </param>\n"
    "  </params>\n"
    "</methodResponse>\n";
  cout << response << endl;
  xmlrpc::MethodResponse doc = xmlrpc::MethodResponse::decode(response);
  decodeItem("value",doc.value);
  logger() << "Score: " << s_test_count << endl;
  return 1;
}

extern "C" int pcsrv_test_read_snapshot(int argc, char** argv)    {
  string fname;
  RTL::CLI cli(argc, argv, 0);
  cli.getopt("input", 1, fname);
  
  dd4hep::xml::DocumentHolder doc(dd4hep::xml::DocumentHandler().load(fname));
  xml_h root = doc.root().child(_rpcU(param)).child(_rpcU(value));
  taskdb::Snapshot s = xmlrpc::XmlCoder(root).to<taskdb::Snapshot>();
  cout << s << endl;
  return 1;
}

extern "C" int run_pcsrv_test_taskdb_api(int argc, char** argv)   {
  using namespace std;
  using namespace taskdb;

  string node, dns;
  string port  = "3500";
  string mount = "/TDBDATA/XMLRPC";
  string server  = "ecs03.lbdaq.cern.ch";
  RTL::CLI cli(argc, argv, [] (int, char**)  {
      cout << " run_task_check  -opt [-opt]                           \n";
      cout << "    -dns=<node-name>         DNS node name             \n";
      cout << "    -node=<node-name>        Node name to query tasks  \n";
      cout << "    -server=<server-name>    TaskDB server host name   \n"
	   << "    -port=<port-number>      TaskDB server port        \n"
	   << "    -mount=<mount-name>      Web server directory name \n"
	   << endl;
      ::exit(0);
    });
  cli.getopt("dns",     3, dns);
  cli.getopt("node",    3, node);
  cli.getopt("port",    3, port);
  cli.getopt("server",  3, server);
  cli.getopt("mount",   3, mount);

  auto cl = rpc::client<http::HttpClient>(server, mount, port);
  cl->setDebug(0);
  cl->imp.userRequestHeaders.push_back(http::HttpHeader("Accept-Encoding","gzip"));

  TaskDBApi api(std::move(cl));
  api.test();

  TaskDBApi::tasks_t tasks = api.getTasksByNode(node);
  cout << "api.getTasksByNode(" << node << ") Got a total of " << tasks.size() << " tasks" << endl;

  tasks = api.getTask("LogDefaultSrv");
  cout << "api.getTask(\"LogDefaultSrv\") " << tasks;

  TaskDBApi::sets_t  sets = api.getSet("SliceCollectors_RECO");
  cout << "api.getSet(\"SliceCollectors_RECO\") " << sets;

  TaskDBApi::classes_t  classes = api.getClass("ReconstructionControlsPC");
  cout << "api.getSet(\"ReconstructionControlsPC\") " << classes;

  TaskDBApi::nodes_t  nodes = api.getNode("hlt[a-m][0-9][0-9]");
  cout << "api.getNode(\"hlt[a-m][0-9][0-9]\") " << nodes;

  //taskdb::Snapshot snap(std::move(api.getCache()));
  auto cache = api.getCache(time(0));
  if ( cache.size() > 128 ) cache[128] = 0;
  cout << "api.getCacheRaw(" << cache.size() << " bytes) : " << endl;
  cout << (char*)&cache[0];
  if ( cache.size() > 128 ) cout << "...";
  cout << endl;
  return 1;
}

#include <chrono>
#include <climits>
extern "C" int run_pcsrv_hammer_taskdb_api(int argc, char** argv)   {
  using namespace std;
  using namespace taskdb;
  int    rounds = INT_MAX;
  string node, dns;
  string port  = "3500";
  string mount = "/TDBDATA/XMLRPC";
  string server  = "ecs03.lbdaq.cern.ch";
  RTL::CLI cli(argc, argv, [] (int, char**)  {
      cout << " run_task_check  -opt [-opt]                           \n";
      cout << "    -dns=<node-name>         DNS node name             \n";
      cout << "    -node=<node-name>        Node name to query tasks  \n";
      cout << "    -server=<server-name>    TaskDB server host name   \n"
	   << "    -port=<port-number>      TaskDB server port        \n"
	   << "    -mount=<mount-name>      Web server directory name \n"
	   << endl;
      ::exit(0);
    });
  cli.getopt("dns",     3, dns);
  cli.getopt("node",    3, node);
  cli.getopt("port",    3, port);
  cli.getopt("server",  3, server);
  cli.getopt("mount",   3, mount);
  cli.getopt("rounds",  3, rounds);

  auto start = std::chrono::system_clock::now(), last = start;
  size_t counts = 0;
  TaskDBApi api;
  while ( --rounds > 0 )  {
    try {
      auto cl = rpc::client<http::HttpClient>(server, mount, port);
      (*cl)->setDebug(0);
      (*cl)->userRequestHeaders.push_back(http::HttpHeader("Accept-Encoding","gzip"));
      api.handler = std::move(cl);

      TaskDBApi::tasks_t tasks = api.getTasksByNode(node);
      if ( tasks.empty() )  {
	cout << "ERROR: api.getTasksByNode(" << node << ") Got a total of " << tasks.size() << " tasks" << endl;
      }
      tasks = api.getTask("LogDefaultSrv");
      if ( tasks.empty() )  {
	cout << "ERROR: api.getTask(\"LogDefaultSrv\") returned nothing!" << tasks;
      }
      TaskDBApi::sets_t  sets = api.getSet("SliceCollectors_RECO");
      if ( sets.empty() )   {
	cout << "ERROR: api.getSet(\"SliceCollectors_RECO\")  returned nothing!" << sets;
      }
      TaskDBApi::classes_t  classes = api.getClass("ReconstructionControlsPC");
      if ( classes.empty() )   {
	cout << "ERROR: api.getSet(\"ReconstructionControlsPC\")  returned nothing!" << classes;
      }
      TaskDBApi::nodes_t  nodes = api.getNode("hlt[a-m][0-9][0-9]");
      if ( nodes.empty() )    {
	cout << "ERROR: api.getNode(\"hlt[a-m][0-9][0-9]\")   returned nothing!" << nodes;
      }
      //taskdb::Snapshot snap(std::move(api.getCache()));
      auto cache = api.getCache(time(0));
      if ( cache.empty() )   {
	cout << "ERROR: api.getCacheRaw(" << cache.size() << " bytes) :  returned nothing!" << endl;
      }
      if ( (rounds%1000) == 0 )  {
	auto now = std::chrono::system_clock::now();
	long diff1  = std::chrono::duration_cast<std::chrono::milliseconds>(now-start).count();
	long diff2  = std::chrono::duration_cast<std::chrono::milliseconds>(now-last).count();
	printf("++ %8ld requests in %6ld sec delta:%6ld msec avg:%5.0f Hz instantaneous:%5.0f Hz\n",
	       api.numberOfCalls(), diff1/1000, diff2,
	       double(api.numberOfCalls())/double(diff1+1)*1000.0,
	       double(api.numberOfCalls()-counts)/double(diff2+1)*1000.0);
	fflush(stdout);
	counts = api.numberOfCalls();
	last = now;
      }
    }
    catch(const exception& e)   {
      cout << "ERROR: Processing exception: " << e.what() << endl;
    }
  }
  return 1;
}

extern "C" int run_pcsrv_hammer_taskdb_cache(int argc, char** argv)   {
  using namespace std;
  using namespace taskdb;
  int    rounds = INT_MAX;
  string node, dns;
  string port  = "3500";
  string mount = "/TDBDATA/XMLRPC";
  string server  = "ecs03.lbdaq.cern.ch";
  RTL::CLI cli(argc, argv, [] (int, char**)  {
      cout << " run_task_check  -opt [-opt]                           \n";
      cout << "    -dns=<node-name>         DNS node name             \n";
      cout << "    -node=<node-name>        Node name to query tasks  \n";
      cout << "    -server=<server-name>    TaskDB server host name   \n"
	   << "    -port=<port-number>      TaskDB server port        \n"
	   << "    -mount=<mount-name>      Web server directory name \n"
	   << endl;
      ::exit(0);
    });
  cli.getopt("dns",     3, dns);
  cli.getopt("node",    3, node);
  cli.getopt("port",    3, port);
  cli.getopt("server",  3, server);
  cli.getopt("mount",   3, mount);
  cli.getopt("rounds",  3, rounds);

  auto start = std::chrono::system_clock::now(), last = start;
  size_t counts = 0;
  TaskDBApi api;
  while ( --rounds > 0 )  {
    try {
      auto cl = rpc::client<http::HttpClient>(server, mount, port);
      (*cl)->setDebug(0);
      (*cl)->userRequestHeaders.push_back(http::HttpHeader("Accept-Encoding","gzip"));
      api.handler = std::move(cl);

      if ( (rounds%1000) == 0 )  {
	auto now = std::chrono::system_clock::now();
	long diff1  = std::chrono::duration_cast<std::chrono::milliseconds>(now-start).count();
	long diff2  = std::chrono::duration_cast<std::chrono::milliseconds>(now-last).count();
	printf("++ %8ld requests in %6ld sec delta:%6ld msec avg:%5.0f Hz instantaneous:%5.0f Hz\n",
	       api.numberOfCalls(), diff1/1000, diff2,
	       double(api.numberOfCalls())/double(diff1+1)*1000.0,
	       double(api.numberOfCalls()-counts)/double(diff2+1)*1000.0);
	fflush(stdout);
	counts = api.numberOfCalls();
	last = now;
      }

      auto cache = api.getCache(time(0));
      if ( cache.empty() )   {
	cout << "ERROR: api.getCache(" << cache.size() << " bytes) :  returned nothing!" << endl;
	continue;
      }
      

    }
    catch(const exception& e)   {
      cout << "ERROR: Processing exception: " << e.what() << endl;
    }
  }
  return 1;
}
