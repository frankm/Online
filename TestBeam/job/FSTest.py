"""
     Minimal Gaudi task in the online environment

     @author M.Frank
"""
from __future__ import print_function
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import sys
import Configurables
import GaudiOnline
import OnlineEnvBase as OnlineEnv
import Gaudi.Configuration as Gaudi

from Configurables import Online__FlowManager as FlowManager
from Configurables import Online__FillingSchemeAlg as FS
application = GaudiOnline.Passthrough(outputLevel=OnlineEnv.OutputLevel,
                                      partitionName=OnlineEnv.PartitionName,
                                      partitionID=OnlineEnv.PartitionID,
                                      classType=GaudiOnline.Class1)
application.setup_fifolog()
#application.setup_file_directory_access(['/group/online/dataflow/cmtuser/data/mdf','file_'])
application.setup_mbm_access('Events', True)
application.setup_monitoring()

application.setup_hive(FlowManager("EventLoop"), 40)
filling_scheme = FS('FillingScheme')
filling_scheme.DNS = "mon01";
wr = application.setup_algorithms([filling_scheme], 1.0)
wr.RequireODIN = False
application.monSvc.DimUpdateInterval   = 1
# Mode slection::  synch: 0 async_queued: 1 sync_queued: 2
application.config.execMode            = 0
application.config.numEventThreads     = 1
application.config.MBM_numConnections  = 1
application.config.MBM_numEventThreads = 1
#
application.config.MBM_requests = [
    'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0',
    'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0'
]
#
print('Setup complete....')
