#!/bin/bash
# =========================================================================
#
#  Farm worker node controller startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
HOST=`hostname -s | tr a-z A-Z`;
#
if test -z "${LOGFIFO}"; then
    export LOGFIFO=/run/fmc/logSrv.fifo;
fi;
if test "${LOGFIFO}" = "/run/fmc/logSrv.fifo" -a -e "/dev/shm/logs.dev"; then
  export LOGFIFO=/dev/shm/logs.dev;
fi;
if test -z "${TMS_DNS}"; then
    export TMS_DNS=${HOST_LONG};
fi;
if test -z "${SMI_DNS}"; then
    export SMI_DNS=${HOST_LONG};
fi;
if test -z "${SMI_FILE}"; then
    export SMI_FILE=${SMICONTROLLERROOT}/options/MonNode
fi;
if test -z "${SMI_DOMAIN}"; then
    export SMI_DOMAIN=${PARTITION_NAME}_${HOST}_SMI;
fi;
if test -z "${DIM_DNS_NODE}"; then
    export DIM_DNS_NODE=${HOST_LONG};
fi;
THREAD_INSTANCE_SERVICE=none;
THREAD_INSTANCE_SERVICE="/${HOST}/NumInstances";
#
exec -a ${UTGID} `which genRunner.exe` libSmiController.so smi_controller \
    -print=4 		         \
    -logger=fifo 	         \
    -part=${PARTITION_NAME}	 \
    -dns=${DIM_DNS_NODE}   	 \
    -tmsdns=${TMS_DNS} 	         \
    -smidns=${SMI_DNS} 	         \
    -smidomain=${SMI_DOMAIN}     \
    -smidebug=0                  \
    -smifile=${SMI_FILE}         \
    -count=${NBOFSLAVES}         \
    -service=${THREAD_INSTANCE_SERVICE}  \
    -runinfo=${RUNINFO}          \
    -taskconfig=${ARCH_FILE}     \
    "${CONTROLLER_REPLACEMENTS}" \
    -standalone=1                \
    -bindcpus=0
