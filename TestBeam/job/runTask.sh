#!/bin/bash
# =========================================================================
#
#  Generic farm task startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
#  Switch to the task directory and execute the controller process
#  ---------------------------------------------------------------
if test -z "${BINARY_TAG}"; then
    export BINARY_TAG=x86_64_v2-centos7-gcc11-do0;
    export BINARY_TAG=x86_64_v2-centos7-gcc11-opt;
fi;
export CMTCONFIG=${BINARY_TAG};  # backward compatibility, still needed?
#
if test -z "${SCRIPT}"; then
    SCRIPT=`realpath $0`;
fi;
SCRIPT_DIR=`dirname ${SCRIPT}`;
TESTBEAM_DIR=`dirname ${SCRIPT_DIR}`;
DIR=`dirname ${TESTBEAM_DIR}`;
#
tag=$BINARY_TAG;
. ${DIR}/setup.${BINARY_TAG}.vars;
. ${FARMCONFIGROOT}/job/createEnvironment.sh $*;
#
if test "${BINARY_TAG}" != "${tag}"; then
    export CMTCONFIG=${BINARY_TAG};
    . ${DIR}/setup.${BINARY_TAG}.vars;
fi;
#
if test -z "${TAN_NODE}"; then
    export DATAINTERFACE=`python /group/online/dataflow/scripts/getDataInterface.py`
    export TAN_PORT=YES
    TAN_NODE=${DATAINTERFACE};
fi;
export TAN_NODE;
if test -z "${EVENTSERVER_SOURCE}"; then
    EVENTSERVER_SOURCE=`hostname -s`::TEST_`hostname -s`_EventSrv_0;
    EVENTSERVER_SOURCE=`echo $EVENTSERVER_SOURCE | tr a-z A-Z`;
fi;
export EVENTSERVER_SOURCE;
#
#
DEBUG=
echo "+++ BINARY_TAG: ${BINARY_TAG}";
echo "+++ SCRIPT:     ${SCRIPT}  [${TASK_SCRIPT}]";
echo "+++ SCRIPT_DIR: ${SCRIPT_DIR}";
echo "+++ PYTHONPATH  ${TESTBEAM_DIR}/options:${PYTHONPATH}";
echo "+++ Task type:  ${TASK_TYPE}";
echo "+++ Partition:  ${PARTITION}";
#
if test "${PARTITION}" = "TEST"; then
    export PYTHONPATH=${TESTBEAM_DIR}/options:${PYTHONPATH};
    export INFO_OPTIONS=${TESTBEAM_DIR}/options/OnlineEnv.opts;
else
    export PYTHONPATH=/group/online/dataflow/options/${PARTITION}:${TESTBEAM_DIR}/options:${PYTHONPATH};
    export INFO_OPTIONS=/group/online/dataflow/options/${PARTITION}/OnlineEnv.opts;
fi;
#
dataflow_task_cmd()
{
    #echo  "`which gdb` --args genRunner.exe libDataflow.so dataflow_run_task -msg=Dataflow_OutputLogger -mon=Dataflow_DIMMonitoring -class=$1";
    echo  "dataflow_task -msg=Dataflow_OutputLogger -mon=Dataflow_DIMMonitoring -class=$1";
}
#
dataflow_task()
{
    #echo  "exec -a ${UTGID} genRunner.exe libDataflow.so dataflow_run_task -msg=Dataflow_OutputLogger -mon=Dataflow_DIMMonitoring -class=$1";
    echo  "exec -a ${UTGID} `dataflow_task_cmd $1`";
}
#
cd ${SCRIPT_DIR};
#
#
if test -f "${TASK_SCRIPT}"; then
    . ${TASK_SCRIPT} $*;
elif test -f ./${TASK_TYPE}.sh; then
    . ./${TASK_TYPE}.sh $*;
elif test -n "${TASK_SCRIPT}"; then
    . ${TASK_SCRIPT} $*;
elif test -f "${OPTIONS}" -a -n "`echo ${OPTIONS} | grep .py`"; then
    if test -n "${DEBUG}"; then
	echo "GaudiPython: OPTIONS: ${OPTIONS}";
    fi;
    if test -z "${APPLICATION}"; then
	APPLICATION=Online::OnlineEventApp;
    fi;
    exec -a ${UTGID} genPython.exe `which gaudirun.py` ${OPTIONS} --application=${APPLICATION};
elif test -f "${OPTIONS}" -a -n "`echo ${OPTIONS} | grep .opts`"; then
    if test -n "${DEBUG}"; then
	echo "Dataflow:    OPTIONS: ${OPTIONS}";
    fi;
    if test "${TASK_TYPE}" = "Receiver"; then
	export RTL_NO_BACKTRACE=1;
	#/home/frankm/bin/valgrind --error-limit=no --track-origins=yes --leak-check=full `dataflow_task_cmd ${CLASS}` -opts=${OPTIONS} ${AUTO_STARTUP} ${DEBUG_STARTUP} 1>/tmp/Receiver.log 2>&1;
	`dataflow_task ${CLASS}` -opts=${OPTIONS} ${AUTO_STARTUP} ${DEBUG_STARTUP};
    else
	`dataflow_task ${CLASS}` -opts=${OPTIONS} ${AUTO_STARTUP} ${DEBUG_STARTUP};
    fi;
elif test -f ./../../Online/FarmConfig/job/${TASK_TYPE}.sh; then
    cd `realpath ./../../Online/FarmConfig/job`;
    echo "$UTGID  :  `pwd`";
    . ./${TASK_TYPE}.sh $*;
else
    echo "+++ Running generic default task as '${TASK_TYPE}'";
    exec -a ${UTGID} genPython.exe ${SMICONTROLLERROOT}/scripts/DummyTask.py -utgid ${UTGID} -partition 103 -print ${OUTPUT_LEVEL};
fi;
#
echo "Going asleep....."
