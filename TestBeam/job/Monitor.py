"""
     Minimal Gaudi task in the online environment

     @author M.Frank
"""
from __future__ import print_function
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import sys
import Configurables
import Gaudi.Configuration as Gaudi

import GaudiOnline
import OnlineEnvBase as OnlineEnv

from Configurables import Online__FlowManager as FlowManager
application = GaudiOnline.Passthrough(outputLevel=OnlineEnv.OutputLevel,
                                      partitionName=OnlineEnv.PartitionName,
                                      partitionID=OnlineEnv.PartitionID,
                                      classType=GaudiOnline.Class1)
application.setup_fifolog()
application.setup_mbm_access('Events', True)
writer = application.setup_mbm_output('EventOutput')
writer.MBM_maxConsumerWait = 10
writer.MBM_allocationSize = 1024*1024*10
writer.MBM_burstPrintCount = 25000
writer.MaxEventsPerTransaction = 10
writer.UseRawData = False
writer.RequireODIN = False

ev_size = Configurables.Online__EventSize('EventSize')
application.setup_hive(FlowManager("EventLoop"), 40)
application.setup_monitoring()
#application.setup_algorithms(writer, 0.05)
application.setup_algorithms([ev_size,writer], 1.0)

application.config.numThreadSvcName = 'NumInstances'
application.passThrough.BurnCPU = 1
application.passThrough.DelayTime = 1

application.monSvc.DimUpdateInterval   = 1

application.config.burstPrintCount     = 30000

# Mode slection::  synch: 0 async_queued: 1 sync_queued: 2
application.config.execMode            = 1
application.config.numEventThreads     = 15
application.config.MBM_numConnections  = 8
application.config.MBM_numEventThreads = 5
#
application.config.execMode            = 1
application.config.numEventThreads     = 15
application.config.MBM_numConnections  = 3
application.config.MBM_numEventThreads = 4
#
# Enable this for debugging
#
# application.config.numEventThreads     = 1
# application.config.MBM_numConnections  = 1
# application.config.MBM_numEventThreads = 1
#
application.config.MBM_requests = [
    'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0',
    'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0'
]
#
print('Setup complete....')

