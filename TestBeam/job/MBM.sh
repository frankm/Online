#!/bin/bash
# =========================================================================
#
#  Default script to start the passthrough process on a farm node.
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
`dataflow_task Class0` -opts=../options/${TASK_TYPE}.opts  ${AUTO_STARTUP} ${DEBUG_START};

