"""
     Minimal Gaudi task in the online environment

     @author M.Frank
"""
from __future__ import print_function
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import sys
import Configurables
import GaudiOnline
import OnlineEnvBase as OnlineEnv
import Gaudi.Configuration as Gaudi

from Configurables import Online__FlowManager as FlowManager
application = GaudiOnline.Passthrough(outputLevel=OnlineEnv.OutputLevel,
                                      partitionName=OnlineEnv.PartitionName,
                                      partitionID=OnlineEnv.PartitionID,
                                      classType=GaudiOnline.Class1)
application.setup_fifolog()
application.setup_mbm_access('Output', True)
writer = application.setup_mbm_output('EventOutput', buffer='Monitor')
print ('writer.MBM_buffer: %s'%(writer.MBM_buffer,))
writer.MBM_maxConsumerWait = 10
writer.UseRawData = False
writer.UseTAELeaves = True;

application.setup_hive(FlowManager("EventLoop"), 40)
wr = application.setup_algorithms(writer, 1.0)
wr.RequireODIN = False
application.setup_monitoring()
application.monSvc.DimUpdateInterval   = 1
# Mode slection::  synch: 0 async_queued: 1 sync_queued: 2
application.config.execMode            = 0
application.config.numEventThreads     = 15
application.config.MBM_numConnections  = 8
application.config.MBM_numEventThreads = 5

application.config.numEventThreads     = 1
application.config.MBM_numConnections  = 1
application.config.MBM_numEventThreads = 1

application.config.MBM_requests = [
    'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0',
    'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0'
]
#
print('Setup complete....')
