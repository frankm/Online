#!/bin/bash
# =========================================================================
#
#  Default script to start the passthrough process on a farm node.
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
exec -a ${UTGID} genPython.exe `which gaudirun.py` ./Monitor.py --application=Online::OnlineEventApp;
