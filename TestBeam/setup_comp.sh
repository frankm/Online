#!/bin/bash
# =========================================================================
#
#  Default script to start the passthrough process on a farm node.
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
export MONITOR_BASE=`realpath /group/online/dataflow/cmtuser/TestBeam`;
export INSTALLATION=`dirname ${MONITOR_BASE}`;
#
#
COMP=gcc11;
if test -n "${1}"; then
    COMP="${1}";
fi;
#
if test "${COMP}" = "gcc10"; then
    export CMTDEB=x86_64_v2-centos7-${COMP}-do0;
    export CMTOPT=x86_64_v2-centos7-${COMP}-opt;
elif test "${COMP}" = "gcc11"; then
    export CMTDEB=x86_64_v2-centos7-${COMP}-do0;
    export CMTOPT=x86_64_v2-centos7-${COMP}-opt;
elif test "${COMP}" = "clang11"; then
    export CMTDEB=x86_64_v2-centos7-${COMP}-dbg;
    export CMTOPT=x86_64_v2-centos7-${COMP}-opt;
else
    export CMTDEB=x86_64-centos7-${COMP}-do0;
    export CMTOPT=x86_64-centos7-${COMP}-opt;
fi;
export CMTCONFIG=$CMTDEB;
export BINARY_TAG=$CMTDEB;
export ALL_CMT_PLATFORMS="${CMTDEB} ${CMTOPT}";
#
#
if test "${COMP}" = "gcc9"; then
    export RDKAFKA_DIR=/group/online/dataflow/cmtuser/libraries/kafka/rdkafka-gcc-9.2.0-opt;
else
    export RDKAFKA_DIR=/group/online/dataflow/cmtuser/libraries/kafka/rdkafka-gcc-10.1.0-opt;
fi;
#
#
H3=`echo $DIM_DNS_NODE | tr a-z A-Z | cut -b 1-3`;
H4=`echo $DIM_DNS_NODE | tr a-z A-Z | cut -b 1-4`;
EB=`echo $DIM_DNS_NODE | tr a-z A-Z | cut -b 3-4`;
MAINDNS=${DIM_DNS_NODE};
if test "${H3}" = "HLT"; then
    MAINDNS="ecstms01";
elif test "${H4}" = "DAQV"; then
    MAINDNS="ecstms01";
elif test "${EB}" = "EB"; then
    MAINDNS="ecstms01";
fi;
#
#
echo "+++ Base:            ${MONITOR_BASE}";
echo "+++ Installation:    ${INSTALLATION}";
echo "+++ Kafka directory: ${RDKAFKA_DIR}";
echo "+++ Compiling for:   $COMP";
echo "+++ Binary tag:      $BINARY_TAG";
echo "+++ MAIN DNS node:   $MAINDNS";
#
#
start_gui()   {
    xterm -title "TestBeam GUI" -geo 180x20 -e ". ${INSTALLATION}/setup.${BINARY_TAG}.vars; \
     gentest.exe libTestBeamGui.so testbeam_node_gui          \
        -maxinst=1 -instances=1 -partition=TEST -host=${HOST} -maindns=${MAINDNS} \
        -replacements=TESTBEAM_DIR:\${MONITOR_BASE}           \
        -ctrl_script=\${MONITOR_BASE}/job/runTask.sh          \
	-runinfo=\${MONITOR_BASE}/options/OnlineEnvBase.py    \
	-architecture=\${MONITOR_BASE}/options/DataflowArch_${1}.xml" &
}
start_gui_FPGA_Source()   {
    start_gui FPGA_Source;
}
start_gui_FPGA_Target()   {
    start_gui FPGA_Target;_
}
start_gui_MUON()   {
    start_gui MUON;
}
start_gui_Allen()   {
    xterm -title "TestBeam GUI" -geo 180x20 -e ". ${INSTALLATION}/setup.${BINARY_TAG}.vars; \
     gentest.exe libTestBeamGui.so testbeam_node_gui          \
        -maxinst=1 -instances=1 -partition=ALLEN -host=${HOST} -maindns=${MAINDNS}  \
        -ctrl_script=/group/online/dataflow/scripts/runFarmTask.sh        \
	-runinfo=/group/online/dataflow/options/ALLEN/OnlineEnvBase.py    \
	-architecture=/group/online/dataflow/options/ALLEN/Architecture.xml" &
}
start_gui_RichMon()   {
    xterm -title "TestBeam GUI" -geo 180x20 -e ". ${INSTALLATION}/setup.${BINARY_TAG}.vars; \
     gentest.exe libTestBeamGui.so testbeam_node_gui          \
        -maxinst=1 -instances=1 -partition=TEST -host=${HOST} -maindns=${MAINDNS}  \
        -replacements=TESTBEAM_DIR:\${MONITOR_BASE}           \
        -ctrl_script=\${MONITOR_BASE}/job/runTask.sh          \
	-runinfo=/group/online/dataflow/options/TEST/OnlineEnvBase.py    \
	-architecture=\${MONITOR_BASE}/options/DataflowArch_RichMon.xml" &
}
#
start_gui_MDF()   {
    xterm -title "TestBeam GUI" -geo 180x20 -e ". ${INSTALLATION}/setup.${BINARY_TAG}.vars; \
     gentest.exe libTestBeamGui.so testbeam_node_gui          \
        -maxinst=1 -instances=1 -partition=TEST -host=${HOST} -maindns=${MAINDNS}  \
        -replacements=TESTBEAM_DIR:\${MONITOR_BASE}           \
        -ctrl_script=\${MONITOR_BASE}/job/runTask.sh          \
	-runinfo=\${MONITOR_BASE}/options/OnlineEnvBase.py    \
	-architecture=\${MONITOR_BASE}/options/DataflowArch_MDF.xml" &
}
#
start_gui_MEP()   {
    xterm -title "TestBeam GUI" -geo 180x20 -e ". ${INSTALLATION}/setup.${BINARY_TAG}.vars; \
     gentest.exe libTestBeamGui.so testbeam_node_gui          \
        -maxinst=1 -instances=1 -partition=TEST -host=${HOST} -maindns=${MAINDNS}  \
        -replacements=TESTBEAM_DIR:\${MONITOR_BASE}           \
        -ctrl_script=\${MONITOR_BASE}/job/runTask.sh          \
	-runinfo=\${MONITOR_BASE}/options/OnlineEnvBase.py    \
	-architecture=\${MONITOR_BASE}/options/DataflowArch_MEP.xml" &
}
#
start_gui_HLT2()   {
    xterm -title "TestBeam GUI" -geo 180x20 -e ". ${INSTALLATION}/setup.${BINARY_TAG}.vars; \
     gentest.exe libTestBeamGui.so testbeam_node_gui          \
        -maxinst=1 -instances=1 -partition=TEST -host=${HOST} -maindns=${MAINDNS}  \
        -replacements=TESTBEAM_DIR:\${MONITOR_BASE}           \
        -ctrl_script=\${MONITOR_BASE}/job/runTask.sh          \
	-runinfo=\${MONITOR_BASE}/options/OnlineEnvBase.py    \
	-architecture=\${MONITOR_BASE}/options/DataflowArch_HLT2.xml" &
}
#
start_gui_Velo()   {
    xterm -title "TestBeam GUI" -geo 180x20 -e ". ${INSTALLATION}/setup.${BINARY_TAG}.vars; \
     gentest.exe libTestBeamGui.so testbeam_node_gui          \
        -maxinst=1 -instances=1 -partition=TEST -host=${HOST} -maindns=${MAINDNS}  \
        -replacements=TESTBEAM_DIR:\${MONITOR_BASE}           \
        -ctrl_script=\${MONITOR_BASE}/job/runTask.sh          \
	-runinfo=\${MONITOR_BASE}/options/OnlineEnvBase.py    \
	-architecture=\${MONITOR_BASE}/options/DataflowArch_VeloHalfVtxMon.xml" &
}
#
start_gui_Align()   {
    xterm -title "TestBeam GUI" -geo 180x20 -e ". ${INSTALLATION}/setup.${BINARY_TAG}.vars; \
     gentest.exe libTestBeamGui.so testbeam_node_gui          \
        -maxinst=1 -instances=1 -partition=TEST -host=${HOST} -maindns=${MAINDNS}  \
        -replacements=TESTBEAM_DIR:\${MONITOR_BASE}           \
        -ctrl_script=\${MONITOR_BASE}/job/runTask.sh          \
	-runinfo=\${MONITOR_BASE}/options/OnlineEnvBase.py    \
	-architecture=\${MONITOR_BASE}/options/DataflowArch_Align.xml" &
}
#
mmbmmon()   {
    xterm -ls -132 -geometry 132x65 -title ${HOST}/MBMMon \
	-e "export UTGID=${HOST}_MBMMon_\$$;              \
            . ${INSTALLATION}/setup.${BINARY_TAG}.vars;   \
            exec -a ${HOST}/MBMMon gentest.exe libOnlineKernel.so mbm_mon_scr" &
}
#
