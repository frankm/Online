#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
build_parse_this()   {
    SOURCE=${1}
    # get param to "."
    thisroot=$(dirname ${SOURCE})
    THIS=$(cd ${thisroot} > /dev/null;pwd); export THIS;
    unset SOURCE thisroot
}
#===============================================================================
# Configure project
#
# Author     : M.Frank
#
#===============================================================================
do_echo()  
{
    echo $*;
    $*;
}
#===============================================================================
# Print help
#
# Author     : M.Frank
#
#===============================================================================
usage()
{
    echo " do_make -opt [-opt]"
    echo " -t --type        <build_type>         Supply build type Debug0|Debug|Release. Default: Debug0";
    echo " -s --source      <source directory>   Source directory for cmake";
    echo "                                       Default: `pwd`";
    echo "                                       CMakeLists.txt required!";
    echo " -b --build-dir   <build  directory>   Build  directory for cmake";
    echo "                                       Default: `pwd`/build_dataflow.<binary-tag>";
    echo " -i --install-dir <install directory>  Installation directory for cmake";
    echo "                                       Default: `pwd`/install_dataflow.<binary-tag>";
    echo " -v --view        <path to LCG view>   Path to the LCG view directory.";
    echo " -T --tag         <tag-name>           Binary tag to be used. Current: '${binary_tag}'";
    echo "                                       ==> Must be an existing build tag of the LCG view!";
    echo " -c --cmake                            Execute only cmake step";
    echo " -B --build                            Only build: no cmake, no install";
    echo " -I --install                          Only install: no cmake, no build";
    echo " -p --parallel    <number>             Number of threads for parallel build (make -j <number>)";
    echo " -e --ebonly                           Build event builder only ";
    echo "                                       changes defaults to: ";
    echo "                                       build-dir:   `pwd`/build_ebonly.<binary-tag>";
    echo "                                       install-dir: `pwd`/install_ebonly.<binary-tag>";
    echo "";
    echo "";
    echo " ===> Example to use the view from Gaudi (/cvmfs/lhcb.cern.ch):";
    echo "  cd /group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r19";
    echo "  . /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_101/gcc/11.1.0/x86_64-centos7/setup.sh";
    echo "  ./build_standalone.sh -t Debug0 --tag x86_64-centos7-gcc11-dbg --view /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_101 -c -B -I";
    echo "";
    echo " ===> Example to use the view from SFT (/cvmfs/sft.cern.ch):";
    echo "  cd /group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r19";
    echo "  . /cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0/x86_64-centos7/setup.sh";
    echo "  ./build_standalone.sh -t Debug0 --tag x86_64-centos7-gcc11-dbg --view /cvmfs/sft.cern.ch/lcg/releases/LCG_101 -c -B -I";
    echo "";
    exit 1;
}
#===============================================================================
#
#===============================================================================
build_parse_this $0;
if [ ! -f ${THIS}/build_standalone.sh ]; then
  echo "+++ Wrong working directory! [${THIS}]";
  exit 1;
fi;
#
BINARY_TAG=; ## x86_64-centos7-gcc11-dbg;
LCG_VIEW=;   ##/cvmfs/sft.cern.ch/lcg/views/LCG_101/${BINARY_TAG};
export RDKAFKA_DIR=/group/online/dataflow/cmtuser/libraries/kafka/rdkafka-gcc-11.1.0-opt;
#
PARSED_ARGUMENTS=$(getopt -a -n do_cmake -o hcBIeT:t:s:b:i:v:p: --longoptions help,cmake,build,install,ebonly,tag:,type:,source:,build-dir:,install-dir:,view:,parallel: -- "[build_dataflow]" $*)
VALID_ARGUMENTS=$?
if [ "$VALID_ARGUMENTS" != "0" ]; then
    echo "====> Invalid arguments to cmake step";
    usage;
fi;
#
#
#
echo "++ PARSED_ARGUMENTS: $PARSED_ARGUMENTS";
build_type=;
build_dir=;
source_dir=;
install_dir=;
lcg_view=${LCG_VIEW};
binary_tag=;
exec_cmake=;
exec_build=;
exec_install=;
eb_only=OFF;
parallel="-j 12";
#
eval set -- "$PARSED_ARGUMENTS";
while :
do
    case "${1}" in
	-t | --type)          build_type=${2};     shift 2 ;;
	-s | --source)        source_dir=${2};     shift 2 ;;
	-b | --build-dir)     build_dir=${2};      shift 2 ;;
	-i | --install-dir)   install_dir=${2};    shift 2 ;;
	-v | --view)          lcg_view=${2};       shift 2 ;;
	-T | --tag)           binary_tag=${2};     shift 2 ;;
	-c | --cmake)         exec_cmake=YES;      shift ;;
	-B | --build)         exec_build=YES;      shift ;;
	-I | --install)       exec_install=YES;    shift ;;
	-p | --parallel)      parallel="-j ${2}";  shift 2 ;;
        -e | --ebonly)        eb_only=ON;          shift ;;
	-h | --help)    usage;                     shift ;;
	# -- means the end of the arguments; drop this, and break out of the while loop
	--) shift;  break;;
	# If invalid options were passed, then getopt should have reported an error,
	# which we checked as VALID_ARGUMENTS when getopt was called...
	*) echo "====> Unexpected option: $1 - this should not happen."
            usage; shift; break;;
    esac;
done;
#
# OK. Now check pre-conditions
#
if test -z "${binary_tag}"; then
    binary_tag=${BINARY_TAG};
fi;
if test -z "${source_dir}"; then
    source_dir=`pwd`;
fi;
#
# OK. Now check pre-conditions
#
#
if [ -z "${binary_tag}" ]; then
    # This should never happen: if no binary tag, no LCG view!
    echo "====> [${build_script}] No valid binary tag supplied.";
    exit 1;
fi;
#
if [ ! -f ${source_dir}/CMakeLists.txt ]; then
    echo "====> [build_standalone] Invalid source directory: ${source_dir}";
    echo "====> [build_standalone]    No CMakeLists.txt file present.";
    exit 1;
fi;

if test -z "${exec_cmake}"; then
    echo "" > /dev/null;
elif test -z "${lcg_view}"; then
    echo "====> [build_standalone] Missing argument --view [path to LCG view]";
    exit 1;
elif [ ! -d ${lcg_view} ]; then
    echo "====> [build_standalone] LCG View path is wrong: no directory found.";
#    exit 1;
fi;

build_prefix=dataflow;
if test "${eb_only}" = "ON"; then
    build_prefix=ebonly;
fi;
if test -z "${build_dir}"; then
    build_dir=${source_dir}/build.${build_prefix}.${binary_tag};
fi;
if test -z "${install_dir}"; then
    install_dir=${source_dir}/install.${build_prefix}.${binary_tag};
fi;
#
lcg_view=`realpath ${lcg_view}`;
build_dir=`realpath ${build_dir}`;
source_dir=`realpath ${source_dir}`;
install_dir=`realpath ${install_dir}`;
#
echo "Using LCG view from ${lcg_view} Binary tag: ${binary_tag}  cmake: `which cmake`";
if test -f ${install_dir}/bin/thisonline.sh -a -z "${exec_cmake}"; then
    source ${install_dir}/bin/thisonline.sh;
elif [ -f "${lcg_view}/setup.sh" ]; then
    source ${lcg_view}/setup.sh;
    CMAKE_OPTS="-DONLINE_ROOT_SYS=${ROOTSYS} -DPLUGIN_SOURCES=${lcg_view}";
elif [ -d "${lcg_view}" ]; then
    #echo "Using LCG view from ${lcg_view} Binary tag: ${binary_tag}  cmake: `which cmake`";
    export PATH=${lcg_view}/CMake/3.20.0/${binary_tag}/bin:${PATH}
    lcg_platform=`echo "${binary_tag}" | tr "-" " " | awk '{ print $1"-"$2 }'`;
    lcg_comp=`echo "${binary_tag}" | tr "-" " " | awk '{ print $3 }'`;
    lcg_dbg=`echo "${binary_tag}" | tr "-" " " | awk '{ print $4 }'`;
    echo "====> [build_standalone] Using LCG view from ${lcg_view} Binary tag: ${binary_tag}";
    echo "====> [build_standalone] cmake: `which cmake` platform: ${lcg_platform} compiler: ${lcg_comp}";
    #echo . ${lcg_view}/${lcg_comp}/11.1.0/${lcg_platform}/setup.sh
    #. ${lcg_view}/gcc/11.1.0/${lcg_platform}/setup.sh
    CMAKE_OPTS="-DBINARY_TAG=${binary_tag} -DPLUGIN_SOURCES=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v36r7/GaudiPluginService";
else
    echo "====> [build_standalone] Do you really know what you want ?";
    usage;
fi;
#
#
#
echo "++ ===============================================================";
echo "++ Configure cmake for building ${build_type} ";
echo "++                   Source directory:  ${source_dir} ";
echo "++                   Build directory:   ${build_dir} ";
echo "++                   Install directory: ${install_dir} ";
echo "++                   LCG view:          ${lcg_view} ";
echo "++                   Current directory: `pwd`";
echo "++ ===============================================================";
#
#
# A bit a queer logic, but I do not know how to do better:
#
if [ -z "${exec_cmake}" ]; then
    echo "++ No cmake step required --> Skipped.";
else
  mkdir -p ${build_dir};
  cd ${build_dir};
  if [ $? -ne  0 ]; then
    echo "====> [build_standalone] ERROR: Failed to change build directory: ${build_dir}.";
    echo "====> [build_standalone] ERROR: Did you miss out something ?";
    exit 1;
  fi;
  do_echo cmake -DCMAKE_INSTALL_PREFIX="${install_dir}" \
        -DCMAKE_BUILD_TYPE=${build_type}  \
        -DSUPPORT_GAUDI=OFF ${CMAKE_OPTS} \
        -DLCG_VIEW=${lcg_view}            \
        -DEVENTBUILDER_ONLY=${eb_only}    \
        ${source_dir}
  if [ $? -ne  0 ]; then
      echo     "====> [build_standalone] ERROR: Failed cmake step!";
      if test -n "echo `which c++ | grep /usr`"; then
	  echo "====> [build_standalone] Are you sure you have the proper compiler setup ?";
      fi;
      exit 1;
  fi;
fi;
#
if test -n "${exec_build}"; then
  cd ${build_dir};
  if [ $? -ne  0 ]; then
    echo "====> [build_standalone] ERROR: Failed to change build directory: ${build_dir}.";
    echo "====> [build_standalone] ERROR: Did you miss out the cmake step ?";
    exit 1;
  fi;
  do_echo make ${parallel};
  if [ $? -ne  0 ]; then
      echo     "====> [build_standalone] ERROR: Failed make step!";
      if test -n "echo `which c++ | grep /usr`"; then
	  echo "====> [build_standalone] Are you sure you have the proper compiler setup ?";
      fi;
      exit 1;
  fi;
fi;
#
if test -n "${exec_install}"; then
  mkdir -p ${install_dir};
  if [ $? -ne  0 ]; then
    echo   "====> [build_standalone] ERROR: Failed to create the install directory: ${install_dir}.";
    exit 1;
  fi;
  if [ ! -f ${build_dir}/bin/thisonline.sh ]; then
    echo "====> [build_standalone] ERROR: cmake step was apparently not successful --> redo ?";
    exit 1;
  fi;
  echo "====> [build_standalone] PATH: $PATH";
  echo "Executing build/installation step...";
  source ${build_dir}/bin/thisonline.sh;
  echo "====> [build_standalone] PATH: $PATH";
  do_echo make install -j 10;
  if [ $? -ne  0 ]; then
      echo "====> [build_standalone] ERROR: Failed install step!";
      exit 1;
  fi;
fi;

