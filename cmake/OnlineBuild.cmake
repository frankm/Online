# =======================================================================
#
#    Online cmake configuration
#
#
#
# =======================================================================
#
# =======================================================================
if ( NOT TARGET Online::rt )
  find_library(CMAKE_RT_LIBS rt)
  add_library(Online::rt IMPORTED SHARED GLOBAL)
  set_property(TARGET Online::rt PROPERTY IMPORTED_LOCATION ${CMAKE_RT_LIBS})
  message(STATUS "++ Use Online::rt: ${CMAKE_RT_LIBS}")
endif()
#
#
# =======================================================================
if ( NOT TARGET Online::dl )
  find_library(ONLINE_DL_LIBS dl)
  add_library(Online::dl IMPORTED SHARED GLOBAL)
  set_property(TARGET Online::dl PROPERTY IMPORTED_LOCATION ${ONLINE_DL_LIBS})
  message(STATUS "++ Use Online::dl: ${CMAKE_DL_LIBS} ${ONLINE_DL_LIBS}")
endif()
#
#
# =======================================================================
function(online_env)
    message(DEBUG "online_env(${ARGV})")
    if(ARGC GREATER 0 AND ARGV0 STREQUAL "PRIVATE")
        # drop the flag and do not propagate the change downstream
        list(REMOVE_AT ARGV 0)
    else()
        # not PRIVATE, so propagate the change downstream
        set_property(GLOBAL APPEND PROPERTY ${PROJECT_NAME}_ENVIRONMENT ${ARGV})
    endif()
    if(NOT TARGET target_runtime_paths)
        add_custom_target(target_runtime_paths)
    endif()
    while(ARGV)
        list(POP_FRONT ARGV action variable value)
        if(action STREQUAL "SET")
            set_property(TARGET target_runtime_paths APPEND_STRING
                PROPERTY extra_commands "export ${variable}=${value};\n")
        elseif(action STREQUAL "PREPEND")
            set_property(TARGET target_runtime_paths APPEND_STRING
                PROPERTY extra_commands "export ${variable}=${value}:\$${variable};\n")
        elseif(action STREQUAL "APPEND")
            set_property(TARGET target_runtime_paths APPEND_STRING
                PROPERTY extra_commands "export ${variable}=\$${variable}:${value};\n")
        elseif(action STREQUAL "DEFAULT")
            set_property(TARGET target_runtime_paths APPEND_STRING
                PROPERTY extra_commands "export ${variable}=\${${variable}:-${value}};\n")
        else()
            message(FATAL_ERROR "invalid environment action ${action}")
        endif()
    endwhile()
endfunction(online_env)
# =======================================================================
#
#
