# =======================================================================
#
#    Online cmake configuration
#
#
#
# =======================================================================
set(ONLINE_FIND_PACKAGE_PATH)
set(ONLINE_BUILD_DATE_TIME)
#
#
# =======================================================================
# Return the date (yyyy-mm-dd hh:mm:ss)
macro(online_build_date_time RESULT)
  execute_process(COMMAND "date" "+%Y-%m-%d %H:%M:%S" OUTPUT_VARIABLE ${RESULT})
  string(REGEX REPLACE "\n" "" ${RESULT} ${${RESULT}})
endmacro()
#
#
# =======================================================================
online_build_date_time(ONLINE_BUILD_DATE_TIME)
#
#
# =======================================================================
macro(online_print_setup)
  message(STATUS "++ Build ${PROJECT_NAME}:    ${ONLINE_BUILD_DATE_TIME}")
  message(STATUS "++ ONLINE_INSTALL_LIBDIR     ${ONLINE_INSTALL_LIBDIR}")
  message(STATUS "++ ONLINE_INSTALL_BINDIR     ${ONLINE_INSTALL_BINDIR}")
  message(STATUS "++ ONLINE_INSTALL_INCLUDEDIR ${ONLINE_INSTALL_INCLUDEDIR}")
  message(STATUS "++ ONLINE_INSTALL_PYTHONDIR  ${ONLINE_INSTALL_PYTHONDIR}")
  message(STATUS "++ ONLINE_INSTALL_SCRIPTDIR  ${ONLINE_INSTALL_SCRIPTDIR}")
endmacro(online_print_setup)
#
#
# =======================================================================
macro(online_locate_package var alias)
  if ( "${ONLINE_FIND_PACKAGE_PATH}" STREQUAL "" )
    find_file(ONLINE_FIND_PACKAGE_PATH find_package_path.py HINTS ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
    if ( "${ONLINE_FIND_PACKAGE_PATH}" STREQUAL "ONLINE_FIND_PACKAGE_PATH-NOTFOUND" )
      message(ERROR  "++ Failed to locate package resolver find_package_path.py ")
      set(${var} "UNRESOLVED")
      return()
    else()
      message(STATUS "++ Package resolver: ${ONLINE_FIND_PACKAGE_PATH}")
    endif()
    #message(status "++++ ONLINE_BINARY_TAG: ${ONLINE_BINARY_TAG}")
    #message(status "++++ ONLINE_LCG_VIEW_LOCATION: ${ONLINE_LCG_VIEW_LOCATION}")
  endif()
  execute_process(
    COMMAND python ${ONLINE_FIND_PACKAGE_PATH}
                --package=${alias} --build=${ONLINE_BINARY_TAG}
                --view=${ONLINE_LCG_VIEW_LOCATION}
        OUTPUT_VARIABLE PACKAGE_DIR
	OUTPUT_STRIP_TRAILING_WHITESPACE
	ERROR_STRIP_TRAILING_WHITESPACE
  )
  set(${var})
  message(STATUS "++ Resolving package: ${alias} ${ARGN} --> ${PACKAGE_DIR}")
  if( NOT "${PACKAGE_DIR}" STREQUAL "UNRESOLVED" )
    if( "${CMAKE_PREFIX_PATH}" STREQUAL "" )
      set(CMAKE_PREFIX_PATH "${PACKAGE_DIR}")
    else()
      list(PREPEND CMAKE_PREFIX_PATH "${PACKAGE_DIR}")
    endif()
    set(${var} "${PACKAGE_DIR}")
    if ( EXISTS "${PACKAGE_DIR}/lib64" )
      online_env(PREPEND ONLINE_LIBRARY_PATH "${PACKAGE_DIR}/lib64")
    elseif ( EXISTS "${PACKAGE_DIR}/lib" )
      online_env(PREPEND ONLINE_LIBRARY_PATH "${PACKAGE_DIR}/lib")
    endif()
    if ( EXISTS "${PACKAGE_DIR}/bin" )
      online_env(PREPEND ONLINE_RUNTIME_PATH "${PACKAGE_DIR}/bin")
    endif()
    if ( EXISTS "${PACKAGE_DIR}/include" )
      online_env(PREPEND ROOT_INCLUDE_PATH "${PACKAGE_DIR}/include")
    endif()
    if ( EXISTS "${PACKAGE_DIR}/python" )
      online_env(PREPEND PYTHON_PATH "${PACKAGE_DIR}/python")
    endif()
  endif()
  # message(STATUS "++ CMAKE_PREFIX_PATH: ${CMAKE_PREFIX_PATH}")
endmacro(online_locate_package)
#
#
# =======================================================================
macro(online_check_prerequisites var)
  set(${var} "NOTOK")
  set(CMAKE_CXX_STANDARD 17 CACHE STRING "C++ standard used for compiling")
  set(CMAKE_CXX_STANDARD_REQUIRED ON)
  set(CMAKE_CXX_EXTENSIONS OFF)
  # Check prerequisites: LCG VIEW
  if ( NOT LCG_VIEW )
    message(ERROR "++ The LCG view to resolve external packages was not specified!")
    return()
  endif()
  # Check prerequisites: LCG VIEW's binary tag
  if( NOT BINARY_TAG )
    message(ERROR "++ The binary tag of packages from the LCG_VIEW is not set!")
    return()
  endif()
  # Check prerequisites: Plugin manager sources
  if( NOT PLUGIN_SOURCES )
    set(PLUGIN_SOURCES /cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v36r7/GaudiPluginService)
    message(STATUS "++ Using default plugin manager sources from ${PLUGIN_SOURCES}")
  else()
    message(STATUS "++ Using plugin manager sources from ${PLUGIN_SOURCES}")
  endif()
  set(ONLINE_BINARY_TAG        "${BINARY_TAG}" CACHE STRING "binary-tag" )
  set(ONLINE_LCG_VIEW_LOCATION "${LCG_VIEW}"   CACHE STRING "view-location")
  set(ONLINE_RUNTIME           ""              CACHE STRING "run-time")
  set(ONLINE_ENVIRONMENT       ""              CACHE STRING "environment")
  message(STATUS "++ Basic online prerequisites checked and OK.")
  message(STATUS "++ ONLINE_LCG_VIEW_LOCATION:  ${ONLINE_LCG_VIEW_LOCATION}")
  message(STATUS "++ ONLINE_BINARY_TAG          ${ONLINE_BINARY_TAG}")
  set(${var} "OK")
endmacro(online_check_prerequisites)
#
#
# =======================================================================
# helper macro for generating project configuration file
macro(online_generate_package_configuration_files)
    foreach( arg ${ARGN} )
        IF( ${arg} MATCHES "Config.cmake" )
            IF( EXISTS "${PROJECT_SOURCE_DIR}/cmake/${arg}.in" )
                configure_file( "${PROJECT_SOURCE_DIR}/cmake/${arg}.in"
                                "${PROJECT_BINARY_DIR}/${arg}" @ONLY
                )
                install( FILES "${PROJECT_BINARY_DIR}/${arg}" DESTINATION ./cmake )
            endif()
        endif()
    endforeach()

    include( CMakePackageConfigHelpers )
    write_basic_package_version_file( ${CMAKE_PROJECT_NAME}ConfigVersion.cmake
                                      VERSION ${Online_VERSION}
                                      COMPATIBILITY AnyNewerVersion )
    install( FILES "${PROJECT_BINARY_DIR}/${CMAKE_PROJECT_NAME}ConfigVersion.cmake" DESTINATION ./cmake )
endmacro(online_generate_package_configuration_files)
#
#
# =======================================================================
macro(online_locate_dependencies)
  set(PKG_DIR)
  online_locate_package(PKG_DIR zeromq)
  online_locate_package(PKG_DIR libsodium)
  online_locate_package(PKG_DIR Boost)
  online_locate_package(PKG_DIR Python)
  online_locate_package(PKG_DIR ROOT)
  set(ENV_VAR LD_LIBRARY_PATH)
  set(ENV{${ENV_VAR}} "${PKG_DIR}/lib:$ENV{${ENV_VAR}}" )
  set(ENV_VAR PATH)
  set(ENV{${ENV_VAR}} "${PKG_DIR}/bin:$ENV{${ENV_VAR}}" )
  set(ONLINE_ROOT_SYS "${PKG_DIR}")
  #MESSAGE(STATUS "LD_LIBRARY_PATH: $ENV{LD_LIBRARY_PATH}")
  #MESSAGE(STATUS "PATH: $ENV{PATH}")
  #MESSAGE(STATUS "PKG_DIR: ${PKG_DIR}")
  online_locate_package(PKG_DIR AIDA)
  online_locate_package(PKG_DIR ZLIB)
  online_locate_package(PKG_DIR TBB)
  online_locate_package(PKG_DIR SQLite)
  online_locate_package(PKG_DIR Oracle)
  online_locate_package(PKG_DIR PkgConfig)
  online_locate_package(PKG_DIR jsonmcpp)
  online_locate_package(PKG_DIR yamlcpp)
  online_locate_package(PKG_DIR MySQL)
  online_locate_package(PKG_DIR PostgreSQL)
endmacro(online_locate_dependencies)
#
#
# =======================================================================
macro(online_set_compiler_flags)
  include(CheckCCompilerFlag)
  include(CheckCXXCompilerFlag)
  if ( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
    set(CMAKE_C_FLAGS             "${CMAKE_C_FLAGS} -g -O0")
    set(CMAKE_CXX_FLAGS           "${CMAKE_CXX_FLAGS} -g -O0")
    set(CMAKE_EXE_LINKER_FLAGS    "${CMAKE_SHARED_EXE_FLAGS} -g -O0")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -g -O0")
    set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -g -O0")
  elseif ( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug0" )
    #set(CMAKE_BUILD_TYPE Debug)
    set(CMAKE_C_FLAGS             "${CMAKE_C_FLAGS} -g -O0")
    set(CMAKE_CXX_FLAGS           "${CMAKE_CXX_FLAGS} -g -O0")
    set(CMAKE_EXE_LINKER_FLAGS    "${CMAKE_SHARED_EXE_FLAGS} -g -O0")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -g -O0")
    set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -g -O0")
  endif()
  set(COMPILER_FLAGS ${COMPILER_FLAGS} -Wshadow -Wunused -Wnon-virtual-dtor -Wsuggest-override -Woverloaded-virtual -Wformat -Wformat-security -Wdeprecated -Wwrite-strings -Wpointer-arith -Wstringop-truncation -Wall -Wextra -pedantic -fdiagnostics-color=auto)
  foreach( FLAG ${COMPILER_FLAGS} )
    ## meed to replace the minus or plus signs from the variables, because it is passed
    ## as a macro to g++ which causes a warning about no whitespace after macro
    ## definition
    STRING(REPLACE "-" "_" FLAG_WORD ${FLAG} )
    STRING(REPLACE "+" "P" FLAG_WORD ${FLAG_WORD} )

    check_cxx_compiler_flag( "${FLAG}" CXX_FLAG_WORKS_${FLAG_WORD} )
    if( ${CXX_FLAG_WORKS_${FLAG_WORD}} )
      message(STATUS "|+++> Adding ${FLAG} to CXX_FLAGS" )
      set ( CMAKE_CXX_FLAGS "${FLAG} ${CMAKE_CXX_FLAGS} ")
    else()
      message(STATUS "|+++> NOT Adding ${FLAG} to CXX_FLAGS" )
    endif()
  endforeach()

  set(COMPILER_FLAGS)
  foreach( FLAG ${COMPILER_FLAGS} )
    STRING(REPLACE "-" "_" FLAG_WORD ${FLAG} )
    STRING(REPLACE "+" "P" FLAG_WORD ${FLAG_WORD} )
    check_c_compiler_flag( "${FLAG}" C_FLAG_WORKS_${FLAG_WORD} )
    if( ${C_FLAG_WORKS_${FLAG_WORD}} )
      message(STATUS "|+++> Adding ${FLAG} to C_FLAGS" )
      set ( CMAKE_C_FLAGS "${FLAG} ${CMAKE_C_FLAGS} ")
    else()
      message(STATUS "|+++> NOT Adding ${FLAG} to C_FLAGS" )
    endif()
  endforeach()
  message(STATUS "|+++> C-Flags:   ${CMAKE_C_FLAGS}" )
  message(STATUS "|+++> CXX-Flags: ${CMAKE_CXX_FLAGS}" )
  set ( CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -Wl,--no-undefined,--as-needed")
  list(REMOVE_DUPLICATES ${CMAKE_CXX_FLAGS})
endmacro(online_set_compiler_flags)
#
#
# =======================================================================
macro(online_import_pluginmanager source_dir)
  find_library(PLUGINMGR GaudiPluginService)   #  PATH ${source_dir}/lib/libGaudiPluginService1.so)
  if ( ${PLUGINMGR} STREQUAL "PLUGINMGR-NOTFOUND" )
    message(ERROR "+++ Plugin manager not found: ${PLUGINMGR}")
    return()
  endif()
  add_library(GaudiPluginService IMPORTED SHARED GLOBAL)
  set_property(TARGET GaudiPluginService PROPERTY IMPORTED_LOCATION ${PLUGINMGR})
  target_include_directories(GaudiPluginService INTERFACE
                             $<BUILD_INTERFACE:${source_dir}/include/>
                             $<INSTALL_INTERFACE:include/>)
  add_library(Gaudi::GaudiPluginService ALIAS GaudiPluginService)
  message(STATUS "|+++> Importing Gaudi::GaudiPluginService ${PLUGINMGR}" )
  #
  find_program(LISTCOMPONENTS listcomponents)  # PATHS ${source_dir}/bin/listcomponents)
  if ( ${LISTCOMPONENTS} STREQUAL "LISTCOMPONENTS-NOTFOUND" )
    message(ERROR "+++ listcomponents not found: ${LISTCOMPONENTS}")
    return()
  endif()
  add_executable(listcomponents IMPORTED)
  set_property(TARGET listcomponents PROPERTY IMPORTED_LOCATION ${LISTCOMPONENTS})
  add_executable(Gaudi::listcomponents ALIAS listcomponents)
  message(STATUS "|+++> Importing Gaudi::listcomponents ${LISTCOMPONENTS}" )
endmacro(online_import_pluginmanager)
#
#
# =======================================================================
function(online_build_pluginmanager source_dir)
  add_library(GaudiPluginService SHARED ${source_dir}/src/PluginServiceV1.cpp ${source_dir}/src/PluginServiceV2.cpp)
  target_include_directories(GaudiPluginService PRIVATE ${source_dir}/include)
  target_include_directories(GaudiPluginService INTERFACE
                             $<BUILD_INTERFACE:${source_dir}/include/>
                             $<INSTALL_INTERFACE:include/>)
  target_compile_options(GaudiPluginService PRIVATE -Wno-unused-function -Wno-shadow)
  target_link_libraries(GaudiPluginService stdc++fs Threads::Threads ${CMAKE_DL_LIBS})
  add_library(Gaudi::GaudiPluginService ALIAS GaudiPluginService)
  install(TARGETS GaudiPluginService EXPORT Online 
          LIBRARY  DESTINATION "${ONLINE_INSTALL_LIBDIR}"
          INCLUDES DESTINATION "${ONLINE_INSTALL_INCLUDEDIR}")
  #
  add_executable(listcomponents ${source_dir}/src/listcomponents.cpp 
  			        ${source_dir}/src/PluginServiceV1.cpp
			        ${source_dir}/src/PluginServiceV2.cpp)
  target_include_directories(listcomponents PRIVATE ${source_dir}/include)
  target_compile_options(listcomponents PRIVATE -Wno-unused-function -Wno-shadow)
  target_link_libraries(listcomponents stdc++fs Threads::Threads ${CMAKE_DL_LIBS})
  add_executable(Gaudi::listcomponents ALIAS listcomponents)
  install(TARGETS listcomponents RUNTIME DESTINATION ${ONLINE_INSTALL_BINDIR})
endfunction(online_build_pluginmanager)
#
#
# =======================================================================
# -- Generate version header (for standalone build)
function(online_generate_version_header)
  configure_file(${PROJECT_SOURCE_DIR}/cmake/ONLINE_VERSION.h.in
	         ${PROJECT_BINARY_DIR}/include/ONLINE_VERSION.h @ONLY)
  install(FILES  ${PROJECT_BINARY_DIR}/include/ONLINE_VERSION.h 
          DESTINATION ${ONLINE_INSTALL_INCLUDEDIR})
endfunction(online_generate_version_header)
#
#
# =======================================================================
macro(online_end_configuration)

  get_property(ONLINE_ENVIRONMENT GLOBAL PROPERTY ${CMAKE_PROJECT_NAME}_ENVIRONMENT)
  ##message(STATUS "+++ ${PROJECT_NAME}_ENVIRONMENT: ${ONLINE_ENVIRONMENT}")
  get_property(ONLINE_RUNTIME TARGET target_runtime_paths PROPERTY extra_commands)
  message(STATUS "+++ target_runtime_paths[extra_commands]:")
  message(STATUS "${ONLINE_RUNTIME}") 

  configure_file(${PROJECT_SOURCE_DIR}/cmake/${CMAKE_PROJECT_NAME}Config.cmake.in
    ${PROJECT_BINARY_DIR}/${CMAKE_PROJECT_NAME}Config.cmake @ONLY
  )
  install(FILES ${PROJECT_BINARY_DIR}/${CMAKE_PROJECT_NAME}Config.cmake DESTINATION ./cmake )

  online_generate_package_configuration_files( ${CMAKE_PROJECT_NAME}Config.cmake )

  set ( ENV{ONLINE_LIBRARY_PATH} ${LD_LIBRARY_PATH} )
  set ( ONLINE_LIBRARY_PATH      ${LD_LIBRARY_PATH} )

  configure_file(cmake/thisonline.sh.in  bin/thisonline.sh @ONLY)
  install(FILES     ${CMAKE_BINARY_DIR}/bin/thisonline.sh DESTINATION bin )
  #install(PROGRAMS ${CMAKE_BINARY_DIR}/bin/run_test.sh   DESTINATION  bin )
  install(FILES     ${PROJECT_SOURCE_DIR}/cmake/find_package_path.py DESTINATION cmake )

  install(EXPORT Online NAMESPACE Online::
          FILE ${CMAKE_PROJECT_NAME}Config-targets.cmake
          DESTINATION cmake
  )
  install(FILES ${CMAKE_BINARY_DIR}/include/LinkDef.h DESTINATION include)
endmacro(online_end_configuration)
# =======================================================================
#
#
