# =======================================================================
if(NOT COMMAND lhcb_find_package)
  # Look for LHCb find_package wrapper
  find_file(LHCbFindPackage_FILE LHCbFindPackage.cmake)
  if(LHCbFindPackage_FILE)
      include(${LHCbFindPackage_FILE})
  else()
      # if not found, use the standard find_package
      macro(lhcb_find_package)
          find_package(${ARGV})
      endmacro()
  endif()
endif()
# =======================================================================
# -- Public dependencies
lhcb_find_package(Gaudi REQUIRED)
lhcb_find_package(LHCb REQUIRED)
# =======================================================================
function(online_gaudi_module name)
  gaudi_add_module(${name} SOURCES ${ARGN})
endfunction()
# =======================================================================
