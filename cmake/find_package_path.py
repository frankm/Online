import os, sys
config='';
default_view = ''

req_package = 'Unknown-package'
prefixes = {}

def find_directory(top, name):
  req_package = name.upper()
  package_names = os.listdir(top)
  for package in package_names:
    fpackage = top + '/' + package
    if package == '.cvmfscatalog' or not os.path.exists(fpackage) or not os.path.isdir(fpackage):
      continue
    if package.upper() != req_package:
      continue
    return (name, package, fpackage, )
  return None

def find_directory2(top, name):
  req_package = (name+'-').upper()
  package_names = os.listdir(top)
  for package in package_names:
    fpackage = top + '/' + package
    if package == '.cvmfscatalog' or not os.path.exists(fpackage) or not os.path.isdir(fpackage):
      continue
    if package.upper().find(req_package) < 0:
      continue
    return (name, package, fpackage, )
  return None

def get_package_version(fpackage):
  package_versions = os.listdir(fpackage)
  for version in package_versions:
    fversion = fpackage + '/' + version
    if version == '.cvmfscatalog' or not os.path.exists(fversion) or not os.path.isdir(fversion):
      continue
    return (version, fversion)
  return None

if __name__ == '__main__':
  import optparse
  parser = optparse.OptionParser()
  parser.description = "Package dependency resolver."
  parser.formatter.width = 132
  #
  #
  #  Debug options
  parser.add_option('-p', '--package', 
                    dest='package', default='',
                    help='Package name to be resolved',
                    metavar='<string>')
  parser.add_option('-v', '--view', 
                    dest='view', default=default_view,
                    help='Name of the LCG view to resolve dependency',
                    metavar='<string>')
  parser.add_option('-b', '--build', 
                    dest='build', default='',
                    help='Binary tag of build',
                    metavar='<string>')

  (opts, args) = parser.parse_args()
  if len(opts.package) and len(opts.view) and len(opts.build):
    package = find_directory(opts.view, opts.package)
    if package:
      version = get_package_version(package[2])
      if version:
        build = find_directory(version[1], opts.build)
        if build:
          print(str(os.path.realpath(build[2])))
          sys.exit(0)
        package_versions = os.listdir(package[2])
        for p in package_versions:
          fversion = package[2]+os.sep+p
          if fversion == '.cvmfscatalog' or not os.path.exists(fversion) or not os.path.isdir(fversion):
            continue
          build = find_directory(fversion, opts.build)
          if build:
            print(str(os.path.realpath(build[2])))
            sys.exit(0)
    package = find_directory2(opts.view+'/lib/cmake', opts.package)
    if package:
      print(str(os.path.realpath(package[2])))
      sys.exit(0)
    package = find_directory2(opts.view+'/lib64/cmake', opts.package)
    if package:
      print(str(os.path.realpath(package[2])))
      sys.exit(0)
  print('UNRESOLVED')

