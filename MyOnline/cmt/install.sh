#!/bin/bash
. /group/online/dataflow/scripts/shell_macros.sh
echo "Creating setup scripts"
crsetup;
. setup.x86_64-slc5-gcc43-opt.vars;
rm ROMon;
ln -s ${ROMONROOT}/scripts ROMon;
