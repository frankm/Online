LHCb Online data processing and dataflow software
=================================================

Master: Markus Frank

## Normalization
Added a variable taking into account the number of histograms merged.
This wariable is in Monitoring -> HistDiff and it is updated every time the add function is called.
